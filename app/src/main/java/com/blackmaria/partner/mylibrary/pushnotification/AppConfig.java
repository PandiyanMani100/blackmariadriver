package com.blackmaria.partner.mylibrary.pushnotification;
 
public interface AppConfig
{

    String TAG = "GCM TAG";
    String REG_ID = "regId";
    String APP_VERSION = "app_version";
    String PARSE_APPLICATION_ID = "973440324120";
    //public static final String PARSE_CLIENT_KEY = "AIzaSyB5Mey3n0-Vh3pvX2a_hpziypsosltMzUE";
    int NOTIFICATION_ID = 100;
}