package com.blackmaria.partner.mylibrary.otpreceiver;

/**
 * Created by Prem Kumar and Anitha on 1/13/2017.
 */

public interface OTPListener {
    public void otpReceived(String messageText);
}
