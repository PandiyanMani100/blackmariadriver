package com.blackmaria.partner.mylibrary.otpreceiver;

import android.Manifest;
import android.app.Activity;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;

/**
 * Created by Prem Kumar and Anitha on 1/17/2017.
 */

public class SmsVerifyCatcher
{
    private final static int PERMISSION_REQUEST_CODE = 12;

    private Activity activity;
    private Fragment fragment;
    private OTPListener otpListener;
    private OtpReader smsReceiver;
    private String phoneNumber;
    private String filter;
    private String senderName="";

    public SmsVerifyCatcher(Activity activity,String mSenderName, OTPListener onSmsCatchListener) {
        this.activity = activity;
        this.senderName=mSenderName;
        this.otpListener = onSmsCatchListener;
        smsReceiver = new OtpReader();
        smsReceiver.setCallback(this.otpListener,this.senderName);
    }

    public SmsVerifyCatcher(Activity activity, String mSenderName, Fragment fragment, OTPListener onSmsCatchListener) {
        this(activity,mSenderName,onSmsCatchListener);
        this.fragment = fragment;
    }


    public void onStart() {
        if (isStoragePermissionGranted(activity, fragment)) {
            registerReceiver();
        }
    }

    private void registerReceiver() {
        smsReceiver = new OtpReader();
        smsReceiver.setCallback(otpListener,senderName);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
        activity.registerReceiver(smsReceiver, intentFilter);

        System.out.println("------------prem otp registered------------");

    }

    public void setPhoneNumberFilter(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void onStop() {
        activity.unregisterReceiver(smsReceiver);
        System.out.println("------------prem otp unregisterReceiver------------");
    }

    public void setFilter(String regexp) {
        this.filter = regexp;
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                registerReceiver();
            }
        }
    }

    //For fragments
    public static boolean isStoragePermissionGranted(Activity activity, Fragment fragment) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.RECEIVE_SMS)
                    == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_SMS)
                            == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                if (fragment == null) {
                    ActivityCompat.requestPermissions(activity,
                            new String[]{Manifest.permission.RECEIVE_SMS,
                                    Manifest.permission.READ_SMS}, PERMISSION_REQUEST_CODE);
                } else {
                    fragment.requestPermissions(
                            new String[]{Manifest.permission.RECEIVE_SMS,
                                    Manifest.permission.READ_SMS}, PERMISSION_REQUEST_CODE);
                }
                return false;
            }
        } else {
            return true;
        }
    }
}
