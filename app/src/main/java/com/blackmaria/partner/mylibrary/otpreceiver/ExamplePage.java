package com.blackmaria.partner.mylibrary.otpreceiver;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.NonNull;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Prem Kumar and Anitha on 1/17/2017.
 */

public class ExamplePage extends Activity
{
    private String SENDER_NAME="IM-WAYSMS";
    private SmsVerifyCatcher smsVerifyCatcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        smsVerifyCatcher = new SmsVerifyCatcher(this, SENDER_NAME ,new OTPListener() {
            @Override
            public void otpReceived(String messageText) {
                String code = parseCode(messageText);
                System.out.println("-------otp code--------"+code);
                setOTP(code);
            }
        });

    }

    private String parseCode(String message) {
        Pattern p = Pattern.compile("\\b\\d{5}\\b");
        Matcher m = p.matcher(message);
        String code = "";
        while (m.find()) {
            code = m.group(0);
        }
        return code;
    }


    private void setOTP(String data) {
        if (data.length() > 0 && data.length() < 6) {
            char[] otp = data.trim().toCharArray();

            System.out.println("--------prem otp 0-----------"+otp[0]);
            System.out.println("--------prem otp 1-----------"+otp[1]);
            System.out.println("--------prem otp 2-----------"+otp[2]);
            System.out.println("--------prem otp 3-----------"+otp[3]);
            System.out.println("--------prem otp 4-----------"+otp[4]);
        }
    }



    @Override
    protected void onStart() {
        super.onStart();
        smsVerifyCatcher.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        smsVerifyCatcher.onStop();
    }

    /**
     * need for Android 6 real time permissions
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
