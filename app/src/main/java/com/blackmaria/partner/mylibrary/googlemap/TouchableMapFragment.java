package com.blackmaria.partner.mylibrary.googlemap;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;

public class TouchableMapFragment extends MapFragment {

    private View mOriginalContentView;
    private TouchableWrapper mTouchView;
    private CustomMapView customMapView;

   /* public void setTouchListener(TouchableWrapper.OnTouchListener onTouchListener) {
        mTouchView.setTouchListener(onTouchListener);
    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent,
                             Bundle savedInstanceState) {
        mOriginalContentView = super.onCreateView(inflater, parent,
                savedInstanceState);

      /*  mTouchView = new TouchableWrapper(getActivity());
        mTouchView.addView(mOriginalContentView);*/

        customMapView = new CustomMapView(getActivity());
        customMapView.addView(mOriginalContentView);


        return customMapView;
    }

    public void init(GoogleMap map) {
        customMapView.init(map);
    }

    @Override
    public View getView() {
        return mOriginalContentView;
    }

}
