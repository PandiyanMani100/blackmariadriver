package com.blackmaria.partner.mylibrary.volley;

import android.content.Context;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.blackmaria.partner.Utils.MyActivityLifecycleCallbacks;
import com.google.firebase.FirebaseApp;


public class AppController extends MultiDexApplication {

    public static final String TAG = AppController.class.getSimpleName();

    private RequestQueue mRequestQueue;
    private static Context context;
    private static AppController mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
//        Fabric.with(this, Crashlytics());
        FirebaseApp.initializeApp(this);
        mInstance = this;
        AppController.context = getApplicationContext();

    }

    public static Context getAppContext() {
        return AppController.context;
    }



    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(AppController.this);
        registerActivityLifecycleCallbacks(new MyActivityLifecycleCallbacks());
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }


    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }



}