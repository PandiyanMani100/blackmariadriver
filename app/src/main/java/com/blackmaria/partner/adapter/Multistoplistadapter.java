package com.blackmaria.partner.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.blackmaria.partner.R;
import com.blackmaria.partner.latestpackageview.Model.tripsummarypojo;

import java.util.ArrayList;

/**
 * Created by user144 on 8/7/2017.
 */

public class Multistoplistadapter extends BaseAdapter {

    private static ArrayList<tripsummarypojo.Response.Details.Multi_stops> data;
    private LayoutInflater mInflater;
    private Activity context;


    public Multistoplistadapter(Activity c, ArrayList<tripsummarypojo.Response.Details.Multi_stops> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    public class ViewHolder {

        private TextView stopsss,stopsssvalue;

    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        final ViewHolder holder;

        if (convertView == null) {
            view = mInflater.inflate(R.layout.multistopsc, parent, false);
            holder = new ViewHolder();
            holder.stopsss=(TextView)view.findViewById(R.id.stopsss);
            holder.stopsssvalue=(TextView)view.findViewById(R.id.stopsssvalue);

            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        holder.stopsssvalue.setText(data.get(position).getLocation().toUpperCase());
        holder.stopsss.setText(context.getString(R.string.stopss)+" "+(position+1));




        return view;
    }


}
