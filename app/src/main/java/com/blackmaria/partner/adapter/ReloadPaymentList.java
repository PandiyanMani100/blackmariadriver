package com.blackmaria.partner.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;


import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ImageLoader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user144 on 9/26/2017.
 */

public class ReloadPaymentList extends BaseAdapter {

    private static HashMap<String, ArrayList<String>> data;
    private LayoutInflater mInflater;
    private Activity context;
    private ImageLoader imageLoader;
    RecyclerItemClick  recycleritemclick;
    RelativeLayout.LayoutParams params;

    public ReloadPaymentList(Activity c, HashMap<String, ArrayList<String>> d, RecyclerItemClick  recycleritemclick) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
        this.recycleritemclick=recycleritemclick;
        imageLoader = new ImageLoader(context);

    }


    @Override
    public int getCount() {
        return data.get("bankInactive").size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private ImageView Iv_e_wallet_payment_type;
        private RelativeLayout Ll_car;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View view;
        final ViewHolder holder;

        if (convertView == null) {
            view = mInflater.inflate(R.layout.ewallet_singal_list_item_singal_new, parent, false);
            holder = new ViewHolder();

            holder.Iv_e_wallet_payment_type = (ImageView) view.findViewById(R.id.e_wallet_page_single_payment_list_imageview);
            holder.Ll_car = (RelativeLayout) view.findViewById(R.id.bookmyride_single_car_layout);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        try {
            if (!data.get("bankCode").get(position).equalsIgnoreCase("xendit-bankpayment")) {
                params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(20, 20, 20, 20);
                params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
                holder.Iv_e_wallet_payment_type.setLayoutParams(params);

            }else{
                params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(0, 0, 0, 0);
                params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
                holder.Iv_e_wallet_payment_type.setLayoutParams(params);
            }
            String image = data.get("bankInactive").get(position);
            Picasso.with(context).load(image).into(holder.Iv_e_wallet_payment_type);
        } catch (Exception e) {
            System.out.println("===paymentoptionadpter error==="+e+e.getMessage()+e.toString());
            e.printStackTrace();
        }


        holder.Iv_e_wallet_payment_type.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recycleritemclick.onItemClick(position);
            }
        });
        return view;


    }
    public interface RecyclerItemClick {

        public void onItemClick(int position);

    }

}

