package com.blackmaria.partner.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.blackmaria.partner.Pojo.ReferralImageBean;
import com.blackmaria.partner.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Ganesh on 18/05/2017.
 */

public class ReferralImageAdapter extends BaseAdapter {

    private ArrayList<ReferralImageBean> data;
    private LayoutInflater mInflater;
    private Context context;

    public ReferralImageAdapter(Context c, ArrayList<ReferralImageBean> data) {
        context = c;
        mInflater = LayoutInflater.from(context);
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public ReferralImageBean getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private RelativeLayout RL_image/*, RL_imageHolder*/;
        private ImageView Iv_profile;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.row_earnings_referral_profile_single, parent, false);
            holder = new ViewHolder();
            holder.RL_image = (RelativeLayout) view.findViewById(R.id.Rl_image);
//            holder.RL_imageHolder = (RelativeLayout) view.findViewById(R.id.RL_img_holder);
            holder.Iv_profile = (ImageView) view.findViewById(R.id.img_profile);

            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }


        String imageUrl = data.get(position).getDriver_image();
        if (imageUrl.length() > 0) {
            Picasso.with(context).load(imageUrl).placeholder(R.drawable.profile_image_dummy).error(R.drawable.profile_image_dummy).memoryPolicy(MemoryPolicy.NO_CACHE).into(holder.Iv_profile);
        }


        if (data.size() <= 3) {
            //Code to adjust image at center
            Display display = ((Activity) context).getWindowManager().getDefaultDisplay();
            System.out.println("Display Width = " + display.getWidth());
            if (data.size() == 1) {
                ViewGroup.LayoutParams params = holder.RL_image.getLayoutParams();
                params.width = (display.getWidth()) - 8;
                holder.RL_image.setLayoutParams(params);
            } else if (data.size() == 2) {
                ViewGroup.LayoutParams params = holder.RL_image.getLayoutParams();
                params.width = (display.getWidth() / 2);
                holder.RL_image.setLayoutParams(params);
            } else {
                ViewGroup.LayoutParams params = holder.RL_image.getLayoutParams();
                params.width = (display.getWidth() / 3) - 8;
                holder.RL_image.setLayoutParams(params);
            }
        }


        return view;
    }
}

