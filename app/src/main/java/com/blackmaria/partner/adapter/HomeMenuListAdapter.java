package com.blackmaria.partner.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;


/**
 * Created by user88 on 5/31/2016.
 */
public class HomeMenuListAdapter extends BaseAdapter {

    Context context;
    String[] mTitle;
    int[] mIcon;
    LayoutInflater inflater;
    View itemView;
    public SessionManager session;

    public HomeMenuListAdapter(Context context, String[] title, int[] icon) {
        this.context = context;
        this.mTitle = title;
        this.mIcon = icon;
    }

    @Override
    public int getCount() {
        return mTitle.length;
    }

    @Override
    public Object getItem(int position) {
        return mTitle [position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Declare Variables
        TextView txtTitle;
        ImageView imgIcon;
        RelativeLayout general_layout;

        session = new SessionManager(context);

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        itemView = inflater.inflate(R.layout.navigation_drawer_single, parent, false);

        txtTitle = (TextView) itemView.findViewById(R.id.title);
        imgIcon = (ImageView) itemView.findViewById(R.id.icon);
        general_layout = (RelativeLayout) itemView.findViewById(R.id.drawer_list_item_normal_layout);
            general_layout.setVisibility(View.VISIBLE);
        if (position == 2 || position == 3 || position == 6 || position == 7) {
            general_layout.setBackgroundColor(context.getResources().getColor(R.color.drawer_secondary_color));
        }
            imgIcon.setImageResource(mIcon[position]);
            txtTitle.setText(mTitle[position]);


        return itemView;
    }
}
