package com.blackmaria.partner.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.blackmaria.partner.Interface.RatingpageOnclick;
import com.blackmaria.partner.Pojo.RatingPojo;
import com.blackmaria.partner.R;

import java.util.ArrayList;


/**
 * Created by GANESH on 23/08/2017.
 */
public class RatingAdapterNew extends BaseAdapter {

    private ArrayList<RatingPojo> data;
    private LayoutInflater mInflater;
    private Context context;
    RatingpageOnclick ratingpageOnclick;

    public RatingAdapterNew(Context c, ArrayList<RatingPojo> d, RatingpageOnclick RatingpageOnclick_local) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
        ratingpageOnclick = RatingpageOnclick_local;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private TextView title;
        private RatingBar rating;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.row_single_rating, parent, false);
            holder = new ViewHolder();
            holder.title = (TextView) view.findViewById(R.id.txt_title);
            holder.rating = (RatingBar) view.findViewById(R.id.rb_rating);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        holder.title.setText(data.get(position).getRatingName());

        holder.rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                ratingpageOnclick.getonclick(data,position,rating);
//                data.get(position).setRatingcount(String.valueOf(rating));
//                notifyDataSetChanged();
            }
        });

        return view;
    }
}


