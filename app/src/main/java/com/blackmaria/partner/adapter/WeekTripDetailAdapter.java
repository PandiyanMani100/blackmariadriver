package com.blackmaria.partner.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.graphics.Color;

import com.blackmaria.partner.Pojo.WeekDetails;
import com.blackmaria.partner.R;

import java.util.ArrayList;


public class WeekTripDetailAdapter extends BaseAdapter {

    private ArrayList<WeekDetails> weekDetailsList;
    private LayoutInflater mInflater;
    private Context context;

    public WeekTripDetailAdapter(Context c, ArrayList<WeekDetails> weekDetailsList) {
        context = c;
        mInflater = LayoutInflater.from(context);
        this.weekDetailsList = weekDetailsList;
    }

    @Override
    public int getCount() {
        return weekDetailsList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private TextView Tv_week, Tv_trips, Tv_distance;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.row_list_item_week_detail, parent, false);
            holder = new ViewHolder();
            holder.Tv_week = (TextView) view.findViewById(R.id.txt_week);
            holder.Tv_trips = (TextView) view.findViewById(R.id.txt_trips);
            holder.Tv_distance = (TextView) view.findViewById(R.id.txt_distance);

            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }



        if(weekDetailsList.get(position).getDate_f().contains("1970")){
            holder.Tv_week.setVisibility(View.INVISIBLE);
        }else{
            holder.Tv_week.setVisibility(View.VISIBLE);
            holder.Tv_week.setText(weekDetailsList.get(position).getDay());
        }


        String sTotalRides = weekDetailsList.get(position).getRidecount();
        if (sTotalRides != null && sTotalRides.length() > 0) {
            int rides = 0;
            try {
                if (sTotalRides.equalsIgnoreCase("-")) {
                    rides = 0;
                } else {
                    rides = Integer.parseInt(sTotalRides);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (rides > 1) {
                holder.Tv_trips.setText(sTotalRides + " " + context.getResources().getString(R.string.mytrips_label_trips));
            } else {
                if (rides == 0) {
                    if (sTotalRides.equalsIgnoreCase("-")) {
                        holder.Tv_trips.setText(sTotalRides);
                    } else {
                        holder.Tv_trips.setText(sTotalRides + " " + context.getResources().getString(R.string.mytrips_label_trip));
                    }
                } else {
                    holder.Tv_trips.setText(sTotalRides + " " + context.getResources().getString(R.string.mytrips_label_trip));
                }
            }
        }

        holder.Tv_distance.setText(weekDetailsList.get(position).getDistance());

        if (position != 0) {
            if (position == (weekDetailsList.size() - 1) - 1) {
                holder.Tv_week.setBackgroundResource(R.drawable.flat_gradient_blue_bg1);
                holder.Tv_week.setTextColor(Color.WHITE);
            } else if (position == weekDetailsList.size() - 1) {
                holder.Tv_week.setBackgroundResource(R.drawable.flat_gradient_red);
                holder.Tv_week.setTextColor(Color.WHITE);
            } else {
                holder.Tv_week.setBackgroundColor(Color.parseColor("#4A5982"));
            }
        } else {
            holder.Tv_week.setBackgroundColor(Color.parseColor("#4A5982"));
        }


        return view;
    }
}

