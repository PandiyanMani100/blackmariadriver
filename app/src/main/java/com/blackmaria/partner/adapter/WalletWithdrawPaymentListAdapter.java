package com.blackmaria.partner.adapter;


import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.blackmaria.partner.Pojo.WalletMoneyPojo;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ImageLoader;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class WalletWithdrawPaymentListAdapter extends BaseAdapter {

    private ArrayList<WalletMoneyPojo> data;
    private LayoutInflater mInflater;
    private Context context;
    private ImageLoader imageLoader;

    public WalletWithdrawPaymentListAdapter(Context c, ArrayList<WalletMoneyPojo> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
        imageLoader = new ImageLoader(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    public class ViewHolder {
        private ImageView Iv_payment_type;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        final ViewHolder holder;

        if (convertView == null) {
            view = mInflater.inflate(R.layout.wallet_money_single_page, parent, false);
            holder = new ViewHolder();

            holder.Iv_payment_type = (ImageView) view.findViewById(R.id.wallet_page_single_payment_list_imageview);


            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }
        /*if ("false".equalsIgnoreCase(data.get(position).getPayment_selected_payment_id())) {
            Picasso.with(context).load(data.get(position).getPayment_normal_img()).into(holder.Iv_payment_type);

        } else {

            Picasso.with(context).load(data.get(position).getPayment_active_img()).into(holder.Iv_payment_type);
        }*/

        String imageUrl = "";

        if (data.get(position).getPayment_code().equalsIgnoreCase(data.get(position).getPayment_selected_payment_id())) {
            imageUrl = data.get(position).getPayment_active_img();
        } else {
            imageUrl = data.get(position).getPayment_normal_img();
        }

        if (imageUrl.length() > 0) {
            Picasso.with(context).load(imageUrl).memoryPolicy(MemoryPolicy.NO_CACHE).into(holder.Iv_payment_type);
        }


        //Code to adjust image at center
       /* Display display = ((Activity) context).getWindowManager().getDefaultDisplay();
        if (data.size() == 1) {
            ViewGroup.LayoutParams params = holder.Iv_payment_type.getLayoutParams();
            params.width = (display.getWidth()) - 8;
            holder.Iv_payment_type.setLayoutParams(params);
        } else if (data.size() == 2) {
            ViewGroup.LayoutParams params = holder.Iv_payment_type.getLayoutParams();
            params.width = (display.getWidth() / 2) - 8;
            holder.Iv_payment_type.setLayoutParams(params);
        } else {
            ViewGroup.LayoutParams params = holder.Iv_payment_type.getLayoutParams();
            params.width = (display.getWidth() / 3) - 8;
            holder.Iv_payment_type.setLayoutParams(params);
        }*/

        return view;
    }


}
