package com.blackmaria.partner.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import com.blackmaria.partner.Pojo.ProfilePojo;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ImageLoader;
import com.blackmaria.partner.latestpackageview.widgets.CustomTextView;

import java.util.ArrayList;

/**
 * Created by user14 on 12/30/2016.
 */

public class ProfileAdapter extends BaseAdapter {

    private ArrayList<ProfilePojo> data;
    private ImageLoader imageLoader;
    private LayoutInflater mInflater;
    private Context context;

    public ProfileAdapter(Context c, ArrayList<ProfilePojo> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
        imageLoader = new ImageLoader(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private LinearLayout LL_texts;
        private CustomTextView value;
        private CustomTextView title;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.profile_page_single, parent, false);
            holder = new ViewHolder();
            holder.LL_texts = (LinearLayout) view.findViewById(R.id.Ll_texts);
            holder.title = (CustomTextView) view.findViewById(R.id.profilepage_single_title);
            holder.value = (CustomTextView) view.findViewById(R.id.profilepage_single_value);

            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        holder.title.setAllCaps(true);
        holder.value.setAllCaps(true);
        holder.title.setTextColor(Color.BLACK);
        holder.value.setTextColor(Color.BLACK);
        holder.title.setText(data.get(position).getTitile());

        if(data.get(position).getValue().equalsIgnoreCase("-")){
            holder.value.setText(": ");
//            holder.value.setText(data.get(position).getValue().replace("-",""));
        }else{
            holder.value.setText(":"+data.get(position).getValue());
        }


        if (position == data.size() - 1) {
            holder.LL_texts.setPadding(0, 16, 0, 16);
        } else {
            holder.LL_texts.setPadding(0, 16, 0, 0);
        }

        return view;
    }
}



