package com.blackmaria.partner.adapter


import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.blackmaria.partner.Pojo.TextMessage
import com.blackmaria.partner.R
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import java.text.SimpleDateFormat
import java.util.*


class MessagesAdapter(private var messages: ArrayList<TextMessage>, private var driverid:String, private var userimage:String, private var driverimage:String, private var ctx:Context)  : RecyclerView.Adapter<MessagesAdapter.MessageViewHolder>() {

    companion object {
        private const val SENT = 0
        private const val RECEIVED = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {
        val view = when (viewType) {
            SENT -> {
                LayoutInflater.from(parent.context).inflate(R.layout.item_sent, parent, false)
            }
            else -> {
                LayoutInflater.from(parent.context).inflate(R.layout.item_received, parent, false)
            }
        }
        return MessageViewHolder(view)
    }

    override fun getItemCount() = messages.size


    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
        holder.bind(messages[position])
    }

    override fun getItemViewType(position: Int): Int {
        return if (messages[position].senderid!!.contentEquals(driverid))
        {
            SENT
        }
        else
        {
            RECEIVED
        }
    }

    fun appendMessage(message: TextMessage) {
        this.messages.add(message)
        notifyItemInserted(this.messages.size - 1)
    }

    inner class MessageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val messageText: TextView = itemView.findViewById(R.id.message_text)
        private val driverphoto: ImageView = itemView.findViewById(R.id.driverphoto)
        private val messagetiime: TextView = itemView.findViewById(R.id.messagetiime)
        private val imagesent: ImageView = itemView.findViewById(R.id.imagesent)

        private val textlayout: LinearLayout = itemView.findViewById(R.id.textlayout)
        private val imagelayout: LinearLayout = itemView.findViewById(R.id.imagelayout)

        fun bind(message: TextMessage) {
            if (message is TextMessage) {


                if(message.message.toString().startsWith("https"))
                {
                    textlayout.visibility = View.GONE
                    imagelayout.visibility = View.VISIBLE
                    Glide.with(ctx)
                            .asBitmap()
                            .apply(RequestOptions().override(200, 200))
                            .load(message.message)
                            .into(object : CustomTarget<Bitmap>() {
                                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                                    imagesent.setImageBitmap(resource)
                                }
                                override fun onLoadCleared(placeholder: Drawable?)
                                {
                                }
                            })
                }
                else
                {
                    textlayout.visibility = View.VISIBLE
                    imagelayout.visibility = View.GONE
                    messageText.text = message.message
                }



                try {
                    val calendar = Calendar.getInstance(Locale.ENGLISH)

                    val tz = TimeZone.getDefault()
                    calendar.timeInMillis = message.timestamp!!.toLong() * 1000
                    calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.timeInMillis))
                    val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm")
                    val currenTimeZone = calendar.time as Date
                    messagetiime.text = message.date
                } catch (e: Exception) {
                }


                var imageofsender:String=""
                if(message.senderid.equals(driverid))
                {
                    imageofsender=driverimage
                }
                else
                {
                    imageofsender=userimage
                }

                Glide.with(ctx)
                        .asBitmap()
                        .apply(RequestOptions().override(60, 60))
                        .load(imageofsender)
                        .into(object : CustomTarget<Bitmap>() {
                            override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                                driverphoto.setImageBitmap(resource)
                            }
                            override fun onLoadCleared(placeholder: Drawable?)
                            {
                            }
                        })
            }
        }
    }
}