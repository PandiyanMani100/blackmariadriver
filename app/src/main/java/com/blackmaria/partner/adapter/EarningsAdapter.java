package com.blackmaria.partner.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blackmaria.partner.Pojo.EarningsPojo;
import com.blackmaria.partner.R;

import java.util.ArrayList;

/**
 * Created by Prem Kumar and Anitha on 3/6/2017.
 */

public class EarningsAdapter extends BaseAdapter {

    private ArrayList<EarningsPojo> data;
    private LayoutInflater mInflater;
    private Context context;

    public EarningsAdapter(Context c, ArrayList<EarningsPojo> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private TextView Tv_no, Tv_RideID, Tv_Duration, Tv_distance, Tv_earning, Tv_status;
        private LinearLayout Ll_trip;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.earnings_single, parent, false);
            holder = new ViewHolder();
            holder.Tv_no = (TextView) view.findViewById(R.id.earnings_single_no_textView);
            holder.Tv_RideID = (TextView) view.findViewById(R.id.earnings_single_date_textView);
            holder.Tv_Duration = (TextView) view.findViewById(R.id.earnings_single_trip_textView);
            holder.Tv_distance = (TextView) view.findViewById(R.id.earnings_single_distance_textView);
            holder.Tv_earning = (TextView) view.findViewById(R.id.earnings_single_earnings_textView);
            holder.Tv_status = (TextView) view.findViewById(R.id.earnings_single_status_textView);
            holder.Ll_trip = (LinearLayout) view.findViewById(R.id.earnings_single_info_header_linear_layout);

            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        holder.Tv_no.setText(data.get(position).getsNo());
        holder.Tv_RideID.setText(data.get(position).getsRideId());
        holder.Tv_Duration.setText(data.get(position).getsDuration());
        holder.Tv_distance.setText(data.get(position).getsDistance());
        holder.Tv_earning.setText(data.get(position).getsEarnings());

        if ((position % 2) == 0) {
            //Is even
            holder.Ll_trip.setBackgroundColor(Color.parseColor("#000000"));
        } else {
            //Is odd
            holder.Ll_trip.setBackgroundColor(Color.parseColor("#74736F"));
        }

       // holder.Tv_status.setText(data.get(position).getsStatus());


        return view;
    }
}

