package com.blackmaria.partner.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blackmaria.partner.Pojo.TodayDetails;
import com.blackmaria.partner.R;

import java.util.ArrayList;

/**
 * Created by GANESH on 13/07/2017.
 */

public class EarningsDayAdapterNew extends BaseAdapter {

    private ArrayList<TodayDetails> data;
    private LayoutInflater mInflater;
    private Context context;

    public EarningsDayAdapterNew(Context c, ArrayList<TodayDetails> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private RelativeLayout RL_main;
        private TextView Tv_count, Tv_name, Tv_amount;
        private ImageView Iv_folder;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.row_single_earnings_today_new, parent, false);
            holder = new ViewHolder();
            holder.RL_main = (RelativeLayout) view.findViewById(R.id.Rl_item);
            holder.Tv_count = (TextView) view.findViewById(R.id.txt_count);
            holder.Tv_name = (TextView) view.findViewById(R.id.txt_name);
            holder.Tv_amount = (TextView) view.findViewById(R.id.txt_amount);
            holder.Iv_folder = (ImageView) view.findViewById(R.id.img_earning);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        if (position % 2 == 0) {
            holder.RL_main.setBackgroundResource(R.drawable.curved_gradient_blue);
        } else {
            holder.RL_main.setBackgroundResource(R.drawable.curved_today_earning_dark_bg);
        }


        holder.Tv_count.setText(data.get(position).getS_no());
        holder.Tv_name.setText(data.get(position).getUser_name() + "\nCRN NO:\t" + data.get(position).getRide_id());
        holder.Tv_amount.setText(data.get(position).getCurrency() + " " + data.get(position).getDriver_earning());

        return view;
    }
}

