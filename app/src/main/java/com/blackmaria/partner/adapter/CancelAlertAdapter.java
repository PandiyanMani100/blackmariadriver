package com.blackmaria.partner.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.blackmaria.partner.latestpackageview.Model.CancelReasonPojo;
import com.blackmaria.partner.R;
import com.blackmaria.partner.latestpackageview.widgets.CustomTextView;

import java.util.ArrayList;

/**
 * Created by Jayachandarn on 12/29/2016.
 */

public class CancelAlertAdapter extends BaseAdapter {

    private ArrayList<CancelReasonPojo> listData;

    private LayoutInflater layoutInflater;

    public CancelAlertAdapter(Context context, ArrayList<CancelReasonPojo> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.cancel_alert_single, null);
            holder = new ViewHolder();
            holder.cancel_check = (ImageView) convertView.findViewById(R.id.cancel_reason_check_imageview);
            holder.cancel_reason = (CustomTextView) convertView.findViewById(R.id.cancel_alert_single_gift_range_textview);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if ("1".equalsIgnoreCase(listData.get(position).getReasonstatus())){
            holder.cancel_check.setImageResource(R.drawable.fare_breakup_checked);
        }else{
            holder.cancel_check.setImageResource(R.drawable.fare_breakup_unchecked);
        }


        holder.cancel_reason.setText(listData.get(position).getReason());

        return convertView;
    }

    static class ViewHolder {
        ImageView cancel_check;
        CustomTextView cancel_reason,redeem_gift;
    }

}