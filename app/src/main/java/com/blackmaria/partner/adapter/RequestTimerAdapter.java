package com.blackmaria.partner.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.GPSTracker_Driver;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.app.TimerPage;
import com.blackmaria.partner.app.TripPageNew;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.blackmaria.partner.latestpackageview.Database.GEODBHelper;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Prem Kumar and Anitha on 12/17/2016.
 */

public class RequestTimerAdapter {

    private SessionManager sm;
    private LayoutInflater mInflater;
    private Activity context;
    private LinearLayout listview;
    public int req_count;

    private CountDownTimer timer;
    private String KEY1 = "key1";
    private String KEY2 = "key2";
    private String KEY3 = "key3";
    private String KEY5 = "key5";
    private String rider_id = "";
    private Dialog dialog;
    private int seconds = 0;
    private MediaPlayer mediaPlayer;
    private Location myLocation;
    private TimerPage.TimerCompleteCallback timerCompleteCallback;
    private Handler mHandler;
    public static String userID;
    private GPSTracker_Driver gps;
    private ServiceRequest mServiceRequest;

    private GEODBHelper myDBHelper;


    public CircularHandler ch;
    ArrayList<CircularHandler> arrobj = new ArrayList<CircularHandler>();

    public int cur_count;
    ViewHolder holder1;
    private Dialog accept_dialog;

    public class ViewHolder {
        public int count;
        public Button accept;
        public Button decline;
        public TextView txtRequestCount, Tv_estimated_arrival;
        //        public TextView cabily_alert_address;
        private RelativeLayout Rl_ride_Requst_layout;
        public JSONObject data;
        //        private TextView Tv_pickUpAddress;
        public TextView Tv_req_title;

    }

    public RequestTimerAdapter(Activity context, Location myLocation, LinearLayout listview) {
        this.context = context;
        sm = new SessionManager(context);
        mInflater = LayoutInflater.from(context);
        this.listview = listview;
        this.myLocation = myLocation;
        mHandler = new Handler();
        myDBHelper = new GEODBHelper(context);

        gps = new GPSTracker_Driver(context);
        if (gps != null && gps.canGetLocation() && gps.isgpsenabled()) {
            this.myLocation = gps.getLocation();
        } else {
            gps.showSettingsAlert();
        }

    }

    public void setTimerCompleteCallBack(TimerPage.TimerCompleteCallback callBack) {
        this.timerCompleteCallback = callBack;
    }

    public View getView(int i, JSONObject data) {
        View view;
        ViewHolder holder;
        String data1 = " ";
        // JSONObject data = (JSONObject) getItem(i);
        view = mInflater.inflate(R.layout.request_timer_single, null, false);
        holder = new ViewHolder();
        holder.data = data;
        holder.count = i;
        holder.Tv_req_title = (TextView) view.findViewById(R.id.txt_label_request_title);
        holder.accept = (Button) view.findViewById(R.id.btn_accept_req);
        holder.decline = (Button) view.findViewById(R.id.btn_decline_req);
        holder.Tv_estimated_arrival = (TextView) view.findViewById(R.id.txt_away_time);
//        holder.cabily_alert_address = (TextView) view.findViewById(R.id.cabily_alert_address);
        holder.txtRequestCount = (TextView) view.findViewById(R.id.txt_req_count);
        holder.Rl_ride_Requst_layout = (RelativeLayout) view.findViewById(R.id.Ll_ride_request_layout);
//        holder.Tv_pickUpAddress = (TextView) view.findViewById(R.id.request_location_pickUp_address_textView);

        view.setTag(holder);

        HashMap<String, Integer> user = sm.getRequestCount();
        req_count = user.get(SessionManager.KEY_COUNT);
        holder.accept.setTag(holder);
        holder.decline.setTag(holder);
        holder.accept.setOnClickListener(acceptBtnlistener);
        holder.decline.setOnClickListener(new DeclineBtnListener(i));

        holder.Tv_req_title.setText(context.getResources().getString(R.string.trip_page_label_incoming_request) + " " + String.valueOf(i));
        holder.Tv_estimated_arrival.setText("" + getDataForPosition(i, KEY5, data));
//        holder.Tv_pickUpAddress.setText("" + getDataForPosition(i, KEY3, data));

        holder.txtRequestCount.setEnabled(false);
        String position = getDataForPosition(i, KEY3, data);
        holder.txtRequestCount.setFocusable(false);
//        holder.txtRequestCount.setMaxValue(Integer.parseInt(getDataForPosition(i, KEY2, data)));
//        holder.txtRequestCount.setValueAnimated(0);
//        holder.txtRequestCount.setTextSize(30);
//        holder.txtRequestCount.setAutoTextSize(true);
//        holder.txtRequestCount.setTextScale(0.6f);
//        holder.txtRequestCount.setTextColor(context.getResources().getColor(R.color.orange_color));
        //      mHandler.post(new CircularHandler(holder,Integer.parseInt(getDataForPosition(i, KEY2,data))));
        ch = new CircularHandler(holder, Integer.parseInt(getDataForPosition(i, KEY2, data)));
        mHandler.post(ch);
        arrobj.add(ch);
        return view;
    }

    private class CircularHandler implements Runnable {
        ViewHolder holder;
        Integer value;
        boolean isRunning;

        public CircularHandler(ViewHolder holder, Integer val) {
            this.holder = holder;
            isRunning = true;
            value = val;
        }

        @Override
        public void run() {
            if (isRunning) {
                value = value - 1;

                holder.txtRequestCount.setText(String.valueOf(Math.abs(value)));
//                holder.txtRequestCount.setTextMode(TextMode.TEXT);
//                holder.txtRequestCount.setValueAnimated(value, 500);
                mHandler.postDelayed(this, 1000);
                System.out.println("counter----jai--------------" + value);
                if (value == 0) {
                    mHandler.removeCallbacks(this);
                    if (timerCompleteCallback != null) {
                        holder.Rl_ride_Requst_layout.setVisibility(View.GONE);

                        System.out.println("requestcount2 above-----jai-------------" + req_count);


                        HashMap<String, Integer> user = sm.getRequestCount();
                        req_count = user.get(SessionManager.KEY_COUNT);
                        System.out.println("requestcount2 above----jai--------------" + req_count);


                        SessionManager session = new SessionManager(context);


                        if (req_count > 0) {
                            req_count = req_count - 1;
                            session.setRequestCount(req_count);
                        } else {
                        }
                        System.out.println("requestcount2 below---------jai---------" + req_count);

                        if (req_count == 0) {

                            System.out.println("activity  finished--------jai----------" + req_count);
                            sm.setRequestCount(0);
                            context.finish();
                            stopPlayer();
                            context.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        }
                    }
                    isRunning = false;
                }
            }
        }
    }

    private String getDataForPosition(int i, String key, JSONObject data) {
        try {
            return (String) data.get(key);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }


    private View.OnClickListener acceptBtnlistener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            /*for (int i = 1; i <= arrobj.size(); i++) {
                CircularHandler ch2 = arrobj.get(i - 1);
                if (ch2 != null) {
                    mHandler.removeCallbacks(ch2);
                }
            }

            sm.setRequestCount(0);*/

            ViewHolder holder = (ViewHolder) view.getTag();
            HashMap<String, String> jsonParams = new HashMap<String, String>();
            HashMap<String, String> userDetails = sm.getUserDetails();
            String driverId = userDetails.get(SessionManager.KEY_DRIVERID);
            rider_id = getDataForPosition(holder.count, KEY1, holder.data);
            cur_count = holder.count;
            holder1 = holder;

            myDBHelper.insertRide_id(rider_id);


            jsonParams.put("ride_id", "" + rider_id);
            jsonParams.put("driver_id", "" + driverId);
            if (TimerPage.myLocation != null) {
                jsonParams.put("driver_lat", "" + TimerPage.myLocation.getLatitude());
                jsonParams.put("driver_lon", "" + TimerPage.myLocation.getLongitude());
            } else {
                if (myLocation != null) {
                    jsonParams.put("driver_lat", "" + myLocation.getLatitude());
                    jsonParams.put("driver_lon", "" + myLocation.getLongitude());
                } else {
                    jsonParams.put("driver_lat", "" + "");
                    jsonParams.put("driver_lon", "" + "");
                }

            }

            AcceptRequest(Iconstant.driverAcceptRequest_Url, jsonParams);
            showDialog();


        }
    };


    private void AcceptRequest(String Url, HashMap<String, String> jsonParams) {

        accept_dialog = new Dialog(context);
        accept_dialog.getWindow();
        accept_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        accept_dialog.setContentView(R.layout.custom_loading);
        accept_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        accept_dialog.setCanceledOnTouchOutside(false);
        accept_dialog.show();

        TextView dialog_title = (TextView) accept_dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(context.getResources().getString(R.string.action_pleasewait));


        System.out.println("--------------AcceptRequest Url-------------------" + Url);
        System.out.println("--------------jsonParams-------------------" + jsonParams);

        mServiceRequest = new ServiceRequest(context);
        mServiceRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------response-------------------" + response);
                String Sstatus = "", SResponse = "", Str_Username = "", Str_UserId = "", Str_User_email = "", Str_Userphoneno = "", Str_Userrating = "", Str_userimg = "", Str_pickuplocation = "", Str_pickuplat = "", Str_pickup_long = "",
                        Str_pickup_time = "", location = "", user_gender = "", eta_arrival = "", member_since = "", title_text = "", subtitle_text = "", Str_message = "", Str_droplat = "", Str_droplon = "", str_drop_location = "", Zero_res = "",
                        Str_payment_type = "", Str_cancelled_rides = "", Str_completed_rides = "", Str_rider_type = "", Str_payment_icon = "";

                try {
                    JSONObject object1 = new JSONObject(response);
                    Sstatus = object1.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        System.out.println("status-----------" + Sstatus);
                        JSONObject jobject = object1.getJSONObject("response");
                        JSONObject jobject2 = jobject.getJSONObject("user_profile");
                        Str_message = jobject.getString("message");
                        Str_payment_icon = jobject.getString("payment_icon");
                        Str_Username = jobject2.getString("user_name");
                        userID = jobject2.getString("user_id");
                        Str_User_email = jobject2.getString("user_email");
                        Str_Userphoneno = jobject2.getString("phone_number");
                        Str_Userrating = jobject2.getString("user_review");
                        Str_pickuplocation = jobject2.getString("pickup_location");
                        Str_pickuplat = jobject2.getString("pickup_lat");
                        Str_pickup_long = jobject2.getString("pickup_lon");
                        eta_arrival = jobject2.getString("eta_arrival");
                        member_since = jobject2.getString("member_since");
                        user_gender = jobject2.getString("user_gender");
                        location = jobject2.getString("location");
                        Str_pickup_time = jobject2.getString("pickup_time");
                        Str_userimg = jobject2.getString("user_image");
                        Str_droplat = jobject2.getString("drop_lat");
                        Str_droplon = jobject2.getString("drop_lon");
                        str_drop_location = jobject2.getString("drop_loc");
                        Str_payment_type = jobject2.getString("payment_type");
                        Str_cancelled_rides = jobject2.getString("cancelled_rides");
                        Str_completed_rides = jobject2.getString("completed_rides");
                        Str_rider_type = jobject2.getString("rider_type");


                        myDBHelper.insertuser_id(userID);


                    } else if (Sstatus.equalsIgnoreCase("0")) {
                        Zero_res = object1.getString("ride_view");
                        SResponse = object1.getString("response");
//                        mHandler.removeCallbacks(null);
//                        sm.setRequestCount(0);
//
//                        if (TimerPage.mediaPlayer != null && TimerPage.mediaPlayer.isPlaying()) {
//                            TimerPage.mediaPlayer.stop();
//                        }
//                        context.finish();
                    }

                    if (Sstatus.equalsIgnoreCase("1")) {
                        for (int i = 1; i <= arrobj.size(); i++) {
                            CircularHandler ch2 = arrobj.get(i - 1);
                            if (ch2 != null) {
                                mHandler.removeCallbacks(ch2);
                            }
                        }

                        sm.setRequestCount(0);
                        stopPlayer();

                        myDBHelper.insertDriverStatus("arrived");
                        Intent intent = new Intent(context, TripPageNew.class);
                        intent.putExtra("address", Str_pickuplocation);
                        intent.putExtra("rideId", rider_id);
                        intent.putExtra("pickuplat", Str_pickuplat);
                        intent.putExtra("pickup_long", Str_pickup_long);
                        intent.putExtra("username", Str_Username);
                        intent.putExtra("userrating", Str_Userrating);
                        intent.putExtra("phoneno", Str_Userphoneno);
                        intent.putExtra("userimg", Str_userimg);
                        intent.putExtra("UserId", userID);
                        intent.putExtra("drop_lat", Str_droplat);
                        intent.putExtra("drop_lon", Str_droplon);
                        intent.putExtra("drop_location", str_drop_location);
                        intent.putExtra("eta_arrival", eta_arrival);
                        intent.putExtra("location", location);
                        intent.putExtra("member_since", member_since);
                        intent.putExtra("user_gender", user_gender);
                        intent.putExtra("trip_status", "arrived");
                        intent.putExtra("TripMode", "");
                        intent.putExtra("IsReturnAvailable", "");
                        intent.putExtra("ReturnState", "");
                        intent.putExtra("IsReturnLatLngAvailable", "");
                        intent.putExtra("ReturnLat", "");
                        intent.putExtra("ReturnLng", "");
                        intent.putExtra("ReturnPickUpLat", "");
                        intent.putExtra("ReturnPickUpLng", "");
                        intent.putExtra("payment_type", Str_payment_type);
                        intent.putExtra("cancelled_rides", Str_cancelled_rides);
                        intent.putExtra("completed_rides", Str_completed_rides);
                        intent.putExtra("rider_type", Str_rider_type);
                        intent.putExtra("payment_icon", Str_payment_icon);
                        context.startActivity(intent);
                        context.finish();
                    } else {

                        if (Sstatus.equalsIgnoreCase("0")) {

                            if (Zero_res.equals("stay")) {

                                Alert_ride_already_accepted(context.getString(R.string.timer_label_alert_sorry), SResponse);

                            } else if (Zero_res.equals("next")) {
                                myDBHelper.insertDriverStatus("arrived");
                                for (int i = 1; i <= arrobj.size(); i++) {
                                    CircularHandler ch2 = arrobj.get(i - 1);
                                    if (ch2 != null) {
                                        mHandler.removeCallbacks(ch2);
                                    }
                                }

                                sm.setRequestCount(0);
                                stopPlayer();

                                Intent intent = new Intent(context, TripPageNew.class);
                                intent.putExtra("address", Str_pickuplocation);
                                intent.putExtra("rideId", rider_id);
                                intent.putExtra("pickuplat", Str_pickuplat);
                                intent.putExtra("pickup_long", Str_pickup_long);
                                intent.putExtra("username", Str_Username);
                                intent.putExtra("userrating", Str_Userrating);
                                intent.putExtra("phoneno", Str_Userphoneno);
                                intent.putExtra("userimg", Str_userimg);
                                intent.putExtra("UserId", userID);
                                intent.putExtra("drop_lat", Str_droplat);
                                intent.putExtra("drop_lon", Str_droplon);
                                intent.putExtra("drop_location", str_drop_location);
                                intent.putExtra("eta_arrival", eta_arrival);
                                intent.putExtra("location", location);
                                intent.putExtra("member_since", member_since);
                                intent.putExtra("user_gender", user_gender);
                                intent.putExtra("trip_status", "arrived");
                                intent.putExtra("payment_type", Str_payment_type);
                                intent.putExtra("payment_icon", Str_payment_icon);
                                context.startActivity(intent);
                                context.finish();

                            } else if (Zero_res.equals("detail")) {
                               /* Intent intent = new Intent(context, TripDetailPage.class);
                                intent.putExtra("ride_id", rider_id);
                                context.startActivity(intent);
                                context.finish();*/
                            } else if (Zero_res.equals("home")) {


                                holder1.count = holder1.count - 1;

                                HashMap<String, Integer> user = sm.getRequestCount();
                                int req_count = user.get(SessionManager.KEY_COUNT);
                                req_count = req_count - 1;

                                System.out.println("----------inside declineBtnListener req_count-----jai-----------" + req_count);

                                sm.setRequestCount(req_count);
                                holder1.Rl_ride_Requst_layout.setVisibility(View.GONE);
                                CircularHandler ch1 = arrobj.get(cur_count - 1);
                                if (ch1 != null) {
                                    mHandler.removeCallbacks(ch1);
                                }

                                if (req_count == 0) {
                                    mHandler.removeCallbacks(null);
                                    sm.setRequestCount(0);

                                    AlertHome(context.getString(R.string.timer_label_alert_sorry), SResponse);

                                } else {
                                    Alert(context.getString(R.string.timer_label_alert_sorry), SResponse);
                                }


                            }
                        }
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (Exception e) {
                }

                dismissDialog();
            }

            @Override
            public void onErrorListener() {
                dismissDialog();
                context.finish();
            }
        });
    }


    private void DeclineRequest(String Url, HashMap<String, String> jsonParams, final int mPosition, final View v) {

        accept_dialog = new Dialog(context);
        accept_dialog.getWindow();
        accept_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        accept_dialog.setContentView(R.layout.custom_loading);
        accept_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        accept_dialog.setCanceledOnTouchOutside(false);
        accept_dialog.show();

        TextView dialog_title = (TextView) accept_dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(context.getResources().getString(R.string.action_pleasewait));


        System.out.println("--------------DeclineRequest Url-------------------" + Url);
        System.out.println("--------------Decline Params-------------------" + jsonParams);

        mServiceRequest = new ServiceRequest(context);
        mServiceRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------Decline Response-------------------" + response);
                String Sstatus = "";

                dismissDialog();

                try {
                    JSONObject object1 = new JSONObject(response);
                    Sstatus = object1.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {

                    } else if (Sstatus.equalsIgnoreCase("0")) {

                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (Exception e) {
                }

                ViewHolder holder = (ViewHolder) v.getTag();
                try {

                    if (timerCompleteCallback != null) {
                        holder.count = holder.count - 1;

                        HashMap<String, Integer> user = sm.getRequestCount();
                        int req_count = user.get(SessionManager.KEY_COUNT);
                        req_count = req_count - 1;

                        sm.setRequestCount(req_count);
                        holder.Rl_ride_Requst_layout.setVisibility(View.GONE);
                        CircularHandler ch1 = arrobj.get(mPosition - 1);
                        if (ch1 != null) {
                            try {
                                mHandler.removeCallbacks(ch1);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        if (req_count == 0) {
                            mHandler.removeCallbacks(null);
                            sm.setRequestCount(0);

                            if (TimerPage.mediaPlayer != null && TimerPage.mediaPlayer.isPlaying()) {
                                TimerPage.mediaPlayer.stop();
                            }
                            if (TimerPage.notifiacation != null) {
                                if (TimerPage.notifiacation.equalsIgnoreCase("")) {
//                                    Intent intent = new Intent(context, NavigationPage.class);
//                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                                    context.startActivity(intent);
                                    context.finish();
                                } else {
//                                    Intent intent = new Intent(context, NavigationPage.class);
//                                    intent.putExtra("fromrequestpage","RequestTimerAdapter");
//                                   intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                                    context.startActivity(intent);
                                    context.finish();
                                }
                            } else {
                                if (TimerPage.notifiacation.equalsIgnoreCase("notification")) {
//                                    Intent intent = new Intent(context, NavigationPage.class);
//                                    intent.putExtra("fromrequestpage","RequestTimerAdapter");
//                                   intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                                    context.startActivity(intent);
                                    context.finish();
                                } else {
//                                    Intent intent = new Intent(context, NavigationPage.class);
//                                   intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                                    context.startActivity(intent);
                                    context.finish();
                                }
                            }


                        } else if (req_count == 1) {
                            mHandler.removeCallbacks(null);
                            sm.setRequestCount(0);
                            if (TimerPage.mediaPlayer != null && TimerPage.mediaPlayer.isPlaying()) {
                                TimerPage.mediaPlayer.stop();
                            }

                            if (TimerPage.notifiacation != null) {
                                if (TimerPage.notifiacation.equalsIgnoreCase("")) {
//                                    Intent intent = new Intent(context, NavigationPage.class);
//                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                    context.startActivity(intent);
                                    context.finish();
                                } else {
//                                    Intent intent = new Intent(context, NavigationPage.class);
//                                    intent.putExtra("fromrequestpage","RequestTimerAdapter");
//                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                    context.startActivity(intent);
                                    context.finish();
                                }
                            } else {
                                if (TimerPage.notifiacation.equalsIgnoreCase("notification")) {
//                                    Intent intent = new Intent(context, NavigationPage.class);
//                                    intent.putExtra("fromrequestpage","RequestTimerAdapter");
//                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                    context.startActivity(intent);
                                    context.finish();
                                } else {
//                                    Intent intent = new Intent(context, NavigationPage.class);
//                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                    context.startActivity(intent);
                                    context.finish();
                                }
                            }
                        } else if (req_count == -1) {
                            mHandler.removeCallbacks(null);
                            sm.setRequestCount(0);
                            if (TimerPage.mediaPlayer != null) {
                                TimerPage.mediaPlayer.stop();
                            }

                            if (TimerPage.notifiacation != null) {
                                if (TimerPage.notifiacation.equalsIgnoreCase("")) {
//                                    Intent intent = new Intent(context, NavigationPage.class);
//                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                    context.startActivity(intent);
                                    context.finish();
                                } else {
//                                    Intent intent = new Intent(context, NavigationPage.class);
//                                    intent.putExtra("fromrequestpage","RequestTimerAdapter");
//                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                    context.startActivity(intent);
                                    context.finish();
                                }
                            } else {
                                if (TimerPage.notifiacation.equalsIgnoreCase("notification")) {
//                                    Intent intent = new Intent(context, NavigationPage.class);
//                                    intent.putExtra("fromrequestpage","RequestTimerAdapter");
//                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                    context.startActivity(intent);
                                    context.finish();
                                } else {
//                                    Intent intent = new Intent(context, NavigationPage.class);
//                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                    context.startActivity(intent);
                                    context.finish();
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    dismissDialog();
                }


            }

            @Override
            public void onErrorListener() {
                dismissDialog();
                context.finish();
            }
        });
    }


    public void showDialog() {
        dialog = new Dialog(context);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public void dismissDialog() {
        try {
            if (dialog != null)
                dialog.dismiss();

            if (accept_dialog != null) {
                accept_dialog.dismiss();
            }


        } catch (IllegalArgumentException e) {
        } catch (Exception e) {
        }

    }


    private void stopPlayer() {
        try {
            if (TimerPage.mediaPlayer != null && TimerPage.mediaPlayer.isPlaying()) {
                TimerPage.mediaPlayer.stop();
            }
        } catch (Exception e) {
        }
    }


    //--------------Alert Method-----------
    private void Alert(String title, String message) {
        final PkDialog mDialog = new PkDialog(context);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(message);
        mDialog.setPositiveButton(context.getString(R.string.ok_lable), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void Alert_ride_already_accepted(String title, String message) {
        final PkDialog mDialog = new PkDialog(context);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(message);
        mDialog.setPositiveButton(context.getString(R.string.ok_lable), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                if (mHandler != null) {
                    mHandler.removeCallbacks(null);
                    sm.setRequestCount(0);

                    if (TimerPage.mediaPlayer != null && TimerPage.mediaPlayer.isPlaying()) {
                        TimerPage.mediaPlayer.stop();
                    }
                    context.finish();
                }
            }
        });
        mDialog.show();
    }

    private void AlertHome(String title, String message) {
        final PkDialog mDialog = new PkDialog(context);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(message);
        mDialog.setPositiveButton(context.getString(R.string.ok_lable), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                context.finish();
                if (TimerPage.mediaPlayer != null && TimerPage.mediaPlayer.isPlaying()) {
                    TimerPage.mediaPlayer.stop();
                }
            }
        });
        mDialog.show();
    }


    public class DeclineBtnListener implements View.OnClickListener {

        int mPosition;

        DeclineBtnListener(int position) {
            mPosition = position;
        }

        @Override
        public void onClick(View v) {

            HashMap<String, String> userDetails = sm.getUserDetails();
            String driverId = userDetails.get(SessionManager.KEY_DRIVERID);

            ViewHolder holder = (ViewHolder) v.getTag();
            rider_id = getDataForPosition(holder.count, KEY1, holder.data);

            HashMap<String, String> jsonParams = new HashMap<>();
            jsonParams.put("driver_id", driverId);
            jsonParams.put("ride_id", rider_id);
            try {
                if (TimerPage.myLocation != null) {
                    jsonParams.put("driver_lat", "" + TimerPage.myLocation.getLatitude());
                    jsonParams.put("driver_lon", "" + TimerPage.myLocation.getLongitude());
                } else {
                    jsonParams.put("driver_lat", "" + myLocation.getLatitude());
                    jsonParams.put("driver_lon", "" + myLocation.getLongitude());
                }
            } catch (NullPointerException e) {

            } catch (Exception e) {
            }

            DeclineRequest(Iconstant.driverDeclineRequest_Url, jsonParams, mPosition, v);


        }
    }


}
