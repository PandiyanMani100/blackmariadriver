package com.blackmaria.partner.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;


import com.blackmaria.partner.Interface.ReplyCloseInterface;
import com.blackmaria.partner.Pojo.ViewComplaintPojo;
import com.blackmaria.partner.R;
import com.blackmaria.partner.latestpackageview.widgets.RoundedImageView;
import com.blackmaria.partner.latestpackageview.widgets.CustomTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by user144 on 3/9/2018.
 */

public class ComplaintPageViewDetailAdapter extends BaseAdapter {

    private ArrayList<ViewComplaintPojo> data;
    private LayoutInflater mInflater;
    private Context context;
    ReplyCloseInterface replycloseinterface;
    private String DriverImage = "", UserImage = "";
    String userType = "", ticketStatus = "";

    public ComplaintPageViewDetailAdapter(Context c, ArrayList<ViewComplaintPojo> d, ReplyCloseInterface replycloseinterface, String driverimage, String userImage, String ticketStatus) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
        this.DriverImage = driverimage;
        this.UserImage = userImage;
        this.replycloseinterface = replycloseinterface;
        this.ticketStatus = ticketStatus;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private CustomTextView view_complaint_close_the_case_textview, view_complaint_page_replay_message_send_imageview, Tv_user_commmnet, Tv_user_commmnet_date, view_complaint_comment_list_you_wrote_label;
        private RelativeLayout complaint_submit_lyt;
        private RoundedImageView complaint_page_user_image;
        private LinearLayout compview;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.complaintreply_single, parent, false);
            holder = new ViewHolder();

            holder.Tv_user_commmnet = (CustomTextView) view.findViewById(R.id.view_complaint_comment_list_user_complaint);
            holder.view_complaint_comment_list_you_wrote_label = (CustomTextView) view.findViewById(R.id.view_complaint_comment_list_you_wrote_label);
            holder.Tv_user_commmnet_date = (CustomTextView) view.findViewById(R.id.view_complaint_comment_list_user_complaint_date);
            holder.complaint_page_user_image = (RoundedImageView) view.findViewById(R.id.complaint_page_user_image);
            holder.compview = (LinearLayout) view.findViewById(R.id.compview);

            holder.complaint_submit_lyt = (RelativeLayout) view.findViewById(R.id.complaint_submit_lyt);
            holder.view_complaint_page_replay_message_send_imageview = (CustomTextView) view.findViewById(R.id.view_complaint_page_replay_message_send_imageview);
            holder.view_complaint_close_the_case_textview = (CustomTextView) view.findViewById(R.id.view_complaint_close_the_case_textview);

            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        holder.view_complaint_page_replay_message_send_imageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replycloseinterface.onReplyUser();
            }
        });
        holder.view_complaint_close_the_case_textview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replycloseinterface.ViewDetails();
            }
        });

        if (ticketStatus.equalsIgnoreCase("closed")) {
            holder.view_complaint_page_replay_message_send_imageview.setVisibility(View.GONE);
        } else {
            holder.view_complaint_page_replay_message_send_imageview.setVisibility(View.VISIBLE);
        }
        if ((position % 2) == 0) {
            holder.compview.setBackgroundColor(Color.parseColor("#35FFFFFF"));
        } else {
            holder.compview.setBackgroundColor(Color.parseColor("#50FFFFFF"));
        }
        if (position == data.size() - 1) {
            holder.complaint_submit_lyt.setVisibility(View.VISIBLE);
        } else {
            holder.complaint_submit_lyt.setVisibility(View.GONE);
        }


        holder.Tv_user_commmnet.setText(data.get(position).getComments());
        holder.view_complaint_comment_list_you_wrote_label.setText(data.get(position).getComment_by() + "  Wrote : ");
        holder.Tv_user_commmnet_date.setText(data.get(position).getComment_date());

        userType = data.get(position).getType();
        if (userType == null) {
            userType = "admin";
        }

        if (userType.equalsIgnoreCase("user")) {
            Picasso.with(context).load(UserImage).placeholder(R.drawable.user_newcomp).into(holder.complaint_page_user_image);
        } else if (userType.equalsIgnoreCase("driver")) {
            Picasso.with(context).load(DriverImage).placeholder(R.drawable.user_newcomp).into(holder.complaint_page_user_image);
        }
        return view;
    }




}
