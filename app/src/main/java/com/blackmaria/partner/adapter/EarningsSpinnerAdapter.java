package com.blackmaria.partner.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.blackmaria.partner.R;

/**
 * Created by Ganesh on 16-05-2017.
 */

public class EarningsSpinnerAdapter extends BaseAdapter {
    Context context;
    String[] statementList;
    LayoutInflater inflter;

    public EarningsSpinnerAdapter(Context applicationContext, String[] statementList) {
        this.context = applicationContext;
        this.statementList = statementList;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return statementList.length;
    }

    @Override
    public String getItem(int position) {
        return statementList[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("WrongConstant")
    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.earnings_spinner_single, null);
        TextView names = (TextView) view.findViewById(R.id.textView);
        names.setTextSize(12);
        names.setText(statementList[position]);
        /*if (position == 0) {
            names.setVisibility(View.GONE);
        }*/
        return view;
    }
}
