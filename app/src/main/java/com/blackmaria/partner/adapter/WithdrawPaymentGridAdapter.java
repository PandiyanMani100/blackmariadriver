package com.blackmaria.partner.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.blackmaria.partner.Pojo.WalletMoneyPojo;
import com.blackmaria.partner.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class WithdrawPaymentGridAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<WalletMoneyPojo> listPayment;

    public WithdrawPaymentGridAdapter(Context c, ArrayList<WalletMoneyPojo> listPayment) {
        mContext = c;
        this.listPayment = listPayment;
    }

    @Override
    public int getCount() {
        return listPayment.size();
    }

    @Override
    public WalletMoneyPojo getItem(int position) {
        return listPayment.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("WrongConstant")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = convertView;
        ViewHolder holder;

        if (convertView == null) {

            view = inflater.inflate(R.layout.row_single_payment_option, null);

            holder = new ViewHolder();
            holder.V_view = (View) view.findViewById(R.id.view_dummy);
            holder.Iv_paymentOption = (ImageView) view.findViewById(R.id.img_payment_option);


            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        String imageUrl = "";

        if (listPayment.get(position).getPayment_code().equalsIgnoreCase(listPayment.get(position).getPayment_selected_payment_id())) {
            imageUrl = listPayment.get(position).getPayment_active_img();
        } else {
            imageUrl = listPayment.get(position).getPayment_normal_img();
        }

        if (listPayment.size() % 2 != 0) {

            if (position != 0) {

                if (listPayment.size() != 3) {
                    if (position == listPayment.size() - 2 || position == listPayment.size() - 1) {
                        holder.V_view.setVisibility(View.INVISIBLE);
                    }
                } else {
                    if (position == listPayment.size() - 1) {
                        holder.V_view.setVisibility(View.INVISIBLE);
                    }
                }

            }

        } else {
            if (listPayment.size() != 2) {
                if (position == listPayment.size() - 1) {
                    holder.V_view.setVisibility(View.INVISIBLE);
                }
            } else {
                holder.V_view.setVisibility(View.VISIBLE);
            }
        }

        if (imageUrl.length() > 0) {
            Picasso.with(mContext).load(imageUrl).memoryPolicy(MemoryPolicy.NO_CACHE).into(holder.Iv_paymentOption);
        }

        return view;
    }

    private class ViewHolder {

        View V_view;
        ImageView Iv_paymentOption;

    }

}