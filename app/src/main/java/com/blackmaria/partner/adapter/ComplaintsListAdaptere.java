package com.blackmaria.partner.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blackmaria.partner.Interface.GoDetailComplaint;
import com.blackmaria.partner.Pojo.ComplaintsListPojo;
import com.blackmaria.partner.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by user144 on 3/8/2018.
 */

public class ComplaintsListAdaptere extends BaseAdapter {

    private ArrayList<ComplaintsListPojo> data;
    private LayoutInflater mInflater;
    private Context context;
    private String typeOfComplaint = "";
    GoDetailComplaint godetailcomplaint;
    public ComplaintsListAdaptere(Context c, ArrayList<ComplaintsListPojo> d, String typeOfCase, GoDetailComplaint godetailcomplaint) {
        context = c;
        mInflater = LayoutInflater.from(context);
        this.typeOfComplaint = typeOfCase;
        data = d;
       this.godetailcomplaint=godetailcomplaint;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private TextView subjectName, ticketNumber, dateAndStatus;
        private ImageView rewardImage, rightarrow;
        private LinearLayout cmplintlyt;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.complaintlistsingle, parent, false);

            holder = new ViewHolder();
            holder.subjectName = (TextView) view.findViewById(R.id.subname);
            holder.ticketNumber = (TextView) view.findViewById(R.id.ticketnumber);
            holder.dateAndStatus = (TextView) view.findViewById(R.id.view_complaint_close_the_case_textview);
            holder.cmplintlyt = (LinearLayout) view.findViewById(R.id. cmplintlyt);
            holder.rewardImage = (ImageView) view.findViewById(R.id.rewardimages);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        if ((position % 2) == 0) {
            holder.cmplintlyt.setBackgroundColor(context.getResources().getColor(R.color.transparant_color));
        } else {
            holder.cmplintlyt.setBackgroundColor(Color.parseColor("#40ffffff"));
        }
            holder.ticketNumber.setText("ticket no " + data.get(position).getTickNumer());
            holder.subjectName.setText(data.get(position).getSubName());


        holder.dateAndStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                godetailcomplaint.goComplaintDetail(position);
            }
        });

        return view;
    }


}