package com.blackmaria.partner.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.blackmaria.partner.Pojo.FareDetailPojo;
import com.blackmaria.partner.R;

import java.util.ArrayList;

/**
 * Created by Jayachandran on 12/22/2016.
 */

public class FareDetailsAdapter extends BaseAdapter {

    private ArrayList<FareDetailPojo> data;
    private LayoutInflater mInflater;
    private Context context;
    private Typeface face;
    public FareDetailsAdapter(Context c, ArrayList<FareDetailPojo> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
        face = Typeface.createFromAsset(context.getAssets(), "fonts/RobotoCondensed-Light.ttf");
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {

        private TextView title, value;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        FareDetailsAdapter.ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.faredetails_single, parent, false);
            holder = new FareDetailsAdapter.ViewHolder();

            holder.title = (TextView) view.findViewById(R.id.receipt_label);
            holder.value = (TextView) view.findViewById(R.id.date_label);

            view.setTag(holder);
        } else {
            view = convertView;
            holder = (FareDetailsAdapter.ViewHolder) view.getTag();
        }
/*
        holder.title.setText(data.get(position).getTitle());
        holder.value.setText(data.get(position).getValue());

        if (position == data.size() - 1) {

            if (position != 0) {
                holder.title.setTextColor(Color.BLACK);
                holder.value.setTextColor(Color.BLACK);

                holder.title.setTypeface(null, Typeface.BOLD);
                holder.value.setTypeface(null, Typeface.BOLD);
            } else {
                holder.title.setTextColor(Color.parseColor("#878787"));
                holder.value.setTextColor(Color.parseColor("#878787"));

                holder.title.setTypeface(null, Typeface.NORMAL);
                holder.value.setTypeface(null, Typeface.NORMAL);
            }
        } else {
            holder.title.setTextColor(Color.parseColor("#878787"));
            holder.value.setTextColor(Color.parseColor("#878787"));

            holder.title.setTypeface(null, Typeface.NORMAL);
            holder.value.setTypeface(null, Typeface.NORMAL);
        }

*/





        if (position == data.size() - 1) {

            if (position != 0) {
                holder.title.setSingleLine(false);
                holder.title.setTextColor(Color.BLACK);
                holder.value.setTextColor(Color.BLACK);

                holder.title.setTypeface(face, Typeface.BOLD);
                holder.value.setTypeface(face, Typeface.BOLD);
            } else {
                holder.title.setSingleLine(true);
                holder.title.setTextColor(Color.parseColor("#333333"));
                holder.value.setTextColor(Color.parseColor("#333333"));

                holder.title.setTypeface(face, Typeface.NORMAL);
                holder.value.setTypeface(face, Typeface.NORMAL);
            }
        } else {
            holder.title.setTextColor(Color.parseColor("#333333"));
            holder.value.setTextColor(Color.parseColor("#333333"));
            holder.title.setSingleLine(true);
            holder.title.setTypeface(face, Typeface.NORMAL);
            holder.value.setTypeface(face, Typeface.NORMAL);
        }
        if (position == data.size() - 1) {
            holder.title.setText(data.get(position).getTitle().toUpperCase());
            holder.value.setText(data.get(position).getValue().toUpperCase());
        } else {
            holder.title.setText(data.get(position).getTitle().toUpperCase() + ".....................................................");
            holder.value.setText(data.get(position).getValue().toUpperCase());
        }




        return view;
    }
}