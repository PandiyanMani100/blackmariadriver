package com.blackmaria.partner.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.blackmaria.partner.Pojo.RatingPojo;
import com.blackmaria.partner.R;

import java.util.ArrayList;


/**
 * Created by GANESH on 23/08/2017.
 */
public class RatingAdapterNew_recycle extends RecyclerView.Adapter<RatingAdapterNew_recycle.MyViewHolder> {

    private ArrayList<RatingPojo> data;
    private LayoutInflater mInflater;
    private Context context;


    public RatingAdapterNew_recycle(Context c, ArrayList<RatingPojo> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.row_single_rating, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.title.setText(data.get(position).getRatingName());

        holder.rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
//                ratingpageOnclick.getonclick(data,position,rating);
                data.get(position).setRatingcount(String.valueOf(rating));
//                notifyItemChanged(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }



    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private RatingBar rating;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.txt_title);
            rating = (RatingBar) view.findViewById(R.id.rb_rating);
        }

    }


}


