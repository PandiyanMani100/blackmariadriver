package com.blackmaria.partner.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.blackmaria.partner.Pojo.WalletMoneyPojo;
import com.blackmaria.partner.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by user127 on 19-09-2017.
 */

public class CloudMoneyWithdrawalRecyclerAdapter extends RecyclerView.Adapter<CloudMoneyWithdrawalRecyclerAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<WalletMoneyPojo> data;
    public RecyclerItemClick itemClickInterface;

    public CloudMoneyWithdrawalRecyclerAdapter(Context context, ArrayList<WalletMoneyPojo> data, RecyclerItemClick itemClickInterface) {
        this.context = context;
        this.data = data;
        this.itemClickInterface = itemClickInterface;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.wallet_money_single_page, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        String imageUrl = data.get(position).getPayment_normal_img();

        if (imageUrl.length() > 0) {
            Picasso.with(context).load(imageUrl).memoryPolicy(MemoryPolicy.NO_CACHE).into(holder.Iv_paymentOption);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView Iv_paymentOption;

        public MyViewHolder(View view) {
            super(view);
            Iv_paymentOption = (ImageView) view.findViewById(R.id.wallet_page_single_payment_list_imageview);
            Iv_paymentOption.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            int position = getLayoutPosition();

            if (view == Iv_paymentOption) {
                itemClickInterface.onItemClick(position);
            }

        }
    }

    public interface RecyclerItemClick {

        public void onItemClick(int position);

    }

}
