package com.blackmaria.partner.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blackmaria.partner.R;

import java.util.ArrayList;

/**
 * Created by GANESH on 11-10-2017.
 */

public class MyCustomSpinnerAdapter extends BaseAdapter {
    Context context;
    ArrayList<String> statementList;
    LayoutInflater inflter;

    public MyCustomSpinnerAdapter(Context applicationContext, ArrayList<String> statementList) {
        this.context = applicationContext;
        this.statementList = statementList;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return statementList.size();
    }

    @Override
    public String getItem(int position) {
        return statementList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("WrongConstant")
    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.row_custom_spinner_item_register, null);
        TextView names = (TextView) view.findViewById(R.id.textView);
//        names.setTextSize(12);
        names.setText(statementList.get(position));
        /*if (position == 0) {
            names.setVisibility(View.GONE);
        }*/
        return view;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view = super.getDropDownView(position, convertView, parent);

        LinearLayout LL_main = (LinearLayout) view.findViewById(R.id.LL_main);
        LL_main.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));

        return view;
    }
}

