package com.blackmaria.partner.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blackmaria.partner.Pojo.MessagePojo;
import com.blackmaria.partner.R;

import java.util.ArrayList;

/**
 * Created by GANESH on 17-08-2017.
 */

public class InfoAdapter extends BaseAdapter {

    Context context;
    ArrayList<MessagePojo> listData;
    private LayoutInflater layoutInflater;

    public InfoAdapter(Context context, ArrayList<MessagePojo> listData) {
        this.context = context;
        this.listData = listData;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.row_single_information, null);
            holder = new ViewHolder();
            holder.Tv_date = (TextView) convertView.findViewById(R.id.txt_date);
            holder.Tv_title = (TextView) convertView.findViewById(R.id.txt_title);
            holder.LL_textHolder = (LinearLayout) convertView.findViewById(R.id.LL_text_holder);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.Tv_date.setText(listData.get(position).getDate());
        holder.Tv_title.setText(listData.get(position).getTitle());

        /*if (position % 2 == 0) {
            holder.LL_textHolder.setBackgroundColor(Color.parseColor("#335175"));
            holder.Tv_title.setTextColor(Color.WHITE);
        } else {
            holder.LL_textHolder.setBackgroundColor(Color.parseColor("#5C7491"));
            holder.Tv_title.setTextColor(Color.BLACK);
        }*/

        return convertView;

    }

    private class ViewHolder {

        private TextView Tv_date, Tv_title;
        private LinearLayout LL_textHolder;

    }

}

