package com.blackmaria.partner.adapter;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;


import com.blackmaria.partner.R;

import java.util.ArrayList;

/**
 * Created by user144 on 6/30/2017.
 */

public class Company_of_registrationadapter extends BaseAdapter implements SpinnerAdapter {

    private final Context activity;
    private ArrayList<String> regTypeList;

    public Company_of_registrationadapter(Context context, ArrayList<String> regTypeList) {
        this.regTypeList = regTypeList;
        activity = context;
    }


    public int getCount() {
        return regTypeList.size();
    }

    public Object getItem(int i) {
        return regTypeList.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }


    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView txt = new TextView(activity);
        txt.setPadding(16, 16, 16, 16);
        txt.setTextSize(16);
        txt.setGravity(Gravity.CENTER);
        txt.setText(regTypeList.get(position));
        txt.setBackground(activity.getResources().getDrawable(R.drawable.ratecard_dropdownbgbg));
        txt.setTextColor(activity.getResources().getColor(R.color.black_color));
        return txt;
    }

    public View getView(int i, View view, ViewGroup viewgroup) {
        TextView txt = new TextView(activity);
        txt.setGravity(Gravity.CENTER);
        txt.setPadding(10, 0, 0, 0);
        txt.setTextSize(14);
        txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        txt.setText(regTypeList.get(i));
        txt.setTextColor(activity.getResources().getColor(R.color.black_color));
        txt.setVisibility(View.INVISIBLE);
        return txt;
    }

}




