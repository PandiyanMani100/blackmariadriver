package com.blackmaria.partner.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackmaria.partner.Pojo.DummyPojo;
import com.blackmaria.partner.R;

import java.util.ArrayList;

/**
 * Created by user127 on 02-06-2017.
 */

public class DummyAdapter extends BaseAdapter {

    Context context;
    ArrayList<DummyPojo> listData;
    private LayoutInflater layoutInflater;

    public DummyAdapter(Context context, ArrayList<DummyPojo> listData) {
        this.context = context;
        this.listData = listData;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        /*if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.dummy_list_item, null);
            holder = new ViewHolder();
            holder.Rv_profile = (ImageView) convertView.findViewById(R.id.rv_profile);
            holder.Iv_withdraw = (ImageView) convertView.findViewById(R.id.iv_withdraw);
            holder.Tv_date = (TextView) convertView.findViewById(R.id.txt_date);
            holder.Tv_amount = (TextView) convertView.findViewById(R.id.txt_amount);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (listData.get(position).getType().equalsIgnoreCase("withdraw")) {
            holder.Iv_withdraw.setVisibility(View.VISIBLE);
            holder.Rv_profile.setVisibility(View.GONE);
            holder.Tv_date.setBackgroundColor(Color.parseColor("#6B748E"));
            holder.Tv_amount.setBackgroundColor(Color.parseColor("#454E6F"));
        } else {
            holder.Iv_withdraw.setVisibility(View.GONE);
            holder.Rv_profile.setVisibility(View.VISIBLE);
            holder.Tv_date.setBackgroundColor(Color.parseColor("#12449A"));
            holder.Tv_amount.setBackgroundColor(Color.parseColor("#16347D"));
        }

        holder.Tv_date.setText(listData.get(position).getDate());
        holder.Tv_amount.setText(listData.get(position).getAmount());*/

        return convertView;

    }

    private class ViewHolder {

        private ImageView Rv_profile, Iv_withdraw;
        private TextView Tv_date, Tv_amount;

    }

}
