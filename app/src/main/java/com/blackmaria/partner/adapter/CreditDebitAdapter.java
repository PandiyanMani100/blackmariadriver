package com.blackmaria.partner.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blackmaria.partner.Pojo.WalletMoneyTransactionPojo;
import com.blackmaria.partner.R;

import java.util.ArrayList;

/**
 * Created by user127 on 18-07-2017.
 */

public class CreditDebitAdapter extends BaseAdapter {

    private ArrayList<WalletMoneyTransactionPojo> data;
    private LayoutInflater mInflater;
    private Context context;

    public CreditDebitAdapter(Context c, ArrayList<WalletMoneyTransactionPojo> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private LinearLayout LL_main;
        private TextView Tv_date, Tv_time, Tv_amount, Tv_details;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.row_debit_credit_statements, parent, false);
            holder = new ViewHolder();
            holder.LL_main = (LinearLayout) view.findViewById(R.id.LL_main);
            holder.Tv_date = (TextView) view.findViewById(R.id.txt_date);
            holder.Tv_time = (TextView) view.findViewById(R.id.txt_time);
            holder.Tv_amount = (TextView) view.findViewById(R.id.txt_amount);
            holder.Tv_details = (TextView) view.findViewById(R.id.txt_details);

            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        if (position % 2 == 0) {
            holder.LL_main.setBackgroundColor(Color.parseColor("#20ffffff"));
        } else {
            holder.LL_main.setBackgroundColor(context.getResources().getColor(R.color.transparant_color));
        }

        holder.Tv_date.setText(data.get(position).getTrans_date());
        holder.Tv_time.setText(data.get(position).getTrans_time());
        holder.Tv_amount.setText(data.get(position).getTrans_amount());
        holder.Tv_details.setText(data.get(position).getTitle());

        return view;
    }
}