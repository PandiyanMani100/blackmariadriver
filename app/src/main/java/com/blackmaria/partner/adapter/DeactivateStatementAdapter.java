package com.blackmaria.partner.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.blackmaria.partner.Pojo.KeyValueBean;
import com.blackmaria.partner.R;

import java.util.ArrayList;

/**
 * Created by user127 on 29-08-2017.
 */


public class DeactivateStatementAdapter extends BaseAdapter {

    Context context;
    ArrayList<KeyValueBean> listData;
    private LayoutInflater layoutInflater;

    public DeactivateStatementAdapter(Context context, ArrayList<KeyValueBean> listData) {
        this.context = context;
        this.listData = listData;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.row_single_last_statement, null);
            holder = new ViewHolder();
            holder.Tv_title = (TextView) convertView.findViewById(R.id.txt_title);
            holder.Tv_value = (TextView) convertView.findViewById(R.id.txt_value);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.Tv_title.setText(listData.get(position).getKey().trim());
        holder.Tv_value.setText(listData.get(position).getValue().trim());
        return convertView;

    }

    private class ViewHolder {

        private TextView Tv_title, Tv_value;

    }


}

