package com.blackmaria.partner.adapter;

import android.app.Activity;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.blackmaria.partner.Pojo.WalletMoneyPojo;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ImageLoader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by user129 on 7/14/2017.
 */
public class EwalletWithdrawalAdapter extends BaseAdapter {

    private static ArrayList<WalletMoneyPojo> data;
    private LayoutInflater mInflater;
    private Activity context;
    private ImageLoader imageLoader;


    public EwalletWithdrawalAdapter(Activity c, ArrayList<WalletMoneyPojo> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
        imageLoader = new ImageLoader(context);
    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private ImageView Iv_e_wallet_payment_type;
        private RelativeLayout Ll_car;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view;
        final ViewHolder holder;

        if (convertView == null) {
            view = mInflater.inflate(R.layout.ewallet_singal_list_item_singal_new, parent, false);
            holder = new ViewHolder();

            holder.Iv_e_wallet_payment_type = (ImageView) view.findViewById(R.id.e_wallet_page_single_payment_list_imageview);
            holder.Ll_car = (RelativeLayout) view.findViewById(R.id.bookmyride_single_car_layout);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        if ("true".equalsIgnoreCase(data.get(position).getPayment_selected_payment_id())) {
            if(!data.get(position).getPayment_active_img().isEmpty() || !data.get(position).getPayment_active_img().equalsIgnoreCase("")){
                try {
                    Picasso.with(context).load(data.get(position).getPayment_active_img()).into(holder.Iv_e_wallet_payment_type);
                }catch (IllegalArgumentException e){}
            }else {

            }

        } else {
            if(!data.get(position).getPayment_normal_img().isEmpty() || !data.get(position).getPayment_normal_img().equalsIgnoreCase("")) {
                try {
                    Picasso.with(context).load(data.get(position).getPayment_normal_img()).into(holder.Iv_e_wallet_payment_type);
                }catch (IllegalArgumentException e){}
            }
        }



        return view;


    }
}
