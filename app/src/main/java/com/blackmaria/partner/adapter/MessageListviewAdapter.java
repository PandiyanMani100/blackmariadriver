package com.blackmaria.partner.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blackmaria.partner.Pojo.MessagePojo;
import com.blackmaria.partner.R;

import java.util.ArrayList;


/**
 * Created by CAS64 on 4/21/2017.
 */

public class MessageListviewAdapter extends BaseAdapter {
    Context context;
    ArrayList<MessagePojo> items;
    LayoutInflater inflater;
    View itemView;
    MessageClickInterface clickInterface;

    @SuppressLint("WrongConstant")
    public MessageListviewAdapter(Context context, ArrayList<MessagePojo> items, MessageClickInterface clickInterface) {
        this.context = context;
        this.items = items;
        this.clickInterface = clickInterface;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @SuppressLint("WrongConstant")
    @Override
    public View getView(final int i, View convertView, ViewGroup viewGroup) {

        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.message_listview_adaper, viewGroup, false);
            holder = new ViewHolder();
            holder.Tv_title = (TextView) convertView.findViewById(R.id.message_title_textview);
            holder.messageDate = (TextView) convertView.findViewById(R.id.message_date);
            holder.LL_title = (RelativeLayout) convertView.findViewById(R.id.message_listview_title_layout);
            holder.LL_delete = (ImageView) convertView.findViewById(R.id.LL_delete);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        /*if (i % 2 != 0) {
            holder.LL_title.setBackgroundResource(R.color.app_bg_dark_blue);
        }*/
        holder.Tv_title.setText(items.get(i).getTitle());
        holder.messageDate.setText(items.get(i).getDate());
        holder.LL_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickInterface.onTitleClick(i);
            }
        });

        holder.LL_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickInterface.onDeleteClick(i);
            }
        });

        return convertView;
    }

    public class ViewHolder {

        TextView Tv_title, messageDate;
        RelativeLayout LL_title;
        ImageView LL_delete;

    }

    public interface MessageClickInterface {

        void onTitleClick(int position);

        void onDeleteClick(int position);

    }

}
