package com.blackmaria.partner.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.blackmaria.partner.Pojo.CancelTripPojo;
import com.blackmaria.partner.R;
import com.blackmaria.partner.latestpackageview.Model.tripcancelreasons;

import java.util.ArrayList;

/**
 * Created by Prem Kumar and Anitha on 11/2/2015.
 */
public class MyRideCancelTripAdapter extends BaseAdapter {

    private tripcancelreasons data;
    private LayoutInflater mInflater;
    private Context context;

    public MyRideCancelTripAdapter(Context c,tripcancelreasons d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
    }

    @Override
    public int getCount() {
        return data.getResponse().getReason().size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private TextView reason;
        private ImageView cancel_select_image;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.myride_cancel_trip_single, parent, false);
            holder = new ViewHolder();
            holder.reason = (TextView) view.findViewById(R.id.myride_cancel_trip_reason_textview);
            holder.cancel_select_image = (ImageView) view.findViewById(R.id.cancel_select_image);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }
        if ("true".equalsIgnoreCase(data.getResponse().getReason().get(position).getStatus())) {
            holder.cancel_select_image.setVisibility(View.VISIBLE);

        } else {
            holder.cancel_select_image.setVisibility(View.GONE);

        }
        holder.reason.setText(data.getResponse().getReason().get(position).getReason());
        return view;
    }
}

