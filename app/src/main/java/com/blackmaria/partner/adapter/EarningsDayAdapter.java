package com.blackmaria.partner.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.blackmaria.partner.Pojo.TodayDetails;
import com.blackmaria.partner.R;

import java.util.ArrayList;

/**
 * Created by GANESH on 13/07/2017.
 */

public class EarningsDayAdapter extends BaseAdapter {

    private ArrayList<TodayDetails> data;
    private LayoutInflater mInflater;
    private Context context;

    public EarningsDayAdapter(Context c, ArrayList<TodayDetails> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private TextView Tv_time, Tv_orderNo, Tv_miles, Tv_fare, Tv_remark;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.row_earnings_day_single, parent, false);
            holder = new ViewHolder();
            holder.Tv_time = (TextView) view.findViewById(R.id.txt_time);
            holder.Tv_orderNo = (TextView) view.findViewById(R.id.txt_order_no);
            holder.Tv_miles = (TextView) view.findViewById(R.id.txt_miles);
            holder.Tv_fare = (TextView) view.findViewById(R.id.txt_fare);
            holder.Tv_remark = (TextView) view.findViewById(R.id.txt_remark);

            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        holder.Tv_time.setText(data.get(position).getRide_time());
        holder.Tv_orderNo.setText(data.get(position).getRide_id());
        holder.Tv_miles.setText(data.get(position).getDistance());
        holder.Tv_fare.setText(data.get(position).getCurrency() + " " + data.get(position).getDriver_earning());
        holder.Tv_remark.setText(data.get(position).getRide_type());

        return view;
    }
}

