package com.blackmaria.partner.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blackmaria.partner.Pojo.TodayDetails;
import com.blackmaria.partner.R;
import com.blackmaria.partner.app.TripSummary;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class TodayDetailListAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<TodayDetails> listTodayTripDetails;
    private ButtonClickInterface clickInterface;

    public TodayDetailListAdapter(Context c, ArrayList<TodayDetails> listTodayTripDetails, ButtonClickInterface clickInterface) {
        mContext = c;
        this.listTodayTripDetails = listTodayTripDetails;
        this.clickInterface = clickInterface;
    }

    @Override
    public int getCount() {
        return listTodayTripDetails.size();
    }

    @Override
    public TodayDetails getItem(int position) {
        return listTodayTripDetails.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("WrongConstant")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = convertView;
        ViewHolder holder;

        if (convertView == null) {

            view = inflater.inflate(R.layout.row_today_detail_list_item, null);

            holder = new ViewHolder();
            holder.Iv_profile = (ImageView) view.findViewById(R.id.iv_profile);
            holder.RL_main = (RelativeLayout) view.findViewById(R.id.Rl_main);
            holder.RL_details = (RelativeLayout) view.findViewById(R.id.Rl_details);
            holder.RL_trackNow = (RelativeLayout) view.findViewById(R.id.Rl_track_now);
            holder.Tv_name = (TextView) view.findViewById(R.id.txt_name);
            holder.Tv_time = (TextView) view.findViewById(R.id.txt_time);
            holder.Tv_mile = (TextView) view.findViewById(R.id.txt_mile);
            holder.Tv_rideId = (TextView) view.findViewById(R.id.txt_ride_id);
            holder.Tv_orderNo = (TextView) view.findViewById(R.id.txt_order_no);
            holder.Tv_status = (TextView) view.findViewById(R.id.txt_status);
            holder.Btn_trackNow = (Button) view.findViewById(R.id.btn_track_now);
            holder.Iv_status = (ImageView) view.findViewById(R.id.img_status);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        String rideStatus = listTodayTripDetails.get(position).getRide_status();
        if (rideStatus.equalsIgnoreCase("Completed") || rideStatus.equalsIgnoreCase("Cancelled")) {
            holder.RL_details.setVisibility(View.VISIBLE);
            holder.RL_trackNow.setVisibility(View.GONE);
            if (rideStatus.equalsIgnoreCase("Cancelled")) {
                holder.Iv_status.setImageResource(R.drawable.icn_del);
            } else {
                holder.Iv_status.setImageResource(R.drawable.icn_tick);
            }
            holder.RL_main.setBackgroundResource(R.drawable.curved_gradient_blue);
        } else {
            holder.RL_details.setVisibility(View.GONE);
            holder.RL_trackNow.setVisibility(View.VISIBLE);
            holder.Iv_status.setImageResource(R.drawable.icn_i);
            holder.RL_main.setBackgroundResource(R.drawable.curved_gradient_yellow);
        }

        String imageUrl = listTodayTripDetails.get(position).getUser_image();
        if (imageUrl.length() > 0) {
            Picasso.with(mContext).load(imageUrl).placeholder(R.drawable.no_user_profile).error(R.drawable.no_user_profile).memoryPolicy(MemoryPolicy.NO_CACHE).into(holder.Iv_profile);
        }

        holder.Tv_name.setText(listTodayTripDetails.get(position).getUser_name());
        holder.Tv_time.setText("TIME : " + listTodayTripDetails.get(position).getRide_time());
        holder.Tv_mile.setText("MILE : " + listTodayTripDetails.get(position).getDistance());
        holder.Tv_rideId.setText("CRN NO : " + listTodayTripDetails.get(position).getRide_id());
        holder.Tv_orderNo.setText("ORDER NUMBER : " + listTodayTripDetails.get(position).getRide_id());
        holder.Tv_status.setText("STATUS : " + listTodayTripDetails.get(position).getDisplay_status());

        holder.Btn_trackNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickInterface.onButtonClick(position);
            }
        });

        return view;
    }

    private class ViewHolder {

        ImageView Iv_profile, Iv_status;
        RelativeLayout RL_main, RL_details, RL_trackNow;
        TextView Tv_name, Tv_time, Tv_mile, Tv_rideId, Tv_orderNo, Tv_status;
        Button Btn_trackNow;

    }

    public interface ButtonClickInterface {
        void onButtonClick(int position);
    }
}