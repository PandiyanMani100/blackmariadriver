package com.blackmaria.partner.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackmaria.partner.Pojo.TodayDetails;
import com.blackmaria.partner.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class TodayDetailGridAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<TodayDetails> listTodayTripDetails;

    public TodayDetailGridAdapter(Context c, ArrayList<TodayDetails> listTodayTripDetails) {
        mContext = c;
        this.listTodayTripDetails = listTodayTripDetails;
    }

    @Override
    public int getCount() {
        return listTodayTripDetails.size();
    }

    @Override
    public TodayDetails getItem(int position) {
        return listTodayTripDetails.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("WrongConstant")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = convertView;
        ViewHolder holder;

        if (convertView == null) {

            view = inflater.inflate(R.layout.row_grid_item_today_detail, null);

            holder = new ViewHolder();
            holder.Tv_SerialNo = (TextView) view.findViewById(R.id.txt_user_count);
            holder.Tv_time = (TextView) view.findViewById(R.id.txt_user_content);
            holder.Iv_profile = (ImageView) view.findViewById(R.id.img_user);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.Tv_SerialNo.setText(listTodayTripDetails.get(position).getS_no());
        holder.Tv_time.setText(listTodayTripDetails.get(position).getDatetime());

        String imageUrl = listTodayTripDetails.get(position).getUser_image();
        if (imageUrl.length() > 0) {
            Picasso.with(mContext).load(imageUrl).placeholder(R.drawable.profile_image_dummy).error(R.drawable.profile_image_dummy).memoryPolicy(MemoryPolicy.NO_CACHE).into(holder.Iv_profile);
        }

        return view;
    }

    private class ViewHolder {

        TextView Tv_SerialNo, Tv_time;
        ImageView Iv_profile;

    }

}