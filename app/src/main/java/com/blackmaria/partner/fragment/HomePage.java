package com.blackmaria.partner.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.FragmentHockeyApp;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.GPSTracker;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.app.ComplaintPage;
import com.blackmaria.partner.app.DriverEarnings;
import com.blackmaria.partner.app.IncentivesDashboard;
import com.blackmaria.partner.app.MyTrip;
import com.blackmaria.partner.app.NavigationPage;
import com.blackmaria.partner.app.SosPage;
import com.blackmaria.partner.app.WalletMoneyPage1;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.view.Dashboard.OnlinepageConstrain;
import com.blackmaria.partner.latestpackageview.view.Dashboard.SOS.Sosdragmap_screen;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.blackmaria.partner.services.Getlocation;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by Prem Kumar and Anitha on 12/16/2016.
 */
public class HomePage extends FragmentHockeyApp implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private TextView Tv_driver_name;
    private LinearLayout LL_verifyAccount;
    private TextView Tv_myTrip, Tv_earnings, Tv_incentive, Tv_driver_unverified;
    private RelativeLayout RL_SOS, RL_Complaint, RL_Support;
    private TextView Tv_Complaint_Notification;
    private LinearLayout LL_driveNow;
    private RelativeLayout RL_way;
    private ImageView Iv_vehicle;

    private String sNormalFareStatus = "", sBidFareStatus = "", sSosStatus = "", sNearbyDriversCount = "", sSearchRadius = "", sVerifyStatus = "", sTrips = "",
            sIncentives = "", sEarnings = "", sCurrencyCode = "", sCurrencySymbol = "", sBannerImage = "",
            sGoOnlineStatus = "", sGoOnlineString = "", sContactEmail = "", sDriverAvailability = "", sVehicleType = "", sNeedReferralCode = "";
    final int PERMISSION_REQUEST_CODE = 111;
    private ServiceRequest mRequest;
    Dialog dialog;

    private View rootView;
    private ConnectionDetector cd;
    private boolean isInternetPresent = false;
    private SessionManager sessionManager;
    private String sDriveID = "", sDriverName = "", sCityName = "";
    private TextView Tv_cars_nearby;
    private ImageView Iv_banner;

    private GPSTracker gpsTracker;
    PendingResult<LocationSettingsResult> result;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    final static int REQUEST_LOCATION = 199;
    private ImageView Iv_status;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.homepage2, container, false);
        // location
        getActivity().startService(new Intent(getActivity(), Getlocation.class));
        initialize(rootView);

        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().stopService(new Intent(getActivity(), Getlocation.class));

    }

    private void initialize(View rootView) {
        sessionManager = new SessionManager(getActivity());
        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();
        gpsTracker = new GPSTracker(getActivity());

        NavigationPage.inHomePage = true;
        sVehicleType = sessionManager.getVehicleType();

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        mGoogleApiClient.connect();

        Iv_banner = (ImageView) rootView.findViewById(R.id.img_meter);
        Tv_driver_name = (TextView) rootView.findViewById(R.id.txt_driver_name);
        Tv_myTrip = (TextView) rootView.findViewById(R.id.txt_my_trip);
        Tv_earnings = (TextView) rootView.findViewById(R.id.txt_earning);
        Tv_incentive = (TextView) rootView.findViewById(R.id.txt_incentives);
        Tv_driver_unverified = (TextView) rootView.findViewById(R.id.txt_label_verify_account);
        LL_verifyAccount = (LinearLayout) rootView.findViewById(R.id.LL_verify_account);
        RL_SOS = (RelativeLayout) rootView.findViewById(R.id.Rl_sos);
        RL_Complaint = (RelativeLayout) rootView.findViewById(R.id.Rl_complaint);
        RL_Support = (RelativeLayout) rootView.findViewById(R.id.Rl_support);
        LL_driveNow = (LinearLayout) rootView.findViewById(R.id.Ll_driver_now);
        Tv_Complaint_Notification = (TextView) rootView.findViewById(R.id.txt_complaint_notification);
        RL_way = (RelativeLayout) rootView.findViewById(R.id.Rl_way);
        Iv_vehicle = (ImageView) rootView.findViewById(R.id.img_car_type);
        Iv_status = (ImageView) rootView.findViewById(R.id.img_status);
        Tv_cars_nearby = (TextView) rootView.findViewById(R.id.txt_cars_count);

        Tv_myTrip.setOnClickListener(this);
        Tv_earnings.setOnClickListener(this);
        Tv_incentive.setOnClickListener(this);
        RL_SOS.setOnClickListener(this);
        RL_Complaint.setOnClickListener(this);
        RL_Support.setOnClickListener(this);
        LL_driveNow.setOnClickListener(this);

        startBlinkingAnimation();
        /*Making the service state empty when app is crashed*/
        sessionManager.setXmppServiceState("");

        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriveID = user.get(SessionManager.KEY_DRIVERID);
        sDriverName = user.get(SessionManager.KEY_DRIVER_NAME);
        sContactEmail = user.get(SessionManager.KEY_EMAIL);

        Tv_driver_name.setText(sDriverName);

//        registerBroadcastReceiver();

        if (sVehicleType.equalsIgnoreCase("motorbike")) {
            Iv_vehicle.setImageResource(R.drawable.bike_home_stroke);
        } else if (sVehicleType.equalsIgnoreCase("taxi")) {
            Iv_vehicle.setImageResource(R.drawable.taxi_cab_new);
        } else {
            Iv_vehicle.setImageResource(R.drawable.cab_new);
        }


    }

    @Override
    public void onResume() {
        super.onResume();

        if (cd != null) {
            isInternetPresent = cd.isConnectingToInternet();
        }
        if (isInternetPresent) {
            if (gpsTracker != null && gpsTracker.isgpsenabled()) {
                homePageRequest(Iconstant.driverHomePage_Url);
            } else {
                enableGpsService();
            }
        } else {
            alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }

    }

    private void startBlinkingAnimation() {
        Animation startAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.blinking_animation);
        Iv_status.startAnimation(startAnimation);
    }

    @Override
    public void onClick(View view) {

        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {

            if (view == Tv_myTrip) {
                Intent intent = new Intent(getActivity(), MyTrip.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
            } else if (view == Tv_earnings) {
                Intent intent = new Intent(getActivity(), DriverEarnings.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
            } else if (view == Tv_incentive) {
                Intent intent = new Intent(getActivity(), IncentivesDashboard.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
            } else if (view == RL_SOS) {
                Intent intent = new Intent(getActivity(), Sosdragmap_screen.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
            } else if (view == RL_Complaint) {
                Intent intent = new Intent(getActivity(), ComplaintPage.class);
                intent.putExtra("frompage", "Homepage");
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
//                sendEmail(sContactEmail);
            } else if (view == RL_Support) {
                /*Intent intent = new Intent(getActivity(), TalkPage.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);*/

                callCustomerCare();
//                Intent intent = new Intent(getActivity(), RatingPage.class);
//                intent.putExtra("RideID","1531216034");
//                startActivity(intent);


            } else if (view == LL_driveNow) {
//                driveNowDialog();
                if (sVerifyStatus.equalsIgnoreCase("Yes")) {
                    if (sGoOnlineStatus.equalsIgnoreCase("1")) {
                        driveNowDialog();
                    } else {
                        showVerifyAccountDialog(getResources().getString(R.string.label_welcome_to) + sCityName, sGoOnlineString);
                    }
                } else {
                    if (sGoOnlineStatus.equalsIgnoreCase("0")) {
                        showVerifyAccountDialog(getResources().getString(R.string.action_error), sGoOnlineString);
                    } else {
                        showVerifyAccountDialog(getResources().getString(R.string.action_error), getActivity().getResources().getString(R.string.driver_homepage_alert_label_verify_account));
                    }
                }
            }

        } else {
            alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }

    }


    @SuppressLint("WrongConstant")
    private void setAvailableCars(String carsQty) throws NumberFormatException {

        int carCount = 0;
        String greetingMsg = "", finalMsg = "";
        int span1 = 0, span2 = 0;

        if (carsQty.length() > 0) {
            carCount = Integer.parseInt(carsQty);
        }

        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);

        if (timeOfDay >= 0 && timeOfDay < 12) {
            greetingMsg = getActivity().getResources().getString(R.string.driver_homepage_greeting_morning);
        } else if (timeOfDay >= 12 && timeOfDay < 16) {
            greetingMsg = getActivity().getResources().getString(R.string.driver_homepage_greeting_afternoon);
        } else if (timeOfDay >= 16 && timeOfDay < 21) {
            greetingMsg = getActivity().getResources().getString(R.string.driver_homepage_greeting_evening);
        } else if (timeOfDay >= 21 && timeOfDay < 24) {
            greetingMsg = getActivity().getResources().getString(R.string.driver_homepage_greeting_night);
        }

        if (carCount == 0) {
//            finalMsg = greetingMsg + getActivity().getResources().getString(R.string.driver_homepage_label_no_car_near);
            finalMsg = greetingMsg + getActivity().getResources().getString(R.string.driver_homepage_label_only_you);
        } else if (carCount == 1) {
            finalMsg = greetingMsg + getActivity().getResources().getString(R.string.driver_homepage_label_there_is) + " " + carCount + " "
                    + getActivity().getResources().getString(R.string.driver_homepage_label_car) + " "
                    + getActivity().getResources().getString(R.string.driver_homepage_label_near_you);

            span1 = (greetingMsg + getActivity().getResources().getString(R.string.driver_homepage_label_there_is) + " ").length();
            span2 = (greetingMsg + getActivity().getResources().getString(R.string.driver_homepage_label_there_is) + " " + carCount + " "
                    + getActivity().getResources().getString(R.string.driver_homepage_label_car)).length();

        } else {
            finalMsg = greetingMsg + getActivity().getResources().getString(R.string.driver_homepage_label_there_are) + " " + carCount + " "
                    + getActivity().getResources().getString(R.string.driver_homepage_label_cars) + " "
                    + getActivity().getResources().getString(R.string.driver_homepage_label_near_you);

            span1 = (greetingMsg + getActivity().getResources().getString(R.string.driver_homepage_label_there_are) + " ").length();
            span2 = (greetingMsg + getActivity().getResources().getString(R.string.driver_homepage_label_there_are) + " " + carCount + " "
                    + getActivity().getResources().getString(R.string.driver_homepage_label_cars)).length();

        }

        Spannable spanText = new SpannableString(finalMsg);
        spanText.setSpan(new ForegroundColorSpan(getResources()
                        .getColor(R.color.white_color)), span1
                , span2, 0);

        Tv_cars_nearby.setText(spanText);

    }


    //--------------Alert Method-----------
    private void alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(getActivity());
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    //--------------Alert Method-----------
    private void alertcontraint(String title, String alert) {
        final PkDialog mDialog = new PkDialog(getActivity());
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                Intent intent = new Intent(getActivity(), WalletMoneyPage1.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        mDialog.show();
    }


    //-----------Alert when driver account not verified----------
    private void showVerifyAccountDialog(String title, String message) {
        final Dialog dialog = new Dialog(getActivity(), R.style.SlideUpDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.alert_could_not_drive);

        TextView Tv_title = (TextView) dialog.findViewById(R.id.txt_label_title);
        TextView Tv_message = (TextView) dialog.findViewById(R.id.txt_label_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        Button okButton = (Button) dialog.findViewById(R.id.btn_ok);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });

        dialog.show();

    }

    /*private void showVerifyAccountDialog(String title) {
        final Dialog dialog = new Dialog(getActivity(), R.style.SlideUpDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.alert_verify_acount);

        TextView text = (TextView) dialog.findViewById(R.id.txt_verify_alert_title);
        text.setText(title);

        Button okButton = (Button) dialog.findViewById(R.id.btn_verify_alert_ok);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });

        dialog.show();

    }*/


    //-----------Alert when clicking drive now----------
    @SuppressWarnings("WrongConstant")
    private void driveNowDialog() {
        final Dialog driveNowDialog = new Dialog(getActivity(), R.style.SlideUpDialog);
        driveNowDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        driveNowDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        driveNowDialog.setCancelable(true);
        driveNowDialog.setCanceledOnTouchOutside(true);
        driveNowDialog.setContentView(R.layout.alert_rules_of_road);

        //--------Adjusting Dialog width and height-----
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 85% of the screen
        int screenHeight = (int) (metrics.heightPixels * 0.85);//fill only 85% of the screen

        driveNowDialog.getWindow().setLayout(screenWidth, screenHeight);

        final RelativeLayout RL_driveNow = (RelativeLayout) driveNowDialog.findViewById(R.id.RL_drive_now);
        final RelativeLayout RL_close = (RelativeLayout) driveNowDialog.findViewById(R.id.RL_close);

        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);

        String greetingMsg = "";
        if (timeOfDay >= 0 && timeOfDay < 12) {
            greetingMsg = getActivity().getResources().getString(R.string.driver_homepage_greeting_morning);
        } else if (timeOfDay >= 12 && timeOfDay < 16) {
            greetingMsg = getActivity().getResources().getString(R.string.driver_homepage_greeting_afternoon);
        } else if (timeOfDay >= 16 && timeOfDay < 21) {
            greetingMsg = getActivity().getResources().getString(R.string.driver_homepage_greeting_evening);
        } else if (timeOfDay >= 21 && timeOfDay < 24) {
            greetingMsg = getActivity().getResources().getString(R.string.driver_homepage_greeting_night);
        }


        RL_driveNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                driveNowDialog.dismiss();

                Goonlinerequestwalletcheck(Iconstant.driverMapView_Url);

            }
        });

        RL_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (driveNowDialog.isShowing()) {
                    driveNowDialog.dismiss();
                }
            }
        });

        driveNowDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                LL_driveNow.setEnabled(true);
            }
        });

        driveNowDialog.show();
        LL_driveNow.setEnabled(false);

    }

    //-------------------Home Page Request----------------
    @SuppressLint("WrongConstant")
    private void homePageRequest(String Url) {

        if (getActivity() != null) {
            dialog = new Dialog(getActivity());
            dialog.getWindow();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.custom_loading);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriveID);
        jsonParams.put("mode", "get");
        jsonParams.put("lat", String.valueOf(gpsTracker.getLatitude()));
        jsonParams.put("lon", String.valueOf(gpsTracker.getLongitude()));

        System.out.println("--------------homePageRequest Url-------------------" + Url);
        System.out.println("--------------jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(getActivity());
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------response-------------------" + response);
                String status = "", sComplaint_badge = "", sInfo_badge = "", sVehicleNo = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {
                            JSONObject jsonObject = object.getJSONObject("response");
                            if (jsonObject.length() > 0) {
                                sNormalFareStatus = jsonObject.getString("normal_fare");
                                sBidFareStatus = jsonObject.getString("bid_fare");
                                sSosStatus = jsonObject.getString("sos");
                                sComplaint_badge = jsonObject.getString("complaint_badge");
                                sInfo_badge = jsonObject.getString("info_badge");
                                sVehicleNo = jsonObject.getString("vehicle_number");
                                sNearbyDriversCount = jsonObject.getString("near_by_driver");
                                sSearchRadius = jsonObject.getString("searching_radius");
                                sVerifyStatus = jsonObject.getString("verify_status");
                                sTrips = jsonObject.getString("total_completed_trip");
                                sIncentives = jsonObject.getString("incetives_total");
                                sEarnings = jsonObject.getString("driver_earning");
                                sCurrencyCode = jsonObject.getString("currencyCode");
                                sGoOnlineStatus = jsonObject.getString("go_online_status");
                                sGoOnlineString = jsonObject.getString("go_online_string");
                                sDriverAvailability = jsonObject.getString("driver_availability");
                                sCityName = jsonObject.getString("city_name");

                                if (jsonObject.has("image")) {
                                    sBannerImage = jsonObject.getString("image");
                                }
                                sNeedReferralCode = jsonObject.getString("referal_code");

//                                sCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(sCurrencyCode);
                                sCurrencySymbol = sCurrencyCode;
                                sessionManager.setCurrency(sCurrencyCode);


                            }
                        } else {
                            String sResponse = object.getString("response");
                            alert(getResources().getString(R.string.action_error), sResponse);
                        }

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }

                if (status.equalsIgnoreCase("1")) {

                   /*if(sBannerImage.length()>0) {
                       Picasso.with(getActivity()).load(sBannerImage).error(R.drawable.meter).error(R.drawable.meter).into(Iv_banner);
                   }*/

                    if (!sComplaint_badge.equalsIgnoreCase("0")) {
                        Tv_Complaint_Notification.setVisibility(View.VISIBLE);
                        Tv_Complaint_Notification.setText(sComplaint_badge);
                    } else {
                        Tv_Complaint_Notification.setVisibility(View.INVISIBLE);
                    }

                    if (sInfo_badge.length() > 0) {
                        Intent updateInfoNotification = new Intent();
                        updateInfoNotification.setAction("com.app.infoNotification.NavigationPage");
                        updateInfoNotification.putExtra("info_badge", sInfo_badge);
                        updateInfoNotification.putExtra("referral_status", sNeedReferralCode);
                        getActivity().sendBroadcast(updateInfoNotification);
                    }

                    if (sNearbyDriversCount.length() > 0) {
                        setAvailableCars(sNearbyDriversCount);
                    } else {
                        setAvailableCars("0");
                    }

                    Tv_myTrip.setText(sTrips);
                    Tv_earnings.setText(sCurrencySymbol + " " + sEarnings);
                    Tv_incentive.setText(sCurrencySymbol + " " + sIncentives);

                    if (sVerifyStatus.equalsIgnoreCase("Yes")) {
                        if (sGoOnlineStatus.equalsIgnoreCase("1")) {
                            LL_verifyAccount.setVisibility(View.GONE);
                            Tv_driver_unverified.setVisibility(View.GONE);
                            RL_way.setVisibility(View.VISIBLE);

                            if (sDriverAvailability.equalsIgnoreCase("yes")) {

                                sessionManager.setXmppServiceState("online");
                                Intent intent = new Intent(getActivity(), OnlinepageConstrain.class);
                                if (NavigationPage.fromrequestpage != null) {
                                    if (NavigationPage.fromrequestpage.equalsIgnoreCase("")) {
                                    } else {
                                        intent.putExtra("fromrequestpage", "RequestTimerAdapter");
                                    }
                                }
                                startActivity(intent);
                                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);

                            }

                        } else {
                            Tv_driver_unverified.setText(sGoOnlineString);
                            LL_verifyAccount.setVisibility(View.VISIBLE);
                            Tv_driver_unverified.setVisibility(View.VISIBLE);
                            RL_way.setVisibility(View.GONE);
                        }
                    } else {
                        Tv_driver_unverified.setVisibility(View.VISIBLE);
                        LL_verifyAccount.setVisibility(View.VISIBLE);
                        RL_way.setVisibility(View.GONE);
                    }
                    sessionManager.setXmppServiceState("online");

                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
    }


    //-------------------Update Fare Request----------------
    @SuppressLint("WrongConstant")
    private void updateFareRequest(String Url, String mType, String mAvailability) {

        dialog = new Dialog(getActivity());
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_updating));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriveID);
        jsonParams.put("mode", "update");
        jsonParams.put("type", mType);
        jsonParams.put("availability", mAvailability);

        System.out.println("--------------updateFareRequest Url-------------------" + Url);
        System.out.println("--------------jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(getActivity());
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------response-------------------" + response);
                String status = "", sComplaint_badge = "", sInfo_badge = "", sVehicleNo = "";

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {
                        status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {
                            JSONObject jsonObject = object.getJSONObject("response");
                            if (jsonObject.length() > 0) {
                                sNormalFareStatus = jsonObject.getString("normal_fare");
                                sBidFareStatus = jsonObject.getString("bid_fare");
                                sSosStatus = jsonObject.getString("sos");
                                sComplaint_badge = jsonObject.getString("complaint_badge");
                                sInfo_badge = jsonObject.getString("info_badge");
                                sVehicleNo = jsonObject.getString("vehicle_number");
                            }
                        } else {
                            String sResponse = object.getString("response");
                            alert(getResources().getString(R.string.action_error), sResponse);
                        }

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (status.equalsIgnoreCase("1")) {

                    /*status_ON_OFF(sNormalFareStatus, Tv_normal_ON, Tv_normal_OFF);
                    status_ON_OFF(sBidFareStatus, Tv_bid_ON, Tv_bid_OFF);
                    status_ON_OFF(sSosStatus, Tv_sos_ON, Tv_sos_OFF);

                    Tv_carNumber.setText(sVehicleNo);*/

                    if (!sComplaint_badge.equalsIgnoreCase("0")) {
                        Tv_Complaint_Notification.setVisibility(View.VISIBLE);
                        Tv_Complaint_Notification.setText(sComplaint_badge);
                    } else {
                        Tv_Complaint_Notification.setVisibility(View.INVISIBLE);
                    }

                }

                if (dialog != null) {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    private void Goonlinerequestwalletcheck(String Url) {
        dialog = new Dialog(getActivity());
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriveID);

        System.out.println("--------------GoOnlineRequest Url-------------------" + Url);
        System.out.println("--------------GoOnlineRequest jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(getActivity());
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------GoOnlineRequest response-------------------" + response);
                String status = "", sCurrency = "", sLast_trip_dt = "", sLast_trip_distance = "", sLast_paid_mode = "", sLast_paid_amount = "", sCurrencySymbol = "", sTotalCompletedDistance = "", sDistanceUnit = "",
                        sOnlineHours = "", sTotalCompletedRides = "0", sCancelledRides = "0", sDeniedRides = "0";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {
                            JSONObject jsonObject = object.getJSONObject("response");
                            String walletcontraint = jsonObject.getString("wallet_constraint");
                            String minwallet = jsonObject.getString("min_wallet_amount ");
                            if (walletcontraint.equalsIgnoreCase("1")) {
                                GoOnlineRequest(Iconstant.updateAvailability_Url);
                            } else {
                                alertcontraint("Error", "Your wallet amount is less than INR " + minwallet + " Would you like to recharge your wallet?");
                            }
                        } else {
                            String sResponse = object.getString("response");
                            alert(getResources().getString(R.string.action_error), sResponse);
                        }

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (status.equalsIgnoreCase("1")) {

                }
                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    //-------------------Go Online Request----------------
    private void GoOnlineRequest(String Url) {

        dialog = new Dialog(getActivity());
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_pleasewait));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriveID);
        jsonParams.put("availability", "Yes");

        System.out.println("--------------GoOnlineRequest Url-------------------" + Url);
        System.out.println("--------------jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(getActivity());
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------response-------------------" + response);
                String status = "";

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {
                        status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {

                            sessionManager.setXmppServiceState("online");
                            Intent intent = new Intent(getActivity(), OnlinepageConstrain.class);
                            if (NavigationPage.fromrequestpage != null) {
                                if (NavigationPage.fromrequestpage.equalsIgnoreCase("")) {
                                } else {
                                    intent.putExtra("fromrequestpage", "RequestTimerAdapter");
                                }
                            }
                            startActivity(intent);
                            getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                        } else {
                            String sResponse = object.getString("response");
                            alert(getResources().getString(R.string.action_error), sResponse);
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    @SuppressLint("WrongConstant")
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        boolean b = false;
        ActivityManager manager = (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                System.out.println("1 already running");
                b = true;
                break;
            } else {
                System.out.println("2 not running");
                b = false;
            }
        }
        System.out.println("3 not running");
        return b;
    }

    //Enabling Gps Service
    private void enableGpsService() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        //...
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(getActivity(), REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @SuppressLint("WrongConstant")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_LOCATION:
                switch (resultCode) {
                    case Activity.RESULT_OK: {
                        Toast.makeText(getActivity(), "Location enabled!", Toast.LENGTH_LONG).show();

                        if (isInternetPresent) {
                            if (gpsTracker.isgpsenabled() && gpsTracker.canGetLocation()) {
                                homePageRequest(Iconstant.driverHomePage_Url);
                            } else {
                                enableGpsService();
                            }
                        } else {
                            alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                        }

                        break;
                    }
                    case Activity.RESULT_CANCELED: {
                        getActivity().finish();
                        break;
                    }
                    default: {
                        break;
                    }
                }
                break;
        }
    }


    private void callCustomerCare() {

        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+

            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + sessionManager.getCustomerServiceNo()));
            startActivity(intent);

        } else {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + sessionManager.getCustomerServiceNo()));
            startActivity(intent);
        }
    }

    private void requestPermission() {
    }



    private boolean checkReadStatePermission() {
        int result = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_PHONE_STATE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + sessionManager.getCustomerServiceNo()));
                    startActivity(callIntent);
                }
                break;
        }
    }

    //----------Sending message on Email Method--------
    protected void sendEmail(String to) {
        String[] TO = {to};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "");
        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.invite_earn_label_email_not_installed));
        }
    }

}
