package com.blackmaria.partner.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.FragmentHockeyApp;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.app.NavigationPage;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class DynamicContent extends FragmentHockeyApp {

    private TextView Tv_title, Tv_content, Tv_empty;
    private RelativeLayout Rl_dataAvailable, Rl_dataEmpty;

    private ConnectionDetector cd;
    private boolean isInternetPresent = false;
    private SessionManager sessionManager;
    private String sDriveID = "", sPageType = "";

    private ServiceRequest mRequest;
    Dialog dialog;

    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.dynamic_content, container, false);
        initialize(rootView);
        return rootView;
    }

    private void initialize(View rootView) {

        sessionManager = new SessionManager(getActivity());
        cd = new ConnectionDetector(getActivity());
        isInternetPresent = cd.isConnectingToInternet();
        NavigationPage.inHomePage = false;

        Tv_title = (TextView) rootView.findViewById(R.id.dynamic_content_title_textView);
        Tv_content = (TextView) rootView.findViewById(R.id.dynamic_content_content_textView);
        Tv_empty = (TextView) rootView.findViewById(R.id.dynamic_content_empty_textView);
        Rl_dataAvailable = (RelativeLayout) rootView.findViewById(R.id.dynamic_content_data_available_layout);
        Rl_dataEmpty = (RelativeLayout) rootView.findViewById(R.id.dynamic_content_data_empty_layout);

        sPageType = getArguments().getString("pageType");

        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriveID = user.get(SessionManager.KEY_DRIVERID);

        if (isInternetPresent) {
            dynamicContentRequest(Iconstant.Dynamic_content_Url);
        } else {
            alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }

    }

    private void alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(getActivity());
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    //-------------------dynamic Content Request----------------
    private void dynamicContentRequest(String Url) {

        dialog = new Dialog(getActivity());
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriveID);
        jsonParams.put("page_type", sPageType);
        jsonParams.put("app_type", "driver");

        System.out.println("--------------dynamic Content Url-------------------" + Url);
        System.out.println("--------------jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(getActivity());
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------response-------------------" + response);
                String status = "", sTitle = "", sContent = "", sResponse = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {
                            JSONObject jsonObject = object.getJSONObject("response");
                            if (jsonObject.length() > 0) {
                                JSONObject pageObject = jsonObject.getJSONObject("pages_details");
                                if (pageObject.length() > 0) {
                                    sTitle = pageObject.getString("page_title");
                                    sContent = pageObject.getString("page_description");
                                }
                            }
                        } else {
                            sResponse = object.getString("response");
                            alert(getResources().getString(R.string.action_error), sResponse);
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (status.equalsIgnoreCase("1")) {
                    Rl_dataAvailable.setVisibility(View.VISIBLE);
                    Rl_dataEmpty.setVisibility(View.GONE);
                    Tv_title.setText(sTitle);
                    Tv_content.setText(Html.fromHtml(sContent));
                } else {
                    Rl_dataAvailable.setVisibility(View.GONE);
                    Rl_dataEmpty.setVisibility(View.VISIBLE);
                    Tv_empty.setText(sResponse);
                }

                if (dialog != null) {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (dialog != null) {
            dialog.dismiss();
        }
    }
}
