package com.blackmaria.partner.iconstant;

public interface Iconstant {
    // Local Url
    /*String BaseUrl = "http://192.168.1.244/cabily/customization/blackmaria/v11/";
    String MainUrl = "http://192.168.1.244/cabily/customixendit-card-payment-driverzation/blackmaria/";*/
//    Live Url
    String BaseUrl = "https://app.blackmaria.co/v11/";
    String MainUrl = "https://app.blackmaria.co/";
    String getcompanynames = BaseUrl + "api/v6/app/get-company-list";
    String getlanguaage = BaseUrl + "api/v6/language/get";
    /* String BaseUrl = "https://blackmaria.casperon.co/v11/";
    String MainUrl = "https://blackmaria.casperon.co/";*/
    String sendnotification = BaseUrl + "api/v6/app/chat/push-chat-message";
    String updatepincode = BaseUrl + "api/v6/app/driver/change/pin";
    String getAppInfo = BaseUrl + "api/v6/get-app-info";
    String updateDriverLocation = BaseUrl + "api/v6/provider/update-driver-geo";
    String loginurl = BaseUrl + "api/v6/provider/check-account";
    String register_otp_url = BaseUrl + "api/v6/provider/login";
    String getlocationlist = BaseUrl + "api/v6/app/driver/get-location-list";
    String sos_alert_url = BaseUrl + "api/v6/app/provider/alert-emergency-contact";
    String register_otp_resend_url = BaseUrl + "api/v6/provider/resend-otp";
    String Verifymobile = BaseUrl + "api/v6/app/verify/mobile";
    String Verifyemail = BaseUrl + "api/v6/app/verify/email";
    String updatepattern = BaseUrl + "api/v6/app/driver/update-pattern";
    String getpattern = BaseUrl + "api/v6/app/driver/get-pattern";
    String driverHomePage_Url = BaseUrl + "api/v6/provider/driver-setting";
    String updateAvailability_Url = BaseUrl + "api/v6/provider/update-availability";
    String driverMapView_Url = BaseUrl + "api/v6/provider/driver-mapview";
    String continuePendingTrip_Url = BaseUrl + "api/v6/provider/continue-trip";

    String driverAcceptRequest_Url = BaseUrl + "api/v6/provider/accept-ride";
    String driverDeclineRequest_Url = BaseUrl + "api/v6/provider/decline-ride";

    String faredetail_Url = BaseUrl + "v6/app/get-fare";
    String receive_payment_Url = BaseUrl + "api/v6/provider/receive-payment";


    String driverArrived_Url = BaseUrl + "api/v6/provider/arrived";
    String driverBegin_Url = BaseUrl + "api/v6/provider/begin-ride";
    String driverEnd_Url = BaseUrl + "api/v6/provider/end-ride";
    String driverCancel_reason_Url = BaseUrl + "api/v6/provider/cancellation-reason";
    String driverCancel_Url = BaseUrl + "api/v6/provider/cancel-ride";
    String payment_received_Url = BaseUrl + "api/v6/provider/payment-received";

    String Rating_option_Url = BaseUrl + "api/v6/app/review/options-list";
    String Rating_submit_Url = BaseUrl + "api/v6/app/review/submit";
    String Profile_emergency_add_Url = BaseUrl + "v6/api/app/emergency-add";
    String Profile_driver_profile_Url = BaseUrl + "v6/api/app/driver-profile";
    String Profile_driver_account_Url = BaseUrl + "v6/api/app/profile-account";
    String Paypal_account_Url = BaseUrl + "api/v6/provider/save-paypal-info";
    String Bank_account_Save_Url = BaseUrl + "api/v6/provider/save-banking-info";
    String Profile_driver_account_Logout_Url = BaseUrl + "api/v6/provider/logout";

    String updatelanguagee = BaseUrl + "api/v6/language/update";

    String Dynamic_content_Url = BaseUrl + "api/v6/app/get-page-details";
    String myTrip_Url = BaseUrl + "api/v6/provider/mytrip-dashboard";
    String myTrip_detail_todayTrip_Url = BaseUrl + "api/v6/provider/today-trip";
    String myTrip_detail_weekTrip_Url = BaseUrl + "api/v6/provider/week-trip";
    String myTrip_detail_monthTrip_Url = BaseUrl + "api/v6/provider/month-trip";
    String multipleDrop_Url = BaseUrl + "api/v6/provider/update-multipledrop";
    String app_availability_url = BaseUrl + "api/v6/xmpp-status";
    String returnTrip_Url = BaseUrl + "api/v6/provider/update-return-drop";

    String earningsPage_Url = BaseUrl + "api/v6/app/earnings_list_driver";
    String earningsPage_withDraw_Url = BaseUrl + "api/v6/app/withdraw-amount";
    String incentivePage_Url = BaseUrl + "api/v6/app/incentives-list";
    String incentivePage_withDraw_Url = BaseUrl + "api/v6/app/withdraw-amount-incetive";

    String phoneMasking_url = BaseUrl + "api/v6/app/phone-call";

    String send_thank_you_Url = BaseUrl + "v6/api/v1/app/send-thankyou";

    String Edit_profile_mobileNo_url = BaseUrl + "api/v1/app/mobilenumber-change";
    String wallet_money_url = BaseUrl + "api/v6/app/get-money-page-driver";
    String wallet_money_transaction_url = BaseUrl + "api/v6/app/get-trans-list-driver";
    String wallet_money_stripe_webview_url = BaseUrl + "v6/mobile/wallet-recharge-driver/payform?";
    String wallet_money_paypal_webview_url = BaseUrl + "v6/mobile/wallet-recharge-driver/paypal-process?";
    String wallet_add_money_url = BaseUrl + "v6/mobile/wallet-recharge-driver/stripe-process";

    String place_search_url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?types=geocode&key=AIzaSyCts8476Bjpz_8wD-PQPnyXxM3KnletsVQ&input=";
    String GetAddressFrom_LatLong_url = "https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyCts8476Bjpz_8wD-PQPnyXxM3KnletsVQ&placeid=";
    String GetDistanceAndTime_url = "https://maps.googleapis.com/maps/api/distancemv6/mobile/wallet-recharge-driver/payform?atrix/json?units=metric&mode=driving&key=AIzaSyCUVscqC6gPU0CkC5AIKyjUNbKmQlQT4No";
    String place_search_key = "AIzaSyCts8476Bjpz_8wD-PQPnyXxM3KnletsVQ";

    String today_trips_url = BaseUrl + "api/v6/provider/today-statistics";
    String week_trips_url = BaseUrl + "api/v6/provider/week-statistics";
    String month_trips_url = BaseUrl + "api/v6/provider/month-statistics";
    String complete_trip_detail_url = BaseUrl + "api/v6/app/view-ride-detail";

    String today_trips_details_url = BaseUrl + "api/v6/provider/daywise-rides";
    String week_trips_details_url = BaseUrl + "api/v6/provider/weekwise-rides";
    String month_trips_details_url = BaseUrl + "api/v6/provider/monthwise-rides";

    String driver_and_earn_url = BaseUrl + "api/v6/app/get-invites-driver";

    String message_url = BaseUrl + "api/v6/app/message-list";

    String earnings_dashboard_url = BaseUrl + "api/v6/app/earnings-dashboard";
    String earnings_daywise_url = BaseUrl + "api/v6/provider/daywise-rides-earning";
    String earnings_weekwise_url = BaseUrl + "api/v6/provider/weekwise-rides-earning";
    String earnings_monthwise_url = BaseUrl + "api/v6/provider/monthwise-rides-earning";
    String earnings_withdraw_url = BaseUrl + "api/v6/app/earnings-withdraw";

    String earnings_withdraw_amount_url = BaseUrl + "api/v6/app/withdraw-earning-amount";

    String incentives_dashboard_url = BaseUrl + "api/v6/app/incentives-dashboard";
    String incentives_list_url = BaseUrl + "api/v6/app/incentives-list";
    String incentives_withdraw_url = BaseUrl + "api/v6/app/incentives-withdraw";
    String incentives_withdraw_amount_url = BaseUrl + "api/v6/app/incentives-withdraw-amount";

    String drive_and_earn_statement_url = BaseUrl + "api/v6/app/earnings-drive-earn";
    String sos_url = BaseUrl + "api/v6/app/provider/alert-emergency-contact";
    String withdrawal_statement_url = BaseUrl + "api/v6/app/withdrawal-statement";
    String check_driver_referral_code_url = BaseUrl + "api/v6/app/driver/referral-code";

    String complaint_list_url = BaseUrl + "api/v6/app/driver/support-list";
    String complaint_list = BaseUrl + "api/v6/app/driver/ticket-list";
    String view_ticket_url = BaseUrl + "api/v6/app/driver/view-ticket";
    String complaint_reply_url = BaseUrl + "api/v6/app/driver/reply-comment";
    String post_comment_url = BaseUrl + "api/v6/app/driver/reply-comment";
    String wallet_withdrawal_dashboard_url = BaseUrl + "api/v6/app/wallet-withdraw";
    String wallet_withdraw_amount_url = BaseUrl + "api/v6/app/wallet-withdraw-amount";

    String waiting_close_url = BaseUrl + "v6/api/v1/app/complementary-close";
    String waiting_close_drop = BaseUrl + "v6/api/v1/app/close-waiting";

    String get_rider_details_url = BaseUrl + "v6/api/app/user-profile";

    String read_message_url = BaseUrl + "api/v6/app/update-message";
    String delete_message_url = BaseUrl + "api/v6/app/delete-message";

    String driver_signup_url = BaseUrl + "app/driver/signup";

    String deactivate_account_statement_url = BaseUrl + "v6/api/v1/app/account-statement-driver";
    String deactivate_account_url = BaseUrl + "v6/api/v1/app/deactivate-account";

    String referral_withdraw_url = BaseUrl + "api/v6/app/referral-withdraw";
    String referral_withdraw_amount_url = BaseUrl + "api/v6/app/referral-amount-withdraw";

    String ratecard_select_city_url = BaseUrl + "api/v1/app/get-location";
    String ratecard_select_cartype_url = BaseUrl + "v6/api/v1/app/get-category";
    String ratecard_display_url = BaseUrl + "api/v6/app/get-ratecard-page";

    String submit_referral_url = BaseUrl + "api/v6/app/submit-referral";
    String confirm_referral_url = BaseUrl + "api/v6/app/driver/confirm-code";

    String forgot_pin_url = BaseUrl + "api/v6/provider/forgot/pin";


    String getXendit_bank_saved_detail = BaseUrl + "api/v6/app/get-bank-xendit-driver";
    String getXendit_bank_save_url = BaseUrl + "api/v6/app/save-bank-xendit-driver";

    String xendit_card_pay_url = BaseUrl + "api/v6/app/xendit-card-payment-driver";
    String xendit_bank_pay_url = BaseUrl + "api/v6/app/xendit-bank-payment-driver";

    String redeem_code_url = BaseUrl + "api/v6/app/apply-redeem-code-driver";

    String request_payment_url = BaseUrl + "api/v6/app/request-payment";
    String check_payment_status_url = BaseUrl + "api/" +
            "v6/check-trip-status";

    String driver_find_friend_wallet_url = BaseUrl + "api/v6/app/provider/find-friend";



    String redeem_code_url1 = BaseUrl + "api/v6/app/free-redeem-code-driver";

    /*DRIVER REGISTRATION URLs*/
    String DriverReg_Locationwise_Carcategory_URL = BaseUrl + "api/v6/app/get-available-category";
    String DriverReg_upload_Image_URl = BaseUrl + "api/v6/app/upload-image";
    String uploadimageforchat = BaseUrl + "app/upload/chat/image";
    String DriverReg_applyReferal_URl = BaseUrl + "api/v6/app/apply-referral-code";
    String DriverReg_getCompanyList_URl = BaseUrl + "api/v6/app/get-company-list";
    String DriverReg_getCountryList_URl = BaseUrl + "api/v6/app/get-country-list";
    String DriverReg_UploadVehicleImage_URl = BaseUrl + "api/v6/app/upload-image-vehicle";
    String DriverReg_VehicleList_URl = BaseUrl + "api/v6/app/get-vehicle-list";
    String DriverReg_MarkerList_URl = BaseUrl + "api/v6/app/get-maker-list";
    String DriverReg_ModelList_URl = BaseUrl + "api/v6/app/get-model-list";
    String DriverReg_YearList_URl = BaseUrl + "api/v6/app/get-year-list";
    String DriverRegistration_URl = BaseUrl + "api/v6/app/register";
    String DriverRegistration_VerifyMobile_URl = BaseUrl + "api/v6/app/send-otp-driver";
    String driver_signup_web_url = MainUrl + "driver/signup";
    //----------------Browser Static Pages Url-------------------
    String about_us_url = MainUrl + "pages/about-us";
    /*  String about_url = "http://blackmaria.co";//Url + "pages/about-us";*/
    String about_booking_url = "https://blackmaria.co/global/faq-booking/";//Url + "pages/booking";
    String earning_url = "https://blackmaria.co/global/earnings-commmision/";//Url + "pages/earningcommision";


    String profile_sos_remove_url = BaseUrl + "v6/api/v1/app/emergency-delete-driver";
    String userprofile_update_sos_info_url = BaseUrl + "v6/api/app/driver-sos-contact";
    String userprofile_update_login_info_url = BaseUrl + "api/v6/app/driver-login";
    String User_Otp_LoginUpdate = BaseUrl + "api/v6/app/contact-update-otp";

    /*Rating Urls*/
    String menupage_ratinglist_home = BaseUrl + "api/v6/app/review/driver-review";
    String menupage_ratinglist = BaseUrl + "api/v6/app/review/review-list-driver";
    String Driver_Activiate_Referal = BaseUrl + "api/v6/app/update-referral-program";
    String Transfersaveplus = BaseUrl + "api/v6/app/driver/amount/transfer";



    String Ewallet_withdrawal_confirm_url = BaseUrl + "api/v6/app/provider/wallet-withdraw-amount";


    // String about_booking_url = MainUrl + "pages/booking";
    String about_bidding_url = MainUrl + "pages/bidding";
    String about_miles_url = MainUrl + "pages/miles";
    String about_referral_url = MainUrl + "pages/referral";
    String about_ewallet_url = MainUrl + "pages/e-wallet";
    String about_sos_url = MainUrl + "pages/sos";
    String about_account_url = MainUrl + "pages/account";
    String about_help_url = MainUrl + "pages/help";
    String terms_and_conditions_url = MainUrl + "pages/terms-and-conditions";
    String user_policy_url = MainUrl + "pages/userpolicy";
    String bidfare_terms_url = MainUrl + "pages/bidfare-terms";
    String loyalty_program_url = MainUrl + "pages/loyalty-program";
    String ride_earn_referral_url = MainUrl + "pages/ride-earn-referral";
    String sos_terms_url = MainUrl + "pages/sos-terms-conditions";
    String privacy_policy_url = MainUrl + "pages/privacy-and-policy";

    String rechargecheck_url = BaseUrl + "api/v6/app/driver/check/recharge";

    String ACTION_ACTION_HOCKYAPPID = "22d9f1b92d89401aa5a83ab6c5169eab";//"9f8e1861d5cc413ba593e3367676bca3";

    //----------------------UserAgent---------------------
    String cabily_userAgent = "cabily2k15android";
    String cabily_IsApplication = "1";
    String cabily_AppLanguage = "en";
    String cabily_AppType = "android";
    String cabily_AppToken = "";


    //----------------Push notification Key--------------------------
    String ACTION_LABEL = "action";
    String ACTION_TAG_RIDE_REQUEST = "ride_request";
    String ACTION_TAG_RIDE_CANCELLED = "ride_cancelled";

    String ACTION_TAG_CHATMESSAGE = "blackmariachat";
    String ACTION_TAG_RECEIVE_CASH = "receive_cash";
    String ACTION_TAG_PAYMENT_PAID = "payment_paid";
    String ACTION_TAG_NEW_TRIP = "new_trip";
    String ACTION_TAG_COMPLEMENTARY = "complementary";
    String ACTION_TAG_REFERRAL_CREDIT = "referral_credit";
    String ACTION_WALLET_SUCCESS = "wallet_success_driver";
    String ACTION_BANK_SUCCESS = "bank_success_driver";
    String ACTION_TAG_WEEK_TRIP = "week_trip";
    String ACTION_TAG_DAILY_TRIP = "daily_trip";
    String ACTION_TAG_DAILY_MILE = "daily_mile";
    String ACTION_TAG_SCHEDULE_INCENTIVE = "schedule_incetive";
    String ACTION_TAG_WALLET_CREDIT = "wallet_credit";


    String pushNotification_Ads = "ads";
    String Ads_title = "key1";
    String Ads_Message = "key2";
    String Ads_image = "key3";
    String Referral_Message = "message";

    String userEndTrip = "ride_completed_user";
    String userRidePaymentTrip = "ride_completed_payment";
    String End_rideid = "key1";
    String End_driver_id = "key2";


    //new api
    String getPaymentOPtion_url_new = BaseUrl + "api/v6/app/get-bank-info-driver";
    String fastpayhome = BaseUrl + "api/v6/app/drivers/wallet/summery";
    String cloudmoney_crossbordervalue_Url = BaseUrl + "api/v6/app/provider/cross-border";
    String driver_wallet_to_wallet_transfer_url = BaseUrl + "api/v6/app/provider/amount-transfer";
    String find_Friend_Account_url = BaseUrl + "api/v6/app/provider/find-friend";
    String paypalsuccessapi = BaseUrl + "wallet-recharge-driver/payment/details";
    String closeaccount_request = BaseUrl + "v6/api/v1/app/close-account-request-driver";
    String closeaccountpaymentslist = BaseUrl + "v6/api/v1/app/close-account-payment-driver";
    String mesggaeg_url = BaseUrl + "api/v6/app/message-list";
    String message_update_list = BaseUrl + "api/v6/app/update-message";
    String getdrivervehicledetails = BaseUrl + "api/v6/app/driver/vehicle/update";
    String removedriver = BaseUrl + "api/v6/app/driver/remove/additional/driver";
    String updateanotherdriver = BaseUrl + "api/v6/app/driver/update/additional/driver";
    String profileemailupdate = BaseUrl + "api/v6/app/driver/update/email";
    String profilemobilechange = BaseUrl + "api/v6/app/driver/update/mobile";
    String profileemailchange = BaseUrl + "api/v6/app/driver/change/email";
    String updatedriverdoc = BaseUrl + "api/v6/app/driver/document/update";
    String warninsapi = BaseUrl + "api/v6/app/get/warnings";
    String driverimageupdate = BaseUrl + "api/v6/app/driver/image/update";
    String getvehicledetails = BaseUrl + "api/v6/app/get-vehicle-details";
    String referrallist = BaseUrl + "api/v6/provider/driver/referral/list";
    String termsurl = MainUrl + "pages/terms-and-conditions";
    String privacyurl = MainUrl + "pages/privacy-and-policy";
    String facebookurl = "https://global.blackmaria.co/facebook-policy";
    String enduserurl = MainUrl + "pages/userpolicy";
}
