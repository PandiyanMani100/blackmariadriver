package com.blackmaria.partner.newdesign;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blackmaria.partner.R;
import com.blackmaria.partner.latestpackageview.widgets.RoundedImageView;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.app.FarePage;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;

public class Saveplus_sender_sendmoney extends AppCompatActivity {

    private RoundedImageView iv_profile;
    private ImageView close;
    private TextView tv_name, tv_saveplusid, tv_ok;
    private EditText tv_enteryourprice, tv_entermoney;
    private SessionManager session;

    private String sDriverID = "", phone_number = "", country_code = "", received_amount = "", total_amount = "", finalmoney = "", name = "", srideid="",saveplusid = "", image = "", ismobilenovisible = "", receiver_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saveplus_sender_sendmoney);


        init();
    }

    private void init() {
        session = new SessionManager(Saveplus_sender_sendmoney.this);
        close = findViewById(R.id.close);
        iv_profile = findViewById(R.id.iv_profile);
        tv_name = findViewById(R.id.tv_name);
        tv_saveplusid = findViewById(R.id.tv_saveplusid);
        tv_ok = findViewById(R.id.tv_ok);
        tv_enteryourprice = findViewById(R.id.tv_enteryourprice);
        tv_entermoney = findViewById(R.id.tv_entermoney);

        HashMap<String, String> user = session.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Saveplus_sender_sendmoney.this, FarePage.class);
                i.putExtra("fromsaveplus","");
                i.putExtra("rideId",srideid);
                startActivity(i);
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });


        receiver_id = getIntent().getStringExtra("receiver_id");
        total_amount = getIntent().getStringExtra("total_amount");
        name = getIntent().getStringExtra("name");
        image = getIntent().getStringExtra("image");
        saveplusid = getIntent().getStringExtra("saveplusid");
        srideid = getIntent().getStringExtra("rideid");

        Picasso.with(Saveplus_sender_sendmoney.this).load(image).memoryPolicy(MemoryPolicy.NO_CACHE).into(iv_profile);
        tv_name.setText(name);
        tv_saveplusid.setText("SAVEPLUS ID " + saveplusid);
        tv_entermoney.setText("");
        tv_enteryourprice.setText(total_amount);


        tv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tv_entermoney.getText().toString().length() == 0) {
                    Alert(getResources().getString(R.string.action_error), "Received amount should not be empty");
                } else if (tv_enteryourprice.getText().toString().length() == 0) {
                    Alert(getResources().getString(R.string.action_error), "Your price amount should not be empty");
                } else {
                    try {
                       int entermoney= Math.round(Float.parseFloat(tv_entermoney.getText().toString().trim()));
                       int yourmoney= Math.round(Float.parseFloat(tv_enteryourprice.getText().toString().trim()));
//                        int totalmoney = entermoney;
//                        int yourmoney = Integer.parseInt(tv_enteryourprice.getText().toString().trim());

                        if (yourmoney < entermoney) {
                            api_transfer_saveplus(Iconstant.Transfersaveplus);
                        } else {
                            Alert(getResources().getString(R.string.action_error), "Received amount shoud be greater than the price amount");
                        }
                    }catch (NumberFormatException e){}
                }
            }
        });
    }

    private void api_transfer_saveplus(String Url) {
        final Dialog dialog = new Dialog(Saveplus_sender_sendmoney.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_processing));
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        System.out.println("-----------Basic profile url--------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("receiver_id", receiver_id);
        jsonParams.put("received_amount", tv_entermoney.getText().toString().trim());
        jsonParams.put("total_amount", total_amount);
        jsonParams.put("driver_id", sDriverID);


        System.out.println("-----------Basic profile jsonParams--------------" + jsonParams);
        ServiceRequest mRequest = new ServiceRequest(Saveplus_sender_sendmoney.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                String Sstatus = "", Smessage = "";
                System.out.println("--------------Basic profile reponse-------------------" + response);

                dialog.dismiss();
                JSONObject object = null;
                try {
                    object = new JSONObject(response);
                    Sstatus = object.getString("status");


                    if (Sstatus.equalsIgnoreCase("1")) {
                        Smessage = object.getJSONObject("response").getString("message");
                        Toast.makeText(Saveplus_sender_sendmoney.this, Smessage, Toast.LENGTH_SHORT).show();
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                Intent i = new Intent(Saveplus_sender_sendmoney.this, FarePage.class);
                                i.putExtra("fromsaveplus","");
                                i.putExtra("rideId",srideid);
                                startActivity(i);
                                finish();
                                overridePendingTransition(R.anim.enter, R.anim.exit);
                            }
                        }, 1500);

                    } else {
                        Smessage = object.getString("response");
                        Alert(getResources().getString(R.string.action_error), Smessage);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(Saveplus_sender_sendmoney.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    @Override
    public void onBackPressed() {

    }
}
