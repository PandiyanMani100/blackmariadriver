package com.blackmaria.partner.Utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import com.blackmaria.partner.latestpackageview.Xmpp.MyXMPP;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;


public class MyActivityLifecycleCallbacks implements Application.ActivityLifecycleCallbacks {

    private String checkLifeCycleStatus = "foreground";
    private SessionManager sessionManager;

    public void onActivityCreated(Activity activity, Bundle bundle) {
    }

    public void onActivityDestroyed(Activity activity) {
    }

    public void onActivityPaused(Activity activity) {
    }

    public void onActivityResumed(Activity activity) {
        if (!isAppIsInBackground(activity)) {
            if (checkLifeCycleStatus.equalsIgnoreCase("foreground")) {
                available(activity);
                checkLifeCycleStatus = "background";
            }
        }
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
    }

    public void onActivityStarted(Activity activity) {
    }

    public void onActivityStopped(Activity activity) {
        if (isAppIsInBackground(activity)) {
            if (checkLifeCycleStatus.equalsIgnoreCase("background")) {
                unAvailable(activity);
                checkLifeCycleStatus = "foreground";
            }
        }
    }


    private void available(Activity activity) {

        sessionManager = new SessionManager(activity);

        HashMap<String, String> state = sessionManager.getXmppServiceState();
        String sState = state.get(SessionManager.KEY_XMPP_SERVICE_RESTART_STATE);

        HashMap<String, String> updatelocationState = sessionManager.getUpdateLocationServiceState();
        String sUpdatelocationState = updatelocationState.get(SessionManager.KEY_UPDATE_LOCATION_SERVICE_RESTART_STATE);
        HashMap<String, String> domain = sessionManager.getXmpp();
        String ServiceName = domain.get(SessionManager.KEY_HOST_NAME);
        String HostAddress = domain.get(SessionManager.KEY_HOST_URL);

        HashMap<String, String> user = sessionManager.getUserDetails();
        String USERNAME = user.get(SessionManager.KEY_DRIVERID);
        String PASSWORD = user.get(SessionManager.KEY_SEC_KEY);

        if(sessionManager.getUserloggedIn()) {
            MyXMPP xmpp = MyXMPP.getInstance(activity, ServiceName, HostAddress, USERNAME, PASSWORD);
            try {
                xmpp.connect("onCreate");
            } catch (IOException e) {
                e.printStackTrace();
            } catch (XMPPException e) {
                e.printStackTrace();
            } catch (SmackException e) {
                e.printStackTrace();
            }
        }

        ChatAvailabilityCheck chatAvailability = new ChatAvailabilityCheck(activity, "available");
        chatAvailability.postChatRequest();
        sessionManager.Setappstatus(true);
    }


    private void unAvailable(Activity activity) {

        ChatAvailabilityCheck chatAvailability = new ChatAvailabilityCheck(activity, "unavailable");
        chatAvailability.postChatRequest();
        sessionManager.Setappstatus(false);
    }


    private boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            if(runningProcesses!=null) {
                if (runningProcesses.size() > 0) {
                    for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                        if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                            for (String activeProcess : processInfo.pkgList) {
                                if (activeProcess.equals(context.getPackageName())) {
                                    isInBackground = false;
                                }
                            }
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }


    private boolean isMyServiceRunning(Class<?> serviceClass, Activity activity) {
        boolean b = false;
        ActivityManager manager = (ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                b = true;
                break;
            } else {
                b = false;
            }
        }
        return b;
    }


}
