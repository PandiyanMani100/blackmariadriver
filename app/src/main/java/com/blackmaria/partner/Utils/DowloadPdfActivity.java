package com.blackmaria.partner.Utils;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import androidx.annotation.RequiresApi;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Toast;


import com.blackmaria.partner.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Random;

/**
 * Created by Muruganantham on 3/14/2018.
 */

public class DowloadPdfActivity {

    private static final String TAG = "Download Task";
    private Context context;

    private String downloadUrl = "", downloadFileName = "", pdfFileName = "";
    private Dialog progressDialog;
    private View pdfView;
    downloadCompleted downloadcompleted;

    public DowloadPdfActivity(Context context, String downloadUrl) {
        this.context = context;

        this.downloadUrl = downloadUrl;


        downloadFileName = downloadUrl.substring(downloadUrl.lastIndexOf('/'), downloadUrl.length());//Create file name by picking download file name from URL
        Log.e(TAG, downloadFileName);

        //Start Downloading Task
        new DownloadingTask().execute();
    }


    public DowloadPdfActivity(Context context, View v, String fileNAme, downloadCompleted downloadcompleted) {
        this.context = context;
        //Start Downloading Task
        this.pdfView = v;
        this.pdfFileName = fileNAme;
        this.downloadcompleted = downloadcompleted;
        new DownloadingTask1().execute();
    }

    private class DownloadingTask1 extends AsyncTask<Void, Void, Void> {


        Bitmap b;
        String path;
        String fileName="";
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new Dialog(context);
            progressDialog.getWindow();
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setContentView(R.layout.downloadpdf);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Void result) {
            try {
                progressDialog.dismiss();
                downloadcompleted.OnDownloadComplete(fileName);
                Toast.makeText(context, "Downloaded Successfully", Toast.LENGTH_SHORT).show();

            } catch (Exception e) {
                e.printStackTrace();

                //Change button text if exception occurs

                Log.e(TAG, "Download Failed with Exception - " + e.getLocalizedMessage());

            }

            super.onPostExecute(result);
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                pdfView.setDrawingCacheEnabled(true);
                pdfView.buildDrawingCache();
                b = pdfView.getDrawingCache();

                PdfDocument document = new PdfDocument();
                PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(b.getWidth(), b.getHeight(), 1).create();
                PdfDocument.Page page = document.startPage(pageInfo);

                Canvas canvas = page.getCanvas();


                Paint paint = new Paint();
                paint.setColor(Color.parseColor("#ffffff"));
                canvas.drawPaint(paint);


                Bitmap bitmap = Bitmap.createScaledBitmap(b, b.getWidth(), b.getHeight(), true);

                paint.setColor(Color.BLUE);
                canvas.drawBitmap(bitmap, 0, 0, null);
                document.finishPage(page);

                File folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Signature/");

                if (!folder.exists()) {
                    folder.mkdir();
                }

                // create instance of Random class
                Random rand = new Random();
                // Generate random integers in range 0 to 999
                int randomNumeber = rand.nextInt(1000000);

                // Print random integers
                System.out.println("Random Integers: "+randomNumeber);


                path = folder.getAbsolutePath();
                // path = path + "/"+Smode  + System.currentTimeMillis() + ".pdf";
                fileName = pdfFileName+randomNumeber + ".pdf";
                File filePath = new File(path, fileName);
                try {
                    document.writeTo(new FileOutputStream(filePath));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                document.close();
            } catch (Exception e) {
                //Read exception if something went wrong
                e.printStackTrace();
                Log.e(TAG, "Download Error Exception " + e.getMessage());
            }

            return null;
        }
    }

    private class DownloadingTask extends AsyncTask<Void, Void, Void> {

        File apkStorage = null;
        File outputFile = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new Dialog(context);
            progressDialog.getWindow();
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setContentView(R.layout.downloadpdf);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Void result) {
            try {
                if (outputFile != null) {
                    progressDialog.dismiss();
                    Toast.makeText(context, "Downloaded Successfully", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            viewPDF(downloadFileName);
                        }
                    }, 1000);


                } else {

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                        }
                    }, 3000);

                    Log.e(TAG, "Download Failed");

                }
            } catch (Exception e) {
                e.printStackTrace();

                //Change button text if exception occurs

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                    }
                }, 3000);
                Log.e(TAG, "Download Failed with Exception - " + e.getLocalizedMessage());

            }


            super.onPostExecute(result);
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                URL url = new URL(downloadUrl);//Create Download URl
                HttpURLConnection c = (HttpURLConnection) url.openConnection();//Open Url Connection
                c.setRequestMethod("GET");//Set Request Method to "GET" since we are grtting data
                c.connect();//connect the URL Connection

                //If Connection response is not OK then show Logs
                if (c.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    Log.e(TAG, "Server returned HTTP " + c.getResponseCode()
                            + " " + c.getResponseMessage());

                }


                //Get File if SD card is present
                if (isSDCardPresent()) {

                    apkStorage = new File(
                            Environment.getExternalStorageDirectory() + "/"
                                    + "Cloud Wallet files");
                } else
                    Toast.makeText(context, "Oops!! There is no SD Card.", Toast.LENGTH_SHORT).show();

                //If File is not present create directory
                if (!apkStorage.exists()) {
                    apkStorage.mkdir();
                    Log.e(TAG, "Directory Created.");
                }

                outputFile = new File(apkStorage, downloadFileName);//Create Output file in Main File

                //Create New File if not present
                if (!outputFile.exists()) {
                    outputFile.createNewFile();
                    Log.e(TAG, "File Created");
                }

                FileOutputStream fos = new FileOutputStream(outputFile);//Get OutputStream for NewFile Location

                InputStream is = c.getInputStream();//Get InputStream for connection

                byte[] buffer = new byte[1024];//Set buffer type
                int len1 = 0;//init length
                while ((len1 = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, len1);//Write new file
                }

                //Close all connection after doing task
                fos.close();
                is.close();

            } catch (Exception e) {

                //Read exception if something went wrong
                e.printStackTrace();
                outputFile = null;
                Log.e(TAG, "Download Error Exception " + e.getMessage());
            }

            return null;
        }
    }

    public void viewPDF(String pdfFileName) {
//        File pdfFile = new File(Environment.getExternalStorageDirectory() + "/Cloud Wallet files/" + pdfFileName);  // -> filename = maven.pdf
//        Uri path = Uri.fromFile(pdfFile);
//        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
//        pdfIntent.setDataAndType(path, "application/pdf");
////        pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//
//        try {
//            context.startActivity(pdfIntent);
//        } catch (ActivityNotFoundException e) {
//            Toast.makeText(context, "No Application available to view PDF", Toast.LENGTH_SHORT).show();
//        }


        File pdfFile = new File(Environment.getExternalStorageDirectory() + "/Cloud Wallet files/" + pdfFileName);  // -> filename = maven.pdf
        Uri uri_path = Uri.fromFile(pdfFile);
        Uri uri = uri_path;

        try {
            File file = null;
            String path = uri.toString();// "/mnt/sdcard/FileName.mp3"
            try {
                file = new File(new URI(path));
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            final Intent intent = new Intent(Intent.ACTION_VIEW)//
                    .setDataAndType(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ?
                                    androidx.core.content.FileProvider.getUriForFile(context, context.getPackageName() + ".provider", file) : Uri.fromFile(file),
                            "application/pdf").addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            try {
                context.startActivity(intent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(context, "No Application available to view PDF", Toast.LENGTH_SHORT).show();
            }

        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, "Cannot read file error. ", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean isSDCardPresent() {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            return true;
        }
        return false;
    }


    public  interface downloadCompleted {
        void OnDownloadComplete(String fileName);
    }

}
