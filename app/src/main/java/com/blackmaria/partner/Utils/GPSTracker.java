package com.blackmaria.partner.Utils;

import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;


import com.blackmaria.partner.services.Getlocation;

import static android.content.Context.LOCATION_SERVICE;

public class GPSTracker {

    public static GPSTracker instance = null;

    public Context mContext;
    // Flag for GPS status
    public boolean isGPSEnabled = false;

    // Flag for network status
    boolean isNetworkEnabled = false;

    // Flag for GPS status
    public boolean canGetLocation = false;

    // Declaring a Location Manager
    protected LocationManager locationManager;

    public GPSTracker(Context context) {
        mContext = context;
        mContext.startService(new Intent(mContext, Locationservices.class));
        getLocation();
    }

    public static GPSTracker getInstance(Context context) {
        if (instance == null) {
            instance = new GPSTracker(context);
        }
        return instance;
    }

    private void getLocation() {
        locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);
        // Getting GPS status
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        // Getting network status
        isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (!isGPSEnabled && !isNetworkEnabled) {
            // No network provider is enabled
        } else {
            this.canGetLocation = true;
            this.isGPSEnabled = true;

        }
    }

    public double getLatitude() {
        SessionManager sessionManager = SessionManager.getInstance(mContext);
        // return latitude
        return sessionManager.getLatitude();
    }

    /**
     * Function to get longitude
     */
    public double getLongitude() {
        // return longitude
        SessionManager sessionManager = SessionManager.getInstance(mContext);
        return sessionManager.getLongitude();
    }


    public boolean canGetLocation() {
        return this.canGetLocation;
    }

    public boolean isgpsenabled() {
        return this.isGPSEnabled;
    }
}
