package com.blackmaria.partner.Utils;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.blackmaria.partner.Pojo.MultipleLocationPojo;
import com.blackmaria.partner.latestpackageview.view.signup.SignupAndSignInPage_Constrain;
import com.blackmaria.partner.latestpackageview.Xmpp.MyXMPP;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;

public class SessionManager {
    // Shared Preferences
    static SharedPreferences pref;
    // Editor for Shared preferences
    static Editor editor;
    // Context
    Context _context;
    // Shared pref mode
    int PRIVATE_MODE = 0;
    // Sharedpref file name
    public static final String KEYCURRENTLANGUAGE = "currentlanguage";
    private static final String PREF_NAME = "BlackMariaPartner";

    public static String LOCATION_LATITUDE = "location_latitiude";
    public static String LOCATION_LONGITUDE = "location_longitude";
    public static String complementary_waitingtime = "complementry_waitingtime";


    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";
    // Email address (make variable public to access from outside)
    public static final String KEY_EMAIL = "email";
    public static final String KEY = "key";
    public static final String KEY_DRIVER_NAME = "drivername";
    public static final String KEY_CATEGORYID = "category_id";
    public static final String KEY_DRIVERID = "driverid";
    public static final String KEY_SECONDDRIVERID = "seconddriverid";
    public static final String KEY_ACCOUNTOWNERTYPE = "accountownertype";
    public static final String KEY_DRIVER_IMAGE = "driverimage";
    public static final String KEY_SECONDDRIVER_IMAGE = "seconddriverimage";
    public static final String KEY_VEHICLENO = "vechicleno";
    public static final String KEY_VEHICLE_MODEL = "vehiclemodel";
    public static final String KEY_SEC_KEY = "vehiclemodel";
    public static final String KEY_VEHICLE_NAME = "VEHICLE_NAME";
    public static final String KEY_VEHICLE_NUMBER = "vehicle_numberdriversettings";
    public static final String KEY_ONLINE = "online";
    public static final String KEY_COUNT = "keycount";
    public static final String KEY_GCM_ID = "gcmId";
    public static final String KEY_Language_code = "language_code";
    public static final String KEY_Language = "language";
    public static final String KEY_WAIT = "waittime";
    public static final String KEY_WAIT_STATUS = "wait_status";
    public static final String KEY_STATUS = "ride_status";
    public static final String KEY_id = "ride_id1";
    public static final String KEY_SERVICE_STATUS = "service_status";
    public static final String KEY_ID_NAME = "Id_Name";
    public static final String KEY_VEHICLE_IMAGE = "Vehicle_image";
    public static final String KEY_VEHICLE_BitMap_IMAGE = "Vehicle_Bitmap";
    public static final String KEY_HOST_URL = "xmpphostUrl";
    public static final String KEY_Traffic = "traffic";
    public static final String KEY_HOST_NAME = "xmpphostName";
    public static final String KEY_APP_STATUS = "appStatus";
    public static final String KEY_USER_ID = "user_id";
    public static final String KEY_CAR_MARKER = "car_marker";
    public static final String KEY_USER_GUIDE = "user_guide";
    public static final String KEY_SECURE_PIN = "secure_pin";
    public static final String KEY_PAYMENTBANKCODE = "paymentbankcode";


    public static final String KEY_REG_OTP_STATUS = "otp_status";
    public static final String KEY_REG_OTP_PIN = "otp_pin";
    public static final String KEY_CUSTOMERSERVICE = "CustomerServiceNo";


    //Google Navigation Session
    public static final String KEY_GOOGLE_NAVIGATION_ADDRESS = "googleNav_address";
    public static final String KEY_GOOGLE_NAVIGATION_RIDE_ID = "googleNav_rideId";
    public static final String KEY_GOOGLE_NAVIGATION_PICKUP_LAT = "googleNav_pickUp_lat";
    public static final String KEY_GOOGLE_NAVIGATION_PICKUP_LONG = "googleNav_pickUp_lng";
    public static final String KEY_GOOGLE_NAVIGATION_USERNAME = "googleNav_userName";
    public static final String KEY_GOOGLE_NAVIGATION_USER_RATING = "googleNav_userRating";
    public static final String KEY_GOOGLE_NAVIGATION_PHONE = "googleNav_phone";
    public static final String KEY_GOOGLE_NAVIGATION_USER_IMAGE = "googleNav_userImage";
    public static final String KEY_GOOGLE_NAVIGATION_USER_ID = "googleNav_userId";
    public static final String KEY_GOOGLE_NAVIGATION_DROP_LAT = "googleNav_dropLat";
    public static final String KEY_GOOGLE_NAVIGATION_DROP_LONG = "googleNav_dropLong";
    public static final String KEY_GOOGLE_NAVIGATION_DROP_LOCATION = "googleNav_dropLocation";
    public static final String KEY_GOOGLE_NAVIGATION_TITLE = "googleNav_title";
    public static final String KEY_GOOGLE_NAVIGATION_SUBTITLE = "googleNav_subTitle";
    public static final String KEY_GOOGLE_NAVIGATION_TRIP_STATUS = "googleNav_tripStatus";
    public static final String KEY_GOOGLE_NAVIGATION_ETA_ARRIVAL = "googleNav_etaArrival";
    public static final String KEY_GOOGLE_NAVIGATION_LOCATION = "googleNav_location";
    public static final String KEY_GOOGLE_NAVIGATION_MEMBER_SINCE = "googleNav_memberSince";
    public static final String KEY_GOOGLE_NAVIGATION_GENDER = "googleNav_gender";
    public static final String KEY_GOOGLE_NAVIGATION_ETA_COST = "googleNav_etaCost";
    public static final String KEY_GOOGLE_NAVIGATION_CURRENCY_SYMBOL = "googleNav_currencySymbol";
    public static final String KEY_GOOGLE_NAVIGATION_MULTIPLE_DROP_STATUS = "googleNav_multipleDropStatus";
    public static final String KEY_GOOGLE_NAVIGATION_MULTIPLE_DROP_LOCATION = "googleNav_multipleDrop";
    public static final String KEY_GOOGLE_NAVIGATION_TRIP_MODE = "googleNav_tripMode";
    public static final String KEY_PINCODE = "keypincode";
    public static final String KEY_PATTERN = "savepluspattern_code";
    public static final String KEY_GOOGLE_NAVIGATION_IS_RETURN_AVAILABLE = "googleNav_IsReturnAvailable";
    public static final String KEY_GOOGLE_NAVIGATION_RETURN_STATE = "googleNav_ReturnState";
    public static final String KEY_GOOGLE_NAVIGATION_IS_RETURN_LAT_LNG_AVAILABLE = "googleNav_IsReturnLatLngAvailable";
    public static final String KEY_GOOGLE_NAVIGATION_RETURN_LAT = "googleNav_ReturnLat";
    public static final String KEY_GOOGLE_NAVIGATION_RETURN_LNG = "googleNav_ReturnLng";
    public static final String KEY_GOOGLE_NAVIGATION_RETURN_PICKUP_LAT = "googleNav_Return_PickUpLat";
    public static final String KEY_GOOGLE_NAVIGATION_RETURN_PICKUP_LNG = "googleNav_Return_PickUpLng";
    public static final String KEY_GOOGLE_NAVIGATION_RETURN_PAYMENT_TYPE = "googleNav_Payment_type";
    public static final String KEY_GOOGLE_NAVIGATION_CANCELLED_RIDES = "googleNav_Cancelled_rides";
    public static final String KEY_GOOGLE_NAVIGATION_COMPLETED_RIDES = "googleNav_Completed_rides";
    public static final String KEY_GOOGLE_NAVIGATION_RIDER_TYPE = "googleNav_Rider_type";
    public static final String KEY_EMER_POLICE = "Police";
    public static final String KEY_EMER_FIRE = "Fire";
    public static final String KEY_EMER_AMBULANCE = "Ambulance";
//    public static final String KEY_GOOGLE_NAVIGATION_MEMBER = "googleNav_Member";

    //Restart Xmpp Service
    public static final String KEY_XMPP_SERVICE_RESTART_STATE = "checkState";
    //Restart UpdateLocation Service
    public static final String KEY_UPDATE_LOCATION_SERVICE_RESTART_STATE = "update_location_checkState";

    public static final String KEY_PHONE_MASKING_STATUS = "phoneMasking";

    public static final String KEY_CONTACT_NUMBER = "contact_number";

    public static final String KEY_WAITED_TIME = "waited_time";
    public static final String KEY_WAITED_MINS = "waited_mins";
    public static final String KEY_WAITED_SECS = "waited_secs";
    public static final String KEY_CALENDAR_TIME = "calendar_time";
    public static final String KEY_WAITING_TIME_STATUS = "waiting_time_status";

    public static final String KEY_VEHICLE_TYPE = "vehicle_type";

    public static final String KEY_IMAGESTATUS = "imagesstatus";
    public static final String KEY_ACCESSKEY = "access_key";
    public static final String KEY_SECRETKEY = "secret_key";
    public static final String KEY_BUCKETNAME = "bucket_name";
    public static final String KEY_BUCKETURL = "bucket_url";

    public static final String KEY_ONLINE_START_TIME = "online_start_time";
    public static final String KEY_ONLINE_TIME_STATUS = "online_time_status";

    public static String REDEEM_CODE = "";
    public static final String CURRENCY = "currency_code";
    public static final String XenditKey1 = "XenditKey";
    public static final String reloadtype = "reloadtype";
    public static final String redeemCode = "redeemcode";
    public static final String currencyconversion = "currencyconversion";
    public static final String KEY_WALLET_AMOUNT = "walletAmount";

    public static final String KEY_DRIVER_REVIEW = "review";
    public static final String KEY_IS_REFERALSTATUS = "refalstatus";
    public static final String KEY_XENDITCHARGESTATUS = "xenditchargestatus";
    public static final String KEY_IS_INFO_BADGE = "badgecountdisplay";
    public static final String KEY_APP_LIVE_STATUS = "appstatus";
    public static final String JOB_ID = "demojob";
    public static final String userlogin = "userlogin";
    private String Destination_location = "finaldestination";
    public static final String KEY_IS_VEHICLEDEAILS = "vehicledetailsinfo";
    public static final String isride = "isride";
    public static final String categoryicon = "categoryicon";
    public static final String continuetripjson = "continuetripjson";
    public static final String KEY_CLOSEAMOUNT = "closeamount";
    public static final String KEY_PAYMENTS = "paymentslist";
    public static final String KEY_CASHLOCTIONSLIST = "cashlocationlist";

    public static final String KEY_ACCOUNTNAME = "accountname";
    public static final String KEY_ACCOUNTNUMBER = "accountnumber";
    public static final String KEY_ACCOUNTCODE = "accountcode";
    public static final String KEY_ACCOUNTBANK = "accountbank";
    public static final String KEYMODEUPDATE = "modeupdate";
    public static SessionManager instance = null;

    // Constructor
    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public static SessionManager getInstance(Context context) {
        if (instance == null) {
            instance = new SessionManager(context);
        }
        return instance;
    }

    /**
     * Create login session
     */
    public void createLoginSession(String image, String driverid, String name, String email, String vehicleno,
                                   String vechilemodel, String key, String sec_key, String gcmID) {
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_DRIVER_IMAGE, image);
        editor.putString(KEY_DRIVERID, driverid);
        editor.putString(KEY_DRIVER_NAME, name);
//        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_VEHICLENO, vehicleno);
        editor.putString(KEY_VEHICLE_MODEL, vechilemodel);
        editor.putString(KEY_SEC_KEY, sec_key);
        editor.putString(KEY, key);
        editor.putString(KEY_GCM_ID, gcmID);
        editor.commit();
    }

    public void createSessionOnline(String online) {
        editor.putString(KEY_ONLINE, online);
        editor.commit();
    }

    public void setModeupdate(String modeupdate) {
        editor.putString(KEYMODEUPDATE, modeupdate);
        editor.commit();
    }

    public String getModeupdate() {
        return pref.getString(KEYMODEUPDATE, "");
    }

    public void setInfoBadge(String badge) {
        editor.putString(KEY_IS_INFO_BADGE, badge);
        editor.commit();
    }

    public String getInfoBadge() {
        String badge = "";
        badge = pref.getString(KEY_IS_INFO_BADGE, "");
        ;
        return badge;
    }

    public void setWaitingtime_rideid(String rideid) {
        editor.putString(complementary_waitingtime, rideid);
        editor.commit();
    }

    public void setImagestatus(String st) {
        editor.putString(KEY_IMAGESTATUS, st);
        editor.commit();
    }

    //------UserImage update code-----
    public void setAmazondetails(String imagestatus,String access_key,String secret_key,String bucket_name,String bucket_url) {
        editor.putString(KEY_IMAGESTATUS, imagestatus);
        editor.putString(KEY_ACCESSKEY, access_key);
        editor.putString(KEY_SECRETKEY, secret_key);
        editor.putString(KEY_BUCKETNAME, bucket_name);
        editor.putString(KEY_BUCKETURL, bucket_url);
        editor.commit();
    }

    public String getimagestatus() {
        return pref.getString(KEY_IMAGESTATUS, "");
    }

    public String getaccesskey() {
        return pref.getString(KEY_ACCESSKEY, "");
    }

    public String getsecretkey() {
        return pref.getString(KEY_SECRETKEY, "");
    }

    public String getbucketname() {
        return pref.getString(KEY_BUCKETNAME, "");
    }

    public String getbucketurl() {
        return pref.getString(KEY_BUCKETURL, "");
    }

    public String getWaitingtime_rideid() {
        return pref.getString(complementary_waitingtime, "");
    }


    public void setUserloggedIn(boolean val) {
        editor.putBoolean(userlogin, val);
        editor.commit();
    }

    public boolean getUserloggedIn() {
        return pref.getBoolean(userlogin, false);
    }


    public void setAgent(String userId) {
        editor.putString(KEY_ID_NAME, userId);
        editor.commit();
    }

    public void SetPincode(String code) {
        editor.putString(KEY_PINCODE, code);
        editor.commit();
    }

    public String getPincode() {
        return pref.getString(KEY_PINCODE, "");
    }


    public void Setappstatus(boolean code) {
        editor.putBoolean(KEY_APP_LIVE_STATUS, code);
        editor.commit();
    }

    public boolean getappstatus() {
        return pref.getBoolean(KEY_APP_LIVE_STATUS, false);
    }

    public void setLatitude(String latitude) {
        editor.putString(LOCATION_LATITUDE, latitude);
        editor.commit();
    }

    public Double getLatitude() {
        double value = 0.0;
        String getlati = pref.getString(LOCATION_LATITUDE, "");
        if (getlati.equalsIgnoreCase("")) {
            value = 0.0;
        } else {
            value = Double.parseDouble(getlati);
        }
        return value;
    }


    public void setLongitude(String longitude) {
        editor.putString(LOCATION_LONGITUDE, longitude);
        editor.commit();
    }

    public Double getLongitude() {
        double value = 0.0;
        String getlati = pref.getString(LOCATION_LONGITUDE, "");
        if (getlati.equalsIgnoreCase("")) {
            value = 0.0;
        } else {
            value = Double.parseDouble(getlati);
        }
        return value;

    }


    public void setCustomerServiceNo(String sCustomerServiceNumber) {
        editor.putString(KEY_CUSTOMERSERVICE, sCustomerServiceNumber);
        editor.commit();
    }

    public String getCustomerServiceNo() {
        return pref.getString(KEY_CUSTOMERSERVICE, "");
    }

    //------UserImage update code-----
    public void setDriverImageUpdate(String image) {
        editor.putString(KEY_DRIVER_IMAGE, image);
        editor.commit();
    }

    //------UserImage update code-----
    public String getDriverImageUpdate() {
        return pref.getString(KEY_DRIVER_IMAGE, "");
    }

    //------UserImage update code-----
    public void setsecondDriverImageUpdate(String image) {
        editor.putString(KEY_SECONDDRIVER_IMAGE, image);
        editor.commit();
    }

    //------UserImage update code-----
    public String getsecondDriverImageUpdate() {
        return pref.getString(KEY_SECONDDRIVER_IMAGE, "");
    }

    public void setseonddriverid(String driverid) {
        editor.putString(KEY_SECONDDRIVERID, driverid);
        editor.commit();
    }

    //------UserImage update code-----
    public String getseonddriverid() {
        return pref.getString(KEY_SECONDDRIVERID, "");
    }


    public void setcategory_id(String categoryid) {
        editor.putString(KEY_CATEGORYID, categoryid);
        editor.commit();
    }

    //------UserImage update code-----
    public String getcategory_id() {
        return pref.getString(KEY_CATEGORYID, "");
    }


    public void setaccountownertype(String owner) {
        editor.putString(KEY_ACCOUNTOWNERTYPE, owner);
        editor.commit();
    }

    //------UserImage update code-----
    public String getaccountownertype() {
        return pref.getString(KEY_ACCOUNTOWNERTYPE, "");
    }


    /**
     *
     */
    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_DRIVER_IMAGE, pref.getString(KEY_DRIVER_IMAGE, ""));
        user.put(KEY_DRIVERID, pref.getString(KEY_DRIVERID, ""));
        user.put(KEY_DRIVER_NAME, pref.getString(KEY_DRIVER_NAME, ""));
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, ""));
        user.put(KEY_VEHICLENO, pref.getString(KEY_VEHICLENO, ""));
        user.put(KEY_VEHICLE_MODEL, pref.getString(KEY_VEHICLE_MODEL, ""));
        user.put(KEY, pref.getString(KEY, ""));
        user.put(KEY_GCM_ID, pref.getString(KEY_GCM_ID, ""));
        user.put(KEY_STATUS, pref.getString(KEY_STATUS, ""));
        user.put(KEY_id, pref.getString(KEY_id, ""));
        user.put(KEY_ID_NAME, pref.getString(KEY_ID_NAME, ""));
        user.put(KEY_Language_code, pref.getString(KEY_Language_code, ""));
        user.put(KEY_VEHICLE_IMAGE, pref.getString(KEY_VEHICLE_IMAGE, ""));
        user.put(KEY_VEHICLE_BitMap_IMAGE, pref.getString(KEY_VEHICLE_BitMap_IMAGE, ""));
        user.put(KEY_SEC_KEY, pref.getString(KEY_SEC_KEY, ""));
        user.put(KEY_USER_ID, pref.getString(KEY_USER_ID, ""));

        return user;
    }

    public void setContactEmail(String email) {
        editor.putString(KEY_EMAIL, email);
        editor.commit();
    }

    public String getContactEmail() {
        return pref.getString(KEY_EMAIL, "");
    }


    public HashMap<String, String> getOnlineDetails() {
        HashMap<String, String> online = new HashMap<String, String>();
        online.put(KEY_ONLINE, pref.getString(KEY_ONLINE, ""));
        return online;
    }

    public void setVehicleType(String vehicleType) {
        editor.putString(KEY_VEHICLE_TYPE, vehicleType);
        editor.commit();
    }

    public String getVehicleType() {
        return pref.getString(KEY_VEHICLE_TYPE, "");
    }

    //emergency contact number

    public void setPolice(String vehicleType) {
        editor.putString(KEY_EMER_POLICE, vehicleType);
        editor.commit();
    }

    public String getPolice() {
        return pref.getString(KEY_EMER_POLICE, "");
    }

    public void setFire(String vehicleType) {
        editor.putString(KEY_EMER_FIRE, vehicleType);
        editor.commit();
    }

    public String getFire() {
        return pref.getString(KEY_EMER_FIRE, "");
    }

    public void setAmbulance(String vehicleType) {
        editor.putString(KEY_EMER_AMBULANCE, vehicleType);
        editor.commit();
    }

    public String getAmbulance() {
        return pref.getString(KEY_EMER_AMBULANCE, "");
    }


    public void setCarMarker(String image) {
        editor.putString(KEY_CAR_MARKER, image);
        editor.commit();
    }

    public String getCarMarker() {
        return pref.getString(KEY_CAR_MARKER, "");
    }

    public void setUserGuide(String userGuide) {
        editor.putString(KEY_USER_GUIDE, userGuide);
        editor.commit();
    }

    public String getUserGuide() {
        return pref.getString(KEY_USER_GUIDE, "");
    }

    public void setlamguage(String userId, String secretKey) {
        editor.putString(KEY_Language_code, userId);
        editor.putString(KEY_Language, secretKey);
        editor.commit();
    }

    public HashMap<String, String> getLanaguage() {
        HashMap<String, String> catID = new HashMap<String, String>();
        catID.put(KEY_Language, pref.getString(KEY_Language, ""));
        return catID;
    }

    public HashMap<String, String> getLanaguageCode() {
        HashMap<String, String> catID = new HashMap<String, String>();
        catID.put(KEY_Language_code, pref.getString(KEY_Language_code, ""));
        return catID;
    }

    public void createStatus(String status) {
        editor.putString(KEY_STATUS, status);
        editor.commit();
    }

    public void createid(String status) {
        editor.putString(KEY_id, status);
        editor.commit();
    }

    public void setRequestCount(int count) {
        editor.putInt(KEY_COUNT, count);
        editor.commit();
    }

    public void setVehicleImage(String count) {
        editor.putString(KEY_VEHICLE_IMAGE, count);
        editor.commit();
    }

    public void setTrafficImage(String count) {
        editor.putString(KEY_Traffic, count);
        editor.commit();
    }


    public HashMap<String, String> getTrafficImage() {
        HashMap<String, String> catID = new HashMap<String, String>();
        catID.put(KEY_Traffic, pref.getString(KEY_Traffic, ""));
        return catID;
    }

    public void setVehicle_BitmapImage(String count) {
        editor.putString(KEY_VEHICLE_BitMap_IMAGE, count);
        editor.commit();
    }

    public HashMap<String, String> getBitmapCode() {
        HashMap<String, String> catID = new HashMap<String, String>();
        catID.put(KEY_VEHICLE_BitMap_IMAGE, pref.getString(KEY_VEHICLE_BitMap_IMAGE, ""));
        return catID;
    }


    public void setOtpstatusAndPin(String status, String otpPin) {
        editor.putString(KEY_REG_OTP_STATUS, status);
        editor.putString(KEY_REG_OTP_PIN, otpPin);
        editor.commit();
    }

    public String getOtpPin() {

        return pref.getString(KEY_REG_OTP_PIN, "");
    }

    public HashMap<String, String> getOtpstatusAndPin() {
        HashMap<String, String> otpstatus = new HashMap<String, String>();
        otpstatus.put(KEY_REG_OTP_STATUS, pref.getString(KEY_REG_OTP_STATUS, ""));
        otpstatus.put(KEY_REG_OTP_PIN, pref.getString(KEY_REG_OTP_PIN, ""));
        return otpstatus;
    }


    public HashMap<String, Integer> getRequestCount() {
        HashMap<String, Integer> count = new HashMap<String, Integer>();
        count.put(KEY_COUNT, pref.getInt(KEY_COUNT, 0));
        return count;
    }

    public void createServiceStatus(String status) {
        editor.putString(KEY_SERVICE_STATUS, status);
        editor.commit();
    }

    public HashMap<String, String> getServiceStatus() {
        HashMap<String, String> service = new HashMap<String, String>();
        service.put(KEY_SERVICE_STATUS, pref.getString(KEY_SERVICE_STATUS, ""));
        return service;
    }

    public void setXmpp(String userId, String secretKey) {
        editor.putString(KEY_HOST_URL, userId);
        editor.putString(KEY_HOST_NAME, secretKey);
        editor.commit();
    }

    public HashMap<String, String> getXmpp() {
        HashMap<String, String> code = new HashMap<String, String>();
        code.put(KEY_HOST_URL, pref.getString(KEY_HOST_URL, ""));
        code.put(KEY_HOST_NAME, pref.getString(KEY_HOST_NAME, ""));
        return code;
    }

    public void setbankdetails(String accountname, String accountnumber, String accountcode,String bankname) {
        editor.putString(KEY_ACCOUNTNAME, accountname);
        editor.putString(KEY_ACCOUNTNUMBER, accountnumber);
        editor.putString(KEY_ACCOUNTCODE, accountcode);
        editor.putString(KEY_ACCOUNTBANK, bankname);
        editor.commit();
    }

    public HashMap<String, String> getbankdetails() {
        HashMap<String, String> code = new HashMap<String, String>();
        code.put(KEY_ACCOUNTNAME, pref.getString(KEY_ACCOUNTNAME, ""));
        code.put(KEY_ACCOUNTNUMBER, pref.getString(KEY_ACCOUNTNUMBER, ""));
        code.put(KEY_ACCOUNTCODE, pref.getString(KEY_ACCOUNTCODE, ""));
        code.put(KEY_ACCOUNTBANK, pref.getString(KEY_ACCOUNTBANK, ""));
        return code;
    }

    /* public void setRequestCount(int count){
         editor.putInt(KEY_COUNT, count);
         editor.commit();
     }

     public HashMap<String, Integer> getRequestCount(){
         HashMap<String, Integer> count = new HashMap<String, Integer>();
         count.put(KEY_COUNT, pref.getInt(KEY_COUNT, 0));
         return count;
     }*/
    public void setWaitingStatus(String mSecs) {
        editor.putString(KEY_WAIT_STATUS, mSecs);
        editor.commit();
    }

    public HashMap<String, String> getWaitingStatus() {
        HashMap<String, String> count = new HashMap<String, String>();
        count.put(KEY_WAIT_STATUS, pref.getString(KEY_WAIT_STATUS, ""));
        return count;
    }

    public void setWaitingTime(String mSecs) {
        editor.putString(KEY_WAIT, mSecs);
        editor.commit();
    }

    public HashMap<String, String> getWaitingTime() {
        HashMap<String, String> count = new HashMap<String, String>();
        count.put(KEY_WAIT, pref.getString(KEY_WAIT, ""));
        return count;
    }

    public void setCount(String mSecs) {
        editor.putString(KEY_WAIT, mSecs);
        editor.commit();
    }

    public HashMap<String, String> getCount() {
        HashMap<String, String> count = new HashMap<String, String>();
        count.put(KEY_WAIT, pref.getString(KEY_WAIT, ""));
        return count;
    }

    public void setdrivercarnumber(String name) {
        editor.putString(KEY_VEHICLE_NUMBER, name);
        editor.commit();
    }

    public String getdrivercarnumber() {
        return pref.getString(KEY_VEHICLE_NUMBER, "");
    }

    public void setUserVehicle(String name) {
        editor.putString(KEY_VEHICLE_NAME, name);
        editor.commit();
    }

    public void setDriverReview(String name) {
        editor.putString(KEY_DRIVER_REVIEW, name);
        editor.commit();
    }

    public String getDriverReview() {
        return pref.getString(KEY_DRIVER_REVIEW, "0");
    }

    //------string App Status----
    public void setAppStatus(String state) {
        editor.putString(KEY_APP_STATUS, state);
        editor.commit();
    }

    public void setaccountcloseamount(String image) {
        editor.putString(KEY_CLOSEAMOUNT, image);
        editor.commit();
    }

    //------UserImage update code-----
    public String getaccountcloseamount() {
        return pref.getString(KEY_CLOSEAMOUNT, "");
    }

    //-----------Get App Status-----
    public HashMap<String, String> getAppStatus() {
        HashMap<String, String> code = new HashMap<String, String>();
        code.put(KEY_APP_STATUS, pref.getString(KEY_APP_STATUS, ""));
        return code;
    }

    public String getUserVehicle() {
        return pref.getString(KEY_VEHICLE_NAME, "");
    }


    public void setXmppServiceState(String state) {
        editor.putString(KEY_XMPP_SERVICE_RESTART_STATE, state);
        editor.commit();
    }

    public HashMap<String, String> getXmppServiceState() {
        HashMap<String, String> xmppState = new HashMap<String, String>();
        xmppState.put(KEY_XMPP_SERVICE_RESTART_STATE, pref.getString(KEY_XMPP_SERVICE_RESTART_STATE, ""));
        return xmppState;
    }

    public String getReferelStatus() {
        String userGuide = "";
        userGuide = pref.getString(KEY_IS_REFERALSTATUS, "0");
        return userGuide;
    }

    public void setvehicleinfo(String vehicle) {
        editor.putString(KEY_IS_VEHICLEDEAILS, vehicle);
        editor.commit();
    }

    public String getvehicleinfo() {
        String badge = "";
        badge = pref.getString(KEY_IS_VEHICLEDEAILS, "");
        return badge;
    }

    public void setReferelStatus(String phone_masking_status) {
        editor.putString(KEY_IS_REFERALSTATUS, phone_masking_status);
        editor.commit();
    }

    public void setUpdateLocationServiceState(String state) {
        editor.putString(KEY_UPDATE_LOCATION_SERVICE_RESTART_STATE, state);
        editor.commit();
    }

    public HashMap<String, String> getUpdateLocationServiceState() {
        HashMap<String, String> xmppState = new HashMap<String, String>();
        xmppState.put(KEY_UPDATE_LOCATION_SERVICE_RESTART_STATE, pref.getString(KEY_UPDATE_LOCATION_SERVICE_RESTART_STATE, ""));
        return xmppState;
    }

    //------string Google Navigation data----
    public void setGoogleNavigationData(String address, String rideId, String pickuplat, String pickup_long, String username, String userrating, String phoneno, String userimg,
                                        String UserId, String drop_lat, String drop_lon, String drop_location, String title_text, String subtitle_text, String trip_status, String eta_arrival,
                                        String location, String member_since, String user_gender, String est_cost, String sCurrencySymbol, String sMultipleDropStatus, String sTripMode, ArrayList<MultipleLocationPojo> multipleDrop, String IsReturnAvailable, String ReturnState, String IsReturnLatLngAvailable, String ReturnLat, String ReturnLng, String ReturnPickUpLat, String ReturnPickUpLng, String payment_type,
                                        String cancelled_rides, String completed_rides, String rider_type) {
        editor.putString(KEY_GOOGLE_NAVIGATION_ADDRESS, address);
        editor.putString(KEY_GOOGLE_NAVIGATION_RIDE_ID, rideId);
        editor.putString(KEY_GOOGLE_NAVIGATION_PICKUP_LAT, pickuplat);
        editor.putString(KEY_GOOGLE_NAVIGATION_PICKUP_LONG, pickup_long);
        editor.putString(KEY_GOOGLE_NAVIGATION_USERNAME, username);
        editor.putString(KEY_GOOGLE_NAVIGATION_USER_RATING, userrating);
        editor.putString(KEY_GOOGLE_NAVIGATION_PHONE, phoneno);
        editor.putString(KEY_GOOGLE_NAVIGATION_USER_IMAGE, userimg);
        editor.putString(KEY_GOOGLE_NAVIGATION_USER_ID, UserId);
        editor.putString(KEY_GOOGLE_NAVIGATION_DROP_LAT, drop_lat);
        editor.putString(KEY_GOOGLE_NAVIGATION_DROP_LONG, drop_lon);
        editor.putString(KEY_GOOGLE_NAVIGATION_DROP_LOCATION, drop_location);
        editor.putString(KEY_GOOGLE_NAVIGATION_TITLE, title_text);
        editor.putString(KEY_GOOGLE_NAVIGATION_SUBTITLE, subtitle_text);
        editor.putString(KEY_GOOGLE_NAVIGATION_TRIP_STATUS, trip_status);
        editor.putString(KEY_GOOGLE_NAVIGATION_ETA_ARRIVAL, eta_arrival);
        editor.putString(KEY_GOOGLE_NAVIGATION_LOCATION, location);
        editor.putString(KEY_GOOGLE_NAVIGATION_MEMBER_SINCE, member_since);
        editor.putString(KEY_GOOGLE_NAVIGATION_GENDER, user_gender);
        editor.putString(KEY_GOOGLE_NAVIGATION_ETA_COST, est_cost);
        editor.putString(KEY_GOOGLE_NAVIGATION_CURRENCY_SYMBOL, sCurrencySymbol);
        editor.putString(KEY_GOOGLE_NAVIGATION_MULTIPLE_DROP_STATUS, sMultipleDropStatus);
        editor.putString(KEY_GOOGLE_NAVIGATION_TRIP_MODE, sTripMode);

        editor.putString(KEY_GOOGLE_NAVIGATION_IS_RETURN_AVAILABLE, IsReturnAvailable);
        editor.putString(KEY_GOOGLE_NAVIGATION_RETURN_STATE, ReturnState);
        editor.putString(KEY_GOOGLE_NAVIGATION_IS_RETURN_LAT_LNG_AVAILABLE, IsReturnLatLngAvailable);
        editor.putString(KEY_GOOGLE_NAVIGATION_RETURN_LAT, ReturnLat);
        editor.putString(KEY_GOOGLE_NAVIGATION_RETURN_LNG, ReturnLng);
        editor.putString(KEY_GOOGLE_NAVIGATION_RETURN_PICKUP_LAT, ReturnPickUpLat);
        editor.putString(KEY_GOOGLE_NAVIGATION_RETURN_PICKUP_LNG, ReturnPickUpLng);
        editor.putString(KEY_GOOGLE_NAVIGATION_RETURN_PAYMENT_TYPE, payment_type);
        editor.putString(KEY_GOOGLE_NAVIGATION_CANCELLED_RIDES, cancelled_rides);
        editor.putString(KEY_GOOGLE_NAVIGATION_COMPLETED_RIDES, completed_rides);
        editor.putString(KEY_GOOGLE_NAVIGATION_RIDER_TYPE, rider_type);
//        editor.putString(KEY_GOOGLE_NAVIGATION_MEMBER, member);

        Gson gson = new Gson();
        String multipleDrop_json = gson.toJson(multipleDrop);
        editor.putString(KEY_GOOGLE_NAVIGATION_MULTIPLE_DROP_LOCATION, multipleDrop_json);
        editor.commit();
    }


    public HashMap<String, String> getGoogleNavigationData() {
        HashMap<String, String> data = new HashMap<String, String>();
        data.put(KEY_GOOGLE_NAVIGATION_ADDRESS, pref.getString(KEY_GOOGLE_NAVIGATION_ADDRESS, ""));
        data.put(KEY_GOOGLE_NAVIGATION_RIDE_ID, pref.getString(KEY_GOOGLE_NAVIGATION_RIDE_ID, ""));
        data.put(KEY_GOOGLE_NAVIGATION_PICKUP_LAT, pref.getString(KEY_GOOGLE_NAVIGATION_PICKUP_LAT, ""));
        data.put(KEY_GOOGLE_NAVIGATION_PICKUP_LONG, pref.getString(KEY_GOOGLE_NAVIGATION_PICKUP_LONG, ""));
        data.put(KEY_GOOGLE_NAVIGATION_USERNAME, pref.getString(KEY_GOOGLE_NAVIGATION_USERNAME, ""));
        data.put(KEY_GOOGLE_NAVIGATION_USER_RATING, pref.getString(KEY_GOOGLE_NAVIGATION_USER_RATING, ""));
        data.put(KEY_GOOGLE_NAVIGATION_PHONE, pref.getString(KEY_GOOGLE_NAVIGATION_PHONE, ""));
        data.put(KEY_GOOGLE_NAVIGATION_USER_IMAGE, pref.getString(KEY_GOOGLE_NAVIGATION_USER_IMAGE, ""));
        data.put(KEY_GOOGLE_NAVIGATION_USER_ID, pref.getString(KEY_GOOGLE_NAVIGATION_USER_ID, ""));
        data.put(KEY_GOOGLE_NAVIGATION_DROP_LAT, pref.getString(KEY_GOOGLE_NAVIGATION_DROP_LAT, ""));
        data.put(KEY_GOOGLE_NAVIGATION_DROP_LONG, pref.getString(KEY_GOOGLE_NAVIGATION_DROP_LONG, ""));
        data.put(KEY_GOOGLE_NAVIGATION_DROP_LOCATION, pref.getString(KEY_GOOGLE_NAVIGATION_DROP_LOCATION, ""));
        data.put(KEY_GOOGLE_NAVIGATION_TITLE, pref.getString(KEY_GOOGLE_NAVIGATION_TITLE, ""));
        data.put(KEY_GOOGLE_NAVIGATION_SUBTITLE, pref.getString(KEY_GOOGLE_NAVIGATION_SUBTITLE, ""));
        data.put(KEY_GOOGLE_NAVIGATION_TRIP_STATUS, pref.getString(KEY_GOOGLE_NAVIGATION_TRIP_STATUS, ""));
        data.put(KEY_GOOGLE_NAVIGATION_ETA_ARRIVAL, pref.getString(KEY_GOOGLE_NAVIGATION_ETA_ARRIVAL, ""));
        data.put(KEY_GOOGLE_NAVIGATION_LOCATION, pref.getString(KEY_GOOGLE_NAVIGATION_LOCATION, ""));
        data.put(KEY_GOOGLE_NAVIGATION_MEMBER_SINCE, pref.getString(KEY_GOOGLE_NAVIGATION_MEMBER_SINCE, ""));
        data.put(KEY_GOOGLE_NAVIGATION_GENDER, pref.getString(KEY_GOOGLE_NAVIGATION_GENDER, ""));
        data.put(KEY_GOOGLE_NAVIGATION_ETA_COST, pref.getString(KEY_GOOGLE_NAVIGATION_ETA_COST, ""));
        data.put(KEY_GOOGLE_NAVIGATION_CURRENCY_SYMBOL, pref.getString(KEY_GOOGLE_NAVIGATION_CURRENCY_SYMBOL, ""));
        data.put(KEY_GOOGLE_NAVIGATION_MULTIPLE_DROP_STATUS, pref.getString(KEY_GOOGLE_NAVIGATION_MULTIPLE_DROP_STATUS, ""));
        data.put(KEY_GOOGLE_NAVIGATION_MULTIPLE_DROP_LOCATION, pref.getString(KEY_GOOGLE_NAVIGATION_MULTIPLE_DROP_LOCATION, ""));
        data.put(KEY_GOOGLE_NAVIGATION_TRIP_MODE, pref.getString(KEY_GOOGLE_NAVIGATION_TRIP_MODE, ""));

        data.put(KEY_GOOGLE_NAVIGATION_IS_RETURN_AVAILABLE, pref.getString(KEY_GOOGLE_NAVIGATION_IS_RETURN_AVAILABLE, ""));
        data.put(KEY_GOOGLE_NAVIGATION_RETURN_STATE, pref.getString(KEY_GOOGLE_NAVIGATION_RETURN_STATE, ""));
        data.put(KEY_GOOGLE_NAVIGATION_IS_RETURN_LAT_LNG_AVAILABLE, pref.getString(KEY_GOOGLE_NAVIGATION_IS_RETURN_LAT_LNG_AVAILABLE, ""));
        data.put(KEY_GOOGLE_NAVIGATION_RETURN_LAT, pref.getString(KEY_GOOGLE_NAVIGATION_RETURN_LAT, ""));
        data.put(KEY_GOOGLE_NAVIGATION_RETURN_LNG, pref.getString(KEY_GOOGLE_NAVIGATION_RETURN_LNG, ""));
        data.put(KEY_GOOGLE_NAVIGATION_RETURN_PICKUP_LAT, pref.getString(KEY_GOOGLE_NAVIGATION_RETURN_PICKUP_LAT, ""));
        data.put(KEY_GOOGLE_NAVIGATION_RETURN_PICKUP_LNG, pref.getString(KEY_GOOGLE_NAVIGATION_RETURN_PICKUP_LNG, ""));
        data.put(KEY_GOOGLE_NAVIGATION_RETURN_PAYMENT_TYPE, pref.getString(KEY_GOOGLE_NAVIGATION_RETURN_PAYMENT_TYPE, ""));
        data.put(KEY_GOOGLE_NAVIGATION_CANCELLED_RIDES, pref.getString(KEY_GOOGLE_NAVIGATION_CANCELLED_RIDES, ""));
        data.put(KEY_GOOGLE_NAVIGATION_COMPLETED_RIDES, pref.getString(KEY_GOOGLE_NAVIGATION_COMPLETED_RIDES, ""));
        data.put(KEY_GOOGLE_NAVIGATION_RIDER_TYPE, pref.getString(KEY_GOOGLE_NAVIGATION_RIDER_TYPE, ""));
//        data.put(KEY_GOOGLE_NAVIGATION_MEMBER, pref.getString(KEY_GOOGLE_NAVIGATION_MEMBER, ""));

        return data;
    }

    public void setpattern(String userId) {
        editor.putString(KEY_PATTERN, userId);
        editor.commit();
    }

    public HashMap<String, String> getpattern() {
        HashMap<String, String> catID = new HashMap<String, String>();
        catID.put(KEY_PATTERN, pref.getString(KEY_PATTERN, ""));
        return catID;
    }


    public void createuserid(String status) {
        editor.putString(KEY_USER_ID, status);
        editor.commit();
    }

    public String getPhoneMaskingStatus() {
        String masking = "";
        masking = pref.getString(KEY_PHONE_MASKING_STATUS, "");
        return masking;
    }

    public void setPhoneMaskingDetail(String phoneMasking) {
        editor.putString(KEY_PHONE_MASKING_STATUS, phoneMasking);
        editor.commit();
    }

    public void setContactNumber(String phoneMasking) {
        editor.putString(KEY_CONTACT_NUMBER, phoneMasking);
        editor.commit();
    }

    public String getContactNumber() {
        return pref.getString(KEY_CONTACT_NUMBER, "");
    }


    public void setSecurePin(String pin) {
        editor.putString(KEY_SECURE_PIN, pin);
        editor.commit();
    }

    public String getSecurePin() {
        return pref.getString(KEY_SECURE_PIN, "");
    }

    public void setpaymentcode(String pin) {
        editor.putString(KEY_PAYMENTBANKCODE, pin);
        editor.commit();
    }

    public String getpaymentcode() {
        return pref.getString(KEY_PAYMENTBANKCODE, "");
    }


    /**
     * Clear session details
     */

    public void logoutUser() {
        editor.clear();
        editor.commit();
        setUserloggedIn(false);

        Intent i = new Intent(_context, SignupAndSignInPage_Constrain.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        _context.startActivity(i);

        SessionManager sessionManager = SessionManager.getInstance(_context);
        HashMap<String, String> domain = sessionManager.getXmpp();
        String ServiceName = domain.get(SessionManager.KEY_HOST_NAME);
        String HostAddress = domain.get(SessionManager.KEY_HOST_URL);
        HashMap<String, String> user = sessionManager.getUserDetails();
        String USERNAME = user.get(SessionManager.KEY_DRIVERID);
        String PASSWORD = user.get(SessionManager.KEY_SEC_KEY);


        MyXMPP myXMPP = MyXMPP.getInstance(_context, ServiceName, HostAddress, USERNAME, PASSWORD);
        myXMPP.disconnect();

    }

    /**
     * Quick check for login
     * *
     */
    // Get Login State
    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }

    public void setWaitedTime(long mins, long secs, String waitedTime, String calendarTime) {
        editor.putString(KEY_WAITED_MINS, String.valueOf(mins));
        editor.putString(KEY_WAITED_SECS, String.valueOf(secs));
        editor.putString(KEY_WAITED_TIME, waitedTime);
        editor.putString(KEY_CALENDAR_TIME, calendarTime);
        editor.commit();
    }

    public HashMap<String, String> getWaitedTime() {
        HashMap<String, String> mapWaitingTime = new HashMap<String, String>();
        mapWaitingTime.put(KEY_WAITED_MINS, pref.getString(KEY_WAITED_MINS, ""));
        mapWaitingTime.put(KEY_WAITED_SECS, pref.getString(KEY_WAITED_SECS, ""));
        mapWaitingTime.put(KEY_WAITED_TIME, pref.getString(KEY_WAITED_TIME, ""));
        mapWaitingTime.put(KEY_CALENDAR_TIME, pref.getString(KEY_CALENDAR_TIME, ""));
        return mapWaitingTime;
    }

    public void setWaitStatus(String status) {
        editor.putString(KEY_WAITING_TIME_STATUS, status);
        editor.commit();
    }

    public String getWaitStatus() {
        return pref.getString(KEY_WAITING_TIME_STATUS, "");
    }

    public void setOnlineStartTime(String time) {
        editor.putString(KEY_ONLINE_START_TIME, time);
        editor.commit();
    }

    public String getOnlineStartTime() {
        return pref.getString(KEY_ONLINE_START_TIME, "");
    }

    public void setOnlineTimeStatus(String status) {
        editor.putString(KEY_ONLINE_TIME_STATUS, status);
        editor.commit();
    }

    public String getOnlineTimeStatus() {
        return pref.getString(KEY_ONLINE_TIME_STATUS, "");
    }

    public void setXenditPublicKey(String XenditKey) {
        editor.putString(XenditKey1, XenditKey);
        editor.commit();
    }

    public String getXenditPublicKey() {
        String currency = "";
        currency = pref.getString(XenditKey1, "");
        return currency;
    }

    public void setReloadPaymentType(String type) {
        editor.putString(reloadtype, type);
        editor.commit();
    }

    public String getReloadPaymentType() {
        String type = "";
        type = pref.getString(reloadtype, "");
        return type;
    }

    public void setCurrency(String currency) {
        editor.putString(CURRENCY, currency);
        editor.commit();
    }

    public String getCurrency() {
        String currency = "";
        currency = pref.getString(CURRENCY, "");
        return currency;
    }

    public void setpaymentlist(String currency) {
        editor.putString(KEY_PAYMENTS, currency);
        editor.commit();
    }

    public String getpaymentlist() {
        String currency = "";
        currency = pref.getString(KEY_PAYMENTS, "");
        return currency;
    }


    public void setcashlocationlist(String currency) {
        editor.putString(KEY_CASHLOCTIONSLIST, currency);
        editor.commit();
    }

    public String getcashlocationlist() {
        String currency = "";
        currency = pref.getString(KEY_CASHLOCTIONSLIST, "");
        return currency;
    }

    public void createWalletAmount(String amount) {
        editor.putString(KEY_WALLET_AMOUNT, amount);
        // commit changes
        editor.commit();
    }

    public HashMap<String, String> getWalletAmount() {
        HashMap<String, String> code = new HashMap<String, String>();
        code.put(KEY_WALLET_AMOUNT, pref.getString(KEY_WALLET_AMOUNT, ""));
        return code;
    }

    public void setCurrentlanguage(String lan) {
        editor.putString(KEYCURRENTLANGUAGE, lan);
        editor.commit();
    }

    public String getCurrentlanguage() {
        return pref.getString(KEYCURRENTLANGUAGE, "en");
    }


    public void setcurrencyconversionKey(String XenditKey) {
        editor.putString(currencyconversion, XenditKey);
        editor.commit();
    }

    public String getcurrencyconversionKey() {
        String currency = "";
        currency = pref.getString(currencyconversion, "");
        return currency;
    }

    public void setXenditChargeStatus(String XenditKey) {
        editor.putString(KEY_XENDITCHARGESTATUS, XenditKey);
        editor.commit();
    }

    public String getXenditChargeStatus() {
        String currency = "";
        currency = pref.getString(KEY_XENDITCHARGESTATUS, "");
        return currency;
    }


    @SuppressLint("WrongConstant")
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        boolean b = false;
        ActivityManager manager = (ActivityManager) _context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                System.out.println("1 already running");
                b = true;
                break;
            } else {
                System.out.println("2 not running");
                b = false;
            }
        }
        System.out.println("3 not running");
        return b;
    }

    public void setjobid(int job) {
        editor.putInt(JOB_ID, job);
        editor.commit();
    }

    public int getjobid() {
        return pref.getInt(JOB_ID, 0);

    }


    public void setfinaldestinaltion(String location) {
        editor.putString(Destination_location, location);
        editor.commit();
    }

    public String getfinaldestinaltion() {
        return pref.getString(Destination_location, "");

    }

    public void setisride(String rideid) {
        editor.putString(isride, rideid);
        editor.commit();
    }

    public String getisride() {
        return pref.getString(isride, "");

    }

    public void setcategoryicon(String icon) {
        editor.putString(categoryicon, icon);
        editor.commit();
    }

    public String getcategoryicon() {
        return pref.getString(categoryicon, "");

    }

    public void setContinuetripjson(String icon) {
        editor.putString(continuetripjson, icon);
        editor.commit();
    }

    public String getContinuetripjson() {
        return pref.getString(continuetripjson, "");

    }


}