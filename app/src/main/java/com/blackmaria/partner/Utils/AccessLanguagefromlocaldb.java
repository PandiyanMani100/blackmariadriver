package com.blackmaria.partner.Utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.blackmaria.partner.app.LanguageDb;

public class AccessLanguagefromlocaldb {

    private Context _context;
    public static AccessLanguagefromlocaldb instance = null;
    private LanguageDb mHelper;

    public AccessLanguagefromlocaldb(Context context) {
        this._context = context;
        this.mHelper=new LanguageDb(_context);
    }

    public static AccessLanguagefromlocaldb getInstance(Context context){
        if(instance == null){
            instance = new AccessLanguagefromlocaldb(context);
        }
        return instance;
    }

    public String getvalue(String key) {
        return mHelper.getvalueforkey(key);
    }
}