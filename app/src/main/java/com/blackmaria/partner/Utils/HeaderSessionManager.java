package com.blackmaria.partner.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;


/**
 * Created by Prem Kumar and Anitha on 12/20/2016.
 */

public class HeaderSessionManager
{

    // Shared Preferences
    static SharedPreferences pref;
    // Editor for Shared preferences
    static SharedPreferences.Editor editor;
    // Context
    Context _context;
    // Shared pref mode
    int PRIVATE_MODE = 0;
    // Sharedpref file name
    private static final String PREF_NAME = "HeaderPremSessionPref";

    // All Shared Preferences Keys
    public static final String KEY_ID_NAME = "Id_Name";
    public static final String KEY_LANGUAGE_CODE = "language_code";


    public HeaderSessionManager(Context context) {
        this._context = context;
        if(pref == null && editor == null) {
            pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
            editor = pref.edit();
        }
    }

    public void createHeaderSession( String Auth, String languageCode) {
        editor.putString(KEY_ID_NAME, Auth);
        editor.putString(KEY_LANGUAGE_CODE, languageCode);
        editor.commit();
    }


    public HashMap<String, String> getHeaderSession() {
        HashMap<String, String> header = new HashMap<String, String>();
        header.put(KEY_ID_NAME, pref.getString(KEY_ID_NAME, ""));
        header.put(KEY_LANGUAGE_CODE, pref.getString(KEY_LANGUAGE_CODE, ""));
        return header;
    }

}
