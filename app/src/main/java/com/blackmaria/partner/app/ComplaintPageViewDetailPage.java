package com.blackmaria.partner.app;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;

import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.Interface.ReplyCloseInterface;
import com.blackmaria.partner.Pojo.ViewComplaintPojo;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.adapter.ComplaintPageViewDetailAdapter;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.CustomTextView;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.devspark.appmsg.AppMsg;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user144 on 3/9/2018.
 */

@SuppressLint("Registered")
public class ComplaintPageViewDetailPage extends ActivityHockeyApp implements ReplyCloseInterface {
    private CustomTextView tv_reply;
    private ImageView back;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private SessionManager session;
    private String UserID = "";
    private ServiceRequest mRequest;
    private Dialog dialog;
    private ListView listview;
    private ArrayList<ViewComplaintPojo> itemlist;
    private ComplaintPageViewDetailAdapter adapter;
    private boolean isCommentAvailable = false;
    private String Sticket_no = "", Sticket_date = "", Ssubject = "", Sbooking_no = "", Sticket_status = "";
    private String SticketId = "";
    private TextView ticket_status, tv_ticket_no, ticketdate, complaint_sub;
    //tv_you_wrote,tv_blackmaria_wrote

    private Dialog success_dialog;
    private String phone_number = "";
    final int PERMISSION_REQUEST_CODE = 111;
    final int PERMISSION_REQUEST_CODES = 222;
    private TextView callDriver;
    private String ticketStatus = "";
    boolean isFromReply = true;
    boolean isfromClompalintlist= false;
    AccessLanguagefromlocaldb db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
        setContentView(R.layout.complaint_reply_close);
        initialize();

        if(getIntent().hasExtra("frompage")){
            isfromClompalintlist = true;
        }
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isfromClompalintlist){
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    finish();
                }else{
                    Intent intent = new Intent(ComplaintPageViewDetailPage.this, ComplaintPage.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    finish();
                }

            }
        });

        callDriver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (phone_number != null) {
//                    HashMap<String, String> phone = session.getPhoneMaskingStatus();
                    String sPhoneMaskingStatus = session.getPhoneMaskingStatus();
//                    sPhoneMaskingStatus = "No";
                    if (sPhoneMaskingStatus.equalsIgnoreCase("Yes")) {
                        postRequest_PhoneMasking(Iconstant.phoneMasking_url);
                    } else {
                        if (Build.VERSION.SDK_INT >= 23) {
                            // Marshmallow+
                            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone_number));
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone_number));
                            startActivity(intent);

                        }
                    }

                } else {
                    Alert(db.getvalue("action_error"), db.getvalue("inavid_call"));
                }
            }
        });


    }

    private void AlertUser(String string, String alert) {
        final PkDialog mDialog = new PkDialog(ComplaintPageViewDetailPage.this);
        mDialog.setDialogTitle(string);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(db.getvalue("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void initialize() {
        session = new SessionManager(ComplaintPageViewDetailPage.this);
        cd = new ConnectionDetector(ComplaintPageViewDetailPage.this);
        isInternetPresent = cd.isConnectingToInternet();
        itemlist = new ArrayList<ViewComplaintPojo>();

        TextView complaint_text = (TextView) findViewById(R.id.complaint_text);
        complaint_text.setText(db.getvalue("call_user"));

        back = (ImageView) findViewById(R.id.complaintback);
        listview = (ListView) findViewById(R.id.view_complaint_page_comments_listview);
        tv_ticket_no = (TextView) findViewById(R.id.view_complaint_page_ticket_no_textview);
        ticket_status = (TextView) findViewById(R.id.ticket_status);
        ticketdate = (TextView) findViewById(R.id.ticketdate);
        complaint_sub = (TextView) findViewById(R.id.complaint_sub);
        callDriver = (TextView) findViewById(R.id.value_reporttext);


        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_DRIVERID);

        Intent intent = getIntent();
        SticketId = intent.getStringExtra("ticket_id");
        ticketStatus = intent.getStringExtra("ticket_Status");
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", UserID);
        jsonParams.put("ticket_id", SticketId);

        if (isInternetPresent) {
            postRequest_viewTicket(Iconstant.view_ticket_url, jsonParams);
        } else {
            Alert(db.getvalue("alert_label_title"), db.getvalue("alert_nointernet"));
        }
    }

    private void postRequest_PhoneMasking(String Url) {

        dialog = new Dialog(ComplaintPageViewDetailPage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(db.getvalue("action_pleasewait"));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("ride_id", Sbooking_no);
        jsonParams.put("user_type", "user");

        System.out.println("-------------PhoneMasking Url----------------" + Url);
        mRequest = new ServiceRequest(ComplaintPageViewDetailPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------PhoneMasking Response----------------" + response);

                String Str_status = "", SResponse = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");
                    SResponse = object.getString("response");
                    if (Str_status.equalsIgnoreCase("1")) {
                        Alert(db.getvalue("action_success"), SResponse);
                    } else {
                        AlertUser(db.getvalue("action_error"), SResponse);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    dialog.dismiss();
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    //-----------------------viewTicket Post Request-----------------
    private void postRequest_viewTicket(String Url, HashMap<String, String> jsonParams) {
        dialog = new Dialog(ComplaintPageViewDetailPage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(db.getvalue("action_pleasewait"));
        System.out.println("-------------viewTicket Url----------------" + Url);

        System.out.println("-------------viewTicket jsonParams----------------" + jsonParams);
        mRequest = new ServiceRequest(ComplaintPageViewDetailPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------viewTicket Response----------------" + response);
                String Sstatus = "", driver_image = "", user_image = "";
                ;
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        if (response_object.length() > 0) {


                            JSONObject details_object = response_object.getJSONObject("details");
                            if (details_object.length() > 0) {

                                Sticket_no = details_object.getString("ticket_no");
                                Sticket_date = details_object.getString("ticket_date");
                                Ssubject = details_object.getString("subject");
                                Sbooking_no = details_object.getString("ride_id");
                                Sticket_status = details_object.getString("ticket_status");

                                driver_image = details_object.getString("driver_image");
                                user_image = details_object.getString("user_image");
                                phone_number = details_object.getString("phone_number");


                                JSONArray comment_by_array = details_object.getJSONArray("comments");
                                if (comment_by_array.length() > 0) {
                                    itemlist.clear();
                                    for (int i = 0; i < comment_by_array.length(); i++) {
                                        JSONObject comment_by_object = comment_by_array.getJSONObject(i);
                                        ViewComplaintPojo pojo = new ViewComplaintPojo();
                                        pojo.setComment_by(comment_by_object.getString("comment_by"));
                                        pojo.setComments(comment_by_object.getString("comments"));
                                        pojo.setComment_date(comment_by_object.getString("comment_date"));
                                        pojo.setType(comment_by_object.getString("type"));
                                        itemlist.add(pojo);
                                    }
                                    isCommentAvailable = true;
                                } else {
                                    isCommentAvailable = false;
                                }

                            }

                        }
                    } else {
                        String Sresponse = object.getString("response");
                        Alert(db.getvalue("alert_label_title"), Sresponse);
                    }

                    if (Sstatus.equalsIgnoreCase("1")) {
                        ticket_status.setText(Sticket_status.toUpperCase());
                        tv_ticket_no.setText(db.getvalue("ticket_no_lable") + ":" + Sticket_no);
                        ticketdate.setText(db.getvalue("recda") + Sticket_date);
                        complaint_sub.setText(Ssubject.toUpperCase());
                        if (isCommentAvailable) {
                            adapter = new ComplaintPageViewDetailAdapter(ComplaintPageViewDetailPage.this, itemlist, ComplaintPageViewDetailPage.this, driver_image, user_image, ticketStatus);
                            listview.setAdapter(adapter);
                            listview.setSelection(listview.getAdapter().getCount() - 1);

                        }
                    }
                } catch (JSONException e) {
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }


    //-----------------------postComment Post Request-----------------
    private void postRequest_postComment(String Url, HashMap<String, String> jsonParams) {
        dialog = new Dialog(ComplaintPageViewDetailPage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(db.getvalue("action_pleasewait"));
        System.out.println("-------------viewTicket Url----------------" + Url);
        System.out.println("-------------postComment jsonParams----------------" + jsonParams);
        mRequest = new ServiceRequest(ComplaintPageViewDetailPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------postComment Response----------------" + response);
                String Sstatus = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    String Sresponse = object.getString("message");
                    isFromReply = true;
                    successDialog(Sticket_no);


                } catch (JSONException e) {
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    //-----------------------closeCase Post Request-----------------

    private void sendBroadcast() {


        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("com.package.ACTION_CLASS_complaint_refreshhomePageComplaintlist");
        sendBroadcast(broadcastIntent);

    }


    private void ReplyComplaint() {

        final Dialog replyDialog = new Dialog(ComplaintPageViewDetailPage.this, R.style.DialogSlideAnim);
        replyDialog.getWindow();
        replyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        replyDialog.setContentView(R.layout.replycomplaint_popup);
        replyDialog.setCanceledOnTouchOutside(true);
        replyDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        replyDialog.show();

        final EditText Et_message = (EditText) replyDialog.findViewById(R.id.view_complaint_page_replay_message_edittext);
        TextView Tv_ok = (TextView) replyDialog.findViewById(R.id.view_complaint_page_replay_message_send_imageview);
        ImageView closeInage = (ImageView) replyDialog.findViewById(R.id.close_im);

        closeInage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replyDialog.dismiss();
            }
        });


        Tv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                cd = new ConnectionDetector(ComplaintPageViewDetailPage.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {


                    String sMessage = Et_message.getText().toString();
                    if (sMessage != null && sMessage.length() > 0) {
                        try {

                            HashMap<String, String> jsonParams = new HashMap<String, String>();
                            jsonParams.put("driver_id", UserID);//"5858fa30cae2aac80b000054");
                            jsonParams.put("ticket_id", SticketId);
                            jsonParams.put("comment", sMessage);

                            postRequest_postComment(Iconstant.post_comment_url, jsonParams);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        replyDialog.dismiss();
                        Et_message.getText().clear();
                    } else {
                        AlertError(db.getvalue("alert_label_title"), db.getvalue("enetma"));
                    }
                } else {
                    AlertError(db.getvalue("alert_label_title"), db.getvalue("alert_nointernet"));
                }


            }
        });


    }

    private void AlertError(String title, String message) {

        String msg = title + "\n" + message;

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        AppMsg.Style style = new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red_color);
        AppMsg snack = AppMsg.makeText(ComplaintPageViewDetailPage.this, msg.toUpperCase(), AppMsg.STYLE_ALERT);
        snack.setView(view);
        snack.setLayoutGravity(Gravity.TOP);
        snack.setPriority(AppMsg.PRIORITY_HIGH);
        snack.setAnimation(R.anim.slidedown, R.anim.slideup);
        snack.show();

    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(ComplaintPageViewDetailPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(db.getvalue("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    @Override
    public void onReplyUser() {
        ReplyComplaint();
    }

    @Override
    public void ViewDetails() {
        isFromReply = false;
        successDialog(SticketId);

    }

    private void successDialog(String ticket_no) {

        success_dialog = new Dialog(ComplaintPageViewDetailPage.this, R.style.DialogSlideAnim3);
        success_dialog.getWindow();
        success_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        success_dialog.setContentView(R.layout.complaint_submit_popup);
        success_dialog.setCanceledOnTouchOutside(true);
//        success_dialog.getWindow().getAttributes().windowAnimations =R.style.Animations_categories_filter;
        success_dialog.show();
        success_dialog.getWindow().setGravity(Gravity.CENTER);

        TextView replySuccessTv = (TextView) success_dialog.findViewById(R.id.replysuccess_tv);
        TextView Tv_ticketNo = (TextView) success_dialog.findViewById(R.id.ticketnumber);
        TextView subject = (TextView) success_dialog.findViewById(R.id.subject);
        TextView bookingnumber = (TextView) success_dialog.findViewById(R.id.bookingnumber);
        TextView today_datetime1 = (TextView) success_dialog.findViewById(R.id.dateandtime);
        TextView img_close = (TextView) success_dialog.findViewById(R.id.image_close);

        if (!isFromReply) {
            replySuccessTv.setVisibility(View.GONE);
        }

        Tv_ticketNo.setText("FOR TICKET NO     : " + ticket_no);
        subject.setText("SUBJECT                 : " + Ssubject.toUpperCase());
        bookingnumber.setText("BOOKING NO          : " + Sbooking_no);
        today_datetime1.setText("Date " + Sticket_date);


        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //sendBroadcast();
                success_dialog.dismiss();
                Intent intent = new Intent(ComplaintPageViewDetailPage.this, ComplaintPage.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            }
        });


    }

    //--------------Alert Method-----------
    private void Alert1(String title, String alert) {
        final PkDialog mDialog = new PkDialog(ComplaintPageViewDetailPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(db.getvalue("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                Intent intent = new Intent(ComplaintPageViewDetailPage.this, ComplaintPageViewDetailPage.class);
                intent.putExtra("ticket_id", Sticket_no);
                intent.putExtra("ticket_status", Sticket_status);
                startActivity(intent);
                finish();


            }
        });
        mDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
       /* Intent intent = new Intent(ComplaintPageViewDetailPage.this, ComplaintPage.class);
        startActivity(intent);*/
        overridePendingTransition(R.anim.enter, R.anim.exit);
        finish();
    }


    private boolean checkCallPhonePermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkReadStatePermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CALL_PHONE, android.Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CALL_PHONE, android.Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODES);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + phone_number));
                    if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    startActivity(callIntent);
                }
        }
    }
}
