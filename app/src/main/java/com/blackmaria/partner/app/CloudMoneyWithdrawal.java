package com.blackmaria.partner.app;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.Pojo.WalletMoneyPojo;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.adapter.CloudMoneyWithdrawalRecyclerAdapter;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.CustomEdittext;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by GANESH on 19-09-2017.
 */

public class CloudMoneyWithdrawal extends ActivityHockeyApp implements View.OnClickListener {

    private Button Btn_cancel;
    private TextView Tv_availableAmount, Tv_lastWithdrawal;
    private RecyclerView Rv_payment;
    private EditText Edt_withdrawAmount;

    private SessionManager sessionManager;
    private ConnectionDetector cd;
    private Dialog dialog;
    private ServiceRequest mRequest;
    private CloudMoneyWithdrawalRecyclerAdapter adapter;

    private String sDriverID = "", sCurrency = "", sLastWithdrawal = "", sMinAmount = "", sMaxAmount = "", sAvailableAmount = "",
            sContactNumber = "", sPayAmount = "", sSelectedPayment = "", sPaypalID = "", sSelectedPaymentName = "", sMinAmountToMaintain = "",
            sProceedStatus = "", sProceedError = "";
    private double MaxAmount = 0.0, MinAmount = 0.0, AmountToMaintain = 0.0;
    //    private String sFromPage = "";
    private ArrayList<WalletMoneyPojo> listPayment;
    final int PERMISSION_REQUEST_CODE = 111;
    private CustomEdittext Edt_paypalId;
    ImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wallet_withdrawal_new);

        initialize();

    }

    private void initialize() {

        sessionManager = new SessionManager(CloudMoneyWithdrawal.this);
        cd = new ConnectionDetector(CloudMoneyWithdrawal.this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        sContactNumber = sessionManager.getContactNumber();
        listPayment = new ArrayList<WalletMoneyPojo>();

        Btn_cancel = (Button) findViewById(R.id.btn_cancel);
        Rv_payment = (RecyclerView) findViewById(R.id.Rv_payment_options);
        Edt_withdrawAmount = (EditText) findViewById(R.id.edt_withdrawal_amount);
        Tv_lastWithdrawal = (TextView) findViewById(R.id.txt_label_last_withdrawal);
//        Tv_currencyCode = (TextView) findViewById(R.id.txt_currency_code);

        Btn_cancel.setOnClickListener(this);

        /*HLV_paymentOptions.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (!listPayment.get(position).getPaymnet_name().equalsIgnoreCase(getResources().getString(R.string.withdrawal_page_label_bank))) {
                    String selectedPaymentId = listPayment.get(position).getPayment_code();
                    sSelectedPaymentName = listPayment.get(position).getPaymnet_name();
                    for (int i = 0; i < listPayment.size(); i++) {
                        listPayment.get(i).setPayment_selected_payment_id(selectedPaymentId);
                    }
                    adapter.notifyDataSetChanged();

                    sPaypalID = "";
                    if (sSelectedPaymentName.equalsIgnoreCase("paypal")) {
                        showPaypalDialog();
                    }
                } else {

                    String paymentName = listPayment.get(position).getPaymnet_name();
                    Alert("", String.format(getResources().getString(R.string.withdrawal_page_option_not_available), paymentName));

                }
            }
        });*/


        if (cd.isConnectingToInternet()) {

            GetWithdrawalData(Iconstant.wallet_withdrawal_dashboard_url);

        } else {
            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }

    }


    @Override
    public void onClick(View view) {

        if (view == Btn_cancel) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);

            Intent intent = new Intent();
            intent.setAction("com.app.Withdraw.Success");
            sendBroadcast(intent);

        } /*else if (view == RL_confirm) {

            if (cd.isConnectingToInternet()) {

                if (sProceedStatus.equalsIgnoreCase("1")) {

                    sPayAmount = Edt_withdrawAmount.getText().toString().trim().replace(sCurrency, "");
                    if (sPayAmount.length() > 0) {

                        try {

                            double amountToWithdraw = Double.parseDouble(sPayAmount);
                            if (amountToWithdraw == 0) {
                                Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.withdrawal_page_error_withdrawal_amount_cannot_be_zero));
                            } else {

                            *//*if (amountToWithdraw > MaxAmount) {
                                WithdrawErrorAlert(getResources().getString(R.string.withdrawal_page_label_max_withdrawal_is), sCurrency + " " + sMaxAmount);
                            }*//*

                                double balanceAmount = MaxAmount - amountToWithdraw;
                                double maxWithdrawAmount = MaxAmount - AmountToMaintain;

                                if (balanceAmount >= AmountToMaintain) {

                                    sSelectedPayment = listPayment.get(0).getPayment_selected_payment_id();

                                    if (sSelectedPaymentName.equalsIgnoreCase("paypal") && sPaypalID.length() == 0) {
                                        showPaypalDialog();
                                    } else {
                                        WithdrawalAmount(Iconstant.wallet_withdraw_amount_url);
                                    }

                                } else {
                                    WithdrawErrorAlert(getResources().getString(R.string.withdrawal_page_label_max_withdrawal_is), sCurrency + " " + maxWithdrawAmount);
                                }

                            }


                        } catch (Exception e) {
                            e.printStackTrace();

                            if (e instanceof NumberFormatException) {
                                Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.withdrawal_page_error_invalid_amount));
                            }
                        }

                    } else {
                        Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.withdrawal_page_error_withdrawal_amount_cannot_be_empty));
                    }

                } else {
                    Alert(getResources().getString(R.string.action_error), sProceedError);
                }

            } else {
                Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
            }

        }*/ /*else if (view == Tv_help) {

            showHelpDialog();

        } else if (view == RL_withdrawal) {

            if (sFromPage.equalsIgnoreCase("earnings")) {
                showWithdrawalDialog();
            }

        } else if (view == Iv_faq || view == Tv_faq) {

            if (sContactNumber.length() > 0) {
                callCustomerCare();
            }

        } else if (view == Tv_moreInfo) {

            showHelpDialog();

        }*/

    }


    private void callCustomerCare() {

        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+


            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + sContactNumber));
            startActivity(intent);


        } else {

            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + sContactNumber));
            startActivity(intent);

        }

    }


    //-------------Help Dialog-----------------
    private void showHelpDialog() {

        //--------Adjusting Dialog width & Height-----
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 85% of the screen
        int screenHeight = (int) (metrics.heightPixels * 0.85);//fill only 85% of the screen

        final Dialog helpDialog = new Dialog(CloudMoneyWithdrawal.this, R.style.SlideUpDialog);
        helpDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        helpDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        helpDialog.setCancelable(true);
        helpDialog.setContentView(R.layout.alert_withdrawal_help_info);
        helpDialog.getWindow().setLayout(screenWidth, screenHeight);

        ImageView Iv_close = (ImageView) helpDialog.findViewById(R.id.img_close);

        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (helpDialog != null && helpDialog.isShowing()) {
                    helpDialog.dismiss();
                }
            }
        });


        helpDialog.show();

    }


    private void showWithdrawalDialog() {
        final Dialog withdrawalDialog = new Dialog(CloudMoneyWithdrawal.this, R.style.SlideUpDialog);
        withdrawalDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        withdrawalDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        withdrawalDialog.setCancelable(true);
        withdrawalDialog.setContentView(R.layout.alert_enter_withdrawal);

        final TextView Tv_currency = (TextView) withdrawalDialog.findViewById(R.id.txt_currency);
        final ImageView Iv_close = (ImageView) withdrawalDialog.findViewById(R.id.img_close);
        final EditText Et_withdrawAmount = (EditText) withdrawalDialog.findViewById(R.id.edt_withdrawal_amount);
        final RelativeLayout RL_confirm = (RelativeLayout) withdrawalDialog.findViewById(R.id.Rl_confirm);

        Et_withdrawAmount.setText(Tv_availableAmount.getText().toString().replace(sCurrency, ""));
        Et_withdrawAmount.setSelection(Et_withdrawAmount.getText().length());
        Tv_currency.setText(sCurrency);

        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (withdrawalDialog != null && withdrawalDialog.isShowing()) {
                    withdrawalDialog.dismiss();
                }
            }
        });

        RL_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String withdrawAmount = Et_withdrawAmount.getText().toString().trim();
                if (withdrawAmount.length() > 0) {

                    double amountToWithdraw = Double.parseDouble(withdrawAmount);
                    if (!(amountToWithdraw > 0)) {
                        Et_withdrawAmount.setError(getResources().getString(R.string.withdrawal_page_error_withdrawal_amount_invalid));
                    } else {

                        Et_withdrawAmount.setError(null);
                        Tv_availableAmount.setText(sCurrency + Et_withdrawAmount.getText().toString().trim());

                        if (withdrawalDialog != null && withdrawalDialog.isShowing()) {
                            withdrawalDialog.dismiss();
                        }

                        CloseKeyboardNew();
                    }

                } else {
                    Et_withdrawAmount.setError(getResources().getString(R.string.withdrawal_page_error_withdrawal_amount_cannot_be_empty));
                }


            }
        });

        Et_withdrawAmount.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    CloseKeyboard(Et_withdrawAmount);
                }
                return false;
            }
        });

        withdrawalDialog.show();

    }


    /*private void showPaypalDialog() {
        final Dialog paypalDialog = new Dialog(WalletWithdrawPage.this, R.style.SlideUpDialog);
        paypalDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        paypalDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        paypalDialog.setCancelable(true);
        paypalDialog.setContentView(R.layout.alert_paypal_id);

        final ImageView Iv_close = (ImageView) paypalDialog.findViewById(R.id.img_close);
        final EditText Et_email = (EditText) paypalDialog.findViewById(R.id.edt_authorize_net);
        final TextView Tv_confirm = (TextView) paypalDialog.findViewById(R.id.paypal_confirm);

        Et_email.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    CloseKeyboard(Et_email);
                }
                return false;
            }
        });

        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (paypalDialog != null && paypalDialog.isShowing()) {
                    paypalDialog.dismiss();
                }
            }
        });

        Tv_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String PaypalID = Et_email.getText().toString().trim();
                if (PaypalID.length() > 0 && isValidEmail(PaypalID)) {
                    Et_email.setError(null);
                    sPaypalID = PaypalID;
                    if (paypalDialog != null && paypalDialog.isShowing()) {
                        paypalDialog.dismiss();
                    }
                } else {
                    if (PaypalID.length() == 0) {
                        Et_email.setError(getResources().getString(R.string.withdrawal_page_label_paypal_validation));
                    } else if (!isValidEmail(PaypalID)) {
                        Et_email.setError(getResources().getString(R.string.withdrawal_page_label_enter_valid_paypal_id));
                    }
                }

            }
        });

        paypalDialog.show();

    }*/

    private void showPaypalDialog() {

        final Dialog paypalDialog = new Dialog(CloudMoneyWithdrawal.this, R.style.SlideRightDialog);
        paypalDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        paypalDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        paypalDialog.setCancelable(true);
        paypalDialog.setCanceledOnTouchOutside(true);
        paypalDialog.setContentView(R.layout.alert_insert_paypal_id);

        Edt_paypalId = (CustomEdittext) paypalDialog.findViewById(R.id.edt_paypal_id1);
        final Button Btn_ok = (Button) paypalDialog.findViewById(R.id.btn_ok1);
        final ImageView Iv_close = (ImageView) paypalDialog.findViewById(R.id.img_close);

        Btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String paypalId = Edt_paypalId.getText().toString().trim();

                if (paypalId.length() == 0) {
                    erroredit(Edt_paypalId, getResources().getString(R.string.withdrawal_page_label_paypal_validation));
                } else if (!isValidEmail(paypalId)) {
                    erroredit(Edt_paypalId, getResources().getString(R.string.withdrawal_page_label_enter_valid_paypal_id));
                } else {
                    sPaypalID = paypalId;
                    paypalDialog.dismiss();
                }

            }
        });

        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paypalDialog.dismiss();
            }
        });

        paypalDialog.show();

    }


    @SuppressLint("WrongConstant")
    private void GetWithdrawalData(String Url) {

        dialog = new Dialog(CloudMoneyWithdrawal.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);

        System.out.println("--------------WalletWithdraw Url-------------------" + Url);
        System.out.println("--------------WalletWithdraw jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(CloudMoneyWithdrawal.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------WalletWithdraw Response-------------------" + response);
                String status = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {
                            JSONObject jsonObject = object.getJSONObject("response");

                            if (jsonObject.length() > 0) {

                                sAvailableAmount = jsonObject.getString("available_amount");
                                sCurrency = jsonObject.getString("currency");
                                sLastWithdrawal = jsonObject.getString("last_withdrawal_txt");
                                sMinAmountToMaintain = jsonObject.getString("wallet_min_amount");
                                if (sMinAmountToMaintain.length() > 0) {
                                    AmountToMaintain = Double.parseDouble(sMinAmountToMaintain);
                                }
                                sProceedStatus = jsonObject.getString("proceed_status");
                                sProceedError = jsonObject.getString("error_message");

                                /*if (jsonObject.has("min_amount")) {
                                    sMinAmount = jsonObject.getString("min_amount");
                                }
                                if (jsonObject.has("max_amount")) {
                                    sMaxAmount = jsonObject.getString("max_amount");
                                }

                                if (sMaxAmount.length() > 0 && sMinAmount.length() > 0) {
                                    MaxAmount = Double.parseDouble(sMaxAmount);
                                    MinAmount = Double.parseDouble(sMinAmount);
                                }*/

                                sMaxAmount = sAvailableAmount;
                                if (sMaxAmount.length() > 0) {
                                    MaxAmount = Double.parseDouble(sMaxAmount);
                                }


                                JSONArray jarPayment = jsonObject.getJSONArray("payment");

                                if (jarPayment.length() > 0) {

                                    listPayment = new ArrayList<WalletMoneyPojo>();
                                    for (int i = 0; i < jarPayment.length(); i++) {

                                        JSONObject jobjPayment = jarPayment.getJSONObject(i);

                                        String name = jobjPayment.getString("name");
                                        String code = jobjPayment.getString("code");
                                        String active_icon = jobjPayment.getString("icon");
                                        String inactive_icon = jobjPayment.getString("inactive_icon");
                                        String selected_payment = "";
                                        if (i == 0) {
                                            selected_payment = code;
                                            sSelectedPaymentName = name;
                                        } else {
                                            selected_payment = "";
                                        }

                                        /*if (name.equalsIgnoreCase(getResources().getString(R.string.withdrawal_page_label_bwallet))) {
                                            selected_payment = code;
                                            sSelectedPaymentName = name;
                                        }*/

                                        listPayment.add(new WalletMoneyPojo(name, code, active_icon, inactive_icon, selected_payment));

                                    }

                                }

                            }


                        } else {
                            String sResponse = object.getString("response");
                            Alert(getResources().getString(R.string.action_error), sResponse);
                        }

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (status.equalsIgnoreCase("1")) {

//                    Tv_availableAmount.setText(sCurrency + sAvailableAmount);
//                    Tv_currencyCode.setText(sCurrency);
                    if (sLastWithdrawal.length() > 0) {
                        Tv_lastWithdrawal.setText(getResources().getString(R.string.withdrawal_page_label_last_withdrawal) + " " + sLastWithdrawal);
                    } else {
                        Tv_lastWithdrawal.setText(getResources().getString(R.string.withdrawal_page_label_no_withdrawal));
                        Tv_lastWithdrawal.setVisibility(View.INVISIBLE);
                    }

//                    adapter = new WalletWithdrawPaymentListAdapter(CloudMoneyWithdrawal.this, listPayment);
//                    HLV_paymentOptions.setAdapter(adapter);
//                    adapter.notifyDataSetChanged();


                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(CloudMoneyWithdrawal.this, LinearLayoutManager.HORIZONTAL, false);
                    Rv_payment.setLayoutManager(mLayoutManager);
                    Rv_payment.setItemAnimator(new DefaultItemAnimator());
                    adapter = new CloudMoneyWithdrawalRecyclerAdapter(CloudMoneyWithdrawal.this, listPayment, new CloudMoneyWithdrawalRecyclerAdapter.RecyclerItemClick() {
                        @Override
                        public void onItemClick(int position) {

//                            Toast.makeText(CloudMoneyWithdrawal.this, "Position = " + position, Toast.LENGTH_SHORT).show();

                            if (cd.isConnectingToInternet()) {

                                if (sProceedStatus.equalsIgnoreCase("1")) {

                                    sPayAmount = Edt_withdrawAmount.getText().toString().trim().replace(sCurrency, "");
                                    if (sPayAmount.length() > 0) {

                                        try {

                                            double amountToWithdraw = Double.parseDouble(sPayAmount);
                                            if (amountToWithdraw == 0) {
                                                Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.withdrawal_page_error_withdrawal_amount_cannot_be_zero));
                                            } else {

                                                if (amountToWithdraw > MaxAmount) {
                                                    WithdrawErrorAlert(getResources().getString(R.string.withdrawal_page_label_max_withdrawal_is), sCurrency + " " + sMaxAmount);
                                                }

                                                double balanceAmount = MaxAmount - amountToWithdraw;
                                                double maxWithdrawAmount = MaxAmount - AmountToMaintain;

                                                if (balanceAmount >= AmountToMaintain) {

                                                    sSelectedPayment = listPayment.get(position).getPayment_selected_payment_id();

                                                    if (sSelectedPaymentName.equalsIgnoreCase("paypal") && sPaypalID.length() == 0) {
                                                        showPaypalDialog();
                                                    } else if(sSelectedPaymentName.contains("bank")){

                                                    } else {
                                                        WithdrawalAmount(Iconstant.wallet_withdraw_amount_url);
                                                    }

                                                } else {
                                                    WithdrawErrorAlert(getResources().getString(R.string.withdrawal_page_label_max_withdrawal_is), sCurrency + " " + maxWithdrawAmount);
                                                }

                                            }


                                        } catch (Exception e) {
                                            e.printStackTrace();

                                            if (e instanceof NumberFormatException) {
                                                Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.withdrawal_page_error_invalid_amount));
                                            }
                                        }

                                    } else {
                                        Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.withdrawal_page_error_withdrawal_amount_cannot_be_empty));
                                    }

                                } else {
                                    Alert(getResources().getString(R.string.action_error), sProceedError);
                                }

                            } else {
                                Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                            }


                        }
                    });

                    Rv_payment.setAdapter(adapter);


                }

                if (dialog != null) {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    @SuppressLint("WrongConstant")
    private void WithdrawalAmount(String Url) {

        dialog = new Dialog(CloudMoneyWithdrawal.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        jsonParams.put("amount", sPayAmount);
        jsonParams.put("mode", sSelectedPayment);
        jsonParams.put("paypal_id", sPaypalID);

        System.out.println("--------------WalletWithdrawalAmount Url-------------------" + Url);
        System.out.println("--------------WalletWithdrawalAmount jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(CloudMoneyWithdrawal.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------WalletWithdrawalAmount Response-------------------" + response);
                String status = "", sResponse = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");
                        sResponse = object.getString("response");

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (dialog != null) {
                    dialog.dismiss();
                }

                if (status.equalsIgnoreCase("1")) {
//                    AlertWithdrawSuccess(getResources().getString(R.string.action_success), sResponse);
                    WithdrawSuccessAlert(sPayAmount, sSelectedPaymentName);
                } else {
//                    Alert(getResources().getString(R.string.action_error), sResponse);
                    WithdrawErrorAlert("SORRY! EXCESS LIMIT", "Please lower your amount");
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void AlertWithdrawSuccess(String title, String alert) {
        final PkDialog mDialog = new PkDialog(CloudMoneyWithdrawal.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

                Intent intent = new Intent();
                intent.setAction("com.app.Withdraw.Success");
                sendBroadcast(intent);

                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });
        mDialog.show();
    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(CloudMoneyWithdrawal.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void WithdrawSuccessAlert(String withdrawAmount, String paymentType) {
        final Dialog withdrawSuccessDialog = new Dialog(CloudMoneyWithdrawal.this, R.style.SlideUpDialog);
        withdrawSuccessDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        withdrawSuccessDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        withdrawSuccessDialog.setCancelable(true);
        withdrawSuccessDialog.setContentView(R.layout.alert_withdraw_success1);

        final ImageView Iv_close = (ImageView) withdrawSuccessDialog.findViewById(R.id.img_close);
        final TextView Tv_withdrawAmount = (TextView) withdrawSuccessDialog.findViewById(R.id.txt_alert_amount);
        final TextView Tv_paymentType = (TextView) withdrawSuccessDialog.findViewById(R.id.txt_alert_payment_type);

        Tv_withdrawAmount.setText(withdrawAmount);
        paymentType = getResources().getString(R.string.withdrawal_page_label_payment_type) + paymentType;
        Tv_paymentType.setText(paymentType);

        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (withdrawSuccessDialog != null && withdrawSuccessDialog.isShowing()) {
                    withdrawSuccessDialog.dismiss();
                }
            }
        });

        withdrawSuccessDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);

                Intent intent = new Intent();
                intent.setAction("com.app.Withdraw.Success");
                sendBroadcast(intent);
            }
        });

        withdrawSuccessDialog.show();

    }

    private void WithdrawErrorAlert(String message1, String message2) {
        final Dialog withdrawErrorDialog = new Dialog(CloudMoneyWithdrawal.this, R.style.SlideUpDialog);
        withdrawErrorDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        withdrawErrorDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        withdrawErrorDialog.setCancelable(true);
        withdrawErrorDialog.setContentView(R.layout.alert_withdrawal_errors);

        final ImageView Iv_close = (ImageView) withdrawErrorDialog.findViewById(R.id.img_close);
        final TextView Tv_message1 = (TextView) withdrawErrorDialog.findViewById(R.id.txt_alert_message1);
        final TextView Tv_message2 = (TextView) withdrawErrorDialog.findViewById(R.id.txt_alert_message2);

        Tv_message1.setText(message1);
        Tv_message2.setText(message2);

        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (withdrawErrorDialog != null && withdrawErrorDialog.isShowing()) {
                    withdrawErrorDialog.dismiss();
                }
            }
        });

        withdrawErrorDialog.show();

    }


    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
    }

    private boolean checkCallPhonePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkReadStatePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }


    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + sContactNumber));
                    startActivity(callIntent);
                }
                break;
        }
    }


    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    //--------------Close KeyBoard Method-----------
    @SuppressWarnings("WrongConstant")
    private void CloseKeyboard(EditText edittext) {
        try {
            InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            in.hideSoftInputFromWindow(edittext.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("WrongConstant")
    private void CloseKeyboardNew() {
        try {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow((null == getCurrentFocus()) ? null : getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {

            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);

            Intent intent = new Intent();
            intent.setAction("com.app.Withdraw.Success");
            sendBroadcast(intent);

            return true;
        }
        return false;
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }
    //--------------------Code to set error for EditText-----------------------
    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(CloudMoneyWithdrawal.this, R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }


}
