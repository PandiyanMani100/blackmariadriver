package com.blackmaria.partner.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.newdesign.Saveplus_sender_sendmoney;
import com.blackmaria.partner.Pojo.FareDetailPojo;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.CurrencySymbolConverter;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.adapter.FareDetailsAdapter;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.view.Dashboard.OnlinepageConstrain;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by GANESH on 26-07-2017.
 */

public class FarePage extends ActivityHockeyApp implements View.OnClickListener {

    private RelativeLayout RL_receivePayment, RL_tripCompleted, RL_processingPayment;
    private ImageView Iv_paymentType;
    private TextView Tv_rideCompetedTime, Tv_totalFare, Tv_rideID;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private SessionManager session;
    private String sDriverID = "", sContactEmail = "", sNeedPayment = "", sPaymentType = "", sPaymentRequest = "", sUserimage = "", sUsername = "", sUserid = "", sUsersaveplusid = "";

    private ServiceRequest mRequest;
    Dialog dialog;
    ArrayList<FareDetailPojo> itemlist_all;
    FareDetailsAdapter adapter;
    private ExpandableHeightListView listview;
    private String Str_RideId = "", sGrand_fare = "", Str_RiderImage = "", sPaymentIcon = "", sFromPage = "";
    private BroadcastReceiver refreshReceiver;

    public static Activity farePage;
    private BroadcastReceiver finishReceiver;

    private RelativeLayout RL_close, RL_rate_user, RL_report;
    private RelativeLayout RL_send_thankYou;
    private Button Btn_pay, Btn_checkStatus;
    private ImageView Iv_status, Iv_rider_pic, Iv_paymentTypeProcessing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_fare_page);
        farePage = FarePage.this;

        registerBroadcast();

        IntentFilter filter = new IntentFilter();
        filter.addAction("com.app.finish.FarePage");
        finishReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                finish();
            }
        };
        registerReceiver(finishReceiver, filter);

        initialize();

    }

    private void initialize() {
        session = new SessionManager(FarePage.this);
        cd = new ConnectionDetector(FarePage.this);
        isInternetPresent = cd.isConnectingToInternet();
        itemlist_all = new ArrayList<FareDetailPojo>();

        RL_receivePayment = (RelativeLayout) findViewById(R.id.RL_receive_payment);
        RL_tripCompleted = (RelativeLayout) findViewById(R.id.RL_trip_completed);
        RL_processingPayment = (RelativeLayout) findViewById(R.id.RL_processing_payment);

        Tv_totalFare = (TextView) findViewById(R.id.txt_total_fare);
        Iv_paymentType = (ImageView) findViewById(R.id.img_payment_type);
        Tv_rideCompetedTime = (TextView) findViewById(R.id.farebeakup_date_time_textview);

        RL_close = (RelativeLayout) findViewById(R.id.Rl_close);
        RL_rate_user = (RelativeLayout) findViewById(R.id.Rl_rate_user);
        RL_report = (RelativeLayout) findViewById(R.id.Rl_report);
        RL_send_thankYou = (RelativeLayout) findViewById(R.id.Rl_rider_profile);
        Btn_pay = (Button) findViewById(R.id.btn_receive);
        Iv_rider_pic = (ImageView) findViewById(R.id.img_rider_profile);
        Tv_rideID = (TextView) findViewById(R.id.txt_ride_id);

        Iv_paymentTypeProcessing = (ImageView) findViewById(R.id.img_payment_type_processing);
        Btn_checkStatus = (Button) findViewById(R.id.btn_check_status);

        RL_close.setOnClickListener(this);
        RL_rate_user.setOnClickListener(this);
        RL_report.setOnClickListener(this);
        RL_send_thankYou.setOnClickListener(this);
        Btn_pay.setOnClickListener(this);
        Btn_checkStatus.setOnClickListener(this);

        listview = (ExpandableHeightListView) findViewById(R.id.listview);
        listview.setExpanded(true);
        Iv_status = (ImageView) findViewById(R.id.img_status);

        startBlinkingAnimation();
        getIntentData();

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        sContactEmail = user.get(SessionManager.KEY_EMAIL);


        ImageView Rl_drawer = (ImageView) findViewById(R.id.drawer_icon);
        final Animation animation = new AlphaAnimation(1, 0);
        animation.setDuration(500);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
        Rl_drawer.startAnimation(animation);


        if (isInternetPresent) {
            postRequest(Iconstant.faredetail_Url);
        } else {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
        }
    }

    private void getIntentData() {
        Intent i = getIntent();

        if (i.hasExtra("fromsaveplus")) {
            Str_RideId = i.getStringExtra("rideId");
            RL_receivePayment.setVisibility(View.GONE);
            RL_tripCompleted.setVisibility(View.VISIBLE);
            RL_processingPayment.setVisibility(View.GONE);
        } else {
            if (i != null) {
                Str_RideId = i.getStringExtra("rideId");
                RL_receivePayment.setVisibility(View.VISIBLE);
                RL_tripCompleted.setVisibility(View.GONE);
                RL_processingPayment.setVisibility(View.GONE);
                if (i.hasExtra("fromPage")) {
                    sFromPage = i.getStringExtra("fromPage");
                }
            }
        }
    }

    private void startBlinkingAnimation() {
        Animation startAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blinking_animation);
        Iv_status.startAnimation(startAnimation);
    }

    //----------Sending message on Email Method--------
    protected void sendEmail(String to) {
        String[] TO = {to};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "");
        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.invite_earn_label_email_not_installed));
        }
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(FarePage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    //--------------Alert Method-----------
    private void AlertPaymentProcessing(String title, String alert) {
        final PkDialog mDialog = new PkDialog(FarePage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mDialog.dismiss();
            }
        }, 3000);
    }

    //--------------Alert Payment Success Method-----------
    @SuppressWarnings("WrongConstant")
    private void AlertPaymentSuccess(String title, String alert) {
        final PkDialog mDialog = new PkDialog(FarePage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

//                Tv_payment_status.setText(getResources().getString(R.string.fare_page_label_cash_paid));
//                Btn_pay.setVisibility(View.GONE);
                Intent i = new Intent(FarePage.this, Saveplus_sender_sendmoney.class);
                i.putExtra("receiver_id", sUserid);
                i.putExtra("total_amount", sGrand_fare);
                i.putExtra("name", sUsername);
                i.putExtra("image", sUserimage);
                i.putExtra("saveplusid", sUsersaveplusid);
                i.putExtra("rideid", Str_RideId);
                startActivity(i);
                finish();

//                RL_receivePayment.setVisibility(View.GONE);
//                RL_tripCompleted.setVisibility(View.VISIBLE);
//                RL_processingPayment.setVisibility(View.GONE);


            }
        });
        mDialog.show();
    }

    //method to convert currency code to currency symbol
    private static Locale getLocale(String strCode) {
        for (Locale locale : NumberFormat.getAvailableLocales()) {
            String code = NumberFormat.getCurrencyInstance(locale).getCurrency().getCurrencyCode();
            if (strCode.equals(code)) {
                return locale;
            }
        }
        return null;
    }

    //-----------------------Fare Breakup Post Request-----------------
    @SuppressWarnings("WrongConstant")
    private void postRequest(String Url) {
        dialog = new Dialog(FarePage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));


        System.out.println("-------------fareBreak Url----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("ride_id", Str_RideId);
        jsonParams.put("driver_id", sDriverID);

        System.out.println("-------------fareBreak jsonParams----------------" + jsonParams);
        mRequest = new ServiceRequest(FarePage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------fareBreak Response----------------" + response);

                String Sstatus = "", currency_code = "", sCurrencySymbol = "", sRideCompetedTime = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        if (response_object.length() > 0) {


                            currency_code = response_object.getString("currency");
                            sCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(currency_code);


                            JSONObject fare_object = response_object.getJSONObject("fare");
                            sRideCompetedTime = fare_object.getString("ride_completed_time");
                            Str_RiderImage = fare_object.getString("user_image");
                            sPaymentIcon = fare_object.getString("payment_icon");


                            sGrand_fare = fare_object.getString("grand_fare");

                            sNeedPayment = fare_object.getString("need_payment");
                            sPaymentType = fare_object.getString("payment_type");
                            sPaymentRequest = fare_object.getString("payment_request");

                            if (sPaymentType.equalsIgnoreCase("cash")) {
                                sUserid = fare_object.getString("user_id");
                                sUserimage = fare_object.getString("user_image");
                                sUsername = fare_object.getString("user_name");
                                sUsersaveplusid = fare_object.getString("user_saveplus_id");
                            }

                            JSONArray trans_array = fare_object.getJSONArray("fare_arr");
                            if (trans_array.length() > 0) {
                                itemlist_all.clear();

                                for (int i = 0; i < trans_array.length(); i++) {
                                    JSONObject trans_object = trans_array.getJSONObject(i);

                                    FareDetailPojo pojo = new FareDetailPojo();
                                    pojo.setTitle(trans_object.getString("title"));
                                    pojo.setValue(sCurrencySymbol + " " + trans_object.getString("value"));

                                    itemlist_all.add(pojo);
                                }
                            }
                        }
                    }


                    if (Sstatus.equalsIgnoreCase("1")) {
//                        Tv_payment_status.setText(getResources().getString(R.string.fare_page_label_cash_payment));
                        Tv_rideCompetedTime.setText(getResources().getString(R.string.myTrip_detail_label_date_small) + " " + sRideCompetedTime);
                        Tv_totalFare.setText(sCurrencySymbol + " " + sGrand_fare);
                        Tv_rideID.setText(getResources().getString(R.string.label_crn) + Str_RideId);
                        if (Str_RiderImage.length() > 0) {
                            Picasso.with(FarePage.this).load(Str_RiderImage).placeholder(R.drawable.no_user_img).error(R.drawable.no_user_img).memoryPolicy(MemoryPolicy.NO_CACHE).resize(50, 100).into(Iv_rider_pic);
                        }
                        if (sPaymentIcon.length() > 0) {
                            Picasso.with(FarePage.this).load(sPaymentIcon).placeholder(R.drawable.no_user_img).error(R.drawable.no_user_img).memoryPolicy(MemoryPolicy.NO_CACHE).into(Iv_paymentType);
                            Picasso.with(FarePage.this).load(sPaymentIcon).placeholder(R.drawable.no_user_img).error(R.drawable.no_user_img).memoryPolicy(MemoryPolicy.NO_CACHE).into(Iv_paymentTypeProcessing);
                        }
                        adapter = new FareDetailsAdapter(FarePage.this, itemlist_all);
                        listview.setAdapter(adapter);

                        // Check If Already Paid
                        if (sNeedPayment.equalsIgnoreCase("no")) {
//                            Tv_payment_status.setText(getResources().getString(R.string.fare_page_label_cash_paid));
//                            Btn_pay.setVisibility(View.GONE);
                            RL_receivePayment.setVisibility(View.GONE);
                            RL_processingPayment.setVisibility(View.GONE);
                            RL_tripCompleted.setVisibility(View.VISIBLE);
                        } else {

                            if (!sPaymentType.equalsIgnoreCase("cash")) {
                                Btn_pay.setText(getResources().getString(R.string.fare_page_label_request_payment));

                                if (sPaymentRequest.equalsIgnoreCase("completed")) {
                                    RL_receivePayment.setVisibility(View.GONE);
                                    RL_tripCompleted.setVisibility(View.GONE);
                                    RL_processingPayment.setVisibility(View.VISIBLE);
                                } else if (sPaymentRequest.equalsIgnoreCase("processing")) {
                                    RL_receivePayment.setVisibility(View.VISIBLE);
                                    RL_tripCompleted.setVisibility(View.GONE);
                                    RL_processingPayment.setVisibility(View.GONE);
                                }

                            } else {
                                Btn_pay.setVisibility(View.VISIBLE);
                                Btn_pay.setText(getResources().getString(R.string.fare_page_label_receive_payment));

                                RL_receivePayment.setVisibility(View.VISIBLE);
                                RL_tripCompleted.setVisibility(View.GONE);
                                RL_processingPayment.setVisibility(View.GONE);
                            }

                        }


                    } else {
                        String Sresponse = object.getString("response");
                        Alert(getResources().getString(R.string.alert_label_title), Sresponse);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });

    }


    //-----------------------Code for begin trip post request-----------------
    private void ReceiveCashRequest(String Url) {
        dialog = new Dialog(FarePage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        System.out.println("-------------PaymentPage----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        jsonParams.put("ride_id", Str_RideId);
        jsonParams.put("amount", sGrand_fare);

        System.out.println("------------jsonParams------------" + jsonParams);

        mRequest = new ServiceRequest(FarePage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("responsepayment---------" + response);

                String Str_status = "", Str_response = "";

                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");
                    Str_response = object.getString("response");

                    System.out.println("response----------" + object.getString("response"));

                    System.out.println("status----------" + object.getString("status"));

                    if (Str_status.equalsIgnoreCase("1")) {

                        AlertPaymentSuccess(getResources().getString(R.string.action_success), Str_response);

                    } else {
                        Alert(getResources().getString(R.string.timer_label_alert_sorry), Str_response);
                    }

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }

        });

    }


    private void RequestPaymentRequest(String Url) {
        dialog = new Dialog(FarePage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        System.out.println("-------------RequestPayment URL----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        jsonParams.put("ride_id", Str_RideId);
        jsonParams.put("amount", sGrand_fare);

        System.out.println("------------RequestPayment jsonParams------------" + jsonParams);

        mRequest = new ServiceRequest(FarePage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("------------RequestPayment response------------" + response);

                String Str_status = "", Str_response = "";

                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");
                    Str_response = object.getString("response");

                    if (Str_status.equalsIgnoreCase("1")) {

                        RL_tripCompleted.setVisibility(View.GONE);
                        RL_receivePayment.setVisibility(View.GONE);
                        RL_processingPayment.setVisibility(View.VISIBLE);

                    } else {
                        Alert(getResources().getString(R.string.timer_label_alert_sorry), Str_response);
                    }

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }

        });

    }


    @Override
    protected void onDestroy() {
        if (finishReceiver != null) {
            unregisterReceiver(finishReceiver);
        }
        if (refreshReceiver != null) {
            unregisterReceiver(refreshReceiver);
        }
        super.onDestroy();
    }

    //-----------------Move Back on pressed phone back button------------------
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View view) {

        if (view == Btn_pay) {
            cd = new ConnectionDetector(FarePage.this);
            isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent) {
                if (Btn_pay.getText().toString().equalsIgnoreCase(getResources().getString(R.string.fare_page_label_request_payment))) {
                    RequestPaymentRequest(Iconstant.request_payment_url);
                } else {
                    ReceiveCashRequest(Iconstant.payment_received_Url);
                }
            } else {
                Alert(getResources().getString(R.string.timer_label_alert_sorry), getResources().getString(R.string.alert_nointernet));
            }
        } else if (view == RL_send_thankYou) {
            postRequestSendThankYou(Iconstant.send_thank_you_Url);
        } else if (view == RL_close) {
            Intent finish_TripPage = new Intent();
            finish_TripPage.setAction("com.app.finish.TripPage");
            sendBroadcast(finish_TripPage);

            Intent finish_FarePage = new Intent();
            finish_FarePage.setAction("com.app.finish.FarePage");
            sendBroadcast(finish_FarePage);

            Intent finish_GoOnlinePage = new Intent();
            finish_GoOnlinePage.setAction("com.app.finish.GoOnlinePage");
            sendBroadcast(finish_GoOnlinePage);

            Intent intent = new Intent(FarePage.this, OnlinepageConstrain.class);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (view == RL_rate_user) {
            Intent intent1 = new Intent(FarePage.this, RatingPage.class);
            intent1.putExtra("RideID", Str_RideId);
            startActivity(intent1);
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (view == RL_report) {
            sendEmail(sContactEmail);
        } else if (view == Btn_checkStatus) {
            if (cd.isConnectingToInternet()) {
                postRequestCheckStatus(Iconstant.check_payment_status_url);
            } else {
                Alert(getResources().getString(R.string.timer_label_alert_sorry), getResources().getString(R.string.alert_nointernet));
            }
        }

    }

    //-----------------------wallet Money Post Request-----------------
    private void postRequestSendThankYou(String Url) {
        dialog = new Dialog(FarePage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        final TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));


        System.out.println("-------------Send Thank You Url----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("ride_id", Str_RideId);
        jsonParams.put("driver_id", sDriverID);

        System.out.println("-------------Send Thank You jsonParams----------------" + jsonParams);
        mRequest = new ServiceRequest(FarePage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Send Thank You Response----------------" + response);

                String Sstatus = "", Sresponse = "";

                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }

                try {

                    JSONObject jobjResponse = new JSONObject(response);

                    Sstatus = jobjResponse.getString("status");
                    Sresponse = jobjResponse.getString("response");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        Alert(getResources().getString(R.string.action_success), Sresponse);
                    } else {
                        Alert(getResources().getString(R.string.alert_label_title), Sresponse);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
    }


    //-----------------------wallet Money Post Request-----------------
    private void postRequestCheckStatus(String Url) {
        dialog = new Dialog(FarePage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        final TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));


        System.out.println("-------------CheckPaymentStatus Url----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("ride_id", Str_RideId);
        jsonParams.put("driver_id", sDriverID);

        System.out.println("-------------CheckPaymentStatus jsonParams----------------" + jsonParams);
        mRequest = new ServiceRequest(FarePage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------CheckPaymentStatus Response----------------" + response);

                String Sstatus = "", Sresponse = "", sTripWaiting = "", sRattingPending = "";

                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }

                try {

                    JSONObject jobjResponse = new JSONObject(response);

                    Sstatus = jobjResponse.getString("status");
//                    Sresponse = jobjResponse.getString("response");

                    if (Sstatus.equalsIgnoreCase("1")) {

                        Object objResponse = jobjResponse.get("response");

                        if (objResponse instanceof JSONObject) {

                            JSONObject jobjRespon = jobjResponse.getJSONObject("response");

                            if (jobjRespon.length() > 0) {

                                sTripWaiting = jobjRespon.getString("trip_waiting");
                                sRattingPending = jobjRespon.getString("ratting_pending");

                                if (sTripWaiting.equalsIgnoreCase("yes")) {
                                    AlertPaymentProcessing("", "Please wait. Payment Processing...");
                                } else {
                                    RL_receivePayment.setVisibility(View.GONE);
                                    RL_processingPayment.setVisibility(View.GONE);
                                    RL_tripCompleted.setVisibility(View.VISIBLE);
                                }

                            }

                        }

                    } else {
                        Alert(getResources().getString(R.string.alert_label_title), Sresponse);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
    }


    private void registerBroadcast() {

        IntentFilter filter = new IntentFilter();
        filter.addAction("com.app.RefreshFarePage");
        refreshReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent != null) {

                    // Refresh Page to show Updated Content when Payment Completed By User
                    /*if (cd.isConnectingToInternet()) {
                        postRequest(Iconstant.faredetail_Url);
                    } else {
                        Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                    }*/
                    RL_receivePayment.setVisibility(View.GONE);
                    RL_processingPayment.setVisibility(View.GONE);
                    RL_tripCompleted.setVisibility(View.VISIBLE);

                }

            }
        };
        registerReceiver(refreshReceiver, filter);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if (intent != null) {
            /*Str_RideId = intent.getStringExtra("rideId");

            if (cd.isConnectingToInternet()) {
                postRequest(Iconstant.faredetail_Url);
            } else {
                Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
            }*/

            if (intent.hasExtra("fromPage")) {
                sFromPage = intent.getStringExtra("fromPage");
            }

            RL_receivePayment.setVisibility(View.GONE);
            RL_processingPayment.setVisibility(View.GONE);
            RL_tripCompleted.setVisibility(View.VISIBLE);

        }

    }
}

