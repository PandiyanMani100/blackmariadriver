package com.blackmaria.partner.app;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.Pojo.DriveEarnStatementChart;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Dashboard_constrain;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.devspark.appmsg.AppMsg;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Ganesh on 07-06-2017.
 */

public class DriveAndEarnStatement extends Activity implements View.OnClickListener {

    private ImageView Iv_back, Iv_withdraw, Iv_viewStatement, Iv_profile;
    private TextView Tv_currentBalance, Tv_todayTrips, Tv_weekTrips, Tv_monthTrips,
            Tv_driverCity, Tv_driverName, Tv_date;
    private RelativeLayout RL_topIncome, RL_topIncomeEmpty;
    private BarChart barChart;

    private ConnectionDetector cd;
    private SessionManager sessionManager;
    private ServiceRequest mRequest;
    private BroadcastReceiver withdrawSuccessReceiver;
    private Dialog dialog;

    private String sDriveID = "", sCurrency = "", sAvailableAmount = "", sTodayCount = "", sWeekCount = "", sMonthCount = "",
            sReferralCode = "", sDisplayMonth = "", sDriverImage = "", sDownloadUrl = "", sMinAmount = "", sMaxAmount = "",
            sTopDriverImage = "", sTopDriverName = "", sTopDriverLocation = "", sProceedWithdrawStatus = "", sProceedWithdrawError = "",
            sFromPage = "", sSecurePin = "", sPayAmount = "", sWithdrawStatus = "", sWithdrawError = "";
    private boolean isDataPresent = false, isChartDataPresent = false, isTopIncomeAvailable = false;
    private ArrayList<DriveEarnStatementChart> chartList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drive_and_earn_statement);

        initialize();

    }

    private void initialize() {

        sessionManager = new SessionManager(DriveAndEarnStatement.this);
        cd = new ConnectionDetector(DriveAndEarnStatement.this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriveID = user.get(SessionManager.KEY_DRIVERID);
        sSecurePin = sessionManager.getSecurePin();
        chartList = new ArrayList<>();

        getIntentData();

        // To Register Broadcast Receiver
        registerBroadcast();

        Iv_back = (ImageView) findViewById(R.id.img_back);
        Iv_withdraw = (ImageView) findViewById(R.id.img_withdraw);
        Iv_viewStatement = (ImageView) findViewById(R.id.img_statement);
        Iv_profile = (ImageView) findViewById(R.id.img_top_driver_profile);
        Tv_currentBalance = (TextView) findViewById(R.id.txt_current_balance);
        Tv_todayTrips = (TextView) findViewById(R.id.txt_today_trips);
        Tv_weekTrips = (TextView) findViewById(R.id.txt_week_trips);
        Tv_monthTrips = (TextView) findViewById(R.id.txt_month_trips);
        Tv_driverName = (TextView) findViewById(R.id.txt_top_driver_name);
        Tv_driverCity = (TextView) findViewById(R.id.txt_top_driver_city);
        Tv_date = (TextView) findViewById(R.id.txt_month);

        RL_topIncome = (RelativeLayout) findViewById(R.id.Rl_top_income);
        RL_topIncomeEmpty = (RelativeLayout) findViewById(R.id.Rl_empty_top_income);

        barChart = (BarChart) findViewById(R.id.chart_weekly_performance);

        Iv_back.setOnClickListener(this);
        Iv_withdraw.setOnClickListener(this);
        Iv_viewStatement.setOnClickListener(this);

        if (cd.isConnectingToInternet()) {
            DriveAndEarnStatementRequest(Iconstant.drive_and_earn_statement_url);
        } else {
            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }

    }

    private void getIntentData() {

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra("fromPage")) {
                sFromPage = intent.getStringExtra("fromPage");
            }
        }

    }

    private void registerBroadcast() {

        IntentFilter filter = new IntentFilter();
        filter.addAction("com.app.Withdraw.Success");
        withdrawSuccessReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent != null) {

                    // Refresh Page to show Updated Content
                    if (cd.isConnectingToInternet()) {
                        DriveAndEarnStatementRequest(Iconstant.drive_and_earn_statement_url);
                    } else {
                        Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                    }

                }

            }
        };
        registerReceiver(withdrawSuccessReceiver, filter);

    }

    @Override
    public void onClick(View view) {

        if (cd.isConnectingToInternet()) {

            if (view == Iv_back) {
                if (sFromPage.equalsIgnoreCase("GCMPushNotification")) {
                    Intent intent = new Intent(DriveAndEarnStatement.this, Dashboard_constrain.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                }
                finish();

            } else if (view == Iv_withdraw) {

//                confirmPin();

                withdrawAmount();


            } else if (view == Iv_viewStatement) {
                Intent intent = new Intent(DriveAndEarnStatement.this, DriveAndEarnStatementDownload.class);
                intent.putExtra("driverImage", sDriverImage);
                intent.putExtra("referralCode", sReferralCode);
                intent.putExtra("downloadLink", sDownloadUrl);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }

        } else {
            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }

    }


    private void withdrawAmount() {

        if (sAvailableAmount.length() > 0) {

            double totalAmount = Double.parseDouble(sAvailableAmount);

            if (totalAmount > 0) {

                /*Intent intent = new Intent(DriveAndEarnStatement.this, WithdrawalPage.class);
                intent.putExtra("page", "referral_earnings");
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);*/

                if (sProceedWithdrawStatus.equalsIgnoreCase("1")) {
                    if (sWithdrawStatus.equalsIgnoreCase("1")) {
                        confirmPin();
                    } else {
                        Alert(getResources().getString(R.string.action_error), sWithdrawError);
                    }
                } else {
                    Alert(getResources().getString(R.string.action_error), sProceedWithdrawError);
                }

            } else {
                String message = getResources().getString(R.string.earnings_page_label_no_amount_to_withdraw);
                Alert(getResources().getString(R.string.action_error), message);
            }

        }

    }

    //--------------Alert----------------
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(DriveAndEarnStatement.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    private void confirmPin() {
        final Dialog confirmPinDialog = new Dialog(DriveAndEarnStatement.this, R.style.SlideRightDialog);
        confirmPinDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmPinDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmPinDialog.setCancelable(true);
        confirmPinDialog.setContentView(R.layout.alert_enter_pin);

        final PinEntryEditText Ed_pin = (PinEntryEditText) confirmPinDialog.findViewById(R.id.edt_withdrawal_amount);
        final TextView Tv_forgotPin = (TextView) confirmPinDialog.findViewById(R.id.txt_label_forgot_pin);
        final RelativeLayout RL_confirm = (RelativeLayout) confirmPinDialog.findViewById(R.id.Rl_confirm);
        final ImageView Iv_close = (ImageView) confirmPinDialog.findViewById(R.id.img_close);

        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmPinDialog.dismiss();
            }
        });

        Tv_forgotPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (cd.isConnectingToInternet()) {
                    ForgotPinRequest(Iconstant.forgot_pin_url);
                } else {
                    Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                }

            }
        });

        RL_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    CloseKeyboard(Ed_pin);
                } catch (Exception e) {

                }

                if (confirmPinDialog != null && confirmPinDialog.isShowing()) {
                    confirmPinDialog.dismiss();
                }

                if (sSecurePin.equalsIgnoreCase(Ed_pin.getText().toString())) {

                    sPayAmount = sAvailableAmount;

                    if (cd.isConnectingToInternet()) {
                        WithdrawalAmount(Iconstant.referral_withdraw_amount_url);
                    }

                } else {
                    AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.label_incorrect_pin));
                }

            }
        });

        confirmPinDialog.show();

    }


    @SuppressLint("WrongConstant")
    private void ForgotPinRequest(String Url) {

        dialog = new Dialog(DriveAndEarnStatement.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriveID);

        System.out.println("--------------ForgotPinRequest Url-------------------" + Url);
        System.out.println("--------------ForgotPinRequest jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(DriveAndEarnStatement.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                if (dialog != null) {
                    dialog.dismiss();
                }

                System.out.println("--------------ForgotPinRequest Response-------------------" + response);
                String status = "", sResponse = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");
                        sResponse = object.getString("message");

                        if (status.equalsIgnoreCase("1")) {
                            Alert(getResources().getString(R.string.action_success), sResponse);
                        } else {
                            Alert(getResources().getString(R.string.action_error), sResponse);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    //-----------------------Drive And Earn Statement Post Request-----------------
    private void DriveAndEarnStatementRequest(String Url) {
        final Dialog dialog = new Dialog(DriveAndEarnStatement.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(DriveAndEarnStatement.this.getResources().getString(R.string.action_pleasewait));

        System.out.println("-------------DriveAndEarnStatement Url----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriveID);

        System.out.println("-------------DriveAndEarnStatement Params----------------" + jsonParams);

        mRequest = new ServiceRequest(DriveAndEarnStatement.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------DriveAndEarnStatement Response----------------" + response);
                String Sstatus = "";

                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        if (object.length() > 0) {
                            JSONObject response_Object = object.getJSONObject("response");
                            if (response_Object.length() > 0) {

                                sCurrency = response_Object.getString("currency");
                                sAvailableAmount = response_Object.getString("available_amount");
                                sTodayCount = response_Object.getString("today_count");
                                sWeekCount = response_Object.getString("week_count");
                                sMonthCount = response_Object.getString("month_count");
                                sReferralCode = response_Object.getString("referal_code");
                                sDisplayMonth = response_Object.getString("month");
                                sMinAmount = response_Object.getString("min_amount");
                                sMaxAmount = response_Object.getString("max_amount");
                                sDriverImage = response_Object.getString("image");
                                sDownloadUrl = response_Object.getString("download_url");
                                sProceedWithdrawStatus = response_Object.getString("proceed_status");
                                sProceedWithdrawError = response_Object.getString("error_message");
                                sWithdrawStatus = response_Object.getString("withdraw_status");
                                sWithdrawError = response_Object.getString("withdraw_error");

                                Object objTopIncome = response_Object.get("top_income_this_week");
                                if (objTopIncome instanceof JSONArray) {

                                    JSONArray jarTopIncome = response_Object.getJSONArray("top_income_this_week");
                                    if (jarTopIncome.length() > 0) {
                                        JSONObject jobjTopIncome = jarTopIncome.getJSONObject(0);
                                        if (jobjTopIncome.length() > 0) {
                                            sTopDriverName = jobjTopIncome.getString("driver_name");
                                            sTopDriverImage = jobjTopIncome.getString("image");
                                            sTopDriverLocation = jobjTopIncome.getString("location");

                                            isTopIncomeAvailable = true;

                                        } else {
                                            isTopIncomeAvailable = false;
                                        }
                                    } else {
                                        isTopIncomeAvailable = false;
                                    }
                                }


                                Object objChart = response_Object.get("chart_array");
                                if (objChart instanceof JSONArray) {
                                    JSONArray jarChart = response_Object.getJSONArray("chart_array");
                                    if (jarChart.length() > 0) {

                                        for (int i = 0; i < jarChart.length(); i++) {

                                            JSONObject jobjChart = jarChart.getJSONObject(i);

                                            String day = jobjChart.getString("day");
                                            String ridecount = jobjChart.getString("ridecount");
                                            String distance = jobjChart.getString("distance");

                                            chartList.add(new DriveEarnStatementChart(day, ridecount, distance));

                                        }

                                        isChartDataPresent = true;

                                    } else {
                                        isChartDataPresent = false;
                                    }


                                } else {
                                    isChartDataPresent = false;
                                }


                            } else {
                                isDataPresent = false;
                            }
                        } else {
                            isDataPresent = false;
                        }

                        isDataPresent = true;

                    } else {
                        isDataPresent = false;
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                dialog.dismiss();

                if (Sstatus.equalsIgnoreCase("1")) {

                    if (isDataPresent) {


                        Tv_currentBalance.setText(sCurrency+" " + sAvailableAmount);
//                        Tv_todayTrips.setText(sTodayCount);
//                        Tv_weekTrips.setText(sWeekCount);
//                        Tv_monthTrips.setText(sMonthCount);
                        Tv_date.setText(sDisplayMonth);

                        setTrips(sTodayCount, Tv_todayTrips);
                        setTrips(sWeekCount, Tv_weekTrips);
                        setTrips(sMonthCount, Tv_monthTrips);

                        if (isTopIncomeAvailable) {

                            if (sTopDriverImage.length() > 0) {
                                Picasso.with(DriveAndEarnStatement.this).load(sTopDriverImage).error(R.drawable.no_user_profile).placeholder(R.drawable.no_user_profile).memoryPolicy(MemoryPolicy.NO_CACHE).resize(150, 150).into(Iv_profile);
                            }

                            Tv_driverName.setText(sTopDriverName);
                            Tv_driverCity.setText(sTopDriverLocation);


                        } else {
                            RL_topIncomeEmpty.setVisibility(View.VISIBLE);
                            RL_topIncome.setVisibility(View.GONE);
                        }

                        if (isChartDataPresent) {

                            ArrayList<BarEntry> entries = new ArrayList<BarEntry>();
                            ArrayList<String> labels = new ArrayList<String>();

                            for (int i = 0; i < chartList.size(); i++) {
                                entries.add(new BarEntry(Float.parseFloat(chartList.get(i).getRidecount()), i));
                                labels.add(chartList.get(i).getDay());
                            }

                            if (chartList.size() > 0) {
                                BarDataSet dataSet = new BarDataSet(entries, getResources().getString(R.string.myTrip_label_weeklyPerformance));
                                dataSet.setColor(Color.parseColor("#1F8BCA"));

                                BarData data = new BarData(labels, dataSet);
                                data.setValueTextColor(Color.parseColor("#FFFFFF"));

                                barChart.setGridBackgroundColor(Color.parseColor("#00000000"));
                                barChart.setBackgroundColor(Color.parseColor("#00000000"));
                                barChart.setData(data);
                                barChart.animateY(5000);
                                barChart.setPinchZoom(false);
                                barChart.getLegend().setTextColor(Color.WHITE);
                                barChart.getXAxis().setTextColor(Color.WHITE);
                                barChart.getAxisLeft().setTextColor(Color.WHITE);
                                barChart.getAxisRight().setTextColor(Color.TRANSPARENT);
//                                Description description = new Description();
//                                description.setText("");
//                                barChart.setDescription(description);
                                barChart.setDescription("");

                                XAxis xAxis = barChart.getXAxis();
                                xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                                xAxis.setDrawAxisLine(true);
                                xAxis.setDrawGridLines(true);
                            }
                        }

                    }

                }

            }


            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    @SuppressLint("WrongConstant")
    private void WithdrawalAmount(String Url) {

        dialog = new Dialog(DriveAndEarnStatement.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriveID);
        jsonParams.put("amount", sPayAmount);
        jsonParams.put("mode", "wallet");
//        jsonParams.put("paypal_id", sPaypalID);

        System.out.println("--------------WithdrawalAmount Url-------------------" + Url);
        System.out.println("--------------WithdrawalAmount jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(DriveAndEarnStatement.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------WithdrawalAmount Response-------------------" + response);
                String status = "", sResponse = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");
                        sResponse = object.getString("response");

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (dialog != null) {
                    dialog.dismiss();
                }

                if (status.equalsIgnoreCase("1")) {
                    WithdrawSuccessAlert(sPayAmount, "wallet");
                } else {
                    WithdrawErrorAlert(getResources().getString(R.string.action_error), sResponse);
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    private void WithdrawSuccessAlert(String withdrawAmount, String paymentType) {
        final Dialog withdrawSuccessDialog = new Dialog(DriveAndEarnStatement.this, R.style.SlideUpDialog);
        withdrawSuccessDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        withdrawSuccessDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        withdrawSuccessDialog.setCancelable(true);
        withdrawSuccessDialog.setContentView(R.layout.alert_withdraw_success1);

        final ImageView Iv_close = (ImageView) withdrawSuccessDialog.findViewById(R.id.img_close);
        final TextView Tv_withdrawAmount = (TextView) withdrawSuccessDialog.findViewById(R.id.txt_alert_amount);
        final TextView Tv_paymentType = (TextView) withdrawSuccessDialog.findViewById(R.id.txt_alert_payment_type);

        Tv_withdrawAmount.setText(withdrawAmount);
        paymentType = getResources().getString(R.string.withdrawal_page_label_payment_type) + paymentType;
        Tv_paymentType.setText(paymentType);

        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (withdrawSuccessDialog != null && withdrawSuccessDialog.isShowing()) {
                    withdrawSuccessDialog.dismiss();
                }
            }
        });

        withdrawSuccessDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);

                Intent intent = new Intent();
                intent.setAction("com.app.Withdraw.Success");
                sendBroadcast(intent);
            }
        });

        withdrawSuccessDialog.show();

    }

    private void WithdrawErrorAlert(String message1, String message2) {
        final Dialog withdrawErrorDialog = new Dialog(DriveAndEarnStatement.this, R.style.SlideUpDialog);
        withdrawErrorDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        withdrawErrorDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        withdrawErrorDialog.setCancelable(true);
        withdrawErrorDialog.setContentView(R.layout.alert_withdrawal_errors);

        final ImageView Iv_close = (ImageView) withdrawErrorDialog.findViewById(R.id.img_close);
        final TextView Tv_message1 = (TextView) withdrawErrorDialog.findViewById(R.id.txt_alert_message1);
        final TextView Tv_message2 = (TextView) withdrawErrorDialog.findViewById(R.id.txt_alert_message2);

        Tv_message1.setText(message1);
        Tv_message2.setText(message2);

        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (withdrawErrorDialog != null && withdrawErrorDialog.isShowing()) {
                    withdrawErrorDialog.dismiss();
                }
            }
        });

        withdrawErrorDialog.show();

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (withdrawSuccessReceiver != null) {
            unregisterReceiver(withdrawSuccessReceiver);
        }

    }

    private void setTrips(String sValue, TextView view) {
        int intValue = 0;
        if (sValue != null && sValue.length() > 0) {
            try {
                intValue = Integer.parseInt(sValue);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (intValue > 1) {
                view.setText(intValue + " " + getResources().getString(R.string.mytrips_label_trips));
            } else {
                view.setText(intValue + " " + getResources().getString(R.string.mytrips_label_trip));
            }
        } else {
            view.setText(intValue + " " + getResources().getString(R.string.mytrips_label_trip));
        }
    }


    @SuppressLint("WrongConstant")
    private void CloseKeyboard(EditText edittext) {
        try {
            InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            in.hideSoftInputFromWindow(edittext.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //-----------------Move Back on pressed phone back button------------------
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            if (sFromPage.equalsIgnoreCase("GCMPushNotification")) {
                Intent intent = new Intent(DriveAndEarnStatement.this, Dashboard_constrain.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
            finish();
            return true;
        }
        return false;
    }


    private void AlertError(String title, String message) {

        String msg = title + "\n" + message;

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        AppMsg.Style style = new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red_color);
        AppMsg snack = AppMsg.makeText(DriveAndEarnStatement.this, msg.toUpperCase(), AppMsg.STYLE_ALERT);
        snack.setView(view);
        snack.setLayoutGravity(Gravity.TOP);
        snack.setPriority(AppMsg.PRIORITY_HIGH);
        snack.setAnimation(R.anim.enter, R.anim.exit);
        snack.show();

    }


}