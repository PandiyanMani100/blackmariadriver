package com.blackmaria.partner.app;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.Pojo.IncentiveStatementDetail;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Ganesh on 22-05-2017.
 */

public class IncentiveStatementMonth extends ActivityHockeyApp implements View.OnClickListener {

    private ImageView Iv_back, Iv_home;
    private TextView Tv_totalIncentives, Tv_totalAmount, Tv_totalPayout, Tv_displayMonth;
    private LinearLayout LL_table, LL_views, LL_title, LL1, LL2, LL3, LL4, LL5;
    private RelativeLayout RL_emptyView;
    private ImageView Iv_target1, Iv_target2, Iv_target3, Iv_target4, Iv_target5;
    private TextView Tv_trip1, Tv_trip2, Tv_trip3, Tv_trip4, Tv_trip5;
    private TextView Tv_miles1, Tv_miles2, Tv_miles3, Tv_miles4, Tv_miles5;
    private TextView Tv_scheduled1, Tv_scheduled2, Tv_scheduled3, Tv_scheduled4, Tv_scheduled5;
    private TextView Tv_sos1, Tv_sos2, Tv_sos3, Tv_sos4, Tv_sos5;

    private SessionManager sessionManager;
    private ConnectionDetector cd;
    private Dialog dialog;
    private ServiceRequest mRequest;

    private String sDriverID = "", sFilterMonth = "", sFilterYear = "", sDisplayMonth = "", sTotalIncentives = "", sTotalAmount = "", sTotalPayout = "", sCurrency = "";
    private boolean isDataAvailable = false;
    private ArrayList<IncentiveStatementDetail> listIncentive;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.incentive_statement_new);

        initialize();

    }

    private void initialize() {
        sessionManager = new SessionManager(IncentiveStatementMonth.this);
        cd = new ConnectionDetector(IncentiveStatementMonth.this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        listIncentive = new ArrayList<IncentiveStatementDetail>();

        getIntentData();

        Iv_back = (ImageView) findViewById(R.id.img_back);
        Iv_home = (ImageView) findViewById(R.id.img_home);

        Tv_totalIncentives = (TextView) findViewById(R.id.txt_total_incentives);
        Tv_totalAmount = (TextView) findViewById(R.id.txt_amount);
        Tv_totalPayout = (TextView) findViewById(R.id.txt_total_payout);
        Tv_displayMonth = (TextView) findViewById(R.id.txt_month_incentives);

        LL_table = (LinearLayout) findViewById(R.id.LL_table);
        LL_views = (LinearLayout) findViewById(R.id.LL_views);
        LL_title = (LinearLayout) findViewById(R.id.LL_title);
        RL_emptyView = (RelativeLayout) findViewById(R.id.Rl_empty);
        LL1 = (LinearLayout) findViewById(R.id.LL_1);
        LL2 = (LinearLayout) findViewById(R.id.LL_2);
        LL3 = (LinearLayout) findViewById(R.id.LL_3);
        LL4 = (LinearLayout) findViewById(R.id.LL_4);
        LL5 = (LinearLayout) findViewById(R.id.LL_5);

        Iv_target1 = (ImageView) findViewById(R.id.img_target1);
        Iv_target2 = (ImageView) findViewById(R.id.img_target2);
        Iv_target3 = (ImageView) findViewById(R.id.img_target3);
        Iv_target4 = (ImageView) findViewById(R.id.img_target4);
        Iv_target5 = (ImageView) findViewById(R.id.img_target5);

        Tv_trip1 = (TextView) findViewById(R.id.txt_trip1);
        Tv_trip2 = (TextView) findViewById(R.id.txt_trip2);
        Tv_trip3 = (TextView) findViewById(R.id.txt_trip3);
        Tv_trip4 = (TextView) findViewById(R.id.txt_trip4);
        Tv_trip5 = (TextView) findViewById(R.id.txt_trip5);

        Tv_miles1 = (TextView) findViewById(R.id.txt_miles1);
        Tv_miles2 = (TextView) findViewById(R.id.txt_miles2);
        Tv_miles3 = (TextView) findViewById(R.id.txt_miles3);
        Tv_miles4 = (TextView) findViewById(R.id.txt_miles4);
        Tv_miles5 = (TextView) findViewById(R.id.txt_miles5);

        Tv_scheduled1 = (TextView) findViewById(R.id.txt_scheduled1);
        Tv_scheduled2 = (TextView) findViewById(R.id.txt_scheduled2);
        Tv_scheduled3 = (TextView) findViewById(R.id.txt_scheduled3);
        Tv_scheduled4 = (TextView) findViewById(R.id.txt_scheduled4);
        Tv_scheduled5 = (TextView) findViewById(R.id.txt_scheduled5);

        Tv_sos1 = (TextView) findViewById(R.id.txt_sos1);
        Tv_sos2 = (TextView) findViewById(R.id.txt_sos2);
        Tv_sos3 = (TextView) findViewById(R.id.txt_sos3);
        Tv_sos4 = (TextView) findViewById(R.id.txt_sos4);
        Tv_sos5 = (TextView) findViewById(R.id.txt_sos5);

        Iv_back.setOnClickListener(this);
        Iv_home.setOnClickListener(this);

        if (cd.isConnectingToInternet()) {
            IncentivesStatementRequest(Iconstant.incentives_list_url);
        } else {
            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }


    }

    private void getIntentData() {

        Intent intent = getIntent();
        if (intent != null) {
            sFilterMonth = intent.getStringExtra("filterMonth");
            sFilterYear = intent.getStringExtra("filterYear");
            sDisplayMonth = intent.getStringExtra("displayMonth");
        }

    }


    @SuppressLint("WrongConstant")
    private void setTableData() {

        if (isDataAvailable) {

            if (listIncentive.size() == 4) {

                LinearLayout.LayoutParams titleParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0);
                titleParam.weight = 0.6f;

                LinearLayout.LayoutParams valuesParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0);
                valuesParam.weight = 1.3f;

                LL_title.setLayoutParams(titleParam);

                LL1.setLayoutParams(valuesParam);
                LL2.setLayoutParams(valuesParam);
                LL3.setLayoutParams(valuesParam);
                LL4.setLayoutParams(valuesParam);

                LL5.setVisibility(View.GONE);

            }

            for (int i = 0; i < listIncentive.size(); i++) {

                switch (i) {
                    case 0:
                        if (listIncentive.get(0).getWeek_trip().length() > 0 && !listIncentive.get(0).getWeek_trip().equalsIgnoreCase("-")) {
                            Iv_target1.setImageResource(R.drawable.white_tick1);
                        } else {
                            Iv_target1.setImageResource(R.drawable.icon_close1);
                        }
                        Tv_trip1.setText(listIncentive.get(0).getDaily_trip());
                        Tv_miles1.setText(listIncentive.get(0).getDaily_mile());
                        Tv_scheduled1.setText(listIncentive.get(0).getSchedule_hours());
                        Tv_sos1.setText(listIncentive.get(0).getSos());
                        break;
                    case 1:
                        if (listIncentive.get(1).getWeek_trip().length() > 0 && !listIncentive.get(1).getWeek_trip().equalsIgnoreCase("-")) {
                            Iv_target2.setImageResource(R.drawable.white_tick1);
                        } else {
                            Iv_target2.setImageResource(R.drawable.icon_close1);
                        }
                        Tv_trip2.setText(listIncentive.get(1).getDaily_trip());
                        Tv_miles2.setText(listIncentive.get(1).getDaily_mile());
                        Tv_scheduled2.setText(listIncentive.get(1).getSchedule_hours());
                        Tv_sos2.setText(listIncentive.get(1).getSos());
                        break;
                    case 2:
                        if (listIncentive.get(2).getWeek_trip().length() > 0 && !listIncentive.get(2).getWeek_trip().equalsIgnoreCase("-")) {
                            Iv_target3.setImageResource(R.drawable.white_tick1);
                        } else {
                            Iv_target3.setImageResource(R.drawable.icon_close1);
                        }
                        Tv_trip3.setText(listIncentive.get(2).getDaily_trip());
                        Tv_miles3.setText(listIncentive.get(2).getDaily_mile());
                        Tv_scheduled3.setText(listIncentive.get(2).getSchedule_hours());
                        Tv_sos3.setText(listIncentive.get(2).getSos());
                        break;
                    case 3:
                        if (listIncentive.get(3).getWeek_trip().length() > 0 && !listIncentive.get(3).getWeek_trip().equalsIgnoreCase("-")) {
                            Iv_target4.setImageResource(R.drawable.white_tick1);
                        } else {
                            Iv_target4.setImageResource(R.drawable.icon_close1);
                        }
                        Tv_trip4.setText(listIncentive.get(3).getDaily_trip());
                        Tv_miles4.setText(listIncentive.get(3).getDaily_mile());
                        Tv_scheduled4.setText(listIncentive.get(3).getSchedule_hours());
                        Tv_sos4.setText(listIncentive.get(3).getSos());
                        break;
                    case 4:
                        if (listIncentive.get(4).getWeek_trip().length() > 0 && !listIncentive.get(4).getWeek_trip().equalsIgnoreCase("-")) {
                            Iv_target5.setImageResource(R.drawable.white_tick1);
                        } else {
                            Iv_target5.setImageResource(R.drawable.icon_close1);
                        }
                        Tv_trip5.setText(listIncentive.get(4).getDaily_trip());
                        Tv_miles5.setText(listIncentive.get(4).getDaily_mile());
                        Tv_scheduled5.setText(listIncentive.get(4).getSchedule_hours());
                        Tv_sos5.setText(listIncentive.get(4).getSos());
                        break;

                }

            }

        } else {
            LL_table.setVisibility(View.GONE);
            LL_views.setVisibility(View.GONE);
            RL_emptyView.setVisibility(View.VISIBLE);
        }

    }

    @SuppressLint("SetTextI18n")
    private void IncentivesStatementRequest(String Url) {

        dialog = new Dialog(IncentiveStatementMonth.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        jsonParams.put("month", sFilterMonth);
        jsonParams.put("year", sFilterYear);

        System.out.println("--------------DriverIncentivesStatement Url-------------------" + Url);
        System.out.println("--------------DriverIncentivesStatement jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(IncentiveStatementMonth.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------DriverIncentivesStatement Response-------------------" + response);
                String status = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {
                            JSONObject jsonObject = object.getJSONObject("response");

                            if (jsonObject.length() > 0) {

                                sCurrency = jsonObject.getString("currency");
                                sTotalAmount = sCurrency +" "+ jsonObject.getString("total_amount");
                                sTotalPayout = sCurrency +" "+ jsonObject.getString("total_payout_amount");
                                sTotalIncentives = jsonObject.getString("total_count");

                                Object incentives_array_check = jsonObject.get("incetives");
                                if (incentives_array_check instanceof JSONArray) {
                                    JSONArray incentivesArray = jsonObject.getJSONArray("incetives");
                                    if (incentivesArray.length() > 0) {
                                        listIncentive = new ArrayList<IncentiveStatementDetail>();

                                        for (int i = 0; i < incentivesArray.length(); i++) {

                                            JSONObject incentiveObject = incentivesArray.getJSONObject(i);

                                            String week = "", week_trip = "", daily_trip = "-", daily_mile = "-", schedule_hours = "-", sos = "-";

                                            if (incentiveObject.has("week")) {
                                                week = incentiveObject.getString("week");
                                            }
                                            if (incentiveObject.has("week_trip")) {
                                                week_trip = incentiveObject.getString("week_trip");
                                            }
                                            if (incentiveObject.has("daily_trip")) {
                                                daily_trip = incentiveObject.getString("daily_trip");
                                            }
                                            if (incentiveObject.has("daily_mile")) {
                                                daily_mile = incentiveObject.getString("daily_mile");
                                            }
                                            if (incentiveObject.has("schedule_hours")) {
                                                schedule_hours = incentiveObject.getString("schedule_hours");
                                            }
                                            if (incentiveObject.has("sos")) {
                                                sos = incentiveObject.getString("sos");
                                            }

                                            listIncentive.add(new IncentiveStatementDetail(week, week_trip, daily_trip, daily_mile, schedule_hours, sos));

                                        }

                                        isDataAvailable = true;
                                    } else {
                                        isDataAvailable = false;
                                    }
                                } else {
                                    isDataAvailable = false;
                                }

                            }

                        } else {
                            String sResponse = object.getString("response");
                            Alert(getResources().getString(R.string.action_error), sResponse);
                        }


                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (status.equalsIgnoreCase("1")) {

                    setTableData();

                    Tv_totalIncentives.setText(sTotalIncentives);
                    Tv_totalAmount.setText(sTotalAmount);
                    Tv_totalPayout.setText(sTotalPayout);
                    Tv_displayMonth.setText(sDisplayMonth + " " + getString(R.string.driver_homepage_label_incentives));

                }

                if (dialog != null) {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(IncentiveStatementMonth.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    @Override
    public void onClick(View view) {

        if (view == Iv_back || view == Iv_home) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }

    }

}
