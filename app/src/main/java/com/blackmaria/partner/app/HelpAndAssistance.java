package com.blackmaria.partner.app;

import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.DowloadPdfActivity;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;

/**
 * Created by GANESH on 29-08-2017.
 */

public class HelpAndAssistance extends ActivityHockeyApp implements View.OnClickListener {

    private RelativeLayout RL_home, RL_booking, RL_earnings, RL_rulesOfRoad, RL_userGuides;

    private ConnectionDetector cd;
    private SessionManager session;
    DownloadManager downloadManager;
    private long myDownloadReference;
    private BroadcastReceiver receiverDownloadComplete, receiverNotificationClicked;
    DownloadManager.Request request;

    private String sUserGuideLink = "";
    private boolean isReceiversRegistered = false, isAlertShowing = false;
private LinearLayout complaint_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help_and_assistance);

        initialize();

    }

    private void initialize() {

        cd = new ConnectionDetector(HelpAndAssistance.this);
        session = new SessionManager(HelpAndAssistance.this);
        sUserGuideLink = session.getUserGuide();
        downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);

        RL_home = (RelativeLayout) findViewById(R.id.RL_home);
        RL_booking = (RelativeLayout) findViewById(R.id.RL_booking);
        RL_earnings = (RelativeLayout) findViewById(R.id.RL_earnings);
        RL_rulesOfRoad = (RelativeLayout) findViewById(R.id.RL_rules_of_road);
        RL_userGuides = (RelativeLayout) findViewById(R.id.RL_user_guides);
        complaint_layout = (LinearLayout) findViewById(R.id.complaint_layout);

        RL_home.setOnClickListener(this);
        RL_booking.setOnClickListener(this);
        RL_earnings.setOnClickListener(this);
        RL_rulesOfRoad.setOnClickListener(this);
        RL_userGuides.setOnClickListener(this);
        complaint_layout.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if (cd.isConnectingToInternet()) {

            if (view == RL_home) {
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            } else if (view == RL_booking) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(Iconstant.about_booking_url));
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            } else if (view == RL_earnings) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(Iconstant.earning_url));
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            } else if (view == RL_rulesOfRoad) {
                Intent intent = new Intent(HelpAndAssistance.this, RulesOfRoad.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            } else if (view == RL_userGuides) {
                if (sUserGuideLink.length() > 0) {
                    new DowloadPdfActivity(HelpAndAssistance.this, sUserGuideLink);
                }
            }else if (view == complaint_layout) {
                Intent intent = new Intent(HelpAndAssistance.this, ComplaintPage.class);
                startActivity(intent);
                overridePendingTransition(R.anim.exit, R.anim.enter);
            }

        } else {
            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }

    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(HelpAndAssistance.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    private void downloadFile(String docLink) {

        if (cd.isConnectingToInternet()) {

            RL_userGuides.setClickable(false);
            Uri uri = Uri.parse(docLink);


            String docName = "UserGuide_" + System.currentTimeMillis();

            request = new DownloadManager.Request(uri);
            MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
            String mimeString = mimeTypeMap.getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(docLink));
            System.out.println("MimeString = " + mimeString);
            request.setMimeType(mimeString);
//            request.setMimeType("application/pdf");
            request.setDescription("Downloading" + " " + docName);
            request.setTitle(docName);
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, docName);

            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE);
//            request.setShowRunningNotification(true);
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE | DownloadManager.Request.NETWORK_WIFI);

            myDownloadReference = downloadManager.enqueue(request);
            Toast.makeText(HelpAndAssistance.this, getString(R.string.downloading), Toast.LENGTH_SHORT).show();

            IntentFilter filter = new IntentFilter(DownloadManager.ACTION_NOTIFICATION_CLICKED);

            receiverNotificationClicked = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String extraId = DownloadManager.EXTRA_NOTIFICATION_CLICK_DOWNLOAD_IDS;
                    long references[] = intent.getLongArrayExtra(extraId);
                    for (long reference : references) {
                        // do something with the download file
                    }
                }
            };

            IntentFilter intentFilter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);

            receiverDownloadComplete = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    long reference = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
                    if (myDownloadReference == reference) {
                        DownloadManager.Query query = new DownloadManager.Query();
                        query.setFilterById(reference);
                        Cursor cursor = downloadManager.query(query);

                        cursor.moveToFirst();

                        int columnIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);

                        int status = cursor.getInt(columnIndex);

                        int fileNameIndex = cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_FILENAME);

                        String filePath = cursor.getString(fileNameIndex);

                        int columnReason = cursor.getColumnIndex(DownloadManager.COLUMN_REASON);

                        int reason = cursor.getInt(columnReason);

                        switch (status) {
                            case DownloadManager.STATUS_SUCCESSFUL:
                                RL_userGuides.setClickable(true);
//                                Toast.makeText(HelpAndAssistance.this, getString(R.string.download_success), Toast.LENGTH_SHORT).show();
                                String action = intent.getAction();
                                if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                                    AlertDownloadCompleted(getResources().getString(R.string.action_success), getResources().getString(R.string.help_page_guide_download_completed), intent);
                                }
                                break;
                            case DownloadManager.STATUS_FAILED:
                                RL_userGuides.setClickable(true);
                                Toast.makeText(HelpAndAssistance.this, getString(R.string.download_success), Toast.LENGTH_SHORT).show();
                                break;
                            case DownloadManager.STATUS_PAUSED:
                                Toast.makeText(HelpAndAssistance.this, getString(R.string.download_paused), Toast.LENGTH_SHORT).show();
                                break;
                            case DownloadManager.STATUS_PENDING:
                                Toast.makeText(HelpAndAssistance.this, getString(R.string.download_pending), Toast.LENGTH_SHORT).show();
                                break;
                            case DownloadManager.STATUS_RUNNING:
//                                Toast.makeText(HelpAndAssistance.this, getString(R.string.downloading), Toast.LENGTH_SHORT).show();
                                break;
                        }

                    }
                }
            };

            registerReceiver(receiverNotificationClicked, filter);
            registerReceiver(receiverDownloadComplete, intentFilter);
            isReceiversRegistered = true;
        } else {
            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }

    }

    private void AlertDownloadCompleted(String title, String alert, final Intent intent) {
        final PkDialog mDialog = new PkDialog(HelpAndAssistance.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_yes), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

                long downloadId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0);
                DownloadManager.Query query = new DownloadManager.Query();
                query.setFilterById(downloadId);
                downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                Cursor c = downloadManager.query(query);
                if (c.moveToFirst()) {
                    int columnIndex = c.getColumnIndex(DownloadManager.COLUMN_STATUS);
                    if (DownloadManager.STATUS_SUCCESSFUL == c.getInt(columnIndex)) {
                        String uriString = c.getString(c.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
                        //TODO : Use this local uri and launch intent to open file

                        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
                        String mimeString = mimeTypeMap.getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(sUserGuideLink));
                        System.out.println("MimeString = " + mimeString);

                        try {

                            Uri uri = Uri.parse(uriString);
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_VIEW);
//                        intent.setDataAndType(uri, "application/pdf");
                            intent.setDataAndType(uri, mimeString);
                            startActivity(intent);
                            overridePendingTransition(R.anim.enter, R.anim.exit);

                        } catch (ActivityNotFoundException e) {
                            Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.cannot_read_file_type));
                        }

                    }
                }

            }
        });
        mDialog.setNegativeButton(getResources().getString(R.string.action_no), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });

        mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                isAlertShowing = false;
            }
        });

        if (!isAlertShowing) {
            mDialog.show();
            isAlertShowing = true;
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (receiverNotificationClicked != null && isReceiversRegistered) {
            unregisterReceiver(receiverNotificationClicked);
        }
        if (receiverDownloadComplete != null && isReceiversRegistered) {
            unregisterReceiver(receiverDownloadComplete);
        }
    }


}