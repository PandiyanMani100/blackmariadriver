package com.blackmaria.partner.app;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Prem Kumar and Anitha on 1/2/2017.
 */

public class MyTripToday extends ActivityHockeyApp implements View.OnClickListener {

    private TextView Tv_tripDate, Tv_tripCompleted, Tv_tripCancelled, Tv_biddingTrip, Tv_sosTrip;
    private ImageView Iv_back, Iv_home;
    private ConnectionDetector cd;
    private boolean isInternetPresent = false;
    private SessionManager sessionManager;
    private String sDriveID = "";

    private ServiceRequest mRequest;
    Dialog dialog;
    private boolean isDataAvailable = false;
    private LinearLayout view_detail;

    private String sCompletedTrips = "", sCancelledTrips = "", sBiddingTrips = "", sSOSTrips = "", sFilterDate = "", sDisplayDate = "", sCurrentDate = "";
    private Calendar calendar = null;
    private SimpleDateFormat mdFormat;
    private ImageView Iv_prev, Iv_next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trip_today);
        initialize();
    }

    private void initialize() {
        sessionManager = new SessionManager(MyTripToday.this);
        cd = new ConnectionDetector(MyTripToday.this);
        isInternetPresent = cd.isConnectingToInternet();

        // Steps to Get Current Date
        calendar = Calendar.getInstance();
        mdFormat = new SimpleDateFormat("yyyy-MM-dd");
        sFilterDate = mdFormat.format(calendar.getTime());
        sCurrentDate = sFilterDate;

        Tv_tripDate = (TextView) findViewById(R.id.txt_date);
        Tv_tripCompleted = (TextView) findViewById(R.id.tv_completed);
        Tv_tripCancelled = (TextView) findViewById(R.id.tv_cancel_trip);
        Tv_biddingTrip = (TextView) findViewById(R.id.tv_bidding_trip);
        Tv_sosTrip = (TextView) findViewById(R.id.tv_sos_trip);
        view_detail = (LinearLayout) findViewById(R.id.ll_view_detail);
        Iv_back = (ImageView) findViewById(R.id.img_back);
        Iv_home = (ImageView) findViewById(R.id.img_home);

        Iv_prev = (ImageView) findViewById(R.id.img_left);
        Iv_next = (ImageView) findViewById(R.id.img_right);

        Iv_prev.setOnClickListener(this);
        Iv_next.setOnClickListener(this);
        Iv_home.setOnClickListener(this);
        Iv_back.setOnClickListener(this);
        view_detail.setOnClickListener(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriveID = user.get(SessionManager.KEY_DRIVERID);

        if (isInternetPresent) {
            MyTripDetailRequest(Iconstant.today_trips_url);
        } else {
            alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }
    }

    @SuppressLint("WrongConstant")
    @Override
    public void onClick(View view) {
        if (view == Iv_back) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (view == Iv_home) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (view == view_detail) {
            Intent intent = new Intent(MyTripToday.this, MyTripTodayDetail.class);
            intent.putExtra("filterDate", sFilterDate);
            intent.putExtra("todayFlag", "1");
//            intent.putExtra("displayDate", sDisplayDate);
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (view == Iv_prev) {
            if (calendar != null) {
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {

                    Date date = null;
                    try {
                        date = mdFormat.parse(sFilterDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if (date != null) {
                        calendar.setTime(date);
                        calendar.add(Calendar.DATE, -1);
                        sFilterDate = mdFormat.format(calendar.getTime());
                    }

                    MyTripDetailRequest(Iconstant.today_trips_url);

                } else {
                    alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                }

            }
        } else if (view == Iv_next) {
            if (calendar != null) {
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {

                    Date date = null;
                    try {
                        date = mdFormat.parse(sFilterDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if (date != null) {
                        calendar.setTime(date);
                        calendar.add(Calendar.DATE, 1);
                        sFilterDate = mdFormat.format(calendar.getTime());
                    }

                    MyTripDetailRequest(Iconstant.today_trips_url);

                } else {
                    alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                }

            }
        }
    }

    private void alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(MyTripToday.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void MyTripDetailRequest(String Url) {

        dialog = new Dialog(MyTripToday.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriveID);
        jsonParams.put("filter_date", sFilterDate);

        System.out.println("--------------TodayTrips Url-------------------" + Url);
        System.out.println("--------------TodayTrips jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(MyTripToday.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------TodayTrips Response-------------------" + response);
                String status = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {
                            JSONObject jsonObject = object.getJSONObject("response");
                            if (jsonObject.length() > 0) {

                                JSONObject jobjToday = jsonObject.getJSONObject("today");
                                if (jobjToday.length() > 0) {
                                    sCompletedTrips = jobjToday.getString("completed_rides");
                                    sBiddingTrips = jobjToday.getString("bidding_trip");
                                    sSOSTrips = jobjToday.getString("sos_trip");
                                    sCancelledTrips = jobjToday.getString("cancel_rides");
                                }
                                sDisplayDate = jsonObject.getString("today_date");

                            }
                        } else {
                            String sResponse = object.getString("response");
                            alert(getResources().getString(R.string.action_error), sResponse);
                        }

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (status.equalsIgnoreCase("1")) {

                    Tv_tripDate.setText(getResources().getString(R.string.mytrips_label_as) + " " + sDisplayDate);

                    Tv_sosTrip.setText(sSOSTrips);
                    Tv_tripCompleted.setText(sCompletedTrips);
                    Tv_tripCancelled.setText(sCancelledTrips);
                    Tv_biddingTrip.setText(sBiddingTrips);

                    if (sFilterDate.equalsIgnoreCase(sCurrentDate)) {
                        Iv_next.setVisibility(View.GONE);
                    } else {
                        Iv_next.setVisibility(View.VISIBLE);
                    }

                }

                if (dialog != null) {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
            return true;
        }
        return false;
    }


}
