package com.blackmaria.partner.app;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.Interface.GoDetailComplaint;
import com.blackmaria.partner.Pojo.ComplaintsListPojo;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.adapter.ComplaintsListAdaptere;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.CustomTextView;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.blackmaria.partner.latestpackageview.widgets.TextRCBold;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Muruganantham on 3/8/2018.
 */

@SuppressLint("Registered")
public class ComplaintListPage extends ActivityHockeyApp implements GoDetailComplaint {
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    Dialog dialog;
    private SessionManager session;

    private Button mailesRewardsBack;
    private RelativeLayout complaintvalue;
    private CustomTextView valueReporttext;
    private TextRCBold complaintText;
    private ListView rewardlistview;

    ArrayList<ComplaintsListPojo> ComplaintsList1 = null;
    private boolean isComplaintsAvailable;

    private String UserID = "";
    private String complaintsType = "", cmpCounts = "";
    private String complainttype = "";
    ComplaintsListAdaptere adapetr;
    AccessLanguagefromlocaldb db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.complaint_listpage);
        db = new AccessLanguagefromlocaldb(this);
        initialize();
    }


    private void initialize() {
        cd = new ConnectionDetector(ComplaintListPage.this);
        isInternetPresent = cd.isConnectingToInternet();
        session = new SessionManager(ComplaintListPage.this);

        ComplaintsList1 = new ArrayList<>();

        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_DRIVERID);




        mailesRewardsBack = (Button) findViewById(R.id.mailes_rewards_back);
        complaintvalue = (RelativeLayout) findViewById(R.id.complaintvalue);
        valueReporttext = (CustomTextView) findViewById(R.id.value_reporttext);
        complaintText = (TextRCBold) findViewById(R.id.complaint_text);
        rewardlistview = (ListView) findViewById(R.id.rewardlistview);

        mailesRewardsBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        Intent in = getIntent();
        complaintsType = in.getStringExtra("typeOfCase");
        cmpCounts = in.getStringExtra("cmpCounts");
        complaintText.setText(complaintsType);
        valueReporttext.setText(cmpCounts);


        if (complaintsType.equalsIgnoreCase("COMPLAINTS")) {
            complainttype = "all";
        } else {
            complainttype = complaintsType.toLowerCase();
        }
        PostRequest();
    }

    private void PostRequest() {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", UserID);//"584aa27ccae2aa741a00002f");
        jsonParams.put("type", complainttype);

        System.out.println("-------------ComplaintsList jsonParams----------------" + jsonParams);
        if (isInternetPresent) {
            postRequest_ComplaintsList(Iconstant.complaint_list, jsonParams);
        } else {
            Alert(db.getvalue("action_error"), db.getvalue("alert_nointernet"));
        }

    }

    //-----------------------viewTicket Post Request-----------------
    private void postRequest_ComplaintsList(String Url, HashMap<String, String> jsonParams) {
        dialog = new Dialog(ComplaintListPage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(db.getvalue("action_pleasewait"));
        System.out.println("-------------ComplaintsList Url----------------" + Url);


        System.out.println("-------------ComplaintsList jsonParams----------------" + jsonParams);
        mRequest = new ServiceRequest(ComplaintListPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------ComplaintsList Response----------------" + response);
                String Sstatus = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    JSONObject resObj = object.getJSONObject("response");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        Object check_object1 = resObj.get("ticket_list");

                        valueReporttext.setText(resObj.getString("total_ticket_Count"));

                        if (check_object1 instanceof JSONArray) {
                            JSONArray ticketArray = resObj.getJSONArray("ticket_list");
                            if (ticketArray.length() > 0) {
                                ComplaintsList1.clear();
                                for (int i = 0; i < ticketArray.length(); i++) {

                                    JSONObject complaintList = ticketArray.getJSONObject(i);
                                    ComplaintsListPojo complaintsPojo = new ComplaintsListPojo();
                                    complaintsPojo.setSubName(complaintList.getString("subject"));
                                    complaintsPojo.setTickNumer(complaintList.getString("ticket_number"));
                                    complaintsPojo.setTicketStatus(complaintList.getString("ticket_status"));
                                    complaintsPojo.setUserImage(complaintList.getString("user_image"));
                                    if (complaintList.has("ticket_date")) {
                                        complaintsPojo.setDateTime("Date " + complaintList.getString("ticket_date"));
                                    } else {
                                        complaintsPojo.setDateTime("");
                                    }
                                    ComplaintsList1.add(complaintsPojo);
                                }
                                isComplaintsAvailable = true;
                            } else {
                                isComplaintsAvailable = false;
                            }

                        }
                    } else {
                        String Sresponse = object.getString("response");
                        Alert(db.getvalue("alert_label_title"), Sresponse);
                    }
                    if (Sstatus.equalsIgnoreCase("1")) {
                        adapetr = new ComplaintsListAdaptere(ComplaintListPage.this, ComplaintsList1, complaintsType,ComplaintListPage.this);
                        rewardlistview.setAdapter(adapetr);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }




    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(ComplaintListPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(db.getvalue("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    @Override
    public void goComplaintDetail(int position) {
        Intent intent1 = new Intent(ComplaintListPage.this, ComplaintPageViewDetailPage.class);
        intent1.putExtra("frompage", "complaintlist");
        intent1.putExtra("ticket_id", ComplaintsList1.get(position).getTickNumer());
        intent1.putExtra("ticket_Status", ComplaintsList1.get(position).getTicketStatus());
        startActivity(intent1);
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }
}
