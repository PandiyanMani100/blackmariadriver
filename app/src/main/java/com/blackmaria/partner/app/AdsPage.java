package com.blackmaria.partner.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.R;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;


public class AdsPage extends ActivityHockeyApp {

    private ImageView Iv_close, Iv_banner;
    private TextView Tv_title, Tv_message;
    //    private View Vi_space;
    private String sTitle = "", sMessage = "", mBanner = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ads_page);
        initialize();

        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

    }

    private void initialize() {
        Iv_close = (ImageView) findViewById(R.id.img_close);
        Iv_banner = (ImageView) findViewById(R.id.img_header);
        Tv_title = (TextView) findViewById(R.id.txt_title);
        Tv_message = (TextView) findViewById(R.id.txt_message);
//        Vi_space = (View) findViewById(R.id.ads_page_view);

        Intent intent = getIntent();

        if (intent.hasExtra("AdsTitle")) {
            sTitle = intent.getStringExtra("AdsTitle");
        }

        if (intent.hasExtra("AdsMessage")) {
            sMessage = intent.getStringExtra("AdsMessage");
        }

        if (intent.hasExtra("AdsBanner")) {
            mBanner = intent.getStringExtra("AdsBanner");
        }

        if (mBanner.length() > 0) {
//            Iv_banner.setVisibility(View.VISIBLE);
//            Vi_space.setVisibility(View.GONE);

            if (mBanner.length() > 0) {

                Picasso.with(AdsPage.this).load(mBanner).error(R.drawable.messagepopupimage).placeholder(R.drawable.messagepopupimage).fit().memoryPolicy(MemoryPolicy.NO_CACHE).into(Iv_banner);
            }

        } /*else {
            Iv_banner.setVisibility(View.GONE);
//            Vi_space.setVisibility(View.VISIBLE);
        }*/

        Tv_title.setText(sTitle);
        Tv_message.setText(sMessage);

    }


    //--------Move Back on pressed phone back button------
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            finish();
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            return true;
        }
        return false;
    }

}