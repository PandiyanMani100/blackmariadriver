package com.blackmaria.partner.app;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.Pojo.WalletMoneyPojo;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.adapter.WithdrawPaymentGridAdapter;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightGridView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Ganesh on 16-05-2017.
 */

public class WithdrawalPage extends ActivityHockeyApp implements View.OnClickListener {

    private ImageView Iv_back, Iv_home, Iv_light;
    private TextView Tv_confirm, Tv_availableAmount, Tv_lastWithdrawal, Tv_moreInfo;
    private RelativeLayout RL_confirm;
    private ExpandableHeightGridView Gv_paymentOptions;

    private SessionManager sessionManager;
    private ConnectionDetector cd;
    private Dialog dialog;
    private ServiceRequest mRequest;
    private WithdrawPaymentGridAdapter adapter;

    private String sDriverID = "", sCurrency = "", sLastWithdrawal = "", sMinAmount = "", sMaxAmount = "", sAvailableAmount = "",
            sContactNumber = "", sPayAmount = "", sSelectedPayment = "", sPaypalID = "", sSelectedPaymentName = "", sProceedStatus = "", sProceedError = "";
    private double MaxAmount = 0.0, MinAmount = 0.0;
    private String sFromPage = "";
    private ArrayList<WalletMoneyPojo> listPayment;
    final int PERMISSION_REQUEST_CODE = 111;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.withdrawal);

        initialize();

    }

    private void initialize() {

        sessionManager = new SessionManager(WithdrawalPage.this);
        cd = new ConnectionDetector(WithdrawalPage.this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        sContactNumber = sessionManager.getContactNumber();
        listPayment = new ArrayList<WalletMoneyPojo>();

        // Get Values From Intent
        getIntentData();


        Iv_back = (ImageView) findViewById(R.id.img_back);
        Iv_home = (ImageView) findViewById(R.id.img_home);
        Iv_light = (ImageView) findViewById(R.id.img_blink);
        startBlinkingAnimation();
        Tv_confirm = (TextView) findViewById(R.id.txt_label_confirm);
        RL_confirm = (RelativeLayout) findViewById(R.id.Rl_confirm);
        Gv_paymentOptions = (ExpandableHeightGridView) findViewById(R.id.grid_payment_options);
        Tv_availableAmount = (TextView) findViewById(R.id.txt_withdrawal_amount);
        Tv_lastWithdrawal = (TextView) findViewById(R.id.txt_label_last_withdrawal);
        Tv_moreInfo = (TextView) findViewById(R.id.txt_label_more_info);

        Iv_back.setOnClickListener(this);
        Iv_home.setOnClickListener(this);
//        Tv_confirm.setOnClickListener(this);
        RL_confirm.setOnClickListener(this);
        Tv_moreInfo.setOnClickListener(this);


        Gv_paymentOptions.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (listPayment.get(position).getPaymnet_name().equalsIgnoreCase(getResources().getString(R.string.withdrawal_page_label_bwallet))) {
                    String selectedPaymentId = listPayment.get(position).getPayment_code();
                    sSelectedPaymentName = listPayment.get(position).getPaymnet_name();
                    for (int i = 0; i < listPayment.size(); i++) {
                        listPayment.get(i).setPayment_selected_payment_id(selectedPaymentId);
                    }

                    adapter.notifyDataSetChanged();

                    sPaypalID = "";
                    if (sSelectedPaymentName.equalsIgnoreCase("paypal")) {
                        showPaypalDialog();
                    }
                } else {
                    String paymentName = listPayment.get(position).getPaymnet_name();
                    Alert("", String.format(getResources().getString(R.string.withdrawal_page_option_not_available), paymentName));
                }
            }
        });

        if (cd.isConnectingToInternet()) {
            if (sFromPage.equalsIgnoreCase("earnings")) {
                GetWithdrawalData(Iconstant.earnings_withdraw_url);
            } else if (sFromPage.equalsIgnoreCase("incentives")) {
                GetWithdrawalData(Iconstant.incentives_withdraw_url);
            } else if (sFromPage.equalsIgnoreCase("referral_earnings")) {
                GetWithdrawalData(Iconstant.referral_withdraw_url);
            }
        } else {
            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }

    }

    private void getIntentData() {

        Intent intent = getIntent();
        if (intent != null) {

            if (intent.hasExtra("page")) {
                sFromPage = intent.getStringExtra("page");
            }

        }

    }


    @Override
    public void onClick(View view) {

        if (view == Iv_back) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);

            Intent intent = new Intent();
            intent.setAction("com.app.Withdraw.Success");
            sendBroadcast(intent);

        } else if (view == Iv_home) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);

            Intent intent = new Intent();
            intent.setAction("com.app.Withdraw.Success");
            sendBroadcast(intent);

        } else if (view == RL_confirm) {

            if (cd.isConnectingToInternet()) {

                sPayAmount = Tv_availableAmount.getText().toString().trim().replace(sCurrency, "");
                if (sPayAmount.length() > 0) {

                    try {

                        if (sProceedStatus.equalsIgnoreCase("1")) {

                            double amountToWithdraw = Double.parseDouble(sPayAmount);
//                            if (amountToWithdraw < MinAmount || amountToWithdraw > MaxAmount) {
                            if (amountToWithdraw < MinAmount) {

                                if (amountToWithdraw < MinAmount) {
                                    WithdrawErrorAlert(getResources().getString(R.string.withdrawal_page_label_min_withdrawal_is), sCurrency + " " + sMinAmount);
                                } else {
                                    WithdrawErrorAlert(getResources().getString(R.string.withdrawal_page_label_max_withdrawal_is), sCurrency + " " + sMaxAmount);
                                }

                            } else {


                                if (sFromPage.equalsIgnoreCase("earnings")) {


                                    sSelectedPayment = listPayment.get(0).getPayment_selected_payment_id();

                                    if (sSelectedPaymentName.equalsIgnoreCase("paypal") && sPaypalID.length() == 0) {
                                        showPaypalDialog();
                                    } else {
                                        WithdrawalAmount(Iconstant.earnings_withdraw_amount_url);
                                    }


                                } else if (sFromPage.equalsIgnoreCase("incentives")) {

                                    sSelectedPayment = listPayment.get(0).getPayment_selected_payment_id();

                                    if (sSelectedPaymentName.equalsIgnoreCase("paypal") && sPaypalID.length() == 0) {
                                        showPaypalDialog();
                                    } else {
                                        WithdrawalAmount(Iconstant.incentives_withdraw_amount_url);
                                    }

                                } else if (sFromPage.equalsIgnoreCase("referral_earnings")) {


                                    sSelectedPayment = listPayment.get(0).getPayment_selected_payment_id();

                                    if (sSelectedPaymentName.equalsIgnoreCase("paypal") && sPaypalID.length() == 0) {
                                        showPaypalDialog();
                                    } else {
                                        WithdrawalAmount(Iconstant.referral_withdraw_amount_url);
                                    }


                                }

                            }

                        } else {
                            Alert(getResources().getString(R.string.action_error), sProceedError);
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            } else {
                Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
            }

        } else if (view == Tv_moreInfo)

        {

            showHelpDialog();

        }

    }


    private void callCustomerCare() {

        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+

            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + sContactNumber));
            startActivity(intent);

        } else {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + sContactNumber));
            startActivity(intent);

        }

    }


    //-------------Help Dialog-----------------
    private void showHelpDialog() {

        //--------Adjusting Dialog width & Height-----
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 85% of the screen
        int screenHeight = (int) (metrics.heightPixels * 0.85);//fill only 85% of the screen

        final Dialog helpDialog = new Dialog(WithdrawalPage.this, R.style.SlideUpDialog);
        helpDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        helpDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        helpDialog.setCancelable(true);
        helpDialog.setContentView(R.layout.alert_withdrawal_help_info);
        helpDialog.getWindow().setLayout(screenWidth, screenHeight);

        ImageView Iv_close = (ImageView) helpDialog.findViewById(R.id.img_close);

        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (helpDialog != null && helpDialog.isShowing()) {
                    helpDialog.dismiss();
                }
            }
        });


        helpDialog.show();

    }


    private void showWithdrawalDialog() {
        final Dialog withdrawalDialog = new Dialog(WithdrawalPage.this, R.style.SlideUpDialog);
        withdrawalDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        withdrawalDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        withdrawalDialog.setCancelable(true);
        withdrawalDialog.setContentView(R.layout.alert_enter_withdrawal);

        final TextView Tv_currency = (TextView) withdrawalDialog.findViewById(R.id.txt_currency);
        final ImageView Iv_close = (ImageView) withdrawalDialog.findViewById(R.id.img_close);
        final EditText Et_withdrawAmount = (EditText) withdrawalDialog.findViewById(R.id.edt_withdrawal_amount);
        final RelativeLayout RL_confirm = (RelativeLayout) withdrawalDialog.findViewById(R.id.Rl_confirm);

        Et_withdrawAmount.setText(Tv_availableAmount.getText().toString().replace(sCurrency, ""));
        Et_withdrawAmount.setSelection(Et_withdrawAmount.getText().length());
        Tv_currency.setText(sCurrency);

        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (withdrawalDialog != null && withdrawalDialog.isShowing()) {
                    withdrawalDialog.dismiss();
                }
            }
        });

        RL_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String withdrawAmount = Et_withdrawAmount.getText().toString().trim();
                if (withdrawAmount.length() > 0) {

                    double amountToWithdraw = Double.parseDouble(withdrawAmount);
                    if (!(amountToWithdraw > 0)) {
                        Et_withdrawAmount.setError(getResources().getString(R.string.withdrawal_page_error_withdrawal_amount_invalid));
                    } else {

                        Et_withdrawAmount.setError(null);
                        Tv_availableAmount.setText(sCurrency+" " + Et_withdrawAmount.getText().toString().trim());

                        if (withdrawalDialog != null && withdrawalDialog.isShowing()) {
                            withdrawalDialog.dismiss();
                        }

                        CloseKeyboardNew();
                    }

                } else {
                    Et_withdrawAmount.setError(getResources().getString(R.string.withdrawal_page_error_withdrawal_amount_cannot_be_empty));
                }


            }
        });

        Et_withdrawAmount.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    CloseKeyboard(Et_withdrawAmount);
                }
                return false;
            }
        });

        withdrawalDialog.show();

    }


    private void showPaypalDialog() {
        final Dialog paypalDialog = new Dialog(WithdrawalPage.this, R.style.SlideUpDialog);
        paypalDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        paypalDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        paypalDialog.setCancelable(true);
        paypalDialog.setContentView(R.layout.alert_paypal_id);

        final ImageView Iv_close = (ImageView) paypalDialog.findViewById(R.id.img_close);
        final EditText Et_email = (EditText) paypalDialog.findViewById(R.id.edt_authorize_net);
        final TextView Tv_confirm = (TextView) paypalDialog.findViewById(R.id.paypal_confirm);

        Et_email.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    CloseKeyboard(Et_email);
                }
                return false;
            }
        });

        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (paypalDialog != null && paypalDialog.isShowing()) {
                    paypalDialog.dismiss();
                }
            }
        });

        Tv_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String PaypalID = Et_email.getText().toString().trim();
                if (PaypalID.length() > 0 && isValidEmail(PaypalID)) {
                    Et_email.setError(null);
                    sPaypalID = PaypalID;
                    if (paypalDialog != null && paypalDialog.isShowing()) {
                        paypalDialog.dismiss();
                    }
                } else {
                    if (PaypalID.length() == 0) {
                        Et_email.setError(getResources().getString(R.string.withdrawal_page_label_paypal_validation));
                    } else if (!isValidEmail(PaypalID)) {
                        Et_email.setError(getResources().getString(R.string.withdrawal_page_label_enter_valid_paypal_id));
                    }
                }

            }
        });

        paypalDialog.show();

    }


    @SuppressLint("WrongConstant")
    private void GetWithdrawalData(String Url) {

        dialog = new Dialog(WithdrawalPage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);

        System.out.println("--------------EarningsWithdraw Url-------------------" + Url);
        System.out.println("--------------EarningsWithdraw jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(WithdrawalPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------EarningsWithdraw Response-------------------" + response);
                String status = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {
                            JSONObject jsonObject = object.getJSONObject("response");

                            if (jsonObject.length() > 0) {

                                if (jsonObject.has("available_amount")) {
                                    sAvailableAmount = jsonObject.getString("available_amount");
                                } else if (jsonObject.has("referral_earning")) {
                                    sAvailableAmount = jsonObject.getString("referral_earning");
                                }

                                sCurrency = jsonObject.getString("currency");
                                sLastWithdrawal = jsonObject.getString("last_withdrawal_txt");
                                if (jsonObject.has("min_amount")) {
                                    sMinAmount = jsonObject.getString("min_amount");
                                }
                                if (jsonObject.has("max_amount")) {
                                    sMaxAmount = jsonObject.getString("max_amount");
                                }
                                sProceedStatus = jsonObject.getString("proceed_status");
                                sProceedError = jsonObject.getString("proceed_error");


                                if (sMaxAmount.length() > 0 && sMinAmount.length() > 0) {
                                    MaxAmount = Double.parseDouble(sMaxAmount);
                                    MinAmount = Double.parseDouble(sMinAmount);
                                }

                                JSONArray jarPayment = jsonObject.getJSONArray("payment");

                                if (jarPayment.length() > 0) {

                                    listPayment = new ArrayList<WalletMoneyPojo>();
                                    String selected_payment = "";
                                    for (int i = 0; i < jarPayment.length(); i++) {
                                        JSONObject jobjPayment = jarPayment.getJSONObject(i);

                                        String name = jobjPayment.getString("name");
                                        String code = jobjPayment.getString("code");

                                        if (name.equalsIgnoreCase(getResources().getString(R.string.withdrawal_page_label_bwallet))) {
                                            selected_payment = code;
                                            sSelectedPaymentName = name;
                                        }

                                    }

                                    for (int i = 0; i < jarPayment.length(); i++) {

                                        JSONObject jobjPayment = jarPayment.getJSONObject(i);

                                        String name = jobjPayment.getString("name");
                                        String code = jobjPayment.getString("code");
                                        String active_icon = jobjPayment.getString("icon");
                                        String inactive_icon = jobjPayment.getString("inactive_icon");

                                        listPayment.add(new WalletMoneyPojo(name, code, active_icon, inactive_icon, selected_payment));

                                    }

                                }

                            }


                        } else {
                            String sResponse = object.getString("response");
                            Alert(getResources().getString(R.string.action_error), sResponse);
                        }

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (status.equalsIgnoreCase("1")) {

                    Tv_availableAmount.setText(sCurrency +" "+ sAvailableAmount);
                    if (sLastWithdrawal.length() > 0) {
                        Tv_lastWithdrawal.setText(getResources().getString(R.string.withdrawal_page_label_last_withdrawal) + " " + sLastWithdrawal);
                    } else {
                        Tv_lastWithdrawal.setText(getResources().getString(R.string.withdrawal_page_label_no_withdrawal));
                        Tv_lastWithdrawal.setVisibility(View.INVISIBLE);
                    }

                    adapter = new WithdrawPaymentGridAdapter(WithdrawalPage.this, listPayment);
                    Gv_paymentOptions.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                }

                if (dialog != null) {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    @SuppressLint("WrongConstant")
    private void WithdrawalAmount(String Url) {

        dialog = new Dialog(WithdrawalPage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        jsonParams.put("amount", sPayAmount);
        jsonParams.put("mode", sSelectedPayment);
        jsonParams.put("paypal_id", sPaypalID);

        System.out.println("--------------WithdrawalAmount Url-------------------" + Url);
        System.out.println("--------------WithdrawalAmount jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(WithdrawalPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------WithdrawalAmount Response-------------------" + response);
                String status = "", sResponse = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");
                        sResponse = object.getString("response");

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (dialog != null) {
                    dialog.dismiss();
                }

                if (status.equalsIgnoreCase("1")) {
//                    AlertWithdrawSuccess(getResources().getString(R.string.action_success), sResponse);
                    WithdrawSuccessAlert(sPayAmount, sSelectedPaymentName);
                } else {
//                    Alert(getResources().getString(R.string.action_error), sResponse);
                    WithdrawErrorAlert(getResources().getString(R.string.action_error), sResponse);
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void AlertWithdrawSuccess(String title, String alert) {
        final PkDialog mDialog = new PkDialog(WithdrawalPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

                Intent intent = new Intent();
                intent.setAction("com.app.Withdraw.Success");
                sendBroadcast(intent);

                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });
        mDialog.show();
    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(WithdrawalPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void WithdrawSuccessAlert(String withdrawAmount, String paymentType) {
        final Dialog withdrawSuccessDialog = new Dialog(WithdrawalPage.this, R.style.SlideUpDialog);
        withdrawSuccessDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        withdrawSuccessDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        withdrawSuccessDialog.setCancelable(true);
        withdrawSuccessDialog.setContentView(R.layout.alert_withdraw_success1);

        final ImageView Iv_close = (ImageView) withdrawSuccessDialog.findViewById(R.id.img_close);
        final TextView Tv_withdrawAmount = (TextView) withdrawSuccessDialog.findViewById(R.id.txt_alert_amount);
        final TextView Tv_paymentType = (TextView) withdrawSuccessDialog.findViewById(R.id.txt_alert_payment_type);

        Tv_withdrawAmount.setText(withdrawAmount);
        paymentType = getResources().getString(R.string.withdrawal_page_label_payment_type) + paymentType;
        Tv_paymentType.setText(paymentType);

        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (withdrawSuccessDialog != null && withdrawSuccessDialog.isShowing()) {
                    withdrawSuccessDialog.dismiss();
                }
            }
        });

        withdrawSuccessDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);

                Intent intent = new Intent();
                intent.setAction("com.app.Withdraw.Success");
                sendBroadcast(intent);
            }
        });

        withdrawSuccessDialog.show();

    }

    private void WithdrawErrorAlert(String message1, String message2) {
        final Dialog withdrawErrorDialog = new Dialog(WithdrawalPage.this, R.style.SlideUpDialog);
        withdrawErrorDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        withdrawErrorDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        withdrawErrorDialog.setCancelable(true);
        withdrawErrorDialog.setContentView(R.layout.alert_withdrawal_errors);

        final ImageView Iv_close = (ImageView) withdrawErrorDialog.findViewById(R.id.img_close);
        final TextView Tv_message1 = (TextView) withdrawErrorDialog.findViewById(R.id.txt_alert_message1);
        final TextView Tv_message2 = (TextView) withdrawErrorDialog.findViewById(R.id.txt_alert_message2);

        Tv_message1.setText(message1);
        Tv_message2.setText(message2);

        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (withdrawErrorDialog != null && withdrawErrorDialog.isShowing()) {
                    withdrawErrorDialog.dismiss();
                }
            }
        });

        withdrawErrorDialog.show();

    }


    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
    }

    private boolean checkCallPhonePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkReadStatePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }


    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + sContactNumber));
                    startActivity(callIntent);
                }
                break;
        }
    }


    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    //--------------Close KeyBoard Method-----------
    @SuppressWarnings("WrongConstant")
    private void CloseKeyboard(EditText edittext) {
        try {
            InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            in.hideSoftInputFromWindow(edittext.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("WrongConstant")
    private void CloseKeyboardNew() {
        try {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow((null == getCurrentFocus()) ? null : getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {

            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);

            Intent intent = new Intent();
            intent.setAction("com.app.Withdraw.Success");
            sendBroadcast(intent);

            return true;
        }
        return false;
    }

    private void startBlinkingAnimation() {
        Animation startAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blinking_animation);
        Iv_light.startAnimation(startAnimation);
    }

}
