package com.blackmaria.partner.app;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user144 on 9/18/2017.
 */

public class NewCloudMoneyReloadHomePage extends ActivityHockeyApp implements View.OnClickListener {
    private RelativeLayout Rl1;
    private ImageView imgBack;
    private ImageView imgBwallet;
    private TextView txtLabelCurrentBalance;
    private RelativeLayout Rl2;
    private static EditText edtMobileNo;
    private LinearLayout buyTv;
    private ImageView indicatingImage;
    private RelativeLayout verifylayout;
    private LinearLayout lytReloadAmount1;
    private TextView reloadAmount1;
    private LinearLayout lytReloadAmount2;
    private TextView reloadAmount2;
    private LinearLayout lytReloadAmount3;
    private TextView reloadAmount3;
    private View view;
    private LinearLayout lytReloadAmount4;
    private View viewDummy;
    private TextView reloadAmount4;


    private SessionManager session;
    private boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    Dialog dialog;


    private String UserID = "";
    private Boolean isDatavailable = false;
    private Boolean isPaymentListAvailable = false;

    private RefreshReceiver refreshReceiver;
    private String walletReloadAmount = "";
    private String current_balance = "", Sxenditcharge_Status="",auto_charge_status = "", currency = "", wal_recharge_amount_one = "", wal_recharge_amount_two = "", wal_recharge_amount_three = "", wal_recharge_amount_four = "";
    ArrayList<String> paymentcardlistName;
    ArrayList<String> paymentcardlistCode;
    ArrayList<String> paymentcardlistInActive;
    ArrayList<String> paymentcardlistActive;
    HashMap<String, ArrayList<String>> paymentList;

    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();
            if (action.equalsIgnoreCase("com.package.ACTION_WALLET_RELOAD_SUCCESS")) {
//                finish();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_cloud_money_reload_home);
        initialize();
    }

    private void initialize() {

        session = new SessionManager(NewCloudMoneyReloadHomePage.this);
        cd = new ConnectionDetector(NewCloudMoneyReloadHomePage.this);
        isInternetPresent = cd.isConnectingToInternet();


        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_DRIVERID);
        //sSecurePin = session.getSecurePin();

        paymentcardlistName = new ArrayList<String>();
        paymentcardlistCode = new ArrayList<String>();
        paymentcardlistInActive = new ArrayList<String>();
        paymentcardlistActive = new ArrayList<String>();

        Rl1 = (RelativeLayout) findViewById(R.id.Rl_1);
        imgBack = (ImageView) findViewById(R.id.img_back);
        imgBwallet = (ImageView) findViewById(R.id.img_bwallet);
        txtLabelCurrentBalance = (TextView) findViewById(R.id.txt_label_current_balance);
        Rl2 = (RelativeLayout) findViewById(R.id.Rl_2);
        edtMobileNo = (EditText) findViewById(R.id.edt_mobile_no);
        buyTv = (LinearLayout) findViewById(R.id.buy_tv);

        indicatingImage = (ImageView) findViewById(R.id.indicating_image);
  /*GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(indicatingImage);
        Glide.with(this).load(R.drawable.blinkgif).into(imageViewTarget);*/


        verifylayout = (RelativeLayout) findViewById(R.id.verifylayout);
        lytReloadAmount1 = (LinearLayout) findViewById(R.id.lyt_reload_amount1);
        reloadAmount1 = (TextView) findViewById(R.id.reload_amount1);
        lytReloadAmount2 = (LinearLayout) findViewById(R.id.lyt_reload_amount2);
        reloadAmount2 = (TextView) findViewById(R.id.reload_amount2);
        lytReloadAmount3 = (LinearLayout) findViewById(R.id.lyt_reload_amount3);
        reloadAmount3 = (TextView) findViewById(R.id.reload_amount3);
        view = (View) findViewById(R.id.view);
        lytReloadAmount4 = (LinearLayout) findViewById(R.id.lyt_reload_amount4);
        viewDummy = (View) findViewById(R.id.view_dummy);
        reloadAmount4 = (TextView) findViewById(R.id.reload_amount4);

        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_WALLET_RELOAD_SUCCESS");
        registerReceiver(refreshReceiver, intentFilter);

        imgBack.setOnClickListener(this);
        verifylayout.setOnClickListener(this);
        lytReloadAmount1.setOnClickListener(this);
        lytReloadAmount2.setOnClickListener(this);
        lytReloadAmount3.setOnClickListener(this);
        lytReloadAmount4.setOnClickListener(this);
        buyTv.setOnClickListener(this);

       new DownloadFilesTask().execute("","","");

    }

    public static void refreshEdittext() {
        edtMobileNo.setText("");
    }

    private void hideSoftKeyboard() {
        if (getCurrentFocus() != null && getCurrentFocus() instanceof EditText) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(edtMobileNo.getWindowToken(), 0);
        }
    }

    private void postData() {
        if (isInternetPresent) {

            HashMap<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("driver_id", UserID);
            PostRquestReloadDashBoardvalue(Iconstant.wallet_money_url, jsonParams);
        } else {

            Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
        }
    }




    private class DownloadFilesTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new Dialog(NewCloudMoneyReloadHomePage.this);
            dialog.getWindow();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.custom_loading);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }



        protected String doInBackground(String... urls) {
            postData();
            return "";
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

           /* if (dialog != null) {
                dialog.dismiss();
            }*/
        }


    }


    @Override
    public void onClick(View v) {
        if (v == imgBack) {
            hideSoftKeyboard();
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (v == buyTv) {
            walletReloadAmount = edtMobileNo.getText().toString().trim();
            if (edtMobileNo.getText().toString().trim().length() == 0) {
                erroredit(edtMobileNo, getResources().getString(R.string.reloadamount_empty_alert));
            } else {
                if(paymentList!=null) {
                    Intent intent = new Intent(NewCloudMoneyReloadHomePage.this, NewCloudMoneyReloadPaymentOptionPage.class);
                    intent.putExtra("insertAmount", walletReloadAmount);
                    intent.putExtra("currency", currency);
                    intent.putExtra("currentBalance", current_balance);
                    intent.putExtra("autochargestatus", auto_charge_status);
                    intent.putExtra("paymentListMap", paymentList);
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                }
            }

        } else if (v == lytReloadAmount4) {
            session.setReloadPaymentType("4");
            edtMobileNo.setText(wal_recharge_amount_four);
            walletReloadAmount = edtMobileNo.getText().toString().trim();
            if(paymentList!=null) {
                Intent intent = new Intent(NewCloudMoneyReloadHomePage.this, NewCloudMoneyReloadPaymentOptionPage.class);
                intent.putExtra("insertAmount", wal_recharge_amount_four);
                intent.putExtra("currency", currency);
                intent.putExtra("currentBalance", current_balance);
                intent.putExtra("autochargestatus", auto_charge_status);
                intent.putExtra("paymentListMap", paymentList);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        } else if (v == lytReloadAmount1) {
            session.setReloadPaymentType("1");
            edtMobileNo.setText(wal_recharge_amount_one);
            walletReloadAmount = edtMobileNo.getText().toString().trim();
            if(paymentList!=null) {
                Intent intent = new Intent(NewCloudMoneyReloadHomePage.this, NewCloudMoneyReloadPaymentOptionPage.class);
                intent.putExtra("insertAmount", wal_recharge_amount_one);
                intent.putExtra("currency", currency);
                intent.putExtra("currentBalance", current_balance);
                intent.putExtra("autochargestatus", auto_charge_status);
                intent.putExtra("paymentListMap", paymentList);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }

        } else if (v == lytReloadAmount2) {
            session.setReloadPaymentType("2");
            edtMobileNo.setText(wal_recharge_amount_two);
            walletReloadAmount = edtMobileNo.getText().toString().trim();

            if(paymentList!=null) {
                Intent intent = new Intent(NewCloudMoneyReloadHomePage.this, NewCloudMoneyReloadPaymentOptionPage.class);
                intent.putExtra("insertAmount", wal_recharge_amount_two);
                intent.putExtra("currency", currency);
                intent.putExtra("currentBalance", current_balance);
                intent.putExtra("autochargestatus", auto_charge_status);
                intent.putExtra("paymentListMap", paymentList);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }

        } else if (v == lytReloadAmount3) {
            session.setReloadPaymentType("3");
            edtMobileNo.setText(wal_recharge_amount_three);
            walletReloadAmount = edtMobileNo.getText().toString().trim();
            if(paymentList!=null) {
                Intent intent = new Intent(NewCloudMoneyReloadHomePage.this, NewCloudMoneyReloadPaymentOptionPage.class);
                intent.putExtra("insertAmount", wal_recharge_amount_three);
                intent.putExtra("currency", currency);
                intent.putExtra("currentBalance", current_balance);
                intent.putExtra("autochargestatus", auto_charge_status);
                intent.putExtra("paymentListMap", paymentList);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }


        } else if (v == verifylayout) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }
    }


    private void PostRquestReloadDashBoardvalue(String Url, HashMap<String, String> jsonParams) {


        if(dialog==null)
        {
            dialog.show();
        }
        System.out.println("-----------PostRquestReloadDashBoardvalue jsonParams--------------" + jsonParams);

        mRequest = new ServiceRequest(NewCloudMoneyReloadHomePage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {


            @Override
            public void onCompleteListener(String response) {
                System.out.println("--------------PostRquestReloadDashBoardvalue reponse-------------------" + response);

                String Sstatus = "", Smessage = "", current_month_credit = "", currencyconversion="",current_month_debit = "", xendit_secret_key = "", xendit_public_key = "", month = "", year = "", month_year = "";
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    auto_charge_status = object.getString("auto_charge_status");
                    Sxenditcharge_Status= object.getString("xendit_charge_status");
                    session.setXenditChargeStatus(Sxenditcharge_Status);
                    if (Sstatus.equalsIgnoreCase("1")) {

                        JSONObject jsonObject = object.getJSONObject("response");

                        currency = jsonObject.getString("currency");
                        current_balance = jsonObject.getString("current_balance");
                        current_month_credit = jsonObject.getString("current_month_credit");
                        current_month_debit = jsonObject.getString("current_month_debit");

                        xendit_secret_key = jsonObject.getString("xendit_secret_key");
                        xendit_public_key = jsonObject.getString("xendit_public_key");
                        if(jsonObject.has("exchange_value")){
                            currencyconversion= jsonObject.getString("exchange_value");
                            session. setcurrencyconversionKey(currencyconversion);
                        }
                        month = jsonObject.getString("month");
                        year = jsonObject.getString("year");
                        month_year = jsonObject.getString("month_year");

                        JSONObject rechBndryObj = jsonObject.getJSONObject("recharge_boundary");

                        if (rechBndryObj.length() > 0) {
                            wal_recharge_amount_one = rechBndryObj.getString("wallet_amount_one");
                            wal_recharge_amount_two = rechBndryObj.getString("wallet_amount_two");
                            wal_recharge_amount_three = rechBndryObj.getString("wallet_amount_three");
                            wal_recharge_amount_four = rechBndryObj.getString("wallet_amount_four");
                        }
                        Object check_object = jsonObject.get("payment_list");
                        if (check_object instanceof JSONArray) {
                            JSONArray payment_list_jsonArray = jsonObject.getJSONArray("payment_list");
                            if (payment_list_jsonArray.length() > 0) {
                                paymentcardlistName.clear();
                                paymentcardlistCode.clear();
                                paymentcardlistActive.clear();
                                paymentcardlistInActive.clear();
                                paymentList = new HashMap<String, ArrayList<String>>();
                                for (int i = 0; i < payment_list_jsonArray.length(); i++) {
                                    JSONObject payment_obj = payment_list_jsonArray.getJSONObject(i);
                                    paymentcardlistName.add(payment_obj.getString("name"));
                                    paymentcardlistCode.add(payment_obj.getString("code"));
                                    paymentcardlistInActive.add(payment_obj.getString("inactive_icon"));
                                    paymentcardlistActive.add(payment_obj.getString("icon"));
                                }
                                paymentList.put("bankName", paymentcardlistName);
                                paymentList.put("bankCode", paymentcardlistCode);
                                paymentList.put("bankInactive", paymentcardlistInActive);
                                paymentList.put("bankActive", paymentcardlistActive);

                                isPaymentListAvailable = true;

                            } else {
                                isPaymentListAvailable = false;
                            }
                        }
                        dialogDismiss();

                    } else {
                        Smessage = object.getString("response");
                        dialogDismiss();
                    }
                    isDatavailable = true;
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    dialogDismiss();
                }
                dialogDismiss();
                if (Sstatus.equalsIgnoreCase("1")) {
                    reloadAmount1.setText(currency +" "+ wal_recharge_amount_one);
                    reloadAmount2.setText(currency +" "+ wal_recharge_amount_two);
                    reloadAmount3.setText(currency +" "+ wal_recharge_amount_three);
                    reloadAmount4.setText(currency +" "+ wal_recharge_amount_four);
                    session.setXenditPublicKey(xendit_public_key);
                } else {
                    Alert(getResources().getString(R.string.action_error), Smessage);
                }
            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });
    }

    private void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }


    //--------------Alert Method-----------
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(NewCloudMoneyReloadHomePage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

            }
        });
        mDialog.show();
    }

    //--------------------Code to set error for EditText-----------------------
    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(NewCloudMoneyReloadHomePage.this, R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }


    private void startBlinkingAnimation() {
        Animation startAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blinking_animation);
        indicatingImage.startAnimation(startAnimation);
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
        if (indicatingImage != null) {
            indicatingImage.clearAnimation();
        }
    }
}
