package com.blackmaria.partner.app;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.FragmentActivityHockeyApp;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.adapter.MyCustomSpinnerAdapter;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.mylibrary.volley.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.countrycodepicker.CountryPicker;
import com.countrycodepicker.CountryPickerListener;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by GANESH on 11-10-2017.
 */

public class RegistrationPersonalDetails extends FragmentActivityHockeyApp implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private SessionManager sessionManager;
    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    private Dialog dialog;
    private MyCustomSpinnerAdapter adapter;
    private boolean isInternetPresent = false;


    private String sDriverID = "";
    private boolean isSpnrTypeOfRegFirstTime = true;
    private boolean isSpnrSelectOfRegFirstTime = true, isSpnrcountryFirstTime = true, isSpnrTypegenderOfRegFirstTime = true, isSpnrSelectExOfRegFirstTime = true;
    private RelativeLayout RL_exit;
    private Spinner Spnr_typeOfReg, drivingExSpnr, spnrGender, spnr_country;
    private TextView Tv_spnrTypeOfReg, txt_spnr_country, txt_spnr_type_of_company, txt_drivingexperience, spnrgender;
    private Button Btn_confirm;
    String experienceyr = "", companyname = "", companyid = "", driverVehicleId = "", driverLocationPlaceId = "", driverProImage = "", driverCountryCode = "", driverPhoneNumber = "", driverReferelCode = "", driverPinCode = "";
    private ArrayList<String> companyId;
    private List<String> countryId;
    private ArrayList<String> countryName;
    private ArrayList<String> countryDialCode;
    private List<String> companyName;
    boolean val = false;
    String[] strcompanyId;
    String[] strcompanyName;
    Spinner spnrCmpnyList;
    private RelativeLayout companydetail_layout, counteychoose_Rl;
    ;
    private ImageView Iv_flag;
    private EditText edtFullName, edtMailId, edtAddress, edtCity, edtPostcode, edtState;
    String number = "", CountryCode = "";
    CountryPicker picker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_personal_details);

        initialize();
        counteychoose_Rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });
        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {
                picker.dismiss();
                txt_spnr_country.setText(name);
                CountryCode = dialCode;
                String drawableName = "flag_"
                        + code.toLowerCase(Locale.ENGLISH);
                Iv_flag.setImageResource(getResId(drawableName));
                CloseKeyBoard();
            }
        });

    }

    private void initialize() {

        sessionManager = new SessionManager(RegistrationPersonalDetails.this);
        cd = new ConnectionDetector(RegistrationPersonalDetails.this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        isInternetPresent = cd.isConnectingToInternet();
        picker = CountryPicker.newInstance(getResources().getString(R.string.select_country_lable));

        counteychoose_Rl = (RelativeLayout) findViewById(R.id.counteychoose_Rl);

        RL_exit = (RelativeLayout) findViewById(R.id.RL_exit);
        Iv_flag = (ImageView) findViewById(R.id.img_flag);
        Spnr_typeOfReg = (Spinner) findViewById(R.id.spnr_type_of_reg);
        spnrCmpnyList = (Spinner) findViewById(R.id.spnr_type_of_company);
        drivingExSpnr = (Spinner) findViewById(R.id.spnr_drivingexperience);
        spnrGender = (Spinner) findViewById(R.id.spnr_gender);
        spnr_country = (Spinner) findViewById(R.id.spnr_country);

        Tv_spnrTypeOfReg = (TextView) findViewById(R.id.txt_spnr_type_of_reg);
        txt_spnr_type_of_company = (TextView) findViewById(R.id.txt_spnr_type_of_company);
        txt_drivingexperience = (TextView) findViewById(R.id.txt_drivingexperience);
        spnrgender = (TextView) findViewById(R.id.txt_spnr_gender);
        txt_spnr_country = (TextView) findViewById(R.id.txt_spnr_country);
        companydetail_layout = (RelativeLayout) findViewById(R.id.companydetail_layout);


        edtFullName = (EditText) findViewById(R.id.edt_full_name);
        edtMailId = (EditText) findViewById(R.id.edt_mail_id);
        edtAddress = (EditText) findViewById(R.id.edt_address);
        edtCity = (EditText) findViewById(R.id.edt_city);
        edtPostcode = (EditText) findViewById(R.id.edt_postcode);
        edtState = (EditText) findViewById(R.id.edt_state);


        Btn_confirm = (Button) findViewById(R.id.btn_confirm);
        RL_exit.setOnClickListener(this);
        Btn_confirm.setOnClickListener(this);

        Spnr_typeOfReg.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                CloseKeyboardNew();
                return false;
            }
        });
        spnrCmpnyList.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                CloseKeyboardNew();
                return false;
            }
        });
        drivingExSpnr.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                CloseKeyboardNew();
                return false;
            }
        });
        spnrGender.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                CloseKeyboardNew();
                return false;
            }
        });
        spnr_country.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                CloseKeyboardNew();
                return false;
            }
        });
        Intent in = getIntent();
        if (in != null) {
            driverVehicleId = in.getStringExtra("driverVehicleId");
            driverLocationPlaceId = in.getStringExtra("driverLocationPlaceId");
            driverProImage = in.getStringExtra("driverProImage");
            driverCountryCode = in.getStringExtra("driverCountryCode");
            driverPhoneNumber = in.getStringExtra("driverPhoneNumber");
            driverReferelCode = in.getStringExtra("driverReferelCode");
            driverPinCode = in.getStringExtra("driverPinCode");
        }


        if (isInternetPresent) {
//            companyListDetals(Iconstant.DriverReg_getCompanyList_URl);
        } else {
            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }

        CompanyTypeList();
        DrivingExperienceSpinner();
        GenderSelection();
    }


    @Override
    public void onClick(View view) {

        if (view == RL_exit) {

            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);

        } else if (view == Btn_confirm) {
            CloseKeyboardNew();
            if (Tv_spnrTypeOfReg.getText().toString().trim().equalsIgnoreCase("Choose Type")) {
                AlertError(getResources().getString(R.string.timer_label_alert_sorry), getResources().getString(R.string.profile_label_alert_pincode));
            } else if (Tv_spnrTypeOfReg.getText().toString().trim().equalsIgnoreCase("Corporate") && txt_spnr_type_of_company.getText().toString().trim().equalsIgnoreCase("Choose Company Name")) {
                AlertError(getResources().getString(R.string.timer_label_alert_sorry), getResources().getString(R.string.profile_label_alert_pincode));
            } else if (edtFullName.getText().toString().trim().equalsIgnoreCase("")) {
                AlertError(getResources().getString(R.string.timer_label_alert_sorry), getResources().getString(R.string.profile_label_alert_username));
            } else if (!isValidEmail(edtMailId.getText().toString().trim().replace(" ", ""))) {
                AlertError(getResources().getString(R.string.timer_label_alert_sorry), getResources().getString(R.string.profile_label_alert_email));
            } else if (spnrgender.getText().toString().trim().equalsIgnoreCase("Select Gender")) {
                AlertError(getResources().getString(R.string.timer_label_alert_sorry), getResources().getString(R.string.profile_label_alert_gender));
            } else if (txt_drivingexperience.getText().toString().trim().equalsIgnoreCase("Choose Experience") || txt_drivingexperience.getText().toString().trim().equalsIgnoreCase("Driving Experience")) {
                AlertError(getResources().getString(R.string.timer_label_alert_sorry), getResources().getString(R.string.profile_lable_experience));
            } else if (edtAddress.getText().toString().trim().equalsIgnoreCase("")) {
                AlertError(getResources().getString(R.string.timer_label_alert_sorry), getResources().getString(R.string.profile_label_alert_address));
            } else if (edtCity.getText().toString().trim().equalsIgnoreCase("")) {
                AlertError(getResources().getString(R.string.timer_label_alert_sorry), getResources().getString(R.string.profile_label_alert_cityname));
            } else if (edtPostcode.getText().toString().trim().equalsIgnoreCase("")) {
                AlertError(getResources().getString(R.string.timer_label_alert_sorry), getResources().getString(R.string.profile_label_alert_pincode));
            } else if (edtPostcode.getText().toString().trim().length() < 2) {
                AlertError(getResources().getString(R.string.timer_label_alert_sorry), getResources().getString(R.string.profile_label_alert_pincodes));
            } else if (edtState.getText().toString().trim().equalsIgnoreCase("")) {
                AlertError(getResources().getString(R.string.timer_label_alert_sorry), getResources().getString(R.string.profile_label_alert_state));
            } else if (txt_spnr_country.getText().toString().trim().equalsIgnoreCase("Select Country")) {
                AlertError(getResources().getString(R.string.timer_label_alert_sorry), getResources().getString(R.string.profile_label_alert_countryname));
            } else {

                checkEmailexists(Iconstant.Verifyemail,edtMailId.getText().toString().trim());

//                    Intent intent = new Intent(RegistrationPersonalDetails.this, RegistrationVehicleDetails.class);
//                    intent.putExtra("driverVehicleId", driverVehicleId);
//                    intent.putExtra("driverLocationPlaceId", driverLocationPlaceId);
//                    intent.putExtra("driverProImage", driverProImage);
//                    intent.putExtra("driverCountryCode", driverCountryCode);
//                    intent.putExtra("driverPhoneNumber", driverPhoneNumber);
//                    intent.putExtra("driverReferelCode", driverReferelCode);
//                    intent.putExtra("driverPinCode", driverPinCode);
//                    intent.putExtra("driverCompanyType", companyname);
//                    intent.putExtra("driverCompanyId", companyid);
//                    intent.putExtra("driverName", edtFullName.getText().toString().trim());
//                    intent.putExtra("driverEmailId", edtMailId.getText().toString().trim());
//                    intent.putExtra("driverGender", spnrgender.getText().toString().trim());
//                    intent.putExtra("driverExperience", experienceyr);
//                    intent.putExtra("driverAddress", edtAddress.getText().toString().trim());
//                    intent.putExtra("driverCityname", edtCity.getText().toString().trim());
//                    intent.putExtra("driverPostCode", edtPostcode.getText().toString().trim());
//                    intent.putExtra("driverState", edtState.getText().toString().trim());
//                    intent.putExtra("driverCountry", txt_spnr_country.getText().toString().trim());
//                    startActivity(intent);
//                    overridePendingTransition(R.anim.enter, R.anim.exit);
//

            }
        }

    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(RegistrationPersonalDetails.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    private void companyListDetals(String Url) {

        dialog = new Dialog(RegistrationPersonalDetails.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        System.out.println("--------------companyListDetals Url-------------------" + Url);

        mRequest = new ServiceRequest(RegistrationPersonalDetails.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------companyListDetals Response-------------------" + response);
                String status = "", Sresponse = "No companylist";
                try {

                    JSONObject object = new JSONObject(response);

                    status = object.getString("status");
                    JSONObject objResponse = object.getJSONObject("response");
                    if (objResponse.length() > 0) {

                        Object intervention = objResponse.get("company");
                        if (intervention instanceof JSONArray) {
                            JSONArray categoryArray = objResponse.getJSONArray("company");
                            companyId = new ArrayList<String>();
                            companyName = new ArrayList<String>();
                            strcompanyId = new String[categoryArray.length()];
                            strcompanyName = new String[categoryArray.length()];

                            companyId.add("10");
                            companyName.add("Choose Company Name");

                            for (int i = 0; i < categoryArray.length(); i++) {
                                JSONObject locCtaListObj = categoryArray.getJSONObject(i);
                                String id = locCtaListObj.getString("id");
                                String name = locCtaListObj.getString("company_name");
                                companyId.add(id);
                                companyName.add(name);
                            }
                            strcompanyId = companyId.toArray(new String[companyId.size()]);
                            strcompanyName = companyName.toArray(new String[companyName.size()]);
                        }
                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (isInternetPresent) {
                    COuntryListDetails(Iconstant.DriverReg_getCountryList_URl);
                } else {
                    Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                }

                if (status.equalsIgnoreCase("1")) {
                    ComapnyNamelistSpinner();
                } else {
                    Alert(getResources().getString(R.string.action_error), Sresponse);
                }


                if (dialog != null) {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void checkEmailexists(String Url,String email){

        dialog = new Dialog(RegistrationPersonalDetails.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("email",email);
        System.out.println("--------------checkEmailexists Url-------------------" + Url);

        mRequest = new ServiceRequest(RegistrationPersonalDetails.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------checkEmailexists Response-------------------" + response);
                String status = "", Sresponse = "";
                try {

                    JSONObject object = new JSONObject(response);
                    status = object.getString("status");
                    if (status.equalsIgnoreCase("1")) {
                        val = true;

                        Intent intent = new Intent(RegistrationPersonalDetails.this, RegistrationVehicleDetails.class);
                        intent.putExtra("driverVehicleId", driverVehicleId);
                        intent.putExtra("driverLocationPlaceId", driverLocationPlaceId);
                        intent.putExtra("driverProImage", driverProImage);
                        intent.putExtra("driverCountryCode", driverCountryCode);
                        intent.putExtra("driverPhoneNumber", driverPhoneNumber);
                        intent.putExtra("driverReferelCode", driverReferelCode);
                        intent.putExtra("driverPinCode", driverPinCode);
                        intent.putExtra("driverCompanyType", companyname);
                        intent.putExtra("driverCompanyId", companyid);
                        intent.putExtra("driverName", edtFullName.getText().toString().trim());
                        intent.putExtra("driverEmailId", edtMailId.getText().toString().trim());
                        intent.putExtra("driverGender", spnrgender.getText().toString().trim());
                        intent.putExtra("driverExperience", experienceyr);
                        intent.putExtra("driverAddress", edtAddress.getText().toString().trim());
                        intent.putExtra("driverCityname", edtCity.getText().toString().trim());
                        intent.putExtra("driverPostCode", edtPostcode.getText().toString().trim());
                        intent.putExtra("driverState", edtState.getText().toString().trim());
                        intent.putExtra("driverCountry", txt_spnr_country.getText().toString().trim());
                        startActivity(intent);
                        overridePendingTransition(R.anim.enter, R.anim.exit);


                    } else {
                        val = false;
                        Alert(getResources().getString(R.string.action_error), Sresponse);
                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (dialog != null) {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

    }

    private void COuntryListDetails(String Url) {

        dialog = new Dialog(RegistrationPersonalDetails.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        System.out.println("--------------COuntryListDetails Url-------------------" + Url);

        mRequest = new ServiceRequest(RegistrationPersonalDetails.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------COuntryListDetails Response-------------------" + response);
                String status = "", Sresponse = "";
                try {

                    JSONObject object = new JSONObject(response);

                    status = object.getString("status");
                    JSONObject objResponse = object.getJSONObject("response");
                    if (objResponse.length() > 0) {

                        Object intervention = objResponse.get("countries");
                        if (intervention instanceof JSONArray) {
                            JSONArray categoryArray = objResponse.getJSONArray("countries");
                            countryId = new ArrayList<String>();
                            countryName = new ArrayList<String>();
                            countryDialCode = new ArrayList<String>();
                            countryId.add("10");
                            countryName.add("Choose Country");
                            countryDialCode.add("0");

                            for (int i = 0; i < categoryArray.length(); i++) {
                                JSONObject locCtaListObj = categoryArray.getJSONObject(i);
                                String id = locCtaListObj.getString("id");
                                String name = locCtaListObj.getString("name");
                                String dialCode = locCtaListObj.getString("country_code");
                                countryDialCode.add(dialCode);
                                countryId.add(id);
                                countryName.add(name);
                            }

                        }
                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                if (status.equalsIgnoreCase("1")) {
                    COuntryChooseMethod();
                } else {
                    Alert(getResources().getString(R.string.action_error), Sresponse);
                }


                if (dialog != null) {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void DrivingExperienceSpinner() {
        isSpnrSelectExOfRegFirstTime = true;
        String spinnerData[] = getResources().getStringArray(R.array.drivingexperience);
        ArrayList<String> regTypeList = new ArrayList<String>();
        regTypeList.addAll(Arrays.asList(spinnerData));
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item_layout, regTypeList);
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item_layout); // The drop down vieww
        drivingExSpnr.setAdapter(spinnerArrayAdapter);
        drivingExSpnr.setOnItemSelectedListener(this);


    }


    private void ComapnyNamelistSpinner() {
//        String spinnerData[] = getResources().getStringArray(R.array.type_of_reg);
//        ArrayList<String> regTypeList = new ArrayList<String>();
//        regTypeList.addAll(Arrays.asList(spinnerData));
        isSpnrSelectOfRegFirstTime = true;
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item_layout, companyName);
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item_layout); // The drop down vieww
        spnrCmpnyList.setAdapter(spinnerArrayAdapter);

        spnrCmpnyList.setOnItemSelectedListener(this);


    }

    private void CompanyTypeList() {
        String spinnerData[] = getResources().getStringArray(R.array.type_of_reg);
        ArrayList<String> regTypeList = new ArrayList<String>();
        regTypeList.addAll(Arrays.asList(spinnerData));
        isSpnrTypeOfRegFirstTime = true;
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item_layout, regTypeList);
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item_layout); // The drop down vieww
        Spnr_typeOfReg.setAdapter(spinnerArrayAdapter);

        Spnr_typeOfReg.setOnItemSelectedListener(this);
    }

    private void GenderSelection() {
        String spinnerData[] = getResources().getStringArray(R.array.genders);
        ArrayList<String> regTypeList = new ArrayList<String>();
        regTypeList.addAll(Arrays.asList(spinnerData));
        isSpnrTypegenderOfRegFirstTime = true;
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item_layout, regTypeList);
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item_layout); // The drop down vieww
        spnrGender.setAdapter(spinnerArrayAdapter);

        spnrGender.setOnItemSelectedListener(this);
    }


    private void COuntryChooseMethod() {
        isSpnrcountryFirstTime = true;
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item_layout, countryName);
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item_layout); // The drop down vieww
        spnr_country.setAdapter(spinnerArrayAdapter);
        spnr_country.setOnItemSelectedListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        System.out.println("++++++++++++position=+++++++++++++" + position);
        if (parent == Spnr_typeOfReg) {
            String sSelectedItem = Spnr_typeOfReg.getSelectedItem().toString();

            companyname = sSelectedItem;
            if (companyname.equalsIgnoreCase("Corporate")) {
                companyname = "company";
            }
            if (!isSpnrTypeOfRegFirstTime) {
                Tv_spnrTypeOfReg.setText(sSelectedItem);

                if (sSelectedItem.equalsIgnoreCase("Corporate")) {
                    companydetail_layout.setVisibility(View.VISIBLE);
                } else {
                    companydetail_layout.setVisibility(View.GONE);
                }
            }
            isSpnrTypeOfRegFirstTime = false;
        } else if (parent == spnrCmpnyList) {
            String sSelectedItem = spnrCmpnyList.getSelectedItem().toString();
            companyid = companyId.get(position);
            if (!isSpnrSelectOfRegFirstTime) {

                txt_spnr_type_of_company.setText(sSelectedItem);
            }
            isSpnrSelectOfRegFirstTime = false;
        } else if (parent == drivingExSpnr) {
            String sSelectedItem = drivingExSpnr.getSelectedItem().toString().trim();
            String[] expYr = sSelectedItem.split(" ");
            experienceyr = expYr[0];
            System.out.println("===========expYr===========" + experienceyr);
            if (!isSpnrSelectExOfRegFirstTime) {
                txt_drivingexperience.setText(sSelectedItem);
            }
            isSpnrSelectExOfRegFirstTime = false;
        } else if (parent == spnrGender) {
            String sSelectedItem = spnrGender.getSelectedItem().toString();
            if (!isSpnrTypegenderOfRegFirstTime) {
                spnrgender.setText(sSelectedItem);
            }
            isSpnrTypegenderOfRegFirstTime = false;
        } else if (parent == spnr_country) {
            String sSelectedItem = spnr_country.getSelectedItem().toString();
            if (!isSpnrcountryFirstTime) {
                txt_spnr_country.setText(sSelectedItem);
                String countryCode = countryDialCode.get(position);
                String drawableName = "flag_"
                        + countryCode.toLowerCase(Locale.ENGLISH);
                Iv_flag.setImageResource(getResId(drawableName));
            }
            isSpnrcountryFirstTime = false;
        }
    }


    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void AlertError(String title, String message) {

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        Snackbar bar = Snackbar.with(RegistrationPersonalDetails.this);
        bar.addView(view);
        bar.colorResource(R.color.dark_red);
        bar.position(Snackbar.SnackbarPosition.TOP);
        bar.type(SnackbarType.MULTI_LINE);
        bar.duration(Snackbar.SnackbarDuration.LENGTH_SHORT);
        bar.animation(true);

        SnackbarManager.show(bar, RegistrationPersonalDetails.this);


    }

    //-------------------------code to Check Email Validation-----------------------
    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    private int getResId(String drawableName) {

        try {
            Class<com.countrycodepicker.R.drawable> res = com.countrycodepicker.R.drawable.class;
            Field field = res.getField(drawableName);
            int drawableId = field.getInt(null);
            return drawableId;
        } catch (Exception e) {
            Log.e("CountryCodePicker", "Failure to get drawable id.", e);
        }
        return -1;
    }

    private void CloseKeyboardNew() {
        try {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow((null == getCurrentFocus()) ? null : getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

    private void CloseKeyBoard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

       /* View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }*/
    }
}
