package com.blackmaria.partner.app;

import android.app.Dialog;


import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.latestpackageview.ApiRequest.RidedbApiIntentService;
import com.blackmaria.partner.latestpackageview.widgets.RoundedImageView;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.adapter.MyRideCancelTripAdapter;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.Model.tripcancelreasons;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.OnlinepageConstrain;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;


public class TrackRideCancelTrip extends AppCompatActivity implements ApIServices.completelisner {
    private RelativeLayout back;
    TextView percentage;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private SessionManager session;
    private String sDriverID = "";

    private ServiceRequest mRequest;
    private Dialog dialog;
    tripcancelreasons itemlist;
    private MyRideCancelTripAdapter adapter;
    private ListView listview;
    private String SrideId_intent = "", driverImage, sShare_ride_status = "", sShare_pool_ride_id = "";
    private TextView cancelimage, home_button_layout;
    private String cancelReason = "";
    private RoundedImageView cancel_trip_user_imageview;
    private ImageView blinking_image;
    private AppUtils appUtils;
    private ProgressBar apiload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.myride_cancel_trip);
        initialize();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });
        home_button_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
            }
        });
        cancelimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cd = new ConnectionDetector(TrackRideCancelTrip.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    if (!cancelReason.equalsIgnoreCase("")) {
                        HashMap<String, String> jsonParams = new HashMap<String, String>();
                        jsonParams.put("driver_id", sDriverID);
                        jsonParams.put("reason", cancelReason);
                        jsonParams.put("ride_id", SrideId_intent);
                        postRequestCancelurl(Iconstant.driverCancel_Url, jsonParams);
                    } else {
                        Alert(getResources().getString(R.string.alert), getResources().getString(R.string.alert_label));
                    }
                } else {
                    Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                }

            }
        });
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                cancelReason = itemlist.getResponse().getReason().get(position).getId();
                for (int i = 0; i < itemlist.getResponse().getReason().size(); i++) {
                    if (i == position) {
                        itemlist.getResponse().getReason().get(i).setStatus("true");
                    } else {
                        itemlist.getResponse().getReason().get(i).setStatus("false");
                    }
                }
                adapter.notifyDataSetChanged();
            }
        });

    }

    private void initialize() {
        appUtils = AppUtils.getInstance(this);
        session = SessionManager.getInstance(this);
        cd = ConnectionDetector.getInstance(this);
        isInternetPresent = cd.isConnectingToInternet();
        apiload = findViewById(R.id.apiload);
        back = (RelativeLayout) findViewById(R.id.cancel_trip_trackme_layout);
        listview = (ListView) findViewById(R.id.my_rides_cancel_trip_listView);
        cancelimage = findViewById(R.id.cancelimage_layout);
        home_button_layout = findViewById(R.id.home_button_layout);
        percentage= findViewById(R.id.percentage);
        cancel_trip_user_imageview = (RoundedImageView) findViewById(R.id.cancel_trip_user_imageview);
        blinking_image = (ImageView) findViewById(R.id.blinking_image);
        final Animation animation = new AlphaAnimation(1, 0);
        animation.setDuration(500);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
        blinking_image.startAnimation(animation);

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);

        Intent intent = getIntent();
        SrideId_intent = intent.getStringExtra("RideID");
        driverImage = intent.getStringExtra("driverImage");
        if (!driverImage.equalsIgnoreCase("")) {
            AppUtils.setImageView(this, String.valueOf(driverImage), cancel_trip_user_imageview);
        }
        try {
            String jsonobj = getIntent().getStringExtra("Reason");
            Type type = new TypeToken<tripcancelreasons>() {
            }.getType();
            tripcancelreasons tripcancelobj = new GsonBuilder().create().fromJson(jsonobj.toString(), type);
            itemlist = tripcancelobj;
            adapter = new MyRideCancelTripAdapter(TrackRideCancelTrip.this, itemlist);
            listview.setAdapter(adapter);

            percentage.setText("-"+tripcancelobj.getAffected_performance()+"%");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(TrackRideCancelTrip.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void postRequestCancelurl(String url, HashMap<String, String> jsonParams) {
        apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(this, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ApIServices apIServices = new ApIServices(this, TrackRideCancelTrip.this);
        apIServices.dooperation(url, jsonParams);
    }

    private void alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(TrackRideCancelTrip.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                startActivity(new Intent(TrackRideCancelTrip.this, OnlinepageConstrain.class));
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        mDialog.show();
    }

    private void alert_Cancel(String title, String alert) {
        final PkDialog mDialog = new PkDialog(TrackRideCancelTrip.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setCancelOnTouchOutside(false);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                startActivity(new Intent(TrackRideCancelTrip.this, OnlinepageConstrain.class));
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        mDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void sucessresponse(String response) {
        try {
            apiload.setVisibility(View.INVISIBLE);
            dialog.dismiss();
            JSONObject object = new JSONObject(response);
            String Str_status = object.getString("status");
            if (Str_status.equalsIgnoreCase("1")) {
                HashMap<String, String> jsonParams = new HashMap<String, String>();
                jsonParams.put("driver_id", sDriverID);
                jsonParams.put("ride_id", SrideId_intent);
                Intent intents = new Intent(this, RidedbApiIntentService.class);
                intents.putExtra("driverid", sDriverID);
                intents.putExtra("isdrivercancel", "true");
                intents.putExtra("rideid", SrideId_intent);
                intents.putExtra("params", jsonParams);
                intents.putExtra("url", Iconstant.complete_trip_detail_url);
                startService(intents);
                session.setisride("");
                JSONObject jobject = object.getJSONObject("response");
                alert_Cancel(getResources().getString(R.string.action_success), jobject.getString("message"));
            } else {
                session.setisride("");
                String Str_message = object.getString("response");
                alert(getResources().getString(R.string.alert_label_title), Str_message);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void errorreponse() {
        apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
    }

    @Override
    public void jsonexception() {
        apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        AppUtils.toastprint(this,"Json Exception");
    }
}

