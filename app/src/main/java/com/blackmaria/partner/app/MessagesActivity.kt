package com.blackmaria.partner.app

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Matrix
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.media.ExifInterface
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.blackmaria.partner.Pojo.TextMessage
import com.blackmaria.partner.R
import com.blackmaria.partner.Utils.SessionManager
import com.blackmaria.partner.adapter.MessagesAdapter
import com.blackmaria.partner.databinding.ActivityMessagesBinding
import com.blackmaria.partner.iconstant.Iconstant
import com.blackmaria.partner.latestpackageview.ApiRequest.MultipartRequest
import com.blackmaria.partner.latestpackageview.widgets.PkDialog
import com.blackmaria.partner.mylibrary.volley.ServiceRequest
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.yalantis.ucrop.UCrop
import com.yalantis.ucrop.util.BitmapLoadUtils
import kotlinx.android.synthetic.main.activity_messages.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.json.JSONException
import org.json.JSONObject
import pub.devrel.easypermissions.EasyPermissions
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class MessagesActivity : AppCompatActivity(), EasyPermissions.PermissionCallbacks,
EasyPermissions.RationaleCallbacks
{

    private var mRequest: ServiceRequest? = null

    private var dialog: Dialog? = null
    private var mImageCaptureUri: Uri? = null
    private  var outputUri:android.net.Uri? = null
    private val SELECT_IMAGE_REQUEST = 9
    var appDirectoryName = "blackmariachat"
    private val RC_LOCATION_CONTACTS_PERM = 124
    private var imageRoot: File? = null
    private  var destination:java.io.File? = null
    private var mHelper: DbHelper? = null
    private var dataBase: SQLiteDatabase? = null
    lateinit var binding: ActivityMessagesBinding
    private lateinit var messagesAdapter: MessagesAdapter
    lateinit var mess:ArrayList<TextMessage>
    private lateinit var viewModel: chatviewmodel
    private val TAKE_PHOTO_REQUEST = 18
    private lateinit var mContext: Activity
    private var dialogs: Dialog? = null
    var driverid:String=""
    var driverimage:String = ""
    private var array: ByteArray? = null
    var rideid:String=""
    var username:String=""
    var userimagee:String=""
    private var sessionManager: SessionManager? = null
    var useridtosend:String=""
    var userphonenumber:String =""
    var messagetemplate : BottomSheetDialog ? =null
    lateinit private var mSessionManager: SessionManager
    var ios_fcm_key:String=""
    var user_fcm_token:String = ""

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_messages)
        mContext = this@MessagesActivity
        sessionManager = SessionManager(mContext)
        intlizerpart()
        getExtravalue()
        setdataonview()
        initlizeadapter()
        initviewmodel()
        binding.setViewModel(viewModel)
    }
    fun intlizerpart()
    {
        mSessionManager = SessionManager(applicationContext!!)
        mHelper= DbHelper(this);
        val user: HashMap<String, String> = mSessionManager.getUserDetails()
        driverid= user[SessionManager.KEY_DRIVERID]!!
        driverimage= user[SessionManager.KEY_DRIVER_IMAGE]!!
        mess = ArrayList<TextMessage>()
    }
    fun getExtravalue()
    {
        rideid = intent?.getStringExtra("rideid").toString()
        username= intent?.getStringExtra("name").toString()
        userimagee= intent?.getStringExtra("userimage").toString()
        useridtosend= intent?.getStringExtra("useridtosend").toString()
        userphonenumber= intent?.getStringExtra("userphonenumber").toString()

        ios_fcm_key= intent?.getStringExtra("ios_fcm_key").toString()
        user_fcm_token= intent?.getStringExtra("user_fcm_token").toString()
    }
    fun setdataonview()
    {
        setimage(userimagee)
        usernames.text = username
    }



    fun locationAndContactsTask()
    {
        // Ask for one permission
        EasyPermissions.requestPermissions(
                this,
                "",
                RC_LOCATION_CONTACTS_PERM,
                Manifest.permission.CAMERA)
    }


    fun initviewmodel()
    {
        viewModel = ViewModelProviders.of(this).get(chatviewmodel::class.java)
        viewModel.closemodelobserver().observe(this, Observer {
            val inputManager = getSystemService(
                    Context.INPUT_METHOD_SERVICE) as InputMethodManager

            inputManager.hideSoftInputFromWindow(enter_message!!.windowToken,
                    InputMethodManager.HIDE_NOT_ALWAYS)
            finish()
        })
        viewModel.templateclickobserver().observe(this, Observer {

            val sPhoneMaskingStatus: String = sessionManager!!.getPhoneMaskingStatus()
            if (sPhoneMaskingStatus.equals("Yes", ignoreCase = true)) {
                postRequest_PhoneMasking(Iconstant.phoneMasking_url)
            } else {

             val number = userphonenumber
                val call = Uri.parse("tel:$number")
                val surf = Intent(Intent.ACTION_DIAL, call)
                startActivity(surf)
            }

        })
        viewModel.sendmessageobserver().observe(this, Observer {
            sendMessage(enter_message.text.toString())
        })
        viewModel.attachimageobsrvere().observe(this, Observer {
            locationAndContactsTask()
        })
        retrivelistfromdb()
    }
    private fun sendMessage(message:String)
    {
        if(message.equals(""))
        {
          Toast.makeText(applicationContext,"Message should not be empty",Toast.LENGTH_LONG).show()
        }
        else
        {
            val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault())
            val currentDateandTime = sdf.format(Date())

            var tsLong:Long = System.currentTimeMillis()/1000;
            var ts:String = tsLong.toString()
            savemessagetodb(message,driverid,ts,currentDateandTime)
            val domain: HashMap<String, String> = mSessionManager.getXmpp()
            val stringtoid = useridtosend + "@" + domain[SessionManager.KEY_HOST_URL]
            viewModel.sendxmppmessage(mContext,stringtoid,useridtosend,rideid,ts,message,currentDateandTime,driverid,ios_fcm_key,user_fcm_token)
            scrollToBottom()
        }
    }
    private fun scrollToBottom()
    {
        messages.scrollToPosition(messagesAdapter.itemCount - 1)
    }
    fun initlizeadapter()
    {
        val layoutMgr = LinearLayoutManager(this)
        layoutMgr.stackFromEnd = true
        messages.layoutManager = layoutMgr
        messagesAdapter = MessagesAdapter(mess,driverid,userimagee,driverimage,applicationContext)
        messages.adapter = messagesAdapter
    }
    fun setimage(userimagee:String)
    {
        if(userimagee.startsWith("http"))
        {
            Glide.with(mContext)
                    .asBitmap()
                    .apply(RequestOptions().override(60, 60))
                    .load(userimagee)
                    .into(object : CustomTarget<Bitmap>() {
                        override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                            runOnUiThread {
                                driverphoto.setImageBitmap(resource)
                            }
                        }
                        override fun onLoadCleared(placeholder: Drawable?)
                        {
                        }
                    })
        }
    }
    override fun onResume()
    {
        super.onResume()
        if(!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this)
        scrollToBottom()
    }
    override fun onDestroy()
    {
        super.onDestroy()
        if(EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this)
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun doThis(intentServiceResult: ChatIntentServiceResult)
    {
        var desc: String = intentServiceResult.desc
        var sender_ID: String = "others"
        var timestamp: String = intentServiceResult.timestamp
        var ride_id: String = intentServiceResult.ride_id
        if(ride_id.equals(rideid))
        {
            val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault())
            val currentDateandTime = sdf.format(Date())
            val textMessage = TextMessage(rideid,desc,sender_ID,timestamp,"1",currentDateandTime)
            enter_message.setText("")
            messagesAdapter.appendMessage(textMessage)
            scrollToBottom()
        }
        viewModel.updateallstatsu(mContext,rideid)
    }
    fun savemessagetodb(message:String,senderid:String,timestamp:String,currentDateandTime:String)
    {
        if(checkreocrdexist(timestamp) == 0)
        {
            val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault())
            val currentDateandTime = sdf.format(Date())
            viewModel.savemessagetodb(mContext,rideid,message,senderid,timestamp,currentDateandTime)
            val textMessage = TextMessage(rideid,message,senderid,timestamp,"1",currentDateandTime)
            enter_message.setText("")
            messagesAdapter.appendMessage(textMessage)

        }
    }
    fun retrivelistfromdb()
    {

        dataBase = mHelper!!.getWritableDatabase();
        var mCursor: Cursor = dataBase!!.rawQuery("SELECT * FROM " + DbHelper.TABLE_NAME+" WHERE rideid='"+rideid+"'", null);
        if (mCursor.moveToFirst()) {
            do {
                var rideid= mCursor.getString(mCursor.getColumnIndex(DbHelper.rideid))
                var message= mCursor.getString(mCursor.getColumnIndex(DbHelper.message))
                var senderid= mCursor.getString(mCursor.getColumnIndex(DbHelper.senderid))
                var timestamp= mCursor.getString(mCursor.getColumnIndex(DbHelper.timestamp))
                var status= mCursor.getString(mCursor.getColumnIndex(DbHelper.status))
                var dateandtime= mCursor.getString(mCursor.getColumnIndex(DbHelper.datetime))
                val textMessage = TextMessage(rideid,message,senderid,timestamp,status,dateandtime)
                messagesAdapter.appendMessage(textMessage)
            } while (mCursor.moveToNext());
        }
        mCursor.close()
        scrollToBottom()
        viewModel.updateallstatsu(mContext,rideid)
    }
    fun checkreocrdexist(timestamp:String):Int
    {
        var valueexist:Int=0
        dataBase = mHelper!!.getWritableDatabase();
        var mCursor: Cursor = dataBase!!.rawQuery("SELECT * FROM " + DbHelper.TABLE_NAME+" WHERE rideid='"+rideid+"' AND timestamp='"+timestamp+"'", null);
        if (mCursor.moveToFirst()) {
            do {
                valueexist=1
            } while (mCursor.moveToNext());
        }
        mCursor.close()
        return valueexist
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {

    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        attaachmentclick()

    }

    override fun onRationaleDenied(requestCode: Int) {

    }

    override fun onRationaleAccepted(requestCode: Int) {

    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_IMAGE_REQUEST) {
                onSelectFromGalleryResult(data!!)
            } else if (requestCode == TAKE_PHOTO_REQUEST) {
                onCaptureImageResult(data)
            }else if (requestCode == UCrop.REQUEST_CROP) {
                onCroppedImageResult(data)
            } else if (resultCode == UCrop.RESULT_ERROR) {
                val cropError = UCrop.getError(data!!)
            }
        }
    }


    fun onCroppedImageResult(data: Intent?) {
        val resultUri = UCrop.getOutput(data!!)
        Log.d("Crop success", "" + resultUri)
        try {
            val bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), resultUri)
            if (bitmap == null) {
                Log.d("Bitmap", "null")
            } else {
                Log.d("Bitmap", "not null")
              //  Toast.makeText(applicationContext,"success",Toast.LENGTH_LONG).show()
            }
            var thumbnail = bitmap
            val picturePath = resultUri!!.path
            val byteArrayOutputStream = ByteArrayOutputStream()
            val curFile = File(picturePath)
            try {
                val exif = ExifInterface(curFile.path)
                val rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
                val rotationInDegrees = BitmapLoadUtils.exifToDegrees(rotation)
                val matrix = Matrix()
                if (rotation.toFloat() != 0f) {
                    matrix.preRotate(rotationInDegrees.toFloat())
                }
                thumbnail = Bitmap.createBitmap(thumbnail!!, 0, 0, thumbnail.width, thumbnail.height, matrix, true)
            } catch (ex: IOException) {
                Log.e("TAG", "Failed to get Exif data", ex)
            }
            thumbnail!!.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream)
            array = byteArrayOutputStream.toByteArray()
            UploadDriverImage(Iconstant.uploadimageforchat)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }


    private fun UploadDriverImage(url: String)
    {
        dialog = Dialog(mContext, R.style.CustomDialogTheme)
        dialog!!.getWindow()
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.loading_model)
        dialog!!.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()


        val mRequest = MultipartRequest(mContext, "blackmariachat.jpg")
        mRequest.makeServiceRequest(url, Request.Method.POST, array, object : MultipartRequest.ServiceListener {
            override fun onCompleteListener(response: String) {
                val sStatus = ""
                val sResponse = ""
                val SUser_image = ""
                val Smsg = ""
                try {
                    dialog!!.dismiss()
                    val json = JSONObject(response)
                    sendMessage(json.getString("image_path"))
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }

            override fun onErrorListener() {
                dialog!!.dismiss()
            }
        })
    }
    fun onSelectFromGalleryResult(data: Intent) {
        try {
            mImageCaptureUri = data.data
            val bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), mImageCaptureUri)
            if (!imageRoot!!.exists()) {
                imageRoot!!.mkdir()
            }


            val filename = "chatimage.png"
            val sd = Environment.getExternalStorageDirectory()
            val dest = File(sd, filename)


            try {
                val out = FileOutputStream(dest)
                bitmap!!.compress(Bitmap.CompressFormat.PNG, 90, out)
                out.flush()
                out.close()
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }



            outputUri = Uri.fromFile(dest)
            val options = UCrop.Options()
            options.setStatusBarColor(getResources().getColor(R.color.black_color))
            options.setToolbarColor(getResources().getColor(R.color.app_primary_color))
            options.setMaxBitmapSize(800)
            UCrop.of(mImageCaptureUri!!, outputUri!!)
                    .withAspectRatio(1f, 1f)
                    .withMaxResultSize(8000, 8000)
                    .withOptions(options)
                    .start(this@MessagesActivity)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }


    fun dateToString(date: Date?, format: String?): String {
        val df = SimpleDateFormat(format)
        return df.format(date)
    }

    fun galleryintent()
    {
        try {


            imageRoot = File(Environment.getExternalStorageDirectory(), appDirectoryName)

            if (!imageRoot!!.exists()) {
                imageRoot!!.mkdir()
            }
            val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            intent.type = "image/*"
            startActivityForResult(intent,SELECT_IMAGE_REQUEST)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    fun attaachmentclick()
    {
        val builder = AlertDialog.Builder(this)
        //set title for alert dialog
        builder.setTitle("Choose an action")
        //set message for alert dialog



        //performing positive action
        builder.setPositiveButton("Camera"){dialogInterface, which ->
            cameraIntent()
        }
        //performing cancel action
        builder.setNeutralButton("Close"){dialogInterface , which ->
         }
        //performing negative action
        builder.setNegativeButton("Gallery"){dialogInterface, which ->
            galleryintent();
        }
        // Create the AlertDialog
        val alertDialog: AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)
        alertDialog.show()
    }


    fun cameraIntent() {
        try {
            val pictureIntent = Intent(
                    MediaStore.ACTION_IMAGE_CAPTURE)
            if (pictureIntent.resolveActivity(getPackageManager()) != null) {
                //Create a file to store the image
                imageRoot = File(Environment.getExternalStorageDirectory(), appDirectoryName)

                if (!imageRoot!!.exists()) {
                    imageRoot!!.mkdir()
                }
            }
            if (imageRoot != null) {
                val name = dateToString(Date(), "yyyy-MM-dd-hh-mm-ss")
                destination = File(imageRoot, "$name.jpg")
                val photoURI = FileProvider.getUriForFile(mContext, "com.blackmaria.provider", destination!!)
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        photoURI)
                pictureIntent.putExtra("android.intent.extras.CAMERA_FACING", 1)
                startActivityForResult(pictureIntent,
                        TAKE_PHOTO_REQUEST)
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }


    fun onCaptureImageResult(data: Intent?) {
        try {
            val imagePath = destination!!.absolutePath
            mImageCaptureUri = Uri.fromFile(File(imagePath))
            outputUri = mImageCaptureUri

//            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), mImageCaptureUri);
            val options = UCrop.Options()
            options.setStatusBarColor(getResources().getColor(R.color.black_color))
            options.setToolbarColor(getResources().getColor(R.color.app_primary_color))
            options.setMaxBitmapSize(800)
            UCrop.of(mImageCaptureUri!!, outputUri!!)
                    .withAspectRatio(1f, 1f)
                    .withMaxResultSize(8000, 8000)
                    .withOptions(options)
                    .start(mContext)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }


    //---------------------------------------------------------------------
    private fun postRequest_PhoneMasking(Url: String) {
        dialogs = Dialog(mContext)
        dialogs!!.window
        dialogs!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialogs!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogs!!.setContentView(R.layout.custom_loading)
        dialogs!!.setCanceledOnTouchOutside(false)
        dialogs!!.show()
        val dialog_title = dialogs!!.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_pleasewait)
        val jsonParams = HashMap<String, String>()
        jsonParams["ride_id"] = rideid
        jsonParams["user_type"] = "driver"
        println("-------------PhoneMasking Url----------------$Url")
        mRequest = ServiceRequest(mContext)
        mRequest!!.makeServiceRequest(Url, Request.Method.POST, jsonParams, object : ServiceRequest.ServiceListener {
            override fun onCompleteListener(response: String) {
                println("-------------PhoneMasking Response----------------$response")
                var Str_status = ""
                var SResponse = ""
                try {
                    val `object` = JSONObject(response)
                    Str_status = `object`.getString("status")
                    SResponse = `object`.getString("response")
                    if (Str_status.equals("1", ignoreCase = true)) {
                        Alert(resources.getString(R.string.action_success), SResponse)
                    } else {
                        Alert(resources.getString(R.string.action_error), SResponse)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                    dialogs!!.dismiss()
                }
                dialogs!!.dismiss()
            }

            override fun onErrorListener() {
                dialogs!!.dismiss()
            }
        })
    }

    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {
        val mDialog = PkDialog(mContext)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(resources.getString(R.string.alert_ok)) { mDialog.dismiss() }
        mDialog.show()
    }
}
