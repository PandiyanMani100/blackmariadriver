package com.blackmaria.partner.app;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.Pojo.KeyValueBean;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.adapter.DeactivateStatementAdapter;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by GANESH on 29-08-2017.
 */

public class DeactivateStatement extends ActivityHockeyApp implements View.OnClickListener {

    private RelativeLayout RL_closeAccount, RL_cancel;
    private ExpandableHeightListView Lv_statement;
    private TextView Tv_totalAmount;

    private String sDriverID = "", sCurrency = "", sTotalAmount = "";
    private ArrayList<KeyValueBean> statementList;
private ImageView img_back;
    private ConnectionDetector cd;
    private DeactivateStatementAdapter adapter;
    private SessionManager session;
    private ServiceRequest mRequest;
    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.deactivation_statement);

        initialize();


    }

    private void initialize() {

        statementList = new ArrayList<>();
        cd = new ConnectionDetector(DeactivateStatement.this);
        session = new SessionManager(DeactivateStatement.this);
        sDriverID = session.getUserDetails().get(SessionManager.KEY_DRIVERID);

        getIntentData();
        img_back=(ImageView)findViewById(R.id.img_back);
        Lv_statement = (ExpandableHeightListView) findViewById(R.id.Lv_last_statement);
        RL_closeAccount = (RelativeLayout) findViewById(R.id.RL_close_account);
        RL_cancel = (RelativeLayout) findViewById(R.id.RL_cancel);
        Tv_totalAmount = (TextView) findViewById(R.id.txt_total_amount);

        if (statementList.size() > 0) {
            adapter = new DeactivateStatementAdapter(DeactivateStatement.this, statementList);
            Lv_statement.setAdapter(adapter);
        }

        RL_closeAccount.setOnClickListener(this);
        RL_cancel.setOnClickListener(this);
        img_back.setOnClickListener(this);

    }

    private void getIntentData() {

        Intent intent = getIntent();
        if (intent != null) {
            sCurrency = intent.getStringExtra("Currency");
            sTotalAmount = intent.getStringExtra("TotalAmount");
            statementList = (ArrayList<KeyValueBean>) intent.getSerializableExtra("StatementList");
        }

    }

    @Override
    public void onClick(View view) {

        if (cd.isConnectingToInternet()) {

            if (view == RL_closeAccount) {

                PostRequestDeactivate(Iconstant.deactivate_account_url);

            } else if (view == RL_cancel) {

                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);

            }else if (view == img_back) {

                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }

        } else {
            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }
    }

    private void PostRequestDeactivate(String url) {

        showDialog();

        HashMap<String, String> jsonParams = new HashMap<>();
        jsonParams.put("driver_id", sDriverID);

        System.out.println("-------------Deactivation Url----------------" + url);
        System.out.println("-------------Deactivation Params----------------" + jsonParams);

        mRequest = new ServiceRequest(DeactivateStatement.this);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Deactivation Response----------------" + response);

                dismissDialog();

                String Sstatus = "", Sresponse;
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    Sresponse = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        processingLogout(getResources().getString(R.string.action_success), getResources().getString(R.string.label_account_has_been_closed));
                    } else {
                        Alert(getResources().getString(R.string.action_error), Sresponse);
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


            }

            @Override
            public void onErrorListener() {

            }
        });


    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(DeactivateStatement.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void processingLogout(String title, String message) {

        final Dialog accountClosedDialog = new Dialog(DeactivateStatement.this);
        accountClosedDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        accountClosedDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        accountClosedDialog.setCancelable(true);
        accountClosedDialog.setContentView(R.layout.alert_account_closed);

        final TextView Tv_title = (TextView) accountClosedDialog.findViewById(R.id.txt_label_title);
        final TextView Tv_message = (TextView) accountClosedDialog.findViewById(R.id.txt_label_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        accountClosedDialog.show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
//                    PostLogout(Iconstant.Profile_driver_account_Logout_Url);
                    session.logoutUser();
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 3000);

    }

    /*private void PostLogout(String url) {
        dialog = new Dialog(DeactivateStatement.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_processing));

        HashMap<String, String> jsonParams = new HashMap<>();
        jsonParams.put("driver_id", sDriverID);
        jsonParams.put("device", "ANDROID");

        System.out.println("-------------Logout Url----------------" + url);
        System.out.println("-------------Logout jsonParams----------------" + jsonParams);

        mRequest = new ServiceRequest(DeactivateStatement.this);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Logout Response----------------" + response);

                String Sstatus = "";
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        session.logoutUser();
                        finish();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });

    }*/

    private void showDialog() {

        dialog = new Dialog(DeactivateStatement.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_processing));

    }

    private void dismissDialog() {

        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }

    }

}
