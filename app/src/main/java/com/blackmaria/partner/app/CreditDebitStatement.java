package com.blackmaria.partner.app;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.blackmaria.partner.Pojo.WalletMoneyTransactionPojo;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.CurrencySymbolConverter;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.adapter.CreditDebitStatementAdapter;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.HashMap;
import java.util.Locale;

public class CreditDebitStatement extends AppCompatActivity implements View.OnClickListener, ApIServices.completelisner {

    private ImageView Iv_back, Iv_statement, Iv_previous, Iv_next, Iv_prevMonth, Iv_nextMonth;
    private ListView Lv_credit_debit_statement;
    private TextView Tv_statementTitle, Tv_month, Tv_empty,Tv_wallet_type;
    private LinearLayout RL_statement;
    private LinearLayout LL_title;
    private ConnectionDetector cd;
    private SessionManager session;
    private Dialog dialog;
    private CreditDebitStatementAdapter adapter;
    private String sDriverID = "", sType = "";
    private String perPage = "100", ScurrentYear = "", ScurrentMonth = "";
    private boolean isTransactionAvailable = false;
    private ArrayList<WalletMoneyTransactionPojo> listStatement;
    private int currentPage = 1;
    int currentYear, currentMonth, currentMonthStatic, ScurrentYearStatic;
    private String sCurrentPage = "", sNextPage = "";
    private ProgressBar apiload;
    AccessLanguagefromlocaldb db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.credit_debit_statement);
        db = new AccessLanguagefromlocaldb(this);
        try {
            initialize();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("WrongConstant")
    private void initialize() throws ParseException {
        session = new SessionManager(CreditDebitStatement.this);
        cd = new ConnectionDetector(CreditDebitStatement.this);
        listStatement = new ArrayList<WalletMoneyTransactionPojo>();

        Intent intent = getIntent();
        sType = intent.getStringExtra("type");
        apiload = findViewById(R.id.apiload);
        Iv_back = (ImageView) findViewById(R.id.walletback);
        Tv_month = (TextView) findViewById(R.id.credit_debit_month);
        RL_statement = (LinearLayout) findViewById(R.id.li_creditdebit);
        Iv_statement = (ImageView) findViewById(R.id.img_statement);
        Lv_credit_debit_statement = (ListView) findViewById(R.id.wallet_money_transaction_listview);
        Tv_statementTitle = (TextView) findViewById(R.id.credit_debit_statement);
        Tv_empty = (TextView) findViewById(R.id.wallet_money_transaction_listview_empty_text);
        Iv_next = (ImageView) findViewById(R.id.next_page);
        Iv_previous = (ImageView) findViewById(R.id.prev_page);
        Iv_nextMonth = (ImageView) findViewById(R.id.next_month);
        Iv_prevMonth = (ImageView) findViewById(R.id.prev_month);
        Tv_wallet_type = (TextView) findViewById(R.id.textView2);
        //LL_title = (LinearLayout) findViewById(R.id.LL_title);
        Iv_back.setOnClickListener(this);
        Iv_previous.setOnClickListener(this);
        Iv_next.setOnClickListener(this);
        Iv_prevMonth.setOnClickListener(this);
        Iv_nextMonth.setOnClickListener(this);
        HashMap<String, String> user = session.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        Calendar c = Calendar.getInstance();
        currentMonthStatic = c.get(Calendar.MONTH) + 1;
        ScurrentYearStatic = c.get(Calendar.YEAR);
        currentYear = c.get(Calendar.YEAR);
        currentMonth = c.get(Calendar.MONTH) + 1;
        ScurrentYear = currentYear + "";
        ScurrentMonth = (currentMonth) + "";

        postData();

    }

    @SuppressLint("WrongConstant")
    @Override
    public void onClick(View view) {
        if (view == Iv_back) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (view == Iv_prevMonth) {
            currentMonth = currentMonth - 1;
            System.out.println("============= PrevMonth1=======" + currentMonth);
            if (currentMonth == 0) {
                currentMonth = 12;
                System.out.println("============= PrevMonth2=======" + currentMonth);
                currentYear = currentYear - 1;
                System.out.println("============= PrevYear=======" + currentYear);
            }
            ScurrentYear = currentYear + "";
            ScurrentMonth = currentMonth + "";
            currentPage = 1;
            postData();
        } else if (view == Iv_nextMonth) {
            currentMonth = currentMonth + 1;
            System.out.println("============= NextMonth1=======" + currentMonth);
            if (currentMonth > 12) {
                currentMonth = 1;
                System.out.println("============= NextMonth2=======" + currentMonth);
                currentYear = currentYear + 1;
                System.out.println("============= NextYear=======" + currentYear);
            }
            ScurrentYear = currentYear + "";
            ScurrentMonth = currentMonth + "";
            currentPage = /*currentPage +*/ 1;
            postData();
        } else if (view == Iv_previous) {
            currentPage = currentPage - 1;
            postData();
        } else if (view == Iv_next) {
            Iv_previous.setVisibility(View.VISIBLE);
            currentPage = currentPage + 1;
            postData();
        }

    }

    public String formatMonth(String month, String Year) throws ParseException {
        SimpleDateFormat monthParse = new SimpleDateFormat("MM");
        SimpleDateFormat monthDisplay = new SimpleDateFormat("MMMM");
        String month1 = monthDisplay.format(monthParse.parse(month)).toUpperCase();
        Tv_month.setText(month1 + " " + Year);
        if (sType.equalsIgnoreCase("debit")) {
            Tv_statementTitle.setText(db.getvalue("wallet_debit_statement"));
            Tv_wallet_type.setText(db.getvalue("wallet_moneyout"));
            RL_statement.setBackgroundResource(R.drawable.flat_red_graident_bg1);
        } else {
            Tv_statementTitle.setText(db.getvalue("wallet_credit_statement"));
            Tv_wallet_type.setText(db.getvalue("wallet_moneyin"));
            RL_statement.setBackgroundResource(R.drawable.trackcar_vg);
        }
        return monthDisplay.format(monthParse.parse(month));
    }

    private void postData() {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        jsonParams.put("type", sType);
        jsonParams.put("month", ScurrentMonth);
        jsonParams.put("year", ScurrentYear);
        jsonParams.put("page", String.valueOf(currentPage));
        jsonParams.put("perPage", perPage);
        apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(CreditDebitStatement.this, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        ApIServices apIServices = new ApIServices(CreditDebitStatement.this, CreditDebitStatement.this);
        apIServices.dooperation(Iconstant.wallet_money_transaction_url, jsonParams);
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(CreditDebitStatement.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(db.getvalue("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    //method to convert currency code to currency symbol
    private static Locale getLocale(String strCode) {
        for (Locale locale : NumberFormat.getAvailableLocales()) {
            String code = NumberFormat.getCurrencyInstance(locale).getCurrency().getCurrencyCode();
            if (strCode.equals(code)) {
                return locale;
            }
        }
        return null;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            onBackPressed();
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
            return true;
        }

        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void sucessresponse(String response) {
        dialog.dismiss();
        apiload.setVisibility(View.INVISIBLE);
        String Sstatus = "", Str_CurrentPage = "", str_NextPage = "", perPage = "", ScurrencySymbol = "", Str_total_amount = "", Str_total_transaction = "";
        try {
            JSONObject object = new JSONObject(response);
            Sstatus = object.getString("status");

            if (Sstatus.equalsIgnoreCase("1")) {
                JSONObject response_object = object.getJSONObject("response");
                ScurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(response_object.getString("currency"));
                Str_total_amount = response_object.getString("total_amount");
                Str_total_transaction = response_object.getString("total_transaction");


                if (response_object.length() > 0) {
                    Currency currencycode = Currency.getInstance(getLocale(response_object.getString("currency")));

                    Object check_trans_object = response_object.get("trans");
                    listStatement.clear();
                    if (check_trans_object instanceof JSONArray) {
                        JSONArray trans_array = response_object.getJSONArray("trans");
                        if (trans_array.length() > 0) {
                            for (int i = 0; i < trans_array.length(); i++) {
                                JSONObject trans_object = trans_array.getJSONObject(i);

                                WalletMoneyTransactionPojo pojo = new WalletMoneyTransactionPojo();
                                pojo.setTrans_type(trans_object.getString("type"));
                                pojo.setTrans_amount(trans_object.getString("trans_amount"));
                                pojo.setTitle(trans_object.getString("title"));
                                pojo.setTrans_date(trans_object.getString("trans_date"));
                                pojo.setTrans_time(trans_object.getString("trans_time"));
                                pojo.setFormatedDate(trans_object.getString("date"));
                                pojo.setBalance_amount(trans_object.getString("balance_amount"));
                                pojo.setImage(trans_object.getString("image"));
                                pojo.setCurrencySymbol(currencycode.getSymbol());
                                listStatement.add(pojo);

                            }
                            isTransactionAvailable = true;
                        } else {
                            isTransactionAvailable = false;
                        }
                    } else {
                        isTransactionAvailable = false;
                    }

                    sCurrentPage = response_object.getString("current_page");
                    sNextPage = response_object.getString("next_page");

                    currentPage = Integer.parseInt(response_object.getString("current_page"));
                    str_NextPage = response_object.getString("next_page");
                    perPage = response_object.getString("perPage");
                }
            }
            if (Sstatus.equalsIgnoreCase("1")) {
                if (isTransactionAvailable) {
                    Tv_empty.setVisibility(View.GONE);
                    Lv_credit_debit_statement.setVisibility(View.VISIBLE);
                    //   LL_title.setVisibility(View.VISIBLE);

                    adapter = new CreditDebitStatementAdapter(CreditDebitStatement.this, listStatement);
                    Lv_credit_debit_statement.setAdapter(adapter);
                } else {
                    Tv_empty.setVisibility(View.VISIBLE);
                    Lv_credit_debit_statement.setVisibility(View.GONE);
                    // LL_title.setVisibility(View.GONE);
                }

                try {
                    formatMonth(ScurrentMonth, ScurrentYear);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if (sNextPage.length() > 0) {
                    Iv_next.setVisibility(View.VISIBLE);
                } else {
                    Iv_next.setVisibility(View.GONE);
                }

                if (sCurrentPage.length() > 0) {
                    if (Integer.parseInt(sCurrentPage) > 1) {
                        Iv_previous.setVisibility(View.VISIBLE);
                    } else {
                        Iv_previous.setVisibility(View.GONE);
                    }
                }

                if (Integer.parseInt(ScurrentMonth) >= currentMonthStatic && Integer.parseInt(ScurrentYear) >= ScurrentYearStatic) {
                    Iv_nextMonth.setVisibility(View.GONE);
                } else {
                    Iv_nextMonth.setVisibility(View.VISIBLE);
                }

            } else {
                String Sresponse = object.getString("response");
                Alert(db.getvalue("alert_label_title"), Sresponse);
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void errorreponse() {
        dialog.dismiss();
        apiload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void jsonexception() {
        dialog.dismiss();
        apiload.setVisibility(View.INVISIBLE);
    }
}
