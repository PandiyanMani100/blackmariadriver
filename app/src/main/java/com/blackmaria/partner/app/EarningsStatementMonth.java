package com.blackmaria.partner.app;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.Pojo.EarningsMonthDetail;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Dashboard_constrain;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by Ganesh on 17-05-2017.
 */

public class EarningsStatementMonth extends ActivityHockeyApp implements View.OnClickListener {

    private ImageView Iv_back, Iv_home, Iv_prev, Iv_next;
    private TextView Tv_displayDate;
    private TextView Tv_earningsWeek1, Tv_earningsWeek2, Tv_earningsWeek3, Tv_earningsWeek4, Tv_earningsWeek5, Tv_totalEarnings;
    private RelativeLayout RL_week1, RL_week2, RL_week3, RL_week4, RL_week5;

    private SessionManager sessionManager;
    private ConnectionDetector cd;
    private Dialog dialog;
    private ServiceRequest mRequest;
    private Calendar calendar = null;
    private SimpleDateFormat mDisplayFormat;
    private SimpleDateFormat monthFormat, yearFormat;

    private String sDriveID = "", sDisplayDate = "", sFilterMonth = "", sFilterYear = "",
            sCurrentMonth = "", sCurrentYear = "", sTotalAmount = "", sCurrency = "";
    private ArrayList<EarningsMonthDetail> listMonthDetail;
    private boolean isDataAvailable = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.earnings_statement_month);

        initialize();

    }

    private void initialize() {

        sessionManager = new SessionManager(EarningsStatementMonth.this);
        cd = new ConnectionDetector(EarningsStatementMonth.this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriveID = user.get(SessionManager.KEY_DRIVERID);
        listMonthDetail = new ArrayList<EarningsMonthDetail>();

        Iv_back = (ImageView) findViewById(R.id.img_back);
        Iv_home = (ImageView) findViewById(R.id.img_home);
        Iv_prev = (ImageView) findViewById(R.id.img_prev);
        Iv_next = (ImageView) findViewById(R.id.img_next);
        Tv_displayDate = (TextView) findViewById(R.id.txt_date);
        Tv_earningsWeek1 = (TextView) findViewById(R.id.txt_earning_week1);
        Tv_earningsWeek2 = (TextView) findViewById(R.id.txt_earning_week2);
        Tv_earningsWeek3 = (TextView) findViewById(R.id.txt_earning_week3);
        Tv_earningsWeek4 = (TextView) findViewById(R.id.txt_earning_week4);
        Tv_earningsWeek5 = (TextView) findViewById(R.id.txt_earning_week5);
        Tv_totalEarnings = (TextView) findViewById(R.id.txt_total_earnings);
        RL_week1 = (RelativeLayout) findViewById(R.id.Rl_week1);
        RL_week2 = (RelativeLayout) findViewById(R.id.Rl_week2);
        RL_week3 = (RelativeLayout) findViewById(R.id.Rl_week3);
        RL_week4 = (RelativeLayout) findViewById(R.id.Rl_week4);
        RL_week5 = (RelativeLayout) findViewById(R.id.Rl_week5);

        Iv_back.setOnClickListener(this);
        Iv_home.setOnClickListener(this);
        Iv_prev.setOnClickListener(this);
        Iv_next.setOnClickListener(this);
        RL_week1.setOnClickListener(this);
        RL_week2.setOnClickListener(this);
        RL_week3.setOnClickListener(this);
        RL_week4.setOnClickListener(this);
        RL_week5.setOnClickListener(this);

        getCurrentMonthAndYear();
        getIntentData();

        if (cd.isConnectingToInternet()) {
            EarningsStatementMonthRequest(Iconstant.earnings_monthwise_url);
        } else {
            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }


    }

    // Steps to Get Current Month And Year
    private void getCurrentMonthAndYear() {

        calendar = Calendar.getInstance();
        mDisplayFormat = new SimpleDateFormat("MMMM yyyy");
        monthFormat = new SimpleDateFormat("MM");
        yearFormat = new SimpleDateFormat("yyyy");
        sFilterMonth = monthFormat.format(calendar.getTime());
        sFilterYear = yearFormat.format(calendar.getTime());
        sCurrentMonth = sFilterMonth;
        sCurrentYear = sFilterYear;

    }

    @SuppressWarnings({"WrongConstant", "deprecation"})
    private void getIntentData() {

        Intent intent = getIntent();
        if (intent != null) {

            try {
                calendar = Calendar.getInstance();

                sFilterMonth = intent.getStringExtra("filterMonth");
                sFilterYear = intent.getStringExtra("filterYear");

                calendar.set(Calendar.MONTH, Integer.parseInt(sFilterMonth) - 1);
                calendar.set(Calendar.YEAR, Integer.parseInt(sFilterYear));

                sDisplayDate = mDisplayFormat.format(calendar.getTime());

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(EarningsStatementMonth.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    @SuppressLint("WrongConstant")
    @Override
    public void onClick(View view) {

        if (view == Iv_back) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (view == Iv_home) {
            Intent intent = new Intent(EarningsStatementMonth.this, Dashboard_constrain.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (view == Iv_prev) {

            if (calendar != null) {
                if (cd.isConnectingToInternet()) {

                    calendar.add(Calendar.MONTH, -1);

                    sFilterMonth = monthFormat.format(calendar.getTime());
                    sFilterYear = yearFormat.format(calendar.getTime());
                    sDisplayDate = mDisplayFormat.format(calendar.getTime());

                    EarningsStatementMonthRequest(Iconstant.earnings_monthwise_url);

                } else {
                    Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                }

            }

        } else if (view == Iv_next) {

            if (calendar != null) {
                if (cd.isConnectingToInternet()) {

                    calendar.add(Calendar.MONTH, 1);

                    sFilterMonth = monthFormat.format(calendar.getTime());
                    sFilterYear = yearFormat.format(calendar.getTime());
                    sDisplayDate = mDisplayFormat.format(calendar.getTime());

                    EarningsStatementMonthRequest(Iconstant.earnings_monthwise_url);

                } else {
                    Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                }

            }

        } else if (view == RL_week1) {

            if (listMonthDetail.size() > 0) {
                Intent intent = new Intent(EarningsStatementMonth.this, EarningsStatementWeek.class);
                intent.putExtra("filterFromDate", listMonthDetail.get(0).getStartdate_f());
                intent.putExtra("filterToDate", listMonthDetail.get(0).getEndate_f());
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }

        } else if (view == RL_week2) {

            if (listMonthDetail.size() > 0) {
                Intent intent = new Intent(EarningsStatementMonth.this, EarningsStatementWeek.class);
                intent.putExtra("filterFromDate", listMonthDetail.get(1).getStartdate_f());
                intent.putExtra("filterToDate", listMonthDetail.get(1).getEndate_f());
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }

        } else if (view == RL_week3) {

            if (listMonthDetail.size() > 0) {
                Intent intent = new Intent(EarningsStatementMonth.this, EarningsStatementWeek.class);
                intent.putExtra("filterFromDate", listMonthDetail.get(2).getStartdate_f());
                intent.putExtra("filterToDate", listMonthDetail.get(2).getEndate_f());
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }

        } else if (view == RL_week4) {

            if (listMonthDetail.size() > 0) {
                Intent intent = new Intent(EarningsStatementMonth.this, EarningsStatementWeek.class);
                intent.putExtra("filterFromDate", listMonthDetail.get(3).getStartdate_f());
                intent.putExtra("filterToDate", listMonthDetail.get(3).getEndate_f());
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }

        } else if (view == RL_week5) {

            if (listMonthDetail.size() > 0) {
                Intent intent = new Intent(EarningsStatementMonth.this, EarningsStatementWeek.class);
                intent.putExtra("filterFromDate", listMonthDetail.get(4).getStartdate_f());
                intent.putExtra("filterToDate", listMonthDetail.get(4).getEndate_f());
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }

        }
    }

    @SuppressLint("WrongConstant")
    private void EarningsStatementMonthRequest(String Url) {

        dialog = new Dialog(EarningsStatementMonth.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriveID);
        jsonParams.put("month", sFilterMonth);
        jsonParams.put("year", sFilterYear);

        System.out.println("--------------EarningsStatementMonth Url-------------------" + Url);
        System.out.println("--------------EarningsStatementMonth jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(EarningsStatementMonth.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------EarningsStatementMonth Response-------------------" + response);
                String status = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {
                            JSONObject jsonObject = object.getJSONObject("response");
                            if (jsonObject.length() > 0) {

//                                sDisplayDate = jsonObject.getString("date_desc");
                                sCurrency = jsonObject.getString("currency");
                                sTotalAmount = jsonObject.getString("total_income");

                                Object objMonth = jsonObject.get("monthreport");
                                if (objMonth instanceof JSONArray) {
                                    JSONArray jarMonth = jsonObject.getJSONArray("monthreport");
                                    if (jarMonth.length() > 0) {

                                        listMonthDetail = new ArrayList<EarningsMonthDetail>();
                                        for (int i = 0; i < jarMonth.length(); i++) {

                                            JSONObject jobjMonth = jarMonth.getJSONObject(i);

                                            String week = jobjMonth.getString("week");
                                            String WeekCount = jobjMonth.getString("WeekCount");
                                            String ridecount = jobjMonth.getString("ridecount");
                                            String distance = jobjMonth.getString("distance");
                                            String income = jobjMonth.getString("income");
                                            String currency = jobjMonth.getString("currency");
                                            String startdate = jobjMonth.getString("startdate");
                                            String endate = jobjMonth.getString("endate");
                                            String startdate_f = jobjMonth.getString("startdate_f");
                                            String endate_f = jobjMonth.getString("endate_f");

                                            listMonthDetail.add(new EarningsMonthDetail(week, WeekCount, ridecount, distance, income,
                                                    currency, startdate, endate, startdate_f, endate_f));

                                        }

                                        isDataAvailable = true;

                                    } else {
                                        isDataAvailable = false;
                                    }
                                } else {
                                    isDataAvailable = false;
                                }

                            } else {
                                isDataAvailable = false;
                            }
                        } else {
                            String sResponse = object.getString("response");
                            Alert(getResources().getString(R.string.action_error), sResponse);
                        }

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (status.equalsIgnoreCase("1")) {

                    if (listMonthDetail.size() != 5) {
                        RL_week5.setVisibility(View.GONE);
                    }

                    for (int i = 0; i < listMonthDetail.size(); i++) {
                        String sAmount = listMonthDetail.get(i).getIncome();

                        if (sAmount.length() > 0) {
                            if (sAmount.equalsIgnoreCase("-")) {
                                sAmount = "0";
                            }
                        } else {
                            sAmount = "0";
                        }

                        String earningsAmount = listMonthDetail.get(i).getCurrency() + sAmount;

                        switch (i) {

                            case 0:
                                Tv_earningsWeek1.setText(earningsAmount);
                                break;
                            case 1:
                                Tv_earningsWeek2.setText(earningsAmount);
                                break;
                            case 2:
                                Tv_earningsWeek3.setText(earningsAmount);
                                break;
                            case 3:
                                Tv_earningsWeek4.setText(earningsAmount);
                                break;
                            case 4:
                                Tv_earningsWeek5.setText(earningsAmount);
                                RL_week5.setVisibility(View.VISIBLE);
                                break;
                        }

                    }
                    Tv_totalEarnings.setText(sCurrency + " " + sTotalAmount);
                    Tv_displayDate.setText(sDisplayDate);

                    if (sCurrentYear.equalsIgnoreCase(sFilterYear)) {
                        if (sCurrentMonth.equalsIgnoreCase(sFilterMonth)) {
                            Iv_next.setVisibility(View.GONE);
                        } else {
                            Iv_next.setVisibility(View.VISIBLE);
                        }
                    } else {
                        Iv_next.setVisibility(View.VISIBLE);
                    }

                }

                if (dialog != null) {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
            return true;
        }
        return false;
    }

}
