package com.blackmaria.partner.app;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.FragmentActivity;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.android.volley.Request;
import com.blackmaria.partner.Interface.FunctionComplete;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ChatAvailabilityCheck;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.GPSTracker;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Locale;


public class OtpPage extends FragmentActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, FunctionComplete {

    private PinEntryEditText Pet_Otp;
    private ImageView Iv_light, Iv_insertReferral;
    private RelativeLayout RL_Login, RL_Resend, RL_ChangeNumber;
    private SessionManager session;
    private ConnectionDetector cd;
    private boolean isInternetPresent = false;
    private String Sphone = "", ScountryCode = "", SdeviceToken = "", SgcmId = "";
    private String Sotp_Status = "", Sotp = "";
    private ServiceRequest mRequest;
    Dialog dialog;
    private String SuserId = "", Spage = " ";
    private String sDriverID = "", sReferralStatus = "", sReferralCode = "",
            sReferralDriverName = "", sReferralDriverImage = "", sReferralDriverPercentage = "", app_identity_name = "", Language_code = "", sPrivacyStatus = "",
            sVehicleType = "", Sis_alive_other = "", sContact_mail = "", sCustomerServiceNumber = "", sSiteUrl = "", sXmppHostUrl = "", sHostName = "",
            sFacebookId = "", sGooglePlusId = "", sPhoneMasking = "", sCarImage = "", sUserGuide = "", sSecurePin = "";
    private boolean isReferralCodeVerified = false;
    private boolean isAppInfoAvailable = false;

    GPSTracker gps;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    PendingResult<LocationSettingsResult> result;
    final static int REQUEST_LOCATION = 199;
    private String sLatitude = "", sLongitude = "";

    FunctionComplete completeInterface;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_page);
        completeInterface = this;
        gps = new GPSTracker(OtpPage.this);
        initialize();

        RL_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String otp = Pet_Otp.getText().toString().trim();

                if (!"Profile".equalsIgnoreCase(Spage)) {
                    if (otp.length() == 0) {
                        Alert("", getResources().getString(R.string.otp_label_alert_otp));
                    } else if (!Sotp.equals(otp)) {
//                        Alert("", getResources().getString(R.string.otp_label_alert_invalid));
                        AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.otp_label_alert_invalid));
                    } else {
                        cd = new ConnectionDetector(OtpPage.this);
                        isInternetPresent = cd.isConnectingToInternet();

                        if (isInternetPresent) {

                            if (gps.isgpsenabled() && gps.canGetLocation()) {

                                if (gps.getLatitude() == 0.0 || gps.getLongitude() == 0.0) {
                                    Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.could_not_fetch_location_retry));
                                } else {
                                    sLatitude = String.valueOf(gps.getLatitude());
                                    sLongitude = String.valueOf(gps.getLongitude());

                                    if (sPrivacyStatus.equalsIgnoreCase("no")) {
                                        // show privacy box
                                        showTermsAndConditions();
                                    } else {
                                        PostRequest(Iconstant.register_otp_url);
                                    }
                                }

                            } else {
                                enableGpsService();
                            }

                        } else {
                            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                        }
                    }
                } else {
                    if (otp.length() == 0) {
                        Alert("", getResources().getString(R.string.otp_label_alert_otp));
                    } else if (!Sotp.equals(otp)) {
                        Alert("", getResources().getString(R.string.otp_label_alert_invalid));
                    } else {
                        cd = new ConnectionDetector(OtpPage.this);
                        isInternetPresent = cd.isConnectingToInternet();

                        if (isInternetPresent) {
                            PostRequest_mobnochange_otp(Iconstant.Edit_profile_mobileNo_url);
                        } else {
                            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                        }
                    }
                }
            }
        });
        RL_Resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cd = new ConnectionDetector(OtpPage.this);
                isInternetPresent = cd.isConnectingToInternet();

                if (isInternetPresent) {
                    PostRequest_resendOtp(Iconstant.register_otp_resend_url);
                } else {
                    Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                }

            }
        });
        RL_ChangeNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OtpPage.this, LoginPage.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            }
        });

        Iv_insertReferral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showReferralAlert();
            }
        });

    }

    @SuppressWarnings("WrongConstant")
    private void initialize() {

        session = new SessionManager(OtpPage.this);
        cd = new ConnectionDetector(OtpPage.this);
        isInternetPresent = cd.isConnectingToInternet();

        mGoogleApiClient = new GoogleApiClient.Builder(OtpPage.this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        mGoogleApiClient.connect();


        Intent intent = getIntent();
        if (intent.hasExtra("page")) {
            SuserId = intent.getStringExtra("userId");
            ScountryCode = intent.getStringExtra("CountryCode");
            Sphone = intent.getStringExtra("Phone");
            Spage = intent.getStringExtra("page");
            Sotp_Status = intent.getStringExtra("Otp_Status");
            Sotp = intent.getStringExtra("Otp");
            sPrivacyStatus = intent.getStringExtra("privacyStatus");
            sReferralStatus = intent.getStringExtra("referralStatus");
        } else {
            Sphone = intent.getStringExtra("Phone");
            ScountryCode = intent.getStringExtra("CountryCode");
            SgcmId = intent.getStringExtra("GcmID");
            Sotp_Status = intent.getStringExtra("Otp_Status");
            Sotp = intent.getStringExtra("Otp");
            SdeviceToken = intent.getStringExtra("Device_Token");
            sPrivacyStatus = intent.getStringExtra("privacyStatus");
            sReferralStatus = intent.getStringExtra("referralStatus");
        }


        System.out.println("------Spage-----------" + Spage);
        System.out.println("------Sotp-----------" + Sotp);
        System.out.println("------Sotp_Status-----------" + Sotp_Status);


        Pet_Otp = (PinEntryEditText) findViewById(R.id.pet_otp);
        Iv_light = (ImageView) findViewById(R.id.img_light);
        Iv_insertReferral = (ImageView) findViewById(R.id.img_insert_referral);
        RL_Login = (RelativeLayout) findViewById(R.id.Rl_login);
        RL_Resend = (RelativeLayout) findViewById(R.id.Rl_resend);
        RL_ChangeNumber = (RelativeLayout) findViewById(R.id.Rl_change);

        startBlinkingAnimation();

        if ("Profile".equalsIgnoreCase(Spage)) {
            RL_ChangeNumber.setVisibility(View.INVISIBLE);
        } else {
            RL_ChangeNumber.setVisibility(View.VISIBLE);
        }

        if (sReferralStatus.equalsIgnoreCase("no")) {
            // already referral code submitted so hide referral code box
            Iv_insertReferral.setVisibility(View.GONE);
        } else {
            Iv_insertReferral.setVisibility(View.VISIBLE);
        }

        otpDataMethod();

    }


    private void otpDataMethod() {

        if (Sotp_Status.equalsIgnoreCase("development")) {
            Pet_Otp.setEnabled(true);
            Pet_Otp.setClickable(true);
            Pet_Otp.setText(Sotp);
            Pet_Otp.setCursorVisible(true);
        } else {
            Pet_Otp.setEnabled(true);
            Pet_Otp.setClickable(true);
            Pet_Otp.setCursorVisible(true);
        }

        Pet_Otp.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

    }

    private void showReferralAlert() {

        //--------Adjusting Dialog width-----
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 85% of the screen
        int screenHeight = (int) (metrics.heightPixels * 0.80);//fill only 80% of the screen

        final Dialog referralDialog = new Dialog(OtpPage.this, R.style.SlideUpDialog);
        referralDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        referralDialog.setContentView(R.layout.alert_enter_referral_code);
        referralDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        referralDialog.getWindow().setLayout(screenWidth, screenHeight);

        final EditText Ed_referral = (EditText) referralDialog.findViewById(R.id.edt_referral_code);
        final Button Btn_confirm = (Button) referralDialog.findViewById(R.id.btn_confirm);
        final ImageView Iv_close = (ImageView) referralDialog.findViewById(R.id.img_close);

        Btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sReferralCode = Ed_referral.getText().toString().trim();

                if (sReferralCode.length() > 0) {
                    Ed_referral.setError(null);
                    referralDialog.dismiss();
//                    showVerifiedAlert();

                    if (cd.isConnectingToInternet()) {
                        checkReferralCode(Iconstant.check_driver_referral_code_url);
                    } else {
                        Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                    }

                } else {
                    Ed_referral.setError(getResources().getString(R.string.otp_page_referral_error));
                }

            }
        });

        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                referralDialog.dismiss();
            }
        });

        referralDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                Iv_insertReferral.setClickable(true);
                CloseKeyboardNew();
            }
        });

        Ed_referral.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    CloseKeyboard(Ed_referral);
                }
                return false;
            }
        });

        referralDialog.show();
        Iv_insertReferral.setClickable(false);

    }

    @SuppressLint("SetTextI18n")
    private void showVerifiedAlert() {

        //--------Adjusting Dialog width and height-----
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 85% of the screen
        int screenHeight = (int) (metrics.heightPixels * 0.80);//fill only 80% of the screen

        final Dialog verifiedDialog = new Dialog(OtpPage.this, R.style.SlideUpDialog);
        verifiedDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        verifiedDialog.setContentView(R.layout.alert_verified_status);
        verifiedDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        verifiedDialog.getWindow().setLayout(screenWidth, screenHeight);

        final ImageView Iv_profile = (ImageView) verifiedDialog.findViewById(R.id.img_profile);
        final TextView Tv_driverName = (TextView) verifiedDialog.findViewById(R.id.txt_user_name);
        final TextView Tv_content1 = (TextView) verifiedDialog.findViewById(R.id.txt_content1);
        final TextView Tv_earnAtLabel = (TextView) verifiedDialog.findViewById(R.id.txt_label_earn_percentage);
        final TextView Tv_earnPercentage = (TextView) verifiedDialog.findViewById(R.id.txt_earn_percentage);
        final Button Btn_proceed = (Button) verifiedDialog.findViewById(R.id.btn_proceed);

        if (sReferralDriverImage.length() > 0) {
            Picasso.with(OtpPage.this).load(sReferralDriverImage).placeholder(R.drawable.no_user_profile).error(R.drawable.no_user_profile).resize(150, 150).memoryPolicy(MemoryPolicy.NO_CACHE).into(Iv_profile);
        }
        Tv_driverName.setText(sReferralDriverName);
        Tv_content1.setText(sReferralDriverName + "\t" + getResources().getString(R.string.referral_verified_content1));
        Tv_earnAtLabel.setText(sReferralDriverName + "\t" + getResources().getString(R.string.referral_verified_earn_at));
        Tv_earnPercentage.setText(sReferralDriverPercentage + "%");

        Btn_proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifiedDialog.dismiss();
            }
        });

        verifiedDialog.show();

    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(OtpPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();

    }

    @SuppressWarnings("WrongConstant")
    private void PostRequest(String Url) {
        dialog = new Dialog(OtpPage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_verifying));

        System.out.println("--------------Otp url-------------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("phone_number", Sphone);
        jsonParams.put("country_code", ScountryCode);
        jsonParams.put("deviceToken", SdeviceToken);
        jsonParams.put("gcm_id", SgcmId);
        jsonParams.put("privacy_status", "yes");
        if (isReferralCodeVerified) {
            jsonParams.put("referal_code", sReferralCode);
        } else {
            jsonParams.put("referal_code", "");
        }

        System.out.println("------otp page jsonParams---------" + jsonParams);

        mRequest = new ServiceRequest(OtpPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------Otpreponse-------------------" + response);


                String Sstatus = "", Smessage = "";
                String Sdriver_image = "", Sdriver_name = "", Semail = "", Svehicle_number = "", Svehicle_model = "", key = "", gcmid = "";
                String SsecretKey = "";

                String gcmId = "";

                try {

                    JSONObject object = new JSONObject(response);

                    Sstatus = object.getString("status");
                    Smessage = object.getString("response");

                    if (Sstatus.equalsIgnoreCase("1")) {

                        sDriverID = object.getString("driver_id");
                        Sdriver_name = object.getString("driver_name");
                        Sdriver_image = object.getString("driver_image");
                        SsecretKey = object.getString("sec_key");
                        gcmId = object.getString("key");
                        Sis_alive_other = object.getString("is_alive_other");
                        Semail = object.getString("email");
                        Svehicle_number = object.getString("vehicle_number");
                        Svehicle_model = object.getString("vehicle_model");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (Sstatus.equalsIgnoreCase("1")) {
                    session.createLoginSession(Sdriver_image, sDriverID, Sdriver_name, Semail, Svehicle_number, Svehicle_model, gcmId, SsecretKey, gcmId);
                    session.setUserVehicle(Svehicle_model);

                    postRequest_applaunch(Iconstant.getAppInfo);

                } else {

                    dialogDismiss();

                    final PkDialog mDialog = new PkDialog(OtpPage.this);
                    mDialog.setDialogTitle(getResources().getString(R.string.action_error));
                    mDialog.setDialogMessage(Smessage);
                    mDialog.setCancelOnTouchOutside(false);
                    mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                        }
                    });
                    mDialog.show();
                }

                // close keyboard
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(Pet_Otp.getWindowToken(), 0);

            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });
    }

    private void postRequest_applaunch(String Url) {

        System.out.println("-------------Splash App Information Url----------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_type", "driver");
        jsonParams.put("id", sDriverID);
        jsonParams.put("lat", sLatitude);
        jsonParams.put("lon", sLongitude);
        mRequest = new ServiceRequest(OtpPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Splash App Information Response----------------" + response);


                String Str_status = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");
                    if (Str_status.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        if (response_object.length() > 0) {
                            JSONObject info_object = response_object.getJSONObject("info");
                            if (info_object.length() > 0) {
                                sContact_mail = info_object.getString("site_contact_mail");
                                sCustomerServiceNumber = info_object.getString("customer_service_number");
                                session.setCustomerServiceNo(sCustomerServiceNumber);
                                sSiteUrl = info_object.getString("site_url");
                                sXmppHostUrl = info_object.getString("xmpp_host_url");
                                sHostName = info_object.getString("xmpp_host_name");
                                app_identity_name = info_object.getString("app_identity_name");
                              /*  sFacebookId = info_object.getString("facebook_app_id");
                                sGooglePlusId = info_object.getString("google_plus_app_id");
                                */
                                sPhoneMasking = info_object.getString("phone_masking_status");
                                Language_code = info_object.getString("lang_code");
                                /*server_mode = info_object.getString("server_mode");
                                site_mode = info_object.getString("site_mode");
                                site_string = info_object.getString("site_mode_string");
                                site_url = info_object.getString("site_url");*/
                                sVehicleType = info_object.getString("vehicle_type");
                                sCarImage = info_object.getString("icon_car_image");
                                if (info_object.has("user_guide")) {
                                    sUserGuide = info_object.getString("user_guide");
                                }
                                sSecurePin = info_object.getString("with_pincode");
                                if(info_object.has("driver_review")) {
                                    session.setDriverReview(info_object.getString("driver_review"));
                                }
                                if (info_object.has("ride_earn")) {

                                    session.setReferelStatus(info_object.getString("ride_earn"));
                                }
                                isAppInfoAvailable = true;

                                try {
                                    JSONArray jsonarray_emergendy_no = info_object.getJSONArray("emergencyNumbers");
                                    for (int i = 0; i <= jsonarray_emergendy_no.length() - 1; i++) {
                                        String value = jsonarray_emergendy_no.getJSONObject(i).getString("title");
                                        if (value.equalsIgnoreCase("Police")) {
                                            session.setPolice(jsonarray_emergendy_no.getJSONObject(i).getString("number"));
                                        } else if (value.equalsIgnoreCase("Fire")) {
                                            session.setFire(jsonarray_emergendy_no.getJSONObject(i).getString("number"));
                                        } else if (value.equalsIgnoreCase("Ambulance")) {
                                            session.setAmbulance(jsonarray_emergendy_no.getJSONObject(i).getString("number"));
                                        }
                                    }

                                }catch (Exception e){

                                }

                            } else {
                                isAppInfoAvailable = false;
                            }

                           /* sPendingRideId= response_object.getString("pending_rideid");
                            sRatingStatus= response_object.getString("ride_status");*/

                        } else {
                            isAppInfoAvailable = false;
                        }
                    } else {
                        isAppInfoAvailable = false;
                    }
                    if (Str_status.equalsIgnoreCase("1") && isAppInfoAvailable) {
                        HashMap<String, String> language = session.getLanaguage();
                        Locale locale = null;

                        switch (Language_code) {

                            case "en":
                                locale = new Locale("en");
                                session.setlamguage("en", "en");
                                break;
                            case "es":
                                locale = new Locale("es");
                                session.setlamguage("es", "es");
                                break;

                            default:
                                locale = new Locale("en");
                                session.setlamguage("en", "en");
                                break;
                        }

                        Locale.setDefault(locale);
                        Configuration config = new Configuration();
                        config.locale = locale;
                        getApplicationContext().getResources().updateConfiguration(config, getApplicationContext().getResources().getDisplayMetrics());

                        /*session.setXmpp(sXmppHostUrl, sHostName);
                        session.setAgent(app_identity_name);
                        session.setPhoneMaskingDetail(sPhoneMasking);
                        session.setContactNumber(sCustomerServiceNumber);
                        session.setContactEmail(sContact_mail);
                        session.setVehicleType(sVehicleType);

                        dialogDismiss();

                        postRequest_SetUserLocation(Iconstant.updateDriverLocation, Sis_alive_other);*/

                        new LoadBitmap().execute(sCarImage);


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {

                dialogDismiss();

            }
        });
    }

    private void postRequest_SetUserLocation(String Url, final String isAlive) {

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        jsonParams.put("latitude", sLatitude);
        jsonParams.put("longitude", sLongitude);

        System.out.println("-------------Splash UserLocation Url----------------" + Url);
        System.out.println("-------------Splash UserLocation jsonParams----------------" + jsonParams);

        mRequest = new ServiceRequest(OtpPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Splash UserLocation Response----------------" + response);

                String Str_status = "", sCategoryID = "", sTripProgress = "", sRideId = "", sRideStage = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");
                    if (Str_status.equalsIgnoreCase("1")) {
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                ChatAvailabilityCheck chatAvailability = new ChatAvailabilityCheck(OtpPage.this, "available");
                chatAvailability.postChatRequest();


                if (isAlive.equalsIgnoreCase("Yes")) {

                    final PkDialog mDialog = new PkDialog(OtpPage.this);
                    mDialog.setDialogTitle(getResources().getString(R.string.app_name));
                    mDialog.setDialogMessage(getResources().getString(R.string.alert_multiple_login));
                    mDialog.setPositiveButton(getResources().getString(R.string.alert_ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                            session.setXmppServiceState("online");

                            session.setUserloggedIn(true);
                            Intent intent = new Intent(OtpPage.this, NavigationPage.class);
                            startActivity(intent);
                            finish();
                            overridePendingTransition(R.anim.enter, R.anim.exit);


                        }
                    });
                    mDialog.show();
                } else {


                    session.setXmppServiceState("online");

                    Intent intent = new Intent(OtpPage.this, NavigationPage.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                }

                dialogDismiss();

            }

            @Override
            public void onErrorListener() {

                dialogDismiss();

                ChatAvailabilityCheck chatAvailability = new ChatAvailabilityCheck(OtpPage.this, "available");
                chatAvailability.postChatRequest();


                Intent intent = new Intent(OtpPage.this, NavigationPage.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
    }


    private void PostRequest_resendOtp(String Url) {
        dialog = new Dialog(OtpPage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        System.out.println("--------------Otp Resend url-------------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("phone_number", Sphone);
        jsonParams.put("country_code", ScountryCode);
        jsonParams.put("lat", String.valueOf(gps.getLatitude()));
        jsonParams.put("lon", String.valueOf(gps.getLongitude()));

//        jsonParams.put("deviceToken", SdeviceToken);
//        jsonParams.put("gcm_id", SgcmId);
        System.out.println("------otp v jsonParams---------" + jsonParams);


        mRequest = new ServiceRequest(OtpPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------Otp Resend reponse-------------------" + response);


                String Sstatus = "", Smessage = "";

                String gcmId = "";

                try {

                    JSONObject object = new JSONObject(response);

                    Sstatus = object.getString("status");
                    Smessage = object.getString("response");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        Sotp_Status = object.getString("otp_status");
                        Sotp = object.getString("otp");
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (Sstatus.equalsIgnoreCase("1")) {
                    otpDataMethod();
                } else {
                    Alert(getResources().getString(R.string.action_error), Smessage);
                }

                dialogDismiss();
            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });
    }


    //--------------------------MobNo change otp----------------------------
    private void PostRequest_mobnochange_otp(String Url) {
        dialog = new Dialog(OtpPage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_verifying));

        System.out.println("--------------PostRequest_mobnochange_otp url-------------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("phone_number", Sphone);
        jsonParams.put("country_code", ScountryCode);
        jsonParams.put("otp", Sotp);
        jsonParams.put("driver_id", SuserId);
        System.out.println("------PostRequest_mobnochange_otp page jsonParams---------" + jsonParams);


        mRequest = new ServiceRequest(OtpPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------PostRequest_mobnochange_otp page reponse-------------------" + response);


                String Sstatus = "", Smessage = "",
                        Scountry_code = "", SphoneNo = "";

                try {

                    JSONObject object = new JSONObject(response);

                    Sstatus = object.getString("status");
                    Smessage = object.getString("response");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        Scountry_code = object.getString("country_code");
                        SphoneNo = object.getString("phone_number");

                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (Sstatus.equalsIgnoreCase("1")) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("countryCode", Scountry_code);
                    returnIntent.putExtra("phoneNo", SphoneNo);
                    returnIntent.putExtra("message", Smessage);
                    setResult(RESULT_OK, returnIntent);
                    onBackPressed();
                    overridePendingTransition(R.anim.slideup, R.anim.slidedown);
                    finish();
                } else {

                    final PkDialog mDialog = new PkDialog(OtpPage.this);
                    mDialog.setDialogTitle(getResources().getString(R.string.action_error));
                    mDialog.setDialogMessage(Smessage);
                    mDialog.setCancelOnTouchOutside(false);
                    mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                        }
                    });
                    mDialog.show();
                }

                // close keyboard
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(Pet_Otp.getWindowToken(), 0);

                dialogDismiss();
            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });
    }

    @SuppressWarnings("WrongConstant")
    private void checkReferralCode(String Url) {
        dialog = new Dialog(OtpPage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_verifying));

        System.out.println("--------------checkReferralCode url-------------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("phone_number", Sphone);
        jsonParams.put("country_code", ScountryCode);
        jsonParams.put("code", sReferralCode);

        System.out.println("------checkReferralCode jsonParams---------" + jsonParams);

        mRequest = new ServiceRequest(OtpPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------checkReferralCode reponse-------------------" + response);


                String Sstatus = "", Sresponse = "";

                try {

                    JSONObject object = new JSONObject(response);

                    Sstatus = object.getString("status");
                    Sresponse = object.getString("response");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        sReferralDriverName = object.getString("driver_name");
                        sReferralDriverPercentage = object.getString("ref_per");
                        sReferralDriverImage = object.getString("driver_image");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                dialogDismiss();

                if (Sstatus.equalsIgnoreCase("1")) {
                    isReferralCodeVerified = true;
//                    Iv_insertReferral.setVisibility(View.GONE);
                    showVerifiedAlert();
                } else {
                    sReferralCode = "";
                    Alert(getResources().getString(R.string.action_error), Sresponse);
                }

                // close keyboard
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(Pet_Otp.getWindowToken(), 0);

            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });
    }


    private void dialogDismiss() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    @SuppressWarnings("WrongConstant")
    private void CloseKeyboard(EditText edittext) {
        try {
            InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            in.hideSoftInputFromWindow(edittext.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("WrongConstant")
    private void CloseKeyboardNew() {
        try {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow((null == getCurrentFocus()) ? null : getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }


    //Enabling Gps Service
    private void enableGpsService() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        //...
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(OtpPage.this, REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_LOCATION:
                switch (resultCode) {
                    case Activity.RESULT_OK: {

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                cd = new ConnectionDetector(OtpPage.this);
                                isInternetPresent = cd.isConnectingToInternet();
                                gps = new GPSTracker(OtpPage.this);

                                if (isInternetPresent) {

                                    if (gps.isgpsenabled() && gps.canGetLocation()) {
                                        sLatitude = String.valueOf(gps.getLatitude());
                                        sLongitude = String.valueOf(gps.getLongitude());
                                        showTermsAndConditions();
                                    } else {
                                        enableGpsService();
                                    }

                                } else {
                                    Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                                }

                            }
                        }, 2000);

                        break;
                    }
                    case Activity.RESULT_CANCELED: {
                        break;
                    }
                    default: {
                        break;
                    }
                }
                break;
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            //Do nothing
            return true;
        }
        return false;
    }

    private void startBlinkingAnimation() {
        Animation startAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blinking_animation);
        Iv_light.startAnimation(startAnimation);
    }

    private void showTermsAndConditions() {

        final Dialog termsDialog = new Dialog(OtpPage.this, R.style.SlideUpDialog);
        termsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        termsDialog.setContentView(R.layout.alert_terms_and_conditions);
        termsDialog.setCanceledOnTouchOutside(false);
        termsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        final CheckBox Cb_termsAndConditions = (CheckBox) termsDialog.findViewById(R.id.cb_terms_and_conditions);
        final CheckBox Cb_privacyPolicy = (CheckBox) termsDialog.findViewById(R.id.cb_privacy_policy);
        final CheckBox Cb_eula = (CheckBox) termsDialog.findViewById(R.id.cb_eula);
        final TextView Tv_termsAndConditions = (TextView) termsDialog.findViewById(R.id.txt_label_terms_and_conditions);
        final TextView Tv_privacyPolicy = (TextView) termsDialog.findViewById(R.id.txt_label_privacy_policy);
        final TextView Tv_eula = (TextView) termsDialog.findViewById(R.id.txt_label_eula);
        final RelativeLayout RL_decline = (RelativeLayout) termsDialog.findViewById(R.id.Rl_decline);
        final Button Btn_agree = (Button) termsDialog.findViewById(R.id.Btn_agree);

        Tv_termsAndConditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Cb_termsAndConditions.isChecked()) {
                    Cb_termsAndConditions.setChecked(false);
                } else {
                    Cb_termsAndConditions.setChecked(true);
                }
            }
        });

        Tv_privacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Cb_privacyPolicy.isChecked()) {
                    Cb_privacyPolicy.setChecked(false);
                } else {
                    Cb_privacyPolicy.setChecked(true);
                }
            }
        });

        Tv_eula.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Cb_eula.isChecked()) {
                    Cb_eula.setChecked(false);
                } else {
                    Cb_eula.setChecked(true);
                }
            }
        });

        Btn_agree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Cb_termsAndConditions.isChecked() && Cb_privacyPolicy.isChecked() && Cb_eula.isChecked()) {
                    PostRequest(Iconstant.register_otp_url);
                } else {
                    Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.label_terms_and_conditions_error));
                }

            }
        });

        RL_decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                termsDialog.dismiss();
            }
        });


        termsDialog.show();


    }


    public class LoadBitmap extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            String car64 = "";

            try {
                URL url = new URL(params[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);

                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                myBitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream.toByteArray();

                car64 = Base64.encodeToString(byteArray, Base64.DEFAULT);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return car64;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            completeInterface.onComplete(result);
        }
    }


    @Override
    public void onComplete(String image) {

        if (image != null && image.length() > 0) {
            session.setCarMarker(image);
        } else {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.carmove);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] imageBytes = baos.toByteArray();
            String imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);
            session.setCarMarker(imageString);
        }

        session.setXmpp(sXmppHostUrl, sHostName);
        session.setAgent(app_identity_name);
        session.setPhoneMaskingDetail(sPhoneMasking);
        session.setContactNumber(sCustomerServiceNumber);
        session.setContactEmail(sContact_mail);
        session.setVehicleType(sVehicleType);
        session.setUserGuide(sUserGuide);
        session.setSecurePin(sSecurePin);

        dialogDismiss();

        postRequest_SetUserLocation(Iconstant.updateDriverLocation, Sis_alive_other);
    }


    @SuppressLint("WrongConstant")
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        boolean b = false;
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                System.out.println("1 already running");
                b = true;
                break;
            } else {
                System.out.println("2 not running");
                b = false;
            }
        }
        System.out.println("3 not running");
        return b;
    }

    public void hideKeyboard(View view) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void AlertError(String title, String message) {

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        Snackbar bar = Snackbar.with(OtpPage.this);
        bar.addView(view);
        bar.colorResource(R.color.dark_red);
        bar.position(Snackbar.SnackbarPosition.TOP);
        bar.type(SnackbarType.MULTI_LINE);
        bar.duration(Snackbar.SnackbarDuration.LENGTH_SHORT);
        bar.animation(true);

        SnackbarManager.show(bar, OtpPage.this);


    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }
}



