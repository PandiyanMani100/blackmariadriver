package com.blackmaria.partner.app;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.Signature;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Telephony;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.KeyboardVisibilityListener;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.google.android.gms.plus.PlusShare;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;


/**
 * Created by Ganesh on 10/13/2015.
 */
public class InviteAndEarn extends Activity implements View.OnClickListener {
    private ImageView back;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private SessionManager session;
    private ImageView Iv_facebook, Iv_twitter, Iv_email, Iv_whatsApp, Iv_gplus, Iv_sms;
    private TextView Tv_referralCode;
    /// private Button Btn_learn_more;
    private ServiceRequest mRequest;
    private String driverID = "";
    private boolean isdataPresent = false;
    private String Sstatus = "", friend_earn_amount = "", Earn_Condition = "", you_earn_amount = "", friends_rides = "", ScurrencyCode = "", referral_code = "", sShareLink = "",
            sShareImage = "", sShareMessage = "", sShareSubject = "";
    String sCurrencySymbol = "";
    Uri imageUri = null;
    final int PERMISSION_REQUEST_CODE = 111;
    private boolean check_status = false;
    private Dialog dialog = null;


    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inviteandearn_new);


        initialize();

        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.blackmaria.partner", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("MY KEY HASH:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });


    }


    public void setKeyboardVisibilityListener(Activity activity, final KeyboardVisibilityListener keyboardVisibilityListener) {
        final View contentView = activity.findViewById(android.R.id.content);
        contentView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            private int mPreviousHeight;

            @Override
            public void onGlobalLayout() {
                int newHeight = contentView.getHeight();
                if (mPreviousHeight != 0) {
                    if (mPreviousHeight > newHeight) {
                        // Height decreased: keyboard was shown
                        keyboardVisibilityListener.onKeyboardVisibilityChanged(true);
                        InputMethodManager imm = (InputMethodManager)
                                getSystemService(Context.INPUT_METHOD_SERVICE);
                        if(imm != null){
                            imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
                        }
                    } else if (mPreviousHeight < newHeight) {
                        // Height increased: keyboard was hidden
                        keyboardVisibilityListener.onKeyboardVisibilityChanged(false);

                    } else {
                        // No change
                    }
                }
                mPreviousHeight = newHeight;
            }
        });
    }

    private void initialize() {
        session = new SessionManager(InviteAndEarn.this);
        cd = new ConnectionDetector(InviteAndEarn.this);
        isInternetPresent = cd.isConnectingToInternet();

        back = (ImageView) findViewById(R.id.btn_home);
        Tv_referralCode = (TextView) findViewById(R.id.txt_referral_code);
        Iv_facebook = (ImageView) findViewById(R.id.img_facebook1);
        Iv_twitter = (ImageView) findViewById(R.id.img_twitter1);
        Iv_email = (ImageView) findViewById(R.id.img_email1);
        Iv_whatsApp = (ImageView) findViewById(R.id.img_whatsapp1);
        Iv_gplus = (ImageView) findViewById(R.id.img_gplus1);
        Iv_sms = (ImageView) findViewById(R.id.img_sms1);
        // Btn_learn_more = (Button) findViewById(R.id.btn_learn_more);
        Iv_facebook.setOnClickListener(this);
        Iv_twitter.setOnClickListener(this);
        Iv_email.setOnClickListener(this);
        Iv_whatsApp.setOnClickListener(this);
        Iv_gplus.setOnClickListener(this);
        Iv_sms.setOnClickListener(this);
        //Btn_learn_more.setOnClickListener(this);   // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        driverID = user.get(SessionManager.KEY_DRIVERID);

        if (!session.getReferelStatus().equalsIgnoreCase("0")) {
            showReferralDetails();
        }

        if (isInternetPresent) {
            displayInvite_Request(Iconstant.driver_and_earn_url);
        } else {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
        }

    }

    @Override
    public void onClick(View v) {
        isdataPresent = cd.isConnectingToInternet();


        if (isdataPresent) {
            if (v == Iv_whatsApp) {
                whatsApp_sendMsg(sShareMessage);
            } else if (v == Iv_sms) {
                imageUri = Uri.parse(sShareImage);
                shareskype(sShareMessage, imageUri);
                // sms_sendMsg(sShareMessage);
            } else if (v == Iv_email) {
                imageUri = Uri.parse(sShareImage);

                Uri imageUri = null;
                try {
                    imageUri = Uri.parse(MediaStore.Images.Media.insertImage(this.getContentResolver(),
                            BitmapFactory.decodeResource(getResources(), R.drawable.app_icon), null, null));
                } catch (NullPointerException e) {
                }

                if (sShareLink.length() > 0) {
                    System.out.println("link");
                    shareInstagramelink(sShareMessage, imageUri);
                } else {
                    System.out.println("text");
                    shareInstagrame(sShareMessage);
                }
            } else if (v == Iv_twitter) {
                String twitter_text = sShareMessage;

                Uri imageUri = null;
                try {
                    imageUri = Uri.parse(MediaStore.Images.Media.insertImage(this.getContentResolver(),
                            BitmapFactory.decodeResource(getResources(), R.drawable.app_icon), null, null));
                } catch (NullPointerException e) {
                }
                shareTwitter(twitter_text, imageUri);
            } else if (v == Iv_facebook) {
                String facebook_text = sShareLink;
                Uri imageUri = null;
                try {
                    imageUri = Uri.parse(MediaStore.Images.Media.insertImage(this.getContentResolver(),
                            BitmapFactory.decodeResource(getResources(), R.drawable.app_icon), null, null));
                } catch (NullPointerException e) {
                }

                if (sShareLink.length() > 0) {
                    shareFacebookLink(sShareLink);
                } else {
                    shareFacebook(facebook_text, imageUri);
                }


            } else if (v == Iv_gplus) {
                googlePlusShare(sShareMessage);
            }
        } else {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.invite_earn_label_problem_server));
        }

    }

    private void showReferralDetails() {
        check_status = false;
        final Dialog dialog = new Dialog(InviteAndEarn.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.invite_and_earn_popip_new);
        final Button ok_button = (Button) dialog.findViewById(R.id.btn_ok);
        final CheckBox chck_agree = (CheckBox) dialog.findViewById(R.id.agree_checkBox);
        final Button cancelbtn = (Button) dialog.findViewById(R.id.cancel);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        cancelbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        chck_agree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    check_status = true;
                } else {
                    check_status = false;
                }
            }
        });


        ok_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check_status) {
                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    jsonParams.put("driver_id", driverID);
                    jsonParams.put("confirmed", "yes");
                    ActivateReferandEarn(Iconstant.Driver_Activiate_Referal, jsonParams);
                    dialog.dismiss();
                } else {
                    Alert("Sorry", "Kindly Agree Terms of Service");
                }
            }
        });

        dialog.show();


    }


    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(InviteAndEarn.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setDialogUpper(false);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();

    }

    private void googlePlusShare(String text) {

        if (isGooglePlusInstalled()) {
            Intent shareIntent = new PlusShare.Builder(InviteAndEarn.this)
                    .setType("text/plain")
                    .setText(text)
                    .setContentUrl(Uri.parse("https://developers.google.com/+/"))
                    .getIntent();

            startActivityForResult(shareIntent, 0);

        } else {
            Alert1(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.invite_earn_label_gplus_not_installed), "https://play.google.com/store/apps/details?id=com.google.android.apps.plus&hl=en");
        }
    }

    public boolean isGooglePlusInstalled() {
        try {
            getPackageManager().getApplicationInfo("com.google.android.apps.plus", 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    //--------Sending message on WhatsApp Method------
    @SuppressLint("WrongConstant")
    private void whatsApp_sendMsg(String text) {
        PackageManager pm = InviteAndEarn.this.getPackageManager();
        try {
            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");
            PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);  //Check if package exists or not. If not then codein catch block will be called
            waIntent.setPackage("com.whatsapp");
            waIntent.putExtra(Intent.EXTRA_TEXT, text);
            startActivity(Intent.createChooser(waIntent, "Share with"));
        } catch (PackageManager.NameNotFoundException e) {
            Alert1(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.invite_earn_label_whatsApp_not_installed), "https://play.google.com/store/apps/details?id=com.whatsapp&hl=en");
        }
    }

    //--------Sending message on Facebook Messenger Method------
    @SuppressLint("WrongConstant")
    private void messenger_sendMsg(String text) {
        PackageManager pm = InviteAndEarn.this.getPackageManager();
        try {
            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");
            PackageInfo info = pm.getPackageInfo("com.facebook.orca", PackageManager.GET_META_DATA);  //Check if package exists or not. If not then codein catch block will be called
            waIntent.setPackage("com.facebook.orca");
            waIntent.putExtra(Intent.EXTRA_TEXT, text);
            startActivity(Intent.createChooser(waIntent, "Share with"));
        } catch (PackageManager.NameNotFoundException e) {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.invite_earn_label_messenger_not_installed));
        }
    }

    //--------Sending message on SMS Method------
    private void sms_sendMsg(String text) {
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+

                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.putExtra("sms_body", text);
                intent.setData(Uri.parse("smsto:" + ""));
                startActivity(intent);

        } else {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.putExtra("sms_body", text);
            intent.setData(Uri.parse("smsto:" + ""));
            startActivity(intent);
        }
    }


    //----------Sending message on Email Method--------
    protected void sendEmail(String text) {
        String[] TO = {""};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, sShareSubject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, text);
        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.invite_earn_label_email_not_installed));
        }
    }

    protected void shareInstagramelink(String text, Uri image) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT, text);
        intent.setType("image/jpeg");
        intent.setPackage("com.instagram.android");
        intent.putExtra(Intent.EXTRA_STREAM, image);
        try {
            startActivity(Intent.createChooser(intent, "Share to"));
        } catch (android.content.ActivityNotFoundException ex) {
            Alert1(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.invite_earn_label_instagram_not_installed), "https://play.google.com/store/search?q=instagram&c=apps&hl=en");
        }
    }

    protected void shareInstagrame(String text) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT, text);
        intent.setType("text/plain");
        intent.setPackage("com.instagram.android");
        try {
            startActivity(Intent.createChooser(intent, "Share to"));
        } catch (android.content.ActivityNotFoundException ex) {
            Alert1(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.invite_earn_label_instagram_not_installed), "https://play.google.com/store/search?q=instagram&c=apps&hl=en");
        }
    }


    //    //----------Share Image and Text on Twitter Method--------
//    protected void shareInstagrame(String text, Uri image) {
//        Intent instagramIntent = new Intent(Intent.ACTION_SEND);
//        instagramIntent.setType("image/*");
////        Intent instagramIntent = new Intent(Intent.ACTION_SEND);
//        instagramIntent.putExtra(Intent.EXTRA_TEXT, text);
//        instagramIntent.putExtra(Intent.EXTRA_STREAM, image);
////        instagramIntent.setType("image/*");
//        instagramIntent.setPackage("com.instagram.android");
//
//        PackageManager packManager = getPackageManager();
//        List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(instagramIntent, PackageManager.MATCH_DEFAULT_ONLY);
//
//        boolean resolved = false;
//        try {
//            ApplicationInfo info = getPackageManager().getApplicationInfo("com.instagram.android", 0);
//            resolved = true;
//        } catch (PackageManager.NameNotFoundException e) {
//            resolved = false;
//        }
//
//        if (resolved) {
//            startActivity(Intent.createChooser(instagramIntent, "Share to"));
//        } else {
//            Alert1(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.invite_earn_label_instagram_not_installed), "https://play.google.com/store/search?q=instagram&c=apps&hl=en");
//        }
//
//    }
    //----------Share Image and Text on skype Method--------
    protected void shareskype(String text, Uri image) {
        // Create the Intent from our Skype URI.
        Intent skypeIntent = new Intent(Intent.ACTION_SEND);
        skypeIntent.putExtra(Intent.EXTRA_TEXT, text);
        skypeIntent.setType("text/plain");
        skypeIntent.setPackage("com.skype.raider");
        PackageManager packManager = getPackageManager();
        List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(skypeIntent, PackageManager.MATCH_DEFAULT_ONLY);

        boolean resolved = false;
        try {
            ApplicationInfo info = getPackageManager().getApplicationInfo("com.skype.raider", 0);
            resolved = true;
        } catch (PackageManager.NameNotFoundException e) {
            resolved = false;
        }

        if (resolved) {
            startActivity(Intent.createChooser(skypeIntent, "Share to"));
        } else {
            Alert1(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.invite_earn_label_skype_not_installed), "https://play.google.com/store/apps/details?id=com.skype.raider&hl=en");
        }

    }


//    //----------Share Image and Text on skype Method--------
//    protected void shareskype(String text, Uri image) {
//
//        // Create the Intent from our Skype URI.
//        Intent skypeIntent = new Intent(Intent.ACTION_SEND);
//
//        skypeIntent.putExtra(Intent.EXTRA_TEXT, text);
//        skypeIntent.setType("text/plain");
////        skypeIntent.putExtra(Intent.EXTRA_STREAM, image);
////        skypeIntent.setType("image/*");
////        skypeIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//        skypeIntent.setPackage("com.skype.raider");
//
//
//        PackageManager packManager = getPackageManager();
//        List<ResolveInfo> resolvedInfoList = packManager.queryIntentActivities(skypeIntent, PackageManager.MATCH_DEFAULT_ONLY);
//
//        boolean resolved = false;
//        try {
//            ApplicationInfo info = getPackageManager().getApplicationInfo("com.skype.raider", 0);
//            resolved = true;
//        } catch (PackageManager.NameNotFoundException e) {
//            resolved = false;
//        }
//
//        if (resolved) {
//            startActivity(Intent.createChooser(skypeIntent, "Share to"));
//        } else {
//            Alert1(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.invite_earn_label_skype_not_installed), "https://play.google.com/store/apps/details?id=com.skype.raider&hl=en");
//        }
//
//    }

    //----------Share Image and Text on Twitter Method--------
    protected void shareTwitter(String text, Uri image) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, text);
//        intent.putExtra(Intent.EXTRA_STREAM, image);
//        intent.setType("image/jpeg");
        intent.setPackage("com.twitter.android");

        try {
            startActivity(intent);
        } catch (android.content.ActivityNotFoundException ex) {
            Alert1(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.invite_earn_label_twitter_not_installed), "https://play.google.com/store/apps/details?id=com.twitter.android&hl=en");
        }
    }

    //----------Share Link on Method--------
    private void shareFacebookLink(String link) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, link);
        Intent pacakage1 = getPackageManager().getLaunchIntentForPackage("com.facebook.katana");
        if (pacakage1 != null) {
            intent.setPackage("com.facebook.katana");
        }  else {
            intent.setPackage("");
        }
        try {
            startActivity(intent);
        } catch (android.content.ActivityNotFoundException ex) {
            Alert1(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.invite_earn_label_facebook_not_installed), "https://play.google.com/store/apps/details?id=com.facebook.katana&hl=en");
        }
    }

    //----------Share Image and Text on Facebook Method--------
    protected void shareFacebook(String text, Uri image) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, text);
        Intent pacakage1 = getPackageManager().getLaunchIntentForPackage("com.facebook.katana");
        Intent pacakage2 = getPackageManager().getLaunchIntentForPackage("com.example.facebook");
        Intent pacakage3 = getPackageManager().getLaunchIntentForPackage("com.facebook.android");
        if (pacakage1 != null) {
            intent.setPackage("com.facebook.katana");
        } else if (pacakage2 != null) {
            intent.setPackage("com.facebook.facebook");
        } else if (pacakage3 != null) {
            intent.setPackage("com.facebook.android");
        } else {
            intent.setPackage("com.facebook.orca");
        }
        try {
            startActivity(intent);
        } catch (android.content.ActivityNotFoundException ex) {
            Alert1(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.invite_earn_label_facebook_not_installed), "https://play.google.com/store/apps/details?id=com.facebook.katana&hl=en");
        }
    }


    private boolean checkSmsPermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

//                    String text = getResources().getString(R.string.invite_earn_label_share_messgae1) + " " + sCurrencySymbol + "" + friend_earn_amount + " " + getResources().getString(R.string.invite_earn_label_share_messgae2) + " " + referral_code + " " + getResources().getString(R.string.invite_earn_label_share_messgae3);

                    String defaultSmsPackageName = null; //Need to change the build to API 19
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(this);
                    }

                    Intent sendIntent = new Intent(Intent.ACTION_SEND);
                    sendIntent.setType("text/plain");
                    sendIntent.putExtra(Intent.EXTRA_TEXT, sShareMessage);
                    if (defaultSmsPackageName != null) {
                        sendIntent.setPackage(defaultSmsPackageName);
                    }
                    startActivity(sendIntent);
                } else {
                }
                break;
        }
    }


    //-----------------------Display Invite Amount Post Request-----------------
    private void displayInvite_Request(String Url) {
        final Dialog dialog = new Dialog(InviteAndEarn.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(InviteAndEarn.this.getResources().getString(R.string.action_pleasewait));

        System.out.println("-------------displayInvite_Request Url----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", driverID);

        mRequest = new ServiceRequest(InviteAndEarn.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------displayInvite_Request Response----------------" + response);

                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (object.length() > 0) {
                        JSONObject response_Object = object.getJSONObject("response");
                        if (response_Object.length() > 0) {
                            JSONObject detail_object = response_Object.getJSONObject("details");
                            if (detail_object.length() > 0) {
                                isdataPresent = true;
                                friend_earn_amount = detail_object.getString("friends_earn_amount");
                                you_earn_amount = detail_object.getString("your_earn_amount");
                                friends_rides = detail_object.getString("your_earn");
                                referral_code = detail_object.getString("referral_code");
                                ScurrencyCode = detail_object.getString("currency");
                                sShareLink = detail_object.getString("url");
                                sShareMessage = detail_object.getString("message");
                                sShareSubject = detail_object.getString("subject");
                                sShareImage = detail_object.getString("share_image");

                                isdataPresent = true;
                            } else {
                                isdataPresent = false;
                            }
                        } else {
                            isdataPresent = false;
                        }
                    } else {
                        isdataPresent = false;
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (isdataPresent) {
//                    sCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(ScurrencyCode);
                    Tv_referralCode.setText(referral_code);

                    /*Tv_friends_earn.setText(getResources().getString(R.string.invite_earn_label_friends_earn) + " " + sCurrencySymbol + "" + friend_earn_amount);
                    Tv_you_earn.setText(getResources().getString(R.string.invite_earn_label_friends_ride) + "," + getResources().getString(R.string.invite_earn_label_friend_ride) + " " + sCurrencySymbol + "" + you_earn_amount);
                    Tv_referral_code.setText(referral_code);*/
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    //--------------Alert Method-----------
    private void Alert1(String title, String alert, final String url) {

        final PkDialog mDialog = new PkDialog(InviteAndEarn.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
        mDialog.show();

    }

    //-----------------Move Back on pressed phone back button------------------
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            onBackPressed();
            finish();
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            return true;
        }
        return false;
    }

    private void ActivateReferandEarn(String Url, HashMap<String, String> jsonParams) {
        dialog = new Dialog(InviteAndEarn.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        System.out.println("-----------ActivateReferandEarn jsonParams--------------" + jsonParams);

        mRequest = new ServiceRequest(InviteAndEarn.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------ActivateReferandEarn reponse-------------------" + response);

                String Sstatus = "", Smessage = "";
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    Smessage = object.getString("response");
                    dialogDismiss();
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    dialogDismiss();
                }
                dialogDismiss();
                if (Sstatus.equalsIgnoreCase("1")) {
                    session.setReferelStatus("0");
                    Alert("", Smessage);
                } else {
                    Alert("", Smessage);
                }
            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });
    }

    private void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }
}