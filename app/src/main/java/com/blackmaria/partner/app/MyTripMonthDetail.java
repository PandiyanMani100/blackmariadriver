package com.blackmaria.partner.app;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.Pojo.MonthDetail;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by GANESH on 4/27/2017.
 */

public class MyTripMonthDetail extends ActivityHockeyApp implements View.OnClickListener {


    private ImageView Iv_back;
    private TextView Tv_trips_count, Tv_date;
    private TextView Tv_weekName1, Tv_weekCount1, Tv_tripsCount1, Tv_tripDistance1;
    private TextView Tv_weekName2, Tv_weekCount2, Tv_tripsCount2, Tv_tripDistance2;
    private TextView Tv_weekName3, Tv_weekCount3, Tv_tripsCount3, Tv_tripDistance3;
    private TextView Tv_weekName4, Tv_weekCount4, Tv_tripsCount4, Tv_tripDistance4;
    private TextView Tv_weekName5, Tv_weekCount5, Tv_tripsCount5, Tv_tripDistance5;
    private RelativeLayout RL_week1, RL_week2, RL_week3, RL_week4, RL_week5;
    private ConnectionDetector cd;
    private SessionManager sessionManager;
    private String sDriveID = "", sFilterMonth = "", sFilterYear = "", sTotalRides = "", sDisplayDate = "";
    private Dialog dialog;
    private ServiceRequest mRequest;
    private ArrayList<MonthDetail> monthList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trip_month_detail);

        initialize();

    }

    private void getIntentData() {

        Intent intent = getIntent();
        if (intent != null) {
            sDisplayDate = intent.getStringExtra("DisplayDate");
            sFilterMonth = intent.getStringExtra("Month");
            sFilterYear = intent.getStringExtra("Year");
        }

    }

    @Override
    public void onClick(View view) {

        if (view == Iv_back) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (view == RL_week1) {
            Intent intent = new Intent(MyTripMonthDetail.this, MyTripWeekDetail.class);
            intent.putExtra("fromDate", monthList.get(0).getStartdate_f());
            intent.putExtra("toDate", monthList.get(0).getEndate_f());
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (view == RL_week2) {
            Intent intent = new Intent(MyTripMonthDetail.this, MyTripWeekDetail.class);
            intent.putExtra("fromDate", monthList.get(1).getStartdate_f());
            intent.putExtra("toDate", monthList.get(1).getEndate_f());
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (view == RL_week3) {
            Intent intent = new Intent(MyTripMonthDetail.this, MyTripWeekDetail.class);
            intent.putExtra("fromDate", monthList.get(2).getStartdate_f());
            intent.putExtra("toDate", monthList.get(2).getEndate_f());
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (view == RL_week4) {
            Intent intent = new Intent(MyTripMonthDetail.this, MyTripWeekDetail.class);
            intent.putExtra("fromDate", monthList.get(3).getStartdate_f());
            intent.putExtra("toDate", monthList.get(3).getEndate_f());
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (view == RL_week5) {
            Intent intent = new Intent(MyTripMonthDetail.this, MyTripWeekDetail.class);
            intent.putExtra("fromDate", monthList.get(4).getStartdate_f());
            intent.putExtra("toDate", monthList.get(4).getEndate_f());
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }

    }

    private void initialize() {

        sessionManager = new SessionManager(MyTripMonthDetail.this);
        cd = new ConnectionDetector(MyTripMonthDetail.this);
        monthList = new ArrayList<MonthDetail>();

        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriveID = user.get(SessionManager.KEY_DRIVERID);

        Tv_weekName1 = (TextView) findViewById(R.id.txt_label_week_1);
        Tv_weekName2 = (TextView) findViewById(R.id.txt_label_week_2);
        Tv_weekName3 = (TextView) findViewById(R.id.txt_label_week_3);
        Tv_weekName4 = (TextView) findViewById(R.id.txt_label_week_4);
        Tv_weekName5 = (TextView) findViewById(R.id.txt_label_week_5);

        Tv_weekCount1 = (TextView) findViewById(R.id.txt_label_count_week_1);
        Tv_weekCount2 = (TextView) findViewById(R.id.txt_label_count_week_2);
        Tv_weekCount3 = (TextView) findViewById(R.id.txt_label_count_week_3);
        Tv_weekCount4 = (TextView) findViewById(R.id.txt_label_count_week_4);
        Tv_weekCount5 = (TextView) findViewById(R.id.txt_label_count_week_5);

        Tv_tripsCount1 = (TextView) findViewById(R.id.txt_trips_count_1);
        Tv_tripsCount2 = (TextView) findViewById(R.id.txt_trips_count_2);
        Tv_tripsCount3 = (TextView) findViewById(R.id.txt_trips_count_3);
        Tv_tripsCount4 = (TextView) findViewById(R.id.txt_trips_count_4);
        Tv_tripsCount5 = (TextView) findViewById(R.id.txt_trips_count_5);

        Tv_tripDistance1 = (TextView) findViewById(R.id.txt_trips_distance_1);
        Tv_tripDistance2 = (TextView) findViewById(R.id.txt_trips_distance_2);
        Tv_tripDistance3 = (TextView) findViewById(R.id.txt_trips_distance_3);
        Tv_tripDistance4 = (TextView) findViewById(R.id.txt_trips_distance_4);
        Tv_tripDistance5 = (TextView) findViewById(R.id.txt_trips_distance_5);

        RL_week1 = (RelativeLayout) findViewById(R.id.Rl_week1);
        RL_week2 = (RelativeLayout) findViewById(R.id.Rl_week2);
        RL_week3 = (RelativeLayout) findViewById(R.id.Rl_week3);
        RL_week4 = (RelativeLayout) findViewById(R.id.Rl_week4);
        RL_week5 = (RelativeLayout) findViewById(R.id.Rl_week5);

        Iv_back = (ImageView) findViewById(R.id.img_back);
        Tv_trips_count = (TextView) findViewById(R.id.txt_trips_count);
        Tv_date = (TextView) findViewById(R.id.txt_date);

        Iv_back.setOnClickListener(this);
        RL_week1.setOnClickListener(this);
        RL_week2.setOnClickListener(this);
        RL_week3.setOnClickListener(this);
        RL_week4.setOnClickListener(this);
        RL_week5.setOnClickListener(this);

        getIntentData();

        if (cd.isConnectingToInternet()) {
            MyTripMonthDetailRequest(Iconstant.month_trips_details_url);
        } else {
            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }

    }


    @SuppressWarnings("WrongConstant")
    private void MyTripMonthDetailRequest(String Url) {

        dialog = new Dialog(MyTripMonthDetail.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriveID);
        jsonParams.put("month", sFilterMonth);
        jsonParams.put("year", sFilterYear);

        System.out.println("--------------MonthTripsDetail Url-------------------" + Url);
        System.out.println("--------------MonthTripsDetail jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(MyTripMonthDetail.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------MonthTripsDetail Response-------------------" + response);
                String status = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {
                            JSONObject jsonObject = object.getJSONObject("response");
                            if (jsonObject.length() > 0) {

                                Object monthObj = jsonObject.get("monthreport");
                                if (monthObj instanceof JSONArray) {

                                    JSONArray jarMonth = jsonObject.getJSONArray("monthreport");

                                    if (jarMonth.length() > 0) {

                                        monthList = new ArrayList<MonthDetail>();
                                        for (int i = 0; i < jarMonth.length(); i++) {

                                            JSONObject jobjMonth = jarMonth.getJSONObject(i);

                                            String week = jobjMonth.getString("week");
                                            String WeekCount = jobjMonth.getString("WeekCount");
                                            String ridecount = jobjMonth.getString("ridecount");
                                            String distance = jobjMonth.getString("distance");
                                            String income = jobjMonth.getString("income");
                                            String currency = jobjMonth.getString("currency");
                                            String startdate = jobjMonth.getString("startdate");
                                            String endate = jobjMonth.getString("endate");
                                            String startdate_f = jobjMonth.getString("startdate_f");
                                            String endate_f = jobjMonth.getString("endate_f");

                                            monthList.add(new MonthDetail(week, WeekCount, ridecount, distance, income, currency, startdate, endate, startdate_f, endate_f));

                                        }

                                    } else {

                                    }


                                }

                                sTotalRides = jsonObject.getString("total_rides");

                            }
                        } else {
                            String sResponse = object.getString("response");
                            Alert(getResources().getString(R.string.action_error), sResponse);
                        }

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (status.equalsIgnoreCase("1")) {

                    Tv_date.setText(sDisplayDate);

                    if (sTotalRides != null && sTotalRides.length() > 0) {
                        int rides = 0;
                        try {
                            rides = Integer.parseInt(sTotalRides);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (rides > 1) {
                            Tv_trips_count.setText(sTotalRides + " " + getResources().getString(R.string.mytrips_label_trips));
                        } else {
                            Tv_trips_count.setText(sTotalRides + " " + getResources().getString(R.string.mytrips_label_trip));
                        }
                    }

                    /*if(isDataAvailable) {
                        adapter = new WeekTripDetailAdapter(MyTripTodayDetail.this, weekDetailsList);
                        listWeek.setAdapter(adapter);
                    }*/

                    if (monthList.size() > 0) {

                        for (int i = 0; i < monthList.size(); i++) {

                            MonthDetail mMonth = monthList.get(i);

                            switch (i) {

                                case 0:
                                    Tv_weekName1.setText(mMonth.getWeek());
                                    Tv_weekCount1.setText(mMonth.getWeekCount());

                                    String rideCount1 = mMonth.getRidecount();
                                    String distance1 = mMonth.getDistance();

                                    if (rideCount1 != null && rideCount1.length() > 0) {
                                        int rides = 0;
                                        if (!rideCount1.equalsIgnoreCase("-")) {
                                            try {
                                                rides = Integer.parseInt(rideCount1);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                        if (rides > 1) {
                                            Tv_tripsCount1.setText(rides + " " + getResources().getString(R.string.mytrips_label_trips));
                                        } else {
                                            Tv_tripsCount1.setText(rides + " " + getResources().getString(R.string.mytrips_label_trip));
                                        }
                                    }

                                    if (distance1 != null && distance1.length() > 0 && distance1.equalsIgnoreCase("-")) {
                                        distance1 = "0 KM";
                                    }

                                    Tv_tripDistance1.setText(distance1);
                                    RL_week1.setVisibility(View.VISIBLE);
                                    break;
                                case 1:
                                    Tv_weekName2.setText(mMonth.getWeek());
                                    Tv_weekCount2.setText(mMonth.getWeekCount());
                                    String rideCount2 = mMonth.getRidecount();
                                    String distance2 = mMonth.getDistance();

                                    if (rideCount2 != null && rideCount2.length() > 0) {
                                        int rides = 0;
                                        if (!rideCount2.equalsIgnoreCase("-")) {
                                            try {
                                                rides = Integer.parseInt(rideCount2);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                        if (rides > 1) {
                                            Tv_tripsCount2.setText(rides + " " + getResources().getString(R.string.mytrips_label_trips));
                                        } else {
                                            Tv_tripsCount2.setText(rides + " " + getResources().getString(R.string.mytrips_label_trip));
                                        }
                                    }

                                    if (distance2 != null && distance2.length() > 0 && distance2.equalsIgnoreCase("-")) {
                                        distance2 = "0 KM";
                                    }

                                    Tv_tripDistance2.setText(distance2);
                                    RL_week2.setVisibility(View.VISIBLE);
                                    break;
                                case 2:
                                    Tv_weekName3.setText(mMonth.getWeek());
                                    Tv_weekCount3.setText(mMonth.getWeekCount());
                                    String rideCount3 = mMonth.getRidecount();
                                    String distance3 = mMonth.getDistance();

                                    if (rideCount3 != null && rideCount3.length() > 0) {
                                        int rides = 0;
                                        if (!rideCount3.equalsIgnoreCase("-")) {
                                            try {
                                                rides = Integer.parseInt(rideCount3);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                        if (rides > 1) {
                                            Tv_tripsCount3.setText(rides + " " + getResources().getString(R.string.mytrips_label_trips));
                                        } else {
                                            Tv_tripsCount3.setText(rides + " " + getResources().getString(R.string.mytrips_label_trip));
                                        }
                                    }

                                    if (distance3 != null && distance3.length() > 0 && distance3.equalsIgnoreCase("-")) {
                                        distance3 = "0 KM";
                                    }

                                    Tv_tripDistance3.setText(distance3);
                                    RL_week3.setVisibility(View.VISIBLE);
                                    break;
                                case 3:
                                    Tv_weekName4.setText(mMonth.getWeek());
                                    Tv_weekCount4.setText(mMonth.getWeekCount());
                                    String rideCount4 = mMonth.getRidecount();
                                    String distance4 = mMonth.getDistance();

                                    if (rideCount4 != null && rideCount4.length() > 0) {
                                        int rides = 0;
                                        if (!rideCount4.equalsIgnoreCase("-")) {
                                            try {
                                                rides = Integer.parseInt(rideCount4);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                        if (rides > 1) {
                                            Tv_tripsCount4.setText(rides + " " + getResources().getString(R.string.mytrips_label_trips));
                                        } else {
                                            Tv_tripsCount4.setText(rides + " " + getResources().getString(R.string.mytrips_label_trip));
                                        }
                                    }

                                    if (distance4 != null && distance4.length() > 0 && distance4.equalsIgnoreCase("-")) {
                                        distance4 = "0 KM";
                                    }

                                    Tv_tripDistance4.setText(distance4);
                                    RL_week4.setVisibility(View.VISIBLE);
                                    break;
                                case 4:
                                    Tv_weekName5.setText(mMonth.getWeek());
                                    Tv_weekCount5.setText(mMonth.getWeekCount());
                                    String rideCount5 = mMonth.getRidecount();
                                    String distance5 = mMonth.getDistance();

                                    if (rideCount5 != null && rideCount5.length() > 0) {
                                        int rides = 0;
                                        if (!rideCount5.equalsIgnoreCase("-")) {
                                            try {
                                                rides = Integer.parseInt(rideCount5);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                        if (rides > 1) {
                                            Tv_tripsCount5.setText(rides + " " + getResources().getString(R.string.mytrips_label_trips));
                                        } else {
                                            Tv_tripsCount5.setText(rides + " " + getResources().getString(R.string.mytrips_label_trip));
                                        }
                                    }

                                    if (distance5 != null && distance5.length() > 0 && distance5.equalsIgnoreCase("-")) {
                                        distance5 = "0 KM";
                                    }

                                    Tv_tripDistance5.setText(distance5);
                                    RL_week5.setVisibility(View.VISIBLE);
                                    break;

                            }

                        }

                    }

                }

                if (dialog != null) {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(MyTripMonthDetail.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

}