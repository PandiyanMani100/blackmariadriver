package com.blackmaria.partner.app;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.Pojo.IncentiveDetail;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by Ganesh on 15-05-2017.
 */

public class DriverIncentives extends ActivityHockeyApp implements View.OnClickListener {

    private TextView Tv_estimatedIncentives, Tv_highlightMonth;
    private RelativeLayout RL_withdraw;
    private TextView Tv_totalPayout, Tv_view_statement;
    private RelativeLayout RL_viewStatement;
    //    private Spinner Spnr_viewStatement;
    private ImageView Iv_back, Iv_home, Iv_prev, Iv_next;

    private SessionManager sessionManager;
    private ConnectionDetector cd;
    private Dialog dialog;
    private ServiceRequest mRequest;
    private Calendar calendar = null;
    private SimpleDateFormat mMonthFormat, mYearFormat, mDisplayFormat;
    private BroadcastReceiver withdrawSuccessReceiver;

    private String sDriverID = "", sTotalAmount = "", sCurrency = "", sFilterMonth = "", sFilterYear = "", sDisplayDate = "",
            sCurrentMonth = "", sCurrentYear = "", sTotalPayoutAmount = "", sProceedWithdrawStatus = "", sProceedWithdrawError = "",
            sSecurePin = "";
    //    private boolean isFirstTime = true;
//    private String filterText = "", formatedFilterText = "";
//    private String[] statementsList;
//    private int selectedPosition = 0;
    private ArrayList<IncentiveDetail> incentivesList;
    private TextView Tv_titleTarget, Tv_titleHighTrip, Tv_titleHighMiles, Tv_titleSession, Tv_titleSOS, Tv_moreInfo;
    //    private TextView Tv_Target, Tv_HighTrip, Tv_HighMiles, Tv_Session, Tv_SOS;
    private RelativeLayout RL_Target, RL_HighTrip, RL_HighMiles, RL_Session, RL_SOS;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.incentives);

        initialize();

    }

    private void initialize() {

        sessionManager = new SessionManager(DriverIncentives.this);
        cd = new ConnectionDetector(DriverIncentives.this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        sSecurePin = sessionManager.getSecurePin();
        incentivesList = new ArrayList<IncentiveDetail>();


        // To Register Broadcast Receiver
        registerBroadcast();


        // Get Current Month And Year
        calendar = Calendar.getInstance();
        mMonthFormat = new SimpleDateFormat("MM");
        mYearFormat = new SimpleDateFormat("yyyy");
        mDisplayFormat = new SimpleDateFormat("MMMM yyyy");

        sFilterMonth = mMonthFormat.format(calendar.getTime());
        sFilterYear = mYearFormat.format(calendar.getTime());
        sDisplayDate = mDisplayFormat.format(calendar.getTime());
        sCurrentMonth = sFilterMonth;
        sCurrentYear = sFilterYear;

        Tv_moreInfo = (TextView) findViewById(R.id.txt_label_more_info);
        Tv_estimatedIncentives = (TextView) findViewById(R.id.txt_estimated_incentive);
        Tv_highlightMonth = (TextView) findViewById(R.id.txt_month_highlights);
        RL_withdraw = (RelativeLayout) findViewById(R.id.Rl_image_withdraw);
        Tv_totalPayout = (TextView) findViewById(R.id.txt_total_pay);
        RL_viewStatement = (RelativeLayout) findViewById(R.id.Rl_view_statement);
//        Spnr_viewStatement = (Spinner) findViewById(R.id.spnr_view_statement);
        Tv_view_statement = (TextView) findViewById(R.id.txt_label_view_statement);
        Iv_back = (ImageView) findViewById(R.id.img_back);
        Iv_home = (ImageView) findViewById(R.id.img_home);
        Iv_prev = (ImageView) findViewById(R.id.img_left);
        Iv_next = (ImageView) findViewById(R.id.img_right);

        Tv_titleTarget = (TextView) findViewById(R.id.txt_label_target);
        Tv_titleHighTrip = (TextView) findViewById(R.id.txt_label_high_trip);
        Tv_titleHighMiles = (TextView) findViewById(R.id.txt_label_high_miles);
        Tv_titleSession = (TextView) findViewById(R.id.txt_label_session);
        Tv_titleSOS = (TextView) findViewById(R.id.txt_label_payment_gateway);

//        Tv_Target = (TextView) findViewById(R.id.txt_target);
//        Tv_HighTrip = (TextView) findViewById(R.id.txt_high_trip);
//        Tv_HighMiles = (TextView) findViewById(R.id.txt_high_miles);
//        Tv_Session = (TextView) findViewById(R.id.txt_session);
//        Tv_SOS = (TextView) findViewById(R.id.txt_sos);

        RL_Target = (RelativeLayout) findViewById(R.id.Rl_target);
        RL_HighTrip = (RelativeLayout) findViewById(R.id.Rl_high_trip);
        RL_HighMiles = (RelativeLayout) findViewById(R.id.Rl_high_miles);
        RL_Session = (RelativeLayout) findViewById(R.id.Rl_session);
        RL_SOS = (RelativeLayout) findViewById(R.id.Rl_payment_gateway);

        Iv_back.setOnClickListener(this);
        Iv_home.setOnClickListener(this);
        Iv_prev.setOnClickListener(this);
        Iv_next.setOnClickListener(this);
        Tv_moreInfo.setOnClickListener(this);
        RL_withdraw.setOnClickListener(this);
        RL_viewStatement.setOnClickListener(this);
        RL_Target.setOnClickListener(this);
        RL_HighTrip.setOnClickListener(this);
        RL_HighMiles.setOnClickListener(this);
        RL_Session.setOnClickListener(this);
        RL_SOS.setOnClickListener(this);



        /*statementsList = getResources().getStringArray(R.array.earnings_statement_array);
        filterText = statementsList[0];
        formatedFilterText = "today";
        Tv_view_statement.setText(filterText);*/

        /*Spnr_viewStatement.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("On Item Selected");
                if (!isFirstTime) {
                    selectedPosition = position;
                    filterText = statementsList[position];

                    if(position==0){
                        formatedFilterText = "today";
                    }else if(position==1){
                        formatedFilterText = "yesterday";
                    }else if(position==2){
                        formatedFilterText = "week";
                    }else if(position==3){
                        formatedFilterText = "month";
                    }

                    System.out.println("Setting Text " + statementsList[position]);
                    Tv_view_statement.setText(filterText);

                    *//*if (cd.isConnectingToInternet()) {
                        DriverEarningsRequest(Iconstant.earnings_dashboard_url);
                    } else {
                        Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                    }*//*

                }
                isFirstTime = false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        EarningsSpinnerAdapter customAdapter = new EarningsSpinnerAdapter(getApplicationContext(), statementsList);
        Spnr_viewStatement.setAdapter(customAdapter);*/


        if (cd.isConnectingToInternet()) {
            IncentivesDashboardRequest(Iconstant.incentives_dashboard_url);
        } else {
            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }


    }

    private void registerBroadcast() {

        IntentFilter filter = new IntentFilter();
        filter.addAction("com.app.Withdraw.Success");
        withdrawSuccessReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent != null) {

                    // Refresh Page to show Updated Content
                    if (cd.isConnectingToInternet()) {
                        IncentivesDashboardRequest(Iconstant.incentives_dashboard_url);
                    } else {
                        Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                    }

                }

            }
        };
        registerReceiver(withdrawSuccessReceiver, filter);

    }

    @SuppressLint("WrongConstant")
    @Override
    public void onClick(View view) {

        if (view == Iv_back) {

//            refreshHomePage();

            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (view == Iv_prev) {

            if (cd.isConnectingToInternet()) {

                if (calendar != null) {

                    calendar.add(Calendar.MONTH, -1);

                    sFilterMonth = mMonthFormat.format(calendar.getTime());
                    sFilterYear = mYearFormat.format(calendar.getTime());
                    sDisplayDate = mDisplayFormat.format(calendar.getTime());

                    IncentivesDashboardRequest(Iconstant.incentives_dashboard_url);

                }

            } else {
                Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
            }

        } else if (view == Iv_next) {

            if (cd.isConnectingToInternet()) {

                if (calendar != null) {

                    calendar.add(Calendar.MONTH, 1);

                    sFilterMonth = mMonthFormat.format(calendar.getTime());
                    sFilterYear = mYearFormat.format(calendar.getTime());
                    sDisplayDate = mDisplayFormat.format(calendar.getTime());

                    IncentivesDashboardRequest(Iconstant.incentives_dashboard_url);

                }

            } else {
                Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
            }

        } else if (view == RL_withdraw) {


            if (sProceedWithdrawStatus.equalsIgnoreCase("1")) {

                String amount = sTotalAmount.replace(sCurrency, "").trim();

                try {

                    if (amount.length() > 0) {

                        double totalAmount = Double.parseDouble(amount);

                        if (totalAmount > 0) {
                            confirmPin();
                        } else {
                            String message = getResources().getString(R.string.earnings_page_label_no_amount_to_withdraw);
                            Alert(getResources().getString(R.string.action_error), message);
                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {
                Alert(getResources().getString(R.string.action_error), sProceedWithdrawError);
            }

        } else if (view == Iv_home) {

            refreshHomePage();

            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (view == RL_viewStatement) {

            Intent intent = new Intent(DriverIncentives.this, IncentiveStatementMonth.class);
            intent.putExtra("filterMonth", sFilterMonth);
            intent.putExtra("filterYear", sFilterYear);
            intent.putExtra("displayMonth", sDisplayDate);
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.exit);

        } else if (view == RL_Target) {

            IncentiveDetail detail = null;

            for (int i = 0; i < incentivesList.size(); i++) {
                String incentiveType = incentivesList.get(i).getIncetives_type();
                if (incentiveType.equalsIgnoreCase(getResources().getString(R.string.incentives_type_label_week_trip))) {
                    detail = incentivesList.get(i);
                }
            }

            if (detail != null) {
                incentivesDialog("target", detail);
            } else {
                Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.incentives_type_module_not_available));
            }

        } else if (view == RL_HighTrip) {

            IncentiveDetail detail = null;

            for (int i = 0; i < incentivesList.size(); i++) {
                String incentiveType = incentivesList.get(i).getIncetives_type();
                if (incentiveType.equalsIgnoreCase(getResources().getString(R.string.incentives_type_label_daily_trip))) {
                    detail = incentivesList.get(i);
                }
            }

            if (detail != null) {
                incentivesDialog("trip", detail);
            } else {
                Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.incentives_type_module_not_available));
            }


        } else if (view == RL_HighMiles) {

            IncentiveDetail detail = null;

            for (int i = 0; i < incentivesList.size(); i++) {
                String incentiveType = incentivesList.get(i).getIncetives_type();
                if (incentiveType.equalsIgnoreCase(getResources().getString(R.string.incentives_type_label_daily_mile))) {
                    detail = incentivesList.get(i);
                }
            }

            if (detail != null) {
                incentivesDialog("mile", detail);
            } else {
                Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.incentives_type_module_not_available));
            }

        } else if (view == RL_Session) {

            IncentiveDetail detail = null;

            for (int i = 0; i < incentivesList.size(); i++) {
                String incentiveType = incentivesList.get(i).getIncetives_type();
                if (incentiveType.equalsIgnoreCase(getResources().getString(R.string.incentives_type_label_schedule_hours))) {
                    detail = incentivesList.get(i);
                }
            }

            if (detail != null) {
                incentivesDialog("scheduled", detail);
            } else {
                Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.incentives_type_module_not_available));
            }


        } else if (view == RL_SOS) {

            IncentiveDetail detail = null;

            for (int i = 0; i < incentivesList.size(); i++) {
                String incentiveType = incentivesList.get(i).getIncetives_type();
                if (incentiveType.equalsIgnoreCase(getResources().getString(R.string.incentives_type_label_sos))) {
                    detail = incentivesList.get(i);
                }
            }

            if (detail != null) {
                incentivesSOSDialog();
            } else {
                Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.incentives_type_module_not_available));
            }

        } else if (view == Tv_moreInfo) {

            moreInfoDialog();

        }

    }

    private void confirmPin() {
        final Dialog confirmPinDialog = new Dialog(DriverIncentives.this, R.style.SlideRightDialog);
        confirmPinDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmPinDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmPinDialog.setCancelable(true);
        confirmPinDialog.setContentView(R.layout.alert_enter_pin);

        final PinEntryEditText Ed_pin = (PinEntryEditText) confirmPinDialog.findViewById(R.id.edt_withdrawal_amount);
        final TextView Tv_forgotPin = (TextView) confirmPinDialog.findViewById(R.id.txt_label_forgot_pin);
        final RelativeLayout RL_confirm = (RelativeLayout) confirmPinDialog.findViewById(R.id.Rl_confirm);
        final ImageView Iv_close = (ImageView) confirmPinDialog.findViewById(R.id.img_close);

        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmPinDialog.dismiss();
            }
        });

        Tv_forgotPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        RL_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (confirmPinDialog != null && confirmPinDialog.isShowing()) {
                    confirmPinDialog.dismiss();
                }

                if (sSecurePin.equalsIgnoreCase(Ed_pin.getText().toString())) {

                    Intent intent = new Intent(DriverIncentives.this, WithdrawalPage.class);
                    intent.putExtra("page", "incentives");
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);

                }

            }
        });

        confirmPinDialog.show();

    }

    @SuppressLint("SetTextI18n")
    private void incentivesDialog(String type, IncentiveDetail detail) {

        //--------Adjusting Dialog width & Height-----
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 80% of the screen
        int screenHeight = (int) (metrics.heightPixels * 0.85);//fill only 80% of the screen

        final Dialog incentiveDetailsDialog = new Dialog(DriverIncentives.this, R.style.SlideUpDialog);
        incentiveDetailsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        incentiveDetailsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        incentiveDetailsDialog.setCancelable(true);
        if (type.equalsIgnoreCase("scheduled")) {
            incentiveDetailsDialog.setContentView(R.layout.alert_incentive_detail_schedule);
        } else {
            incentiveDetailsDialog.setContentView(R.layout.alert_incentive_details);
        }
        incentiveDetailsDialog.getWindow().setLayout(screenWidth, screenHeight);

        ImageView Iv_incentiveType = (ImageView) incentiveDetailsDialog.findViewById(R.id.img_incentive_type);
        TextView Tv_incentiveAmount = (TextView) incentiveDetailsDialog.findViewById(R.id.txt_incentive_amount);
        TextView Tv_targetIncentiveLabel = (TextView) incentiveDetailsDialog.findViewById(R.id.txt_label_target_incentive);
//        TextView Tv_currentIncentiveLabel = (TextView) incentiveDetailsDialog.findViewById(R.id.txt_label_current_incentive);
        TextView Tv_targetIncentive = (TextView) incentiveDetailsDialog.findViewById(R.id.txt_target_incentive);
        TextView Tv_currentIncentive = (TextView) incentiveDetailsDialog.findViewById(R.id.txt_current_incentive);
        TextView Tv_period = (TextView) incentiveDetailsDialog.findViewById(R.id.txt_incentive_period);
//        ImageView Iv_car = (ImageView) incentiveDetailsDialog.findViewById(R.id.img_car);
        ImageView Iv_clock = (ImageView) incentiveDetailsDialog.findViewById(R.id.img_clock);

        if (incentivesList.size() > 0) {
            if (type.equalsIgnoreCase("target")) {

                Iv_incentiveType.setImageResource(R.drawable.key_blue);
                Tv_incentiveAmount.setText(detail.getCurrency() + " " + detail.getIncetive_amount());
                Tv_targetIncentiveLabel.setText(getResources().getString(R.string.incentives_page_label_weekly_trip_target));
                Tv_targetIncentive.setText(detail.getTarget());
                Tv_currentIncentive.setText(detail.getCompleted_level());
                Tv_period.setText(detail.getIncentive_period());

            } else if (type.equalsIgnoreCase("trip")) {

                Iv_incentiveType.setImageResource(R.drawable.steering_blue);
                Tv_incentiveAmount.setText(detail.getCurrency() + " " + detail.getIncetive_amount());
                Tv_targetIncentiveLabel.setText(getResources().getString(R.string.incentives_page_label_highest_now_at));
                Tv_targetIncentive.setText(detail.getTarget());
                Tv_currentIncentive.setText(detail.getCompleted_level());
                Tv_period.setText(detail.getIncentive_period());

            } else if (type.equalsIgnoreCase("mile")) {

                Iv_incentiveType.setImageResource(R.drawable.distance_meter_blue);
                Tv_incentiveAmount.setText(detail.getCurrency() + " " + detail.getIncetive_amount());
                Tv_targetIncentiveLabel.setText(getResources().getString(R.string.incentives_page_label_highest_now_at));
                Tv_targetIncentive.setText(detail.getTarget_km());
                Tv_currentIncentive.setText(detail.getTarget_distance());
                Tv_period.setText(detail.getIncentive_period());

            } else if (type.equalsIgnoreCase("scheduled")) {

                Iv_incentiveType.setImageResource(R.drawable.img_foc_blue);
                Tv_incentiveAmount.setText(detail.getCurrency() + " " + detail.getIncetive_amount());
                Tv_targetIncentiveLabel.setText(getResources().getString(R.string.incentives_page_label_scheduled_incentives));
                /*Tv_targetIncentive.setText(getResources().getString(R.string.incentives_page_label_midnight));
                Tv_currentIncentive.setText(getResources().getString(R.string.incentives_page_label_evening));*/
                Tv_targetIncentive.setText(detail.getDay_time());
                Tv_currentIncentive.setText(detail.getCurrent_period());
//                Iv_car.setVisibility(View.GONE);
                Iv_clock.setVisibility(View.GONE);

            }
        }


        incentiveDetailsDialog.show();


    }


    private void incentivesSOSDialog() {

        //--------Adjusting Dialog width & Height-----
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 80% of the screen
        int screenHeight = (int) (metrics.heightPixels * 0.85);//fill only 80% of the screen

        final Dialog incentiveSOSDialog = new Dialog(DriverIncentives.this, R.style.SlideUpDialog);
        incentiveSOSDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        incentiveSOSDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        incentiveSOSDialog.setCancelable(true);
        incentiveSOSDialog.setContentView(R.layout.alert_incentive_detail_sos);
        incentiveSOSDialog.getWindow().setLayout(screenWidth, screenHeight);

        incentiveSOSDialog.show();

    }


    private void moreInfoDialog() {

        //--------Adjusting Dialog width & Height-----
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 80% of the screen
        int screenHeight = (int) (metrics.heightPixels * 0.85);//fill only 80% of the screen

        final Dialog moreInfoDialog = new Dialog(DriverIncentives.this, R.style.SlideUpDialog);
        moreInfoDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        moreInfoDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        moreInfoDialog.setCancelable(true);
        moreInfoDialog.setContentView(R.layout.alert_incentives_more_info);
        moreInfoDialog.getWindow().setLayout(screenWidth, screenHeight);

        final ImageView Iv_close = (ImageView) moreInfoDialog.findViewById(R.id.img_close);

        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moreInfoDialog.dismiss();
            }
        });

        /*String sWhatIsIncentives = bold(getResources().getString(R.string.incentives_page_alert_label_what_is_incentives)) + getResources().getString(R.string.incentives_page_alert_label_what_is_incentives_content);
//        final SpannableString text = new SpannableString(sWhatIsIncentives);
//        text.setSpan(new StyleSpan(Typeface.BOLD), 0, getResources().getString(R.string.incentives_page_alert_label_what_is_incentives).length(), 0);

        TextView Tv_whatIsIncentives = (TextView) moreInfoDialog.findViewById(R.id.txt_what_is_incentives);
        Tv_whatIsIncentives.setText(sWhatIsIncentives);*/

        moreInfoDialog.show();


    }


    @SuppressLint("WrongConstant")
    private void IncentivesDashboardRequest(String Url) {

        dialog = new Dialog(DriverIncentives.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        jsonParams.put("month", sFilterMonth);
        jsonParams.put("year", sFilterYear);

        System.out.println("--------------DriverIncentives Url-------------------" + Url);
        System.out.println("--------------DriverIncentives jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(DriverIncentives.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------DriverIncentives Response-------------------" + response);
                String status = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {
                            JSONObject jsonObject = object.getJSONObject("response");

                            if (jsonObject.length() > 0) {

                                sCurrency = jsonObject.getString("currency");
                                sTotalAmount = sCurrency +" "+ jsonObject.getString("total_amount");
                                sFilterMonth = jsonObject.getString("month");
                                sFilterYear = jsonObject.getString("year");
                                sTotalPayoutAmount = sCurrency +" "+ jsonObject.getString("total_payout_amount");
                                sProceedWithdrawStatus = jsonObject.getString("proceed_status");
                                sProceedWithdrawError = jsonObject.getString("error_message");

                                Object objIncentives = jsonObject.get("incetives_count");
                                if (objIncentives instanceof JSONArray) {

                                    JSONArray jarIncentives = jsonObject.getJSONArray("incetives_count");

                                    if (jarIncentives.length() > 0) {

                                        incentivesList = new ArrayList<IncentiveDetail>();
                                        for (int i = 0; i < jarIncentives.length(); i++) {

                                            JSONObject jobjIncentive = jarIncentives.getJSONObject(i);

                                            IncentiveDetail detail = new IncentiveDetail();
                                            String incetives_type = jobjIncentive.getString("incetives_type");
                                            String target = jobjIncentive.getString("target");
                                            String completed_level = jobjIncentive.getString("completed_level");
                                            String target_km = jobjIncentive.getString("target_km");
                                            String target_distance = jobjIncentive.getString("target_distance");
                                            String incetive_amount = jobjIncentive.getString("incetive_amount");
                                            String currency = jobjIncentive.getString("currency");
                                            String incentive_period = jobjIncentive.getString("incentive_period");
                                            String current_period = jobjIncentive.getString("current_period");
                                            String day_time = jobjIncentive.getString("day_time");

                                            detail.setIncetives_type(incetives_type);
                                            detail.setTarget(target);
                                            detail.setCompleted_level(completed_level);
                                            detail.setTarget_km(target_km);
                                            detail.setTarget_distance(target_distance);
                                            detail.setIncetive_amount(incetive_amount);
                                            detail.setCurrency(currency);
                                            detail.setIncentive_period(incentive_period);
                                            detail.setCurrent_period(current_period);
                                            detail.setDay_time(day_time);

                                            incentivesList.add(detail);

                                        }

                                    }

                                }

                            }

                        } else {
                            String sResponse = object.getString("response");
                            Alert(getResources().getString(R.string.action_error), sResponse);
                        }


                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (status.equalsIgnoreCase("1")) {

                    Tv_estimatedIncentives.setText(sTotalAmount);
                    Tv_totalPayout.setText(sTotalPayoutAmount);

                    if (sCurrentYear.equalsIgnoreCase(sFilterYear)) {
                        if (sCurrentMonth.equalsIgnoreCase(sFilterMonth)) {
                            Iv_next.setVisibility(View.GONE);
                        } else {
                            Iv_next.setVisibility(View.VISIBLE);
                        }
                    } else {
                        Iv_next.setVisibility(View.VISIBLE);
                    }

                    Tv_highlightMonth.setText(sDisplayDate + " " + getResources().getString(R.string.incentives_page_label_highlights));

                }

                if (dialog != null) {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(DriverIncentives.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (withdrawSuccessReceiver != null) {
            unregisterReceiver(withdrawSuccessReceiver);
        }

    }

    // Sending Broadcast to Refresh Home Page
    private void refreshHomePage() {

        // Refresh Home Page
        Intent intent = new Intent();
        intent.setAction("com.app.Refresh.HomePage");
        sendBroadcast(intent);

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {

//            refreshHomePage();

            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);


            return true;
        }
        return false;
    }

}
