package com.blackmaria.partner.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;

/**
 * Created by user144 on 9/18/2017.
 */

public class NewCloudMoneyReloadResponseWallet extends ActivityHockeyApp implements View.OnClickListener {
    private TextView reloadSuccessTv;
    private TextView reloadAmount1;
    private View dummyview;
    private Button closeButton;
    private RefreshReceiver refreshReceiver;
    private String reloadAmoun = "", paymenttype = "";
    SessionManager session;
    private LinearLayout vovcherLayout;
    ImageView vovcherImage;

    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            /*if (intent.getAction().equals("com.pushnotification.updateBottom_view")) {
                if (intent.getExtras() != null && intent.getExtras().containsKey("rideStatus")) {
                    String mRideStatus = (String) intent.getExtras().get("rideStatus");
                    String mRideid = (String) intent.getExtras().get("ride_id");
                    if (Iconstant.PushNotification_RideCancelled_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(NewCloudMoneyReloadResponseWallet.this, BookingPage1.class);
                        startActivity(intent1);
                        finish();
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                    } else if (Iconstant.PushNotification_PaymentPaid_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(NewCloudMoneyReloadResponseWallet.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "1");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    } else if (Iconstant.PushNotification_RideCompleted_Key.equalsIgnoreCase(mRideStatus) || Iconstant.PushNotification_RequestPayment_makepayment_Stripe_Key.equalsIgnoreCase(mRideStatus)) {
                        Intent intent1 = new Intent(NewCloudMoneyReloadResponseWallet.this, FareBreakUp.class);
                        intent1.putExtra("RideID", mRideid);
                        intent1.putExtra("ratingflag", "2");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();
                    }
                }
            }*/
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_cloud_money_reload_response_home);

        initialize();
    }

    private void initialize() {
        session = new SessionManager(NewCloudMoneyReloadResponseWallet.this);
        reloadSuccessTv = (TextView) findViewById(R.id.reload_success_tv);
        reloadAmount1 = (TextView) findViewById(R.id.reload_amount1);
        dummyview = (View) findViewById(R.id.dummyview);
        closeButton = (Button) findViewById(R.id.close_button);

        vovcherLayout = (LinearLayout) findViewById(R.id.layout);
        vovcherImage = (ImageView) findViewById(R.id.vovcherimage);

        String currency = session.getCurrency();
        closeButton.setOnClickListener(this);
        reloadAmoun = getIntent().getStringExtra("reloadamount");
        reloadAmount1.setText(currency + reloadAmoun);

        paymenttype = session.getReloadPaymentType();
        if (paymenttype.equalsIgnoreCase("1")) {
            vovcherLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.darkgrey_curved_bg));
            vovcherImage.setBackgroundDrawable(getResources().getDrawable(R.drawable.voucher_black));
            reloadAmount1.setTextColor(getResources().getColor(R.color.black_color));
        } else if (paymenttype.equalsIgnoreCase("2")) {
            vovcherLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.bluecurved_bg));
            vovcherImage.setBackgroundDrawable(getResources().getDrawable(R.drawable.voucher_white));
            reloadAmount1.setTextColor(getResources().getColor(R.color.white_color));
        } else if (paymenttype.equalsIgnoreCase("3")) {
            vovcherLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.darkgrey_curved_bg));
            vovcherImage.setBackgroundDrawable(getResources().getDrawable(R.drawable.voucher_orange));
            reloadAmount1.setTextColor(getResources().getColor(R.color.orange));
        } else if (paymenttype.equalsIgnoreCase("4")) {
            vovcherLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.bluecurved_bg));
            vovcherImage.setBackgroundDrawable(getResources().getDrawable(R.drawable.voucher_blue));
            reloadAmount1.setTextColor(getResources().getColor(R.color.white_color));
        } else {
            vovcherLayout.setBackgroundDrawable(getResources().getDrawable(R.drawable.darkgrey_curved_bg));
            vovcherImage.setBackgroundDrawable(getResources().getDrawable(R.drawable.voucher_black));
            reloadAmount1.setTextColor(getResources().getColor(R.color.black_color));
        }

        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);

    }

    @Override
    public void onClick(View v) {
        if (v == closeButton) {

            Intent intent = new Intent();
            intent.setAction("com.package.ACTION_WALLET_RELOAD_SUCCESS");
            sendBroadcast(intent);

            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {

            Intent intent = new Intent();
            intent.setAction("com.package.ACTION_WALLET_RELOAD_SUCCESS");
            sendBroadcast(intent);

            Intent intent1 = new Intent(NewCloudMoneyReloadResponseWallet.this, WalletMoneyPage1.class);
            intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent1);
            overridePendingTransition(R.anim.enter, R.anim.exit);
            finish();

            return true;
        }
        return false;
    }


}
