package com.blackmaria.partner.app;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.adapter.PlaceSearchAdapter;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by GANESH on 26-09-2017.
 */

public class RegistrationSelectLocation extends ActivityHockeyApp implements View.OnClickListener {

    private SessionManager sessionManager;
    private ConnectionDetector cd;
    private PlaceSearchAdapter adapter;
    private ServiceRequest mRequest;
    private Dialog dialog;

    private String sDriverID = "";
    private boolean isdataAvailable = false, isInternetPresent = false;
    ArrayList<String> itemList_location = new ArrayList<String>();
    ArrayList<String> itemList_placeId = new ArrayList<String>();

    private ListView Lv_locations;
    private EditText Edt_locationSearch;
    private RelativeLayout RL_exit;
    private Button Btn_confirm;
    private String Slatitude = "", Slongitude = "", Sselected_location = "", SplaceId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_select_location);

        initialize();

    }

    private void initialize() {

        sessionManager = new SessionManager(RegistrationSelectLocation.this);
        cd = new ConnectionDetector(RegistrationSelectLocation.this);
        isInternetPresent = cd.isConnectingToInternet();
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);

        RL_exit = (RelativeLayout) findViewById(R.id.RL_exit);
        Lv_locations = (ListView) findViewById(R.id.Lv_locations);
        Edt_locationSearch = (EditText) findViewById(R.id.Edt_location);
        Btn_confirm = (Button) findViewById(R.id.btn_confirm);

        RL_exit.setOnClickListener(this);
        Btn_confirm.setOnClickListener(this);


        Edt_locationSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {

                isInternetPresent = cd.isConnectingToInternet();

                if (isInternetPresent) {
                    if (mRequest != null) {
                        mRequest.cancelRequest();
                    }
                    Lv_locations.setVisibility(View.VISIBLE);
                    String data = Edt_locationSearch.getText().toString().toLowerCase().replace("%", "").replace(" ", "%20").trim();
                    String url = Iconstant.place_search_url + data;
                    url = url.replace("types=geocode", "types=(cities)");
                    CitySearchRequest(url);
                } else {
                    Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                }

            }
        });

        Edt_locationSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    CloseKeyboard(Edt_locationSearch);
                }
                return false;
            }
        });

        Edt_locationSearch.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager keyboard = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                keyboard.showSoftInput(Edt_locationSearch, 0);
            }
        }, 200);


        Lv_locations.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String locationName = itemList_location.get(position);
                Edt_locationSearch.setText(locationName);
                Sselected_location = locationName;
                SplaceId = itemList_placeId.get(position);
                Edt_locationSearch.setSelection(Edt_locationSearch.getText().length());
                if (isInternetPresent) {
                    if (mRequest != null) {
                        mRequest.cancelRequest();
                    }
                    LatLongRequest(Iconstant.GetAddressFrom_LatLong_url + SplaceId);
                } else {
                    Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                }
            }
        });

    }

    @Override
    public void onClick(View view) {

        if (view == RL_exit) {

            try {
                //close keyboard
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(RL_exit.getWindowToken(), 0);
            } catch (Exception e) {
                e.printStackTrace();
            }

            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);

        } else if (view == Btn_confirm) {


            if (!Edt_locationSearch.getText().toString().trim().equals("")) {

                if (!Slatitude.equalsIgnoreCase( "")) {

                    if (isInternetPresent) {
                        LocationBasedCarCategory(Iconstant.DriverReg_Locationwise_Carcategory_URL);
                    } else {
                        Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                    }

                } else {
                    Alert(getResources().getString(R.string.action_error), "Kindly Enter Valid Location");
                }

            } else {
                Alert(getResources().getString(R.string.action_error), "Location Should not be Empty");
            }

        }

    }

    private void LocationBasedCarCategory(String Url) {

        dialog = new Dialog(RegistrationSelectLocation.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("lat", Slatitude);
        jsonParams.put("lon", Slongitude);

        System.out.println("--------------LocationBasedCarCategory Url-------------------" + Url);
        System.out.println("--------------LocationBasedCarCategory jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(RegistrationSelectLocation.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------LocationBasedCarCategory Response-------------------" + response);
                String status = "", message = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");
                        message = object.getString("message");

                        if (status.equalsIgnoreCase("1")) {
                            Intent intent = new Intent(RegistrationSelectLocation.this, RegistrationSelectCategory.class);
                            intent.putExtra("lat", Slatitude);
                            intent.putExtra("lon", Slongitude);
                            intent.putExtra("placeId", SplaceId);
                            startActivity(intent);
                            overridePendingTransition(R.anim.enter, R.anim.exit);
                        } else {
                            String sResponse = object.getString("response");
                            Alert(getResources().getString(R.string.action_error), sResponse);
                        }

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (status.equalsIgnoreCase("1")) {

                }

                if (dialog != null) {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }



    private void CloseKeyboard(EditText edittext) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(edittext.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(RegistrationSelectLocation.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.alert_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();

    }

    //-------------------Search Place Request----------------
    private void CitySearchRequest(String Url) {

        System.out.println("--------------Search city url-------------------" + Url);

        mRequest = new ServiceRequest(RegistrationSelectLocation.this);
        mRequest.makeServiceRequest(Url, Request.Method.GET, null, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------Search city  reponse-------------------" + response);
                String status = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");
                        JSONArray place_array = object.getJSONArray("predictions");
                        if (status.equalsIgnoreCase("OK")) {
                            if (place_array.length() > 0) {
                                itemList_location.clear();
                                itemList_placeId.clear();
                                for (int i = 0; i < place_array.length(); i++) {
                                    JSONObject place_object = place_array.getJSONObject(i);
                                    itemList_location.add(place_object.getString("description"));
                                    itemList_placeId.add(place_object.getString("place_id"));
                                }
                                isdataAvailable = true;
                            } else {
                                itemList_location.clear();
                                itemList_placeId.clear();
                                isdataAvailable = false;
                            }
                        } else {
                            itemList_location.clear();
                            itemList_placeId.clear();
                            isdataAvailable = false;
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (isdataAvailable) {
//                    tv_emptyText.setVisibility(View.GONE);
                    if (itemList_location.size() > 0) {
                        adapter = new PlaceSearchAdapter(RegistrationSelectLocation.this, itemList_location);
                        Lv_locations.setAdapter(adapter);
                        Lv_locations.setVisibility(View.VISIBLE);
//                        adapter.notifyDataSetChanged();
                    } else {
                        Lv_locations.setVisibility(View.GONE);
                    }
                } else {
//                    tv_emptyText.setVisibility(View.VISIBLE);
                    Lv_locations.setVisibility(View.GONE);
                }

            }

            @Override
            public void onErrorListener() {

                // close keyboard
                CloseKeyboard(Edt_locationSearch);
            }
        });
    }


    //-------------------Get Latitude and Longitude from Address(Place ID) Request----------------
    private void LatLongRequest(String Url) {
        Lv_locations.setVisibility(View.GONE);
        dialog = new Dialog(RegistrationSelectLocation.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_processing));

        System.out.println("--------------LatLong url-------------------" + Url);

        mRequest = new ServiceRequest(RegistrationSelectLocation.this);
        mRequest.makeServiceRequest(Url, Request.Method.GET, null, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------LatLong  reponse-------------------" + response);
                String status = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");
                        JSONObject place_object = object.getJSONObject("result");
                        if (status.equalsIgnoreCase("OK")) {
                            if (place_object.length() > 0) {
                                JSONObject geometry_object = place_object.getJSONObject("geometry");
                                if (geometry_object.length() > 0) {
                                    JSONObject location_object = geometry_object.getJSONObject("location");
                                    if (location_object.length() > 0) {
                                        Slatitude = location_object.getString("lat");
                                        Slongitude = location_object.getString("lng");
                                        isdataAvailable = true;
                                    } else {
                                        isdataAvailable = false;
                                    }
                                } else {
                                    isdataAvailable = false;
                                }
                            } else {
                                isdataAvailable = false;
                            }
                        } else {
                            isdataAvailable = false;
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (isdataAvailable) {

                    Lv_locations.setVisibility(View.GONE);
                    dialog.dismiss();

                   /* Intent returnIntent = new Intent();
                    returnIntent.putExtra("Selected_Latitude", Slatitude);
                    returnIntent.putExtra("Selected_Longitude", Slongitude);
                    returnIntent.putExtra("Selected_Location", Sselected_location);
                    setResult(RESULT_OK, returnIntent);
                    */

                } else {
                    dialog.dismiss();
                    Alert(/*getResources().getString(R.string.alert)*/"sorry", status);
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

    //-----------------Move Back on pressed phone back button------------------
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {

            try {
                // close keyboard
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(RL_exit.getWindowToken(), 0);
            } catch (Exception e) {
                e.printStackTrace();
            }

            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
            return true;
        }
        return false;
    }

}
