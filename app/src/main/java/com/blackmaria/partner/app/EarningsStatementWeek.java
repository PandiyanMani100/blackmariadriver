package com.blackmaria.partner.app;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.Pojo.EarningsWeekDetail;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Dashboard_constrain;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Ganesh on 17-05-2017.
 */

public class EarningsStatementWeek extends ActivityHockeyApp implements View.OnClickListener {

    private ImageView Iv_back, Iv_home, Iv_prev, Iv_next;
    private TextView Tv_displayDate;
    private TextView Tv_earningsMon, Tv_earningsTue, Tv_earningsWed, Tv_earningsThu, Tv_earningsFri, Tv_earningsSat, Tv_earningsSun;
    private RelativeLayout RL_mon, RL_tue, RL_wed, RL_thu, RL_fri, RL_sat, RL_sun;

    private SessionManager sessionManager;
    private ConnectionDetector cd;
    private Dialog dialog;
    private ServiceRequest mRequest;
    private Calendar mCurrentCalendar = null, mCalendar = null;
    private SimpleDateFormat mdFormat;

    private String sDriveID = "";
    private String sFilterDateFrom = "", sFilterDateTo = "", sEndDate = "", sDisplayDate = "";
    private ArrayList<EarningsWeekDetail> listWeekDetails;
    private boolean isDataAvailable = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.earnings_statement_week);

        initialize();

    }

    @SuppressLint("WrongConstant")
    private void initialize() {

        sessionManager = new SessionManager(EarningsStatementWeek.this);
        cd = new ConnectionDetector(EarningsStatementWeek.this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriveID = user.get(SessionManager.KEY_DRIVERID);
        listWeekDetails = new ArrayList<EarningsWeekDetail>();
        mdFormat = new SimpleDateFormat("yyyy-MM-dd");
        mCalendar = Calendar.getInstance();
        mCalendar.setFirstDayOfWeek(Calendar.MONDAY);

        Iv_back = (ImageView) findViewById(R.id.img_back);
        Iv_home = (ImageView) findViewById(R.id.img_home);
        Iv_prev = (ImageView) findViewById(R.id.img_prev);
        Iv_next = (ImageView) findViewById(R.id.img_next);
        Tv_displayDate = (TextView) findViewById(R.id.txt_date);
        Tv_earningsMon = (TextView) findViewById(R.id.txt_earning_monday);
        Tv_earningsTue = (TextView) findViewById(R.id.txt_earning_tuesday);
        Tv_earningsWed = (TextView) findViewById(R.id.txt_earning_wednesday);
        Tv_earningsThu = (TextView) findViewById(R.id.txt_earning_thursday);
        Tv_earningsFri = (TextView) findViewById(R.id.txt_earning_friday);
        Tv_earningsSat = (TextView) findViewById(R.id.txt_earning_saturday);
        Tv_earningsSun = (TextView) findViewById(R.id.txt_earning_sunday);
        RL_mon = (RelativeLayout) findViewById(R.id.Rl_mon);
        RL_tue = (RelativeLayout) findViewById(R.id.Rl_tue);
        RL_wed = (RelativeLayout) findViewById(R.id.Rl_wed);
        RL_thu = (RelativeLayout) findViewById(R.id.Rl_thu);
        RL_fri = (RelativeLayout) findViewById(R.id.Rl_fri);
        RL_sat = (RelativeLayout) findViewById(R.id.Rl_sat);
        RL_sun = (RelativeLayout) findViewById(R.id.Rl_sun);

        Iv_back.setOnClickListener(this);
        Iv_home.setOnClickListener(this);
        Iv_prev.setOnClickListener(this);
        Iv_next.setOnClickListener(this);
        RL_mon.setOnClickListener(this);
        RL_tue.setOnClickListener(this);
        RL_wed.setOnClickListener(this);
        RL_thu.setOnClickListener(this);
        RL_fri.setOnClickListener(this);
        RL_sat.setOnClickListener(this);
        RL_sun.setOnClickListener(this);

        getCurrentWeek();
        getIntentData();

        if (cd.isConnectingToInternet()) {
            EarningsStatementWeekRequest(Iconstant.earnings_weekwise_url);
        } else {
            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }


    }

    @SuppressLint("WrongConstant")
    private void getIntentData() {

        Intent intent = getIntent();
        if (intent != null) {
            sFilterDateFrom = intent.getStringExtra("filterFromDate");
            sFilterDateTo = intent.getStringExtra("filterToDate");

            System.out.println("Current Week From Date = " + sFilterDateFrom + "\nCurrent Week To Date = " + sFilterDateTo);

            mCalendar = Calendar.getInstance();
//            mCalendar.setFirstDayOfWeek(Calendar.MONDAY);

            try {
                Date mDate = mdFormat.parse(sFilterDateTo);
                mCalendar.setTime(mDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }

    }

    // Steps to Get Current Week
    @SuppressLint("WrongConstant")
    private void getCurrentWeek() {

        mCalendar = Calendar.getInstance();

        mCalendar.set(Calendar.DAY_OF_WEEK, 1);
        sFilterDateFrom = mdFormat.format(mCalendar.getTime());

        mCalendar.set(Calendar.DAY_OF_WEEK, 7);
        sFilterDateTo = mdFormat.format(mCalendar.getTime());
        sEndDate = sFilterDateTo;

    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(EarningsStatementWeek.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    @SuppressLint("WrongConstant")
    @Override
    public void onClick(View view) {

        if (view == Iv_back) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (view == Iv_home) {
            Intent i = new Intent(EarningsStatementWeek.this, Dashboard_constrain.class);
            startActivity(i);
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (view == Iv_prev) {

            if (mCalendar != null) {
                if (cd.isConnectingToInternet()) {

                    mCalendar.add(Calendar.DATE, -7);

                    mCalendar.set(Calendar.DAY_OF_WEEK, 1);
                    sFilterDateFrom = mdFormat.format(mCalendar.getTime());

                    mCalendar.set(Calendar.DAY_OF_WEEK, 7);
                    sFilterDateTo = mdFormat.format(mCalendar.getTime());

                    System.out.println("From Date = " + sFilterDateFrom + "\n" + "To Date = " + sFilterDateTo);

                    EarningsStatementWeekRequest(Iconstant.earnings_weekwise_url);

                } else {
                    Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                }


            }

        } else if (view == Iv_next) {

            if (mCalendar != null) {
                if (cd.isConnectingToInternet()) {

                    mCalendar.add(Calendar.DATE, 7);

                    mCalendar.set(Calendar.DAY_OF_WEEK, 1);
                    sFilterDateFrom = mdFormat.format(mCalendar.getTime());

                    mCalendar.set(Calendar.DAY_OF_WEEK, 7);
                    sFilterDateTo = mdFormat.format(mCalendar.getTime());

                    System.out.println("From Date = " + sFilterDateFrom + "\n" + "To Date = " + sFilterDateTo);

                    EarningsStatementWeekRequest(Iconstant.earnings_weekwise_url);

                } else {
                    Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                }

            }

        } else if (view == RL_mon) {
            openDayStatement(1);
        } else if (view == RL_tue) {
            openDayStatement(2);
        } else if (view == RL_wed) {
            openDayStatement(3);
        } else if (view == RL_thu) {
            openDayStatement(4);
        } else if (view == RL_fri) {
            openDayStatement(5);
        } else if (view == RL_sat) {
            openDayStatement(6);
        } else if (view == RL_sun) {
            openDayStatement(7);
        }
    }

    private void openDayStatement(int weekOrder) {

        for (int i = 0; i < listWeekDetails.size(); i++) {
            int order = listWeekDetails.get(i).getOrder();
            String filterDate = listWeekDetails.get(i).getDate_f();
            if (order == weekOrder) {
                Intent intent = new Intent(EarningsStatementWeek.this, EarningsStatementDay.class);
                intent.putExtra("filterDate", filterDate);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }

        }

    }

    @SuppressLint("WrongConstant")
    private void EarningsStatementWeekRequest(String Url) {

        dialog = new Dialog(EarningsStatementWeek.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriveID);
        jsonParams.put("from_date", sFilterDateFrom);
        jsonParams.put("to_date", sFilterDateTo);

        System.out.println("--------------EarningsStatementWeek Url-------------------" + Url);
        System.out.println("--------------EarningsStatementWeek jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(EarningsStatementWeek.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------EarningsStatementDay Response-------------------" + response);
                String status = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {
                            JSONObject jsonObject = object.getJSONObject("response");
                            if (jsonObject.length() > 0) {

                                sDisplayDate = jsonObject.getString("date_desc").replace("As", "");

                                Object objWeek = jsonObject.get("week");
                                if (objWeek instanceof JSONArray) {
                                    JSONArray jarWeek = jsonObject.getJSONArray("week");
                                    if (jarWeek.length() > 0) {

                                        listWeekDetails = new ArrayList<EarningsWeekDetail>();
                                        for (int i = 0; i < jarWeek.length(); i++) {

                                            JSONObject jobjWeek = jarWeek.getJSONObject(i);

                                            String day = jobjWeek.getString("day");
                                            String ridecount = jobjWeek.getString("ridecount");
                                            String distance = jobjWeek.getString("distance");
                                            String driver_earning = jobjWeek.getString("driver_earning");
                                            String currency = jobjWeek.getString("currency");
                                            int order = jobjWeek.getInt("order");
                                            String date_f = jobjWeek.getString("date_f");

                                            listWeekDetails.add(new EarningsWeekDetail(day, ridecount, distance, driver_earning, currency, order, date_f));
                                        }

                                        isDataAvailable = true;

                                    } else {
                                        isDataAvailable = false;
                                    }
                                } else {
                                    isDataAvailable = false;
                                }

                            } else {
                                isDataAvailable = false;
                            }
                        } else {
                            String sResponse = object.getString("response");
                            Alert(getResources().getString(R.string.action_error), sResponse);
                        }

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (status.equalsIgnoreCase("1")) {

                    if (isDataAvailable) {

                        for (int i = 0; i < listWeekDetails.size(); i++) {
                            String earningsAmount = listWeekDetails.get(i).getCurrency() + " " + listWeekDetails.get(i).getDriver_earning();
//                            String day = listWeekDetails.get(i).getDay();
                            int order = listWeekDetails.get(i).getOrder();
                            String date = listWeekDetails.get(i).getDate_f();

                            /*if(day.equalsIgnoreCase("monday")){
                                Tv_earningsMon.setText(earningsAmount);
                            }else if (day.equalsIgnoreCase("tuesday")){
                                Tv_earningsTue.setText(earningsAmount);
                            }else if (day.equalsIgnoreCase("wednesday")){
                                Tv_earningsWed.setText(earningsAmount);
                            }else if (day.equalsIgnoreCase("thursday")){
                                Tv_earningsThu.setText(earningsAmount);
                            }else if (day.equalsIgnoreCase("friday")){
                                Tv_earningsFri.setText(earningsAmount);
                            }else if (day.equalsIgnoreCase("saturday")){
                                Tv_earningsSat.setText(earningsAmount);
                            }else if (day.equalsIgnoreCase("sunday")){
                                Tv_earningsSun.setText(earningsAmount);
                            }*/

                            if (order == 1) {
                                Tv_earningsMon.setText(earningsAmount);
                                if(date.contains("1970")){
                                    RL_mon.setVisibility(View.INVISIBLE);
                                }else{
                                    RL_mon.setVisibility(View.VISIBLE);
                                }

                            } else if (order == 2) {
                                Tv_earningsTue.setText(earningsAmount);

                                if(date.contains("1970")){
                                    RL_tue.setVisibility(View.INVISIBLE);
                                }else{
                                    RL_tue.setVisibility(View.VISIBLE);
                                }
                            } else if (order == 3) {
                                Tv_earningsWed.setText(earningsAmount);

                                if(date.contains("1970")){
                                    RL_wed.setVisibility(View.INVISIBLE);
                                }else{
                                    RL_wed.setVisibility(View.VISIBLE);
                                }
                            } else if (order == 4) {
                                Tv_earningsThu.setText(earningsAmount);

                                if(date.contains("1970")){
                                    RL_thu.setVisibility(View.INVISIBLE);
                                }else{
                                    RL_thu.setVisibility(View.VISIBLE);
                                }
                            } else if (order == 5) {
                                Tv_earningsFri.setText(earningsAmount);

                                if(date.contains("1970")){
                                    RL_fri.setVisibility(View.INVISIBLE);
                                }else{
                                    RL_fri.setVisibility(View.VISIBLE);
                                }
                            } else if (order == 6) {
                                Tv_earningsSat.setText(earningsAmount);

                                if(date.contains("1970")){
                                    RL_sat.setVisibility(View.INVISIBLE);
                                }else{
                                    RL_sat.setVisibility(View.VISIBLE);
                                }
                            } else if (order == 7) {
                                Tv_earningsSun.setText(earningsAmount);

                                if(date.contains("1970")){
                                    RL_sun.setVisibility(View.INVISIBLE);
                                }else{
                                    RL_sun.setVisibility(View.VISIBLE);
                                }
                            }

                        }

                    }

                    if (sEndDate.equals(sFilterDateTo)) {
                        Iv_next.setVisibility(View.GONE);
                    } else {
                        Iv_next.setVisibility(View.VISIBLE);
                    }

                    Tv_displayDate.setText(sDisplayDate);

                }

                if (dialog != null) {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
            return true;
        }
        return false;
    }

}
