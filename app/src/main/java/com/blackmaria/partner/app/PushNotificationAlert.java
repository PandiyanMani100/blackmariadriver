package com.blackmaria.partner.app;

import android.content.Intent;
import android.os.Bundle;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.view.View;
import android.widget.TextView;

import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.view.Dashboard.OnlinepageConstrain;
import com.blackmaria.partner.latestpackageview.view.fare.FareSummaryConstrain;

import java.text.NumberFormat;
import java.util.Locale;


public class PushNotificationAlert extends ActivityHockeyApp {

    private ConstraintLayout RL_main;
    private TextView Message_Tv, Textview_Ok, Textview_alert_header;
    private String message = "", action = "", amount = "", rideid = "", currency_code = "", str_amount = "",driverId="";
    private TextView Btn_ok;
    private SessionManager sessionManager;

    private String Amount="",currency="";
    String user_id = "", paymenttype = "", payment_via = "", from_number = "", from = "", date = "", time = "", trans_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pushnotification);
        initialize();

        Btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("action-----------" + action);
                if (action.equalsIgnoreCase("ride_cancelled")) {

                    Intent finish_TripPage = new Intent();
                    finish_TripPage.setAction("com.app.finish.TripPage");
                    sendBroadcast(finish_TripPage);

                    Intent finish_GoOnlinePage = new Intent();
                    finish_GoOnlinePage.setAction("com.app.finish.GoOnlinePage");
                    sendBroadcast(finish_GoOnlinePage);

                    Intent intent = new Intent(PushNotificationAlert.this, OnlinepageConstrain.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.enter, R.anim.exit);

                }
                else if (action.equalsIgnoreCase("payment_paid") || action.equalsIgnoreCase(Iconstant.userEndTrip)) {
                    sessionManager.setWaitedTime(0, 0, "", "");
                    Intent intent = new Intent(PushNotificationAlert.this, FareSummaryConstrain.class);
                    intent.putExtra("rideId", rideid);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    finish();

                }
                else if (action.equalsIgnoreCase("share_pool_ride_cancelled")) {
                    Intent broadcastIntent_loading = new Intent();
                    broadcastIntent_loading.setAction("com.check.sharePool_ride_list");
                    sendBroadcast(broadcastIntent_loading);
                    finish();
                } else if (action.equalsIgnoreCase(Iconstant.ACTION_TAG_COMPLEMENTARY)) {
                    finish();
                }
                else if (Iconstant.ACTION_WALLET_SUCCESS.equalsIgnoreCase(action)) {

                    Intent finishPage = new Intent();
                    finishPage.setAction("com.package.ACTION_WALLET_WITHDRAWAL_SUCCESS");
                    sendBroadcast(finishPage);

                    Intent finishPage1 = new Intent();
                    finishPage1.setAction("com.package.ACTION_WALLET_RELOAD_SUCCESS");
                    sendBroadcast(finishPage1);

                    Intent intent = new Intent(PushNotificationAlert.this, Reload_Transfer_PushNotification.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("Str_action",action);
                    intent.putExtra("user_id", user_id);
                    intent.putExtra("Amount", Amount);
                    intent.putExtra("Currencycode", currency_code);
                    intent.putExtra("paymenttype", paymenttype);
                    intent.putExtra("payment_via", payment_via);
                    intent.putExtra("from_number", from_number);
                    intent.putExtra("from", from);
                    intent.putExtra("date", date);
                    intent.putExtra("time", time);
                    intent.putExtra("trans_id", trans_id);
                    startActivity(intent);
                    finish();
                }
                else if (Iconstant.ACTION_BANK_SUCCESS.equalsIgnoreCase(action)) {

                    Intent finishPage = new Intent();
                    finishPage.setAction("com.package.ACTION_WALLET_WITHDRAWAL_SUCCESS");
                    sendBroadcast(finishPage);

                    Intent finishPage1 = new Intent();
                    finishPage1.setAction("com.package.ACTION_WALLET_RELOAD_SUCCESS");
                    sendBroadcast(finishPage1);


                    Intent intent = new Intent(PushNotificationAlert.this, Reload_Transfer_PushNotification.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("Str_action",action);
                    intent.putExtra("user_id", user_id);
                    intent.putExtra("Amount", Amount);
                    intent.putExtra("Currencycode", currency_code);
                    intent.putExtra("paymenttype", paymenttype);
                    intent.putExtra("payment_via", payment_via);
                    intent.putExtra("from_number", from_number);
                    intent.putExtra("from", from);
                    intent.putExtra("date", date);
                    intent.putExtra("time", time);
                    intent.putExtra("trans_id", trans_id);
                    startActivity(intent);
                    finish();

                } else if (action.equalsIgnoreCase(Iconstant.ACTION_TAG_WEEK_TRIP) || action.equalsIgnoreCase(Iconstant.ACTION_TAG_DAILY_TRIP)
                        || action.equalsIgnoreCase(Iconstant.ACTION_TAG_DAILY_MILE) || action.equalsIgnoreCase(Iconstant.ACTION_TAG_SCHEDULE_INCENTIVE)) {

                    finish();
                    overridePendingTransition(R.anim.enter, R.anim.exit);

                } else if (Iconstant.ACTION_TAG_WALLET_CREDIT.equalsIgnoreCase(action)) {

                    Intent finishPage = new Intent();
                    finishPage.setAction("com.package.ACTION_WALLET_WITHDRAWAL_SUCCESS");
                    sendBroadcast(finishPage);

                    Intent intent = new Intent(PushNotificationAlert.this, Reload_Transfer_PushNotification.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("Str_action",action);
                    intent.putExtra("user_id", user_id);
                    intent.putExtra("Amount", Amount);
                    intent.putExtra("Currencycode", currency_code);
                    intent.putExtra("paymenttype", paymenttype);
                    intent.putExtra("payment_via", payment_via);
                    intent.putExtra("from_number", from_number);
                    intent.putExtra("from", from);
                    intent.putExtra("date", date);
                    intent.putExtra("time", time);
                    intent.putExtra("trans_id", trans_id);
                    startActivity(intent);
                    finish();
                }
                else
                {
                    finish();
                }
            }
        });
    }


    private void initialize() {
        sessionManager = SessionManager.getInstance(this);
        Intent i = getIntent();
        if (i.hasExtra("MessagePage")) {
            message = i.getStringExtra("MessagePage");
        }
        if (i.hasExtra("Action")) {
            action = i.getStringExtra("Action");
        }
        if (i.hasExtra("amount")) {
            amount = i.getStringExtra("amount");
        }
        if (i.hasExtra("RideId")) {
            rideid = i.getStringExtra("RideId");
        }
        if (i.hasExtra("Currencycode")) {
            currency_code = i.getStringExtra("Currencycode");
        }
        if (i.hasExtra("driverId")) {
            driverId = i.getStringExtra("driverId");
        }

        if (i.hasExtra("Amount")) {
            Amount = i.getStringExtra("Amount");
        }
        if (action.equalsIgnoreCase(Iconstant.ACTION_TAG_WALLET_CREDIT)||action.equalsIgnoreCase(Iconstant.ACTION_WALLET_SUCCESS) ||action.equalsIgnoreCase(Iconstant.ACTION_BANK_SUCCESS)) {

            user_id = i.getStringExtra("user_id");
            paymenttype = i.getStringExtra("paymenttype");
            payment_via = i.getStringExtra("payment_via");
            from_number = i.getStringExtra("from_number");
            from = i.getStringExtra("from");
            date = i.getStringExtra("date");
            time = i.getStringExtra("time");
            trans_id = i.getStringExtra("trans_id");
        }
        RL_main =  findViewById(R.id.RL_main);
        Message_Tv = (TextView) findViewById(R.id.custom_dialog_library_message_textview);
        Textview_alert_header = (TextView) findViewById(R.id.custom_dialog_library_title_textview);
        Btn_ok =  findViewById(R.id.custom_dialog_library_ok_button);
        Message_Tv.setText(message);

        //--------Adjusting Dialog width-----
//        DisplayMetrics metrics = getResources().getDisplayMetrics();
//        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 85% of the screen

//        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(screenWidth, RelativeLayout.LayoutParams.WRAP_CONTENT);
//        params.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
//        RL_main.setLayoutParams(params);
//        RL_main.setGravity(Gravity.CENTER);
        if (action.equalsIgnoreCase("ride_cancelled") || action.equalsIgnoreCase("share_pool_ride_cancelled")) {
            Textview_alert_header.setText(getResources().getString(R.string.label_pushnotification_canceled));
        } else if (action.equalsIgnoreCase("receive_cash")) {
            Textview_alert_header.setText(getResources().getString(R.string.label_pushnotification_cashreceived));
        } else if (action.equalsIgnoreCase("payment_paid")) {
            Textview_alert_header.setText(getResources().getString(R.string.label_pushnotification_ride_completed));
        } else if (action.equalsIgnoreCase(Iconstant.ACTION_TAG_COMPLEMENTARY)) {
            Textview_alert_header.setText(getResources().getString(R.string.label_pushnotification_complementary));
        } else if (action.equalsIgnoreCase(Iconstant.ACTION_WALLET_SUCCESS) || action.equalsIgnoreCase(Iconstant.ACTION_BANK_SUCCESS)) {
            Textview_alert_header.setText(getResources().getString(R.string.label_pushnotification_cashreceived));
        } else if (action.equalsIgnoreCase(Iconstant.ACTION_TAG_WEEK_TRIP) || action.equalsIgnoreCase(Iconstant.ACTION_TAG_DAILY_TRIP)
                || action.equalsIgnoreCase(Iconstant.ACTION_TAG_DAILY_MILE) || action.equalsIgnoreCase(Iconstant.ACTION_TAG_SCHEDULE_INCENTIVE)) {
            Textview_alert_header.setText(getResources().getString(R.string.label_pushnotification_cashreceived));
        } else if (action.equalsIgnoreCase(Iconstant.ACTION_TAG_WALLET_CREDIT)) {
            Textview_alert_header.setText(getResources().getString(R.string.label_pushnotification_cashreceived));
        }else if (action.equalsIgnoreCase(Iconstant.userEndTrip)) {
            Textview_alert_header.setText(getResources().getString(R.string.label_pushnotification_cashreceived));
            Message_Tv.setText("Ride completed");
        }

        if (currency_code != null && currency_code.length() > 0) {
//            Currency currencycode = Currency.getInstance(getLocale(currency_code));
//            str_amount = currencycode.getSymbol() + amount;
            str_amount = currency_code + " " + amount;
        }
    }

    @Override
    public void onBackPressed() {

    }

    //method to convert currency code to currency symbol
    private static Locale getLocale(String strCode) {

        for (Locale locale : NumberFormat.getAvailableLocales()) {
            String code = NumberFormat.getCurrencyInstance(locale).getCurrency().getCurrencyCode();
            if (strCode != null && strCode.equals(code)) {
                return locale;
            }
        }
        return null;
    }

}

