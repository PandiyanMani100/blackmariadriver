package com.blackmaria.partner.app;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by Prem Kumar and Anitha on 1/2/2017.
 */

public class MyTripWeek extends ActivityHockeyApp implements View.OnClickListener {

    private TextView Tv_tripDate, Tv_tripCompleted, Tv_tripCancelled, Tv_biddingTrip, Tv_sosTrip;
    private ImageView Iv_back, Iv_home;
    private ConnectionDetector cd;
    private boolean isInternetPresent = false;
    private SessionManager sessionManager;
    private String sDriveID = "";

    private ServiceRequest mRequest;
    Dialog dialog;
    private boolean isDataAvailable = false;
    private LinearLayout view_detail;

    private String sCompletedTrips = "", sCancelledTrips = "", sBiddingTrips = "", sSOSTrips = "", sFilterDateFrom = "", sFilterDateTo = "",
                   sEndDate = "";
    private Calendar calendar = null;
    private SimpleDateFormat mdFormat;
    private ImageView Iv_prev, Iv_next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trip_today);
        initialize();
    }

    @SuppressLint("WrongConstant")
    private void initialize() {
        sessionManager = new SessionManager(MyTripWeek.this);
        cd = new ConnectionDetector(MyTripWeek.this);
        isInternetPresent = cd.isConnectingToInternet();


        // Steps to Get Current Week
        mdFormat = new SimpleDateFormat("yyyy-MM-dd");

        if (calendar == null) {
            calendar = Calendar.getInstance();
        }

        calendar.set(Calendar.DAY_OF_WEEK, 1);
        sFilterDateFrom = mdFormat.format(calendar.getTime());

        calendar.set(Calendar.DAY_OF_WEEK, 7);
        sFilterDateTo = mdFormat.format(calendar.getTime());
        sEndDate = sFilterDateTo;

        System.out.println("From Date = " + sFilterDateFrom + "\n" + "To Date = " + sFilterDateTo);

        Tv_tripDate = (TextView) findViewById(R.id.txt_date);
        Tv_tripCompleted = (TextView) findViewById(R.id.tv_completed);
        Tv_tripCancelled = (TextView) findViewById(R.id.tv_cancel_trip);
        Tv_biddingTrip = (TextView) findViewById(R.id.tv_bidding_trip);
        Tv_sosTrip = (TextView) findViewById(R.id.tv_sos_trip);
        view_detail = (LinearLayout) findViewById(R.id.ll_view_detail);
        Iv_back = (ImageView) findViewById(R.id.img_back);
        Iv_home = (ImageView) findViewById(R.id.img_home);

        Iv_prev = (ImageView) findViewById(R.id.img_left);
        Iv_next = (ImageView) findViewById(R.id.img_right);

        Iv_prev.setOnClickListener(this);
        Iv_next.setOnClickListener(this);
        Iv_home.setOnClickListener(this);
        Iv_back.setOnClickListener(this);
        view_detail.setOnClickListener(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriveID = user.get(SessionManager.KEY_DRIVERID);

        if (isInternetPresent) {
            MyTripWeekRequest(Iconstant.week_trips_url);
        } else {
            alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }
    }

    @SuppressLint("WrongConstant")
    @Override
    public void onClick(View view) {
        if (view == Iv_back) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (view == Iv_home) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (view == view_detail) {
            Intent intent = new Intent(MyTripWeek.this, MyTripWeekDetail.class);
            intent.putExtra("fromDate",sFilterDateFrom);
            intent.putExtra("toDate",sFilterDateTo);
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (view == Iv_prev) {

                if (calendar != null) {
                    isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {

                        calendar.add(Calendar.DATE, -7);

                        calendar.set(Calendar.DAY_OF_WEEK, 1);
                        sFilterDateFrom = mdFormat.format(calendar.getTime());

                        calendar.set(Calendar.DAY_OF_WEEK, 7);
                        sFilterDateTo = mdFormat.format(calendar.getTime());

                        System.out.println("From Date = " + sFilterDateFrom + "\n" + "To Date = " + sFilterDateTo);

                        MyTripWeekRequest(Iconstant.week_trips_url);

                    } else {
                        alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                    }


            }

        } else if (view == Iv_next) {

            if (calendar != null) {
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {

                    calendar.add(Calendar.DATE, 7);

                    calendar.set(Calendar.DAY_OF_WEEK, 1);
                    sFilterDateFrom = mdFormat.format(calendar.getTime());

                    calendar.set(Calendar.DAY_OF_WEEK, 7);
                    sFilterDateTo = mdFormat.format(calendar.getTime());

                    System.out.println("From Date = " + sFilterDateFrom + "\n" + "To Date = " + sFilterDateTo);

                    MyTripWeekRequest(Iconstant.week_trips_url);

                } else {
                    alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                }

            }

        }
    }

    private void alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(MyTripWeek.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    private void MyTripWeekRequest(String Url) {

        dialog = new Dialog(MyTripWeek.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriveID);
        jsonParams.put("filter_date_from", sFilterDateFrom);
        jsonParams.put("filter_date_to", sFilterDateTo);

        System.out.println("--------------WeekTrips Url-------------------" + Url);
        System.out.println("--------------WeekTrips jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(MyTripWeek.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------WeekTrips Response-------------------" + response);
                String status = "", sFromDate = "", sToDate = "", sDisplayDate = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {
                            JSONObject jsonObject = object.getJSONObject("response");
                            if (jsonObject.length() > 0) {

                                JSONObject jobjWeek = jsonObject.getJSONObject("week");
                                if (jobjWeek.length() > 0) {
                                    sCompletedTrips = jobjWeek.getString("completed_rides");
                                    sBiddingTrips = jobjWeek.getString("bidding_trip");
                                    sSOSTrips = jobjWeek.getString("sos_trip");
                                    sCancelledTrips = jobjWeek.getString("cancel_rides");
                                }

                                sFromDate = jsonObject.getString("from_date");
                                sToDate = jsonObject.getString("to_date");
                                sDisplayDate = jsonObject.getString("week_desc");

                            }
                        } else {
                            String sResponse = object.getString("response");
                            alert(getResources().getString(R.string.action_error), sResponse);
                        }

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (status.equalsIgnoreCase("1")) {

                    Tv_tripDate.setText(sDisplayDate);
                    Tv_sosTrip.setText(sSOSTrips);
                    Tv_tripCompleted.setText(sCompletedTrips);
                    Tv_tripCancelled.setText(sCancelledTrips);
                    Tv_biddingTrip.setText(sBiddingTrips);

                    if(sEndDate.equals(sFilterDateTo)){
                        Iv_next.setVisibility(View.GONE);
                    } else {
                        Iv_next.setVisibility(View.VISIBLE);
                    }

                }

                if (dialog != null) {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
            return true;
        }
        return false;
    }


}
