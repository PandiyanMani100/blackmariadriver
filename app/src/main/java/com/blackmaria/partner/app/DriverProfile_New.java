package com.blackmaria.partner.app;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.Pojo.ProfilePojo;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.adapter.ProfileAdapter;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.CircularImageView;
import com.blackmaria.partner.latestpackageview.widgets.CustomTextView;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;

import static com.blackmaria.partner.Utils.Myanim.collapse;
import static com.blackmaria.partner.Utils.Myanim.expand;

/**
 * Created by user144 on 1/8/2018.
 */

@SuppressLint("Registered")
public class DriverProfile_New extends ActivityHockeyApp implements View.OnClickListener {

    private ImageView Iv_back;
    private ImageView Iv_vehicle;
    private CircularImageView Iv_driverProfile;
    private TextView Et_name, Et_email, Et_mobile, /*Et_mobile_code,*/
            Et_relationship, Et_homeaddress;
    private ExpandableHeightListView Lv_profile_detail, Lv_driver_info_detail, Lv_bank_listView;
    // private Button Btn_editSos;
    private RelativeLayout RL_titlePartnerProfile, Rl_editdetail_bank, Rl_bankinfo_layout, RL_titleLoginInfo, RL_titleSos;
    private FrameLayout FL_partnerProfile, FL_LoginInfo, bankinfo_rl;
    private RelativeLayout RL_sos;
    //  private RatingBar Rb_driverRatting;
    private TextView profileEdit, LoginEdit, SosEdit, bankinfo_textview;

    private ConnectionDetector cd;
    private SessionManager sessionManager;
    private ServiceRequest mRequest;
    private Dialog dialog;
    private ProfileAdapter profile_adapter, driver_info_adapter, bankInfoAdapter;

    private String sDriveID = "", sEmergencyName = "", sEmergencyEmail = "", sEmergencyMobileCode = "", sEmergencyMobile = "", sEmergencyRelationship = "", sEmergencyHomeAddress = "", sEmergencyName1 = "", sEmergencyEmail1 = "", sEmergencyMobileCode1 = "", sEmergencyMobile1 = "", sEmergencyRelationship1 = "", sEmergencyHomeAddress1 = "";

    private ArrayList<ProfilePojo> driver_info_itemList;
    private ArrayList<ProfilePojo> profile_itemList;
    private ArrayList<ProfilePojo> bank_itemList;

    private boolean isInternetPresent = false, sosAvailable = false, sosAvailable1 = false, isDataAvailable = false;

    private CustomTextView Tv_sos_Name1, Tv_sos_mobileno1, Tv_sos_email1, Tv_sos_relationship1, Tv_sos_address1, Tv_SOS_Contact1_Add_Remove, Tv_SOS_Contact2_Remove;
    private RelativeLayout Rl_sos_contact_1_edit, Rl_SOS_View, Rl_sos_contact_2_edit;

    RelativeLayout User_Sos_Update_btn, Rl_SOS_Contact_2, Rl_SOS_Contact_1;
    private boolean sos_contact1_stats = false;
    private boolean sos_contact2_stats = false;
    String key_satus = "";

    private String Dialog_shows_sos_city = "", Dialog_shows_sos_state = "", Dialog_shows_sos_country = "", Dialog_shows_sos_pincode = "";
    private String Dialog_shows_sos_city1 = "", Dialog_shows_sos_state1 = "", Dialog_shows_sos_country1 = "", Dialog_shows_sos_pincode1 = "";
    BroadcastReceiver updateReceiver;
    private RelativeLayout Rl_LoginInfo_Edit;


    String Semail = "", Spasscode = "", sCountryCode = "", SmobNo = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_profilepage);

        initialize();

    }

    private void initialize() {

        sessionManager = new SessionManager(DriverProfile_New.this);
        cd = new ConnectionDetector(DriverProfile_New.this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriveID = user.get(SessionManager.KEY_DRIVERID);
        driver_info_itemList = new ArrayList<ProfilePojo>();
        profile_itemList = new ArrayList<ProfilePojo>();
        bank_itemList = new ArrayList<ProfilePojo>();


        Iv_back = (ImageView) findViewById(R.id.img_back);
        Iv_back.setOnClickListener(this);

        Et_name = (TextView) findViewById(R.id.edt_paypal_id);
        Et_email = (TextView) findViewById(R.id.edt_authorize_net);
        //Et_mobile_code = (TextView) findViewById(R.id.edt_stripe_id);
        Et_mobile = (TextView) findViewById(R.id.edt_mob_no);
        Et_relationship = (TextView) findViewById(R.id.edt_relationship);
        Et_homeaddress = (TextView) findViewById(R.id.edt_address);

        Iv_driverProfile = (CircularImageView) findViewById(R.id.profile_driver_imageview);
        Iv_vehicle = (ImageView) findViewById(R.id.Riv_car);
        Lv_profile_detail = (ExpandableHeightListView) findViewById(R.id.lst_vehicle_details);
        Lv_profile_detail.setExpanded(true);
        Lv_driver_info_detail = (ExpandableHeightListView) findViewById(R.id.profile_driver_info_listView);
        Lv_driver_info_detail.setExpanded(true);
        //Btn_editSos = (Button) findViewById(R.id.btn_edit_sos);
        //  Btn_editSos.setOnClickListener(this);
        RL_titlePartnerProfile = (RelativeLayout) findViewById(R.id.Rl_title_basic_info);
        RL_titlePartnerProfile.setOnClickListener(this);
        RL_titleLoginInfo = (RelativeLayout) findViewById(R.id.Rl_title_driver_info);
        RL_titleLoginInfo.setOnClickListener(this);
        RL_titleSos = (RelativeLayout) findViewById(R.id.Rl_title_payment_gateway);
        RL_titleSos.setOnClickListener(this);

        RL_sos = (RelativeLayout) findViewById(R.id.Rl_payment_gateway);

        FL_partnerProfile = (FrameLayout) findViewById(R.id.Rl_basic_info);
        FL_LoginInfo = (FrameLayout) findViewById(R.id.Rl_driver_info);
        bankinfo_rl = (FrameLayout) findViewById(R.id.bankinfo_rl);


        profileEdit = (TextView) findViewById(R.id.txt_label_partner_profile);
        LoginEdit = (TextView) findViewById(R.id.txt_label_driver_info);
        SosEdit = (TextView) findViewById(R.id.txt_label_payment_gateway);
        Rl_LoginInfo_Edit = (RelativeLayout) findViewById(R.id.login_info_edit_layout);


        Rl_bankinfo_layout = (RelativeLayout) findViewById(R.id.bankinfo_layout);
        Rl_bankinfo_layout.setOnClickListener(this);
        bankinfo_textview = (TextView) findViewById(R.id.bankinfo_textview);
        Rl_editdetail_bank = (RelativeLayout) findViewById(R.id.editdetail_bank);

        Lv_bank_listView = (ExpandableHeightListView) findViewById(R.id.profile_driver_bank_listView);
        Lv_bank_listView.setExpanded(true);
        Rl_editdetail_bank.setOnClickListener(this);
        Tv_sos_Name1 = (CustomTextView) findViewById(R.id.profile_sos_contact_2_name_textview);
        Tv_sos_email1 = (CustomTextView) findViewById(R.id.profile_sos_contact_2_email_textview);
        Tv_sos_mobileno1 = (CustomTextView) findViewById(R.id.profile_sos_contact_2_mobile_no_textview);
        Tv_sos_relationship1 = (CustomTextView) findViewById(R.id.profile_sos_contact_2_relationship_textview);
        Tv_sos_address1 = (CustomTextView) findViewById(R.id.profile_sos_contact_2_address_textview);


        Rl_sos_contact_1_edit = (RelativeLayout) findViewById(R.id.sos_contact_1_edit_layout);
        Rl_sos_contact_2_edit = (RelativeLayout) findViewById(R.id.sos_contact_2_edit_layout);
        Tv_SOS_Contact1_Add_Remove = (CustomTextView) findViewById(R.id.sos_add_or_remove_textview);
        Tv_SOS_Contact2_Remove = (CustomTextView) findViewById(R.id.sos_contact_2_remove_textview);


        Rl_SOS_Contact_2 = (RelativeLayout) findViewById(R.id.profile_sos_contact_2_layout);
        Rl_SOS_Contact_1 = (RelativeLayout) findViewById(R.id.sos_contact_1_layout);


        Rl_SOS_View = (RelativeLayout) findViewById(R.id.sos_view);
        //Rb_driverRatting = (RatingBar) findViewById(R.id.rb_driver_rating);
        // Receiving the data from broadcast
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.app.PaymentListPage.refreshPage");
        updateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent != null) {
                    Refresh_page();
                }
            }
        };
        registerReceiver(updateReceiver, filter);
        if (cd.isConnectingToInternet()) {

            HashMap<String, String> jsonParams = new HashMap<>();
            jsonParams.put("driver_id", sDriveID);

            profile_displayRequest(Iconstant.Profile_driver_profile_Url, jsonParams);
        } else {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
        }
        OtherClickListener();



    }


    @Override
    public void onClick(View view) {
        if (cd.isConnectingToInternet()) {
            if (view == Iv_back) {
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            } else if (view == RL_titlePartnerProfile) {
                LaypoutHandle(1);
                if (FL_partnerProfile.getVisibility() == View.VISIBLE) {
                    profileEdit.setText("PROFILE DETAILS");
                    collapse(FL_partnerProfile);
                } else {
                    profileEdit.setText("MY PROFILE");
                    expand(FL_partnerProfile);
                }
            } else if (view == RL_titleLoginInfo) {
                LaypoutHandle(2);
                if (FL_LoginInfo.getVisibility() == View.VISIBLE) {
                    LoginEdit.setText("LOGIN INFORMATION");
                    collapse(FL_LoginInfo);
                } else {
                    LoginEdit.setText("LOGIN DETAILS");
                    expand(FL_LoginInfo);
                }
            } else if (view == RL_titleSos) {
                LaypoutHandle(4);
                if (RL_sos.getVisibility() == View.VISIBLE) {
                    SosEdit.setText("S.O.S CONTACT");
                    collapse(RL_sos);
                } else {
                    SosEdit.setText("S.O.S INFORMATION");
                    expand(RL_sos);
                }
            } else if (view == Rl_editdetail_bank) {

                ChooseBankCard();
            } else if (view == Rl_bankinfo_layout) {
                LaypoutHandle(3);
                if (bankinfo_rl.getVisibility() == View.VISIBLE) {
                    bankinfo_textview.setText("BANK AND CARD");
                    collapse(bankinfo_rl);

                } else {
                    bankinfo_textview.setText("BANK INFORMATION");
                    expand(bankinfo_rl);

                }
            }
        } else {
            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }
    }

    public void Refresh_page() {

        if (cd.isConnectingToInternet()) {

            HashMap<String, String> jsonParams = new HashMap<>();
            jsonParams.put("driver_id", sDriveID);

            profile_displayRequest(Iconstant.Profile_driver_profile_Url, jsonParams);
        } else {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
        }

    }



    private void PostRequestnewpincode(String Url,String oldpin,String newpin) {
        final Dialog dialog = new Dialog(DriverProfile_New.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> user = sessionManager.getUserDetails();
        String sDriverID = user.get(SessionManager.KEY_DRIVERID);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        jsonParams.put("pin", oldpin);
        jsonParams.put("new_pin", newpin);

        ServiceRequest mRequest = new ServiceRequest(DriverProfile_New.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------Profilepage getdata reponse-------------------" + response);

                dialog.dismiss();
                String Sstatus = "", key1_sos_name = "", key1_sos_email = "", key1_sos_mobileno = "", key1_sos_relationship = "", key1_sos_address = "",
                        key2_sos_name = "", key2_sos_email = "", key2_sos_mobileno = "", key2_sos_relationship = "", key2_sos_address = "";

                try {


                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    response = object.getString("response");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        Alert(getResources().getString(R.string.action_success), response);
                    } else {
                        Alert(getResources().getString(R.string.action_error), response);
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();

                }



            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    private void ChooseBankCard() {
        final Dialog dialog = new Dialog(DriverProfile_New.this, R.style.DialogAnimation);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.otp_check_popup);

        final Button confirm = (Button) dialog.findViewById(R.id.confirm_button);
        final Button cancel = (Button) dialog.findViewById(R.id.cancel_btn);
        final EditText insetPicode_Edt = (EditText) dialog.findViewById(R.id.insert_code);
        final SmoothProgressBar smoothBar = (SmoothProgressBar) dialog.findViewById(R.id.smooth_progressbar);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (insetPicode_Edt.getText().toString().trim().equalsIgnoreCase(sessionManager.getSecurePin())) {
                    Intent intent = new Intent(DriverProfile_New.this, NewProfile_Page_BankEdit.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.exit, R.anim.enter);
                } else {
                    Alert(getResources().getString(R.string.timer_label_alert_sorry), "Enter Valid Pincode");
                }
                dialog.dismiss();
            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(DriverProfile_New.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void LaypoutHandle(int flag) {
        if (flag == 1) {
            if (FL_LoginInfo.getVisibility() == View.VISIBLE)
                collapse(FL_LoginInfo);
            if (bankinfo_rl.getVisibility() == View.VISIBLE)
                collapse(bankinfo_rl);
            if (RL_sos.getVisibility() == View.VISIBLE)
                collapse(RL_sos);
        } else if (flag == 2) {
            if (FL_partnerProfile.getVisibility() == View.VISIBLE) {
                collapse(FL_partnerProfile);
                profileEdit.setText("MY PROFILE");
            }
            if (bankinfo_rl.getVisibility() == View.VISIBLE)
                collapse(bankinfo_rl);
            if (RL_sos.getVisibility() == View.VISIBLE)
                collapse(RL_sos);
        } else if (flag == 3) {
            if (FL_partnerProfile.getVisibility() == View.VISIBLE) {
                collapse(FL_partnerProfile);
                profileEdit.setText("MY PROFILE");
            }

            if (FL_LoginInfo.getVisibility() == View.VISIBLE)
                collapse(FL_LoginInfo);

            if (RL_sos.getVisibility() == View.VISIBLE)
                collapse(RL_sos);
        } else if (flag == 4) {

            if (FL_partnerProfile.getVisibility() == View.VISIBLE) {
                collapse(FL_partnerProfile);
                profileEdit.setText("MY PROFILE");
            }

            if (FL_LoginInfo.getVisibility() == View.VISIBLE)
                collapse(FL_LoginInfo);
            if (bankinfo_rl.getVisibility() == View.VISIBLE)
                collapse(bankinfo_rl);
        }

    }

    //-----------------------Driver Profile Display Post Request-----------------
    private void profile_displayRequest(String Url, final HashMap<String, String> jsonParams) {
        dialog = new Dialog(DriverProfile_New.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_processing));

        System.out.println("-------------Driver Profile Url----------------" + Url);
        System.out.println("-------------Driver Profile jsonParams----------------" + jsonParams);

        mRequest = new ServiceRequest(DriverProfile_New.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------Driver Profile Response----------------" + response);
                String sStatus = "", sDriverProfile = "", sDriverRating = "", sVehicleImage = "";
                dialog.dismiss();
                try {
                    JSONObject object = new JSONObject(response);
                    sStatus = object.getString("status");
                    if (sStatus.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        if (response_object.length() > 0) {
                            Object check_driver_profile_object = response_object.get("driver_profile");
                            if (check_driver_profile_object instanceof JSONObject) {
                                JSONObject driver_profile_object = response_object.getJSONObject("driver_profile");
                                sDriverProfile = driver_profile_object.getString("driver_image");
                                sVehicleImage = driver_profile_object.getString("vehicle_image");
                                sDriverRating = driver_profile_object.getString("driver_review");
                                Semail = driver_profile_object.getString("email");
                                Spasscode = driver_profile_object.getString("pass_code");
                                sCountryCode = driver_profile_object.getString("country_code");
                                SmobNo = driver_profile_object.getString("mobile_number");
                            }
                            Object check_profile_object = response_object.get("profile");
                            if (check_profile_object instanceof JSONArray) {
                                JSONArray profile_array = response_object.getJSONArray("profile");
                                if (profile_array.length() > 0) {
                                    profile_itemList.clear();
                                    for (int i = 0; i < profile_array.length(); i++) {
                                        JSONObject profile_object = profile_array.getJSONObject(i);
                                        ProfilePojo stdrate_pojo = new ProfilePojo();
                                        stdrate_pojo.setTitile(profile_object.getString("title"));
                                        stdrate_pojo.setValue(profile_object.getString("value"));
                                        stdrate_pojo.setEditable("0");
                                        profile_itemList.add(stdrate_pojo);
                                    }
                                }
                            }
                            Object check_bank_object = response_object.get("bank_info");
                            if (check_bank_object instanceof JSONArray) {
                                JSONArray bank_array = response_object.getJSONArray("bank_info");
                                if (bank_array.length() > 0) {
                                    bank_itemList.clear();
                                    for (int i = 0; i < bank_array.length(); i++) {
                                        JSONObject bankObj_object = bank_array.getJSONObject(i);
                                        ProfilePojo bankPojo = new ProfilePojo();
                                        bankPojo.setTitile(bankObj_object.getString("title"));
                                        bankPojo.setValue(bankObj_object.getString("value"));
                                        bankPojo.setEditable("0");
                                        bank_itemList.add(bankPojo);
                                    }
                                }
                            }
                            Object check_driver_info_object = response_object.get("driver_info");
                            if (check_driver_info_object instanceof JSONArray) {

                                JSONArray driver_info_array = response_object.getJSONArray("driver_info");
                                if (driver_info_array.length() > 0) {
                                    driver_info_itemList.clear();
                                    for (int j = 0; j < driver_info_array.length(); j++) {
                                        JSONObject driver_info_object = driver_info_array.getJSONObject(j);
                                        ProfilePojo rate_pojo = new ProfilePojo();
                                        rate_pojo.setTitile(driver_info_object.getString("title"));
                                        rate_pojo.setValue(driver_info_object.getString("value"));
                                        rate_pojo.setEditable("0");
                                        driver_info_itemList.add(rate_pojo);
                                    }
                                }
                            }

                            Object check_emergency_contact_object = response_object.get("emergency_contact");

                            if (check_emergency_contact_object instanceof JSONObject) {
                                JSONObject emergency_contact_object = response_object.getJSONObject("emergency_contact");


                                if (emergency_contact_object.length() > 0) {

                                    if (emergency_contact_object.has("key_1")) {
                                        sos_contact1_stats = true;
                                        JSONObject emergency_contactkey_1_jsonObject = emergency_contact_object.getJSONObject("key_1");
                                        if (emergency_contactkey_1_jsonObject.length() > 0) {
                                            sEmergencyName = emergency_contactkey_1_jsonObject.getString("name");
                                            sEmergencyEmail = emergency_contactkey_1_jsonObject.getString("email");
                                            sEmergencyMobileCode = emergency_contactkey_1_jsonObject.getString("mobile_code");
                                            sEmergencyMobile = emergency_contactkey_1_jsonObject.getString("mobile");
                                            sEmergencyRelationship = emergency_contactkey_1_jsonObject.getString("relationship");
                                            sEmergencyHomeAddress = emergency_contactkey_1_jsonObject.getString("home_address");

                                           /* Dialog_shows_sos_city = emergency_contactkey_1_jsonObject.getString("city");
                                            Dialog_shows_sos_state = emergency_contactkey_1_jsonObject.getString("state");
                                            Dialog_shows_sos_pincode = emergency_contactkey_1_jsonObject.getString("postcode");
                                            Dialog_shows_sos_country = emergency_contactkey_1_jsonObject.getString("country");
*/
                                            sosAvailable = true;
                                        }
                                        if (emergency_contact_object.has("key_2")) {
                                            JSONObject emergency_contactkey_2_jsonObject = emergency_contact_object.getJSONObject("key_2");
                                            if (emergency_contactkey_2_jsonObject.length() > 0) {

                                                sEmergencyName1 = emergency_contactkey_2_jsonObject.getString("name");
                                                sEmergencyEmail1 = emergency_contactkey_2_jsonObject.getString("email");
                                                sEmergencyMobileCode1 = emergency_contactkey_2_jsonObject.getString("mobile_code");
                                                sEmergencyMobile1 = emergency_contactkey_2_jsonObject.getString("mobile");
                                                sEmergencyRelationship1 = emergency_contactkey_2_jsonObject.getString("relationship");
                                                sEmergencyHomeAddress1 = emergency_contactkey_2_jsonObject.getString("home_address");

                                                  /*  Dialog_shows_sos_city1 = emergency_contactkey_2_jsonObject.getString("city");
                                                    Dialog_shows_sos_state1 = emergency_contactkey_2_jsonObject.getString("state");
                                                    Dialog_shows_sos_pincode1 = emergency_contactkey_2_jsonObject.getString("postcode");
                                                    Dialog_shows_sos_country1 = emergency_contactkey_2_jsonObject.getString("country");
*/

                                                sosAvailable1 = true;
                                                sos_contact2_stats = true;
                                            }

                                        }
                                    }
                                }

                            }


                            isDataAvailable = true;

                        } else {
                            isDataAvailable = false;
                        }
                    } else {
                        isDataAvailable = false;
                    }


                    if (sStatus.equalsIgnoreCase("1") && isDataAvailable) {


                        if (sDriverProfile.length() > 0) {
                            Picasso.with(DriverProfile_New.this)
                                    .load(sDriverProfile).resize(100, 100)
                                    .into(Iv_driverProfile);
                        }

                        if (sVehicleImage.length() > 0) {
                            Picasso.with(DriverProfile_New.this)
                                    .load(sVehicleImage).resize(100, 100)
                                    .into(Iv_vehicle);
                        }


                        if (profile_itemList.size() > 0) {
                            profile_adapter = new ProfileAdapter(DriverProfile_New.this, profile_itemList);
                            Lv_profile_detail.setAdapter(profile_adapter);
                        }

                        if (bank_itemList.size() > 0) {
                            bankInfoAdapter = new ProfileAdapter(DriverProfile_New.this, bank_itemList);
                            Lv_bank_listView.setAdapter(bankInfoAdapter);
                        }

                        if (driver_info_itemList.size() > 0) {
                            driver_info_adapter = new ProfileAdapter(DriverProfile_New.this, driver_info_itemList);
                            Lv_driver_info_detail.setAdapter(driver_info_adapter);
                            expand(FL_LoginInfo);
                        }


                        if (sos_contact1_stats) {
                            Tv_SOS_Contact1_Add_Remove.setVisibility(View.VISIBLE);
                            Et_name.setText(":" + sEmergencyName);
                            Et_email.setText(":" + sEmergencyEmail);
                            //Et_mobile_code.setText(sEmergencyMobileCode);
                            Et_mobile.setText(":" + sEmergencyMobileCode + " " + sEmergencyMobile);
                            Et_relationship.setText(":" + sEmergencyRelationship);
                            Et_homeaddress.setText(":" + sEmergencyHomeAddress);
                        } else {
                            Tv_SOS_Contact1_Add_Remove.setVisibility(View.GONE);
                        }
                        if (sos_contact2_stats) {

                            Rl_SOS_Contact_2.setVisibility(View.VISIBLE);
                            Tv_SOS_Contact1_Add_Remove.setText("-remove");
                            Rl_SOS_View.setVisibility(View.VISIBLE);

                            Tv_sos_Name1.setText(":" + sEmergencyName1);
                            Tv_sos_email1.setText(":" + sEmergencyEmail1);
                            Tv_sos_mobileno1.setText(":" + sEmergencyMobileCode1 + " " + sEmergencyMobile1);
                            Tv_sos_relationship1.setText(":" + sEmergencyRelationship1);
                            Tv_sos_address1.setText(":" + sEmergencyHomeAddress1);

                        } else {
                            Rl_SOS_Contact_2.setVisibility(View.GONE);
                            Rl_SOS_View.setVisibility(View.GONE);
                        }


                        if (sDriverRating.length() > 0) {
                            //Rb_driverRatting.setRating(Float.parseFloat(sDriverRating));
                        }


                    } else {
                        String Sresponse = object.getString("response");
                        Alert(getResources().getString(R.string.alert_label_title), Sresponse);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }


    private void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    //-------------------------code to Check Email Validation-----------------------
    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    //--------------------Code to set error for EditText-----------------------
    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(DriverProfile_New.this, R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }

    // validating Phone Number
    public static final boolean isValidPhoneNumber(CharSequence target) {
        if (target == null || TextUtils.isEmpty(target) || target.length() <= 9) {
            return false;
        } else {
            return android.util.Patterns.PHONE.matcher(target).matches();
        }
    }


    private void OtherClickListener() {

        Rl_LoginInfo_Edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(DriverProfile_New.this, NewProfile_Page_LoginEdit.class);
                in.putExtra("countrycode", sCountryCode);
                in.putExtra("email", Semail);
                in.putExtra("mobilenumber", SmobNo);
                in.putExtra("pincode", Spasscode);
                startActivity(in);
                overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });


        Rl_sos_contact_1_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(DriverProfile_New.this, NewProfile_Page_SosEdit.class);
                in.putExtra("name", Et_name.getText().toString().replace(":", ""));
                in.putExtra("email", Et_email.getText().toString().replace(":", ""));
                in.putExtra("mobile", sEmergencyMobile);
                in.putExtra("relatuion", Et_relationship.getText().toString().trim().replace(":", ""));
                in.putExtra("address", Et_homeaddress.getText().toString().trim().replace(":", ""));
                in.putExtra("city", Dialog_shows_sos_city);
                in.putExtra("state", Dialog_shows_sos_state);
                in.putExtra("picode", Dialog_shows_sos_pincode);
                in.putExtra("country", Dialog_shows_sos_country);

                in.putExtra("countryCode", sCountryCode);
                in.putExtra("soscount", "1");
                startActivity(in);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        Rl_sos_contact_2_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(DriverProfile_New.this, NewProfile_Page_SosEdit.class);
                in.putExtra("name", Tv_sos_Name1.getText().toString().replace(":", ""));
                in.putExtra("email", Tv_sos_email1.getText().toString().replace(":", ""));
                in.putExtra("mobile", sEmergencyMobile1);
                in.putExtra("relatuion", Tv_sos_relationship1.getText().toString().trim().replace(":", ""));
                in.putExtra("address", Tv_sos_address1.getText().toString().trim().replace(":", ""));
                in.putExtra("city", Dialog_shows_sos_city1);
                in.putExtra("state", Dialog_shows_sos_state1);
                in.putExtra("picode", Dialog_shows_sos_pincode1);
                in.putExtra("country", Dialog_shows_sos_country1);

                in.putExtra("countryCode", sCountryCode);
                in.putExtra("soscount", "2");
                startActivity(in);
                overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });
        Tv_SOS_Contact1_Add_Remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Tv_SOS_Contact1_Add_Remove.getText().toString().toLowerCase().equalsIgnoreCase("ADD SOS CONTACT")) {

                    Tv_SOS_Contact1_Add_Remove.setText("-remove");

                    Rl_SOS_Contact_2.setVisibility(View.VISIBLE);
                    Rl_SOS_View.setVisibility(View.VISIBLE);

                    Intent in = new Intent(DriverProfile_New.this, NewProfile_Page_SosEdit.class);
                    in.putExtra("name", "");
                    in.putExtra("email", "");
                    in.putExtra("mobile", "");
                    in.putExtra("relatuion", "");
                    in.putExtra("address", "");
                    in.putExtra("city", "");
                    in.putExtra("state", "");
                    in.putExtra("picode", "");
                    in.putExtra("country", "");

                    in.putExtra("countryCode", sCountryCode);
                    in.putExtra("soscount", "2");
                    startActivity(in);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                } else if (Tv_SOS_Contact1_Add_Remove.getText().toString().toLowerCase().equalsIgnoreCase("-remove")) {

                    if (!Et_name.getText().toString().equalsIgnoreCase("n/a")) {

                        removeContact("key_1");

                    } else {

                        if (!Tv_sos_Name1.getText().toString().equalsIgnoreCase("n/a")) {
                            Et_name.setText(":" + Tv_sos_Name1.getText().toString());
                            Et_email.setText(":" + Tv_sos_email1.getText().toString());
                            Et_mobile.setText(":" + Tv_sos_mobileno1.getText().toString());
                            Et_relationship.setText(":" + Tv_sos_relationship1.getText().toString());
                            Et_homeaddress.setText(":" + Tv_sos_address1.getText().toString());

                            Tv_SOS_Contact1_Add_Remove.setText("ADD SOS CONTACT");
                            Rl_SOS_Contact_2.setVisibility(View.GONE);
                            Rl_SOS_View.setVisibility(View.GONE);

                            Tv_sos_Name1.setText("n/a");
                            Tv_sos_email1.setText("n/a");
                            Tv_sos_mobileno1.setText("n/a");
                            Tv_sos_relationship1.setText("n/a");
                            Tv_sos_address1.setText("n/a");

                        } else {
                            Et_name.setText("n/a");
                            Et_email.setText("n/a");
                            Et_mobile.setText("n/a");
                            Et_relationship.setText("n/a");
                            Et_homeaddress.setText("n/a");

                            Tv_SOS_Contact1_Add_Remove.setText("ADD SOS CONTACT");
                            Rl_SOS_Contact_2.setVisibility(View.GONE);
                            Rl_SOS_View.setVisibility(View.GONE);
                        }

                    }


                }

            }
        });


        Tv_SOS_Contact2_Remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Tv_SOS_Contact2_Remove.getText().toString().toLowerCase().equalsIgnoreCase("-remove")) {
                    if (!Tv_sos_Name1.getText().toString().toLowerCase().equalsIgnoreCase("n/a")) {
                        removeContact("key_2");
                    } else {
                        Tv_SOS_Contact1_Add_Remove.setText("ADD SOS CONTACT");
                        Rl_SOS_Contact_2.setVisibility(View.GONE);
                        Tv_sos_Name1.setText("n/a");
                        Tv_sos_email1.setText("n/a");
                        Tv_sos_mobileno1.setText("n/a");
                        Tv_sos_relationship1.setText("n/a");
                        Tv_sos_address1.setText("n/a");
                        if (!Et_name.getText().toString().toLowerCase().equalsIgnoreCase("n/a")) {
                            Tv_SOS_Contact1_Add_Remove.setVisibility(View.VISIBLE);
                        } else {
                            Tv_SOS_Contact1_Add_Remove.setVisibility(View.GONE);
                        }
                    }
                }
            }
        });
    }


    public void removeContact(String index) {
        cd = new ConnectionDetector(DriverProfile_New.this);
        isInternetPresent = cd.isConnectingToInternet();

        if (isInternetPresent) {
            PostRequest_removeSos(Iconstant.profile_sos_remove_url, index);
        } else {
            Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
        }
    }

    private void PostRequest_removeSos(String Url, String index) {

        key_satus = index;
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriveID);
        jsonParams.put("edit_index", index);
        System.out.println("-----------PostRequest_removeSos jsonParams--------------" + jsonParams);

        System.out.println("-----------PostRequest_removeSos index------------" + index);

        mRequest = new ServiceRequest(DriverProfile_New.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                Log.e("registr", response);

                System.out.println("--------------PostRequest_removeSos reponse-------------------" + response);

                String Sstatus = "", Smessage = "", Sotp_status = "", Sotp = "";

                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    Smessage = object.getString("response");

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    dialogDismiss();
                }

                if (Sstatus.equalsIgnoreCase("1")) {

                    if (key_satus.equalsIgnoreCase("key_1")) {

                        Et_name.setText(Tv_sos_Name1.getText().toString());
                        Et_email.setText(Tv_sos_email1.getText().toString());
                        Et_mobile.setText(Tv_sos_mobileno1.getText().toString());
                        Et_relationship.setText(Tv_sos_relationship1.getText().toString());
                        Et_homeaddress.setText(Tv_sos_address1.getText().toString());

                        Tv_SOS_Contact1_Add_Remove.setText("ADD SOS CONTACT");
                        Rl_SOS_Contact_2.setVisibility(View.GONE);
                        Rl_SOS_View.setVisibility(View.GONE);

                        Tv_sos_Name1.setText("n/a");
                        Tv_sos_email1.setText("n/a");
                        Tv_sos_mobileno1.setText("n/a");
                        Tv_sos_relationship1.setText("n/a");
                        Tv_sos_address1.setText("n/a");

                        if (!Tv_sos_Name1.getText().toString().toLowerCase().equalsIgnoreCase("n/a")) {
                            Tv_SOS_Contact1_Add_Remove.setVisibility(View.VISIBLE);
                        } else {
                            Tv_SOS_Contact1_Add_Remove.setVisibility(View.GONE);
                        }

                    } else {

                        Tv_SOS_Contact1_Add_Remove.setText("ADD SOS CONTACT");
                        Rl_SOS_Contact_2.setVisibility(View.GONE);

                        Tv_sos_Name1.setText("n/a");
                        Tv_sos_email1.setText("n/a");
                        Tv_sos_mobileno1.setText("n/a");
                        Tv_sos_relationship1.setText("n/a");
                        Tv_sos_address1.setText("n/a");

                        if (!Et_name.getText().toString().toLowerCase().equalsIgnoreCase("n/a")) {
                            Tv_SOS_Contact1_Add_Remove.setVisibility(View.VISIBLE);
                        } else {
                            Tv_SOS_Contact1_Add_Remove.setVisibility(View.GONE);
                        }
                    }
                } else {
                    Alert(getResources().getString(R.string.action_error), Smessage);
                }
                dialogDismiss();
            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (updateReceiver != null) {
            unregisterReceiver(updateReceiver);
            updateReceiver = null;
        }
    }
}

