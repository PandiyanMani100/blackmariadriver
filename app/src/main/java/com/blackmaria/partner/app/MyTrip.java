package com.blackmaria.partner.app;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.Pojo.MyTripChartPojo;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.latestpackageview.widgets.RoundedImageView;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Prem Kumar and Anitha on 12/17/2016.
 */
public class MyTrip extends ActivityHockeyApp implements View.OnClickListener {

    //  private SpeedView Sv_todayTrip, Sv_thisWeek, Sv_thisMonth;
    private TextView Tv_totalTripTodayCount, Tv_totalTripWeekCount, Tv_totalTripMonthCount, Tv_totalTripTodayDistance, Tv_totalTripWeekDistance, Tv_totalTripMonthDistance;
    private TextView Tv_todayHighlight_engineStart, Tv_todayHighlight_lastTrip, Tv_todayHighlight_CancelTrip, Tv_todayHighlight_totalTrip, Tv_todayHighlight_sosTrip, Tv_todayHighlight_complaint;
    private TextView Tv_pastTrip_month, Tv_pastTrip_date, Tv_pastTrip_day;
    private TextView Tv_topTrip_driverName, Tv_topTrip_driverTotalTrip, Tv_topTrip_carType, Tv_topTrip_cityLocation;
    private TextView Tv_highestTripCurrency, Tv_highestTripAmount;
    private RoundedImageView Iv_topTrip_driverImage;
    private ImageView Iv_topTrip_brand;
    private RatingBar Rb_topTrip_driverRating;
    private ImageView Tv_back, Iv_light;
    private RelativeLayout Ll_totalTrip, Ll_weekTrip, Ll_monthTrip;
    private RelativeLayout Rl_topTripAvailable, Rl_topTripEmpty;
    private BarChart barChart;

    private ConnectionDetector cd;
    private boolean isInternetPresent = false;
    private SessionManager sessionManager;
    private String sDriveID = "";

    private boolean isTopTripAvailable = false, isPerformanceAvailable = false;

    private ServiceRequest mRequest;
    Dialog dialog;
    private ArrayList<MyTripChartPojo> chartList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mytrip_page_new);
        initialize();
    }

    private void initialize() {

        sessionManager = new SessionManager(MyTrip.this);
        cd = new ConnectionDetector(MyTrip.this);
        isInternetPresent = cd.isConnectingToInternet();
        chartList = new ArrayList<MyTripChartPojo>();

       /* Sv_todayTrip = (SpeedView) findViewById(R.id.myTrip_total_trip_speedView);
        Sv_thisWeek = (SpeedView) findViewById(R.id.myTrip_thisWeek_trip_speedView);
        Sv_thisMonth = (SpeedView) findViewById(R.id.myTrip_thisMonth_trip_speedView);*/

        Tv_totalTripTodayCount = (TextView) findViewById(R.id.myTrip_totalTrip_today_textView);
        Tv_totalTripWeekCount = (TextView) findViewById(R.id.myTrip_totalTrip_thisWeek_textView);
        Tv_totalTripMonthCount = (TextView) findViewById(R.id.myTrip_totalTrip_thisMonth_textView);
        Tv_totalTripTodayDistance = (TextView) findViewById(R.id.myTrip_totalTrip_today_km_textView);
        Tv_totalTripWeekDistance = (TextView) findViewById(R.id.myTrip_totalTrip_thisWeek_km_textView);
        Tv_totalTripMonthDistance = (TextView) findViewById(R.id.myTrip_totalTrip_thisMonth_km_textView);

        //    Tv_todayHighlight_engineStart = (TextView) findViewById(R.id.myTrip_today_highlight_content_startEngine_TextView);
        //   Tv_todayHighlight_lastTrip = (TextView) findViewById(R.id.myTrip_today_highlight_content_lastTrip_TextView);
        Tv_todayHighlight_CancelTrip = (TextView) findViewById(R.id.myTrip_today_highlight_cancelTrip_TextView);
        Tv_todayHighlight_totalTrip = (TextView) findViewById(R.id.tv_total_trip_value);
        Tv_todayHighlight_sosTrip = (TextView) findViewById(R.id.myTrip_today_highlight_sosTrip_TextView);
        Tv_todayHighlight_complaint = (TextView) findViewById(R.id.myTrip_today_highlight_complaint_TextView);

        //  Tv_pastTrip_month = (TextView) findViewById(R.id.myTrip_past_trip_month_textView);
        //  Tv_pastTrip_date = (TextView) findViewById(R.id.myTrip_past_trip_date_textView);
        //   Tv_pastTrip_day = (TextView) findViewById(R.id.myTrip_past_trip_week_textView);

        Tv_topTrip_driverName = (TextView) findViewById(R.id.myTrip_trip_topTrip_profile_name_textView);
//        Tv_topTrip_driverTotalTrip = (TextView) findViewById(R.id.myTrip_trip_topTrip_position_textView);
        Tv_topTrip_cityLocation = (TextView) findViewById(R.id.myTrip_trip_topTrip_location_textView);
        //       Iv_topTrip_brand = (ImageView) findViewById(R.id.myTrip_trip_topTrip_carType_imageView);
        Tv_topTrip_carType = (TextView) findViewById(R.id.myTrip_trip_topTrip_carType_textView);
        Iv_topTrip_driverImage = (RoundedImageView) findViewById(R.id.myTrip_trip_topTrip_profile_imageView);
        //       Rb_topTrip_driverRating = (RatingBar) findViewById(R.id.myTrip_trip_topTrip_ratingbar);

        Tv_back = (ImageView) findViewById(R.id.backbtn);
        Iv_light = (ImageView) findViewById(R.id.img_light);
        startBlinkingAnimation();

        Ll_totalTrip = (RelativeLayout) findViewById(R.id.myTrip_trip_meter_totalTrip_linear_layout);
        Ll_weekTrip = (RelativeLayout) findViewById(R.id.myTrip_trip_meter_weekTrip_linear_layout);
        Ll_monthTrip = (RelativeLayout) findViewById(R.id.myTrip_trip_meter_monthTrip_linear_layout);
        Rl_topTripAvailable = (RelativeLayout) findViewById(R.id.myTrip_trip_topTrip_content_layout);
        Rl_topTripEmpty = (RelativeLayout) findViewById(R.id.myTrip_trip_topTrip_content_empty_layout);

        Tv_highestTripCurrency = (TextView) findViewById(R.id.txt_highest_trip_currency);
        Tv_highestTripAmount = (TextView) findViewById(R.id.txt_highest_trip_amount);

        barChart = (BarChart) findViewById(R.id.myTrip_weeklyPerformance_chart);

        Tv_back.setOnClickListener(this);
        Ll_totalTrip.setOnClickListener(this);
        Ll_weekTrip.setOnClickListener(this);
        Ll_monthTrip.setOnClickListener(this);


        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriveID = user.get(SessionManager.KEY_DRIVERID);

        if (isInternetPresent) {
            MyTripRequest(Iconstant.myTrip_Url);
        } else {
            alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }

    }

    @Override
    public void onClick(View view) {

        cd = new ConnectionDetector(MyTrip.this);
        isInternetPresent = cd.isConnectingToInternet();

        if (isInternetPresent) {
            if (view == Tv_back) {
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            } else if (view == Ll_totalTrip) {
                Intent intent = new Intent(MyTrip.this, MyTripToday.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            } else if (view == Ll_weekTrip) {
                Intent intent = new Intent(MyTrip.this, MyTripWeek.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            } else if (view == Ll_monthTrip) {
                Intent intent = new Intent(MyTrip.this, MyTripMonth.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        } else {
            alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }


    }

    private void startBlinkingAnimation() {
        Animation startAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blinking_animation);
        Iv_light.startAnimation(startAnimation);
    }

    private void alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(MyTrip.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    //-------------------My Trip Request----------------
    @SuppressLint("WrongConstant")
    private void MyTripRequest(String Url) {

        dialog = new Dialog(MyTrip.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriveID);

        System.out.println("--------------MyTripRequest Url-------------------" + Url);
        System.out.println("--------------jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(MyTrip.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------response-------------------" + response);
                String status = "", sTodayRideCount = "", sTodayRideDistance = "", sTodayRideLevel = "", sWeekRideCount = "", sWeekRideDistance = "", sWeekRideLevel = "", sMonthRideCount = "", sMonthRideDistance = "", sMonthRideLevel = "",
                        sEngineStart = "", sLastTrip = "", sCancelTrip = "", sCompletedTrip = "", sosTrip = "", sComplaint = "", sPastTripMonth = "", sPastTripDate = "", sPastTripWeek = "", sTopTripDriverImage = "",
                        sTopTripDriverName = "", sTopTripDriverRating = "", sTopTripDriverTotalTrip = "", sTopTripDriverLocation = "", sTopTripDriverBrand = "", sTopTripDriverVehicleModel = "", sHighestTripAmmount = "", sCurrencyCode = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {
                            JSONObject jsonObject = object.getJSONObject("response");
                            if (jsonObject.length() > 0) {

                                JSONObject todayObject = jsonObject.getJSONObject("today");
                                if (todayObject.length() > 0) {
                                    sTodayRideCount = todayObject.getString("rides_count");
                                    sTodayRideDistance = todayObject.getString("rides_distance");
                                    sTodayRideLevel = todayObject.getString("level");
                                }

                                JSONObject weekObject = jsonObject.getJSONObject("week");
                                if (weekObject.length() > 0) {
                                    sWeekRideCount = weekObject.getString("rides_count");
                                    sWeekRideDistance = weekObject.getString("rides_distance");
                                    sWeekRideLevel = weekObject.getString("level");
                                }

                                JSONObject monthObject = jsonObject.getJSONObject("month");
                                if (monthObject.length() > 0) {
                                    sMonthRideCount = monthObject.getString("rides_count");
                                    sMonthRideDistance = monthObject.getString("rides_distance");
                                    sMonthRideLevel = monthObject.getString("level");
                                }

                                /*JSONObject todayHighlightObject = jsonObject.getJSONObject("today_highlight");
                                if (todayHighlightObject.length() > 0) {
                                    sEngineStart = todayHighlightObject.getString("start_engine");
                                    sLastTrip = todayHighlightObject.getString("last_trip");
                                    sCancelTrip = todayHighlightObject.getString("cancel_trip");
                                    sCompletedTrip = todayHighlightObject.getString("completed");
                                    sosTrip = todayHighlightObject.getString("sos_trip");
                                    sComplaint = todayHighlightObject.getString("complaint");
                                    sPastTripMonth = todayHighlightObject.getString("last_trip_m");
                                    sPastTripDate = todayHighlightObject.getString("last_trip_d_txt");
                                    sPastTripWeek = todayHighlightObject.getString("last_trip_d");
                                }*/

                                JSONObject yearHighlightObject = jsonObject.getJSONObject("year_highlight");
                                if (yearHighlightObject.length() > 0) {
                                    sEngineStart = yearHighlightObject.getString("start_engine");
                                    sLastTrip = yearHighlightObject.getString("last_trip");
                                    sCancelTrip = yearHighlightObject.getString("cancel_trip");
                                    sCompletedTrip = yearHighlightObject.getString("completed");
                                    sosTrip = yearHighlightObject.getString("sos_trip");
                                    sComplaint = yearHighlightObject.getString("complaint");
                                    sPastTripMonth = yearHighlightObject.getString("last_trip_m");
                                    sPastTripDate = yearHighlightObject.getString("last_trip_d_txt");
                                    sPastTripWeek = yearHighlightObject.getString("last_trip_d");
                                }

                                JSONObject topTripObject = jsonObject.getJSONObject("top_trip");
                                if (topTripObject.length() > 0) {
                                    sTopTripDriverImage = topTripObject.getString("driver_image");
                                    sTopTripDriverName = topTripObject.getString("driver_name");
                                    sTopTripDriverRating = topTripObject.getString("driver_review");
                                    sTopTripDriverLocation = topTripObject.getString("city_location");
                                    sTopTripDriverBrand = topTripObject.getString("brand_image");
                                    sTopTripDriverTotalTrip = topTripObject.getString("total_trip");
                                    sTopTripDriverVehicleModel = topTripObject.getString("vehicle_model");

                                    isTopTripAvailable = true;

                                } else {
                                    isTopTripAvailable = false;
                                }


                                Object chart_array_check = jsonObject.get("chart_array");
                                if (chart_array_check instanceof JSONArray) {
                                    JSONArray chartArray = jsonObject.getJSONArray("chart_array");
                                    if (chartArray.length() > 0) {
                                        chartList.clear();
                                        for (int i = 0; i < chartArray.length(); i++) {
                                            JSONObject chartArrayObject = chartArray.getJSONObject(i);
                                            MyTripChartPojo pojo = new MyTripChartPojo();
                                            pojo.setDay(chartArrayObject.getString("day"));
                                            pojo.setRideCount(chartArrayObject.getString("ridecount"));
                                            pojo.setDistance(chartArrayObject.getString("distance"));

                                            chartList.add(pojo);
                                        }

                                        isPerformanceAvailable = true;

                                    } else {
                                        chartList.clear();
                                        isPerformanceAvailable = false;
                                    }
                                } else {
                                    chartList.clear();
                                    isPerformanceAvailable = false;
                                }

                                sCurrencyCode = jsonObject.getString("currency");
                                sHighestTripAmmount = jsonObject.getString("highest_trip_amount");


                            }
                        } else {
                            String sResponse = object.getString("response");
                            alert(getResources().getString(R.string.action_error), sResponse);
                        }

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (status.equalsIgnoreCase("1")) {

                    Tv_totalTripTodayCount.setText(sTodayRideCount);
                    Tv_totalTripWeekCount.setText(sWeekRideCount);
                    Tv_totalTripMonthCount.setText(sMonthRideCount);
                    Tv_totalTripTodayDistance.setText(sTodayRideDistance);
                    Tv_totalTripWeekDistance.setText(sWeekRideDistance);
                    Tv_totalTripMonthDistance.setText(sMonthRideDistance);


                    /*if (sTodayRideLevel.length() > 0) {
                        Sv_todayTrip.speedTo(Integer.parseInt(sTodayRideLevel));
                    }

                    if (sWeekRideLevel.length() > 0) {
                        Sv_thisWeek.speedTo(Integer.parseInt(sWeekRideLevel));
                    }

                    if (sMonthRideLevel.length() > 0) {
                        Sv_thisMonth.speedTo(Integer.parseInt(sMonthRideLevel));
                    }*/

                    //         Tv_todayHighlight_engineStart.setText(sEngineStart);
                    //         Tv_todayHighlight_lastTrip.setText(sLastTrip);
                    Tv_todayHighlight_CancelTrip.setText(sCancelTrip);
                    Tv_todayHighlight_totalTrip.setText(sCompletedTrip);
                    Tv_todayHighlight_sosTrip.setText(sosTrip);
                    Tv_todayHighlight_complaint.setText(sComplaint);

                   /* Tv_pastTrip_month.setText(sPastTripMonth);
                    Tv_pastTrip_date.setText(sPastTripDate);
                    Tv_pastTrip_day.setText(sPastTripWeek);*/


                    if (isTopTripAvailable) {

                        Rl_topTripAvailable.setVisibility(View.VISIBLE);
                        Rl_topTripEmpty.setVisibility(View.GONE);

                        Tv_topTrip_driverName.setText(sTopTripDriverName);
                        //                      Tv_topTrip_driverTotalTrip.setText(sTopTripDriverTotalTrip);
                        Tv_topTrip_cityLocation.setText(sTopTripDriverLocation);
                        Tv_topTrip_carType.setText(sTopTripDriverVehicleModel);

                        if (sTopTripDriverImage.length() > 0) {
                            Picasso.with(MyTrip.this).load(sTopTripDriverImage).memoryPolicy(MemoryPolicy.NO_CACHE).resize(150, 150).into(Iv_topTrip_driverImage);
                        }

                       /* if (sTopTripDriverBrand.length() > 0) {
                            Picasso.with(MyTrip.this).load(sTopTripDriverBrand).into(Iv_topTrip_brand);
                        }*/

                       /* if (sTopTripDriverRating.length() > 0) {
                            Rb_topTrip_driverRating.setRating(Float.parseFloat(sTopTripDriverRating));
                        }*/

                    } else {
                        Rl_topTripAvailable.setVisibility(View.GONE);
                        Rl_topTripEmpty.setVisibility(View.VISIBLE);
                    }


                    if (isPerformanceAvailable) {

                        ArrayList<BarEntry> entries = new ArrayList<BarEntry>();
                        ArrayList<String> labels = new ArrayList<String>();

                        for (int h = 0; h < chartList.size(); h++) {
                            entries.add(new BarEntry(Float.parseFloat(chartList.get(h).getRideCount()), h));
                            labels.add(chartList.get(h).getDay());
                        }

                        if (chartList.size() > 0) {
                            BarDataSet dataSet = new BarDataSet(entries, getResources().getString(R.string.myTrip_label_weeklyPerformance));
//                            dataSet.setColor(Color.rgb(110, 240, 130));
                            dataSet.setColor(Color.parseColor("#1F8BCA"));

                            BarData data = new BarData(labels, dataSet);
                            data.setValueTextColor(Color.parseColor("#FFFFFF"));

//                            barChart.setDescriptionColor(Color.parseColor("#FFFFFF"));
                            barChart.setGridBackgroundColor(Color.parseColor("#00000000"));
                            barChart.setBackgroundColor(Color.parseColor("#00000000"));
                            barChart.setData(data);
                            barChart.animateY(5000);
                            barChart.setPinchZoom(false);
                            barChart.getLegend().setTextColor(Color.WHITE);
                            barChart.getXAxis().setTextColor(Color.WHITE);
                            barChart.getAxisLeft().setTextColor(Color.WHITE);
                            barChart.getAxisRight().setTextColor(Color.TRANSPARENT);
                            barChart.setDescription("");
//                            Description description = new Description();
//                            description.setText("");
//                            barChart.setDescription(description);


                            XAxis xAxis = barChart.getXAxis();
                            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
                            xAxis.setDrawAxisLine(true);
                            xAxis.setDrawGridLines(true);
                        }
                    }

                    Tv_highestTripCurrency.setText(sCurrencyCode);
                    if (sHighestTripAmmount.length() > 0) {
                        Tv_highestTripAmount.setText(sHighestTripAmmount);
                    }


                }

                if (dialog != null) {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
            return true;
        }
        return false;
    }


}
