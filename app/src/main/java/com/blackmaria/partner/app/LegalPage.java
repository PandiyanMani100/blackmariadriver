package com.blackmaria.partner.app;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;

import java.util.HashMap;

/**
 * Created by GANESH on 28-08-2017.
 */

public class LegalPage extends ActivityHockeyApp implements View.OnClickListener {

    private TextView Tv_privacyPolicy, Tv_termsOfService, Tv_endUserAgreement;
    private RelativeLayout RL_home;
    TextView RL_facebookPolicy;
    private ImageView Iv_call;

    private ConnectionDetector cd;
    private SessionManager sessionManager;

    private String sDriveID = "", sSupportNumber = "";
    final int PERMISSION_REQUEST_CODE = 111;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.legal_page_new);

        initialize();

    }

    private void initialize() {

        sessionManager = new SessionManager(LegalPage.this);
        cd = new ConnectionDetector(LegalPage.this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriveID = user.get(SessionManager.KEY_DRIVERID);
        sSupportNumber = sessionManager.getContactNumber();

        RL_home = (RelativeLayout) findViewById(R.id.RL_home);
        Tv_privacyPolicy = (TextView) findViewById(R.id.txt_label_privacy_policy);
        Tv_termsOfService = (TextView) findViewById(R.id.txt_label_terms_of_service);
        Tv_endUserAgreement = (TextView) findViewById(R.id.txt_label_end_user_agreement);

        Iv_call = (ImageView) findViewById(R.id.img_call);
        RL_facebookPolicy= (TextView) findViewById(R.id.txt_label_facebook_policy);

        RL_home.setOnClickListener(this);
        Tv_privacyPolicy.setOnClickListener(this);
        Tv_termsOfService.setOnClickListener(this);
        Tv_endUserAgreement.setOnClickListener(this);
        RL_facebookPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LegalPage.this, LegalDynamicContent.class);
                intent.putExtra("pageType", "facebook-policy");
                intent.putExtra("pageTitle", getResources().getString(R.string.legal_page_label_facebook_policy));
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        Iv_call.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
        if (view == RL_home) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (view == Tv_privacyPolicy) {
            Intent intent = new Intent(LegalPage.this, LegalDynamicContent.class);
            intent.putExtra("pageType", "privacy");
            intent.putExtra("pageTitle", getResources().getString(R.string.legal_page_label_privacy_policy));
//            intent.putExtra("pageNo", "1");
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (view == Tv_termsOfService) {
            Intent intent = new Intent(LegalPage.this, LegalDynamicContent.class);
            intent.putExtra("pageType", "termsof-service");
            intent.putExtra("pageTitle", getResources().getString(R.string.legal_page_label_terms_of_service));
//            intent.putExtra("pageNo", "1");
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (view == Tv_endUserAgreement) {
            Intent intent = new Intent(LegalPage.this, LegalDynamicContent.class);
            intent.putExtra("pageType", "user-agreement");
            intent.putExtra("pageTitle", getResources().getString(R.string.legal_page_label_end_user_agreement));
//            intent.putExtra("pageNo", "1");
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }else if (view == Iv_call) {
            getSupportPopup();

        }
    }

    //----------------------------Run time permission for call--------------------------------------------

    private boolean checkCallPhonePermission() {
        int result = ContextCompat.checkSelfPermission(LegalPage.this, android.Manifest.permission.CALL_PHONE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkReadStatePermission() {
        int result = ContextCompat.checkSelfPermission(LegalPage.this, android.Manifest.permission.READ_PHONE_STATE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }


    private void requestPermission() {
        ActivityCompat.requestPermissions(LegalPage.this, new String[]{android.Manifest.permission.CALL_PHONE, android.Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
    }




    private void getSupportPopup() {
        final Dialog dialog = new Dialog(LegalPage.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.legal_popup);

        final Button getSupport = (Button) dialog.findViewById(R.id.getsupport_btn);
        final Button callnow = (Button) dialog.findViewById(R.id.callnow_btn);
        final ImageView CloseImage = (ImageView) dialog.findViewById(R.id.close_image);

        callnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= 23) {
                    if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                        requestPermission();
                    } else {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + sSupportNumber));
                        startActivity(callIntent);
                    }
                } else {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + sSupportNumber));
                    startActivity(callIntent);
                }
                dialog.dismiss();

            }
        });
        getSupport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LegalPage.this, ComplaintPage.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                dialog.dismiss();

            }
        });
        CloseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + sSupportNumber));
                    if (ActivityCompat.checkSelfPermission(LegalPage.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    startActivity(callIntent);
                }
                break;


        }
    }


}
