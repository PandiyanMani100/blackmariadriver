package com.blackmaria.partner.app;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.GPSTracker;
import com.blackmaria.partner.Utils.HeaderSessionManager;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.view.signup.RegistrationSuccess;
import com.blackmaria.partner.mylibrary.volley.AppController;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.mylibrary.volley.VolleyMultipartRequest;
import com.blackmaria.partner.latestpackageview.widgets.CustomEdittext;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.blackmaria.partner.latestpackageview.widgets.TextRobotoBold;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.yalantis.ucrop.UCrop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.yalantis.ucrop.util.BitmapLoadUtils.exifToDegrees;

/**
 * Created by user127 on 16-10-2017.
 */

public class RegistrationVehicleDetails extends ActivityHockeyApp implements View.OnClickListener, DatePickerDialog.OnDateSetListener, AdapterView.OnItemSelectedListener {

    private SessionManager sessionManager;
    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    private Dialog dialog;
    private boolean isInternetPresent = false;

    private static final int PERMISSION_REQUEST_CODE = 111;
    int permissionCode = -1;

    private TextView Tv_roadTaxExpiry, Tv_drivingLicenseExpiry, Tv_insuranceExpiry, Tv_photoname;
    private RelativeLayout RL_exit, RL_photo;
    private Button Btn_registerNow, Btn_browse;
    private ImageView Iv_profile;


    private static final int SELECT_IMAGE_REQUEST = 011;
    private static final int TAKE_PHOTO_REQUEST = 022;
    File imageRoot, destination;
    private Uri mImageCaptureUri, outputUri;
    String appDirectoryName = "";

    private int year, month, day, pickerNumber = 0;
    private Calendar calendar;
    private DatePickerDialog pickerDialog;
    String experienceyr = "", companyname = "", companyid = "", driverVehicleId = "", driverLocationPlaceId = "", driverProImage = "", driverCountryCode = "", driverPhoneNumber = "", driverReferelCode = "", driverPinCode = "";
    String driverCompanyType = "", driverCompanyId = "", driverName = "", driverEmailId = "", driverGender = "", driverExperience = "", driverAddress = "",
            driverCityname = "", driverPostCode = "", driverState = "", driverCountry = "";
    byte[] array = null;
    private String Agent_Name = "", language_code = "";
    private String sDriverID = "", gcmID = "", SVehicleimageName = "";
    HeaderSessionManager headerSessionManager;


    private ArrayList<String> arrVehicleId;
    private ArrayList<String> arrVehicleType;

    private ArrayList<String> arrMarkerId;
    private ArrayList<String> arrBrandName;

    private ArrayList<String> arrModelId;
    private ArrayList<String> arrModelName;

    private ArrayList<String> arrModelYr;


    private String driverVehicleYearId = "", driverVehicleModelId = "", slectMarkerId = "", selctVehicleId = "";


    private Spinner Spnr_typeOfVehicle, Spnr_VehicleMaker, Spnr_VehicleModel, Spnr_VehicleModelYear;
    private TextView Tv_typeOfVehicle, txt_VehicleMaker, txt_VehicleModel, txt_VehicleModelYear, txt_content_assistance;
    private boolean isSpnrSelectVehicleMakerFirstTime = true, isSpnrSelectVehicleFirstTime = true;
    private String strRoadTax = "", strLicenseTax = "", strInsuranceExp = "";
    private CustomEdittext vehicleRegnum;
    String sOtpPin = "", sOtpPinStatus = "";
    PinEntryEditText Ed_pin;
    GPSTracker gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_vehicle_details);
        gps = new GPSTracker(RegistrationVehicleDetails.this);
        initialize();
    }

    @SuppressLint("WrongConstant")
    private void initialize() {
        headerSessionManager = new HeaderSessionManager(RegistrationVehicleDetails.this);
        sessionManager = new SessionManager(RegistrationVehicleDetails.this);
        cd = new ConnectionDetector(RegistrationVehicleDetails.this);
        isInternetPresent = cd.isConnectingToInternet();
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        appDirectoryName = getResources().getString(R.string.app_name);
        HashMap<String, String> header = headerSessionManager.getHeaderSession();
        Agent_Name = header.get(HeaderSessionManager.KEY_ID_NAME);
        language_code = header.get(HeaderSessionManager.KEY_LANGUAGE_CODE);
        gcmID = user.get(SessionManager.KEY_GCM_ID);

        HashMap<String, String> user1 = sessionManager.getOtpstatusAndPin();
        sOtpPin = user1.get(SessionManager.KEY_REG_OTP_PIN);
        sOtpPinStatus = user1.get(SessionManager.KEY_REG_OTP_STATUS);

        Intent in = getIntent();
        if (in != null) {
            driverVehicleId = in.getStringExtra("driverVehicleId");
            driverLocationPlaceId = in.getStringExtra("driverLocationPlaceId");
            driverProImage = in.getStringExtra("driverProImage");
            driverCountryCode = in.getStringExtra("driverCountryCode");
            driverPhoneNumber = in.getStringExtra("driverPhoneNumber");
            driverReferelCode = in.getStringExtra("driverReferelCode");
            driverPinCode = in.getStringExtra("driverPinCode");

            driverCompanyType = in.getStringExtra("driverCompanyType");
            driverCompanyId = in.getStringExtra("driverCompanyId");
            driverName = in.getStringExtra("driverName");
            driverEmailId = in.getStringExtra("driverEmailId");
            driverGender = in.getStringExtra("driverGender");
            driverExperience = in.getStringExtra("driverExperience");
            driverAddress = in.getStringExtra("driverAddress");
            driverCityname = in.getStringExtra("driverCityname");
            driverPostCode = in.getStringExtra("driverPostCode");
            driverState = in.getStringExtra("driverState");
            driverCountry = in.getStringExtra("driverCountry");
        }


        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        RL_exit = (RelativeLayout) findViewById(R.id.RL_exit);
        RL_photo = (RelativeLayout) findViewById(R.id.RL_photo);
        Iv_profile = (ImageView) findViewById(R.id.img_profile);
        Btn_browse = (Button) findViewById(R.id.btn_browse);
        Tv_roadTaxExpiry = (TextView) findViewById(R.id.txt_road_tax_expiry);
        Tv_drivingLicenseExpiry = (TextView) findViewById(R.id.txt_driving_license_expiry);
        Tv_insuranceExpiry = (TextView) findViewById(R.id.txt_insurance_expiry);
        Btn_registerNow = (Button) findViewById(R.id.btn_register_now);
        vehicleRegnum = (CustomEdittext) findViewById(R.id.edt_reg_no);
        Tv_photoname = (TextView) findViewById(R.id.txt_label_photo);
        Spnr_typeOfVehicle = (Spinner) findViewById(R.id.spnr_vehicletype);
        Spnr_VehicleMaker = (Spinner) findViewById(R.id.spnr_vehiclemarker);
        Spnr_VehicleModel = (Spinner) findViewById(R.id.spnr_vehiclemodel);
        Spnr_VehicleModelYear = (Spinner) findViewById(R.id.spnr_drivingexperience);
        txt_content_assistance = (TextView) findViewById(R.id.txt_content_assistance);
        Tv_typeOfVehicle = (TextView) findViewById(R.id.txt_vehicletype);
        txt_VehicleMaker = (TextView) findViewById(R.id.txt_spnr_vehiclemarker);
        txt_VehicleModel = (TextView) findViewById(R.id.txt_spnr_vehiclemodel);
        txt_VehicleModelYear = (TextView) findViewById(R.id.txt_drivingexperience);


        RL_exit.setOnClickListener(this);
        RL_photo.setOnClickListener(this);
        Btn_browse.setOnClickListener(this);
        Tv_roadTaxExpiry.setOnClickListener(this);
        Tv_drivingLicenseExpiry.setOnClickListener(this);
        Tv_insuranceExpiry.setOnClickListener(this);
        Btn_registerNow.setOnClickListener(this);

        Spnr_typeOfVehicle.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                CloseKeyboardNew();
                return false;
            }
        });
        Spnr_VehicleMaker.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                CloseKeyboardNew();
                return false;
            }
        });
        Spnr_VehicleModel.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                CloseKeyboardNew();
                return false;
            }
        });
        Spnr_VehicleModelYear.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                CloseKeyboardNew();
                return false;
            }
        });
        if (isInternetPresent) {
            DriverRegVehicleList(Iconstant.DriverReg_VehicleList_URl);

        } else {
            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }
    }

    private void VehiceleTeypeMethos() {
        try {
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item_layout, arrVehicleType);
            spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item_layout); // The drop down vieww
            Spnr_typeOfVehicle.setAdapter(spinnerArrayAdapter);

            Spnr_typeOfVehicle.setOnItemSelectedListener(this);
        } catch (Exception e) {
        }

    }

    private void VehiceleMakerMethos() {
        try {
            if (arrBrandName.size() > 0) {
                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item_layout, arrBrandName);
                spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item_layout); // The drop down vieww
                Spnr_VehicleMaker.setAdapter(spinnerArrayAdapter);
                isSpnrSelectVehicleMakerFirstTime = true;
                Spnr_VehicleMaker.setOnItemSelectedListener(this);
            }
        } catch (Exception e) {
        }
    }

    private void VehiceleModelMethos() {
        try {
            if (arrModelName.size() > 0) {
                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item_layout, arrModelName);
                spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item_layout); // The drop down vieww
                Spnr_VehicleModel.setAdapter(spinnerArrayAdapter);
                isSpnrSelectVehicleFirstTime = true;
                Spnr_VehicleModel.setOnItemSelectedListener(this);
            }
        } catch (Exception e) {
        }
    }

    private void VehiceleModelYearMethos() {
        try {
            if (arrModelYr.size() > 0) {
                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.spinner_item_layout, arrModelYr);
                spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item_layout); // The drop down vieww
                Spnr_VehicleModelYear.setAdapter(spinnerArrayAdapter);
                Spnr_VehicleModelYear.setOnItemSelectedListener(this);
            }
        } catch (Exception e) {
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        System.out.println("++++++++++++position=+++++++++++++" + position);
        if (parent == Spnr_typeOfVehicle) {
            try {
                String sSelectedItem = Spnr_typeOfVehicle.getSelectedItem().toString();
                selctVehicleId = arrVehicleId.get(position);
                Tv_typeOfVehicle.setText(sSelectedItem);
            } catch (Exception e) {
            }
        } else if (parent == Spnr_VehicleMaker) {
            try {
                slectMarkerId = arrMarkerId.get(position);
                String sSelectedItem = Spnr_VehicleMaker.getSelectedItem().toString();
                txt_VehicleMaker.setText(sSelectedItem);
                if (!isSpnrSelectVehicleMakerFirstTime) {
                    if (isInternetPresent) {
                        if (arrModelId != null) {
                            arrModelId.clear();
                            arrModelName.clear();
                        }

                        DriverRegModelList(Iconstant.DriverReg_ModelList_URl);
                    } else {
                        Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                    }
                }
                isSpnrSelectVehicleMakerFirstTime = false;
            } catch (Exception e) {
            }
        } else if (parent == Spnr_VehicleModel) {
            try {
                driverVehicleModelId = arrModelId.get(position);
                String sSelectedItem = "";
                if (Spnr_VehicleModel != null)
                    sSelectedItem = Spnr_VehicleModel.getSelectedItem().toString().trim();
                if (!isSpnrSelectVehicleFirstTime) {
                    txt_VehicleModel.setText(sSelectedItem);
                    DriverRegYrList(Iconstant.DriverReg_YearList_URl);
                }

                isSpnrSelectVehicleFirstTime = false;
            } catch (Exception e) {
            }
        } else if (parent == Spnr_VehicleModelYear) {
            try {
                String sSelectedItem = Spnr_VehicleModelYear.getSelectedItem().toString();
                txt_VehicleModelYear.setText(sSelectedItem);
                driverVehicleYearId = sSelectedItem;
            } catch (Exception e) {
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void showDatePicker() {
//        long minDate = new Date().getTime();

        /*pickerDialog = new DatePickerDialog(RegistrationVehicleDetails.this, R.style.MyDatePickerDialogTheme, this, year, month, day);
        pickerDialog.getDatePicker().setMinDate(minDate);
        pickerDialog.show();*/
        pickerDialog = DatePickerDialog.newInstance(this, year, month, day);
        pickerDialog.setThemeDark(false);
        pickerDialog.showYearPickerFirst(false);
        pickerDialog.setAccentColor(getResources().getColor(R.color.signin_and_signup_slider_ball_blue));
//        pickerDialog.setTitle("Select Date From DatePickerDialog");
        pickerDialog.setMinDate(Calendar.getInstance());
        pickerDialog.show(getFragmentManager(), "DatePickerDialog");

    }

    @Override
    public void onClick(View view) {

        if (view == RL_exit) {
            CloseKeyboardNew();
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (view == RL_photo) {
            CloseKeyboardNew();
            checkAndGrantPermission(0);
        } else if (view == Btn_browse) {
            CloseKeyboardNew();
            checkAndGrantPermission(1);
        } else if (view == Tv_roadTaxExpiry) {
            CloseKeyboardNew();
//            showDialog(999);
            pickerNumber = 1;
            showDatePicker();
        } else if (view == Tv_drivingLicenseExpiry) {
            CloseKeyboardNew();
            pickerNumber = 2;
            showDatePicker();
        } else if (view == Tv_insuranceExpiry) {
            CloseKeyboardNew();
            pickerNumber = 3;
            showDatePicker();
        } else if (view == Btn_registerNow) {
            CloseKeyboardNew();
            if (isInternetPresent) {

                if (SVehicleimageName.equalsIgnoreCase("")) {
                    AlertError(getResources().getString(R.string.timer_label_alert_sorry), getResources().getString(R.string.driver_reg_vehicleimage));
                } else if (strRoadTax.equalsIgnoreCase("")) {
                    AlertError(getResources().getString(R.string.timer_label_alert_sorry), getResources().getString(R.string.driver_reg_roadtaxt_exp));
                } else if (strLicenseTax.equalsIgnoreCase("")) {
                    AlertError(getResources().getString(R.string.timer_label_alert_sorry), getResources().getString(R.string.driver_reg_License_exp));
                } else if (strInsuranceExp.equalsIgnoreCase("")) {
                    AlertError(getResources().getString(R.string.timer_label_alert_sorry), getResources().getString(R.string.driver_reg_insurance));
                } else if (vehicleRegnum.getText().toString().trim().equalsIgnoreCase("")) {
                    AlertError(getResources().getString(R.string.timer_label_alert_sorry), getResources().getString(R.string.driver_reg_vehiclereg_num));
                } else if (selctVehicleId.equalsIgnoreCase("")) {
                    AlertError(getResources().getString(R.string.timer_label_alert_sorry), getResources().getString(R.string.driver_reg_vehicle_type));
                } else if (slectMarkerId.equalsIgnoreCase("")) {
                    AlertError(getResources().getString(R.string.timer_label_alert_sorry), getResources().getString(R.string.driver_reg_vehicle_maker1));
                } else if (driverVehicleModelId.equalsIgnoreCase("")) {
                    AlertError(getResources().getString(R.string.timer_label_alert_sorry), getResources().getString(R.string.driver_reg_vehicle_model));
                } else if (driverVehicleYearId.equalsIgnoreCase("")) {
                    AlertError(getResources().getString(R.string.timer_label_alert_sorry), getResources().getString(R.string.driver_reg_vehicle_year_model));
                } else {
                    if (isInternetPresent) {

                        HashMap<String, String> jsonParams = new HashMap<String, String>();
                        jsonParams.put("driver_location", driverLocationPlaceId);
                        jsonParams.put("category", driverVehicleId);

                        jsonParams.put("profile_image", driverProImage);
                        jsonParams.put("dail_code", driverCountryCode);
                        jsonParams.put("mobile_number", driverPhoneNumber);
                        jsonParams.put("privacy", "yes");
                        jsonParams.put("pincode", driverPinCode);
                        jsonParams.put("referral_code", driverReferelCode);

                        jsonParams.put("driver_type", driverCompanyType);
                        jsonParams.put("company_id", driverCompanyId);
                        jsonParams.put("driver_name", driverName);
                        jsonParams.put("gender_type", driverGender);
                        jsonParams.put("driving_exp", driverExperience);
                        jsonParams.put("email", driverEmailId);
                        jsonParams.put("address", driverAddress);
                        jsonParams.put("country", driverCountry);
                        jsonParams.put("state", driverState);
                        jsonParams.put("city", driverCityname);
                        jsonParams.put("postal_code", driverPostCode);

                        jsonParams.put("vehicle_image", SVehicleimageName);
                        jsonParams.put("road_tax_expiry", strRoadTax);
                        jsonParams.put("licence_expiry", strLicenseTax);
                        jsonParams.put("insurance_expiry", strInsuranceExp);
                        jsonParams.put("vehicle_number", vehicleRegnum.getText().toString().trim());
                        jsonParams.put("vehicle_type", selctVehicleId);
                        jsonParams.put("vehicle_maker", slectMarkerId);
                        jsonParams.put("vehicle_model", driverVehicleModelId);
                        jsonParams.put("vehicle_model_year", driverVehicleYearId);
                        confirmPin(jsonParams);

                    } else {
                        Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                    }

                }


            }
        }

    }

    /*@Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            return new DatePickerDialog(this, myDateListener, year, month, day);
        }
        return null;
    }*/

    /*private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker picker, int year, int month, int day) {
            // arg1 = year
            // arg2 = month
            // arg3 = day
        }
    };*/


    // If code is 0 -> Its For Camera
    // If code is 1 -> Its For Pick From Gallery
    private void checkAndGrantPermission(int code) {

        // Permission Code -> For Identifying Whether the click is from take photo (or) Pick Image From Galley
        permissionCode = code;

        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            if (!checkCameraPermission() || !checkReadExternalStoragePermission() || !checkWriteExternalStoragePermission()) {
                requestPermission();
            } else {
                if (code == 0) {
                    cameraIntent();
                } else if (code == 1) {
                    galleryIntent();
                }
            }
        } else {
            if (code == 0) {
                cameraIntent();
            } else if (code == 1) {
                galleryIntent();
            }
        }

    }

    private boolean checkCameraPermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkReadExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkWriteExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // If permissionCode is 0 -> Its For Camera
                    // If permissionCode is 1 -> Its For Pick From Gallery
                    if (permissionCode == 0) {
                        cameraIntent();
                    } else if (permissionCode == 1) {
                        galleryIntent();
                    }
                }
                break;
        }
    }


    public String dateToString(Date date, String format) {
        SimpleDateFormat df = new SimpleDateFormat(format);
        return df.format(date);
    }

    private void cameraIntent() {


        try {
            Intent pictureIntent = new Intent(
                    MediaStore.ACTION_IMAGE_CAPTURE);
            if (pictureIntent.resolveActivity(getPackageManager()) != null) {
                //Create a file to store the image
                imageRoot = new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES), appDirectoryName);
                if (!imageRoot.exists()) {
                    imageRoot.mkdir();
                }
            }
            if (imageRoot != null) {
                String name = dateToString(new Date(), "yyyy-MM-dd-hh-mm-ss");
                destination = new File(imageRoot, name + ".jpg");
                Uri photoURI = FileProvider.getUriForFile(this, "com.blackmaria.provider", destination);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        photoURI);
                pictureIntent.putExtra("android.intent.extras.CAMERA_FACING", 1);
                startActivityForResult(pictureIntent,
                        TAKE_PHOTO_REQUEST);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void galleryIntent() {
        try {

            imageRoot = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES), appDirectoryName);

            if (!imageRoot.exists()) {
                imageRoot.mkdir();
            }

            Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setType("image/*");
            startActivityForResult(intent, SELECT_IMAGE_REQUEST);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_IMAGE_REQUEST) {
                onSelectFromGalleryResult(data);
            } else if (requestCode == TAKE_PHOTO_REQUEST) {
                onCaptureImageResult(data);
            } else if (requestCode == UCrop.REQUEST_CROP) {
                onCroppedImageResult(data);
            }
        } else if (resultCode == UCrop.RESULT_ERROR) {
            final Throwable cropError = UCrop.getError(data);
        }
    }


    private void onCaptureImageResult(Intent data) {
        try {
            String imagePath = destination.getAbsolutePath();
            mImageCaptureUri = Uri.fromFile(new File(imagePath));
            outputUri = mImageCaptureUri;

//            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), mImageCaptureUri);

            UCrop.Options options = new UCrop.Options();
            options.setStatusBarColor(getResources().getColor(R.color.black_color));
            options.setToolbarColor(getResources().getColor(R.color.app_primary_color));
            options.setMaxBitmapSize(800);

            UCrop.of(mImageCaptureUri, outputUri)
                    .withAspectRatio(1, 1)
                    .withMaxResultSize(8000, 8000)
                    .withOptions(options)
                    .start(RegistrationVehicleDetails.this);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void onSelectFromGalleryResult(Intent data) {

        try {

//          imagePath = destination.getAbsolutePath();
            mImageCaptureUri = data.getData();


//            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), mImageCaptureUri);

            if (!imageRoot.exists()) {
                imageRoot.mkdir();
            }

            String name = dateToString(new Date(), "yyyy-MM-dd-hh-mm-ss");
            destination = new File(imageRoot, name + ".jpg");

            outputUri = Uri.fromFile(destination);

            UCrop.Options options = new UCrop.Options();
            options.setStatusBarColor(getResources().getColor(R.color.black_color));
            options.setToolbarColor(getResources().getColor(R.color.app_primary_color));
            options.setMaxBitmapSize(800);

            UCrop.of(mImageCaptureUri, outputUri)
                    .withAspectRatio(1, 1)
                    .withMaxResultSize(8000, 8000)
                    .withOptions(options)
                    .start(RegistrationVehicleDetails.this);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void onCroppedImageResult(Intent data) {

        final Uri resultUri = UCrop.getOutput(data);

        Log.d("Crop success", "" + resultUri);
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);

            if (bitmap == null) {
                Log.d("Bitmap", "null");
            } else {
                Log.d("Bitmap", "not null");
                Iv_profile.setImageBitmap(bitmap);
                Tv_photoname.setVisibility(View.GONE);
            }
            Tv_photoname.setVisibility(View.GONE);

            Bitmap thumbnail = bitmap;
            final String picturePath = resultUri.getPath();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            File curFile = new File(picturePath);
            try {
                ExifInterface exif = new ExifInterface(curFile.getPath());
                int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                int rotationInDegrees = exifToDegrees(rotation);

                Matrix matrix = new Matrix();
                if (rotation != 0f) {
                    matrix.preRotate(rotationInDegrees);
                }
                thumbnail = Bitmap.createBitmap(thumbnail, 0, 0, thumbnail.getWidth(), thumbnail.getHeight(), matrix, true);
            } catch (IOException ex) {
                Log.e("TAG", "Failed to get Exif data", ex);
            }

            thumbnail.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
            array = byteArrayOutputStream.toByteArray();

            if (isInternetPresent) {
                UploadDriverImage(Iconstant.DriverReg_UploadVehicleImage_URl);

            } else {
                Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void confirmPin(final HashMap<String, String> jsonParams) {
        final Dialog confirmPinDialog = new Dialog(RegistrationVehicleDetails.this, R.style.SlideRightDialog);
        confirmPinDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmPinDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmPinDialog.setCancelable(true);
        confirmPinDialog.setContentView(R.layout.alert_enter_pin_register);

        Ed_pin = (PinEntryEditText) confirmPinDialog.findViewById(R.id.edt_withdrawal_amount);
        final TextRobotoBold RL_confirm = (TextRobotoBold) confirmPinDialog.findViewById(R.id.txt_label_confirm);
        final TextRobotoBold RL_Resend = (TextRobotoBold) confirmPinDialog.findViewById(R.id.txt_label_resend);
        final ImageView Iv_close = (ImageView) confirmPinDialog.findViewById(R.id.img_close);

        if (sOtpPinStatus.equalsIgnoreCase("development")) {
            Ed_pin.setText(sOtpPin);
        }
        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmPinDialog.dismiss();
            }
        });
        RL_Resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isInternetPresent) {
                    CheckMobileNum(Iconstant.DriverRegistration_VerifyMobile_URl);
                } else {
                    Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                }
            }
        });
        RL_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, String> user1 = sessionManager.getOtpstatusAndPin();
                sOtpPin = user1.get(SessionManager.KEY_REG_OTP_PIN);
                sOtpPinStatus = user1.get(SessionManager.KEY_REG_OTP_STATUS);
                try {
                    CloseKeyboard(Ed_pin);
                } catch (Exception e) {
                }

                if (confirmPinDialog != null && confirmPinDialog.isShowing()) {
                    confirmPinDialog.dismiss();
                }

                if (sOtpPin.equalsIgnoreCase(Ed_pin.getText().toString())) {
                    DriverRegistratinMethod(Iconstant.DriverRegistration_URl, jsonParams);
                } else {
                    AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.label_incorrect_pin));
                }

            }
        });

        confirmPinDialog.show();

    }

    //--------------Close KeyBoard Method-----------
    @SuppressLint("WrongConstant")
    private void CloseKeyboard(EditText edittext) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(edittext.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private void CheckMobileNum(String Url) {

        dialog = new Dialog(RegistrationVehicleDetails.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("dail_code", driverCountryCode);
        jsonParams.put("mobile_number", driverPhoneNumber);
        jsonParams.put("lat", String.valueOf(gps.getLatitude()));
        jsonParams.put("lon", String.valueOf(gps.getLongitude()));

        System.out.println("--------------CheckMobileNum Url-------------------" + Url);
        System.out.println("--------------CheckMobileNum jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(RegistrationVehicleDetails.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------CheckMobileNum Response-------------------" + response);
                String status = "", Sresponse = "", otpStatus = "", otpPin = "";
                try {

                    JSONObject object = new JSONObject(response);

                    status = object.getString("status");
                    Sresponse = object.getString("response");
                    if (status.equalsIgnoreCase("1")) {
                        otpStatus = object.getString("otp_status");
                        otpPin = object.getString("otp");
                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (status.equalsIgnoreCase("1")) {
                    if (otpStatus.equalsIgnoreCase("development")) {
                        Ed_pin.setText(otpPin);
                    }
                    sessionManager.setOtpstatusAndPin(otpStatus, otpPin);
                } else {
                    Alert(getResources().getString(R.string.action_error), Sresponse);
                }

                if (dialog != null) {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    private void DriverRegVehicleList(String Url) {

        dialog = new Dialog(RegistrationVehicleDetails.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("category_id", driverVehicleId);
        System.out.println("--------------DriverRegVehicleList Url-------------------" + Url);
        System.out.println("--------------DriverRegVehicleList jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(RegistrationVehicleDetails.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------DriverRegVehicleList Response-------------------" + response);
                String status = "", Sresponse = "";
                try {

                    JSONObject object = new JSONObject(response);

                    status = object.getString("status");
                    JSONObject objResponse = object.getJSONObject("response");
                    if (objResponse.length() > 0) {

                        Object intervention = objResponse.get("vehicle");
                        if (intervention instanceof JSONArray) {
                            JSONArray categoryArray = objResponse.getJSONArray("vehicle");
                            arrVehicleId = new ArrayList<String>();
                            arrVehicleType = new ArrayList<String>();

                            arrVehicleId.add("0");
                            arrVehicleType.add("Choose Vehicle Type");

                            for (int i = 0; i < categoryArray.length(); i++) {
                                JSONObject locCtaListObj = categoryArray.getJSONObject(i);
                                String id = locCtaListObj.getString("id");
                                String name = locCtaListObj.getString("vehicle_type");
                                arrVehicleId.add(id);
                                arrVehicleType.add(name);
                            }

                        }
                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (status.equalsIgnoreCase("1")) {

                    VehiceleTeypeMethos();
                    if (isInternetPresent) {
                        DriverRegMarkerList(Iconstant.DriverReg_MarkerList_URl);
                    } else {
                        Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                    }
                } else {
                    Alert(getResources().getString(R.string.action_error), Sresponse);
                }


                if (dialog != null) {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void DriverRegYrList(String Url) {

        dialog = new Dialog(RegistrationVehicleDetails.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("model_id", driverVehicleModelId);
        System.out.println("--------------DriverRegYrList Url-------------------" + Url);
        System.out.println("--------------DriverRegYrList jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(RegistrationVehicleDetails.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------DriverRegYrList Response-------------------" + response);
                String status = "", Sresponse = "";
                try {

                    JSONObject object = new JSONObject(response);

                    status = object.getString("status");
                    JSONObject objResponse = object.getJSONObject("response");
                    if (objResponse.length() > 0) {

                        Object intervention = objResponse.get("model");
                        if (intervention instanceof JSONArray) {
                            JSONArray categoryArray = objResponse.getJSONArray("model");
                            arrModelYr = new ArrayList<String>();
                            arrModelYr.add("Choose Model year");

                            for (int i = 0; i < categoryArray.length(); i++) {
                                String name = categoryArray.getString(i);
                                arrModelYr.add(name);
                            }

                        }
                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (status.equalsIgnoreCase("1")) {
                    VehiceleModelYearMethos();
                } else {
                    Alert(getResources().getString(R.string.action_error), Sresponse);
                }
                if (dialog != null) {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void DriverRegMarkerList(String Url) {

        dialog = new Dialog(RegistrationVehicleDetails.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        //jsonParams.put("category_id",driverVehicleId);
        System.out.println("--------------DriverRegMarkerList Url-------------------" + Url);
        // System.out.println("--------------DriverRegMarkerList jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(RegistrationVehicleDetails.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------DriverRegMarkerList Response-------------------" + response);
                String status = "", Sresponse = "";
                try {

                    JSONObject object = new JSONObject(response);

                    status = object.getString("status");
                    JSONObject objResponse = object.getJSONObject("response");
                    if (objResponse.length() > 0) {

                        Object intervention = objResponse.get("maker");
                        if (intervention instanceof JSONArray) {
                            JSONArray categoryArray = objResponse.getJSONArray("maker");
                            arrMarkerId = new ArrayList<String>();
                            arrBrandName = new ArrayList<String>();

                            arrMarkerId.add("0");
                            arrBrandName.add("Choose Brand Name");

                            for (int i = 0; i < categoryArray.length(); i++) {
                                JSONObject locCtaListObj = categoryArray.getJSONObject(i);
                                String id = locCtaListObj.getString("id");
                                String name = locCtaListObj.getString("brand_name");
                                arrMarkerId.add(id);
                                arrBrandName.add(name);
                            }

                        }
                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (status.equalsIgnoreCase("1")) {
                    VehiceleMakerMethos();

                } else {
                    Alert(getResources().getString(R.string.action_error), Sresponse);
                }
                if (dialog != null) {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    private void DriverRegModelList(String Url) {

        dialog = new Dialog(RegistrationVehicleDetails.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("maker_id", slectMarkerId);
        jsonParams.put("vehicle_id", selctVehicleId);
        System.out.println("--------------DriverRegModelList Url-------------------" + Url);
        System.out.println("--------------DriverRegModelList jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(RegistrationVehicleDetails.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------DriverRegModelList Response-------------------" + response);
                String status = "", Sresponse = "";
                try {
                    JSONObject object = new JSONObject(response);
                    status = object.getString("status");
                    JSONObject objResponse = object.getJSONObject("response");
                    if (objResponse.length() > 0) {
                        Object intervention = objResponse.get("model");
                        if (intervention instanceof JSONArray) {
                            JSONArray categoryArray = objResponse.getJSONArray("model");
                            arrModelId = new ArrayList<String>();
                            arrModelName = new ArrayList<String>();
                            arrModelId.add("0");
                            arrModelName.add("Choose Model Name");
                            for (int i = 0; i < categoryArray.length(); i++) {
                                JSONObject locCtaListObj = categoryArray.getJSONObject(i);
                                String id = locCtaListObj.getString("id");
                                String name = locCtaListObj.getString("name");
                                arrModelId.add(id);
                                arrModelName.add(name);
                            }
                        }
                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (status.equalsIgnoreCase("1")) {
                    VehiceleModelMethos();
                } else {
                    Alert(getResources().getString(R.string.action_error), Sresponse);
                }
                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void UploadDriverImage(String url) {
        dialog = new Dialog(RegistrationVehicleDetails.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {

                System.out.println("------------- image response-----------------" + response.data);


                String resultResponse = new String(response.data);
                System.out.println("-------------  response-----------------" + resultResponse);
                String sStatus = "", sResponse = "", SUser_image = "", Smsg = "";
                try {
                    JSONObject jsonObject = new JSONObject(resultResponse);
                    sStatus = jsonObject.getString("status");
                    JSONObject imagObj = jsonObject.getJSONObject("response");
                    if (sStatus.equalsIgnoreCase("1")) {
                        SVehicleimageName = imagObj.getString("image_name");
                    } else {
                        Smsg = jsonObject.getString("response");
                        Alert(getResources().getString(R.string.action_error), Smsg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                dialog.dismiss();

                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");

                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
               /* params.put("user_id", UserID);
                System.out.println("user_id---------------" + UserID);*/
                return params;
            }

            @Override
            protected Map<String, VolleyMultipartRequest.DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                params.put("vehicle_image", new DataPart("blackmariavehicle.jpg", array));
                //   System.out.println("user_image--------edit------" + byteArray);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Authkey", Agent_Name);
                headers.put("isapplication", Iconstant.cabily_IsApplication);
                headers.put("applanguage", language_code);
                headers.put("apptype", Iconstant.cabily_AppType);
                headers.put("driverid", sDriverID);
                headers.put("apptoken", gcmID);

                System.out.println("------------Headers-----" + headers);
                return headers;
            }

        };

        //to avoid repeat request Multiple Time
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        multipartRequest.setRetryPolicy(retryPolicy);
        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        multipartRequest.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(multipartRequest);

    }


    private void DriverRegistratinMethod(String Url, HashMap<String, String> jsonParams) {

        dialog = new Dialog(RegistrationVehicleDetails.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));


        System.out.println("--------------DriverRegModelList Url-------------------" + Url);
        System.out.println("--------------DriverRegModelList jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(RegistrationVehicleDetails.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------DriverRegModelList Response-------------------" + response);
                String status = "", Sresponse = "";
                try {
                    JSONObject object = new JSONObject(response);
                    status = object.getString("status");
                    Sresponse = object.getString("response");

                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (status.equalsIgnoreCase("1")) {
                    Alert1(getResources().getString(R.string.action_success), Sresponse);
                } else {
                    Alert(getResources().getString(R.string.action_error), Sresponse);
                }
                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void Alert1(String title, String alert) {

        final PkDialog mDialog = new PkDialog(RegistrationVehicleDetails.this);
        mDialog.setDialogTitle(title);
        mDialog.setCancelOnTouchOutside(false);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                Intent intent = new Intent(RegistrationVehicleDetails.this, RegistrationSuccess.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | /*Intent.FLAG_ACTIVITY_NO_HISTORY |*/ Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            }
        });
        mDialog.show();
    }

    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(RegistrationVehicleDetails.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
        datePickerDialog.dismiss();
        int months = month + 1;
        ;
        this.year = year;
        this.month = month;
        this.day = day;

        if (pickerNumber == 1) {
            Tv_roadTaxExpiry.setText(day + "/" + months + "/" + year);
            strRoadTax = day + "/" + months + "/" + year;
        } else if (pickerNumber == 2) {
            Tv_drivingLicenseExpiry.setText(day + "/" + months + "/" + year);
            strLicenseTax = day + "/" + months + "/" + year;
        } else if (pickerNumber == 3) {
            Tv_insuranceExpiry.setText(day + "/" + months + "/" + year);
            strInsuranceExp = day + "/" + months + "/" + year;
        }
    }

    private void AlertError(String title, String message) {

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        Snackbar bar = Snackbar.with(RegistrationVehicleDetails.this);
        bar.addView(view);
        bar.colorResource(R.color.dark_red);
        bar.position(Snackbar.SnackbarPosition.TOP);
        bar.type(SnackbarType.MULTI_LINE);
        bar.duration(Snackbar.SnackbarDuration.LENGTH_SHORT);
        bar.animation(true);
        SnackbarManager.show(bar, RegistrationVehicleDetails.this);


    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

    private void CloseKeyboardNew() {
        try {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow((null == getCurrentFocus()) ? null : getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

