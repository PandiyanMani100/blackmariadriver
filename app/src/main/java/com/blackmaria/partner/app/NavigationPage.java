package com.blackmaria.partner.app;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.provider.Settings;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActionBarActivityHockeyApp;
import com.blackmaria.partner.Pojo.KeyValueBean;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.fragment.HomePage;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.view.sidemenu.LegalPage;
import com.blackmaria.partner.latestpackageview.widgets.CustomEdittext;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class NavigationPage extends ActionBarActivityHockeyApp implements View.OnClickListener {

    private RelativeLayout RL_about, RL_terms, RL_menu, RL_help, RL_info;
    private TextView Tv_about, Tv_terms, Tv_faq, Tv_info, Tv_infoNotification;
    //    private TextView Tv_HeaderText;
    private String driver_id = "", sReferralCode = "", sReferralDriverName = "", sReferralDriverImage = "", sReferralDriverPercentage = "";

    private Dialog dialog;
    private SessionManager session;
    private ConnectionDetector cd;
    private boolean isInternetPresent = false;

    private ActionBarDrawerToggle mDrawerToggle;
    public static DrawerLayout mDrawerLayout;
    static RelativeLayout mDrawer;

    private ServiceRequest mRequest;
    private RelativeLayout Btn_logout, relative_review, relative_star;
    private RelativeLayout RL_profile, RL_account, RL_ewallet, RL_messages, RL_drive_and_earn, RL_referral, RL_feedback, RL_rate_cards, RL_home, RL_resign;
    private ImageView btnBack;
    ImageView fileimage;
    private BroadcastReceiver notificationReceiver;
    public static boolean inHomePage = true;
    private String sContactMail = "", driverRating;
    public static BroadcastReceiver refreshHomePageReceiver;

    private static final int RESULT_INTENT_CODE = 101;
    private ArrayList<KeyValueBean> statementList;
    RatingBar driverRatingBar;
    public static String fromrequestpage = "";

    public static Activity parent;

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_page_layout2);

        parent = NavigationPage.this;
        if(getIntent().hasExtra("fromrequestpage")){
            fromrequestpage = getIntent().getStringExtra("fromrequestpage");
        }

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Intent intent = new Intent();
            String packageName = getPackageName();
            PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
            if (!pm.isIgnoringBatteryOptimizations(packageName)) {
                intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                intent.setData(Uri.parse("package:" + packageName));
                startActivity(intent);
            }
        }

//        DemoSyncJob.scheduleJob();
//        if(DemoSyncJob.jobid != 0){
//            Utils.cancelJob(DemoSyncJob.jobid);
//        }

        initialize();

        if (savedInstanceState == null) {
            FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
            tx.replace(R.id.main_page_content_frame, new HomePage());
            tx.commit();
        }


        InfoRequest(Iconstant.message_url);


        // Receiving the data from broadcast
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.app.infoNotification.NavigationPage");
        notificationReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent != null) {
                    if (intent.hasExtra("info_badge")) {
                        if (Tv_infoNotification != null) {
                            String notiValue = intent.getStringExtra("info_badge");
                            if (notiValue.equalsIgnoreCase("0")) {
                                Tv_infoNotification.setVisibility(View.GONE);
                            } else {
                                Tv_infoNotification.setVisibility(View.VISIBLE);
                            }
                            Tv_infoNotification.setText(notiValue);
                        }
                    }
                    if (intent.hasExtra("referral_status")) {
                        String sReferralStatus = intent.getStringExtra("referral_status");
                        if (sReferralStatus.equalsIgnoreCase("no")) {
                            // already referral code submitted so hide referral code box
                            RL_referral.setVisibility(View.GONE);
                        } else {
                            RL_referral.setVisibility(View.VISIBLE);
                        }

                    }
                }
            }
        };
        registerReceiver(notificationReceiver, filter);


    }



    private void InfoRequest(String Url) {

        HashMap<String, String> user = session.getUserDetails();
        String sDriveID = user.get(SessionManager.KEY_DRIVERID);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("id", sDriveID);
        jsonParams.put("page", String.valueOf("1"));
        jsonParams.put("perPage", "5");
        jsonParams.put("type", "notification");

        System.out.println("-------------InfoRequest  Url----------------" + Url);


        mRequest = new ServiceRequest(NavigationPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------InfoRequest  Response----------------" + response);

                String Sstatus = "", str_NextPage = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");

                        if (response_object.length() > 0) {
                            String totlanotification = response_object.getString("total_record");
                            session.setInfoBadge(totlanotification);
                            Tv_infoNotification.setText(totlanotification);
                        }
                    }

                    if (Sstatus.equalsIgnoreCase("1")) {

                    } else {
                        String Sresponse = object.getString("response");
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


            }

            @Override
            public void onErrorListener() {
            }
        });

    }


    private void initialize() {
        // Steps for Navigation Drawer
        mDrawerLayout = (DrawerLayout) findViewById(R.id.navigation_drawer);
        mDrawer = (RelativeLayout) findViewById(R.id.Rl_drawer);
        statementList = new ArrayList<>();
        setUpDrawer();

        session = new SessionManager(NavigationPage.this);
        HashMap<String, String> user = session.getUserDetails();
        sContactMail = user.get(SessionManager.KEY_EMAIL);
        driverRating = session.getDriverReview();
        driver_id = user.get(SessionManager.KEY_DRIVERID);

        RL_about = (RelativeLayout) findViewById(R.id.RL_about);
        RL_terms = (RelativeLayout) findViewById(R.id.RL_terms);
        RL_menu = (RelativeLayout) findViewById(R.id.RL_menu);
        RL_help = (RelativeLayout) findViewById(R.id.RL_help);
        RL_info = (RelativeLayout) findViewById(R.id.RL_info);
        Tv_infoNotification = (TextView) findViewById(R.id.txt_info_notification);

//        Tv_HeaderText = (TextView) findViewById(R.id.headerBar_title_textView);

//        Typeface face = Typeface.createFromAsset(getAssets(),"fonts/AdventPro-SemiBold.ttf");
//        Tv_HeaderText.setTypeface(face);

        Btn_logout = (RelativeLayout) findViewById(R.id.RL_logout);
        relative_review = (RelativeLayout) findViewById(R.id.relative_review);
        relative_star = (RelativeLayout) findViewById(R.id.relatvie_star);
        RL_profile = (RelativeLayout) findViewById(R.id.Rl_profile);
        RL_account = (RelativeLayout) findViewById(R.id.RL_account);
        RL_ewallet = (RelativeLayout) findViewById(R.id.RL_bwallet);
        RL_messages = (RelativeLayout) findViewById(R.id.RL_messages);
        RL_drive_and_earn = (RelativeLayout) findViewById(R.id.RL_drive_and_earn);
        RL_referral = (RelativeLayout) findViewById(R.id.RL_referral);
        RL_rate_cards = (RelativeLayout) findViewById(R.id.RL_rate_cards);
        RL_home = (RelativeLayout) findViewById(R.id.RL_home);
        RL_resign = (RelativeLayout) findViewById(R.id.RL_resign);
        fileimage = (ImageView) findViewById(R.id.fileimage);
        driverRatingBar = (RatingBar) findViewById(R.id.myride_rating_single_ratingbar);

        if (driverRating.equalsIgnoreCase(null) || driverRating.equalsIgnoreCase("")) {
            float userRate = Float.parseFloat("0");
            driverRatingBar.setRating(userRate);
        } else {
            float userRate = Float.parseFloat(driverRating);
            driverRatingBar.setRating(userRate);
        }

//        RL_feedback = (RelativeLayout) findViewById(R.id.Rl_feedback);
//        btnBack = (ImageView) findViewById(R.id.img_btn_back);

        relative_review.setOnClickListener(this);
        relative_star.setOnClickListener(this);
        Btn_logout.setOnClickListener(this);
        RL_profile.setOnClickListener(this);
        RL_account.setOnClickListener(this);
        RL_ewallet.setOnClickListener(this);
        RL_messages.setOnClickListener(this);
        RL_drive_and_earn.setOnClickListener(this);
        RL_referral.setOnClickListener(this);
        RL_rate_cards.setOnClickListener(this);
        RL_home.setOnClickListener(this);
        RL_resign.setOnClickListener(this);
        fileimage.setOnClickListener(this);
//        btnBack.setOnClickListener(this);

        RL_about.setOnClickListener(this);
        RL_terms.setOnClickListener(this);
        RL_menu.setOnClickListener(this);
        RL_help.setOnClickListener(this);
        RL_info.setOnClickListener(this);

        enableSwipeDrawer();
//        openDrawer();

//        registerBroadcastReceiver();

        session.setXmppServiceState("online");


    }

    public void setUpDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(NavigationPage.this, mDrawerLayout, R.string.app_name, R.string.app_name);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

    }

    @Override
    public void onClick(View view) {

        cd = new ConnectionDetector(NavigationPage.this);
        isInternetPresent = cd.isConnectingToInternet();

        if (isInternetPresent) {


            if (view == RL_about) {

                /*FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
                Bundle args = new Bundle();
                args.putString("pageType", "about-us");
                Fragment dynamicPage = new DynamicContent();
                dynamicPage.setArguments(args);
                tx.replace(R.id.main_page_content_frame, dynamicPage);
                tx.commit();*/

                Intent intent = new Intent(NavigationPage.this, AboutUs.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);


            } else if (view == RL_terms) {

                /*FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
                Bundle args = new Bundle();
                args.putString("pageType", "terms-condition");
                DynamicContent dynamicPage = new DynamicContent();
                dynamicPage.setArguments(args);
                tx.replace(R.id.main_page_content_frame, dynamicPage);
                tx.commit();*/

                Intent intent = new Intent(NavigationPage.this, LegalPage.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);

            } else if (view == RL_help) {

                /*FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
                Bundle args = new Bundle();
                args.putString("pageType", "faq");
                DynamicContent dynamicPage = new DynamicContent();
                dynamicPage.setArguments(args);
                tx.replace(R.id.main_page_content_frame, dynamicPage);
                tx.commit();*/

                Intent intent = new Intent(NavigationPage.this, HelpAndAssistance.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);

            } else if (view == RL_info) {
                /*FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
                tx.replace(R.id.main_page_content_frame, new InfoPage());
                tx.commit();*/

                Intent intent = new Intent(NavigationPage.this, InfoPage.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);

            } else if (view == RL_menu) {
                if (inHomePage) {
                    NavigationPage.openDrawer();
//                    new Handler().post(new Runnable() {
//                        @Override
//                        public void run() {
//                            mDrawerLayout.openDrawer(GravityCompat.START);
//                        }
//                    });
                } else {
                    FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
                    tx.replace(R.id.main_page_content_frame, new HomePage());
                    tx.commit();
                }
            } else if (view == RL_profile) {
                Intent intent = new Intent(NavigationPage.this, DriverProfile_New.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            } else if (view == RL_account) {

//                DriverAccount();
                Alert("Sorry", "Kindly Contact Admin");

            } else if (view == RL_ewallet) {
                Intent intent = new Intent(NavigationPage.this, WalletMoneyPage1.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                if (mDrawerLayout.isDrawerOpen(mDrawer)) {
//                    mDrawerLayout.closeDrawer(mDrawer);
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            mDrawerLayout.closeDrawer(GravityCompat.START);
                        }
                    });
                }
            } else if (view == RL_messages) {
                Intent intent = new Intent(NavigationPage.this, MessagePage.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            } else if (view == RL_drive_and_earn) {
                Intent intent = new Intent(NavigationPage.this, InviteAndEarn.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            } else if (view == RL_rate_cards) {
                Intent intent = new Intent(NavigationPage.this, RateCardPage.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            } else if (view == RL_home) {
//                mDrawerLayout.closeDrawer(mDrawer);
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        mDrawerLayout.closeDrawer(GravityCompat.START);
                    }
                });
            } else if (view == RL_referral) {
                showReferralAlert();
            } else if (view == RL_resign) {

                DriverResign();
            } else if (view == fileimage) {
                Intent intent_sh = new Intent(NavigationPage.this, MenuDriverratingHOme.class);
                intent_sh.putExtra("frompage", "Home");
                startActivity(intent_sh);
            } else if (view == relative_review) {
                Intent intent_sh = new Intent(NavigationPage.this, MenuDriverratingHOme.class);
                intent_sh.putExtra("frompage", "Home");
                startActivity(intent_sh);
            } else if (view == relative_star) {
                Intent intent_sh = new Intent(NavigationPage.this, MenuDriverratingHOme.class);
                intent_sh.putExtra("frompage", "Home");
                startActivity(intent_sh);
            }

            /*else if (view == RL_feedback) {
                sendEmail(sContactMail);
            } *//*else if (view == btnBack) {
                mDrawerLayout.closeDrawer(mDrawer);
            }*/

            else if (view == Btn_logout) {
                logoutDriver();
            }

            /*if(mDrawerLayout.isDrawerOpen(mDrawer)) {
                mDrawerLayout.closeDrawer(mDrawer);
            }*/

        } else {
            alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }

    }


    private void logoutDriver() {

        final PkDialog mDialog = new PkDialog(NavigationPage.this);
        mDialog.setDialogTitle(getResources().getString(R.string.dialog_app_logout1));
        mDialog.setDialogMessage(getResources().getString(R.string.dialog_app_logout));
        mDialog.setPositiveButton(getResources().getString(R.string.ok_lable), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                try {
                    PostLogout(Iconstant.Profile_driver_account_Logout_Url);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        mDialog.setNegativeButton(getResources().getString(R.string.cancel_lable), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();

    }


    private void DriverAccount() {

        final Dialog dialog = new Dialog(NavigationPage.this, R.style.SlideRightDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.profile_account_popup);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        Button closeImage = (Button) dialog.findViewById(R.id.custom_dialog_library_ok_button);
        final CustomEdittext account_pass = (CustomEdittext) dialog.findViewById(R.id.edt_passcode);
        Button InRTv = (Button) dialog.findViewById(R.id.custom_dialog_library_cancel_button);


        InRTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!account_pass.getText().toString().equalsIgnoreCase("")) {
                    Intent intent = new Intent(NavigationPage.this, DriverAccount.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    //alert(getResources().getString(R.string.timer_label_alert_sorry),"Please contact Administrator");
                    dialog.dismiss();
                } else {
                    Alert("Sorry", "Please enter account password");
                }

            }
        });
        // Et_enteramount.setHint(getResources().getString(R.string.walletmoney_lable_rechargemoney_edittext_hint) + " " + ScurrencySymbol + Str_minimum_amt + " " + "-" + " " + ScurrencySymbol + Str_maximum_amt);
        closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }

        });

        dialog.show();
    }

    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(NavigationPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();

    }


    private void DriverResign() {

        final Dialog dialog = new Dialog(NavigationPage.this, R.style.SlideRightDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.accountclosepopup);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        Button closeImage = (Button) dialog.findViewById(R.id.custom_dialog_library_ok_button);
        Button InRTv = (Button) dialog.findViewById(R.id.custom_dialog_library_cancel_button);


        InRTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent closeIntent = new Intent(NavigationPage.this, ProcessingDeactivation.class);
                startActivityForResult(closeIntent, RESULT_INTENT_CODE);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                dialog.dismiss();
            }
        });

        // Et_enteramount.setHint(getResources().getString(R.string.walletmoney_lable_rechargemoney_edittext_hint) + " " + ScurrencySymbol + Str_minimum_amt + " " + "-" + " " + ScurrencySymbol + Str_maximum_amt);
        closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }

        });

        dialog.show();
    }

    private void showReferralAlert() {

        //--------Adjusting Dialog width-----
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 85% of the screen
        int screenHeight = (int) (metrics.heightPixels * 0.80);//fill only 80% of the screen

        final Dialog referralDialog = new Dialog(NavigationPage.this, R.style.SlideUpDialog);
        referralDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        referralDialog.setContentView(R.layout.alert_enter_referral_code);
        referralDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        referralDialog.getWindow().setLayout(screenWidth, screenHeight);

        final EditText Ed_referral = (EditText) referralDialog.findViewById(R.id.edt_referral_code);
        final Button Btn_confirm = (Button) referralDialog.findViewById(R.id.btn_confirm);
        final ImageView Iv_close = (ImageView) referralDialog.findViewById(R.id.img_close);

        Btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sReferralCode = Ed_referral.getText().toString().trim();

                if (sReferralCode.length() > 0) {
                    Ed_referral.setError(null);
                    referralDialog.dismiss();
//                    showVerifiedAlert();

                    if (cd.isConnectingToInternet()) {
                        checkReferralCode(Iconstant.confirm_referral_url);
                    } else {
                        alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                    }

                } else {
                    Ed_referral.setError(getResources().getString(R.string.otp_page_referral_error));
                }

            }
        });

        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                referralDialog.dismiss();
            }
        });

        referralDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
//                Iv_insertReferral.setClickable(true);
                CloseKeyboardNew();
            }
        });

        Ed_referral.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    CloseKeyboard(Ed_referral);
                }
                return false;
            }
        });

        referralDialog.show();
//        Iv_insertReferral.setClickable(false);

    }

    @SuppressLint("SetTextI18n")
    private void showVerifiedAlert() {

        //--------Adjusting Dialog width and height-----
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 85% of the screen
        int screenHeight = (int) (metrics.heightPixels * 0.80);//fill only 80% of the screen

        final Dialog verifiedDialog = new Dialog(NavigationPage.this, R.style.SlideUpDialog);
        verifiedDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        verifiedDialog.setContentView(R.layout.alert_verified_status);
        verifiedDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        verifiedDialog.getWindow().setLayout(screenWidth, screenHeight);

        final ImageView Iv_profile = (ImageView) verifiedDialog.findViewById(R.id.img_profile);
        final TextView Tv_driverName = (TextView) verifiedDialog.findViewById(R.id.txt_user_name);
        final TextView Tv_content1 = (TextView) verifiedDialog.findViewById(R.id.txt_content1);
        final TextView Tv_earnAtLabel = (TextView) verifiedDialog.findViewById(R.id.txt_label_earn_percentage);
        final TextView Tv_earnPercentage = (TextView) verifiedDialog.findViewById(R.id.txt_earn_percentage);
        final Button Btn_proceed = (Button) verifiedDialog.findViewById(R.id.btn_proceed);

        if (sReferralDriverImage.length() > 0) {
            Picasso.with(NavigationPage.this).load(sReferralDriverImage).placeholder(R.drawable.no_user_profile).error(R.drawable.no_user_profile).resize(150, 150).memoryPolicy(MemoryPolicy.NO_CACHE).into(Iv_profile);
        }
        Tv_driverName.setText(sReferralDriverName);
        Tv_content1.setText(sReferralDriverName + "\t" + getResources().getString(R.string.referral_verified_content1));
        Tv_earnAtLabel.setText(sReferralDriverName + "\t" + getResources().getString(R.string.referral_verified_earn_at));
        Tv_earnPercentage.setText(sReferralDriverPercentage + "%");

        Btn_proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                verifiedDialog.dismiss();

                if (cd.isConnectingToInternet()) {
                    submitReferralCode(Iconstant.submit_referral_url);
                } else {
                    alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                }

            }
        });

        verifiedDialog.show();

    }


    private void alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(NavigationPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    @SuppressWarnings("WrongConstant")
    private void CloseKeyboard(EditText edittext) {
        try {
            InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            in.hideSoftInputFromWindow(edittext.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("WrongConstant")
    private void CloseKeyboardNew() {
        try {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow((null == getCurrentFocus()) ? null : getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void openDrawer() {
        mDrawerLayout.openDrawer(mDrawer);
    }

    public static void disableSwipeDrawer() {
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    public static void enableSwipeDrawer() {
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {

            if (notificationReceiver != null) {
                unregisterReceiver(notificationReceiver);
            }
            if (refreshHomePageReceiver != null) {
                unregisterReceiver(refreshHomePageReceiver);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void PostLogout(String url) {
        dialog = new Dialog(NavigationPage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_processing));

        HashMap<String, String> jsonParams = new HashMap<>();
        jsonParams.put("driver_id", driver_id);
        jsonParams.put("device", "ANDROID");

        System.out.println("-------------Logout Url----------------" + url);
        System.out.println("-------------Logout jsonParams----------------" + jsonParams);

        mRequest = new ServiceRequest(NavigationPage.this);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Logout Response----------------" + response);

                String Sstatus = "";
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        session.logoutUser();
                        finish();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });

    }

    @SuppressWarnings("WrongConstant")
    private void checkReferralCode(String Url) {
        dialog = new Dialog(NavigationPage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_verifying));

        System.out.println("--------------checkReferralCode url-------------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", driver_id);
        jsonParams.put("code", sReferralCode);

        System.out.println("------checkReferralCode jsonParams---------" + jsonParams);

        mRequest = new ServiceRequest(NavigationPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------checkReferralCode response-------------------" + response);


                String Sstatus = "", Sresponse = "";

                try {

                    JSONObject object = new JSONObject(response);

                    Sstatus = object.getString("status");
                    Sresponse = object.getString("response");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        sReferralDriverName = object.getString("driver_name");
                        sReferralDriverPercentage = object.getString("ref_per");
                        sReferralDriverImage = object.getString("driver_image");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                dialogDismiss();

                if (Sstatus.equalsIgnoreCase("1")) {
                    showVerifiedAlert();
                } else {
                    alert(getResources().getString(R.string.action_error), Sresponse);
                }

            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });
    }

    @SuppressWarnings("WrongConstant")
    private void submitReferralCode(String Url) {
        dialog = new Dialog(NavigationPage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_verifying));

        System.out.println("--------------submitReferralCode url-------------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", driver_id);
        jsonParams.put("referral_code", sReferralCode);

        System.out.println("------submitReferralCode jsonParams---------" + jsonParams);

        mRequest = new ServiceRequest(NavigationPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------submitReferralCode response-------------------" + response);


                String Sstatus = "", Sresponse = "";

                try {

                    JSONObject object = new JSONObject(response);

                    Sstatus = object.getString("status");
                    Sresponse = object.getString("response");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                dialogDismiss();

                if (Sstatus.equalsIgnoreCase("1")) {
                    RL_referral.setVisibility(View.GONE);
                }

            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });
    }


    private void dialogDismiss() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }


    //----------Sending message on Email Method--------
    protected void sendEmail(String to) {
        String[] TO = {to};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "");
        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.invite_earn_label_email_not_installed));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {

            if (requestCode == RESULT_INTENT_CODE) {

                int status = (int) data.getExtras().getInt("Status");

                if (status == 1) {

                    String sCurrency = data.getExtras().getString("Currency");
                    String sTotalAmount = data.getExtras().getString("TotalAmount");
                    statementList = (ArrayList<KeyValueBean>) data.getSerializableExtra("StatementList");

                    Intent intent = new Intent(NavigationPage.this, DeactivateStatement.class);
                    intent.putExtra("Currency", sCurrency);
                    intent.putExtra("TotalAmount", sTotalAmount);
                    intent.putExtra("StatementList", statementList);
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);

                } else if (status == 0) {
                    // Some Problem in Loading Statement
                } else if (status == -1) {
                    // Internet Not Connected
                    alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                }

                if (mDrawerLayout.isDrawerOpen(mDrawer)) {
//                    mDrawerLayout.closeDrawer(mDrawer);
                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            mDrawerLayout.closeDrawer(GravityCompat.START);
                        }
                    });
                }

            }
        }
    }


    @Override
    protected void onPause() {
        super.onPause();


        System.out.println("NavigationPage -> onPause");

//        System.gc();
//        Runtime.getRuntime().gc();
    }

    @SuppressLint("WrongConstant")
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        boolean b = false;
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                System.out.println("1 already running");
                b = true;
                break;
            } else {
                System.out.println("2 not running");
                b = false;
            }
        }
        System.out.println("3 not running");
        return b;
    }

}
