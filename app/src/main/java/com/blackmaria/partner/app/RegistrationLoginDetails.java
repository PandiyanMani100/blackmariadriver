package com.blackmaria.partner.app;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.blackmaria.partner.Hockeyapp.FragmentActivityHockeyApp;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.CountryDialCode;
import com.blackmaria.partner.Utils.GPSTracker;
import com.blackmaria.partner.Utils.HeaderSessionManager;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.mylibrary.volley.AppController;
import com.blackmaria.partner.mylibrary.volley.ServiceRequest;
import com.blackmaria.partner.mylibrary.volley.VolleyMultipartRequest;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.countrycodepicker.CountryPicker;
import com.countrycodepicker.CountryPickerListener;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.yalantis.ucrop.util.BitmapLoadUtils.exifToDegrees;

/**
 * Created by GANESH on 10-10-2017.
 */

public class RegistrationLoginDetails extends FragmentActivityHockeyApp implements View.OnClickListener {

    private SessionManager sessionManager;
    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    private Dialog dialog;
    CountryPicker picker;
    HeaderSessionManager headerSessionManager;
    private String Agent_Name = "", language_code = "";
    private String sDriverID = "", gcmID = "";
    private static final int PERMISSION_REQUEST_CODE = 111;
    int permissionCode = -1;

    private RelativeLayout RL_exit, RL_photo, RL_countryCode;
    private Button Btn_confirm, Btn_browse;
    private ImageView Iv_profile, Iv_flag;
    private TextView Tv_countryCode, Tv_photoname;
    private boolean isInternetPresent = false;
    private EditText edt_PhobneNum;
    private static final int SELECT_IMAGE_REQUEST = 011;
    private static final int TAKE_PHOTO_REQUEST = 022;
    File imageRoot, destination;
    private Uri mImageCaptureUri, outputUri;
    String appDirectoryName = "";
    private String CarCatId = "", SDriverReferalCode = "", driverLocationPlaceId = "", SimageName = "";
    byte[] array = null;


    private GPSTracker gpsTracker;
    private PinEntryEditText referaledittext;
    private EditText edtReferalCode;
    private Button verifyBtn;
    String sReferralDriverName = "", sReferralDriverPercentage = "", sReferralDriverImage = "", sReferralCode = "",referalStatus="";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_login_details);

        initialize();

    }

    private void initialize() {
        headerSessionManager = new HeaderSessionManager(RegistrationLoginDetails.this);
        sessionManager = new SessionManager(RegistrationLoginDetails.this);
        cd = new ConnectionDetector(RegistrationLoginDetails.this);
        isInternetPresent = cd.isConnectingToInternet();


        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        HashMap<String, String> header = headerSessionManager.getHeaderSession();
        Agent_Name = header.get(HeaderSessionManager.KEY_ID_NAME);
        language_code = header.get(HeaderSessionManager.KEY_LANGUAGE_CODE);
        picker = CountryPicker.newInstance(getResources().getString(R.string.select_country_lable));
        gcmID = user.get(SessionManager.KEY_GCM_ID);

        Intent in = getIntent();
        if (in.hasExtra("vehicleId")) {
            CarCatId = in.getStringExtra("vehicleId");
            driverLocationPlaceId = in.getStringExtra("driverLocationPlaceId");
        }
//        String name = dateToString(new Date(), "yyyy-MM-dd-hh-mm-ss");
//        destination = new File(Environment.getExternalStorageDirectory(), name + ".jpg");
        appDirectoryName = getResources().getString(R.string.app_name);

        RL_exit = (RelativeLayout) findViewById(R.id.RL_exit);
        RL_photo = (RelativeLayout) findViewById(R.id.RL_photo);
        RL_countryCode = (RelativeLayout) findViewById(R.id.RL_flag);
        Iv_flag = (ImageView) findViewById(R.id.img_flag);
        Tv_countryCode = (TextView) findViewById(R.id.txt_country_code);
        Iv_profile = (ImageView) findViewById(R.id.img_profile);
        Btn_browse = (Button) findViewById(R.id.btn_browse);
        Btn_confirm = (Button) findViewById(R.id.btn_confirm);
        Tv_photoname = (TextView) findViewById(R.id.txt_label_photo);
        edt_PhobneNum = (EditText) findViewById(R.id.phonenum_edt);
        referaledittext = (PinEntryEditText) findViewById(R.id.referaledittext);
        edtReferalCode = (EditText) findViewById(R.id.edt_referral_code);
        verifyBtn = (Button) findViewById(R.id.btn_verify);

        RL_exit.setOnClickListener(this);
        RL_photo.setOnClickListener(this);
        Btn_browse.setOnClickListener(this);
        Btn_confirm.setOnClickListener(this);
        RL_countryCode.setOnClickListener(this);
        verifyBtn.setOnClickListener(this);

        gpsTracker = new GPSTracker(RegistrationLoginDetails.this);
        if (gpsTracker.canGetLocation() && gpsTracker.isgpsenabled()) {

            double MyCurrent_lat = gpsTracker.getLatitude();
            double MyCurrent_long = gpsTracker.getLongitude();

            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            try {
                List<Address> addresses = geocoder.getFromLocation(MyCurrent_lat, MyCurrent_long, 1);
                if (addresses != null && !addresses.isEmpty()) {

                    String Str_getCountryCode = addresses.get(0).getCountryCode();
                    if (Str_getCountryCode.length() > 0 && !Str_getCountryCode.equals(null) && !Str_getCountryCode.equals("null")) {
                        String Str_countyCode = CountryDialCode.getCountryCode(Str_getCountryCode);
                        Tv_countryCode.setText(Str_countyCode);
                        String drawableName = "flag_"
                                + Str_getCountryCode.toLowerCase(Locale.ENGLISH);
                        Iv_flag.setImageResource(getResId(drawableName));
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {
                picker.dismiss();
                Tv_countryCode.setText(dialCode);

                String drawableName = "flag_"
                        + code.toLowerCase(Locale.ENGLISH);
                Iv_flag.setImageResource(getResId(drawableName));
//                cell.imageView.setImageResource(getResId(drawableName));

                // close keyboard
                /*InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(Rl_countryCode.getWindowToken(), 0);*/

                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(Tv_countryCode.getWindowToken(), 0);
            }
        });

    }

    @Override
    public void onClick(View view) {

        if (view == RL_exit) {

            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);

        } else if (view == RL_photo) {
            checkAndGrantPermission(0);
        } else if (view == Btn_browse) {
            checkAndGrantPermission(1);
        } else if (view == Btn_confirm) {

            if (!isValidPhoneNumber(edt_PhobneNum.getText().toString())) {
                AlertError(getResources().getString(R.string.action_error_phone_number), getResources().getString(R.string.login_page_alert_phoneNo));
            } else if (referaledittext.getText().toString().trim().length() <= 0) {
                AlertError(getResources().getString(R.string.timer_label_alert_sorry), getResources().getString(R.string.profile_label_alert_pincode));
            } else if (referaledittext.getText().toString().trim().length() < 6) {
                AlertError(getResources().getString(R.string.timer_label_alert_sorry), getResources().getString(R.string.profile_label_alert_pincodes));
            }else if (edtReferalCode.getText().toString().trim().length()>0 && referalStatus.equalsIgnoreCase("0")) {
                AlertError(getResources().getString(R.string.timer_label_alert_sorry), getResources().getString(R.string.profile_label_alert_referal));
            } else {
                if (isInternetPresent) {
                    CheckMobileNum_verify(Iconstant.Verifymobile);
                } else {
                    Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                }



            }
        } else if (view == RL_countryCode) {
            picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
        } else if (view == verifyBtn) {
            if (isInternetPresent) {
                if (edtReferalCode.getText().toString().trim().length() > 0) {
                    applyReferalCode(Iconstant.DriverReg_applyReferal_URl);
                } else {
                    AlertError(getResources().getString(R.string.timer_label_alert_sorry), getResources().getString(R.string.otp_page_referral_error));
                }

            } else {
                Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
            }
        }
    }

    // validating Phone Number
    public static final boolean isValidPhoneNumber(CharSequence target) {
        if (target == null || TextUtils.isEmpty(target) || target.length() <= 5) {
            return false;
        } else {
            return Patterns.PHONE.matcher(target).matches();
        }
    }

    // If code is 0 -> Its For Camera
    // If code is 1 -> Its For Pick From Gallery
    private void checkAndGrantPermission(int code) {

        // Permission Code -> For Identifying Whether the click is from take photo (or) Pick Image From Galley
        permissionCode = code;

        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            if (!checkCameraPermission() || !checkReadExternalStoragePermission() || !checkWriteExternalStoragePermission()) {
                requestPermission();
            } else {
                if (code == 0) {
                    cameraIntent();
                } else if (code == 1) {
                    galleryIntent();
                }
            }
        } else {
            if (code == 0) {
                cameraIntent();
            } else if (code == 1) {
                galleryIntent();
            }
        }

    }

    private boolean checkCameraPermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkReadExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkWriteExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // If permissionCode is 0 -> Its For Camera
                    // If permissionCode is 1 -> Its For Pick From Gallery
                    if (permissionCode == 0) {
                        cameraIntent();
                    } else if (permissionCode == 1) {
                        galleryIntent();
                    }
                }
                break;
        }
    }

    public String dateToString(Date date, String format) {
        SimpleDateFormat df = new SimpleDateFormat(format);
        return df.format(date);
    }

    private void cameraIntent() {
        try {
            Intent pictureIntent = new Intent(
                    MediaStore.ACTION_IMAGE_CAPTURE);
            if (pictureIntent.resolveActivity(getPackageManager()) != null) {
                //Create a file to store the image
                imageRoot = new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES), appDirectoryName);
            }
            if (imageRoot != null) {
                String name = dateToString(new Date(), "yyyy-MM-dd-hh-mm-ss");
                destination = new File(imageRoot, name + ".jpg");
                Uri photoURI = FileProvider.getUriForFile(this, "com.blackmaria.provider", destination);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        photoURI);
                pictureIntent.putExtra("android.intent.extras.CAMERA_FACING", 1);
                startActivityForResult(pictureIntent,
                        TAKE_PHOTO_REQUEST);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void galleryIntent() {
        try {

            imageRoot = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES), appDirectoryName);

            if (!imageRoot.exists()) {
                imageRoot.mkdir();
            }
            Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setType("image/*");
            startActivityForResult(intent, SELECT_IMAGE_REQUEST);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_IMAGE_REQUEST) {
                onSelectFromGalleryResult(data);
            } else if (requestCode == TAKE_PHOTO_REQUEST) {
                onCaptureImageResult(data);
            } else if (requestCode == UCrop.REQUEST_CROP) {
                onCroppedImageResult(data);
            } else if (resultCode == UCrop.RESULT_ERROR) {
                final Throwable cropError = UCrop.getError(data);
            }
        }
    }


    private void onCaptureImageResult(Intent data) {
        try {
            String imagePath = destination.getAbsolutePath();
            mImageCaptureUri = Uri.fromFile(new File(imagePath));
            outputUri = mImageCaptureUri;

//            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), mImageCaptureUri);

            UCrop.Options options = new UCrop.Options();
            options.setStatusBarColor(getResources().getColor(R.color.black_color));
            options.setToolbarColor(getResources().getColor(R.color.app_primary_color));
            options.setMaxBitmapSize(800);

            UCrop.of(mImageCaptureUri, outputUri)
                    .withAspectRatio(1, 1)
                    .withMaxResultSize(8000, 8000)
                    .withOptions(options)
                    .start(RegistrationLoginDetails.this);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onSelectFromGalleryResult(Intent data) {

        try {

//                    imagePath = destination.getAbsolutePath();
            mImageCaptureUri = data.getData();


            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), mImageCaptureUri);

            if (!imageRoot.exists()) {
                imageRoot.mkdir();
            }

            String name = dateToString(new Date(), "yyyy-MM-dd-hh-mm-ss");
            destination = new File(imageRoot, name + ".jpg");

            outputUri = Uri.fromFile(destination);

            UCrop.Options options = new UCrop.Options();
            options.setStatusBarColor(getResources().getColor(R.color.black_color));
            options.setToolbarColor(getResources().getColor(R.color.app_primary_color));
            options.setMaxBitmapSize(800);

            UCrop.of(mImageCaptureUri, outputUri)
                    .withAspectRatio(1, 1)
                    .withMaxResultSize(8000, 8000)
                    .withOptions(options)
                    .start(RegistrationLoginDetails.this);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void onCroppedImageResult(Intent data) {

        final Uri resultUri = UCrop.getOutput(data);

        Log.d("Crop success", "" + resultUri);
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);

            if (bitmap == null) {
                Log.d("Bitmap", "null");
            } else {
                Log.d("Bitmap", "not null");
                Iv_profile.setImageBitmap(bitmap);
                Tv_photoname.setVisibility(View.GONE);
            }
            Tv_photoname.setVisibility(View.GONE);
         /*   int bytes = bitmap.getByteCount();
            ByteBuffer buffer = ByteBuffer.allocate(bytes); //Create a new buffer
            bitmap.copyPixelsToBuffer(buffer); //Move the byte data to the buffer
            array = buffer.array();
*/
            Bitmap thumbnail = bitmap;
            final String picturePath = resultUri.getPath();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            File curFile = new File(picturePath);
            try {
                ExifInterface exif = new ExifInterface(curFile.getPath());
                int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                int rotationInDegrees = exifToDegrees(rotation);

                Matrix matrix = new Matrix();
                if (rotation != 0f) {
                    matrix.preRotate(rotationInDegrees);
                }
                thumbnail = Bitmap.createBitmap(thumbnail, 0, 0, thumbnail.getWidth(), thumbnail.getHeight(), matrix, true);
            } catch (IOException ex) {
                Log.e("TAG", "Failed to get Exif data", ex);
            }

            thumbnail.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
            array = byteArrayOutputStream.toByteArray();

            if (isInternetPresent) {
                UploadDriverImage(Iconstant.DriverReg_upload_Image_URl);

            } else {
                Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void UploadDriverImage(String url) {

        dialog = new Dialog(RegistrationLoginDetails.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {

                System.out.println("------------- image response-----------------" + response.data);


                String resultResponse = new String(response.data);
                System.out.println("-------------  response-----------------" + resultResponse);
                String sStatus = "", sResponse = "", SUser_image = "", Smsg = "";
                try {
                    JSONObject jsonObject = new JSONObject(resultResponse);
                    sStatus = jsonObject.getString("status");
                    JSONObject imagObj = jsonObject.getJSONObject("response");
                    if (sStatus.equalsIgnoreCase("1")) {
                        SimageName = imagObj.getString("image_name");
                    } else {
                        Smsg = jsonObject.getString("response");
                        Alert(getResources().getString(R.string.action_error), Smsg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                dialog.dismiss();

                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");

                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message + " Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message + " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message + " Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
               /* params.put("user_id", UserID);
                System.out.println("user_id---------------" + UserID);*/
                return params;
            }

            @Override
            protected Map<String, VolleyMultipartRequest.DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                params.put("image", new DataPart("blackmariadriver.jpg", array));
                //   System.out.println("user_image--------edit------" + byteArray);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Authkey", Agent_Name);
                headers.put("isapplication", Iconstant.cabily_IsApplication);
                headers.put("applanguage", language_code);
                headers.put("apptype", Iconstant.cabily_AppType);
                headers.put("driverid", sDriverID);
                headers.put("apptoken", gcmID);

                System.out.println("------------Headers-----" + headers);
                return headers;
            }

        };

        //to avoid repeat request Multiple Time
        DefaultRetryPolicy retryPolicy = new DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        multipartRequest.setRetryPolicy(retryPolicy);
        multipartRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        multipartRequest.setShouldCache(false);
        AppController.getInstance().addToRequestQueue(multipartRequest);

    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(RegistrationLoginDetails.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.close_lable), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    private void applyReferalCode(String Url) {

        dialog = new Dialog(RegistrationLoginDetails.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("code", edtReferalCode.getText().toString().trim());
        System.out.println("--------------applyReferalCode Url-------------------" + Url);
        System.out.println("--------------applyReferalCode jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(RegistrationLoginDetails.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------applyReferalCode Response-------------------" + response);
                String status = "", Sresponse = "";
                try {

                    JSONObject object = new JSONObject(response);

                    status = object.getString("status");
                    Sresponse = object.getString("response");
                    referalStatus=status;
                    if (status.equalsIgnoreCase("1")) {

                        sReferralDriverName = object.getString("driver_name");
                        sReferralDriverPercentage = object.getString("ref_per");
                        sReferralDriverImage = object.getString("driver_image");
                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                if (status.equalsIgnoreCase("1")) {
//                    Iv_insertReferral.setVisibility(View.GONE);
                    sReferralCode = edtReferalCode.getText().toString().trim();
                    showVerifiedAlert();
                } else {
                    sReferralCode = "";
                    Alert(getResources().getString(R.string.action_error), Sresponse);
                }


                if (dialog != null) {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void CheckMobileNum_verify(String Url) {

        dialog = new Dialog(RegistrationLoginDetails.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("dail_code", Tv_countryCode.getText().toString().trim());
        jsonParams.put("mobile_number", edt_PhobneNum.getText().toString().trim());

        System.out.println("--------------CheckMobileNum Url-------------------" + Url);
        System.out.println("--------------CheckMobileNum jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(RegistrationLoginDetails.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------CheckMobileNum Response-------------------" + response);
                String status = "", Sresponse = "",otpStatus="",otpPin="";
                try {

                    JSONObject object = new JSONObject(response);

                    status = object.getString("status");
                    Sresponse = object.getString("response");
                    if (status.equalsIgnoreCase("1")) {
                        otpStatus = object.getString("otp_status");
                        otpPin = object.getString("otp");
                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (status.equalsIgnoreCase("1")) {

                    sessionManager.setOtpstatusAndPin(otpStatus,otpPin);

                    Intent intent = new Intent(RegistrationLoginDetails.this, RegistrationPersonalDetails.class);
                    intent.putExtra("driverVehicleId", CarCatId);
                    intent.putExtra("driverLocationPlaceId", driverLocationPlaceId);
                    intent.putExtra("driverProImage", SimageName);
                    intent.putExtra("driverCountryCode", Tv_countryCode.getText().toString().trim());
                    intent.putExtra("driverPhoneNumber", edt_PhobneNum.getText().toString().trim());
                    intent.putExtra("driverReferelCode", sReferralCode);
                    intent.putExtra("driverPinCode", referaledittext.getText().toString().trim());
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                } else {
                    Alert("Sorry",Sresponse);
                }
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }
    private void CheckMobileNum(String Url) {

        dialog = new Dialog(RegistrationLoginDetails.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("dail_code", Tv_countryCode.getText().toString().trim());
        jsonParams.put("mobile_number", edt_PhobneNum.getText().toString().trim());
        jsonParams.put("lat", String.valueOf(gpsTracker.getLatitude()));
        jsonParams.put("lon", String.valueOf(gpsTracker.getLongitude()));


        System.out.println("--------------CheckMobileNum Url-------------------" + Url);
        System.out.println("--------------CheckMobileNum jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(RegistrationLoginDetails.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------CheckMobileNum Response-------------------" + response);
                String status = "", Sresponse = "",otpStatus="",otpPin="";
                try {

                    JSONObject object = new JSONObject(response);

                    status = object.getString("status");
                    Sresponse = object.getString("response");
                    if (status.equalsIgnoreCase("1")) {
                        otpStatus = object.getString("otp_status");
                        otpPin = object.getString("otp");
                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (status.equalsIgnoreCase("1")) {

                    sessionManager.setOtpstatusAndPin(otpStatus,otpPin);

                    Intent intent = new Intent(RegistrationLoginDetails.this, RegistrationPersonalDetails.class);
                    intent.putExtra("driverVehicleId", CarCatId);
                    intent.putExtra("driverLocationPlaceId", driverLocationPlaceId);
                    intent.putExtra("driverProImage", SimageName);
                    intent.putExtra("driverCountryCode", Tv_countryCode.getText().toString().trim());
                    intent.putExtra("driverPhoneNumber", edt_PhobneNum.getText().toString().trim());
                    intent.putExtra("driverReferelCode", sReferralCode);
                    intent.putExtra("driverPinCode", referaledittext.getText().toString().trim());
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                } else {
                    Alert("",getResources().getString(R.string.action_error_phone_invalid2));
                }
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    @SuppressLint("SetTextI18n")
    private void showVerifiedAlert() {

        //--------Adjusting Dialog width and height-----
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 85% of the screen
        int screenHeight = (int) (metrics.heightPixels * 0.80);//fill only 80% of the screen

        final Dialog verifiedDialog = new Dialog(RegistrationLoginDetails.this, R.style.SlideUpDialog);
        verifiedDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        verifiedDialog.setContentView(R.layout.alert_verified_status);
        verifiedDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        verifiedDialog.getWindow().setLayout(screenWidth, screenHeight);

        final ImageView Iv_profile = (ImageView) verifiedDialog.findViewById(R.id.img_profile);
        final TextView Tv_driverName = (TextView) verifiedDialog.findViewById(R.id.txt_user_name);
        final TextView Tv_content1 = (TextView) verifiedDialog.findViewById(R.id.txt_content1);
        final TextView Tv_earnAtLabel = (TextView) verifiedDialog.findViewById(R.id.txt_label_earn_percentage);
        final TextView Tv_earnPercentage = (TextView) verifiedDialog.findViewById(R.id.txt_earn_percentage);
        final Button Btn_proceed = (Button) verifiedDialog.findViewById(R.id.btn_proceed);

        if (sReferralDriverImage.length() > 0) {
            Picasso.with(RegistrationLoginDetails.this).load(sReferralDriverImage).placeholder(R.drawable.no_user_profile).error(R.drawable.no_user_profile).resize(150, 150).memoryPolicy(MemoryPolicy.NO_CACHE).into(Iv_profile);
        }
        Tv_driverName.setText(sReferralDriverName);
        Tv_content1.setText(sReferralDriverName + "\t" + getResources().getString(R.string.referral_verified_content1));
        Tv_earnAtLabel.setText(sReferralDriverName + "\t" + getResources().getString(R.string.referral_verified_earn_at));
        Tv_earnPercentage.setText(sReferralDriverPercentage + "%");

        Btn_proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifiedDialog.dismiss();
            }
        });

        verifiedDialog.show();

    }

    private void AlertError(String title, String message) {

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        Snackbar bar = Snackbar.with(RegistrationLoginDetails.this);
        bar.addView(view);
        bar.colorResource(R.color.dark_red);
        bar.position(Snackbar.SnackbarPosition.TOP);
        bar.type(SnackbarType.MULTI_LINE);
        bar.duration(Snackbar.SnackbarDuration.LENGTH_SHORT);
        bar.animation(true);

        SnackbarManager.show(bar, RegistrationLoginDetails.this);


    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }
    private int getResId(String drawableName) {

        try {
            Class<com.countrycodepicker.R.drawable> res = com.countrycodepicker.R.drawable.class;
            Field field = res.getField(drawableName);
            int drawableId = field.getInt(null);
            return drawableId;
        } catch (Exception e) {
            Log.e("CountryCodePicker", "Failure to get drawable id.", e);
        }
        return -1;
    }

}
