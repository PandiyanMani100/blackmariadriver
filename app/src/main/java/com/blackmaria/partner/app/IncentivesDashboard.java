package com.blackmaria.partner.app;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.Pojo.IncentiveDetail;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by GANESH on 22-09-2017.
 */

public class IncentivesDashboard extends ActivityHockeyApp implements View.OnClickListener {

    private TextView Tv_lastMonth, Tv_thisMonth, Tv_thisYear, Tv_incentiveAmount, Tv_highlightsDate,
            Tv_amountWeeklyTarget, Tv_amountDailyTarget, Tv_amountMiles, Tv_amountScheduled, Tv_amountSpecial, Tv_amountEstimated;
    private ImageView Iv_back, Iv_home, Iv_info;
    private RelativeLayout RL_scheduled, RL_special, RL_statement;
    private LinearLayout LL_weeklyTarget, LL_dailyTarget, LL_mileIncentives;


    private SessionManager sessionManager;
    private ConnectionDetector cd;
    private Dialog dialog;
    private ServiceRequest mRequest;

    final int ColorTransWhite40 = Color.parseColor("#40ffffff");
    final int ColorTransWhite70 = Color.parseColor("#70ffffff");
    private String sDriverID = "", sTotalAmount = "", sCurrency = "", sMonthAmount = "", sYearAmount = "", sLastMonthAmount = "",
            sTotalPayoutAmount = "", sProceedWithdrawStatus = "", sProceedWithdrawError = "", sWeeklyTarget = "", sDailyTarget = "",
            sMilesIncentives = "", sScheduledIncentives = "", sSpecialIncentives = "", sEstimatedThisMonth = "", sDisplayMonth = "";

    private ArrayList<IncentiveDetail> incentivesList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.driver_incentives);

        initialize();

    }

    private void initialize() {

        sessionManager = new SessionManager(IncentivesDashboard.this);
        cd = new ConnectionDetector(IncentivesDashboard.this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        incentivesList = new ArrayList<IncentiveDetail>();

        Iv_back = (ImageView) findViewById(R.id.img_back);
        Iv_home = (ImageView) findViewById(R.id.img_home);

        Tv_lastMonth = (TextView) findViewById(R.id.txt_label_last_month);
        Tv_thisMonth = (TextView) findViewById(R.id.txt_label_this_month);
        Tv_thisYear = (TextView) findViewById(R.id.txt_label_this_year);

        Tv_incentiveAmount = (TextView) findViewById(R.id.txt_incentive_amount);

        Tv_highlightsDate = (TextView) findViewById(R.id.txt_label_highlight);

        Tv_amountWeeklyTarget = (TextView) findViewById(R.id.txt_amount_weekly_target);
        Tv_amountDailyTarget = (TextView) findViewById(R.id.txt_amount_daily_target);
        Tv_amountMiles = (TextView) findViewById(R.id.txt_amount_miles_incentives);
        Tv_amountScheduled = (TextView) findViewById(R.id.txt_amount_scheduled_incentives);
        Tv_amountSpecial = (TextView) findViewById(R.id.txt_amount_special_incentives);
        Tv_amountEstimated = (TextView) findViewById(R.id.txt_amount_estimated_this_month);

        Iv_info = (ImageView) findViewById(R.id.img_info);

        RL_statement = (RelativeLayout) findViewById(R.id.RL_statement);

        RL_scheduled = (RelativeLayout) findViewById(R.id.RL_scheduled);
        RL_special = (RelativeLayout) findViewById(R.id.RL_special);
        LL_weeklyTarget = (LinearLayout) findViewById(R.id.LL_weekly_target);
        LL_dailyTarget = (LinearLayout) findViewById(R.id.LL_daily_target);
        LL_mileIncentives = (LinearLayout) findViewById(R.id.LL_mile_incentives);

        Iv_back.setOnClickListener(this);
        Iv_home.setOnClickListener(this);
        Iv_info.setOnClickListener(this);
        Tv_lastMonth.setOnClickListener(this);
        Tv_thisMonth.setOnClickListener(this);
        Tv_thisYear.setOnClickListener(this);
        RL_statement.setOnClickListener(this);
        RL_scheduled.setOnClickListener(this);
        RL_special.setOnClickListener(this);
        LL_weeklyTarget.setOnClickListener(this);
        LL_dailyTarget.setOnClickListener(this);
        LL_mileIncentives.setOnClickListener(this);


        try {

            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("MMMM yyyy");
            String currentMonthYear = sdf.format(cal.getTime());

            if (currentMonthYear.length() > 0) {
                sDisplayMonth = getResources().getString(R.string.incentives_page_label_highlight) + " " + currentMonthYear;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        if (cd.isConnectingToInternet()) {
            IncentivesDashboardRequest(Iconstant.incentives_dashboard_url);
        } else {
            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }


    }


    @SuppressLint("WrongConstant")
    private void IncentivesDashboardRequest(String Url) {

        dialog = new Dialog(IncentivesDashboard.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);

        System.out.println("--------------IncentivesDashboard Url-------------------" + Url);
        System.out.println("--------------IncentivesDashboard jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(IncentivesDashboard.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------IncentivesDashboard Response-------------------" + response);
                String status = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {
                            JSONObject jsonObject = object.getJSONObject("response");

                            if (jsonObject.length() > 0) {

                                sCurrency = jsonObject.getString("currency");
                                sTotalAmount = sCurrency + " " + jsonObject.getString("total_amount");

                                sMonthAmount = sCurrency + " " + jsonObject.getString("month_amount");
                                sYearAmount = sCurrency + " " + jsonObject.getString("year_amount");
                                sLastMonthAmount = sCurrency + " " + jsonObject.getString("last_month_amount");

                                sTotalPayoutAmount = sCurrency + " " + jsonObject.getString("total_payout_amount");

                                sProceedWithdrawStatus = jsonObject.getString("proceed_status");
                                sProceedWithdrawError = jsonObject.getString("error_message");

                                Object objMonthStatement = jsonObject.get("month_statement");
                                if (objMonthStatement instanceof JSONObject) {

                                    JSONObject jobjMonthStatement = jsonObject.getJSONObject("month_statement");

                                    if (jobjMonthStatement.length() > 0) {

                                        if(jobjMonthStatement.has("week_trip")){
                                            sWeeklyTarget = sCurrency + " " + jobjMonthStatement.getString("week_trip");
                                        }
                                        if(jobjMonthStatement.has("daily_trip")){
                                            sDailyTarget = sCurrency + " " + jobjMonthStatement.getString("daily_trip");
                                        }
                                        if(jobjMonthStatement.has("daily_mile")){
                                            sMilesIncentives = sCurrency + " " + jobjMonthStatement.getString("daily_mile");
                                        }
                                        if(jobjMonthStatement.has("schedule_hours")){
                                            sScheduledIncentives = sCurrency + " " + jobjMonthStatement.getString("schedule_hours");
                                        }
                                        if(jobjMonthStatement.has("sos")){
                                            sSpecialIncentives = sCurrency + " " + jobjMonthStatement.getString("sos");
                                        }
                                        if(jobjMonthStatement.has("total_amount")){
                                            sEstimatedThisMonth = sCurrency + " " + jobjMonthStatement.getString("total_amount");
                                        }
                                    }
                                }

                                Object objIncentives = jsonObject.get("incetives_count");
                                if (objIncentives instanceof JSONArray) {

                                    JSONArray jarIncentives = jsonObject.getJSONArray("incetives_count");

                                    if (jarIncentives.length() > 0) {

                                        incentivesList = new ArrayList<IncentiveDetail>();
                                        for (int i = 0; i < jarIncentives.length(); i++) {

                                            JSONObject jobjIncentive = jarIncentives.getJSONObject(i);

                                            IncentiveDetail detail = new IncentiveDetail();
                                            String incetives_type = jobjIncentive.getString("incetives_type");
                                            String target = jobjIncentive.getString("target");
                                            String completed_level = jobjIncentive.getString("completed_level");
                                            String target_km = jobjIncentive.getString("target_km");
                                            String target_distance = jobjIncentive.getString("target_distance");
                                            String incetive_amount = jobjIncentive.getString("incetive_amount");
                                            String currency = jobjIncentive.getString("currency");
                                            String incentive_period = jobjIncentive.getString("incentive_period");
                                            String current_period = jobjIncentive.getString("current_period");
                                            String day_time = jobjIncentive.getString("day_time");

                                            detail.setIncetives_type(incetives_type);
                                            detail.setTarget(target);
                                            detail.setCompleted_level(completed_level);
                                            detail.setTarget_km(target_km);
                                            detail.setTarget_distance(target_distance);
                                            detail.setIncetive_amount(incetive_amount);
                                            detail.setCurrency(currency);
                                            detail.setIncentive_period(incentive_period);
                                            detail.setCurrent_period(current_period);
                                            detail.setDay_time(day_time);

                                            incentivesList.add(detail);

                                        }

                                    }

                                }

                            }

                        } else {
                            String sResponse = object.getString("response");
                            Alert(getResources().getString(R.string.action_error), sResponse);
                        }


                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (status.equalsIgnoreCase("1")) {

                    Tv_thisMonth.setBackgroundColor(ColorTransWhite70);
                    Tv_lastMonth.setBackgroundColor(ColorTransWhite40);
                    Tv_thisYear.setBackgroundColor(ColorTransWhite40);

                    if (sMonthAmount.length() > 0) {
                        Tv_incentiveAmount.setText(sMonthAmount);
                    }

                    Tv_amountWeeklyTarget.setText(sWeeklyTarget);
                    Tv_amountDailyTarget.setText(sDailyTarget);
                    Tv_amountMiles.setText(sMilesIncentives);
                    Tv_amountScheduled.setText(sScheduledIncentives);
                    Tv_amountSpecial.setText(sSpecialIncentives);
                    Tv_amountEstimated.setText(sEstimatedThisMonth);


                    if (sDisplayMonth.length() > 0) {
                        Tv_highlightsDate.setText(sDisplayMonth);
                    }

                }

                if (dialog != null) {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(IncentivesDashboard.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {

            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);

            return true;
        }
        return false;
    }

    @Override
    public void onClick(View view) {

        if (view == Iv_back || view == Iv_home) {

            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);

        } else if (view == Iv_info) {

            moreInfoDialog();

        } else if (view == Tv_thisMonth) {

            Tv_thisMonth.setBackgroundColor(ColorTransWhite70);
            Tv_lastMonth.setBackgroundColor(ColorTransWhite40);
            Tv_thisYear.setBackgroundColor(ColorTransWhite40);

//            Tv_incentiveAmount.setText(sMonthAmount);

            if(!sMonthAmount.equalsIgnoreCase("")){
                Tv_incentiveAmount.setText(sMonthAmount);
            }else{
                Tv_incentiveAmount.setText(sCurrency+" "+"0.00");
            }


        } else if (view == Tv_lastMonth) {

            Tv_thisMonth.setBackgroundColor(ColorTransWhite40);
            Tv_lastMonth.setBackgroundColor(ColorTransWhite70);
            Tv_thisYear.setBackgroundColor(ColorTransWhite40);

//            Tv_incentiveAmount.setText(sLastMonthAmount);
            if(!sLastMonthAmount.equalsIgnoreCase("")){
                Tv_incentiveAmount.setText(sLastMonthAmount);
            }else{
                Tv_incentiveAmount.setText(sCurrency+" "+"0.00");
            }

        } else if (view == Tv_thisYear) {

            Tv_thisMonth.setBackgroundColor(ColorTransWhite40);
            Tv_lastMonth.setBackgroundColor(ColorTransWhite40);
            Tv_thisYear.setBackgroundColor(ColorTransWhite70);

            if(!sYearAmount.equalsIgnoreCase("")){
                Tv_incentiveAmount.setText(sYearAmount);
            }else{
                Tv_incentiveAmount.setText(sCurrency+" "+"0.00");
            }


        } else if (view == RL_statement) {

            Intent intent = new Intent(IncentivesDashboard.this, IncentiveStatementNew.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.exit);

        } else if (view == RL_scheduled) {

            IncentiveDetail detail = null;

            for (int i = 0; i < incentivesList.size(); i++) {
                String incentiveType = incentivesList.get(i).getIncetives_type();
                if (incentiveType.equalsIgnoreCase(getResources().getString(R.string.incentives_type_label_schedule_hours))) {
                    detail = incentivesList.get(i);
                }
            }

            if (detail != null) {
                incentivesDialog("scheduled", detail);
            } else {
                Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.incentives_type_module_not_available));
            }

        } else if (view == RL_special) {

            IncentiveDetail detail = null;

            for (int i = 0; i < incentivesList.size(); i++) {
                String incentiveType = incentivesList.get(i).getIncetives_type();
                if (incentiveType.equalsIgnoreCase(getResources().getString(R.string.incentives_type_label_sos))) {
                    detail = incentivesList.get(i);
                }
            }

            if (detail != null) {
                incentivesSOSDialog(detail);
            } else {
                Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.incentives_type_module_not_available));
            }

        } else if (view == LL_weeklyTarget) {

            IncentiveDetail detail = null;

            for (int i = 0; i < incentivesList.size(); i++) {
                String incentiveType = incentivesList.get(i).getIncetives_type();
                if (incentiveType.equalsIgnoreCase(getResources().getString(R.string.incentives_type_label_week_trip))) {
                    detail = incentivesList.get(i);
                }
            }

            if (detail != null) {
                incentivesDialog("weekly_target", detail);
            } else {
                Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.incentives_type_module_not_available));
            }

        } else if (view == LL_dailyTarget) {

            IncentiveDetail detail = null;

            for (int i = 0; i < incentivesList.size(); i++) {
                String incentiveType = incentivesList.get(i).getIncetives_type();
                if (incentiveType.equalsIgnoreCase(getResources().getString(R.string.incentives_type_label_daily_trip))) {
                    detail = incentivesList.get(i);
                }
            }

            if (detail != null) {
                incentivesDialog("daily_target", detail);
            } else {
                Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.incentives_type_module_not_available));
            }

        } else if (view == LL_mileIncentives) {

            IncentiveDetail detail = null;

            for (int i = 0; i < incentivesList.size(); i++) {
                String incentiveType = incentivesList.get(i).getIncetives_type();
                if (incentiveType.equalsIgnoreCase(getResources().getString(R.string.incentives_type_label_daily_mile))) {
                    detail = incentivesList.get(i);
                }
            }

            if (detail != null) {
                incentivesDialog("mile", detail);
            } else {
                Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.incentives_type_module_not_available));
            }

        }

    }


    @SuppressLint("SetTextI18n")
    private void incentivesDialog(String type, IncentiveDetail detail) {

        //--------Adjusting Dialog width & Height-----
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 85% of the screen
        int screenHeight = (int) (metrics.heightPixels * 0.85);//fill only 85% of the screen

        final Dialog incentiveDetailsDialog = new Dialog(IncentivesDashboard.this, R.style.SlideUpDialog);
        incentiveDetailsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        incentiveDetailsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        incentiveDetailsDialog.setCancelable(true);
        /*if (type.equalsIgnoreCase("scheduled")) {
            incentiveDetailsDialog.setContentView(R.layout.alert_incentive_detail_schedule);
        } else {
            incentiveDetailsDialog.setContentView(R.layout.alert_incentive_details);
        }*/
        incentiveDetailsDialog.setContentView(R.layout.alert_incentive_details);
        incentiveDetailsDialog.getWindow().setLayout(screenWidth, screenHeight);

        ImageView Iv_incentiveType = (ImageView) incentiveDetailsDialog.findViewById(R.id.img_incentive_type);
        TextView Tv_incentiveAmount = (TextView) incentiveDetailsDialog.findViewById(R.id.txt_incentive_amount);
        TextView Tv_targetIncentiveLabel = (TextView) incentiveDetailsDialog.findViewById(R.id.txt_label_target_incentive);
        TextView Tv_currentIncentiveLabel = (TextView) incentiveDetailsDialog.findViewById(R.id.txt_label_current_incentive);
        TextView Tv_targetIncentive = (TextView) incentiveDetailsDialog.findViewById(R.id.txt_target_incentive);
        TextView Tv_currentIncentive = (TextView) incentiveDetailsDialog.findViewById(R.id.txt_current_incentive);
        TextView Tv_period = (TextView) incentiveDetailsDialog.findViewById(R.id.txt_incentive_period);
        ImageView Iv_car = (ImageView) incentiveDetailsDialog.findViewById(R.id.img_car);
        ImageView Iv_clock = (ImageView) incentiveDetailsDialog.findViewById(R.id.img_clock);
        Button Btn_close = (Button) incentiveDetailsDialog.findViewById(R.id.btn_close);

        if (incentivesList.size() > 0) {
            if (type.equalsIgnoreCase("daily_target")) {

                Iv_incentiveType.setImageResource(R.drawable.steering_blue);
                Tv_incentiveAmount.setText(detail.getCurrency() + " " + detail.getIncetive_amount());
                Tv_targetIncentiveLabel.setText(getResources().getString(R.string.incentives_page_label_today_trip_target));
                Tv_targetIncentive.setText(detail.getTarget());
                Tv_currentIncentiveLabel.setText(getResources().getString(R.string.incentives_page_label_your_current_trip));
                Tv_currentIncentive.setText(detail.getCompleted_level());
                Tv_period.setText(detail.getIncentive_period());

            } else if (type.equalsIgnoreCase("weekly_target")) {

                Iv_incentiveType.setImageResource(R.drawable.key_blue);
                Tv_incentiveAmount.setText(detail.getCurrency() + " " + detail.getIncetive_amount());
                Tv_targetIncentiveLabel.setText(getResources().getString(R.string.incentives_page_label_weekly_trip_target));
                Tv_targetIncentive.setText(detail.getTarget());
                Tv_currentIncentiveLabel.setText(getResources().getString(R.string.incentives_page_label_your_trip_level));
                Tv_currentIncentive.setText(detail.getCompleted_level());
                Tv_period.setText(detail.getIncentive_period());

            } else if (type.equalsIgnoreCase("mile")) {

                Iv_incentiveType.setImageResource(R.drawable.distance_meter_blue);
                Tv_incentiveAmount.setText(detail.getCurrency() + " " + detail.getIncetive_amount());
                Tv_targetIncentiveLabel.setText(getResources().getString(R.string.incentives_page_label_mileage_incentives));
                Tv_targetIncentive.setText(detail.getTarget_km());
                Tv_currentIncentiveLabel.setText(getResources().getString(R.string.incentives_page_label_your_mileage));
                Tv_currentIncentive.setText(detail.getTarget_distance());
                Tv_period.setText(detail.getIncentive_period());

            } else if (type.equalsIgnoreCase("scheduled")) {

                Iv_incentiveType.setImageResource(R.drawable.img_foc_blue);
                Tv_incentiveAmount.setText(detail.getCurrency() + " " + detail.getIncetive_amount());
                Tv_targetIncentiveLabel.setText(getResources().getString(R.string.incentives_page_label_incentives_title));
                Tv_targetIncentive.setText(detail.getDay_time());
                Tv_currentIncentiveLabel.setText(getResources().getString(R.string.incentives_page_label_current_status));
                Tv_currentIncentive.setText(detail.getCurrent_period());
                Tv_period.setText(detail.getIncentive_period());
                Iv_car.setVisibility(View.GONE);
                Iv_clock.setVisibility(View.GONE);

            }
        }

        Btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                incentiveDetailsDialog.dismiss();
            }
        });

        incentiveDetailsDialog.show();


    }


    private void incentivesSOSDialog(IncentiveDetail detail) {

        //--------Adjusting Dialog width & Height-----
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 80% of the screen
        int screenHeight = (int) (metrics.heightPixels * 0.85);//fill only 80% of the screen

        final Dialog incentiveSOSDialog = new Dialog(IncentivesDashboard.this, R.style.SlideUpDialog);
        incentiveSOSDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        incentiveSOSDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        incentiveSOSDialog.setCancelable(true);
        incentiveSOSDialog.setContentView(R.layout.alert_incentive_detail_sos);
        incentiveSOSDialog.getWindow().setLayout(screenWidth, screenHeight);
        TextView Tv_incentiveAmount = (TextView) incentiveSOSDialog.findViewById(R.id.txt_incentive_amount);
        TextView Tv_period = (TextView) incentiveSOSDialog.findViewById(R.id.txt_incentive_period);
        Tv_incentiveAmount.setText(detail.getCurrency() + " " + detail.getIncetive_amount());
        Tv_period.setText(detail.getIncentive_period());
        final Button Btn_close = (Button) incentiveSOSDialog.findViewById(R.id.btn_close);

        Btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                incentiveSOSDialog.dismiss();
            }
        });

        incentiveSOSDialog.show();

    }


    private void moreInfoDialog() {

        //--------Adjusting Dialog width & Height-----
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 85% of the screen
        int screenHeight = (int) (metrics.heightPixels * 0.85);//fill only 85% of the screen

        final Dialog moreInfoDialog = new Dialog(IncentivesDashboard.this, R.style.SlideUpDialog);
        moreInfoDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        moreInfoDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        moreInfoDialog.setCancelable(true);
        moreInfoDialog.setContentView(R.layout.alert_driver_incentives_info);
        moreInfoDialog.getWindow().setLayout(screenWidth, screenHeight);

        final ImageView Iv_close = (ImageView) moreInfoDialog.findViewById(R.id.img_close);

        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moreInfoDialog.dismiss();
            }
        });

        /*String sWhatIsIncentives = bold(getResources().getString(R.string.incentives_page_alert_label_what_is_incentives)) + getResources().getString(R.string.incentives_page_alert_label_what_is_incentives_content);
//        final SpannableString text = new SpannableString(sWhatIsIncentives);
//        text.setSpan(new StyleSpan(Typeface.BOLD), 0, getResources().getString(R.string.incentives_page_alert_label_what_is_incentives).length(), 0);

        TextView Tv_whatIsIncentives = (TextView) moreInfoDialog.findViewById(R.id.txt_what_is_incentives);
        Tv_whatIsIncentives.setText(sWhatIsIncentives);*/

        moreInfoDialog.show();


    }


}
