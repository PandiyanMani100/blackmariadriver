package com.blackmaria.partner.app;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.Pojo.MessagePojo;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.adapter.MessageListviewAdapter;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class MessagePage extends ActivityHockeyApp {

    private ConnectionDetector cd;
    private SessionManager session;
    private ServiceRequest mRequest;
    MessageListviewAdapter adapter;
    private Dialog dialog;

    TextView empty_text;
    ListView mMessageList;
    RelativeLayout messageNavigationBackIcon;
    ImageView messageHome, Iv_previous, Iv_next;

    private Boolean isInternetPresent = false;
    ArrayList<MessagePojo> itemlist_all;
    private String perPage = "5";
    private int currentPage = 1;
    private String sDriverId = "";
    private boolean isTransactionAvailable = false;

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_new);
        inilialize();

        messageNavigationBackIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });


        Iv_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Iv_previous.setVisibility(View.VISIBLE);
                currentPage = currentPage + 1;
                postData();
            }
        });

        Iv_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                currentPage = currentPage - 1;
                postData();

            }
        });

        /*mMessageList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("===============message selected postion================" + position);
                if (itemlist_all.get(position).getType().equalsIgnoreCase("earning")) {
                    withdrawalAdvice(position);
                } else {
                    MessagePopUp(position);
                }
            }
        });*/


    }

    //-------------Confirm Withdrawal Dialog-----------------
    @SuppressLint("SetTextI18n")
    public void withdrawalAdvice(int position) {

        //--------Adjusting Dialog width & Height-----
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 80% of the screen
        int screenHeight = (int) (metrics.heightPixels * 0.85);//fill only 80% of the screen

        final Dialog withdrawalAdviceDialog = new Dialog(MessagePage.this, R.style.SlideUpDialog);
        withdrawalAdviceDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        withdrawalAdviceDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        withdrawalAdviceDialog.setCancelable(true);
        withdrawalAdviceDialog.setContentView(R.layout.alert_withdrawal_advice);
        withdrawalAdviceDialog.getWindow().setLayout(screenWidth, screenHeight);

        final ImageView Iv_close = (ImageView) withdrawalAdviceDialog.findViewById(R.id.img_close);
        final TextView Tv_refNo = (TextView) withdrawalAdviceDialog.findViewById(R.id.txt_ref_no);
        final TextView Tv_locationId = (TextView) withdrawalAdviceDialog.findViewById(R.id.txt_location_id);
        final TextView Tv_partnerId = (TextView) withdrawalAdviceDialog.findViewById(R.id.txt_partner_id);
        final TextView Tv_driverId = (TextView) withdrawalAdviceDialog.findViewById(R.id.txt_driver_id);
        final TextView Tv_companyId = (TextView) withdrawalAdviceDialog.findViewById(R.id.txt_company_id);
        final TextView Tv_amount = (TextView) withdrawalAdviceDialog.findViewById(R.id.txt_amount);
        final TextView Tv_period = (TextView) withdrawalAdviceDialog.findViewById(R.id.txt_period);
        final TextView Tv_payment = (TextView) withdrawalAdviceDialog.findViewById(R.id.txt_payment);
        final TextView Tv_completedTrip = (TextView) withdrawalAdviceDialog.findViewById(R.id.txt_completed_trip);
        final TextView Tv_paidByCash = (TextView) withdrawalAdviceDialog.findViewById(R.id.txt_paid_by_cash);
        final TextView Tv_byWallet = (TextView) withdrawalAdviceDialog.findViewById(R.id.txt_ewallet);
        final TextView Tv_byCards = (TextView) withdrawalAdviceDialog.findViewById(R.id.txt_cards);
        final TextView Tv_grossEarnings = (TextView) withdrawalAdviceDialog.findViewById(R.id.txt_gross_earnings);
        final TextView Tv_feesAndCharges = (TextView) withdrawalAdviceDialog.findViewById(R.id.txt_fees_and_charges);
        final TextView Tv_referralIncome = (TextView) withdrawalAdviceDialog.findViewById(R.id.txt_referral_income);
        final TextView Tv_incentives = (TextView) withdrawalAdviceDialog.findViewById(R.id.txt_incentives);
        final TextView Tv_nettEarn = (TextView) withdrawalAdviceDialog.findViewById(R.id.txt_net_earn);

        Tv_refNo.setText(itemlist_all.get(position).getEarningDetail().getRef_no());
        Tv_locationId.setText(itemlist_all.get(position).getEarningDetail().getLocation_id());
        Tv_driverId.setText(itemlist_all.get(position).getEarningDetail().getDriver_id());
        Tv_companyId.setText(itemlist_all.get(position).getEarningDetail().getCompany_id());
        Tv_amount.setText(itemlist_all.get(position).getEarningDetail().getCurrency() + " " + itemlist_all.get(position).getEarningDetail().getAmount());
        Tv_period.setText(itemlist_all.get(position).getEarningDetail().getPeriod());
        Tv_payment.setText(itemlist_all.get(position).getEarningDetail().getPayment());
        Tv_completedTrip.setText(itemlist_all.get(position).getEarningDetail().getCompleted_trip());
        Tv_paidByCash.setText(itemlist_all.get(position).getEarningDetail().getCurrency() + " " + itemlist_all.get(position).getEarningDetail().getPaid_by_cash());
        Tv_byWallet.setText(itemlist_all.get(position).getEarningDetail().getCurrency() + " " + itemlist_all.get(position).getEarningDetail().getBy_wallet());
        Tv_byCards.setText(itemlist_all.get(position).getEarningDetail().getCurrency() + " " + itemlist_all.get(position).getEarningDetail().getBy_card());
        Tv_grossEarnings.setText(itemlist_all.get(position).getEarningDetail().getCurrency() + " " + itemlist_all.get(position).getEarningDetail().getGross_earning());
        Tv_feesAndCharges.setText(itemlist_all.get(position).getEarningDetail().getFees_charge());
        Tv_nettEarn.setText(itemlist_all.get(position).getEarningDetail().getCurrency() + " " + itemlist_all.get(position).getEarningDetail().getNett_earn());

        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                withdrawalAdviceDialog.dismiss();
            }
        });

        withdrawalAdviceDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mMessageList.setEnabled(true);
            }
        });

        withdrawalAdviceDialog.show();
        mMessageList.setEnabled(false);

    }


    private void inilialize() {
        session = new SessionManager(MessagePage.this);
        cd = new ConnectionDetector(MessagePage.this);
        isInternetPresent = cd.isConnectingToInternet();
        itemlist_all = new ArrayList<MessagePojo>();

        mMessageList = (ListView) findViewById(R.id.message_listview);
        messageNavigationBackIcon = (RelativeLayout) findViewById(R.id.message_navigation_icon_layout);
//        messageHome = (ImageView) findViewById(R.id.message_bottom_layout_home);
        empty_text = (TextView) findViewById(R.id.emptytext);
        Iv_previous = (ImageView) findViewById(R.id.message_bottom_layout_prev);
        Iv_next = (ImageView) findViewById(R.id.message_bottom_layout_next);
        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        sDriverId = user.get(SessionManager.KEY_DRIVERID);

        postData();
    }


    private void postData() {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("id", sDriverId);
        jsonParams.put("page", String.valueOf(currentPage));
        jsonParams.put("perPage", perPage);
        jsonParams.put("type", "message");
        if (isInternetPresent) {
            postRequest_Message(Iconstant.message_url, jsonParams);
        } else {
            Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));

        }
    }

    //-----------------------Messages Post Request-----------------
    @SuppressLint("WrongConstant")
    private void postRequest_Message(String Url, HashMap<String, String> jsonParams) {

        showDialog();

        System.out.println("-------------MessagePage  Url----------------" + Url);


        mRequest = new ServiceRequest(MessagePage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------MessagePage  Response----------------" + response);

                String Sstatus = "", Str_CurrentPage = "", str_NextPage = "", perPage = "", ScurrencySymbol = "", Str_total_amount = "", Str_total_transaction = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");

                        if (response_object.length() > 0) {


                            Object check_trans_object = response_object.get("notification");
                            itemlist_all.clear();
                            if (check_trans_object instanceof JSONArray) {
                                JSONArray trans_array = response_object.getJSONArray("notification");
                                if (trans_array.length() > 0) {

                                    for (int i = 0; i < trans_array.length(); i++) {
                                        JSONObject trans_object = trans_array.getJSONObject(i);
                                        MessagePojo pojo = new MessagePojo();
                                        pojo.setDate(trans_object.getString("date"));
                                        pojo.setIMage(trans_object.getString("image"));
                                        pojo.setTitle(trans_object.getString("title"));
                                        pojo.setDescription(trans_object.getString("description"));
                                        pojo.setNotification_id(trans_object.getString("notification_id"));
                                        pojo.setType(trans_object.getString("type"));

                                        /*String ref_no, location_id, driver_id, company_id, amount, period,
                                                payment, completed_trip, paid_by_cash, by_card, by_wallet,
                                                gross_earning, nett_earn, currency;*/

                                        if (trans_object.getString("type").equalsIgnoreCase("earning")) {

                                            JSONObject jobjEarning = trans_object.getJSONObject("earning_statement");
                                            if (jobjEarning.length() > 0) {

                                                MessagePojo.EarningDetail earningDetail = pojo.new EarningDetail();
                                                earningDetail.setRef_no(jobjEarning.getString("ref_no"));
                                                earningDetail.setLocation_id(jobjEarning.getString("location_id"));
                                                earningDetail.setDriver_id(jobjEarning.getString("driver_id"));
                                                earningDetail.setCompany_id(jobjEarning.getString("company_id"));
                                                earningDetail.setAmount(jobjEarning.getString("amount"));
                                                earningDetail.setPeriod(jobjEarning.getString("period"));
                                                earningDetail.setPayment(jobjEarning.getString("payment"));
                                                earningDetail.setCompleted_trip(jobjEarning.getString("completed_trip"));
                                                earningDetail.setPaid_by_cash(jobjEarning.getString("paid_by_cash"));
                                                earningDetail.setBy_card(jobjEarning.getString("by_card"));
                                                earningDetail.setBy_wallet(jobjEarning.getString("by_wallet"));
                                                earningDetail.setGross_earning(jobjEarning.getString("gross_earning"));
                                                earningDetail.setNett_earn(jobjEarning.getString("nett_earn"));
                                                earningDetail.setCurrency(jobjEarning.getString("currency"));
                                                earningDetail.setFees_charge(jobjEarning.getString("fees_charge"));

                                                pojo.setEarningDetail(earningDetail);

                                            }

                                        }

                                        itemlist_all.add(pojo);
                                    }
                                    isTransactionAvailable = true;
                                } else {
                                    isTransactionAvailable = false;
                                }
                            } else {
                                isTransactionAvailable = false;
                            }
                            currentPage = Integer.parseInt(response_object.getString("current_page"));
                            str_NextPage = response_object.getString("next_page");
                            perPage = response_object.getString("perPage");
                        }
                    }

                    if (currentPage == 1) {
                        Iv_previous.setVisibility(View.GONE);
                    } else {
                        Iv_previous.setVisibility(View.VISIBLE);
                    }

                    if (Sstatus.equalsIgnoreCase("1")) {
                        if (isTransactionAvailable) {
                            empty_text.setVisibility(View.GONE);
                            mMessageList.setVisibility(View.VISIBLE);
                            adapter = new MessageListviewAdapter(MessagePage.this, itemlist_all, new MessageListviewAdapter.MessageClickInterface() {
                                @Override
                                public void onTitleClick(int position) {
                                    System.out.println("===============message selected position================" + position);
                                    if (itemlist_all.get(position).getType().equalsIgnoreCase("earning")) {
                                        withdrawalAdvice(position);
                                    } else {
                                        MessagePopUp(position);
                                    }
                                }

                                @Override
                                public void onDeleteClick(int position) {
                                    confirmDelete(getResources().getString(R.string.label_sure_to_delete), position);
                                }
                            });
                            mMessageList.setAdapter(adapter);

                            if (str_NextPage.length() > 0) {
                                Iv_next.setVisibility(View.VISIBLE);
                            } else {
                                Iv_next.setVisibility(View.GONE);
                            }

                        } else {
                            empty_text.setVisibility(View.VISIBLE);
                            mMessageList.setVisibility(View.GONE);
                            Iv_next.setVisibility(View.GONE);
                        }
                    } else {
                        String Sresponse = object.getString("response");
                        Alert(getResources().getString(R.string.alert_label_title), Sresponse);
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                dismissDialog();
            }

            @Override
            public void onErrorListener() {
                dismissDialog();
            }
        });

    }


    private void DeleteMessageRequest(String Url, HashMap<String, String> jsonParams) {

        showDialog();

        System.out.println("-------------DeleteMessage Url----------------" + Url);
        System.out.println("-------------DeleteMessage Params----------------" + jsonParams);


        mRequest = new ServiceRequest(MessagePage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------DeleteMessage Response----------------" + response);

                dismissDialog();

                String Sstatus = "";

                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    String sResponse = object.getString("response");

                    if (Sstatus.equalsIgnoreCase("1")) {
//                        AlertDeleteSuccess(getResources().getString(R.string.action_success), sResponse);
                        postData();
                    } else {
                        Alert(getResources().getString(R.string.action_error), sResponse);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


            }

            @Override
            public void onErrorListener() {
                dismissDialog();
            }
        });

    }


    private void showDialog() {

        dialog = new Dialog(MessagePage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

    }

    private void dismissDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }


    /*private void MessagePopUp(int postion) {
        final Dialog contentDialog = new Dialog(MessagePage.this, R.style.SlideUpDialog);
        contentDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        contentDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        contentDialog.setContentView(R.layout.messagepopup);

        RoundedImageView messageimage = (RoundedImageView) contentDialog.findViewById(R.id.messageimage);
        CustomTextView title = (CustomTextView) contentDialog.findViewById(R.id.title);
        CustomTextView date = (CustomTextView) contentDialog.findViewById(R.id.date);
        CustomTextView description = (CustomTextView) contentDialog.findViewById(R.id.description);
        CustomTextView Ok_Profilechange = (CustomTextView) contentDialog.findViewById(R.id.ok_profilechange);

        title.setText(itemlist_all.get(postion).getTitle().toUpperCase());
        date.setText(itemlist_all.get(postion).getDate());
        description.setText(itemlist_all.get(postion).getDescription().toUpperCase());
        if (itemlist_all.get(postion).getIMage().length() > 0) {
            Picasso.with(MessagePage.this).load(String.valueOf(itemlist_all.get(postion).getIMage())).into(messageimage);
        }
        Ok_Profilechange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (contentDialog != null) {
                    contentDialog.dismiss();
                }

            }
        });

        contentDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mMessageList.setEnabled(true);
            }
        });

        contentDialog.show();
        mMessageList.setEnabled(false);
    }*/

    private void MessagePopUp(int postion) {
        final Dialog dialog = new Dialog(MessagePage.this, R.style.SlideUpDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.message_popup_new);

        final ImageView messageimage = (ImageView) dialog.findViewById(R.id.img_header);
        TextView title = (TextView) dialog.findViewById(R.id.title);
        //CustomTextView date = (CustomTextView) dialog.findViewById(R.id.date);
        TextView description = (TextView) dialog.findViewById(R.id.description);
        ImageView Ok_Profilechange = (ImageView) dialog.findViewById(R.id.ok_profilechange);

        String imageUrl = itemlist_all.get(postion).getIMage();
        if (imageUrl != null && imageUrl.length() > 0) {
            Picasso.with(MessagePage.this).load(imageUrl).error(R.drawable.messagepopupimage).placeholder(R.drawable.messagepopupimage).fit().memoryPolicy(MemoryPolicy.NO_CACHE).into(messageimage);
        }

        title.setText(itemlist_all.get(postion).getTitle().toUpperCase());
        //  date.setText(itemlist_all.get(postion).getDate());
        description.setText(itemlist_all.get(postion).getDescription().toUpperCase());
        // Picasso.with(Message.this).load(String.valueOf(itemlist_all.get(postion).getIMage())).into(messageimage);
        Ok_Profilechange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null) {
                    dialog.dismiss();
                }

            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mMessageList.setEnabled(true);
            }
        });

        dialog.show();
        mMessageList.setEnabled(false);
    }


    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(MessagePage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    //--------------Alert Method-----------
    private void AlertDeleteSuccess(String title, String alert) {

        final PkDialog mDialog = new PkDialog(MessagePage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });

        mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                postData();
            }
        });

        mDialog.show();
    }

    private void confirmDelete(String title, final int position) {
        final Dialog confirmDeleteDialog = new Dialog(MessagePage.this);
        confirmDeleteDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmDeleteDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmDeleteDialog.setCancelable(true);
        confirmDeleteDialog.setContentView(R.layout.alert_sure_to_cancel);

        final TextView Tv_title = (TextView) confirmDeleteDialog.findViewById(R.id.txt_label_title);
        final Button Btn_yes = (Button) confirmDeleteDialog.findViewById(R.id.btn_yes);
        final Button Btn_no = (Button) confirmDeleteDialog.findViewById(R.id.btn_no);

        Tv_title.setText(title);

        Btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmDeleteDialog.dismiss();

                if (cd.isConnectingToInternet()) {

                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    jsonParams.put("id", sDriverId);
                    jsonParams.put("notification_id", itemlist_all.get(position).getNotification_id());

                    DeleteMessageRequest(Iconstant.delete_message_url, jsonParams);

                } else {
                    Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
                }

            }
        });

        Btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmDeleteDialog.dismiss();
            }
        });

        confirmDeleteDialog.show();

    }


    //-----------------------Favourite Save Post Request-----------------

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }
}