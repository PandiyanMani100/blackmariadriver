package com.blackmaria.partner.app;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.GPSTracker;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.view.Dashboard.OnlinepageConstrain;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.plus.PlusShare;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by GANESH on 31-08-2017.
 */

public class AboutUs extends ActivityHockeyApp implements View.OnClickListener {



    private ImageView RL_home,Iv_drive, Iv_facebook, Iv_twitter, Iv_instagram;


    private ConnectionDetector cd;
    private SessionManager sessionManager;
    private ServiceRequest mRequest;
    private Dialog dialog;
    private TextView Tv_feedback;
    private String sDriveID = "", sContactMail = "", sNearbyDriversCount = "", sSearchRadius = "", sVerifyStatus = "", sGoOnlineStatus = "", sGoOnlineString = "", sCityName = "";
    ;
    private boolean isDataPresent = false;
    AccessLanguagefromlocaldb db;

    private GPSTracker gpsTracker;
    PendingResult<LocationSettingsResult> result;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    final static int REQUEST_LOCATION = 199;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aboutusnewmockupp);
        db = new AccessLanguagefromlocaldb(this);
        initialize();

    }

    private void initialize() {

        sessionManager = new SessionManager(AboutUs.this);
        cd = new ConnectionDetector(AboutUs.this);
        gpsTracker = new GPSTracker(AboutUs.this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriveID = user.get(SessionManager.KEY_DRIVERID);
        sContactMail = user.get(SessionManager.KEY_EMAIL);

        RL_home = (ImageView) findViewById(R.id.RL_home);
        Iv_drive = (ImageView) findViewById(R.id.img_drive);

        TextView terms = (TextView) findViewById(R.id.terms);
        terms.setText(db.getvalue("sidemenu_aboutus"));

        TextView demandlabel = (TextView) findViewById(R.id.demandlabel);
        demandlabel.setText(db.getvalue("demand_responsive_transport_technology"));

        TextView content = (TextView) findViewById(R.id.content);
        content.setText(db.getvalue("about_us_label_about_us_content"));




        Iv_facebook = (ImageView) findViewById(R.id.img_facebook);
        Iv_twitter = (ImageView) findViewById(R.id.img_twitter);
        Iv_drive = (ImageView) findViewById(R.id.Iv_drive);
        Iv_instagram = (ImageView) findViewById(R.id.img_instagram);
        Tv_feedback = (TextView) findViewById(R.id.txt_label_feedback);
        Tv_feedback.setText(db.getvalue("feedback_lable"));
        Iv_instagram.setOnClickListener(this);

        RL_home.setOnClickListener(this);
        Tv_feedback.setOnClickListener(this);
        Iv_facebook.setOnClickListener(this);
        Iv_twitter.setOnClickListener(this);
        Iv_drive.setOnClickListener(this);

        if (cd.isConnectingToInternet()) {
            if (gpsTracker != null && gpsTracker.isgpsenabled()) {
                homePageRequest(Iconstant.driverHomePage_Url);
            } else {
                enableGpsService();
            }
        } else {
            Alert(db.getvalue("alert_nointernet"), db.getvalue("alert_nointernet_message"));
        }

    }

    @Override
    public void onClick(View view) {

        if (cd.isConnectingToInternet()) {

            if (view == RL_home) {
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            } else if (view == Iv_facebook) {
//                shareFacebookLink("");
                String url = "http://facebook.com/blackmaria.co";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
            else if (view == Tv_feedback) {
                sendEmail(sContactMail);
            } else if (view == Iv_twitter) {
//                shareTwitter("");
                String url = "http://twitter.com/blackmaria_inc";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            } else if (view == Iv_instagram) {
                String url = "http://instagram.com/blackmaria.inc";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
            else if (view == Iv_drive) {

                if (sVerifyStatus.equalsIgnoreCase("Yes")) {
                    if (sGoOnlineStatus.equalsIgnoreCase("1")) {
                        driveNowDialog();
                    } else {
                        showVerifyAccountDialog(db.getvalue("label_welcome_to") + sCityName, sGoOnlineString);
                    }
                } else {
                    if (sGoOnlineStatus.equalsIgnoreCase("0")) {
                        showVerifyAccountDialog(db.getvalue("action_error"), sGoOnlineString);
                    } else {
                        showVerifyAccountDialog(db.getvalue("action_error"), db.getvalue("driver_homepage_alert_label_verify_account"));
                    }
                }

            }

        } else {
            Alert(db.getvalue("alert_nointernet"),db.getvalue("alert_nointernet_message"));
        }

    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(AboutUs.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(db.getvalue("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void ConfirmTransferPopUp() {
        final Dialog dialog = new Dialog(AboutUs.this, R.style.SlideUpDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.bepartner_popup);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Button userStatusTv = (Button) dialog.findViewById(R.id.custom_dialog_library_ok_button);
        ImageView close = (ImageView) dialog.findViewById(R.id.image_close);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        userStatusTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "https://blackmaria.co/global/company/become-partner/";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    //-----------Alert when driver account not verified----------
    private void showVerifyAccountDialog(String title, String message) {
        final Dialog dialog = new Dialog(AboutUs.this);
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 85% of the screen
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_could_not_drive);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setLayout(screenWidth, LinearLayout.LayoutParams.WRAP_CONTENT);

        TextView Tv_title = (TextView) dialog.findViewById(R.id.custom_dialog_library_title_textview);
        TextView Tv_message = (TextView) dialog.findViewById(R.id.custom_dialog_library_message_textview);

        Tv_title.setText(title);
        Tv_message.setText(message);

        TextView okButton = dialog.findViewById(R.id.custom_dialog_library_ok_button);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });

        dialog.show();

    }

    //----------Share Link on Method--------
    private void shareFacebookLink(String link) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, link);
        Intent pacakage1 = getPackageManager().getLaunchIntentForPackage("com.facebook.katana");
        Intent pacakage2 = getPackageManager().getLaunchIntentForPackage("com.example.facebook");
        Intent pacakage3 = getPackageManager().getLaunchIntentForPackage("com.facebook.android");

        if (pacakage1 != null) {
            intent.setPackage("com.facebook.katana");
        } else if (pacakage2 != null) {
            intent.setPackage("com.facebook.facebook");
        } else if (pacakage3 != null) {
            intent.setPackage("com.facebook.android");
        }

        try {
            startActivity(intent);
        } catch (android.content.ActivityNotFoundException ex) {
            Alert(db.getvalue("alert_label_title"), db.getvalue("invite_earn_label_facebook_not_installed"));
        }
    }

    //----------Share Image and Text on Twitter Method--------
    protected void shareTwitter(String text) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, text);
        intent.setPackage("com.twitter.android");

        try {
            startActivity(intent);
        } catch (android.content.ActivityNotFoundException ex) {
            Alert(db.getvalue("alert_label_title"), db.getvalue("invite_earn_label_twitter_not_installed"));
        }
    }

    private void googlePlusShare(String text) {

        if (isGooglePlusInstalled()) {
            Intent shareIntent = new PlusShare.Builder(AboutUs.this)
                    .setType("text/plain")
                    .setText(text)
                    .setContentUrl(Uri.parse("https://developers.google.com/+/"))
                    .getIntent();

            startActivityForResult(shareIntent, 0);

        } else {
            Alert(db.getvalue("alert_label_title"), db.getvalue("invite_earn_label_gplus_not_installed"));
        }
    }

    public boolean isGooglePlusInstalled() {
        try {
            getPackageManager().getApplicationInfo("com.google.android.apps.plus", 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    //----------Sending message on Email Method--------
    private void sendEmail(String to) {
        String[] TO = {to};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "");
        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Alert(db.getvalue("alert_label_title"), db.getvalue("invite_earn_label_email_not_installed"));
        }
    }


    @SuppressLint("WrongConstant")
    private void homePageRequest(String Url) {

        showDialog();

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriveID);
        jsonParams.put("mode", "get");
        jsonParams.put("lat", String.valueOf(gpsTracker.getLatitude()));
        jsonParams.put("lon", String.valueOf(gpsTracker.getLongitude()));

        System.out.println("--------------homePageRequest Url-------------------" + Url);
        System.out.println("--------------jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(AboutUs.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------response-------------------" + response);
                String status = "", sComplaint_badge = "", sInfo_badge = "", sVehicleNo = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {
                            JSONObject jsonObject = object.getJSONObject("response");
                            if (jsonObject.length() > 0) {
                                sNearbyDriversCount = jsonObject.getString("near_by_driver");
                                sSearchRadius = jsonObject.getString("searching_radius");
                                sVerifyStatus = jsonObject.getString("verify_status");
                                sGoOnlineStatus = jsonObject.getString("go_online_status");
                                sGoOnlineString = jsonObject.getString("go_online_string");
                                sCityName = jsonObject.getString("city_name");
//                                sDriverAvailability = jsonObject.getString("driver_availability");
                            }
                        } else {
                            String sResponse = object.getString("response");
                            Alert(db.getvalue("action_error"), sResponse);
                        }

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                dismissDialog();

            }

            @Override
            public void onErrorListener() {
                dismissDialog();
            }
        });
    }

    //-----------Alert when clicking drive now----------
    @SuppressWarnings("WrongConstant")
    private void driveNowDialog() {
        final Dialog driveNowDialog = new Dialog(AboutUs.this, R.style.SlideUpDialog);
        driveNowDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        driveNowDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        driveNowDialog.setCancelable(true);
        driveNowDialog.setCanceledOnTouchOutside(true);
        driveNowDialog.setContentView(R.layout.alert_rules_of_road);

        //--------Adjusting Dialog width and height-----
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 85% of the screen
        int screenHeight = (int) (metrics.heightPixels * 0.85);//fill only 85% of the screen

        driveNowDialog.getWindow().setLayout(screenWidth, screenHeight);

        final RelativeLayout RL_driveNow = (RelativeLayout) driveNowDialog.findViewById(R.id.RL_drive_now);
        final RelativeLayout RL_close = (RelativeLayout) driveNowDialog.findViewById(R.id.RL_close);

        final TextView txt_label_rules_of_road = driveNowDialog.findViewById(R.id.txt_label_rules_of_road);
        txt_label_rules_of_road.setText(db.getvalue("rules_of_the_road"));

        final TextView txt_content1 = driveNowDialog.findViewById(R.id.txt_content1);
        txt_content1.setText(db.getvalue("drive_now_alert_content1"));

        final TextView txt_content2 = driveNowDialog.findViewById(R.id.txt_content2);
        txt_content2.setText(db.getvalue("drive_now_alert_content2"));

        final TextView txt_label_close = driveNowDialog.findViewById(R.id.txt_label_close);
        txt_label_close.setText(db.getvalue("close_lable"));

        final TextView txt_label_drive_now = driveNowDialog.findViewById(R.id.txt_label_drive_now);
        txt_label_drive_now.setText(db.getvalue("driver_homepage_label_drive_now"));
        RL_driveNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (driveNowDialog.isShowing()) {
                    driveNowDialog.dismiss();
                }

                GoOnlineRequest(Iconstant.updateAvailability_Url);

            }
        });

        RL_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (driveNowDialog.isShowing()) {
                    driveNowDialog.dismiss();
                }
            }
        });



        driveNowDialog.show();


    }

    //-------------------Go Online Request----------------
    private void GoOnlineRequest(String Url) {

        dialog = new Dialog(AboutUs.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(db.getvalue("action_pleasewait"));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriveID);
        jsonParams.put("availability", "Yes");

        System.out.println("--------------GoOnlineRequest Url-------------------" + Url);
        System.out.println("--------------jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(AboutUs.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------response-------------------" + response);
                String status = "";

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {
                        status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {

                            sessionManager.setXmppServiceState("online");
                            Intent intent = new Intent(AboutUs.this, OnlinepageConstrain.class);
                            startActivity(intent);
                            overridePendingTransition(R.anim.enter, R.anim.exit);
                        } else {
                            String sResponse = object.getString("response");
                            Alert(db.getvalue("action_error"), sResponse);
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void showDialog() {
        dialog = new Dialog(AboutUs.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(db.getvalue("action_loading"));
    }

    private void dismissDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    @SuppressLint("WrongConstant")
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        boolean b = false;
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                System.out.println("1 already running");
                b = true;
                break;
            } else {
                System.out.println("2 not running");
                b = false;
            }
        }
        System.out.println("3 not running");
        return b;
    }

    //Enabling Gps Service
    private void enableGpsService() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        //...
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(AboutUs.this, REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });
    }

}

