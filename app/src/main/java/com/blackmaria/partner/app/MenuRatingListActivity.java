package com.blackmaria.partner.app;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.Pojo.MenuHomeratingPojo;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.latestpackageview.Adapter.MenuHomeListRatingAdapter;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Muruganantham on 9/2/2017.
 */

@SuppressLint("Registered")
public class MenuRatingListActivity extends ActivityHockeyApp {
    private SessionManager session;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private Dialog dialog;
    private ServiceRequest mRequest;

    private String UserID = "";
    ArrayList<MenuHomeratingPojo> itemlist;
    private boolean isDataAvailable = false;
    private String rideId1 = "";
    ImageView Iv_previous, Iv_next;
    String perPage = "5";
    private int currentPage = 1;
    MenuHomeListRatingAdapter adapter;
    private ListView listview;
    private ImageView img_back;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_ride_rating_list);
        Initialize();

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(MenuRatingListActivity.this, MenuDriverratingHOme.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        Iv_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Iv_previous.setVisibility(View.VISIBLE);
                currentPage = currentPage + 1;
                postData();
            }
        });

        Iv_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                currentPage = currentPage - 1;
                postData();

            }
        });
    }

    private void Initialize() {
        session = new SessionManager(MenuRatingListActivity.this);
        cd = new ConnectionDetector(MenuRatingListActivity.this);
        isInternetPresent = cd.isConnectingToInternet();

        itemlist = new ArrayList<MenuHomeratingPojo>();
        Iv_next = (ImageView) findViewById(R.id.next_page);
        Iv_previous = (ImageView) findViewById(R.id.prev_page);
        listview = (ListView) findViewById(R.id.menulistview);
        img_back=(ImageView)findViewById(R.id.img_back);// get user data from session
        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_DRIVERID);


        // -----code to refresh drawer using broadcast receiver-----
        postData();
    }

    private void postData() {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", UserID);
        jsonParams.put("page", String.valueOf(currentPage));
        jsonParams.put("perPage", perPage);
        if (isInternetPresent) {
            postRequest_MenuratingHome(Iconstant.menupage_ratinglist, jsonParams);
        } else {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
        }
    }

    //-----------------------Rating List Post Request-----------------
    private void postRequest_MenuratingHome(String Url, HashMap<String, String> jsonParams) {
        dialog = new Dialog(MenuRatingListActivity.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));


        System.out.println("-------------MenuratingHomeList  Url----------------" + Url);


        mRequest = new ServiceRequest(MenuRatingListActivity.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                String userName = "", userEmail = "", userGender = "", userAge = "", userImage = "", userAvgReview = "", driverName = "", driveromments = "", DriverImage = "", reviewDate1 = "";
                Log.e("rateing", response);

                System.out.println("-------------MenuratingHomeList Response----------------" + response);

                String Sstatus = "", str_NextPage;
                String SRating_status = "";

                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    JSONObject jsonObj = object.getJSONObject("response");
                    currentPage = Integer.parseInt(jsonObj.getString("current_page"));
                    str_NextPage = jsonObj.getString("next_page");
                    if (str_NextPage.equalsIgnoreCase("")) {
                        Iv_next.setVisibility(View.GONE);
                    } else {
                        Iv_next.setVisibility(View.VISIBLE);
                    }
                    if (Sstatus.equalsIgnoreCase("1")) {

                        JSONArray objLastreview1 = jsonObj.getJSONArray("review");
                        if (objLastreview1.length() > 0) {
                            itemlist.clear();
                            for (int i = 0; i < objLastreview1.length(); i++) {
                                MenuHomeratingPojo pojo = new MenuHomeratingPojo();
                                JSONObject objLastreview = objLastreview1.getJSONObject(i);
                                pojo.setDriverName(objLastreview.getString("user_name"));
                                pojo.setDriveromments(objLastreview.getString("comments"));
                                pojo.setDriverImage(objLastreview.getString("image"));
                                pojo.setRideId1(objLastreview.getString("ride_id"));
                                pojo.setReviewDate1(objLastreview.getString("review_date"));
                                itemlist.add(pojo);
                            }
                            isDataAvailable = true;
                        } else {
                            isDataAvailable = false;
                        }
                    }

                    if (Sstatus.equalsIgnoreCase("1") && isDataAvailable) {
                        if (currentPage == 1) {
                            Iv_previous.setVisibility(View.GONE);
                        } else {
                            Iv_previous.setVisibility(View.VISIBLE);
                        }

                        if (SRating_status.equalsIgnoreCase("1")) {

                            Toast.makeText(getApplicationContext(), "Already submitted your rating", Toast.LENGTH_SHORT).show();

                        } else {


//                            adapter = new MenuHomeListRatingAdapter(MenuRatingListActivity.this, itemlist,UserID);
//                            listview.setAdapter(adapter);
                        }

                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(MenuRatingListActivity.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

}
