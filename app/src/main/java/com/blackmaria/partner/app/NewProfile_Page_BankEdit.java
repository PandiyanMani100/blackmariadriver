package com.blackmaria.partner.app;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.Pojo.WalletMoneyPojo;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.CustomEdittext;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;

/**
 * Created by Muruganantham on 12/28/2017.
 */

@SuppressLint("Registered")
public class NewProfile_Page_BankEdit extends ActivityHockeyApp implements View.OnClickListener {

    private ServiceRequest mRequest;
    private ConnectionDetector cd;
    private boolean isInternetPresent = false;
    private SessionManager session;


    private RelativeLayout bankEditLyHeader;
    private ImageView cancelLayout;
    private LinearLayout myloginDialogFormWholeLayout;
    private CustomEdittext bankDetailsAccountNumber;
    private CustomEdittext bankDetailsHolderName;
    private CheckBox agreeBox;
    private RelativeLayout bankInformationDetailsUpdate;
    private SmoothProgressBar profileLoadingProgressbar;

    private Boolean isDatavailable = false;
    private Boolean isPaymentListAvailable = false;
    private String UserID = "";
    ArrayList<WalletMoneyPojo> paymentcardlist;
    ArrayList<String> paymentcardNamelist;

    String bankImageUrl = "";
    String accHolderName = "", holderAccountNumber = "";
    private int selectPositionSpinner = 0;
    private EditText Edt_bankName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_bank_info_edit_layout);
        initialize();
    }

    private void initialize() {
        session = new SessionManager(NewProfile_Page_BankEdit.this);
        cd = new ConnectionDetector(NewProfile_Page_BankEdit.this);
        isInternetPresent = cd.isConnectingToInternet();
        paymentcardlist = new ArrayList<>();
        paymentcardNamelist = new ArrayList<>();


        HashMap<String, String> info = session.getUserDetails();
        UserID = info.get(SessionManager.KEY_DRIVERID);


        bankEditLyHeader = (RelativeLayout) findViewById(R.id.bank_edit_ly_header);
        cancelLayout = (ImageView) findViewById(R.id.cancel_layout);
        myloginDialogFormWholeLayout = (LinearLayout) findViewById(R.id.mylogin_dialog_form_whole_layout);
        bankDetailsAccountNumber = (CustomEdittext) findViewById(R.id.bank_details_account_number);
        bankDetailsHolderName = (CustomEdittext) findViewById(R.id.bank_details_holder_name);
        agreeBox = (CheckBox) findViewById(R.id.agree_box);
        bankInformationDetailsUpdate = (RelativeLayout) findViewById(R.id.bank_information_details_update);
        profileLoadingProgressbar = (SmoothProgressBar) findViewById(R.id.profile_loading_progressbar);
        Edt_bankName = (EditText) findViewById(R.id.edt_bank_name);

        cancelLayout.setOnClickListener(this);
        bankInformationDetailsUpdate.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v == cancelLayout) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (v == bankInformationDetailsUpdate) {
            BankUpdate();
        }
    }


    private void BankUpdate() {
        if (agreeBox.isChecked())
            if (bankDetailsHolderName.getText().toString().trim().length() <= 0) {
                AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.driver_account_label_alert_bankaccholderno));
            } else if (bankDetailsAccountNumber.getText().toString().trim().length() <= 0) {
                AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.driver_account_label_alert_bankaccno));
            } else if (Edt_bankName.getText().toString().trim().length() <= 0) {
                AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.driver_account_label_alert_branch_name));
            } else {
                accHolderName = bankDetailsHolderName.getText().toString().trim();
                holderAccountNumber = bankDetailsAccountNumber.getText().toString().trim();
                if (isInternetPresent) {
                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    jsonParams.put("driver_id", UserID);
                    jsonParams.put("acc_holder_name", accHolderName);
                    jsonParams.put("acc_holder_address", "");
                    jsonParams.put("acc_number", holderAccountNumber);
                    jsonParams.put("city", "");
                    jsonParams.put("country", "");
                    jsonParams.put("swift_code", "");
                    jsonParams.put("bank_name", Edt_bankName.getText().toString());
                    jsonParams.put("routing_number", "");
                    jsonParams.put("iban_number", "");
                    System.out.println("-----------EditProfile jsonParams--------------" + jsonParams);
                    postRequestaccount_detail_Saveurl(Iconstant.Bank_account_Save_Url, jsonParams);
                } else {
                    Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
                }
            }
        else {
            Alert(getResources().getString(R.string.action_error), "Please Confirm the information you provided is correct and true");

        }
    }

    private void postRequestaccount_detail_Saveurl(String Url, HashMap<String, String> jsonParams) {

        profileLoadingProgressbar.setVisibility(View.VISIBLE);

        System.out.println("-------------account_detail_Save Url----------------" + Url);
        System.out.println("-------------account_detail jsonParams----------------" + jsonParams);

        mRequest = new ServiceRequest(NewProfile_Page_BankEdit.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------account_detail Response----------------" + response);

                String Sstatus = "";
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    String message = object.getString("message");
                    if (Sstatus.equalsIgnoreCase("1")) {

                        Edt_bankName.setEnabled(false);
                        Alert1(getResources().getString(R.string.action_success), message);

                    } else {
                        Alert(getResources().getString(R.string.action_error), message);
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                profileLoadingProgressbar.setVisibility(View.GONE);
            }

            @Override
            public void onErrorListener() {
                profileLoadingProgressbar.setVisibility(View.GONE);
            }
        });
    }


    //--------------------Code to set error for EditText-----------------------
    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(NewProfile_Page_BankEdit.this, R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(NewProfile_Page_BankEdit.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    //--------------Alert Method-----------
    private void Alert1(String title, String alert) {

        final PkDialog mDialog = new PkDialog(NewProfile_Page_BankEdit.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent local = new Intent();
                local.setAction("com.app.PaymentListPage.refreshPage");
                local.putExtra("refresh", "yes");
                sendBroadcast(local);
                finish();
                overridePendingTransition(R.anim.exit, R.anim.enter);
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void AlertError(String title, String message) {
        String msg = title + "\n" + message;
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        Snackbar bar = Snackbar.with(NewProfile_Page_BankEdit.this); // context
        bar.addView(view);
        bar.colorResource(R.color.dark_red);
        bar.position(Snackbar.SnackbarPosition.TOP);
        bar.type(SnackbarType.MULTI_LINE); // Set is as a multi-line snackbar
//        bar.text(msg); // text to be displayed
        bar.duration(Snackbar.SnackbarDuration.LENGTH_SHORT); // make it shorter
        bar.animation(true); // don't animate it
        SnackbarManager.show(bar, NewProfile_Page_BankEdit.this);
    }
}
