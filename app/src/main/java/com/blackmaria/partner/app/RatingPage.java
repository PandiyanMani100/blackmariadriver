package com.blackmaria.partner.app;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.OrientationHelper;
import androidx.recyclerview.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.Pojo.RatingPojo;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.adapter.RatingAdapterNew_recycle;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.view.Dashboard.OnlinepageConstrain;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by GANESH on 23-08-2017.
 */

public class RatingPage extends ActivityHockeyApp implements View.OnClickListener {

    private TextView Tv_name, Tv_vehicleModel, Tv_vehicleNumber;
    private RatingBar Rb_rating;
    private ImageView Iv_profile;
    private EditText Edt_comments;
    private Button Btn_submit, Btn_skip;
    private RecyclerView Lv_rating;

    private ConnectionDetector cd;
    //    private SessionManager session;
    private Dialog loadingDialog;
    private ServiceRequest mRequest;
    private RatingAdapterNew_recycle adapter;

    private String sRideID = "", sUserName = "", sUserGender = "", sUserImage = "", sUserMemberSince = "", sUserLocation = "",
            sUserRating = "";
    private ArrayList<RatingPojo> ratingList;
    private boolean isDataAvailable = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rating_new);

        initialize();

    }

    private void initialize() {
        cd = new ConnectionDetector(RatingPage.this);
        ratingList = new ArrayList<>();

        getIntentData();

        Tv_name = (TextView) findViewById(R.id.txt_driver_name);
        Tv_vehicleModel = (TextView) findViewById(R.id.txt_vehicle_model);
        Tv_vehicleNumber = (TextView) findViewById(R.id.txt_vehicle_number);
        Rb_rating = (RatingBar) findViewById(R.id.rb_driver_rating);
        Iv_profile = (ImageView) findViewById(R.id.img_profile);
        Edt_comments = (EditText) findViewById(R.id.edt_comments);
        Btn_submit = (Button) findViewById(R.id.btn_submit);
        Btn_skip = (Button) findViewById(R.id.btn_skip);
        Lv_rating = (RecyclerView) findViewById(R.id.Lv_rating);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, OrientationHelper.VERTICAL, false);
        Lv_rating.setLayoutManager(linearLayoutManager);
        Lv_rating.setItemAnimator(new DefaultItemAnimator());


        Btn_submit.setOnClickListener(this);
        Btn_skip.setOnClickListener(this);

        Edt_comments.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    CloseKeyboard(Edt_comments);
                }
                return false;
            }
        });

        if (cd.isConnectingToInternet()) {
            postRequest_RatingList(Iconstant.Rating_option_Url);
        } else {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
        }

    }

    private void getIntentData() {

        Intent intent = getIntent();
        if (intent != null) {
            sRideID = intent.getStringExtra("RideID");
        }

    }

    @Override
    public void onClick(View v) {
        if (v == Btn_submit) {
            CloseKeyboardNew();
            boolean isRatingEmpty = false;
            if (ratingList != null) {

                for (int i = 0; i < ratingList.size(); i++) {
                    if (!ratingList.get(i).getRatingcount().equalsIgnoreCase("")) {
                        float number = Float.parseFloat(ratingList.get(i).getRatingcount());
                        if (number > 0.0) {
                            isRatingEmpty = true;
                        }
                    }
                }

                if (isRatingEmpty) {
                    if (cd.isConnectingToInternet()) {

                        System.out.println("------------ride_id-------------" + sRideID);
                        System.out.println("------------comments-------------" + Edt_comments.getText().toString().trim());
                        System.out.println("------------ratingsFor-------------" + "driver");
                        if (Edt_comments.getText().toString().trim().length() > 0) {
                            HashMap<String, String> jsonParams = new HashMap<String, String>();
                            jsonParams.put("comments", Edt_comments.getText().toString().trim());
                            jsonParams.put("ratingsFor", "rider");
                            jsonParams.put("ride_id", sRideID);
                            for (int i = 0; i < ratingList.size(); i++) {
                                jsonParams.put("ratings[" + i + "][option_id]", ratingList.get(i).getRatingId());
                                jsonParams.put("ratings[" + i + "][option_title]", ratingList.get(i).getRatingName());
                                jsonParams.put("ratings[" + i + "][rating]", ratingList.get(i).getRatingcount());
                            }
                            System.out.println("------------jsonParams-------------" + jsonParams);

                            postRequest_SubmitRating(Iconstant.Rating_submit_Url, jsonParams);

                        } else {
                            Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.rating_label_comment_cannot_be_empty));
                        }
                    } else {
                        Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                    }
                } else {
                    Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.rating_label_rate_everything));
                }

            }
        } else if (v == Btn_skip) {

            CloseKeyboardNew();

            Intent finish_TripPage = new Intent();
            finish_TripPage.setAction("com.app.finish.TripPage");
            sendBroadcast(finish_TripPage);

            Intent finish_FarePage = new Intent();
            finish_FarePage.setAction("com.app.finish.FarePage");
            sendBroadcast(finish_FarePage);

            Intent finish_GoOnlinePage = new Intent();
            finish_GoOnlinePage.setAction("com.app.finish.GoOnlinePage");
            sendBroadcast(finish_GoOnlinePage);

            Intent intent = new Intent(RatingPage.this, OnlinepageConstrain.class);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);

        }
    }

    //-----------------------Rating List Post Request-----------------
    private void postRequest_RatingList(String Url) {

        showDialog();

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("optionsFor", "rider");
        jsonParams.put("ride_id", sRideID);

        System.out.println("-------------Rating List Url----------------" + Url);
        System.out.println("-------------Rating List Params----------------" + jsonParams);

        mRequest = new ServiceRequest(RatingPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Rating List Response----------------" + response);

                String Sstatus = "";
                String SRating_status = "";

                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    SRating_status = object.getString("ride_ratting_status");

                    if (Sstatus.equalsIgnoreCase("1")) {

                        JSONObject user_profile_object = object.getJSONObject("user_profile");
                        if (user_profile_object.length() > 0) {
                            sUserName = user_profile_object.getString("user_name");
                            sUserGender = user_profile_object.getString("user_gender");
                            sUserImage = user_profile_object.getString("user_image");
                            sUserMemberSince = user_profile_object.getString("member_since");
                            sUserLocation = user_profile_object.getString("location");
                            sUserRating = user_profile_object.getString("user_review");
                        }

                        JSONArray payment_array = object.getJSONArray("review_options");
                        if (payment_array.length() > 0) {
                            ratingList.clear();
                            for (int i = 0; i < payment_array.length(); i++) {
                                JSONObject reason_object = payment_array.getJSONObject(i);
                                RatingPojo pojo = new RatingPojo();
                                pojo.setRatingId(reason_object.getString("option_id"));
                                pojo.setRatingName(reason_object.getString("option_title"));
                                pojo.setRatingcount("");
                                ratingList.add(pojo);
                            }
                            isDataAvailable = true;
                        } else {
                            isDataAvailable = false;
                        }
                    }

                    if (Sstatus.equalsIgnoreCase("1") && isDataAvailable) {

                        if (SRating_status.equalsIgnoreCase("1")) {

                            Toast.makeText(getApplicationContext(), "Already submitted your rating", Toast.LENGTH_SHORT).show();

                        } else {

                            if (sUserImage.length() > 0) {
                                Picasso.with(RatingPage.this)
                                        .load(sUserImage)
                                        .fit()
                                        .into(Iv_profile);
                            }

                            Tv_name.setText(sUserName);
                            Tv_vehicleModel.setText(sUserLocation);

                            float rating = 0;
                            if (sUserRating != null && sUserRating.length() > 0) {
                                try {
                                    rating = Float.parseFloat(sUserRating);
                                } catch (NumberFormatException e) {
                                    e.printStackTrace();
                                }
                            }
                            Rb_rating.setRating(rating);

                            adapter = new RatingAdapterNew_recycle(RatingPage.this, ratingList);
                            Lv_rating.setAdapter(adapter);
//                            Lv_rating.setExpanded(true);
                        }

                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                dismissDialog();
            }

            @Override
            public void onErrorListener() {
                dismissDialog();
            }
        });
    }

    //-----------------------Submit Rating Post Request-----------------
    private void postRequest_SubmitRating(String Url, final HashMap<String, String> jsonParams) {

        showDialog();

        System.out.println("-------------Submit Rating Url----------------" + Url);

        mRequest = new ServiceRequest(RatingPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("------------Submit Rating Response----------------" + response);

                String Sstatus = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {

                        String sResponse = object.getString("response");

                        final PkDialog mDialog = new PkDialog(RatingPage.this);
                        mDialog.setDialogTitle(getResources().getString(R.string.action_success));
                        mDialog.setDialogMessage(sResponse);
                        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mDialog.dismiss();

                                Intent finish_TripPage = new Intent();
                                finish_TripPage.setAction("com.app.finish.TripPage");
                                sendBroadcast(finish_TripPage);

                                Intent finish_FarePage = new Intent();
                                finish_FarePage.setAction("com.app.finish.FarePage");
                                sendBroadcast(finish_FarePage);

                                Intent finish_GoOnlinePage = new Intent();
                                finish_GoOnlinePage.setAction("com.app.finish.GoOnlinePage");
                                sendBroadcast(finish_GoOnlinePage);

                                Intent intent = new Intent(RatingPage.this, OnlinepageConstrain.class);
                                startActivity(intent);
                                finish();
                                overridePendingTransition(R.anim.enter, R.anim.exit);

                            }
                        });
                        mDialog.show();
                    } else {
                        String Sresponse = object.getString("response");
                        Alert(getResources().getString(R.string.alert_label_title), Sresponse);
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                dismissDialog();
            }

            @Override
            public void onErrorListener() {
                dismissDialog();
            }
        });
    }


    //--------------Alert Method-----------
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(RatingPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void showDialog() {
        loadingDialog = new Dialog(RatingPage.this);
        loadingDialog.getWindow();
        loadingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loadingDialog.setContentView(R.layout.custom_loading);
        loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        loadingDialog.setCanceledOnTouchOutside(false);
        loadingDialog.show();

        TextView dialog_title = (TextView) loadingDialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));
    }

    private void dismissDialog() {
        if (loadingDialog != null && loadingDialog.isShowing()) {
            loadingDialog.dismiss();
        }
    }

    //--------------Close KeyBoard Method-----------
    @SuppressLint("WrongConstant")
    private void CloseKeyboard(EditText edittext) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(edittext.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    @SuppressWarnings("WrongConstant")
    private void CloseKeyboardNew() {
        try {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow((null == getCurrentFocus()) ? null : getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            //Do nothing
            return true;
        }
        return false;
    }


}
