package com.blackmaria.partner.app;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.R;

/**
 * Created by Prem Kumar and Anitha on 3/14/2017.
 */

public class TalkPage extends ActivityHockeyApp implements View.OnClickListener
{
    private TextView Tv_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.talk_page);
        initialize();
    }

    private void initialize() {
        Tv_back = (TextView) findViewById(R.id.talk_page_headerBar_back_textView);
        Tv_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == Tv_back) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
            return true;
        }
        return false;
    }
}
