package com.blackmaria.partner.app;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;

import com.blackmaria.partner.Hockeyapp.FragmentActivityHockeyApp;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.CustomEdittext;
import com.blackmaria.partner.latestpackageview.widgets.CustomTextViewForm;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.blackmaria.partner.latestpackageview.Countrycodepicker.countrycodepicker.CountryPicker;
import com.blackmaria.partner.latestpackageview.Countrycodepicker.countrycodepicker.CountryPickerListener;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;

/**
 * Created by user144 on 1/8/2018.
 */

@SuppressLint("Registered")
public class NewProfile_Page_SosEdit extends FragmentActivityHockeyApp implements View.OnClickListener {
    private ServiceRequest mRequest;
    private ConnectionDetector cd;
    private boolean isInternetPresent = false;
    private SessionManager session;
    private String UserID = "";
    private RelativeLayout profileEditLyHeader;
    private ImageView cancelLayout;
    private CustomEdittext SOSDialogFormNameEdittext;
    private CustomTextViewForm myprofileDialogFormEmailTextview;
    private CustomEdittext SOSDialogFormEmailEdittext;
    private CustomTextViewForm profilePageFormAgeTextview;
    private CustomEdittext SOSDialogFormMobileEdittext;
    private CustomEdittext SOSDialogFormRelationEdittext;
    private AutoCompleteTextView SOSDialogFormAddressEdittext;
    private CustomEdittext SOSDialogFormCityEdittext;
    private CustomEdittext SOSDialogFormStateEdittext;
    private CustomEdittext SOSDialogFormPostcodeEdittext;
    private CustomEdittext SOSDialogFormCountryEdittext;
    private RelativeLayout userProfileUpdateBtn;
    private SmoothProgressBar profileLoadingProgressbar;
    private CustomTextViewForm myloginDialogFormCountryCodeTextview;
    private CountryPicker picker;

    ArrayList<String> itemList_location = new ArrayList<String>();
    ArrayList<String> itemList_placeId = new ArrayList<String>();
    ArrayAdapter<String> adapter;
    private Boolean isDatavailable = false;
    private boolean isDataAvailable = false;
    private boolean isAddressAvailable = false;
    private String sLatitude = "", sLongitude = "", sSelected_location = "";

    private    String name = "", email = "", mobile = "", countrycode = "", relation = "", address = "", city = "", state = "", pincode = "", country = "", sos_count = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_sos_edit_layout);
        initialize();
        OtherClickEvents();
    }

    private void initialize() {
        session = new SessionManager(NewProfile_Page_SosEdit.this);
        cd = new ConnectionDetector(NewProfile_Page_SosEdit.this);
        isInternetPresent = cd.isConnectingToInternet();

        HashMap<String, String> info = session.getUserDetails();
        UserID = info.get(SessionManager.KEY_DRIVERID);



        profileEditLyHeader = (RelativeLayout) findViewById(R.id.profile_edit_ly_header);
        cancelLayout = (ImageView) findViewById(R.id.cancel_layout);
        SOSDialogFormNameEdittext = (CustomEdittext) findViewById(R.id.SOS_dialog_form_name_edittext);
        myprofileDialogFormEmailTextview = (CustomTextViewForm) findViewById(R.id.myprofile_dialog_form_email_textview);
        SOSDialogFormEmailEdittext = (CustomEdittext) findViewById(R.id.SOS_dialog_form_email_edittext);
        profilePageFormAgeTextview = (CustomTextViewForm) findViewById(R.id.profile_page_form_age_textview);
        SOSDialogFormMobileEdittext = (CustomEdittext) findViewById(R.id.SOS_dialog_form_gender_edittext);
        SOSDialogFormRelationEdittext = (CustomEdittext) findViewById(R.id.SOS_dialog_form_relation_edittext);
        SOSDialogFormAddressEdittext = (AutoCompleteTextView) findViewById(R.id.SOS_dialog_form_address_edittext);
        SOSDialogFormCityEdittext = (CustomEdittext) findViewById(R.id.SOS_dialog_form_city_edittext);
        SOSDialogFormStateEdittext = (CustomEdittext) findViewById(R.id.SOS_dialog_form_state_edittext);
        SOSDialogFormPostcodeEdittext = (CustomEdittext) findViewById(R.id.SOS_dialog_form_postcode_edittext);
        SOSDialogFormCountryEdittext = (CustomEdittext) findViewById(R.id.SOS_dialog_form_country_edittext);
        userProfileUpdateBtn = (RelativeLayout) findViewById(R.id.user_profile_update_btn);
        profileLoadingProgressbar = (SmoothProgressBar) findViewById(R.id.profile_loading_progressbar);
        myloginDialogFormCountryCodeTextview = (CustomTextViewForm) findViewById(R.id.mylogin_dialog_form_country_code_textview);
        picker = CountryPicker.newInstance(getResources().getString(R.string.select_country_lable));


        Intent in = getIntent();
        name = in.getStringExtra("name");
        email = in.getStringExtra("email");
        mobile = in.getStringExtra("mobile");
        relation = in.getStringExtra("relatuion");
        address = in.getStringExtra("address");
        city = in.getStringExtra("city");
        state = in.getStringExtra("state");
        pincode = in.getStringExtra("picode");
        country = in.getStringExtra("country");
        countrycode = in.getStringExtra("countryCode");
        sos_count = in.getStringExtra("soscount");

        if(name.equalsIgnoreCase("n/a")){
            name ="";
        }else if(email.equalsIgnoreCase("n/a")){
            email ="";
        }else if(mobile.equalsIgnoreCase("n/a")){
            mobile ="";
        }else if(relation.equalsIgnoreCase("n/a")){
            relation ="";
        }else if(address.equalsIgnoreCase("n/a")){
            address ="";
        }


        myloginDialogFormCountryCodeTextview.setText(countrycode);
        SOSDialogFormNameEdittext.setText(name);
        SOSDialogFormEmailEdittext.setText(email);
        SOSDialogFormMobileEdittext.setText(mobile);
        SOSDialogFormRelationEdittext.setText(relation);
        SOSDialogFormAddressEdittext.setText(address);
        SOSDialogFormCityEdittext.setText(city);
        SOSDialogFormStateEdittext.setText(state);
        SOSDialogFormPostcodeEdittext.setText(pincode);
        SOSDialogFormCountryEdittext.setText(country);


        cancelLayout.setOnClickListener(this);
        userProfileUpdateBtn.setOnClickListener(this);
    }

    private void OtherClickEvents() {




        myloginDialogFormCountryCodeTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });

        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {
                picker.dismiss();
                myloginDialogFormCountryCodeTextview.setText(dialCode);
                countrycode=dialCode;
                // close keyboard
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(myloginDialogFormCountryCodeTextview.getWindowToken(), 0);
            }
        });
        SOSDialogFormAddressEdittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                cd = new ConnectionDetector(NewProfile_Page_SosEdit.this);
                isInternetPresent = cd.isConnectingToInternet();

                if (isInternetPresent) {
                    if (mRequest != null) {
                        mRequest.cancelRequest();
                    }

                    String sTypedPlace = "";
                    try {
                        sTypedPlace = URLEncoder.encode(SOSDialogFormAddressEdittext.getText().toString().toLowerCase(), "utf-8");
                    } catch (UnsupportedEncodingException e1) {
                        e1.printStackTrace();
                    }

                    CitySearchRequest(Iconstant.place_search_url + sTypedPlace, "sos_contact_address");
                } else {
                    Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });


        SOSDialogFormAddressEdittext.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                SOSDialogFormAddressEdittext.dismissDropDown();
                sSelected_location = itemList_location.get(position);
                cd = new ConnectionDetector(NewProfile_Page_SosEdit.this);
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    LatLongRequest(Iconstant.GetAddressFrom_LatLong_url + itemList_placeId.get(position), "sos_contact_address");
                } else {
                    Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
                }


            }
        });

    }

    @Override
    public void onClick(View v) {
        if (v == userProfileUpdateBtn) {
            SosUpdate();
        } else if (v == cancelLayout) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }
    }

    private void SosUpdate() {
        try {
            SOSDialogFormNameEdittext.setText(Html.fromHtml(SOSDialogFormNameEdittext.getText().toString()).toString());
            SOSDialogFormEmailEdittext.setText(Html.fromHtml(SOSDialogFormEmailEdittext.getText().toString()).toString());
            SOSDialogFormMobileEdittext.setText(Html.fromHtml(SOSDialogFormMobileEdittext.getText().toString()).toString());
            SOSDialogFormRelationEdittext.setText(Html.fromHtml(SOSDialogFormRelationEdittext.getText().toString()).toString());
            SOSDialogFormAddressEdittext.setText(Html.fromHtml(SOSDialogFormAddressEdittext.getText().toString()).toString());
            /*SOSDialogFormCityEdittext.setText(Html.fromHtml(SOSDialogFormCityEdittext.getText().toString()).toString());
            SOSDialogFormStateEdittext.setText(Html.fromHtml(SOSDialogFormStateEdittext.getText().toString()).toString());
            SOSDialogFormPostcodeEdittext.setText(Html.fromHtml(SOSDialogFormPostcodeEdittext.getText().toString()).toString());
            SOSDialogFormCountryEdittext.setText(Html.fromHtml(SOSDialogFormCountryEdittext.getText().toString()).toString());

*/
            if (SOSDialogFormNameEdittext.getText().toString().length() == 0) {
                AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.profile_label_alert_username));
            } else if (!isValidEmail(SOSDialogFormEmailEdittext.getText().toString().trim().replace(" ", ""))) {
                AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.profile_label_alert_email));
            } else if (!isValidPhoneNumber(SOSDialogFormMobileEdittext.getText().toString())) {
                AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.profile_lable_error_mobile));
            } else if (SOSDialogFormRelationEdittext.getText().toString().length() == 0) {
                AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.profile_label_alert_relationship));
            } else if (SOSDialogFormAddressEdittext.getText().toString().length() == 0) {
                AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.profile_label_alert_address1));
            } /*else if (SOSDialogFormCityEdittext.getText().toString().length() == 0) {
                erroredit(SOSDialogFormCityEdittext, getResources().getString(R.string.profile_label_alert_streetname));
            } else if (SOSDialogFormStateEdittext.getText().toString().length() == 0) {
                erroredit(SOSDialogFormStateEdittext, getResources().getString(R.string.profile_label_alert_state));
            } else if (SOSDialogFormPostcodeEdittext.getText().toString().length() == 0) {
                erroredit(SOSDialogFormPostcodeEdittext, getResources().getString(R.string.profile_label_alert_pincode));
            } else if (SOSDialogFormCountryEdittext.getText().toString().length() == 0) {
                erroredit(SOSDialogFormCountryEdittext, getResources().getString(R.string.profile_label_alert_district));
            } */else {

                cd = new ConnectionDetector(NewProfile_Page_SosEdit.this);
                isInternetPresent = cd.isConnectingToInternet();

                if (isInternetPresent) {


                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    jsonParams.put("driver_id", UserID);
                    jsonParams.put("mode", "update");
                    if (sos_count.equalsIgnoreCase("1")) {
                        jsonParams.put("edit_index", "key_1");
                        jsonParams.put("emergency_conatct[key_1][em_name]", SOSDialogFormNameEdittext.getText().toString());
                        jsonParams.put("emergency_conatct[key_1][em_email]", SOSDialogFormEmailEdittext.getText().toString());
                        jsonParams.put("emergency_conatct[key_1][em_mobile]", SOSDialogFormMobileEdittext.getText().toString());
                        jsonParams.put("emergency_conatct[key_1][em_mobile_code]", countrycode);
                        jsonParams.put("emergency_conatct[key_1][em_relationship]", SOSDialogFormRelationEdittext.getText().toString());
                        jsonParams.put("emergency_conatct[key_1][em_address]", SOSDialogFormAddressEdittext.getText().toString());
                       /* jsonParams.put("emergency_conatct[key_1][em_city]", SOSDialogFormCityEdittext.getText().toString());
                        jsonParams.put("emergency_conatct[key_1][em_state]", SOSDialogFormStateEdittext.getText().toString());
                        jsonParams.put("emergency_conatct[key_1][em_postcode]", SOSDialogFormPostcodeEdittext.getText().toString());
                        jsonParams.put("emergency_conatct[key_1][em_country]", SOSDialogFormCountryEdittext.getText().toString());
*/
                    } else {
                        jsonParams.put("edit_index", "key_2");
                        jsonParams.put("emergency_conatct[key_2][em_name]", SOSDialogFormNameEdittext.getText().toString());
                        jsonParams.put("emergency_conatct[key_2][em_email]", SOSDialogFormEmailEdittext.getText().toString());
                        jsonParams.put("emergency_conatct[key_2][em_mobile]", SOSDialogFormMobileEdittext.getText().toString());
                        jsonParams.put("emergency_conatct[key_2][em_mobile_code]", countrycode);
                        jsonParams.put("emergency_conatct[key_2][em_relationship]", SOSDialogFormRelationEdittext.getText().toString());
                        jsonParams.put("emergency_conatct[key_2][em_address]", SOSDialogFormAddressEdittext.getText().toString());
                        /*jsonParams.put("emergency_conatct[key_2][em_city]", SOSDialogFormCityEdittext.getText().toString());
                        jsonParams.put("emergency_conatct[key_2][em_state]", SOSDialogFormStateEdittext.getText().toString());
                        jsonParams.put("emergency_conatct[key_2][em_postcode]", SOSDialogFormPostcodeEdittext.getText().toString());
                        jsonParams.put("emergency_conatct[key_2][em_country]", SOSDialogFormCountryEdittext.getText().toString());
                    */}

                    System.out.println("-----------sos jsonParams--------------" + jsonParams);

                    PostRequest_updatesos_info(Iconstant.userprofile_update_sos_info_url, jsonParams, sos_count);

                } else {

                    Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));

                }


            }


        } catch (Exception e) {
            e.printStackTrace();

        }
    }
    private void PostRequest_updatesos_info(String Url, final HashMap<String, String> jsonParams, final String sos_key) {
        profileLoadingProgressbar.setVisibility(View.VISIBLE);
        System.out.println("--------------updateProfile-------------------" + Url);
        System.out.println("--------------updateProfile jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(NewProfile_Page_SosEdit.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
//                Log.e("registr", response);
                System.out.println("--------------updateProfile reponse-------------------" + response);

                String Sstatus = "", Smessage = "", Up_login_user_name = "", Up_login_phone_no = "", Up_login_country_code = "", Up_home_pho_no = "";


                String key1_sos_name = "", key1_sos_email = "", key1_sos_mobileno = "", key1_sos_relationship = "", key1_sos_address = "",
                        key2_sos_name = "", key2_sos_email = "", key2_sos_mobileno = "", key2_sos_relationship = "", key2_sos_address = "";
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {

                        JSONObject jsonObject = object.getJSONObject("response");

                        Smessage = jsonObject.getString("message");

                        Object check_object = jsonObject.get("emergency_contact");
                        if (check_object instanceof JSONObject) {

                            JSONObject emergency_contact_jsonObject = jsonObject.getJSONObject("emergency_contact");
                            if (emergency_contact_jsonObject.length() > 0) {

                                if (emergency_contact_jsonObject.has("key_1")) {
                                    JSONObject emergency_contactkey_1_jsonObject = emergency_contact_jsonObject.getJSONObject("key_1");
                                    if (emergency_contactkey_1_jsonObject.length() > 0) {

                                      /*  key1_sos_name = emergency_contactkey_1_jsonObject.getString("name");
                                        key1_sos_email = emergency_contactkey_1_jsonObject.getString("email");
                                        key1_sos_mobileno = emergency_contactkey_1_jsonObject.getString("mobile");
                                        key1_sos_relationship = emergency_contactkey_1_jsonObject.getString("realtionship");
                                        key1_sos_address = emergency_contactkey_1_jsonObject.getString("address");
                                        Dialog_shows_sos_city = emergency_contactkey_1_jsonObject.getString("city");
                                        Dialog_shows_sos_state = emergency_contactkey_1_jsonObject.getString("state");
                                        Dialog_shows_sos_pincode = emergency_contactkey_1_jsonObject.getString("postcode");
                                        Dialog_shows_sos_country = emergency_contactkey_1_jsonObject.getString("country");

                                        up_sos_contact1_stats = true;*/

                                        if (emergency_contact_jsonObject.has("key_2")) {
                                            JSONObject emergency_contactkey_2_jsonObject = emergency_contact_jsonObject.getJSONObject("key_2");
                                            if (emergency_contactkey_2_jsonObject.length() > 0) {
/*
                                                key2_sos_name = emergency_contactkey_2_jsonObject.getString("name");
                                                key2_sos_email = emergency_contactkey_2_jsonObject.getString("email");
                                                key2_sos_mobileno = emergency_contactkey_2_jsonObject.getString("mobile");
                                                key2_sos_relationship = emergency_contactkey_2_jsonObject.getString("realtionship");
                                                key2_sos_address = emergency_contactkey_2_jsonObject.getString("address");
                                                Dialog_shows_sos_city1 = emergency_contactkey_2_jsonObject.getString("city");
                                                Dialog_shows_sos_state1 = emergency_contactkey_2_jsonObject.getString("state");
                                                Dialog_shows_sos_pincode1 = emergency_contactkey_2_jsonObject.getString("postcode");
                                                Dialog_shows_sos_country1 = emergency_contactkey_2_jsonObject.getString("country");

                                                 up_sos_contact2_stats = true;*/

                                            }
                                        }

                                    }
                                }
                            }

                        } else {

                        }
                        profileLoadingProgressbar.setVisibility(View.GONE);


                    } else {
                        Smessage = object.getString("response");
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    profileLoadingProgressbar.setVisibility(View.GONE);
                }

                if (Sstatus.equalsIgnoreCase("1")) {



                    Alert1(getResources().getString(R.string.action_success), Smessage);
                } else {
                    Alert(getResources().getString(R.string.action_error), Smessage);
                }
                profileLoadingProgressbar.setVisibility(View.GONE);
            }

            @Override
            public void onErrorListener() {
                profileLoadingProgressbar.setVisibility(View.GONE);
            }
        });
    }
    //-------------------Search Place Request----------------
    private void CitySearchRequest(String Url, final String addressforwho) {

        System.out.println("--------------Search city url-------------------" + Url);

        mRequest = new ServiceRequest(NewProfile_Page_SosEdit.this);
        mRequest.makeServiceRequest(Url, Request.Method.GET, null, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------Search city  reponse-------------------" + response);
                String status = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");
                        JSONArray place_array = object.getJSONArray("predictions");
                        if (status.equalsIgnoreCase("OK")) {
                            if (place_array.length() > 0) {
                                itemList_location.clear();
                                itemList_placeId.clear();
                                for (int i = 0; i < place_array.length(); i++) {
                                    JSONObject place_object = place_array.getJSONObject(i);
                                    itemList_location.add(place_object.getString("description"));
                                    itemList_placeId.add(place_object.getString("place_id"));
                                }
                                isDataAvailable = true;
                            } else {
                                itemList_location.clear();
                                itemList_placeId.clear();
                                isDataAvailable = false;
                            }
                        } else {
                            itemList_location.clear();
                            itemList_placeId.clear();
                            isDataAvailable = false;
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (isDataAvailable) {
                    if (addressforwho.equalsIgnoreCase("user_profile_address")) {
                        adapter = new ArrayAdapter<String>
                                (NewProfile_Page_SosEdit.this, R.layout.auto_complete_list_item, R.id.autoComplete_textView, itemList_location);
                        SOSDialogFormAddressEdittext.setAdapter(adapter);
                    } else {
                        adapter = new ArrayAdapter<String>
                                (NewProfile_Page_SosEdit.this, R.layout.auto_complete_list_item, R.id.autoComplete_textView, itemList_location);
                        SOSDialogFormAddressEdittext.setAdapter(adapter);
                    }
                }
            }

            @Override
            public void onErrorListener() {
            }
        });
    }


    //-------------------Get Latitude and Longitude from Address(Place ID) Request----------------
    private void LatLongRequest(String Url, final String addressforwho) {


        profileLoadingProgressbar.setVisibility(View.VISIBLE);
        System.out.println("--------------LatLong url-------------------" + Url);

        mRequest = new ServiceRequest(NewProfile_Page_SosEdit.this);
        mRequest.makeServiceRequest(Url, Request.Method.GET, null, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------LatLong  reponse-------------------" + response);
                String status = "", sArea = "", sLocality = "", sCity = "", sState = "", sPostalCode = "", sRoute = "", sStreetName = "", sCountry = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");
                        JSONObject place_object = object.getJSONObject("result");
                        if (status.equalsIgnoreCase("OK")) {
                            if (place_object.length() > 0) {

                                sArea = place_object.getString("vicinity");
                                JSONArray addressArray = place_object.getJSONArray("address_components");
                                if (addressArray.length() > 0) {
                                    for (int i = 0; i < addressArray.length(); i++) {
                                        JSONObject address_object = addressArray.getJSONObject(i);

                                        JSONArray typesArray = address_object.getJSONArray("types");
                                        if (typesArray.length() > 0) {
                                            for (int j = 0; j < typesArray.length(); j++) {


                                                if (typesArray.get(j).toString().equalsIgnoreCase("locality")) {
                                                    sLocality = address_object.getString("long_name");
                                                } else if (typesArray.get(j).toString().equalsIgnoreCase("administrative_area_level_2")) {
                                                    sCity = address_object.getString("long_name");
                                                } else if (typesArray.get(j).toString().equalsIgnoreCase("administrative_area_level_1")) {
                                                    sState = address_object.getString("long_name");
                                                } else if (typesArray.get(j).toString().equalsIgnoreCase("postal_code")) {
                                                    sPostalCode = address_object.getString("long_name");
                                                } else if (typesArray.get(j).toString().equalsIgnoreCase("route")) {
                                                    sRoute = address_object.getString("long_name");
                                                } else if (typesArray.get(j).toString().equalsIgnoreCase("sublocality_level_1")) {
                                                    sStreetName = address_object.getString("long_name");
                                                } else if (typesArray.get(j).toString().equalsIgnoreCase("country")) {
                                                    sCountry = address_object.getString("long_name");
                                                }


                                            }


                                            isAddressAvailable = true;
                                        } else {
                                            isAddressAvailable = false;
                                        }
                                    }
                                } else {
                                    isAddressAvailable = false;
                                }

                                JSONObject geometry_object = place_object.getJSONObject("geometry");
                                if (geometry_object.length() > 0) {
                                    JSONObject location_object = geometry_object.getJSONObject("location");
                                    if (location_object.length() > 0) {
                                        sLatitude = location_object.getString("lat");
                                        sLongitude = location_object.getString("lng");
                                        isDataAvailable = true;
                                    } else {
                                        isDataAvailable = false;
                                    }
                                } else {
                                    isDataAvailable = false;
                                }
                            } else {
                                isDataAvailable = false;
                            }
                        } else {
                            isDataAvailable = false;
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (isDataAvailable) {

                    SOSDialogFormAddressEdittext.setText(sSelected_location);
                    SOSDialogFormCityEdittext.setText(sCity);
                    SOSDialogFormStateEdittext.setText(sState);
                    SOSDialogFormPostcodeEdittext.setText(sPostalCode);
                    SOSDialogFormCountryEdittext.setText(sCountry);


                    profileLoadingProgressbar.setVisibility(View.GONE);
                } else {
                    profileLoadingProgressbar.setVisibility(View.GONE);
                    Alert(getResources().getString(R.string.action_error), status);
                }
            }

            @Override
            public void onErrorListener() {
                profileLoadingProgressbar.setVisibility(View.GONE);
            }
        });
    }

    //-------------------------code to Check Email Validation-----------------------
    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }


    //--------------------Code to set error for EditText-----------------------
    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(NewProfile_Page_SosEdit.this, R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(NewProfile_Page_SosEdit.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    // validating Phone Number
    public static final boolean isValidPhoneNumber(CharSequence target) {
        if (target == null || TextUtils.isEmpty(target) || target.length() <= 5) {
            return false;
        } else {
            return android.util.Patterns.PHONE.matcher(target).matches();
        }
    }

    //--------------Alert Method-----------
    private void Alert1(String title, String alert) {

        final PkDialog mDialog = new PkDialog(NewProfile_Page_SosEdit.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.close_lable), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent local = new Intent();
                local.setAction("com.app.PaymentListPage.refreshPage");
                local.putExtra("refresh", "yes");
                sendBroadcast(local);
                finish();
                overridePendingTransition(R.anim.exit, R.anim.enter);
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void AlertError(String title, String message) {
        String msg = title + "\n" + message;
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        Snackbar bar = Snackbar.with(NewProfile_Page_SosEdit.this); // context
        bar.addView(view);
        bar.colorResource(R.color.dark_red);
        bar.position(Snackbar.SnackbarPosition.TOP);
        bar.type(SnackbarType.MULTI_LINE); // Set is as a multi-line snackbar
//        bar.text(msg); // text to be displayed
        bar.duration(Snackbar.SnackbarDuration.LENGTH_SHORT); // make it shorter
        bar.animation(true); // don't animate it
        SnackbarManager.show(bar, NewProfile_Page_SosEdit.this);
    }
}
