package com.blackmaria.partner.app;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.partner.Pojo.ProfilePojo;
import com.blackmaria.partner.Pojo.WalletMoneyPojo;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.adapter.ProfileAdapter;
import com.blackmaria.partner.adapter.XenditBankDetailsAdapter;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.devspark.appmsg.AppMsg;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;



public class DriverAccount extends AppCompatActivity implements View.OnClickListener {

    private ImageView Iv_back;
    private RelativeLayout RL_titleBasicInfo, RL_titleDriverInfo, RL_titlePaymentGateway, RL_titleBankInfo;
    private RelativeLayout RL_basicInfo, RL_driverInfo, RL_paymentGateway, RL_bankInfo;
    private ExpandableHeightListView Lv_basicInfo, Lv_driverInfo;
    private EditText Edt_paypalId, Edt_stripeId, Edt_authorizeId, Edt_bankName, Edt_address, Edt_city, Edt_country, Edt_accountNo,
            Edt_accountName, Edt_swiftCode, Edt_routingNo, Edt_ibanNo;
    private Button Btn_editBankInfo, Btn_editPaymentGateway;
    private LinearLayout LL_bankName, LL_address, LL_city, LL_country, LL_accountNo, LL_accountName, LL_swiftCode, LL_routingNo, LL_ibanNo;

    private String sDriveID = "", sPasscode = "", sCountryCode = "";
    private ArrayList<ProfilePojo> listBasicInfo, listDriverInfo;

    private ConnectionDetector cd;
    private SessionManager sessionManager;
    private ServiceRequest mRequest;
    private Dialog dialog;
    private ProfileAdapter basicInfoAdapter, driverInfoAdapter;

    ArrayList<WalletMoneyPojo> paymentcardlist;
    XenditBankDetailsAdapter Eadaptter;
    boolean isPaymentListAvailable = false, isDatavailable = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_account_details);

        initialize();


    }

    private void initialize() {
        sessionManager = new SessionManager(DriverAccount.this);
        cd = new ConnectionDetector(DriverAccount.this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriveID = user.get(SessionManager.KEY_DRIVERID);
        listBasicInfo = new ArrayList<ProfilePojo>();
        listDriverInfo = new ArrayList<ProfilePojo>();

        Iv_back = (ImageView) findViewById(R.id.img_back);
        RL_titleBasicInfo = (RelativeLayout) findViewById(R.id.Rl_title_basic_info);
        RL_titleDriverInfo = (RelativeLayout) findViewById(R.id.Rl_title_driver_info);




        RL_titlePaymentGateway = (RelativeLayout) findViewById(R.id.Rl_title_payment_gateway);
        RL_titleBankInfo = (RelativeLayout) findViewById(R.id.Rl_title_bank_info);
        RL_basicInfo = (RelativeLayout) findViewById(R.id.Rl_basic_info);
        RL_driverInfo = (RelativeLayout) findViewById(R.id.Rl_driver_info);
        RL_paymentGateway = (RelativeLayout) findViewById(R.id.Rl_payment_gateway);
        RL_bankInfo = (RelativeLayout) findViewById(R.id.Rl_bank_info);
        Lv_basicInfo = (ExpandableHeightListView) findViewById(R.id.lst_basic_info);
        Lv_basicInfo.setExpanded(true);
        Lv_driverInfo = (ExpandableHeightListView) findViewById(R.id.lst_driver_info);
        Lv_driverInfo.setExpanded(true);
        Edt_paypalId = (EditText) findViewById(R.id.edt_paypal_id);
        Edt_stripeId = (EditText) findViewById(R.id.edt_stripe_id);
        Edt_authorizeId = (EditText) findViewById(R.id.edt_authorize_net);
        Edt_bankName = (EditText) findViewById(R.id.edt_bank_name);
        Edt_address = (EditText) findViewById(R.id.edt_address);
        Edt_city = (EditText) findViewById(R.id.edt_city);
        Edt_country = (EditText) findViewById(R.id.edt_country);
        Edt_accountNo = (EditText) findViewById(R.id.edt_account_no);
        Edt_accountName = (EditText) findViewById(R.id.edt_account_name);
        Edt_swiftCode = (EditText) findViewById(R.id.edt_swift_code);
        Edt_routingNo = (EditText) findViewById(R.id.edt_routing_no);
        Edt_ibanNo = (EditText) findViewById(R.id.edt_iban_no);
        Btn_editBankInfo = (Button) findViewById(R.id.btn_edit_bank_info);
        Btn_editPaymentGateway = (Button) findViewById(R.id.btn_edit_sos);
        LL_bankName = (LinearLayout) findViewById(R.id.LL_bank_name);
        LL_address = (LinearLayout) findViewById(R.id.LL_address);
        LL_city = (LinearLayout) findViewById(R.id.LL_city);
        LL_country = (LinearLayout) findViewById(R.id.LL_country);
        LL_accountNo = (LinearLayout) findViewById(R.id.LL_account_no);
        LL_accountName = (LinearLayout) findViewById(R.id.LL_account_name);
        LL_swiftCode = (LinearLayout) findViewById(R.id.LL_swift_code);
        LL_routingNo = (LinearLayout) findViewById(R.id.LL_routing_no);
        LL_ibanNo = (LinearLayout) findViewById(R.id.LL_iban_no);


        Iv_back.setOnClickListener(this);
        RL_titleBasicInfo.setOnClickListener(this);
        RL_titleDriverInfo.setOnClickListener(this);
        RL_titlePaymentGateway.setOnClickListener(this);
        RL_titleBankInfo.setOnClickListener(this);
        Btn_editBankInfo.setOnClickListener(this);
        Btn_editPaymentGateway.setOnClickListener(this);

        if (cd.isConnectingToInternet()) {
            postRequest(Iconstant.Profile_driver_account_Url);
        } else {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
        }

    }

    @Override
    public void onClick(View view) {
        if (view == Iv_back) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (view == RL_titleBasicInfo) {
            if (RL_basicInfo.getVisibility() == View.VISIBLE) {
                RL_basicInfo.setVisibility(View.GONE);
            } else {
                RL_basicInfo.setVisibility(View.VISIBLE);
            }
        } else if (view == RL_titleDriverInfo) {
            if (RL_driverInfo.getVisibility() == View.VISIBLE) {
                RL_driverInfo.setVisibility(View.GONE);
            } else {
                RL_driverInfo.setVisibility(View.VISIBLE);
            }
        } else if (view == RL_titlePaymentGateway) {
            if (RL_paymentGateway.getVisibility() == View.VISIBLE) {
                RL_paymentGateway.setVisibility(View.GONE);
            } else {
                RL_paymentGateway.setVisibility(View.VISIBLE);
            }
        } else if (view == RL_titleBankInfo) {
            if (RL_bankInfo.getVisibility() == View.VISIBLE) {
                RL_bankInfo.setVisibility(View.GONE);
            } else {
                RL_bankInfo.setVisibility(View.VISIBLE);
            }
        } else if (view == Btn_editBankInfo) {

            if (Btn_editBankInfo.getText().toString().trim().equalsIgnoreCase(getResources().getString(R.string.label_edit))) {
                showPasscodeDialog(2);
            } else {

                if (Edt_bankName.getText().toString().trim().length() == 0) {
                    errorEdit(Edt_bankName, getResources().getString(R.string.driver_account_label_alert_bankname));
                } else if (Edt_address.getText().toString().trim().length() == 0) {
                    errorEdit(Edt_address, getResources().getString(R.string.driver_account_label_alert_bankaddess));
                } else if (Edt_city.getText().toString().trim().length() == 0) {
                    errorEdit(Edt_city, getResources().getString(R.string.driver_account_label_alert_city));
                } else if (Edt_country.getText().toString().trim().length() == 0) {
                    errorEdit(Edt_country, getResources().getString(R.string.driver_account_label_alert_country));
                } else if (Edt_accountName.getText().toString().trim().length() == 0) {
                    errorEdit(Edt_accountName, getResources().getString(R.string.driver_account_label_alert_bankaccholderno));
                } else if (Edt_accountNo.getText().toString().trim().length() == 0) {
                    errorEdit(Edt_accountNo, getResources().getString(R.string.driver_account_label_alert_bankaccno));
                } else if (Edt_swiftCode.getText().toString().trim().length() == 0) {
                    errorEdit(Edt_swiftCode, getResources().getString(R.string.driver_account_label_alert_swiftcode));
                } else if (Edt_routingNo.getText().toString().trim().length() == 0) {
                    errorEdit(Edt_routingNo, getResources().getString(R.string.driver_account_label_alert_routingno));
                } /*else if (Edt_ibanNo.getText().toString().trim().length() == 0) {
                    errorEdit(Edt_ibanNo, getResources().getString(R.string.driver_account_label_alert_ibanno));
                } */ else {
                    HashMap<String, String> jsonParams = new HashMap<>();
                    jsonParams.put("driver_id", sDriveID);
                    jsonParams.put("acc_holder_name", Edt_accountName.getText().toString());
                    jsonParams.put("acc_holder_address", Edt_address.getText().toString());
                    jsonParams.put("acc_number", Edt_accountNo.getText().toString());
                    jsonParams.put("city", Edt_city.getText().toString());
                    jsonParams.put("country", Edt_country.getText().toString());
                    jsonParams.put("swift_code", Edt_swiftCode.getText().toString());
                    jsonParams.put("bank_name", Edt_bankName.getText().toString());
                    /*jsonParams.put("branch_address", branch_address.getText().toString());
                    jsonParams.put("branch_name", bank_branch_name.getText().toString());*/
                    jsonParams.put("routing_number", Edt_routingNo.getText().toString());
                    jsonParams.put("iban_number", Edt_ibanNo.getText().toString());

                    System.out.println("-----------EditProfile jsonParams--------------" + jsonParams);

                    postRequestaccount_detail_Saveurl(Iconstant.Bank_account_Save_Url, jsonParams);
                }

            }

        } else if (view == Btn_editPaymentGateway) {

            if (Btn_editPaymentGateway.getText().toString().trim().equalsIgnoreCase(getResources().getString(R.string.label_edit))) {

                /*Edt_paypalId.setEnabled(true);
                Edt_stripeId.setEnabled(true);
                Edt_authorizeId.setEnabled(true);
                Btn_editPaymentGateway.setText(getResources().getString(R.string.label_save));*/

                showPasscodeDialog(1);

            } else if (Btn_editPaymentGateway.getText().toString().trim().equalsIgnoreCase(getResources().getString(R.string.label_save))) {

                if (!isValidEmail(Edt_paypalId.getText().toString().trim().replace(" ", ""))) {
                    errorEdit(Edt_paypalId, "Paypal ID is Invalid");
                } else {
                    postRequestPaypal_Saveurl(Iconstant.Paypal_account_Url, Edt_paypalId.getText().toString().trim().replace(" ", ""));
                }

            }

        }
    }


    private void postRequest(String Url) {

        dialog = new Dialog(DriverAccount.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_processing));


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriveID);


        System.out.println("-------------DriverAccount Url----------------" + Url);
        System.out.println("-------------DriverAccount jsonParams----------------" + jsonParams);

        mRequest = new ServiceRequest(DriverAccount.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------DriverAccount Response----------------" + response);

                String Sstatus = "", str_paypal_id = "", str_stripe_id = "", acc_holder_name = "", acc_holder_address = "", acc_number = "", str_bank_name = "", branch_name = "", str_branch_address = "", str_swift_code = "", routing_number = "";
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        JSONObject payment_object = response_object.getJSONObject("payment_gateway");
                        sPasscode = response_object.getString("pass_code");
                        sCountryCode = response_object.getString("country_code");
                        if (response_object.length() > 0) {
                            Object check_profile_object = response_object.get("account");
                            if (check_profile_object instanceof JSONArray) {
                                JSONArray profile_array = response_object.getJSONArray("account");
                                if (profile_array.length() > 0) {
                                    listBasicInfo.clear();
                                    for (int i = 0; i < profile_array.length(); i++) {
                                        JSONObject profile_object = profile_array.getJSONObject(i);
                                        ProfilePojo stdrate_pojo = new ProfilePojo();
                                        stdrate_pojo.setTitile(profile_object.getString("title"));
                                        stdrate_pojo.setValue(profile_object.getString("value"));
                                        stdrate_pojo.setEditable("0");
                                        listBasicInfo.add(stdrate_pojo);
                                    }
                                }
                            }

                            Object check_driver_info_object = response_object.get("driver_info");
                            if (check_driver_info_object instanceof JSONArray) {

                                JSONArray driver_info_array = response_object.getJSONArray("driver_info");
                                if (driver_info_array.length() > 0) {
                                    listDriverInfo.clear();
                                    for (int j = 0; j < driver_info_array.length(); j++) {
                                        JSONObject driver_info_object = driver_info_array.getJSONObject(j);
                                        ProfilePojo rate_pojo = new ProfilePojo();
                                        rate_pojo.setTitile(driver_info_object.getString("title"));
                                        rate_pojo.setValue(driver_info_object.getString("value"));
                                        rate_pojo.setEditable("0");
                                        listDriverInfo.add(rate_pojo);
                                    }
                                }
                            }
                            str_paypal_id = payment_object.getString("paypal_id");
                            if (payment_object.has("stripe_id")) {
                                str_stripe_id = payment_object.getString("stripe_id");
                            }
                            Object check_driver_bank_info_object = response_object.get("bank_info");
                            if (check_driver_bank_info_object instanceof JSONObject) {


                                JSONObject bank_info = response_object.getJSONObject("bank_info");

                                acc_holder_name = bank_info.getString("acc_holder_name");
                                acc_holder_address = bank_info.getString("acc_holder_address");
                                acc_number = bank_info.getString("acc_number");
                                str_bank_name = bank_info.getString("bank_name");
                                branch_name = bank_info.getString("branch_name");
                                str_branch_address = bank_info.getString("branch_address");
                                str_swift_code = bank_info.getString("swift_code");
                                routing_number = bank_info.getString("routing_number");
                            }


                        }
                    }

                    if (Sstatus.equalsIgnoreCase("1")) {

                        Edt_paypalId.setText(str_paypal_id);
                        Edt_stripeId.setText(str_stripe_id);


                        Edt_bankName.setText(str_bank_name);
//                        address.setText(acc_holder_address);
                        Edt_city.setText("");
                        Edt_country.setText("");
                        Edt_accountName.setText(acc_holder_name);
                        Edt_accountNo.setText(acc_number);
                        Edt_swiftCode.setText(str_swift_code);
                        //    branch_address.setText(str_branch_address);
                        Edt_routingNo.setText(routing_number);
                        Edt_ibanNo.setText("");
                        //    bank_branch_name.setText(branch_name);

                        if (listBasicInfo.size() > 0) {
                            basicInfoAdapter = new ProfileAdapter(DriverAccount.this, listBasicInfo);
                            Lv_basicInfo.setAdapter(basicInfoAdapter);
                        }

                        if (listDriverInfo.size() > 0) {
                            driverInfoAdapter = new ProfileAdapter(DriverAccount.this, listDriverInfo);
                            Lv_driverInfo.setAdapter(driverInfoAdapter);
                        }

                        // Just For Testing The Functionality Hardcoding Country Code as Indonesia(ID)
//                         sCountryCode = "ID";

                        if (sCountryCode.equalsIgnoreCase("ID")) {
                            LL_bankName.setVisibility(View.VISIBLE);
                            LL_address.setVisibility(View.GONE);
                            LL_city.setVisibility(View.GONE);
                            LL_country.setVisibility(View.GONE);
                            LL_accountNo.setVisibility(View.VISIBLE);
                            LL_accountName.setVisibility(View.VISIBLE);
                            LL_swiftCode.setVisibility(View.GONE);
                            LL_routingNo.setVisibility(View.GONE);
                            LL_ibanNo.setVisibility(View.GONE);
                        } else {
                            LL_bankName.setVisibility(View.VISIBLE);
                            LL_address.setVisibility(View.VISIBLE);
                            LL_city.setVisibility(View.VISIBLE);
                            LL_country.setVisibility(View.VISIBLE);
                            LL_accountNo.setVisibility(View.VISIBLE);
                            LL_accountName.setVisibility(View.VISIBLE);
                            LL_swiftCode.setVisibility(View.VISIBLE);
                            LL_routingNo.setVisibility(View.VISIBLE);
                            LL_ibanNo.setVisibility(View.VISIBLE);
                        }

                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });


    }


    private void postRequestPaypal_Saveurl(String Url, String email) {
        dialog = new Dialog(DriverAccount.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_processing));


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriveID);
        jsonParams.put("email_id", email);


        System.out.println("-------------Paypal_Save Url----------------" + Url);
        System.out.println("-------------Paypal_Save jsonParams----------------" + jsonParams);

        mRequest = new ServiceRequest(DriverAccount.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------DriverAccount Response----------------" + response);

                String Sstatus = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        /*paypal_id.setEnabled(false);
                        payment_save.setVisibility(View.GONE);
                        payment_edit.setVisibility(View.VISIBLE);*/
                        Edt_paypalId.setEnabled(false);
                        Edt_stripeId.setEnabled(false);
                        Edt_authorizeId.setEnabled(false);
                        Btn_editPaymentGateway.setText(getResources().getString(R.string.label_edit));
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    private void postRequestaccount_detail_Saveurl(String
                                                           Url, HashMap<String, String> jsonParams) {
        dialog = new Dialog(DriverAccount.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_processing));


        System.out.println("-------------account_detail_Save Url----------------" + Url);
        System.out.println("-------------account_detail jsonParams----------------" + jsonParams);

        mRequest = new ServiceRequest(DriverAccount.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------account_detail Response----------------" + response);

                String Sstatus = "";
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {

                        Edt_bankName.setEnabled(false);
                        Edt_address.setEnabled(false);
                        Edt_city.setEnabled(false);
                        Edt_country.setEnabled(false);
                        Edt_accountNo.setEnabled(false);
                        Edt_accountName.setEnabled(false);
                        Edt_swiftCode.setEnabled(false);
                        Edt_routingNo.setEnabled(false);
                        Edt_ibanNo.setEnabled(false);
                        Btn_editBankInfo.setText(getResources().getString(R.string.label_edit));

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    //-------------------------code to Check Email Validation-----------------------
    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    //--------------------Code to set error for EditText-----------------------
    private void errorEdit(EditText editName, String msg) {
        Animation shake = AnimationUtils.loadAnimation(DriverAccount.this, R.anim.shake);
        editName.startAnimation(shake);

        ForegroundColorSpan fgcSpan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssBuilder = new SpannableStringBuilder(msg);
        ssBuilder.setSpan(fgcSpan, 0, msg.length(), 0);
        editName.setError(ssBuilder);
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(DriverAccount.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();

    }

    private void showPasscodeDialog(final int value) {

        if (sPasscode.length() > 0) {
            final Dialog passcodeDialog = new Dialog(DriverAccount.this, R.style.SlideUpDialog);
            passcodeDialog.getWindow();
            passcodeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            passcodeDialog.setContentView(R.layout.alert_enter_passcode);
            passcodeDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            passcodeDialog.setCanceledOnTouchOutside(true);
            passcodeDialog.show();
            passcodeDialog.getWindow().setGravity(Gravity.CENTER);

            final EditText pop_passcode = (EditText) passcodeDialog.findViewById(R.id.edt_passcode);
            final Button Btn_ok = (Button) passcodeDialog.findViewById(R.id.btn_ok);
            final ImageView Iv_close = (ImageView) passcodeDialog.findViewById(R.id.img_close);

            Btn_ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (pop_passcode.getText().toString().equals(sPasscode)) {

                        passcodeDialog.dismiss();

                        if (cd.isConnectingToInternet()) {
                            if (value == 1) {
                                Edt_paypalId.setEnabled(true);
                                Edt_stripeId.setEnabled(true);
                                Edt_authorizeId.setEnabled(true);
                                Btn_editPaymentGateway.setText(getResources().getString(R.string.label_save));
                            } else {
                                if (!sCountryCode.equalsIgnoreCase("ID")) {
                                    Edt_bankName.setEnabled(true);
                                    Edt_bankName.requestFocus();
                                    Edt_address.setEnabled(true);
                                    Edt_city.setEnabled(true);
                                    Edt_country.setEnabled(true);
                                    Edt_accountNo.setEnabled(true);
                                    Edt_accountName.setEnabled(true);
                                    Edt_swiftCode.setEnabled(true);
                                    Edt_routingNo.setEnabled(true);
                                    Edt_ibanNo.setEnabled(true);
                                    Btn_editBankInfo.setText(getResources().getString(R.string.label_save));
                                } else {

                                    PostRquestXenditBankList(Iconstant.getPaymentOPtion_url_new);

                                }


                            }
                        } else {
                            Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
                        }

                    } else {
                        Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.dialog_account_label_wrong_passcode));
                    }
                }

            });

            Iv_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    passcodeDialog.dismiss();
                }
            });

        }
    }


    private void changeBankDetails() {


        final Dialog dialog = new Dialog(DriverAccount.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.new_cloud_transfer_withdraw_changebank_popup);
        ImageView close_imag = (ImageView) dialog.findViewById(R.id.close_imag);
        final EditText name = (EditText) dialog.findViewById(R.id.fullname);
        final EditText accNum = (EditText) dialog.findViewById(R.id.accountnum);
        GridView grid_view = (GridView) dialog.findViewById(R.id.grid_view);
        close_imag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        grid_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (name.getText().toString().trim().length() == 0) {
                    erroredit(name, getResources().getString(R.string.valid_card_holder_name));
                } else if (accNum.getText().toString().trim().length() == 0) {
                    erroredit(accNum, getResources().getString(R.string.valid_card_no_enter));
                } else {

                    dialog.dismiss();

                    String bankCode = paymentcardlist.get(position).getXenditBankCode();
                    String bankName = paymentcardlist.get(position).getXenditBankName();
                    String bankImageUrl = paymentcardlist.get(position).getXendiActiveImage();
                    String accHolderName = name.getText().toString().trim();
                    String holderAccountNumber = accNum.getText().toString().trim();


                    if (cd.isConnectingToInternet()) {

                        HashMap<String, String> jsonParams = new HashMap<String, String>();
                        jsonParams.put("driver_id", sDriveID);
                        jsonParams.put("acc_holder_name", accHolderName);
                        jsonParams.put("acc_number", holderAccountNumber);
                        jsonParams.put("bank_name", bankName);
                        jsonParams.put("bank_code", bankCode);
                        PostRquestXenditBanDetailSave(Iconstant.getXendit_bank_save_url, jsonParams);
                    } else {

                        Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
                    }

                }

            }
        });
        if (isPaymentListAvailable) {

            Eadaptter = new XenditBankDetailsAdapter(DriverAccount.this, paymentcardlist);
            grid_view.setAdapter(Eadaptter);

        }
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void PostRquestXenditBanDetailSave(String Url, HashMap<String, String> jsonParams) {
        dialog = new Dialog(DriverAccount.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        System.out.println("-----------PostRquestXenditBanDetailSave jsonParams--------------" + jsonParams);

        mRequest = new ServiceRequest(DriverAccount.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                dialogDismiss();

                System.out.println("--------------PostRquestXenditBanDetailSave reponse-------------------" + response);

                String Sstatus = "", Smessage = "", acc_holder_name = "", acc_number = "", bank_name = "", bank_code = "";
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {

                        JSONObject jsonObject = object.getJSONObject("response");
                        if (jsonObject.has("banking")) {

                            JSONObject banking = jsonObject.getJSONObject("banking");
                            if (banking.length() > 0) {
                                acc_holder_name = banking.getString("acc_holder_name");
                                acc_number = banking.getString("acc_number");
                                bank_name = banking.getString("bank_name");
                                bank_code = banking.getString("bank_code");
                            }

                        }


                    } else {
                        Smessage = object.getString("response");
                        dialogDismiss();
                    }

                    if (Sstatus.equalsIgnoreCase("1")) {

                        Edt_accountNo.setText(acc_number);
                        Edt_bankName.setText(bank_name);
                        Edt_accountName.setText(acc_holder_name);

                        Btn_editBankInfo.setText(getResources().getString(R.string.label_edit));

                    } else {
                        Alert(getResources().getString(R.string.action_error), Smessage);
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    dialogDismiss();
                }


            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });


    }

    private void PostRquestXenditBankList(String Url) {

        dialog = new Dialog(DriverAccount.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        System.out.println("-----------Xendit bank url--------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriveID);


        System.out.println("------------Xendit bank jsonParams--------------" + jsonParams);


        mRequest = new ServiceRequest(DriverAccount.this);

        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {


            @Override
            public void onCompleteListener(String response) {


                System.out.println("------------Xendit bank response--------------" + response);

                String Ssatus = "";


                try {

                    JSONObject jsondata = new JSONObject(response);
                    Ssatus = jsondata.getString("status");

                    if (Ssatus.equalsIgnoreCase("1")) {


                        // JSONObject jsondata = object.getJSONObject("response");

                        Object check_object = jsondata.get("bank_list");

                        if (check_object instanceof JSONArray) {


                            JSONArray payment_list_jsonArray = jsondata.getJSONArray("bank_list");


                            if (payment_list_jsonArray.length() > 0) {

                                paymentcardlist = new ArrayList<>();

                                for (int i = 0; i < payment_list_jsonArray.length(); i++) {

                                    JSONObject payment_obj = payment_list_jsonArray.getJSONObject(i);

                                    WalletMoneyPojo wmpojo = new WalletMoneyPojo();

                                    wmpojo.setXenditBankName(payment_obj.getString("name"));
                                    wmpojo.setXenditBankCode(payment_obj.getString("code"));
                                    //   wmpojo.setPayment_normal_img(payment_obj.getString("inactive_icon"));
                                    wmpojo.setXendiActiveImage(payment_obj.getString("image"));
                                    //  wmpojo.setPayment_selected_payment_id("false");

                                    paymentcardlist.add(wmpojo);

                                }


                                isPaymentListAvailable = true;

                            } else {

                                isPaymentListAvailable = false;

                            }


                        }


                        isDatavailable = true;


                    }


                    if (dialog != null) {
                        dialog.dismiss();
                    }


                    if (Ssatus.equalsIgnoreCase("1") && isDatavailable) {

                        changeBankDetails();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }


            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }

            }
        });


    }

    private void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    private void AlertError(String title, String message) {
        String msg = title + "\n" + message;
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        AppMsg.Style style = new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red_color);
        AppMsg snack = AppMsg.makeText(DriverAccount.this, msg.toUpperCase(), AppMsg.STYLE_ALERT);
        snack.setView(view);
        snack.setLayoutGravity(Gravity.TOP);
        snack.setPriority(AppMsg.PRIORITY_HIGH);
        snack.setAnimation(R.anim.enter, R.anim.exit);
        snack.show();

    }

    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(DriverAccount.this, R.anim.shake);
        editname.startAnimation(shake);
        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

}