package com.blackmaria.partner.app;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.android.volley.Request;

import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.Pojo.WalletMoneyPojo;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.ImageLoader;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.adapter.XenditBankDetailsAdapter;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.devspark.appmsg.AppMsg;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user144 on 9/18/2017.
 */

public class NewCloudMoneyWithDrawBankPage extends ActivityHockeyApp implements View.OnClickListener {

    private RelativeLayout Rl1;
    private ImageView imgBack;
    private ImageView imgBwallet;
    private TextView txtLabelCurrentBalance;
    private RelativeLayout Rl2;
    private ImageView indicatingImage;
    private RelativeLayout verifylayout;
    private TextView accountNumber, nameHolder;
    private TextView transferAmount;
    private TextView transferFee;
    private TextView totalTransaction, transferfee;
    private ImageView bankImage;
    private TextView chnageRecvBank;
    private TextView help_tv;
    private TextView confirmNowTv;
    private ImageLoader imageLoader;
    private SessionManager session;
    private boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    Dialog dialog;
    private String flag = "", accHolderName1 = "", bankCode = "", holderAccountNumber1 = "";

    private String sSecurePin = "", transferAmount1 = "", paymentMode = "", CurrencySymbol = "";
    private String sDriverID = "", Savil_amount = "", Scurrency = "", Slast_withdrawwl = "", Swithdrawel_status = "", Swithdrawel_status_txt = "";
    ArrayList<WalletMoneyPojo> paymentcardlist;
    private Boolean isDatavailable = false;
    private Boolean isPaymentListAvailable = false;
    XenditBankDetailsAdapter Eadaptter;
    private int Listposition = 0;
    private Double totalAmount = 0.0;
    String bankImageUrl = "";
    String accHolderName = "", holderAccountNumber = "";

    BroadcastReceiver refreshReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_cloud_transfer_withdraw_bank_page);
        initialize();
    }

    private void initialize() {


        session = new SessionManager(NewCloudMoneyWithDrawBankPage.this);
        cd = new ConnectionDetector(NewCloudMoneyWithDrawBankPage.this);
        isInternetPresent = cd.isConnectingToInternet();
        imageLoader = new ImageLoader(NewCloudMoneyWithDrawBankPage.this);
        paymentcardlist = new ArrayList<WalletMoneyPojo>();
        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        sSecurePin = session.getSecurePin();

        registerBroadcast();

        Rl1 = (RelativeLayout) findViewById(R.id.Rl_1);
        imgBack = (ImageView) findViewById(R.id.img_back);
        imgBwallet = (ImageView) findViewById(R.id.img_bwallet);
        txtLabelCurrentBalance = (TextView) findViewById(R.id.txt_label_current_balance);
        Rl2 = (RelativeLayout) findViewById(R.id.Rl_2);

        indicatingImage = (ImageView) findViewById(R.id.indicating_image);
        /*GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(indicatingImage);
        Glide.with(this).load(R.drawable.blinkgif).into(imageViewTarget);*/


        verifylayout = (RelativeLayout) findViewById(R.id.verifylayout);
        accountNumber = (TextView) findViewById(R.id.account_number);
        nameHolder = (TextView) findViewById(R.id.nameHolder);
        transferAmount = (TextView) findViewById(R.id.transfer_amount);
        transferFee = (TextView) findViewById(R.id.transfer_fee);
        totalTransaction = (TextView) findViewById(R.id.total_transaction);
        bankImage = (ImageView) findViewById(R.id.bank_image);
        chnageRecvBank = (TextView) findViewById(R.id.chnage_recv_bank);
        confirmNowTv = (TextView) findViewById(R.id.confirm_now_tv);
        help_tv = (TextView) findViewById(R.id.help_tv);
        imgBack.setOnClickListener(this);
        chnageRecvBank.setOnClickListener(this);
        verifylayout.setOnClickListener(this);
        confirmNowTv.setOnClickListener(this);
        CurrencySymbol = session.getCurrency();
        help_tv.setOnClickListener(this);

        if (getIntent().hasExtra("flag")) {
            flag = getIntent().getStringExtra("flag");
            if (flag.equalsIgnoreCase("2")) {
                accHolderName1 = getIntent().getStringExtra("accHolderName");
                holderAccountNumber1 = getIntent().getStringExtra("accHolderNumber");
                bankCode = getIntent().getStringExtra("bankCode");

                nameHolder.setText(accHolderName1);
                accountNumber.setText(holderAccountNumber1);
            }
        }



        transferAmount1 = getIntent().getStringExtra("transfer_amount");
        paymentMode = getIntent().getStringExtra("paymentMode");


        transferAmount.setText(CurrencySymbol + " " + transferAmount1);
        transferFee.setText(CurrencySymbol + " " + "0");
        totalAmount = Double.parseDouble(transferAmount1) + Double.parseDouble("0");
        totalTransaction.setText(CurrencySymbol + " " + totalAmount);
        if (isInternetPresent) {
            HashMap<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("driver_id", sDriverID);
            PostRquestXenditBanDetailGet(Iconstant.getXendit_bank_saved_detail, jsonParams);
        } else {

            Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
        }


    }


    private void registerBroadcast() {

        IntentFilter filter = new IntentFilter();
        filter.addAction("com.package.ACTION_WALLET_WITHDRAWAL_SUCCESS");
        refreshReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent != null) {

                    // Finish Page
                    finish();

                }

            }
        };
        registerReceiver(refreshReceiver, filter);

    }

    @Override
    public void onClick(View v) {
        if (v == imgBack) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (v == chnageRecvBank) {
            changeBankDetails();
        } else if (v == confirmNowTv) {

            Intent in = new Intent(NewCloudMoneyWithDrawBankPage.this, CloudTransferAmountPinEnter1.class);
            in.putExtra("amount", totalAmount + "");
            in.putExtra("mode", paymentMode);
            in.putExtra("accHolderName1", accHolderName1);
            in.putExtra("holderAccountNumber1", holderAccountNumber1);
            in.putExtra("bankCode", bankCode);

            startActivity(in);
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);


            // confirmPin();
        } else if (v == verifylayout) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (v == help_tv) {
            infoPage();
        }
    }

    private void infoPage() {
        final Dialog dialog = new Dialog(NewCloudMoneyWithDrawBankPage.this, R.style.DialogSlideAnim);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.cloud_help_dialog_bank);
        // Button sendNow = (Button) dialog.findViewById(R.id.custom_dialog_library_ok_button);
        RelativeLayout close_imag = (RelativeLayout) dialog.findViewById(R.id.dig_cancel);

        close_imag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void confirmPin() {
        final Dialog confirmPinDialog = new Dialog(NewCloudMoneyWithDrawBankPage.this, R.style.SlideUpDialog);
        confirmPinDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmPinDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmPinDialog.setCancelable(true);
        confirmPinDialog.setContentView(R.layout.alert_enter_pin);

        final PinEntryEditText Ed_pin = (PinEntryEditText) confirmPinDialog.findViewById(R.id.edt_withdrawal_amount);
        final TextView Tv_forgotPin = (TextView) confirmPinDialog.findViewById(R.id.txt_label_forgot_pin);
        final RelativeLayout RL_confirm = (RelativeLayout) confirmPinDialog.findViewById(R.id.Rl_confirm);
        final ImageView Iv_close = (ImageView) confirmPinDialog.findViewById(R.id.img_close);

        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmPinDialog.dismiss();
            }
        });

        Tv_forgotPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (cd.isConnectingToInternet()) {
                    ForgotPinRequest(Iconstant.forgot_pin_url);
                } else {
                    Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                }

            }
        });

        RL_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    CloseKeyboard(Ed_pin);
                } catch (Exception e) {

                }

                if (confirmPinDialog != null && confirmPinDialog.isShowing()) {
                    confirmPinDialog.dismiss();
                }

                if (sSecurePin.equalsIgnoreCase(Ed_pin.getText().toString())) {

                    if (isInternetPresent) {
                        PostRquestForConfirm(Iconstant.wallet_withdraw_amount_url);

                    } else {

                        Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));

                    }
                    if (confirmPinDialog != null && confirmPinDialog.isShowing()) {
                        confirmPinDialog.dismiss();
                    }


                } else {
                    AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.label_incorrect_pin));
                }

            }
        });

        confirmPinDialog.show();

    }

    private void PostRquestForConfirm(String Url) {

        dialog = new Dialog(NewCloudMoneyWithDrawBankPage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        System.out.println("-----------E Wallet confirm url--------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        jsonParams.put("amount", transferAmount1);
        jsonParams.put("mode", paymentMode);

        System.out.println("------------E Wallet confirm jsonParams--------------" + jsonParams);


        mRequest = new ServiceRequest(NewCloudMoneyWithDrawBankPage.this);

        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {

                System.out.println("------------E Wallet confirm response--------------" + response);

                String Ssatus = "";

                try {

                    JSONObject object = new JSONObject(response);
                    Ssatus = object.getString("status");

                    dialogDismiss();

                    if (Ssatus.equalsIgnoreCase("1")) {

                        String Sresponse = object.getString("response");
                        Alertsuccess(getResources().getString(R.string.action_success), Sresponse);

                    } else {
                        String Sresponse = object.getString("response");
                        Alert("SORRY! EXCESS LIMIT", "Please lower your amount");
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    dialogDismiss();
                }


            }

            @Override
            public void onErrorListener() {
                dialogDismiss();

            }
        });


    }

    private void changeBankDetails() {


        final Dialog dialog = new Dialog(NewCloudMoneyWithDrawBankPage.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.new_cloud_transfer_withdraw_changebank_popup);
        ImageView close_imag = (ImageView) dialog.findViewById(R.id.close_imag);
        final EditText name = (EditText) dialog.findViewById(R.id.fullname);
        final EditText accNum = (EditText) dialog.findViewById(R.id.accountnum);
        GridView grid_view = (GridView) dialog.findViewById(R.id.grid_view);
        close_imag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        grid_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (name.getText().toString().trim().length() <= 0) {
                    erroredit(name, getResources().getString(R.string.valid_card_holder_name));
                } else if (accNum.getText().toString().trim().length() <= 0) {
                    erroredit(accNum, getResources().getString(R.string.valid_card_no_enter));
                } else {
                    String bankCode = paymentcardlist.get(position).getXenditBankCode();
                    String bankName = paymentcardlist.get(position).getXenditBankName();
                    bankImageUrl = paymentcardlist.get(position).getXendiActiveImage();
                    accHolderName = name.getText().toString().trim();
                    holderAccountNumber = accNum.getText().toString().trim();


                    if (isInternetPresent) {

                        HashMap<String, String> jsonParams = new HashMap<String, String>();
                        jsonParams.put("driver_id", sDriverID);
                        jsonParams.put("acc_holder_name", accHolderName);
                        jsonParams.put("acc_number", holderAccountNumber);
                        jsonParams.put("bank_name", bankName);
                        jsonParams.put("bank_code", bankCode);
                        PostRquestXenditBanDetailSave(Iconstant.getXendit_bank_save_url, jsonParams);
                        dialog.dismiss();
                    } else {

                        Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
                    }

                }



            }
        });
        if (isPaymentListAvailable) {

            Eadaptter = new XenditBankDetailsAdapter(NewCloudMoneyWithDrawBankPage.this, paymentcardlist);
            grid_view.setAdapter(Eadaptter);

        }
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    private void PostRquestXenditBanDetailSave(String Url, HashMap<String, String> jsonParams) {
        dialog = new Dialog(NewCloudMoneyWithDrawBankPage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        System.out.println("-----------PostRquestXenditBanDetailSave jsonParams--------------" + jsonParams);

        mRequest = new ServiceRequest(NewCloudMoneyWithDrawBankPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------PostRquestXenditBanDetailSave reponse-------------------" + response);

                String Sstatus = "", Smessage = "", acc_holder_name = "", acc_number = "", bank_name = "", bank_code = "";
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {

                        JSONObject jsonObject = object.getJSONObject("response");
                        if (jsonObject.has("banking")) {

                            JSONObject banking = jsonObject.getJSONObject("banking");
                            if (banking.length() > 0) {
                                acc_holder_name = banking.getString("acc_holder_name");
                                acc_number = banking.getString("acc_number");
                                bank_name = banking.getString("bank_name");
                                bank_code = banking.getString("bank_code");
                            }

                        }

                        dialogDismiss();

                    } else {
                        Smessage = object.getString("response");
                        dialogDismiss();
                    }
                    if (Sstatus.equalsIgnoreCase("1")) {
                        accountNumber.setText(acc_number);
                        nameHolder.setText(acc_holder_name.toUpperCase());
                        Picasso.with(NewCloudMoneyWithDrawBankPage.this).load(String.valueOf(bankImageUrl)).into(bankImage);
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    dialogDismiss();
                }
                dialogDismiss();

                if (Sstatus.equalsIgnoreCase("1")) {

                } else {
                    Alert(getResources().getString(R.string.action_error), Smessage);
                }
            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });


    }

    private void PostRquestXenditBanDetailGet(String Url, HashMap<String, String> jsonParams) {
        dialog = new Dialog(NewCloudMoneyWithDrawBankPage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        System.out.println("-----------PostRquestXenditBanDetailSave jsonParams--------------" + jsonParams);

        mRequest = new ServiceRequest(NewCloudMoneyWithDrawBankPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------PostRquestXenditBanDetailSave reponse-------------------" + response);

                String Sstatus = "", Smessage = "", acc_holder_name = "", acc_number = "", bank_name = "", bank_code = "", BankImage = "";
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {

                        JSONObject jsonObject = object.getJSONObject("response");
                        BankImage = jsonObject.getString("bank_image");
                        if (jsonObject.has("banking")) {

                            JSONObject banking = jsonObject.getJSONObject("banking");
                            if (banking.length() > 0) {
                                acc_holder_name = banking.getString("acc_holder_name");
                                acc_number = banking.getString("acc_number");
                                bank_name = banking.getString("bank_name");
//                                bank_code = banking.getString("bank_code");
                            }

                        }

                        dialogDismiss();

                    } else {
                        Smessage = object.getString("response");
                        dialogDismiss();
                    }
                    try {
                        PostRquestXenditBankList(Iconstant.getPaymentOPtion_url_new);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (Sstatus.equalsIgnoreCase("1")) {
                        if (holderAccountNumber1.equalsIgnoreCase("")) {
                            accountNumber.setText(acc_number);
                            nameHolder.setText(acc_holder_name.toUpperCase());
                        }

                        if (!BankImage.equalsIgnoreCase("null") && !BankImage.equalsIgnoreCase("") && BankImage != null)
                            Picasso.with(NewCloudMoneyWithDrawBankPage.this).load(String.valueOf(BankImage)).into(bankImage);
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    dialogDismiss();
                }
                dialogDismiss();

                if (Sstatus.equalsIgnoreCase("1")) {

                } else {
                    Alert(getResources().getString(R.string.action_error), Smessage);
                }
            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });


    }

    private void AlertError(String title, String message) {
        String msg = title + "\n" + message;
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        AppMsg.Style style = new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red_color);
        AppMsg snack = AppMsg.makeText(NewCloudMoneyWithDrawBankPage.this, msg.toUpperCase(), AppMsg.STYLE_ALERT);
        snack.setView(view);
        snack.setLayoutGravity(Gravity.TOP);
        snack.setPriority(AppMsg.PRIORITY_HIGH);
        snack.setAnimation(R.anim.enter, R.anim.exit);
        snack.show();

    }

    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(NewCloudMoneyWithDrawBankPage.this, R.anim.shake);
        editname.startAnimation(shake);
        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }

    @SuppressLint("WrongConstant")
    private void ForgotPinRequest(String Url) {

        dialog = new Dialog(NewCloudMoneyWithDrawBankPage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);

        System.out.println("--------------ForgotPinRequest Url-------------------" + Url);
        System.out.println("--------------ForgotPinRequest jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(NewCloudMoneyWithDrawBankPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                if (dialog != null) {
                    dialog.dismiss();
                }

                System.out.println("--------------ForgotPinRequest Response-------------------" + response);
                String status = "", sResponse = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");
                        sResponse = object.getString("message");

                        if (status.equalsIgnoreCase("1")) {
                            Alert(getResources().getString(R.string.action_success), sResponse);
                        } else {
                            Alert(getResources().getString(R.string.action_error), sResponse);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(NewCloudMoneyWithDrawBankPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

            }
        });
        mDialog.show();
    }

    //--------------Alert Method-----------
    private void Alertsuccess(String title, String alert) {
        final PkDialog mDialog = new PkDialog(NewCloudMoneyWithDrawBankPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
//                finish();
//                overridePendingTransition(R.anim.enter, R.anim.exit);

                Intent intent = new Intent();
                intent.setAction("com.package.ACTION_WALLET_WITHDRAWAL_SUCCESS");
                sendBroadcast(intent);

            }
        });
        mDialog.show();
    }

    private void PostRquestXenditBankList(String Url) {

        final Dialog dialog = new Dialog(NewCloudMoneyWithDrawBankPage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        System.out.println("-----------Xendit bank url--------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);


        System.out.println("------------Xendit bank jsonParams--------------" + jsonParams);


        mRequest = new ServiceRequest(NewCloudMoneyWithDrawBankPage.this);

        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {


            @Override
            public void onCompleteListener(String response) {


                System.out.println("------------Xendit bank jsonParams--------------" + response);

                String Ssatus = "";


                try {

                    JSONObject jsondata = new JSONObject(response);
                    Ssatus = jsondata.getString("status");

                    if (Ssatus.equalsIgnoreCase("1")) {


                        // JSONObject jsondata = object.getJSONObject("response");

                        Object check_object = jsondata.get("bank_list");

                        if (check_object instanceof JSONArray) {


                            JSONArray payment_list_jsonArray = jsondata.getJSONArray("bank_list");


                            if (payment_list_jsonArray.length() > 0) {

                                paymentcardlist.clear();


                                for (int i = 0; i < payment_list_jsonArray.length(); i++) {

                                    JSONObject payment_obj = payment_list_jsonArray.getJSONObject(i);

                                    WalletMoneyPojo wmpojo = new WalletMoneyPojo();

                                    wmpojo.setXenditBankName(payment_obj.getString("name"));
                                    wmpojo.setXenditBankCode(payment_obj.getString("code"));
                                    //   wmpojo.setPayment_normal_img(payment_obj.getString("inactive_icon"));
                                    wmpojo.setXendiActiveImage(payment_obj.getString("image"));
                                    //  wmpojo.setPayment_selected_payment_id("false");

                                    paymentcardlist.add(wmpojo);

                                }


                                isPaymentListAvailable = true;

                            } else {

                                isPaymentListAvailable = false;

                            }


                        }


                        isDatavailable = true;


                    }


                    if (dialog != null) {
                        dialog.dismiss();
                    }


                    if (Ssatus.equalsIgnoreCase("1") && isDatavailable) {

                        // Tv_currency_code.setText(Scurrency);
                        //  Tv_last_trip.setText(Slast_withdrawwl);


                    } else {

                       /* String Sresponse = object.getString("response");
                        Alert(getResources().getString(R.string.alert_label_title), Sresponse);
*/
                    }


                } catch (JSONException e) {
                    e.printStackTrace();

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }


            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }

            }
        });


    }

    private void dialogDismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }


    //--------------Close KeyBoard Method-----------
    private void CloseKeyboard(EditText edittext) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(edittext.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private void startBlinkingAnimation() {
        Animation startAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blinking_animation);
        indicatingImage.startAnimation(startAnimation);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (refreshReceiver != null) {
            unregisterReceiver(refreshReceiver);
        }
        if (indicatingImage != null) {
            indicatingImage.clearAnimation();
        }
    }
}
