package com.blackmaria.partner.app;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.Pojo.WeekDetails;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.adapter.WeekTripDetailAdapter;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user88 on 4/27/2017.
 */

public class MyTripWeekDetail extends ActivityHockeyApp implements View.OnClickListener {

    private ImageView Iv_back;
    private TextView Tv_trips_count, Tv_date;
    private ListView listWeek;
    private ConnectionDetector cd;
    private SessionManager sessionManager;
    private String sDriveID = "", fromDate = "", toDate = "", sTotalRides = "", sDisplayDate = "";
    private Dialog dialog;
    private ServiceRequest mRequest;
    private ArrayList<WeekDetails> weekDetailsList;
    private WeekTripDetailAdapter adapter;
    private boolean isDataAvailable = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trip_week_detail);

        initialize();

    }

    @Override
    public void onClick(View view) {

        if (view == Iv_back) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }

    }

    private void initialize() {

        sessionManager = new SessionManager(MyTripWeekDetail.this);
        cd = new ConnectionDetector(MyTripWeekDetail.this);

        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriveID = user.get(SessionManager.KEY_DRIVERID);

        Iv_back = (ImageView) findViewById(R.id.img_back);
        Tv_trips_count = (TextView) findViewById(R.id.txt_trips_count);
        Tv_date = (TextView) findViewById(R.id.txt_date);
        listWeek = (ListView) findViewById(R.id.lv_today_details);

        getIntentData();

        Iv_back.setOnClickListener(this);

        if (cd.isConnectingToInternet()) {
            MyTripWeekDetailRequest(Iconstant.week_trips_details_url);
        } else {
            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }

        listWeek.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MyTripWeekDetail.this, MyTripTodayDetail.class);
                intent.putExtra("filterDate", weekDetailsList.get(position).getDate_f());
                intent.putExtra("todayFlag", "1");
//            intent.putExtra("displayDate", sDisplayDate);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });


    }

    private void getIntentData() {

        Intent intent = getIntent();
        if (intent != null) {
            fromDate = intent.getStringExtra("fromDate");
            toDate = intent.getStringExtra("toDate");
        }

    }

    private void MyTripWeekDetailRequest(String Url) {

        dialog = new Dialog(MyTripWeekDetail.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriveID);
        jsonParams.put("from_date", fromDate);
        jsonParams.put("to_date", toDate);

        System.out.println("--------------WeekTripsDetail Url-------------------" + Url);
        System.out.println("--------------WeekTripsDetail jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(MyTripWeekDetail.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------WeekTripsDetail Response-------------------" + response);
                String status = "";
                ArrayList<WeekDetails> dummyWeekDetailsList = new ArrayList<>();
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {
                            JSONObject jsonObject = object.getJSONObject("response");
                            if (jsonObject.length() > 0) {

                                Object weekObject = jsonObject.get("week");
                                if (weekObject instanceof JSONArray) {

                                    JSONArray jarWeek = jsonObject.getJSONArray("week");
                                    if (jarWeek.length() > 0) {

                                        weekDetailsList = new ArrayList<>();
                                        dummyWeekDetailsList = new ArrayList<>();
                                        for (int i = 0; i < jarWeek.length(); i++) {

                                            JSONObject jobjWeek = jarWeek.getJSONObject(i);

                                            if (jobjWeek.length() > 0) {

                                                WeekDetails weekDetail = new WeekDetails();

                                                weekDetail.setDay(jobjWeek.getString("day"));
                                                weekDetail.setRidecount(jobjWeek.getString("ridecount"));
                                                weekDetail.setDistance(jobjWeek.getString("distance"));
                                                weekDetail.setDriver_earning(jobjWeek.getString("driver_earning"));
                                                weekDetail.setCurrency(jobjWeek.getString("currency"));
                                                weekDetail.setOrder(jobjWeek.getString("order"));
                                                weekDetail.setDate_f(jobjWeek.getString("date_f"));

                                                weekDetailsList.add(weekDetail);
                                                dummyWeekDetailsList.add(weekDetail);

                                            }
                                        }
                                        isDataAvailable = true;
                                    } else {
                                        isDataAvailable = false;
                                    }
                                } else {
                                    isDataAvailable = false;
                                }

                                sTotalRides = jsonObject.getString("total_rides");
                                sDisplayDate = jsonObject.getString("date_desc");

                            }
                        } else {
                            String sResponse = object.getString("response");
                            Alert(getResources().getString(R.string.action_error), sResponse);
                        }

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (status.equalsIgnoreCase("1")) {

                    Tv_date.setText(sDisplayDate);

                    if (sTotalRides != null && sTotalRides.length() > 0) {
                        int rides = 0;
                        try {
                            if (sTotalRides.equalsIgnoreCase("-")) {
                                rides = 0;
                            } else {
                                rides = Integer.parseInt(sTotalRides);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (rides > 1) {
                            Tv_trips_count.setText(sTotalRides + " " + getResources().getString(R.string.mytrips_label_trips));
                        } else {
                            Tv_trips_count.setText(sTotalRides + " " + getResources().getString(R.string.mytrips_label_trip));
                        }
                    }

                    if (isDataAvailable) {

                        if (dummyWeekDetailsList.size() > 0) {

//                            for (int i = 0; i < dummyWeekDetailsList.size(); i++) {
//
//                                String sOrder = dummyWeekDetailsList.get(i).getOrder();
//                                if (sOrder != null && sOrder.length() > 0 && !sOrder.equalsIgnoreCase("-")) {
//                                    int index = Integer.parseInt(sOrder) - 1;
//                                    weekDetailsList.set(index, dummyWeekDetailsList.get(i));
//                                }
//
//                            }

                            adapter = new WeekTripDetailAdapter(MyTripWeekDetail.this, weekDetailsList);
                            listWeek.setAdapter(adapter);

                        }
                    }

                }

                if (dialog != null) {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(MyTripWeekDetail.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

}