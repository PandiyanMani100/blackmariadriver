package com.blackmaria.partner.app;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;

import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by Prem Kumar and Anitha on 3/13/2017.
 */

public class SosPage extends ActivityHockeyApp implements View.OnClickListener {

    private ImageView Iv_back;
    private ImageView Iv_callSos;

    private String sSelectedPanicNumber = "";
    final int PERMISSION_REQUEST_CODE = 111;
    SessionManager session;
    RelativeLayout rel_police,rel_sos_page_ambulance_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sos_page);
        session = new SessionManager(SosPage.this);
        initialize();
    }

    private void initialize() {
        Iv_back = (ImageView) findViewById(R.id.img_back);
        Iv_callSos = (ImageView) findViewById(R.id.sos_page_sosCall_imageView);
        rel_police = (RelativeLayout) findViewById(R.id.sos_page_police_layout);
        rel_sos_page_ambulance_layout = (RelativeLayout) findViewById(R.id.sos_page_ambulance_layout);

        Iv_back.setOnClickListener(this);
        Iv_callSos.setOnClickListener(this);
        rel_police.setOnClickListener(this);
        rel_sos_page_ambulance_layout.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == Iv_back) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (view == Iv_callSos) {
            panic();
        }else if (view == rel_police) {
            if (Build.VERSION.SDK_INT >= 23) {
                // Marshmallow+

                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + session.getPolice()));
                startActivity(intent);

            } else {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + session.getPolice()));
                startActivity(intent);
            }
        }else if (view == rel_sos_page_ambulance_layout) {
            if (Build.VERSION.SDK_INT >= 23) {
                // Marshmallow+
                if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                    requestPermission();
                } else {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + session.getAmbulance()));
                    startActivity(callIntent);
                }
            } else {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + session.getAmbulance()));
                startActivity(callIntent);
            }
        }
    }


    private void panic() {
        System.out.println("----------------panic method-----------------");
        View view = View.inflate(SosPage.this, R.layout.sos_panic_dialog, null);
        final MaterialDialog dialog = new MaterialDialog(SosPage.this);
        dialog.setContentView(view).setNegativeButton(getResources().getString(R.string.cancel_lable), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                }
        ).show();
        Button call_police = (Button) view.findViewById(R.id.call_police_button);
        Button call_fire = (Button) view.findViewById(R.id.call_fireservice_button);
        Button call_ambulance = (Button) view.findViewById(R.id.call_ambulance_button);
        call_police.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                dialog.dismiss();


                if (Build.VERSION.SDK_INT >= 23) {
                    // Marshmallow+
                    if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                        requestPermission();
                    } else {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + session.getPolice()));
                        startActivity(callIntent);
                    }
                } else {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + session.getPolice()));
                    startActivity(callIntent);
                }
            }
        });

        call_fire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                dialog.dismiss();

                if (Build.VERSION.SDK_INT >= 23) {
                    // Marshmallow+
                    if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                        requestPermission();
                    } else {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + session.getFire()));
                        startActivity(callIntent);
                    }
                } else {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + session.getFire()));
                    startActivity(callIntent);
                }
            }
        });
        call_ambulance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                dialog.dismiss();

                if (Build.VERSION.SDK_INT >= 23) {
                    // Marshmallow+
                    if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                        requestPermission();
                    } else {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + session.getAmbulance()));
                        startActivity(callIntent);
                    }
                } else {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + session.getAmbulance()));
                    startActivity(callIntent);
                }

            }
        });

    }


    private boolean checkCallPhonePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkReadStatePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + sSelectedPanicNumber));
                    startActivity(callIntent);
                }
                break;

        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
            return true;
        }
        return false;
    }
}
