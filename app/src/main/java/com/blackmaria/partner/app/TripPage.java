package com.blackmaria.partner.app;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.CycleInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.latestpackageview.Model.CancelReasonPojo;
import com.blackmaria.partner.Pojo.MultipleLocationPojo;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.CurrencySymbolConverter;
import com.blackmaria.partner.Utils.GMapV2GetRouteDirection;
import com.blackmaria.partner.Utils.GPSTracker;
import com.blackmaria.partner.latestpackageview.widgets.RoundedImageView;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.adapter.CancelAlertAdapter;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.mylibrary.latlnginterpolation.LatLngInterpolator;
import com.blackmaria.partner.mylibrary.latlnginterpolation.MarkerAnimation;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.CustomTextView;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.blackmaria.partner.latestpackageview.Database.GEODBHelper;
import com.blackmaria.partner.services.GoogleNavigationService;
import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class TripPage extends ActivityHockeyApp implements RoutingListener, View.OnClickListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {

    private ConnectionDetector cd;
    private boolean isInternetPresent = false;

    private GoogleMap googleMap;
    private GPSTracker gps;
    private String sDriverLat = "", sDriverLng = "";
    private ArrayList<CancelReasonPojo> Cancelreason_arraylist;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private PendingResult<LocationSettingsResult> result;
    private final static int REQUEST_LOCATION = 199;
    public static Location myLocation;
    private double MyCurrent_lat = 0.0, MyCurrent_long = 0.0;

    //    private TextView tv_Trip_Button, tv_cancel, tv_navigate, tv_ride_id, tv_drop, tv_eta;
    private Marker currentDriverMarker;
    LatLng pickup_latlong = null, drop_latlong = null, cur_latlong = null, return_latLong = null, return_pickUp_latLong = null;
    GMapV2GetRouteDirection v2GetRouteDirection;

    private ServiceRequest mRequest;
    Dialog dialog;
    private SessionManager sessionManager;

    //    RelativeLayout arrived_layout, Trip_layout;
    TextView Tv_title, subtitle, Tv_name, tv_usergender, tv_member_since, arrive_subtitle;
    RoundedImageView userimg;

    private String driver_id = "", driver_name = "";
    private String Str_RideId = "", sPickUpAddress = "", Str_pickUp_Lat = "", Str_pickUp_Long = "", Str_username = "",
            Str_user_rating = "", Str_user_phoneno = "", Str_user_img = "", Str_droplat = "", Str_droplon = "",
            str_drop_location = "", Suser_Id = "", title_text = "", subtitle_text = "", eta_arrival = "",
            location = "", member_since = "", user_gender = "", est_cost = "", sCurrencySymbol = "", Trip_Status = "arrived",
            sContactNumber = "";

    private ArrayList<LatLng> wayPointList;
    private LatLngBounds.Builder wayPointBuilder;
    private List<Polyline> polyLines;
    private LatLng startWayPoint, endWayPoint;
    private ArrayList<MultipleLocationPojo> multipleDropList;
    private boolean isMultipleDropAvailable = false;
    private LatLng endTrip_startWayPoint, endTrip_endWayPoint;

    //Moving Marker
    private LatLng newLatLng, oldLatLng;
    private LatLngInterpolator mLatLngInterpolator;
    private Location oldLocation;
    private double myMovingDistance = 0.0;

    private BroadcastReceiver finishReceiver;

    private final static int OVERLAY_PERMISSION_REQ_CODE = 1234;
    final int PERMISSION_REQUEST_NAVIGATION_CODE = 222;
    final int PERMISSION_REQUEST_CODE = 111, PERMISSION_REQUEST_CODE_SOS = 333;

    private GEODBHelper myDBHelper;

    private float totalDistanceTravelled;
    private boolean show_progress_status = false;
    private Dialog cancel_dialog;

    CancelAlertAdapter CancelAlertAdapter;
    private String reason_id;
    private ListView cancel_list;
    private CustomTextView Tv_close;
    private ImageView Iv_tripWayIcon;

    private RelativeLayout Rl_multipleDrop, Rl_returnTrip;
    private TextView Tv_multipleDropState, Tv_returnType;
    private int multipleDropCount = 0;
    private String sRecordId = "";
    private String sEndNavigationLat = "", sEndNavigationLng = "", sMultipleDropStatus = "";
    private LatLng endTrip_NavigationLatLng;

    private String sTripMode = "", isReturnAvailable = "";
    private String sReturnState = "";
    private String sReturnLat = "", sReturnLng = "", sReturnPickUpLat = "", sReturnPickUpLng = "",
            sPaymentType = "", sCancelledRides = "", sCompletedRides = "", sRiderType = "";
    private boolean isReturnLatLngAvailable = false;

    private String sCurrentLocationName = "";

    private LatLng pickup_multiStop_latlong = null;

    private TextView Txt_pickupAddress_or_destinationAddress, Txt_bookingNo_or_driverName_or_tripStatus, Txt_paymentMethod_or_estimatedFare_or_oneWay_or_multipleStop;
    private ImageView Img_time, Img_distance, Img_cancel_or_trip_info, Img_call_or_sos;
    private TextView Txt_time, Txt_distance, Txt_cancel_or_trip_info, Txt_call_or_sos;
    private Button btn_navigate, btn_ride;
    private RelativeLayout RL_rider_profile;
    private TextView Txt_rider_name;
    private ImageView Img_rider_profile_pic;
    private ImageButton Ib_currentLocation;
    private String Str_DistanceTo = "", Str_TimeTo = "";
    private Handler travelTimeHandler;
    private Runnable travelTimeRunnable;
    private RelativeLayout RL_profile;

    MapFragment mapFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.blackmaria_trip2);

        // Receiving the data from broadcast
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.app.finish.TripPage");
        finishReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                finish();
            }
        };
        registerReceiver(finishReceiver, filter);

        initialize();
        initializeMap();
        try {
            setLocationRequest();
            buildGoogleApiClient();
            startLocationUpdates();
        } catch (Exception e) {
        }
    }

    private void initialize() {

        sessionManager = new SessionManager(TripPage.this);
        gps = new GPSTracker(TripPage.this);
        cd = new ConnectionDetector(TripPage.this);
        v2GetRouteDirection = new GMapV2GetRouteDirection();
        isInternetPresent = cd.isConnectingToInternet();
        wayPointList = new ArrayList<LatLng>();
        polyLines = new ArrayList<Polyline>();
        myDBHelper = new GEODBHelper(TripPage.this);
        Cancelreason_arraylist = new ArrayList<CancelReasonPojo>();
        multipleDropList = new ArrayList<MultipleLocationPojo>();

        Txt_pickupAddress_or_destinationAddress = (TextView) findViewById(R.id.txt_pickupAddress_or_destinationAddress);
        Txt_bookingNo_or_driverName_or_tripStatus = (TextView) findViewById(R.id.txt_bookingNo_or_driverName_or_tripStatus);
        Txt_paymentMethod_or_estimatedFare_or_oneWay_or_multipleStop = (TextView) findViewById(R.id.txt_paymentMethod_or_estimatedFare_or_oneWay_or_multipleStop);

        Img_time = (ImageView) findViewById(R.id.img_time);
        Img_distance = (ImageView) findViewById(R.id.img_distance);
        Img_cancel_or_trip_info = (ImageView) findViewById(R.id.img_cancel_or_trip_info);
        Img_call_or_sos = (ImageView) findViewById(R.id.img_call_or_sos);

        Txt_time = (TextView) findViewById(R.id.txt_time);
        Txt_distance = (TextView) findViewById(R.id.txt_distance);
        Txt_cancel_or_trip_info = (TextView) findViewById(R.id.txt_cancel_or_trip_info);
        Txt_call_or_sos = (TextView) findViewById(R.id.txt_call_or_sos);

        btn_navigate = (Button) findViewById(R.id.btn_navigate);
        btn_ride = (Button) findViewById(R.id.btn_ride);

        RL_rider_profile = (RelativeLayout) findViewById(R.id.Rl_rider_profile);
        Txt_rider_name = (TextView) findViewById(R.id.txt_label_rider_name);
        Img_rider_profile_pic = (ImageView) findViewById(R.id.img_profile_rider);

        RL_profile = (RelativeLayout) findViewById(R.id.Rl_profile);

        Ib_currentLocation = (ImageButton) findViewById(R.id.Ibtn_current_location);

//        RL_rider_profile.setVisibility(View.VISIBLE);


//        Tv_riderName = (TextView) findViewById(R.id.txt_rider_name);
//        Rb_rider = (RatingBar) findViewById(R.id.rb_rider);
//        Cv_riderProfile = (ImageView) findViewById(R.id.img_profile_alert_rider);
//        Tv_riderProfession = (TextView) findViewById(R.id.txt_rider_profession);
//        Tv_member = (TextView) findViewById(R.id.txt_member);
//        Tv_dateJoin = (TextView) findViewById(R.id.txt_date_join);
//        Tv_gender = (TextView) findViewById(R.id.txt_gender);
//        Tv_tripCompleted = (TextView) findViewById(R.id.txt_trip_completed);
//        Tv_tripCancelled = (TextView) findViewById(R.id.txt_trip_cancelled);
//        btn_close = (Button) findViewById(R.id.btn_close);

        Img_time.setOnClickListener(this);
        Img_distance.setOnClickListener(this);
        Img_cancel_or_trip_info.setOnClickListener(this);
        Img_call_or_sos.setOnClickListener(this);
        RL_rider_profile.setOnClickListener(this);
        btn_ride.setOnClickListener(this);
        btn_navigate.setOnClickListener(this);
        Ib_currentLocation.setOnClickListener(this);

//        btn_close.setOnClickListener(this);

//        tv_member_since = (TextView) findViewById(R.id.Arrived_trip_membersine);
//        tv_usergender = (TextView) findViewById(R.id.Arrived_trip_usergender);
//        Tv_title = (TextView) findViewById(R.id.tripPage_alert_title);
//        subtitle = (TextView) findViewById(R.id.tripPage_alert_subtitle);
//        arrive_subtitle = (TextView) findViewById(R.id.Arrived_trip_label_label);
//        Tv_name = (TextView) findViewById(R.id.Arrived_trip_username);
//        Trip_layout = (RelativeLayout) findViewById(R.id.AlertTrip_layout);
//        arrived_layout = (RelativeLayout) findViewById(R.id.Arrived_trip_layout);
        userimg = (RoundedImageView) findViewById(R.id.user_img);
        Rl_multipleDrop = (RelativeLayout) findViewById(R.id.tripPage_multipleDrop);
        Tv_multipleDropState = (TextView) findViewById(R.id.tripPage_multipleDrop_textView);

        Iv_tripWayIcon = (ImageView) findViewById(R.id.tripPage_alert_oneWay_icon);
        Rl_returnTrip = (RelativeLayout) findViewById(R.id.Rl_return_trip);
        Tv_returnType = (TextView) findViewById(R.id.tripPage_returnTrip_textView);

        travelTimeRunnable = new Runnable() {
            @Override
            public void run() {

                System.out.println("-----------Ganesh Handler-----------");

                if (gps != null && gps.canGetLocation()) {

                    String currentLat = String.valueOf(gps.getLatitude());
                    String currentLong = String.valueOf(gps.getLongitude());

                    String destinationLat = "", destinationLong = "";

                    if (!Trip_Status.equalsIgnoreCase("end")) {

                        destinationLat = String.valueOf(Str_droplat);
                        destinationLong = String.valueOf(Str_droplon);

                    } else {

                        System.out.println("------------prem -------sEndNavigationLat----------" + sEndNavigationLat + "," + sEndNavigationLng);
                        if (sTripMode.equalsIgnoreCase("return")) {

                            if (isReturnAvailable.equalsIgnoreCase("1")) {

                                if (sReturnLat.length() > 0 && sReturnLng.length() > 0) {
                                    destinationLat = sReturnLat;
                                    destinationLong = sReturnLng;
                                }
                            } else {
                                if (sEndNavigationLat.length() > 0 && sEndNavigationLng.length() > 0) {
                                    destinationLat = sEndNavigationLat;
                                    destinationLong = sEndNavigationLng;
                                }
                            }

                        } else {
                            if (sEndNavigationLat.length() > 0 && sEndNavigationLng.length() > 0) {
                                destinationLat = sEndNavigationLat;
                                destinationLong = sEndNavigationLng;
                            }
                        }

                    }

                    String url = Iconstant.GetDistanceAndTime_url + "&origins=" + currentLat + "," + currentLong
                            + "&destinations=" + destinationLat + "," + destinationLong;
                    getDistanceAndTimeRequest(url);

                }

            }
        };

        Rl_multipleDrop.setOnClickListener(this);
        Rl_returnTrip.setOnClickListener(this);


        HashMap<String, String> userDetails = sessionManager.getUserDetails();
        driver_id = userDetails.get(SessionManager.KEY_DRIVERID);
        driver_name = userDetails.get(SessionManager.KEY_DRIVER_NAME);
        sContactNumber = sessionManager.getContactNumber();

        Intent i = getIntent();
        sPickUpAddress = i.getStringExtra("address");
        Str_RideId = i.getStringExtra("rideId");
        Str_pickUp_Lat = i.getStringExtra("pickuplat");
        Str_pickUp_Long = i.getStringExtra("pickup_long");
        Str_username = i.getStringExtra("username");
        Str_user_rating = i.getStringExtra("userrating");
        Str_user_phoneno = i.getStringExtra("phoneno");
        Str_user_img = i.getStringExtra("userimg");
        Suser_Id = i.getStringExtra("UserId");
        Str_droplat = i.getStringExtra("drop_lat");
        Str_droplon = i.getStringExtra("drop_lon");
        str_drop_location = i.getStringExtra("drop_location");
        Trip_Status = i.getStringExtra("trip_status");
        pickup_latlong = new LatLng(Double.parseDouble(Str_pickUp_Lat), Double.parseDouble(Str_pickUp_Long));
        pickup_multiStop_latlong = new LatLng(Double.parseDouble(Str_pickUp_Lat), Double.parseDouble(Str_pickUp_Long));
        drop_latlong = new LatLng(Double.parseDouble(Str_droplat), Double.parseDouble(Str_droplon));
        sTripMode = i.getStringExtra("TripMode");

        isReturnAvailable = i.getStringExtra("IsReturnAvailable");
        sReturnState = i.getStringExtra("ReturnState");
        String sReturnLatLngAvailable = i.getStringExtra("IsReturnLatLngAvailable");
        if (sReturnLatLngAvailable.equalsIgnoreCase("1")) {
            isReturnLatLngAvailable = true;
        } else {
            isReturnLatLngAvailable = false;
        }
        sReturnLat = i.getStringExtra("ReturnLat");
        sReturnLng = i.getStringExtra("ReturnLng");

        sReturnPickUpLat = i.getStringExtra("ReturnPickUpLat");
        sReturnPickUpLng = i.getStringExtra("ReturnPickUpLng");
        sPaymentType = i.getStringExtra("payment_type");
        sCancelledRides = i.getStringExtra("cancelled_rides");
        sCompletedRides = i.getStringExtra("completed_rides");
        sRiderType = i.getStringExtra("rider_type");

        if (sReturnLat.length() > 0 && sReturnLng.length() > 0 && sReturnLat != null && sReturnLng != null) {
            return_latLong = new LatLng(Double.parseDouble(sReturnLat), Double.parseDouble(sReturnLng));
        }

        if (sReturnPickUpLat.length() > 0 && sReturnPickUpLng.length() > 0 && sReturnPickUpLat != null && sReturnPickUpLng != null) {
            return_pickUp_latLong = new LatLng(Double.parseDouble(sReturnPickUpLat), Double.parseDouble(sReturnPickUpLng));
        }

        if (!Trip_Status.equals("arrived")) {
            title_text = i.getStringExtra("title_text");
            subtitle_text = i.getStringExtra("subtitle_text");
            est_cost = i.getStringExtra("est_cost");
            sCurrencySymbol = i.getStringExtra("sCurrencySymbol");

        } else {
            eta_arrival = i.getStringExtra("eta_arrival");
            location = i.getStringExtra("location");
            member_since = i.getStringExtra("member_since");
            user_gender = i.getStringExtra("user_gender");
        }


//        Tv_riderName.setText(Str_username);
//        Rb_rider.setRating(Float.parseFloat(Str_user_rating));
//        Tv_member.setText(location);
//        Tv_riderProfession.setText(sRiderType);
//        Tv_tripCompleted.setText(sCompletedRides);
//        Tv_tripCancelled.setText(sCancelledRides);
//        Tv_dateJoin.setText(member_since);
//        Tv_gender.setText(user_gender);

//        Picasso.with(TripPage.this).load(Str_user_img).error(R.drawable.no_user_img).placeholder(R.drawable.no_user_img).memoryPolicy(MemoryPolicy.NO_CACHE).into(Cv_riderProfile);


        /*if(Trip_Status.equalsIgnoreCase("arrived")){

            Txt_rider_name.setText(Str_username);
            Picasso.with(TripPage.this).load(Str_user_img).error(R.drawable.no_user_img).error(R.drawable.no_user_img).into(Img_rider_profile_pic);

            Txt_bookingNo_or_driverName_or_tripStatus.setText(getResources().getString(R.string.trip_label_booking_no) + Str_RideId);
            Txt_pickupAddress_or_destinationAddress.setText(getResources().getString(R.string.trip_label_pickup_address) + sPickUpAddress.replaceAll("\n"," "));
            Txt_paymentMethod_or_estimatedFare_or_oneWay_or_multipleStop.setText(getResources().getString(R.string.trip_label_payment_type) + "Cash");



        } else if(Trip_Status.equalsIgnoreCase("begin")){

            Txt_bookingNo_or_driverName_or_tripStatus.setText(driver_name);
            Txt_pickupAddress_or_destinationAddress.setText(getResources().getString(R.string.trip_label_destination_address) + str_drop_location.replaceAll("\n"," "));
            Txt_paymentMethod_or_estimatedFare_or_oneWay_or_multipleStop.setText(getResources().getString(R.string.trip_label_estimated_fare) + est_cost);

        } else if(Trip_Status.equalsIgnoreCase("end")) {

            Txt_bookingNo_or_driverName_or_tripStatus.setText(driver_name);
            Txt_pickupAddress_or_destinationAddress.setText(getResources().getString(R.string.trip_label_destination_address) + str_drop_location.replaceAll("\n"," "));

            if (sTripMode.length() > 0) {
                if (sTripMode.equalsIgnoreCase("oneway")) {
//                Iv_tripWayIcon.setImageResource(R.drawable.one_way_icon);
                    Txt_paymentMethod_or_estimatedFare_or_oneWay_or_multipleStop.setText(getResources().getString(R.string.trip_label_one_way));
                } else if (sTripMode.equalsIgnoreCase("return")) {
//                Iv_tripWayIcon.setImageResource(R.drawable.return_way_icon);
                    Txt_paymentMethod_or_estimatedFare_or_oneWay_or_multipleStop.setText(getResources().getString(R.string.trip_label_multiple_stop));
                }
            }

        }*/


        if (Trip_Status.equals("end")) {
            if (i.hasExtra("MultipleDropStatus")) {
                sMultipleDropStatus = i.getStringExtra("MultipleDropStatus");
                if (sMultipleDropStatus.equalsIgnoreCase("1")) {

                    isMultipleDropAvailable = true;
                    Rl_multipleDrop.setVisibility(View.VISIBLE);
                    try {
                        Bundle bundleObject = getIntent().getExtras();
                        multipleDropList = (ArrayList<MultipleLocationPojo>) bundleObject.getSerializable("additionalDropLocation");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    if (multipleDropList != null) {
                        for (int j = 0; j < multipleDropList.size(); j++) {

                            System.out.println("------multipleDropList------------" + multipleDropList.get(j).getIsEnd());

                            if (multipleDropList.get(j).getIsEnd().equalsIgnoreCase("0")) {

                                sEndNavigationLat = multipleDropList.get(j).getDropLat();
                                sEndNavigationLng = multipleDropList.get(j).getDropLon();
                                endTrip_NavigationLatLng = new LatLng(Double.parseDouble(sEndNavigationLat), Double.parseDouble(sEndNavigationLng));

                                sRecordId = multipleDropList.get(j).getRecordId();

                                if (multipleDropList.get(j).getMode().equalsIgnoreCase("start")) {
                                    Rl_multipleDrop.setBackgroundResource(R.drawable.multiple_drop_end);
                                    Tv_multipleDropState.setText(getResources().getString(R.string.trip_label_drop));
                                    multipleDropCount = 0;
                                } else {
                                    Rl_multipleDrop.setBackgroundResource(R.drawable.multiple_drop_start);
                                    Tv_multipleDropState.setText(getResources().getString(R.string.trip_label_start));
                                    multipleDropCount = 1;
                                }

                                Rl_multipleDrop.setVisibility(View.VISIBLE);


                                break;
                            } else {
                                sEndNavigationLat = Str_droplat;
                                sEndNavigationLng = Str_droplon;

                                pickup_multiStop_latlong = new LatLng(Double.parseDouble(multipleDropList.get(j).getDropLat()), Double.parseDouble(multipleDropList.get(j).getDropLon()));
                                endTrip_NavigationLatLng = new LatLng(Double.parseDouble(Str_droplat), Double.parseDouble(Str_droplon));
                                Rl_multipleDrop.setVisibility(View.GONE);


                            }
                        }

                    } else {
                        sEndNavigationLat = Str_droplat;
                        sEndNavigationLng = Str_droplon;
                    }


                } else {
                    sEndNavigationLat = Str_droplat;
                    sEndNavigationLng = Str_droplon;
                    isMultipleDropAvailable = false;
                    Rl_multipleDrop.setVisibility(View.GONE);
                }
            } else {
                sEndNavigationLat = Str_droplat;
                sEndNavigationLng = Str_droplon;
                isMultipleDropAvailable = false;
            }
        }

//        Txt_bookingNo_or_driverName_or_tripStatus.setText(getResources().getString(R.string.trip_label_booking_no) + Str_RideId);

        travelTimeHandler = new Handler();
        travelTimeHandler.post(travelTimeRunnable);

    }

    private void initializeMap() {
        if (googleMap == null) {
             mapFragment = ((MapFragment) getFragmentManager().findFragmentById(R.id.tripPage_googleMap));
            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap arg) {
                    loadMap(arg);
                }
            });
        }
    }

    public void loadMap(GoogleMap arg) {

        googleMap = arg;
        googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getApplicationContext(), R.raw.style));
        googleMap.getUiSettings().setRotateGesturesEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        gps = new GPSTracker(TripPage.this);
        if (gps != null && gps.canGetLocation()) {
            double dLatitude = gps.getLatitude();
            double dLongitude = gps.getLongitude();
            MyCurrent_lat = dLatitude;
            MyCurrent_long = dLongitude;
            cur_latlong = new LatLng(MyCurrent_lat, MyCurrent_long);
        } else {
            enableGpsService();
        }

        if (!String.valueOf(MyCurrent_lat).equals(0.0) && !String.valueOf(MyCurrent_long).equals(0.0)) {
            if (googleMap != null) {
                CameraPosition cameraPosition = new CameraPosition.Builder().target(cur_latlong).zoom(13).build();
                googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                MarkerOptions marker = new MarkerOptions().position(cur_latlong);
                marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.carmove));
                currentDriverMarker = googleMap.addMarker(marker);

                if (Trip_Status.equals("arrived")) {
//                    arrived_layout.setVisibility(View.VISIBLE);
//                    Trip_layout.setVisibility(View.INVISIBLE);
                    arrived(sPickUpAddress, eta_arrival, Str_RideId, location, member_since, user_gender, Str_username, Str_user_img);
                } else if (Trip_Status.equals("begin")) {
//                    arrived_layout.setVisibility(View.INVISIBLE);
//                    Trip_layout.setVisibility(View.VISIBLE);
                    Begin(str_drop_location, est_cost + sCurrencySymbol, Str_RideId, title_text, subtitle_text);
                } else if (Trip_Status.equals("end")) {
//                    arrived_layout.setVisibility(View.INVISIBLE);
//                    Trip_layout.setVisibility(View.GONE);
                    End(str_drop_location, est_cost + sCurrencySymbol, Str_RideId, title_text, subtitle_text);
                }

            }
        }

        sessionManager.setUpdateLocationServiceState("trip");


    }

    private void pulseMarker(final Bitmap markerIcon, final Marker markerr, final long onePulseDuration) {
        final Handler handler = new Handler();
        final long startTime = System.currentTimeMillis();

        final Interpolator interpolator = new CycleInterpolator(1f);
        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = System.currentTimeMillis() - startTime;
                float t = interpolator.getInterpolation((float) elapsed / onePulseDuration);
                markerr.setIcon(BitmapDescriptorFactory.fromBitmap(scaleBitmap(markerIcon, 1f + 0.05f * t)));
                handler.postDelayed(this, 16);
            }
        });
    }

    public Bitmap scaleBitmap(Bitmap bitmap, float scaleFactor) {
        final int sizeX = Math.round(bitmap.getWidth() * scaleFactor);
        final int sizeY = Math.round(bitmap.getHeight() * scaleFactor);
        Bitmap bitmapResized = Bitmap.createScaledBitmap(bitmap, sizeX, sizeY, false);
        return bitmapResized;
    }


    /*private void addPulsatingEffect(final LatLng userLatlng) {


        if (lastPulseAnimator != null) {
            lastPulseAnimator.cancel();
            Log.d("onLocationUpdated: ", "cancelled");
        }
        if (lastUserCircle != null)
            lastUserCircle.setCenter(userLatlng);
        lastPulseAnimator = valueAnimate(0, pulseDuration, new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                if (lastUserCircle != null)
                    lastUserCircle.setRadius((Float) animation.getAnimatedValue());
                else {
                    lastUserCircle = googleMap.addCircle(new CircleOptions()
                            .center(userLatlng)
                            .radius((Float) animation.getAnimatedValue())
                            .strokeColor(Color.RED)
                            .fillColor(Color.BLUE));
                }
            }
        });

    }*/

    /*@SuppressLint("WrongConstant")
    protected ValueAnimator valueAnimate(float accuracy, long duration, ValueAnimator.AnimatorUpdateListener updateListener) {
        Log.d("valueAnimate: ", "called");
        ValueAnimator va = ValueAnimator.ofFloat(0, accuracy);
        va.setDuration(duration);
        va.addUpdateListener(updateListener);
        va.setRepeatCount(ValueAnimator.INFINITE);
        va.setRepeatMode(ValueAnimator.RESTART);

        va.start();
        return va;
    }*/


    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (mGoogleApiClient != null)
            mGoogleApiClient.disconnect();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (myLocation == null) {
            myLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        }

    }

    private void setLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void SetRouteMap(LatLng fromPosition, LatLng toPosition) {
        if (fromPosition != null && toPosition != null) {
            GetRouteTask getRoute = new GetRouteTask(fromPosition, toPosition);
            getRoute.execute();
        }
    }

    protected void startLocationUpdates() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onClick(View view) {

        cd = new ConnectionDetector(TripPage.this);
        isInternetPresent = cd.isConnectingToInternet();

        if (isInternetPresent) {

            if (view == Img_time) {

            } else if (view == Img_distance) {

            } else if (view == Img_call_or_sos) {

                if (Txt_call_or_sos.getText().toString().equalsIgnoreCase(getResources().getString(R.string.trip_page_label_call))) {

                    if (Str_user_phoneno != null) {

//                        HashMap<String, String> phone = sessionManager.getPhoneMaskingStatus();
                        String sPhoneMaskingStatus = sessionManager.getPhoneMaskingStatus();

                        if (sPhoneMaskingStatus.equalsIgnoreCase("Yes")) {
                            postRequest_PhoneMasking(Iconstant.phoneMasking_url);
                        } else {


                        }

                    } else {
                        alert(TripPage.this.getResources().getString(R.string.action_error), TripPage.this.getResources().getString(R.string.inavid_call));
                    }


                } else if (Txt_call_or_sos.getText().toString().equalsIgnoreCase(getResources().getString(R.string.trip_page_label_sos))) {

                    showSOSAlert();

                }

            } else if (view == Img_cancel_or_trip_info) {

                if (Txt_cancel_or_trip_info.getText().toString().equalsIgnoreCase(getResources().getString(R.string.trip_page_label_cancel))) {

                    PostRequestCancel(Iconstant.driverCancel_reason_Url);

                } else if (Txt_cancel_or_trip_info.getText().toString().equalsIgnoreCase(getResources().getString(R.string.trip_page_label_trip_info))) {

                    Intent intent = new Intent(TripPage.this, TripSummary.class);
                    intent.putExtra("rideID", Str_RideId);
                    intent.putExtra("fromPage", "TripPage");
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);

                }

            } else if (view == btn_navigate) {

                checkNavigation();

            } else if (view == btn_ride) {

                if (btn_ride.getText().toString().equalsIgnoreCase(getResources().getString(R.string.trip_page_label_arrive))) {

                    PostRequest(Iconstant.driverArrived_Url, "arrived");

                } else if (btn_ride.getText().toString().equalsIgnoreCase(getResources().getString(R.string.trip_page_label_start))) {

                    PostRequest(Iconstant.driverBegin_Url, "begin");

                }/*else if(btn_ride.getText().toString().equalsIgnoreCase(getResources().getString(R.string.trip_page_label_finish))){

                }*/ else {

                    if (Rl_multipleDrop.getVisibility() == View.VISIBLE) {

                        if (Tv_multipleDropState.getText().toString().equalsIgnoreCase(getResources().getString(R.string.trip_label_start))) {

                            alert(TripPage.this.getResources().getString(R.string.action_error), TripPage.this.getResources().getString(R.string.trip_label_startRide_continue_alert));

                        } else if (Tv_multipleDropState.getText().toString().equalsIgnoreCase(getResources().getString(R.string.trip_label_drop))) {

                            alert(TripPage.this.getResources().getString(R.string.action_error), TripPage.this.getResources().getString(R.string.trip_label_dropRide_continue_alert));

                        }

                    } else if (Rl_returnTrip.getVisibility() == View.VISIBLE) {

                        if (Tv_returnType.getText().toString().equalsIgnoreCase(getResources().getString(R.string.trip_label_drop))) {

                            alert(TripPage.this.getResources().getString(R.string.action_error), TripPage.this.getResources().getString(R.string.trip_label_dropRide_continue_alert));

                        } else if (Tv_returnType.getText().toString().equalsIgnoreCase(getResources().getString(R.string.trip_label_return))) {

                            alert(TripPage.this.getResources().getString(R.string.action_error), TripPage.this.getResources().getString(R.string.trip_label_returnRide_continue_alert));

                        }

                    } else {

                        if (multipleDropCount == 1) {
                            alert(TripPage.this.getResources().getString(R.string.action_error), TripPage.this.getResources().getString(R.string.trip_label_startRide_continue_alert));
                        } else {

                            dialog = new Dialog(TripPage.this);
                            dialog.getWindow();
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog.setContentView(R.layout.custom_loading);
                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            dialog.setCanceledOnTouchOutside(false);
                            dialog.show();

                            GeoCoderAsyncTask asyncTask = new GeoCoderAsyncTask();
                            asyncTask.execute();
                        }

                    }

                }

            } else if (view == RL_rider_profile) {

                showRiderProfile();

            } else if (view == Rl_multipleDrop) {
                if (multipleDropCount == 1) {
                    postRequestDropAndStartRide(Iconstant.multipleDrop_Url, "end", sRecordId);
                } else {
                    postRequestDropAndStartRide(Iconstant.multipleDrop_Url, "start", sRecordId);
                }
            } else if (view == Rl_returnTrip) {
                if (Tv_returnType.getText().toString().equalsIgnoreCase(getResources().getString(R.string.trip_label_drop))) {
                    postRequestReturnTrip(Iconstant.returnTrip_Url, "drop");
                } else {
                    postRequestReturnTrip(Iconstant.returnTrip_Url, "start");
                }
            } else if (view == Ib_currentLocation) {
                if (googleMap != null) {
                    if (gps != null && gps.canGetLocation()) {

                        try {

                            double lat = gps.getLatitude();
                            double lon = gps.getLongitude();

                            if (lat != 0.0 && lon != 0.0) {
                                LatLng latLng = new LatLng(gps.getLatitude(), gps.getLongitude());

                                float zoom = googleMap.getCameraPosition().zoom;
                                CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(zoom).build();
                                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
            } /*else if (view == btn_close){

                RL_profile.setVisibility(View.GONE);

            }*/

        } else {
            alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }
    }


    private void showRiderProfile() {
        final Dialog dialog = new Dialog(TripPage.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.alert_trip_rider_details);

        /*Window window = dialog.getWindow();
        WindowManager.LayoutParams param = window.getAttributes();
        param.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;
        param.y = 100;
        window.setAttributes(param);
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));*/

        TextView Tv_riderName = (TextView) dialog.findViewById(R.id.txt_rider_name);
        RatingBar Rb_rider = (RatingBar) dialog.findViewById(R.id.rb_rider);
        ImageView Cv_riderProfile = (ImageView) dialog.findViewById(R.id.img_profile_alert_rider);
        TextView Tv_riderType = (TextView) dialog.findViewById(R.id.txt_rider_type);
        TextView Tv_member = (TextView) dialog.findViewById(R.id.txt_member);
        TextView Tv_dateJoin = (TextView) dialog.findViewById(R.id.txt_date_join);
        TextView Tv_gender = (TextView) dialog.findViewById(R.id.txt_gender);
        TextView Tv_tripCompleted = (TextView) dialog.findViewById(R.id.txt_trip_completed);
        TextView Tv_tripCancelled = (TextView) dialog.findViewById(R.id.txt_trip_cancelled);
        Button btn_close = (Button) dialog.findViewById(R.id.btn_close);

        Tv_riderName.setText(Str_username);
        Rb_rider.setRating(Float.parseFloat(Str_user_rating));
        Tv_member.setText(location);
        Tv_riderType.setText(sRiderType);
        Tv_tripCompleted.setText(sCompletedRides);
        Tv_tripCancelled.setText(sCancelledRides);
        Tv_dateJoin.setText(member_since);
        Tv_gender.setText(user_gender);

        Picasso.with(TripPage.this).load(Str_user_img).error(R.drawable.no_user_img).placeholder(R.drawable.no_user_img).memoryPolicy(MemoryPolicy.NO_CACHE).into(Cv_riderProfile);

        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });

        dialog.show();

//        RL_profile.setVisibility(View.VISIBLE);

    }


    private class GeoCoderAsyncTask extends AsyncTask<String, String, String> {

        String strAdd = "";

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... params) {

            Geocoder geocoder = new Geocoder(TripPage.this, Locale.getDefault());
            try {
                List<Address> addresses = geocoder.getFromLocation(MyCurrent_lat, MyCurrent_long, 1);
                if (addresses != null) {
                    Address returnedAddress = addresses.get(0);
                    StringBuilder strReturnedAddress = new StringBuilder("");
                    for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                        strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                    }
                    strAdd = strReturnedAddress.toString();
                } else {
                    strAdd = "error";
                    Log.e("Current loction address", "No Address returned!");
                }
            } catch (Exception e) {
                e.printStackTrace();
                strAdd = "error";
                Log.e("Current loction address", "Cannot get Address!");
            }
            return strAdd;
        }

        @Override
        protected void onPostExecute(String result) {
            if (result.equalsIgnoreCase("error")) {
                sCurrentLocationName = "";
                PostRequest(Iconstant.driverEnd_Url, "end");
            } else {
                sCurrentLocationName = result;
                PostRequest(Iconstant.driverEnd_Url, "end");
            }
        }

    }


    private void checkNavigation() {

        if (Build.VERSION.SDK_INT >= 23) {

            if (!checkWriteExternalStoragePermission()) {
                requestNavigationPermission();
            } else {
                if (!Settings.canDrawOverlays(TripPage.this)) {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                            Uri.parse("package:" + getPackageName()));
                    startActivityForResult(intent, OVERLAY_PERMISSION_REQ_CODE);
                } else {
                    moveNavigation();
                }
            }
        } else {
            moveNavigation();
        }
    }


    private void moveNavigation() {
        if (isGoogleMapsInstalled()) {

            if (Trip_Status.equals("arrived")) {
                startServiceNavigation();
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(String.format("google.navigation:ll=%s,%s%s", Double.parseDouble(Str_pickUp_Lat), Double.parseDouble(Str_pickUp_Long), "&mode=c")));
                startActivity(intent);
            } else if (Trip_Status.equals("begin")) {
                startServiceNavigation();
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(String.format("google.navigation:ll=%s,%s%s", Double.parseDouble(Str_droplat), Double.parseDouble(Str_droplon), "&mode=c")));
                startActivity(intent);
            } else if (Trip_Status.equals("end")) {

                System.out.println("------------prem -------sEndNavigationLat----------" + sEndNavigationLat + "," + sEndNavigationLng);

                if(Build.VERSION.SDK_INT >= 23) {
                    if (!Settings.canDrawOverlays(TripPage.this)) {
                        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                Uri.parse("package:" + getPackageName()));
                        startActivityForResult(intent, 1234);
                    }
                }
                else
                {
                    if (!isMyServiceRunning(GoogleNavigationService.class)) {
                        startService(new Intent(getApplicationContext(), GoogleNavigationService.class));
                    }
                }

                String sReturnStateString = "";
                if (isReturnLatLngAvailable) {
                    sReturnStateString = "1";
                } else {
                    sReturnStateString = "0";
                }

                sessionManager.setGoogleNavigationData(sPickUpAddress, Str_RideId, Str_pickUp_Lat, Str_pickUp_Long, Str_username, Str_user_rating, Str_user_phoneno,
                        Str_user_img, Suser_Id, Str_droplat, Str_droplon, str_drop_location, title_text, subtitle_text, Trip_Status, eta_arrival, location,
                        member_since, user_gender, est_cost, sCurrencySymbol, sMultipleDropStatus, sTripMode, multipleDropList, isReturnAvailable, sReturnState, sReturnStateString, sReturnLat, sReturnLng, sReturnPickUpLat, sReturnPickUpLng,
                        sPaymentType, sCancelledRides, sCompletedRides, sRiderType);


                if (sTripMode.equalsIgnoreCase("return")) {

                    if (isReturnAvailable.equalsIgnoreCase("1")) {

                        if (sReturnLat.length() > 0 && sReturnLng.length() > 0) {
                            Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(String.format("google.navigation:ll=%s,%s%s", Double.parseDouble(sReturnLat), Double.parseDouble(sReturnLng), "&mode=c")));
                            startActivity(intent);
                        }
                    } else {
                        if (sEndNavigationLat.length() > 0 && sEndNavigationLng.length() > 0) {
                            Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(String.format("google.navigation:ll=%s,%s%s", Double.parseDouble(sEndNavigationLat), Double.parseDouble(sEndNavigationLng), "&mode=c")));
                            startActivity(intent);
                        }
                    }

                } else {
                    if (sEndNavigationLat.length() > 0 && sEndNavigationLng.length() > 0) {
                        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(String.format("google.navigation:ll=%s,%s%s", Double.parseDouble(sEndNavigationLat), Double.parseDouble(sEndNavigationLng), "&mode=c")));
                        startActivity(intent);
                    }
                }

            }
        } else {
            alert(getResources().getString(R.string.alert_label_oops), getResources().getString(R.string.alert_label_no_google_map_installed));
        }
    }

    private void startServiceNavigation() {
        if(Build.VERSION.SDK_INT >= 23) {
            if (!Settings.canDrawOverlays(TripPage.this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, 1234);
            }
        }
        else
        {
            if (!isMyServiceRunning(GoogleNavigationService.class)) {
                startService(new Intent(getApplicationContext(), GoogleNavigationService.class));
            }
        }

        String sReturnStateString = "";
        if (isReturnLatLngAvailable) {
            sReturnStateString = "1";
        } else {
            sReturnStateString = "0";
        }

        sessionManager.setGoogleNavigationData(sPickUpAddress, Str_RideId, Str_pickUp_Lat, Str_pickUp_Long, Str_username, Str_user_rating, Str_user_phoneno,
                Str_user_img, Suser_Id, Str_droplat, Str_droplon, str_drop_location, title_text, subtitle_text, Trip_Status, eta_arrival, location,
                member_since, user_gender, est_cost, sCurrencySymbol, sMultipleDropStatus, sTripMode, multipleDropList, isReturnAvailable, sReturnState, sReturnStateString, sReturnLat, sReturnLng, sReturnPickUpLat, sReturnPickUpLng,
                sPaymentType, sCancelledRides, sCompletedRides, sRiderType);

    }

    private boolean isGoogleMapsInstalled() {
        try {
            ApplicationInfo info = getPackageManager().getApplicationInfo("com.google.android.apps.maps", 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        boolean b = false;
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                b = true;
                break;
            } else {
                b = false;
            }
        }
        System.out.println("3 not running");
        return b;
    }


    private boolean checkWriteExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }


    private void requestNavigationPermission() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_NAVIGATION_CODE);
    }


    //-----------------------PhoneMasking Post Request-----------------
    private void postRequest_PhoneMasking(String Url) {

        dialog = new Dialog(TripPage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_pleasewait));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("ride_id", Str_RideId);
        jsonParams.put("user_type", "driver");

        System.out.println("-------------PhoneMasking Url----------------" + Url);
        mRequest = new ServiceRequest(TripPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------PhoneMasking Response----------------" + response);

                String Str_status = "", SResponse = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");
                    SResponse = object.getString("response");
                    if (Str_status.equalsIgnoreCase("1")) {
                        alert(getResources().getString(R.string.action_success), SResponse);
                    } else {
                        alert(getResources().getString(R.string.action_error), SResponse);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    dialog.dismiss();
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }


    private void PostRequestCancel(String Url) {
        dialog = new Dialog(TripPage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_pleasewait));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", driver_id);
        jsonParams.put("ride_id", Str_RideId);
        System.out.println("-------------------cancel-jsonParams----------------------" + jsonParams);
        mRequest = new ServiceRequest(TripPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                String Sstatus = "", Str_response = "";
                try {
                    System.out.println("-------------------cancel-response----------------------" + response);

                    JSONObject jobject = new JSONObject(response);
                    Sstatus = jobject.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject object = jobject.getJSONObject("response");
                        JSONArray jarry = object.getJSONArray("reason");

                        if (jarry.length() > 0) {
                            Cancelreason_arraylist.clear();
                            for (int i = 0; i < jarry.length(); i++) {

                                JSONObject object1 = jarry.getJSONObject(i);

                                CancelReasonPojo items = new CancelReasonPojo();

                                items.setReason(object1.getString("reason"));
                                items.setCancelreason_id(object1.getString("id"));

                                System.out.println("reason----------" + object1.getString("reason"));

                                Cancelreason_arraylist.add(items);

                            }
                            show_progress_status = true;

                        } else {
                            show_progress_status = false;
                        }
                    } else {
                        Str_response = jobject.getString("response");
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                if (Sstatus.equalsIgnoreCase("1")) {

                    if (show_progress_status) {
                        cancelDialog();
                    }

                } else {
                    alert(getResources().getString(R.string.action_error), Str_response);
                }


                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void cancelDialog() {

        cd = new ConnectionDetector(TripPage.this);
        isInternetPresent = cd.isConnectingToInternet();

        cancel_dialog = new Dialog(TripPage.this);
        cancel_dialog.getWindow();
        cancel_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        cancel_dialog.setContentView(R.layout.cancel_reason_alert);
        cancel_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        cancel_dialog.setCanceledOnTouchOutside(true);
        cancel_dialog.show();
        cancel_dialog.getWindow().setGravity(Gravity.CENTER);

        cancel_list = (ListView) cancel_dialog.findViewById(R.id.cancel_alert_listview);
        Tv_close = (CustomTextView) cancel_dialog.findViewById(R.id.cancel_alert_back_textview);
        CustomTextView Tv_ok = (CustomTextView) cancel_dialog.findViewById(R.id.cancel_alert_confirm_textview);

        CancelAlertAdapter = new CancelAlertAdapter(TripPage.this, Cancelreason_arraylist);
        cancel_list.setAdapter(CancelAlertAdapter);

        cancel_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                reason_id = Cancelreason_arraylist.get(position).getCancelreason_id();

                for (int i = 0; i < Cancelreason_arraylist.size(); i++) {
                    if (i == position) {
                        Cancelreason_arraylist.get(i).setCancelstatus("1");
                    } else {
                        Cancelreason_arraylist.get(i).setCancelstatus("0");
                    }
                }
                CancelAlertAdapter.notifyDataSetChanged();
            }
        });
        Tv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HashMap<String, String> jsonParams = new HashMap<String, String>();
                jsonParams.put("driver_id", driver_id);
                jsonParams.put("reason", reason_id);
                jsonParams.put("ride_id", Str_RideId);

                if (isInternetPresent) {
                    cancel_dialog.dismiss();
                    postRequestCancelurl(Iconstant.driverCancel_Url, jsonParams);
                } else {
                    alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));

                }
            }
        });

        Tv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel_dialog.dismiss();
            }
        });
    }

    private void postRequestCancelurl(String url, HashMap<String, String> jsonParams) {
        dialog = new Dialog(TripPage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        System.out.println("-------------cancelling----------------" + url);


        System.out.println("jsonParams-------------" + jsonParams);

        mRequest = new ServiceRequest(TripPage.this);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------- Response----------------" + response);
                String Str_status = "", Str_message = "", Str_Id = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");
                    JSONObject jobject = object.getJSONObject("response");
                    Str_message = jobject.getString("message");
                    Str_Id = jobject.getString("ride_id");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (Str_status.equalsIgnoreCase("1")) {

                    sessionManager.setUpdateLocationServiceState("");
                    alert_Cancel(getResources().getString(R.string.action_success), Str_message);
                } else {
                    alert(getResources().getString(R.string.alert_label_title), Str_message);
                }
                dialog.dismiss();

            }

            @Override
            public void onErrorListener() {

                dialog.dismiss();
            }

        });
    }


    //-----------------------drop ride start ride  Post Request-----------------
    private void postRequestDropAndStartRide(String Url, final String mode, String recordId) {

        dialog = new Dialog(TripPage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        final TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_pleasewait));


        System.out.println("-------------start and drop url----------------" + Url);
        ArrayList<String> travel_history = myDBHelper.getDataEndTrip(Str_RideId);
        System.out.println("-----------jai---total_distance-------------------" + travel_history.toString().replace("[", "").replace("]", "").replace(" ", ""));
        StringBuilder builder = new StringBuilder();
        for (String string : travel_history) {
            builder.append("," + string);
        }
        ArrayList<LatLng> distance_travelled = myDBHelper.getDisEndTrip(Str_RideId);
        calculateDistance(distance_travelled);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("ride_id", Str_RideId);
        jsonParams.put("latitude", String.valueOf(MyCurrent_lat));
        jsonParams.put("longitude", String.valueOf(MyCurrent_long));
        jsonParams.put("driver_id", driver_id);
        jsonParams.put("mode", mode);
        jsonParams.put("record_id", recordId);

        if(mode.equalsIgnoreCase("end")){
            jsonParams.put("travel_history", builder.toString());
            jsonParams.put("distance", String.valueOf(totalDistanceTravelled / 1000));
        }
        System.out.println("-------------start and drop jsonParams----------------" + jsonParams);

        mRequest = new ServiceRequest(TripPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------latlongupdate----------------" + response);

                String dropStatus = "", status = "";

                try {
                    JSONObject object = new JSONObject(response);
                    status = object.getString("status");
                    if ("1".equalsIgnoreCase(status)) {
                        JSONObject jsonObject = object.getJSONObject("response");
                        sRecordId = jsonObject.getString("record_id");
                        dropStatus = jsonObject.getString("multi_drop_button_status");

                        if (jsonObject.has("multistop_array")) {

                            System.out.println("--------prem inside multistop_array-----------");

                            Object multiDrop_array_check = jsonObject.get("multistop_array");
                            if (multiDrop_array_check instanceof JSONArray) {
                                JSONArray multiDropArray = jsonObject.getJSONArray("multistop_array");
                                if (multiDropArray.length() > 0) {
                                    multipleDropList.clear();
                                    for (int i = 0; i < multiDropArray.length(); i++) {
                                        JSONObject multipleDropArrayObject = multiDropArray.getJSONObject(i);

                                        MultipleLocationPojo pojo = new MultipleLocationPojo();
                                        pojo.setDropLocation(multipleDropArrayObject.getString("location"));
                                        pojo.setIsEnd(multipleDropArrayObject.getString("is_end"));
                                        pojo.setMode(multipleDropArrayObject.getString("mode"));
                                        pojo.setRecordId(multipleDropArrayObject.getString("record_id"));

                                        JSONObject latLngArrayObject = multipleDropArrayObject.getJSONObject("latlong");
                                        if (latLngArrayObject.length() > 0) {
                                            pojo.setDropLat(latLngArrayObject.getString("lat"));
                                            pojo.setDropLon(latLngArrayObject.getString("lon"));
                                        }

                                        multipleDropList.add(pojo);
                                    }

                                    isMultipleDropAvailable = true;
                                    sMultipleDropStatus = "1";

                                } else {
                                    multipleDropList.clear();
                                    sMultipleDropStatus = "0";
                                    isMultipleDropAvailable = false;
                                }
                            } else {
                                multipleDropList.clear();
                                sMultipleDropStatus = "0";
                                isMultipleDropAvailable = false;
                            }
                        } else {
                            sMultipleDropStatus = "0";
                            isMultipleDropAvailable = false;
                        }


                        if (isMultipleDropAvailable) {
                            for (int j = 0; j < multipleDropList.size(); j++) {

                                if (multipleDropList.get(j).getIsEnd().equalsIgnoreCase("0")) {

                                    sEndNavigationLat = multipleDropList.get(j).getDropLat();
                                    sEndNavigationLng = multipleDropList.get(j).getDropLon();
                                    endTrip_NavigationLatLng = new LatLng(Double.parseDouble(sEndNavigationLat), Double.parseDouble(sEndNavigationLng));
                                    break;
                                } else {
                                    sEndNavigationLat = Str_droplat;
                                    sEndNavigationLng = Str_droplon;
                                    endTrip_NavigationLatLng = new LatLng(Double.parseDouble(Str_droplat), Double.parseDouble(Str_droplon));
                                }
                            }
                        } else {
                            sEndNavigationLat = Str_droplat;
                            sEndNavigationLng = Str_droplon;
                        }


                        if (mode.equalsIgnoreCase("end")) {
                            Rl_multipleDrop.setBackgroundResource(R.drawable.multiple_drop_end);
                            Tv_multipleDropState.setText(getResources().getString(R.string.trip_label_drop));
                            multipleDropCount = 0;
                            SetRouteMap(cur_latlong, endTrip_NavigationLatLng);
                        } else {
                            Rl_multipleDrop.setBackgroundResource(R.drawable.multiple_drop_start);
                            Tv_multipleDropState.setText(getResources().getString(R.string.trip_label_start));
                            multipleDropCount = 1;
                        }

                        if (dropStatus.equalsIgnoreCase("0")) {
                            Rl_multipleDrop.setVisibility(View.GONE);
                        } else {
                            Rl_multipleDrop.setVisibility(View.VISIBLE);
                        }

                    }

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {

                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    //-----------------------Return Trip  Post Request-----------------
    private void postRequestReturnTrip(String Url, final String mode) {

        dialog = new Dialog(TripPage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        final TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_pleasewait));
        ArrayList<String> travel_history = myDBHelper.getDataEndTrip(Str_RideId);
        System.out.println("-----------jai---total_distance-------------------" + travel_history.toString().replace("[", "").replace("]", "").replace(" ", ""));
        StringBuilder builder = new StringBuilder();
        for (String string : travel_history) {
            builder.append("," + string);
        }
        ArrayList<LatLng> distance_travelled = myDBHelper.getDisEndTrip(Str_RideId);
        calculateDistance(distance_travelled);

        System.out.println("-------------Return Trip url----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("ride_id", Str_RideId);
        jsonParams.put("latitude", String.valueOf(MyCurrent_lat));
        jsonParams.put("longitude", String.valueOf(MyCurrent_long));

        jsonParams.put("driver_id", driver_id);
        jsonParams.put("mode", mode);
        if(mode.equalsIgnoreCase("drop")){
            jsonParams.put("travel_history", builder.toString());
            jsonParams.put("distance", String.valueOf(totalDistanceTravelled / 1000));
        }


        System.out.println("------------Return Trip jsonParams----------------" + jsonParams);

        mRequest = new ServiceRequest(TripPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Return Trip response----------------" + response);

                String status = "";

                try {
                    JSONObject object = new JSONObject(response);
                    status = object.getString("status");
                    if ("1".equalsIgnoreCase(status)) {
                        JSONObject jsonObject = object.getJSONObject("response");
                        isReturnAvailable = jsonObject.getString("is_return_over");
                        sReturnState = jsonObject.getString("mode");


                        Object return_array_check = jsonObject.get("next_drop_location");
                        if (return_array_check instanceof JSONObject) {
                            JSONObject returnArrayObject = jsonObject.getJSONObject("next_drop_location");
                            JSONObject latLngArrayObject = returnArrayObject.getJSONObject("latlong");
                            if (latLngArrayObject.length() > 0) {
                                sReturnLat = latLngArrayObject.getString("lat");
                                sReturnLng = latLngArrayObject.getString("lon");

                                isReturnLatLngAvailable = true;
                            } else {
                                isReturnLatLngAvailable = false;
                            }
                        } else {
                            isReturnLatLngAvailable = false;
                        }


                        if (sReturnState.equalsIgnoreCase("start")) {
                            Rl_returnTrip.setBackgroundResource(R.drawable.multiple_drop_end);
                            Tv_returnType.setText(getResources().getString(R.string.trip_label_return));
                        } else if (sReturnState.equalsIgnoreCase("over")) {
                            if (isReturnLatLngAvailable) {
                                return_pickUp_latLong = new LatLng(MyCurrent_lat, MyCurrent_long);
                                return_latLong = new LatLng(Double.parseDouble(sReturnLat), Double.parseDouble(sReturnLng));
                                SetRouteMap(return_pickUp_latLong, return_latLong);
                            }
                        }

                        if (isReturnAvailable.equalsIgnoreCase("0")) {
                            Rl_returnTrip.setVisibility(View.VISIBLE);
                        } else {
                            Rl_returnTrip.setVisibility(View.GONE);
                        }

                    }

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {

                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    private void PostRequest(String Url, final String trip_status) {

        if (!trip_status.equals("end")) {
            dialog = new Dialog(TripPage.this);
            dialog.getWindow();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.custom_loading);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_pleasewait));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", driver_id);
        jsonParams.put("ride_id", Str_RideId);
        if (trip_status.equals("begin")) {
            jsonParams.put("pickup_lat", String.valueOf(MyCurrent_lat));
            jsonParams.put("pickup_lon", String.valueOf(MyCurrent_long));
        } else if (trip_status.equals("end")) {

            ArrayList<String> travel_history = myDBHelper.getDataEndTrip(Str_RideId);

            System.out.println("-----------jai---total_distance-------------------" + travel_history.toString().replace("[", "").replace("]", "").replace(" ", ""));
            StringBuilder builder = new StringBuilder();
            for (String string : travel_history) {
                builder.append("," + string);
            }
            ArrayList<LatLng> distance_travelled = myDBHelper.getDisEndTrip(Str_RideId);
            calculateDistance(distance_travelled);

            jsonParams.put("drop_lat", String.valueOf(MyCurrent_lat));
            jsonParams.put("drop_lon", String.valueOf(MyCurrent_long));
            jsonParams.put("drop_loc", sCurrentLocationName);
            jsonParams.put("distance", String.valueOf(totalDistanceTravelled / 1000));
            jsonParams.put("travel_history", builder.toString());
            jsonParams.put("wait_time", "");
        }

        System.out.println("--------------TRIP Url-------------------" + Url);
        System.out.println("--------------jsonParams-------------------" + jsonParams);
        System.out.println("--------------trip_status-------------------" + trip_status);
        mRequest = new ServiceRequest(TripPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------response-------------------" + response);
                String status = "", need_payment = "", ride_view = "", drop_location = "", est_cost = "", currency_code = "", ride_id = "", sCurrencySymbol = "", rider_image = "";

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {
                        status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {

                            if (!trip_status.equals("end")) {
                                ride_view = object.getString("ride_view");
                                drop_location = object.getString("drop_location");
                                est_cost = object.getString("est_cost");
                                currency_code = object.getString("currency_code");
                                ride_id = object.getString("ride_id");
                                title_text = object.getString("title_text");
                                subtitle_text = object.getString("subtitle_text");

                                if (object.has("is_return_over")) {
                                    isReturnAvailable = object.getString("is_return_over");
                                }

                                if (object.has("return_mode")) {
                                    sReturnState = object.getString("return_mode");
                                }

                                if (object.has("mode")) {
                                    sTripMode = object.getString("mode");
                                }

                                sCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(currency_code);

                                if (object.has("additional_drop_loc")) {
                                    Object multiDrop_array_check = object.get("additional_drop_loc");
                                    if (multiDrop_array_check instanceof JSONArray) {
                                        JSONArray multiDropArray = object.getJSONArray("additional_drop_loc");
                                        if (multiDropArray.length() > 0) {
                                            multipleDropList.clear();
                                            for (int i = 0; i < multiDropArray.length(); i++) {
                                                JSONObject multipleDropArrayObject = multiDropArray.getJSONObject(i);

                                                MultipleLocationPojo pojo = new MultipleLocationPojo();
                                                pojo.setDropLocation(multipleDropArrayObject.getString("location"));
                                                pojo.setIsEnd(multipleDropArrayObject.getString("is_end"));
                                                pojo.setMode(multipleDropArrayObject.getString("mode"));
                                                pojo.setRecordId(multipleDropArrayObject.getString("record_id"));

                                                JSONObject latLngArrayObject = multipleDropArrayObject.getJSONObject("latlong");
                                                if (latLngArrayObject.length() > 0) {
                                                    pojo.setDropLat(latLngArrayObject.getString("lat"));
                                                    pojo.setDropLon(latLngArrayObject.getString("lon"));
                                                }

                                                multipleDropList.add(pojo);
                                            }

                                            isMultipleDropAvailable = true;
                                            sMultipleDropStatus = "1";

                                        } else {
                                            multipleDropList.clear();
                                            sMultipleDropStatus = "0";
                                            isMultipleDropAvailable = false;
                                            sEndNavigationLat = Str_droplat;
                                            sEndNavigationLng = Str_droplon;
                                        }
                                    } else {
                                        multipleDropList.clear();
                                        sMultipleDropStatus = "0";
                                        isMultipleDropAvailable = false;
                                        sEndNavigationLat = Str_droplat;
                                        sEndNavigationLng = Str_droplon;
                                    }
                                } else {
                                    sMultipleDropStatus = "0";
                                    isMultipleDropAvailable = false;
                                    sEndNavigationLat = Str_droplat;
                                    sEndNavigationLng = Str_droplon;
                                }


                                for (int j = 0; j < multipleDropList.size(); j++) {

                                    System.out.println("------multipleDropList------------" + multipleDropList.get(j));

                                    if (multipleDropList.get(j).getIsEnd().equalsIgnoreCase("0")) {

                                        sEndNavigationLat = multipleDropList.get(j).getDropLat();
                                        sEndNavigationLng = multipleDropList.get(j).getDropLon();
                                        endTrip_NavigationLatLng = new LatLng(Double.parseDouble(sEndNavigationLat), Double.parseDouble(sEndNavigationLng));
                                        sRecordId = multipleDropList.get(j).getRecordId();

                                        Rl_multipleDrop.setBackgroundResource(R.drawable.multiple_drop_end);
                                        Tv_multipleDropState.setText(getResources().getString(R.string.trip_label_drop));
                                        multipleDropCount = 0;

                                        Rl_multipleDrop.setVisibility(View.VISIBLE);

                                        break;
                                    } else {
                                        sEndNavigationLat = Str_droplat;
                                        sEndNavigationLng = Str_droplon;
                                        endTrip_NavigationLatLng = new LatLng(Double.parseDouble(Str_droplat), Double.parseDouble(Str_droplon));
                                        Rl_multipleDrop.setVisibility(View.GONE);
                                    }
                                }

                            } else {
                                sEndNavigationLat = Str_droplat;
                                sEndNavigationLng = Str_droplon;
                                JSONObject res_object = object.getJSONObject("response");
                                need_payment = res_object.getString("need_payment");
                                rider_image = res_object.getString("user_image");

                            }


                            if (trip_status.equals("arrived")) {
                                Trip_Status = "begin";
                                pickup_latlong = new LatLng(MyCurrent_lat, MyCurrent_long);
                                Begin(drop_location, sCurrencySymbol + est_cost, ride_id, title_text, subtitle_text);
                            } else if (trip_status.equals("begin")) {

                                if (sTripMode.length() > 0) {
                                    if (sTripMode.equalsIgnoreCase("oneway")) {
                                        Iv_tripWayIcon.setImageResource(R.drawable.one_way_icon);
                                    } else if (sTripMode.equalsIgnoreCase("return")) {
                                        Iv_tripWayIcon.setImageResource(R.drawable.return_way_icon);
                                    }
                                }

                                Trip_Status = "end";
                                End(drop_location, sCurrencySymbol + est_cost, ride_id, title_text, subtitle_text);
                            } else if (trip_status.equals("end")) {
                                myDBHelper.insertDriverStatus("service_stop");
                                sessionManager.setUpdateLocationServiceState("");
                                /*if (need_payment.equals("YES")) {
                                    Intent intent = new Intent(TripPage.this, FarePage.class);
                                    intent.putExtra("rideId", Str_RideId);
                                    intent.putExtra("riderImage", rider_image);
                                    startActivity(intent);
                                    finish();
                                    overridePendingTransition(R.anim.enter, R.anim.exit);
                                } else {
                                    Intent intent = new Intent(TripPage.this, RatingsPage.class);
                                    intent.putExtra("RideID", Str_RideId);
                                    startActivity(intent);
                                    finish();
                                    overridePendingTransition(R.anim.enter, R.anim.exit);
                                }*/

                                Intent intent = new Intent(TripPage.this, FarePage.class);
                                intent.putExtra("rideId", Str_RideId);
                                startActivity(intent);
                                finish();
                                overridePendingTransition(R.anim.enter, R.anim.exit);

                            }
                        } else {
                            if (status.equalsIgnoreCase("0")) {
                                String sResponse = object.getString("response");
                                String ride_view1 = object.getString("ride_view");
                                if (ride_view1.equals("detail")) {
                                    sessionManager.setUpdateLocationServiceState("");
                                    alert_Cancel(getResources().getString(R.string.action_error), sResponse);
                                } else {
                                    alert(getResources().getString(R.string.action_error), sResponse);
                                }
                            }
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void calculateDistance(ArrayList<LatLng> points) {

        float tempTotalDistance = 0;

        for (int i = 0; i < points.size() - 1; i++) {
            LatLng pointA = points.get(i);
            LatLng pointB = points.get(i + 1);
            float[] results = new float[3];
            Location.distanceBetween(pointA.latitude, pointA.longitude, pointB.latitude, pointB.longitude, results);
            tempTotalDistance += results[0];
        }

        totalDistanceTravelled = tempTotalDistance;
    }

    private void arrived(String drop, String eta, String ride_id, String location, String member_since, String user_gender, String Str_username, String Str_user_img) {

        myDBHelper.insertDriverStatus("arrived");
//        arrived_layout.setVisibility(View.VISIBLE);
//        Trip_layout.setVisibility(View.INVISIBLE);
        Iv_tripWayIcon.setVisibility(View.GONE);
        /*tv_eta.setText(eta);
        tv_ride_id.setText(getResources().getString(R.string.trip_label_orderNo) + ride_id);
        tv_drop.setText(getResources().getString(R.string.trip_label_pickUp) + sPickUpAddress);
        tv_Trip_Button.setText(getResources().getString(R.string.trip_label_onCall));
        tv_cancel.setText(getResources().getString(R.string.trip_label_cancel));
        tv_cancel.setVisibility(View.VISIBLE);
        Picasso.with(TripPage.this).load(Str_user_img).into(userimg);
        tv_usergender.setText(user_gender);
        Tv_name.setText(Str_username);
        tv_member_since.setText(member_since);
        arrive_subtitle.setText(location);*/

        Txt_rider_name.setText(Str_username);
        Picasso.with(TripPage.this).load(Str_user_img).error(R.drawable.no_user_img).error(R.drawable.no_user_img).fit().into(Img_rider_profile_pic);

        Txt_bookingNo_or_driverName_or_tripStatus.setText(getResources().getString(R.string.trip_label_booking_no) + Str_RideId);
        Txt_pickupAddress_or_destinationAddress.setText(getResources().getString(R.string.trip_label_pickup_address).toUpperCase() + sPickUpAddress.replaceAll("\n", " "));
        Txt_paymentMethod_or_estimatedFare_or_oneWay_or_multipleStop.setText(getResources().getString(R.string.trip_label_payment_type) + sPaymentType);

        Txt_time.setText(eta);
        RL_rider_profile.setVisibility(View.VISIBLE);

        btn_ride.setText(getResources().getString(R.string.trip_page_label_arrive));
        btn_ride.setTextColor(Color.WHITE);
        btn_ride.setBackgroundResource(R.drawable.bg_arrive);

//        RL_profile.setVisibility(View.GONE);

        SetRouteMap(cur_latlong, pickup_latlong);
    }

    private void Begin(String drop, String eta, String ride_id, String title, String subtitle_text) {

        myDBHelper.insertDriverStatus("begin");
//        arrived_layout.setVisibility(View.INVISIBLE);
//        Trip_layout.setVisibility(View.VISIBLE);
        Iv_tripWayIcon.setVisibility(View.GONE);
//        tv_eta.setText(eta);
//        tv_ride_id.setText(getResources().getString(R.string.trip_label_orderNo) + ride_id);
//        tv_drop.setText(getResources().getString(R.string.trip_label_dropOff) + drop);
//        tv_Trip_Button.setText(getResources().getString(R.string.trip_label_tapToStart));
//        tv_cancel.setText(getResources().getString(R.string.trip_label_cancel));
//        tv_cancel.setVisibility(View.VISIBLE);
//        Tv_title.setText(title);
//        subtitle.setText(subtitle_text);

        RL_rider_profile.setVisibility(View.GONE);

        Txt_bookingNo_or_driverName_or_tripStatus.setText(driver_name);
        Txt_pickupAddress_or_destinationAddress.setText(getResources().getString(R.string.trip_label_destination_address).toUpperCase() + str_drop_location.replaceAll("\n", " "));
        Txt_paymentMethod_or_estimatedFare_or_oneWay_or_multipleStop.setText(getResources().getString(R.string.trip_label_estimated_fare) + eta);

        btn_ride.setText(getResources().getString(R.string.trip_label_start));
        btn_ride.setTextColor(Color.BLACK);
        btn_ride.setBackgroundResource(R.drawable.bg_start);

//        RL_profile.setVisibility(View.GONE);

        SetRouteMap(cur_latlong, drop_latlong);
    }

    private void End(String drop, String eta, String ride_id, String title, String subtitle_text) {

        myDBHelper.insertDriverStatus("end");
//        arrived_layout.setVisibility(View.INVISIBLE);
//        Trip_layout.setVisibility(View.GONE);


        RL_rider_profile.setVisibility(View.GONE);
        Txt_bookingNo_or_driverName_or_tripStatus.setText(driver_name);
        Txt_pickupAddress_or_destinationAddress.setText(getResources().getString(R.string.trip_label_destination_address).toUpperCase() + str_drop_location.replaceAll("\n", " "));

        Txt_cancel_or_trip_info.setText(getResources().getString(R.string.trip_page_label_trip_info));
        Txt_call_or_sos.setText(getResources().getString(R.string.trip_page_label_sos));

        btn_ride.setBackgroundResource(R.drawable.bg_finish);
        btn_ride.setTextColor(Color.WHITE);
        btn_ride.setText(getResources().getString(R.string.trip_page_label_finish));

        Img_cancel_or_trip_info.setImageResource(R.drawable.icon_trip_info);
        Img_call_or_sos.setImageResource(R.drawable.icon_sos);
        btn_navigate.setVisibility(View.VISIBLE);


        if (sTripMode.length() > 0) {
            if (sTripMode.equalsIgnoreCase("oneway")) {
//                Iv_tripWayIcon.setImageResource(R.drawable.one_way_icon);
                Txt_paymentMethod_or_estimatedFare_or_oneWay_or_multipleStop.setText(getResources().getString(R.string.trip_label_one_way));
            } else if (sTripMode.equalsIgnoreCase("return")) {
//                Iv_tripWayIcon.setImageResource(R.drawable.return_way_icon);
                Txt_paymentMethod_or_estimatedFare_or_oneWay_or_multipleStop.setText(getResources().getString(R.string.trip_label_return));
            } else if (sTripMode.equalsIgnoreCase("multistop")) {
                Txt_paymentMethod_or_estimatedFare_or_oneWay_or_multipleStop.setText(getResources().getString(R.string.trip_label_multiple_stop));
            }
        }

        if (sTripMode.equalsIgnoreCase("oneway")) {
//            Iv_tripWayIcon.setVisibility(View.VISIBLE);
            Rl_returnTrip.setVisibility(View.GONE);
            Txt_paymentMethod_or_estimatedFare_or_oneWay_or_multipleStop.setText(getResources().getString(R.string.trip_label_one_way));
        } else if (sTripMode.equalsIgnoreCase("return")) {
//            Iv_tripWayIcon.setVisibility(View.VISIBLE);
            Txt_paymentMethod_or_estimatedFare_or_oneWay_or_multipleStop.setText(getResources().getString(R.string.trip_label_return));

            System.out.println("-----------prem isReturnAvailable---------" + isReturnAvailable);
            if (isReturnAvailable.length() > 0) {
                if (isReturnAvailable.equalsIgnoreCase("0")) {


                    System.out.println("---------prem sReturnState-----------" + sReturnState);

                    Rl_returnTrip.setVisibility(View.VISIBLE);

                    if (sReturnState.equalsIgnoreCase("drop")) {
                        Tv_returnType.setText(getResources().getString(R.string.trip_label_drop));
                        Rl_returnTrip.setBackgroundResource(R.drawable.multiple_drop_start);
                    } else if (sReturnState.equalsIgnoreCase("start")) {
                        Tv_returnType.setText(getResources().getString(R.string.trip_label_return));
                        Rl_returnTrip.setBackgroundResource(R.drawable.multiple_drop_end);
                    }

                } else {
                    Rl_returnTrip.setVisibility(View.GONE);
                }
            } else {
                Rl_returnTrip.setVisibility(View.GONE);
            }

        } else {
            Iv_tripWayIcon.setVisibility(View.GONE);
            Rl_returnTrip.setVisibility(View.GONE);
        }


        /*tv_eta.setText(eta);
        tv_ride_id.setText(getResources().getString(R.string.trip_label_orderNo) + ride_id);
        tv_drop.setText(getResources().getString(R.string.trip_label_dropOff) + drop);
        tv_Trip_Button.setText(getResources().getString(R.string.trip_label_tapToFinish));
        tv_cancel.setText(getResources().getString(R.string.trip_label_sendReceipt));
        tv_cancel.setVisibility(View.GONE);
        Tv_title.setText(title);
        subtitle.setText(subtitle_text);*/

        /*if (sTripMode.equalsIgnoreCase("oneway")) {

            Txt_paymentMethod_or_estimatedFare_or_oneWay_or_multipleStop.setText("One Way");

        } else if (sTripMode.equalsIgnoreCase("return")) {

            Txt_paymentMethod_or_estimatedFare_or_oneWay_or_multipleStop.setText("Multiple Stop");

        } else {

        }*/

        if (isMultipleDropAvailable) {
            SetRouteMap(pickup_multiStop_latlong, endTrip_NavigationLatLng);
        } else if (sTripMode.equalsIgnoreCase("return")) {
            if (isReturnAvailable.equalsIgnoreCase("1")) {
                SetRouteMap(return_pickUp_latLong, return_latLong);
            } else {
                SetRouteMap(pickup_latlong, drop_latlong);
            }
        } else {
            SetRouteMap(pickup_latlong, drop_latlong);
        }

    }

    private void alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(TripPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void alert_Cancel(String title, String alert) {
        final PkDialog mDialog = new PkDialog(TripPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setCancelOnTouchOutside(false);


        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();


                Intent finish_GoOnlinePage = new Intent();
                finish_GoOnlinePage.setAction("com.app.finish.GoOnlinePage");
                sendBroadcast(finish_GoOnlinePage);

                Intent intent = new Intent(TripPage.this, GoOnlinePage.class);
                startActivity(intent);


                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);


            }
        });
        mDialog.show();
    }

    public class GetRouteTask extends AsyncTask<String, Void, String> {

        String response = "";
        LatLng from_LatLng, to_LatLng;
        private ArrayList<LatLng> wayLatLng;

        GetRouteTask(LatLng from, LatLng to) {
            from_LatLng = to;
            to_LatLng = from;
            startWayPoint = from;
            endWayPoint = to;
            wayLatLng = addWayPointPoint(from, to, multipleDropList);
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... urls) {

            System.out.println("-------prem wayLatLng---------" + wayLatLng);

            Routing routing = new Routing.Builder()
                    .travelMode(AbstractRouting.TravelMode.DRIVING)
                    .withListener(TripPage.this)
                    .alternativeRoutes(true)
                    .key(Iconstant.place_search_key)
                    .waypoints(wayLatLng)
                    .build();
            routing.execute();


            response = "Success";
            return response;

        }

        @Override
        protected void onPostExecute(String result) {
            if (result.equalsIgnoreCase("Success")) {
            }
        }
    }


    //Enabling Gps Service
    private void enableGpsService() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final com.google.android.gms.common.api.Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        //...
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(TripPage.this, REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });
    }


    @Override
    public void onConnected(Bundle bundle) {

        System.out.println("on---onConnected----------jai-");

        myLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

    }

    @Override
    public void onConnectionSuspended(int i) {
        System.out.println("on---onConnectionSuspended----------jai-");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        System.out.println("on---onConnectionFailed----------jai-");
    }

    @Override
    public void onLocationChanged(final Location location) {

        System.out.println("on---locationchange----------jai-");

//        addPulsatingEffect(new LatLng(location.getLatitude(), location.getLongitude()), location.getAccuracy());

        this.myLocation = location;
        if (myLocation != null) {

            try {
                LatLng latLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
                cur_latlong = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());

                MyCurrent_lat = myLocation.getLatitude();
                MyCurrent_long = myLocation.getLongitude();

                if (oldLatLng == null) {
                    oldLatLng = latLng;
                }
                newLatLng = latLng;

                if (mLatLngInterpolator == null) {
                    mLatLngInterpolator = new LatLngInterpolator.Linear();
                }

                oldLocation = new Location("");
                oldLocation.setLatitude(oldLatLng.latitude);
                oldLocation.setLongitude(oldLatLng.longitude);
                float bearingValue = oldLocation.bearingTo(location);

                myMovingDistance = oldLocation.distanceTo(location);

                //
                if (myMovingDistance > 10) {

                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            updateEta(oldLocation, location);
                        }
                    });


                }

                System.out.println("moving distance------------" + myMovingDistance);
                System.out.println("moving Trip_Status------------" + Trip_Status);
                if (Trip_Status.equals("end")) {

                    /*code to visible Trip layout after 300m*/
                    Location locationA = new Location("point A");
                    locationA.setLatitude(location.getLatitude());
                    locationA.setLongitude(location.getLongitude());
                    Location locationB = new Location("point B");

                    if (sTripMode.equalsIgnoreCase("return")) {
                        if (isReturnAvailable.equalsIgnoreCase("1")) {
                            SetRouteMap(return_pickUp_latLong, return_latLong);
                            locationB.setLatitude(Double.parseDouble(sReturnLat));
                            locationB.setLongitude(Double.parseDouble(sReturnLng));
                        } else {
                            locationB.setLatitude(Double.parseDouble(Str_droplat));
                            locationB.setLongitude(Double.parseDouble(Str_droplon));
                        }
                    } else {
                        locationB.setLatitude(Double.parseDouble(Str_droplat));
                        locationB.setLongitude(Double.parseDouble(Str_droplon));
                    }

                    float distance = locationA.distanceTo(locationB);
                    /*if (distance <= 300) {
                        Trip_layout.setVisibility(View.VISIBLE);
                    } else {
                        Trip_layout.setVisibility(View.GONE);
                    }*/

                }

                if (googleMap != null) {

                    if (myMovingDistance > 1) {
                        if (currentDriverMarker != null) {
                            if (!String.valueOf(bearingValue).equalsIgnoreCase("NaN")) {
                                if (location.getAccuracy() < 100.0 && location.getSpeed() < 6.95) {
                                    rotateMarker(currentDriverMarker, bearingValue, googleMap);
                                    MarkerAnimation.animateMarkerToGB(currentDriverMarker, latLng, mLatLngInterpolator);
                                    float zoom = googleMap.getCameraPosition().zoom;
                                    CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(zoom).build();
                                    googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                                }
                            }
                        } else {
                            currentDriverMarker.remove();
                            currentDriverMarker = googleMap.addMarker(new MarkerOptions()
                                    .position(latLng)
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.carmove))
                                    .anchor(0.5f, 0.5f)
                                    .rotation(myLocation.getBearing())
                                    .flat(true));
                        }
                    }
                }

                oldLatLng = newLatLng;
            } catch (Exception e) {
            }

        }
    }

    //Method to smooth turn marker
    static public void rotateMarker(final Marker marker, final float toRotation, GoogleMap map) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final float startRotation = marker.getRotation();
        final long duration = 1555;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed / duration);

                float rot = t * toRotation + (1 - t) * startRotation;

                marker.setRotation(-rot > 180 ? rot / 2 : rot);
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
            }
        });
    }


    private ArrayList<LatLng> addWayPointPoint(LatLng start, LatLng end, ArrayList<MultipleLocationPojo> mMultipleDropLatLng) {

        if (googleMap != null) {
            googleMap.clear();
            wayPointList.clear();

            wayPointBuilder = new LatLngBounds.Builder();

            wayPointBuilder.include(start);
            wayPointList.add(start);

            if (mMultipleDropLatLng != null) {
                if (isMultipleDropAvailable) {
                    for (int i = 0; i < mMultipleDropLatLng.size(); i++) {

                        String sLat = mMultipleDropLatLng.get(i).getDropLat();
                        String sLng = mMultipleDropLatLng.get(i).getDropLon();

                        double Dlatitude = Double.parseDouble(sLat);
                        double Dlongitude = Double.parseDouble(sLng);

                        // wayPointList.add(new LatLng(Dlatitude, Dlongitude));
                        MarkerOptions marker = new MarkerOptions().position(new LatLng(Dlatitude, Dlongitude));
                        marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.icn_flag));

                        // wayPointBuilder.include(new LatLng(Dlatitude, Dlongitude));
                        googleMap.addMarker(marker);
                    }
                }
            }


            wayPointBuilder.include(end);
            wayPointList.add(end);
        }


        return wayPointList;
    }


    @Override
    public void onRoutingFailure(RouteException e) {
        System.out.println("-----------prem onRoutingFailure-----------------" + e);
    }

    @Override
    public void onRoutingStart() {

    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int i) {

        if (polyLines.size() > 0) {
            for (Polyline poly : polyLines) {
                poly.remove();
            }
        }

        polyLines = new ArrayList<>();

        PolylineOptions polyOptions = new PolylineOptions();
        polyOptions.color(getResources().getColor(R.color.app_secondary_color));
        polyOptions.width(10);
        polyOptions.addAll(route.get(0).getPoints());
        Polyline polyline = googleMap.addPolyline(polyOptions);
        polyLines.add(polyline);


        System.out.println("------------Trip_Status------------------" + Trip_Status);
        System.out.println("------------sTripMode------------------" + sTripMode);
        System.out.println("------------isReturnAvailable------------------" + isReturnAvailable);

        if (Trip_Status.equals("end")) {

            if (sTripMode.equalsIgnoreCase("oneway")) {
                googleMap.addMarker(new MarkerOptions()
                        .position(pickup_latlong)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.icn_location)));
                googleMap.addMarker(new MarkerOptions()
                        .position(drop_latlong)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.icn_flag)));
            } else if (sTripMode.equalsIgnoreCase("return")) {

                if (isReturnAvailable.equalsIgnoreCase("1")) {
                    googleMap.addMarker(new MarkerOptions()
                            .position(return_pickUp_latLong)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.icn_location)));
                    googleMap.addMarker(new MarkerOptions()
                            .position(return_latLong)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.icn_flag)));
                } else {
                    googleMap.addMarker(new MarkerOptions()
                            .position(pickup_latlong)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.icn_location)));
                    googleMap.addMarker(new MarkerOptions()
                            .position(drop_latlong)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.icn_flag)));
                }

            } else if (sTripMode.equalsIgnoreCase("multistop")) {
                googleMap.addMarker(new MarkerOptions()
                        .position(pickup_latlong)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.icn_location)));
                googleMap.addMarker(new MarkerOptions()
                        .position(drop_latlong)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.icn_flag)));
            }
        } else if (Trip_Status.equals("arrived")) {
            googleMap.addMarker(new MarkerOptions()
                    .position(endWayPoint)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.icn_location)));
        } else {
            googleMap.addMarker(new MarkerOptions()
                    .position(pickup_latlong)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.icn_location)));
            googleMap.addMarker(new MarkerOptions()
                    .position(endWayPoint)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.icn_flag)));
        }

        currentDriverMarker = googleMap.addMarker(new MarkerOptions()
                .position(cur_latlong)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carmove)));


//            LatLngBounds bounds = wayPointBuilder.build();
//            int padding = 90; // offset from edges of the map in pixels
//            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
//            googleMap.animateCamera(cu);
            if (wayPointBuilder != null) {
//                 bounds = wayPointBuilder.build();
//                int padding = 30; // offset from edges of the map in pixels
//                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
////            googleMap_confirm.moveCamera(cu);
//                googleMap.moveCamera(cu);
                int padding, paddingH, paddingW;
                LatLngBounds bounds = wayPointBuilder.build();

//                if (mapFragment == null) {
//                    mapFragment = ((MapFragment) getFragmentManager().findFragmentById(
//                            R.id.tripPage_googleMap));
//                }

                final View mapview = mapFragment.getView();
                float maxX = mapview.getMeasuredWidth();
                float maxY = mapview.getMeasuredHeight();

                DisplayMetrics displayMetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                int height = displayMetrics.heightPixels;
                int width = displayMetrics.widthPixels;

                System.out.println("***************************height and width" + height + "," + width);

                float percentageH = 50.0f;
                float percentageW = 80.0f;
                paddingH = (int) (maxY * (percentageH / 100.0f));
                paddingW = (int) (maxX * (percentageW / 100.0f));


                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, paddingW, paddingH, 100);
                googleMap.animateCamera(cu);
            }

    }

    @Override
    public void onRoutingCancelled() {

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_LOCATION:
                switch (resultCode) {
                    case Activity.RESULT_OK: {
                        Toast.makeText(TripPage.this, "Location enabled!", Toast.LENGTH_LONG).show();

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                gps = new GPSTracker(TripPage.this);
                                if (gps != null && gps.canGetLocation()) {
                                    double Dlatitude = gps.getLatitude();
                                    double Dlongitude = gps.getLongitude();
                                    MyCurrent_lat = Dlatitude;
                                    MyCurrent_long = Dlongitude;

                                    System.out.println("currntlat----------" + MyCurrent_lat);
                                    System.out.println("currntlon----------" + MyCurrent_long);

                                    if (!String.valueOf(MyCurrent_lat).equals(0.0) && !String.valueOf(MyCurrent_long).equals(0.0)) {
                                        if (googleMap != null) {
                                            LatLng latLng = new LatLng(MyCurrent_lat, MyCurrent_long);

                                            cur_latlong = new LatLng(MyCurrent_lat, MyCurrent_long);

                                            currentDriverMarker = googleMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.carmove)));
                                            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(13).build();
                                            CameraUpdate camUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
                                            googleMap.moveCamera(camUpdate);

                                            if (Trip_Status.equals("arrived")) {
//                                                arrived_layout.setVisibility(View.VISIBLE);
//                                                Trip_layout.setVisibility(View.INVISIBLE);
                                                arrived(sPickUpAddress, eta_arrival, Str_RideId, location, member_since, user_gender, Str_username, Str_user_img);
                                            } else if (Trip_Status.equals("begin")) {
//                                                arrived_layout.setVisibility(View.INVISIBLE);
//                                                Trip_layout.setVisibility(View.VISIBLE);
                                                Begin(str_drop_location, est_cost + sCurrencySymbol, Str_RideId, title_text, subtitle_text);
                                            } else {
//                                                arrived_layout.setVisibility(View.INVISIBLE);
//                                                Trip_layout.setVisibility(View.GONE);
                                                End(str_drop_location, est_cost + sCurrencySymbol, Str_RideId, title_text, subtitle_text);
                                            }

                                        }
                                    }
                                }
                            }
                        }, 2000);

                        break;
                    }
                    case Activity.RESULT_CANCELED: {
                        finish();
                        break;
                    }
                    default: {
                        break;
                    }
                }
                break;


            case OVERLAY_PERMISSION_REQ_CODE:

                if (resultCode == Activity.RESULT_OK) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (!Settings.canDrawOverlays(TripPage.this)) {
                            Toast.makeText(TripPage.this, "SYSTEM_ALERT_WINDOW permission not granted...", Toast.LENGTH_SHORT).show();
                        } else {
                            moveNavigation();
                        }
                    }
                }

                break;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_NAVIGATION_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    checkNavigation();

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + Str_user_phoneno));
                    startActivity(callIntent);
                }
                break;
            case PERMISSION_REQUEST_CODE_SOS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + sContactNumber));
                    startActivity(callIntent);
                }
                break;
        }
    }

    private void requestPermission() {
    }

    private void requestPermissionSOS() {
    }

    private boolean checkCallPhonePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkReadStatePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(finishReceiver);

        if (travelTimeHandler != null) {
            travelTimeHandler.removeCallbacks(travelTimeRunnable);
        }

        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            //Do nothing
            return true;
        }
        return false;
    }


    private void updateEta(Location currentLocation, Location destLocation) {

        String url = Iconstant.GetDistanceAndTime_url + "&origins=" + currentLocation.getLatitude() + "," + currentLocation.getLongitude()
                + "&destinations=" + destLocation.getLatitude() + "," + destLocation.getLongitude();

        getDistanceAndTimeRequest(url);

    }

    private void getDistanceAndTimeRequest(String url) {

        ServiceRequest request = new ServiceRequest(TripPage.this);
        request.makeServiceRequest(url, Request.Method.GET, null, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                getDistanceAndTime(response);

                if (Txt_time != null) {
                    if (Str_TimeTo.length() > 0) {
                        Txt_time.setText(Str_TimeTo);
                    }
                }

                if (Txt_distance != null) {
                    if (Str_DistanceTo.length() > 0) {
                        Txt_distance.setText(Str_DistanceTo);
                    }
                }

//                travelTimeHandler.postDelayed(travelTimeRunnable, 1 * 1000);

                /*if(mapDistanceAndTime.get("distance")!=null){

                }

                if(mapDistanceAndTime.get("duration")!=null){

                }*/

            }

            @Override
            public void onErrorListener() {

            }
        });


    }


    private void getDistanceAndTime(String result) {

//        HashMap<String, String> mapDistanceAndTime = new HashMap<String , String>();

        try {

            JSONObject jobjResult = new JSONObject(result);

            Object objRows = jobjResult.get("rows");

            if (objRows instanceof JSONArray) {

                JSONArray jarRows = jobjResult.getJSONArray("rows");

                if (jarRows.length() > 0) {

                    Object objSubResult = jarRows.get(0);

                    if (objSubResult instanceof JSONObject) {

                        JSONObject jobjSubResult = jarRows.getJSONObject(0);

                        if (jobjSubResult.length() > 0) {

                            if (jobjSubResult.has("elements")) {

                                Object objElements = jobjSubResult.get("elements");

                                if (objElements instanceof JSONArray) {

                                    JSONArray jarElements = jobjSubResult.getJSONArray("elements");

                                    if (jarElements.length() > 0) {

                                        Object objFinalResult = jarElements.get(0);

                                        if (objFinalResult instanceof JSONObject) {

                                            JSONObject jobjFinalResult = jarElements.getJSONObject(0);

                                            if (jobjFinalResult.length() > 0) {

                                                if (jobjFinalResult.has("distance")) {

                                                    Object objDistance = jobjFinalResult.get("distance");

                                                    if (objDistance instanceof JSONObject) {
                                                        JSONObject jobjDistance = jobjFinalResult.getJSONObject("distance");

                                                        if (jobjDistance.length() > 0) {

                                                            if (jobjDistance.has("text")) {
                                                                Str_DistanceTo = jobjDistance.getString("text");
//                                                                mapDistanceAndTime.put("distance", distance);
                                                            }

                                                            if (jobjDistance.has("value")) {

                                                            }

                                                        }

                                                    }

                                                }

                                                if (jobjFinalResult.has("duration")) {

                                                    Object objDuration = jobjFinalResult.get("duration");

                                                    if (objDuration instanceof JSONObject) {
                                                        JSONObject jobjDuration = jobjFinalResult.getJSONObject("duration");

                                                        if (jobjDuration.length() > 0) {

                                                            if (jobjDuration.has("text")) {
                                                                Str_TimeTo = jobjDuration.getString("text");
//                                                                mapDistanceAndTime.put("duration", duration);
                                                            }

                                                            if (jobjDuration.has("value")) {

                                                            }

                                                        }

                                                    }

                                                }

                                            }

                                        }

                                    }

                                }

                            }

                        }

                    }

                }

            }


        } catch (Exception e) {
            e.printStackTrace();
        }

//        return mapDistanceAndTime;

    }

    private void SOSRequest(String url) {
        dialog = new Dialog(TripPage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_processing));

        HashMap<String, String> jsonParams = new HashMap<>();
        jsonParams.put("driver_id", driver_id);
        jsonParams.put("device", "ANDROID");

        System.out.println("-------------SOSRequest Url----------------" + url);
        System.out.println("-------------SOSRequest jsonParams----------------" + jsonParams);

        mRequest = new ServiceRequest(TripPage.this);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------SOSRequest Response----------------" + response);

                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }

                String Sstatus = "", sResponse = "";
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    sResponse = object.getString("response");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        alert(getResources().getString(R.string.action_success), sResponse);
                    } else {
                        showSOSNoRecordAlert();
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });

    }

    private void showSOSAlert() {

        final Dialog sosDialog = new Dialog(TripPage.this, R.style.SlideUpDialog);
        sosDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        sosDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        sosDialog.setCancelable(true);
        sosDialog.setContentView(R.layout.sos_popup1);

        final TextView Tv_confirm = (TextView) sosDialog.findViewById(R.id.txt_confirm);
        final ImageView Iv_close = (ImageView) sosDialog.findViewById(R.id.img_close);

        Tv_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (sosDialog != null && sosDialog.isShowing()) {
                    sosDialog.dismiss();
                }

                if (cd.isConnectingToInternet()) {
                    SOSRequest(Iconstant.sos_url);
                } else {
                    alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                }

            }
        });

        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sosDialog != null && sosDialog.isShowing()) {
                    sosDialog.dismiss();
                }
            }
        });

        sosDialog.show();

    }

    private void showSOSNoRecordAlert() {

        final Dialog sosNoRecordDialog = new Dialog(TripPage.this, R.style.SlideUpDialog);
        sosNoRecordDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        sosNoRecordDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        sosNoRecordDialog.setCancelable(true);
        sosNoRecordDialog.setContentView(R.layout.sos_popup2);

        final TextView Tv_mobileNumber = (TextView) sosNoRecordDialog.findViewById(R.id.tv_callassist);
        final ImageView Iv_close = (ImageView) sosNoRecordDialog.findViewById(R.id.img_close);
        final ImageView Iv_call = (ImageView) sosNoRecordDialog.findViewById(R.id.call_assistance);

        Iv_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (sosNoRecordDialog != null && sosNoRecordDialog.isShowing()) {
                    sosNoRecordDialog.dismiss();
                }

                callCustomerCare();

            }
        });

        Tv_mobileNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (sosNoRecordDialog != null && sosNoRecordDialog.isShowing()) {
                    sosNoRecordDialog.dismiss();
                }

                callCustomerCare();

            }
        });

        Tv_mobileNumber.setText(sContactNumber);


        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sosNoRecordDialog != null && sosNoRecordDialog.isShowing()) {
                    sosNoRecordDialog.dismiss();
                }
            }
        });

        sosNoRecordDialog.show();

    }


    private void callCustomerCare() {

        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+

            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + sContactNumber));
            startActivity(intent);

        } else {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + sContactNumber));
            startActivity(intent);
        }

    }

}