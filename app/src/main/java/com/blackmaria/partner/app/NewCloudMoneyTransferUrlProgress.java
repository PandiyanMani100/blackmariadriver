package com.blackmaria.partner.app;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.CurrencySymbolConverter;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;

public class NewCloudMoneyTransferUrlProgress extends ActivityHockeyApp {
    private SessionManager session;
    private boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    private String payAmount = "", reloadAmount = "", tokenId = "", flag = "", UserID = "", bankcode = "", friendId = "", authenticationId = "";
    //    private Dialog dialog;
    private String radeemNumber = "", accHolderName1 = "", bankCode = "", holderAccountNumber1 = "", paymentMod = "", withdrawlAmount = "", crosstransfer = "", paypal_amount = "", paypal_paymentcode = "", paypal_id = "";
    private View cashoutview;
    String transId = "", from = "", date = "", currencyCode = "", time = "", Amount = "", currency = "";
    private SmoothProgressBar timerpage_loading_progressbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_cloud_transfer_url_progress);
        initialize();
    }

    private void initialize() {

        session = new SessionManager(NewCloudMoneyTransferUrlProgress.this);
        cd = new ConnectionDetector(NewCloudMoneyTransferUrlProgress.this);
        isInternetPresent = cd.isConnectingToInternet();
        timerpage_loading_progressbar = (SmoothProgressBar) findViewById(R.id.timerpage_loading_progressbar);
        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_DRIVERID);
        cashoutview = (View) findViewById(R.id.cashoutview);

        new DownloadFilesTask().execute();


    }


    private class DownloadFilesTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            timerpage_loading_progressbar.setVisibility(View.VISIBLE);
        }


        protected String doInBackground(String... urls) {
            Intent reloadIn = getIntent();
            radeemNumber = session.REDEEM_CODE;//session.getRedeemCouponcode();
            flag = reloadIn.getStringExtra("flag");
            if (flag.equalsIgnoreCase("2")) {

                if (reloadIn.hasExtra("authentication_id")) {
                    authenticationId = reloadIn.getStringExtra("authentication_id");
                }

                reloadAmount = reloadIn.getStringExtra("total_amount");
                payAmount = reloadIn.getStringExtra("pay_amount");
                tokenId = reloadIn.getStringExtra("token_id");
            } else if (flag.equalsIgnoreCase("3")) {
                reloadAmount = reloadIn.getStringExtra("reloadamount");
                bankcode = reloadIn.getStringExtra("bankcode");
            } else if (flag.equalsIgnoreCase("0")) {

                reloadAmount = reloadIn.getStringExtra("reloadamount");
                friendId = reloadIn.getStringExtra("friendId");
                crosstransfer = reloadIn.getStringExtra("crosstransfer");

            } else if (flag.equalsIgnoreCase("XenditbankTransfer")) {
                withdrawlAmount = reloadIn.getStringExtra("amount");
                paymentMod = reloadIn.getStringExtra("mode");

                if (reloadIn.hasExtra("accHolderName1")) {
                    accHolderName1 = reloadIn.getStringExtra("accHolderName1");
                    holderAccountNumber1 = reloadIn.getStringExtra("holderAccountNumber1");
                    bankCode = reloadIn.getStringExtra("bankCode");

                }


            } else if (flag.equalsIgnoreCase("paypal_withdrawal")) {
                paypal_amount = reloadIn.getStringExtra("amount");
                paypal_paymentcode = reloadIn.getStringExtra("payment_code");
                paypal_id = reloadIn.getStringExtra("paypal_id");

            } else if (flag.equalsIgnoreCase("cash_withdrawal")) {
                paypal_amount = reloadIn.getStringExtra("amount");
                paypal_paymentcode = reloadIn.getStringExtra("payment_code");
                cashoutview.setVisibility(View.VISIBLE);
            }
            if (flag.equalsIgnoreCase("1")) {


                transId = reloadIn.getStringExtra("transId");
                from = reloadIn.getStringExtra("from");
                date = reloadIn.getStringExtra("date").replace("%20", " ");
                time = reloadIn.getStringExtra("time").replace("%20", " ");
                Amount = reloadIn.getStringExtra("amount");
                currency = reloadIn.getStringExtra("currency");
                Intent intent1 = new Intent(NewCloudMoneyTransferUrlProgress.this, ReloadXenditPaymentSuccessActivity.class);
                intent1.putExtra("amount", Amount);
                intent1.putExtra("ref_no", transId);
                intent1.putExtra("currency", currency);
                intent1.putExtra("from", from);
                intent1.putExtra("date", date);
                intent1.putExtra("time", time);
                intent1.putExtra("flag", "paypalpayment");
                startActivity(intent1);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
                session.createWalletAmount(currency + " " + Amount);

            } else if (flag.equalsIgnoreCase("2")) {
                if (isInternetPresent) {
                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    jsonParams.put("driver_id", UserID);
                    jsonParams.put("pay_amount", payAmount);
                    jsonParams.put("total_amount", reloadAmount);
                    jsonParams.put("token_id", tokenId);
                    jsonParams.put("redeem_code", radeemNumber);
                    if (authenticationId.length() > 0) {
                        jsonParams.put("authentication_id", authenticationId);
                    }
                    xenditcardMethod(Iconstant.xendit_card_pay_url, jsonParams);
                } else {
                    Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
                }

            } else if (flag.equalsIgnoreCase("3")) {

                if (isInternetPresent) {

                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    jsonParams.put("driver_id", UserID);
                    jsonParams.put("total_amount", reloadAmount);
                    jsonParams.put("bank_code", bankcode);
                    xenditBankMethod(Iconstant.xendit_bank_pay_url, jsonParams);
                    jsonParams.put("redeem_code", radeemNumber);
                } else {
                    Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
                }

            } else if (flag.equalsIgnoreCase("0")) {

                if (isInternetPresent) {
                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    jsonParams.put("driver_id", UserID);
                    jsonParams.put("transfer_amount", reloadAmount);
                    jsonParams.put("friend_id", friendId);
                    jsonParams.put("cross_transfer", crosstransfer);
                    transferToFriendAccount(Iconstant.driver_wallet_to_wallet_transfer_url, jsonParams);
                } else {
                    Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
                }
            } else if (flag.equalsIgnoreCase("XenditbankTransfer")) {
                if (isInternetPresent) {
                    PostRquestForConfirm(Iconstant.wallet_withdraw_amount_url);
                } else {
                    Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
                }
            } else if (flag.equalsIgnoreCase("paypal_withdrawal")) {

                HashMap<String, String> jsonParams = new HashMap<String, String>();
                jsonParams.put("driver_id", UserID);
                jsonParams.put("amount", paypal_amount);
                jsonParams.put("mode", paypal_paymentcode);
                jsonParams.put("paypal_id", paypal_id);
                System.out.println("------------CloudMoney withdraw jsonParams--------------" + jsonParams);

                PostRquestForConfirm(Iconstant.wallet_withdraw_amount_url, jsonParams);


            } else if (flag.equalsIgnoreCase("cash_withdrawal")) {


                HashMap<String, String> jsonParams = new HashMap<String, String>();
                jsonParams.put("driver_id", UserID);
                jsonParams.put("amount", paypal_amount);
                jsonParams.put("mode", paypal_paymentcode);
                System.out.println("------------CloudMoney withdraw jsonParams--------------" + jsonParams);

                PostRquestForConfirm(Iconstant.wallet_withdraw_amount_url, jsonParams);


            }
            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


        }


    }


    private void transferToFriendAccount(String Url, HashMap<String, String> jsonParams) {


        System.out.println("--------------transferToFriendAccount Url-------------------" + Url);
        System.out.println("--------------transferToFriendAccount jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(NewCloudMoneyTransferUrlProgress.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {


                System.out.println("--------------transferToFriendAccount Response-------------------" + response);
                String status = "", sResponse = "", wallet_amount = "", user_name = "", user_image = "", city = "",
                        transfer_amount = "", transaction_number = "", currency = "", user_status = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");
                        sResponse = object.getString("response");
                        if (status.equalsIgnoreCase("1")) {
                            user_name = object.getString("driver_name");
                            user_image = object.getString("user_image");
                            city = object.getString("city");
                            user_status = object.getString("user_status");
                            transfer_amount = object.getString("transfer_amount");
                            transaction_number = object.getString("transaction_number");
                            currency = object.getString("currency");
                        }
                        if (status.equalsIgnoreCase("1")) {


                            Intent broadcastIntent = new Intent();
                            broadcastIntent.setAction("com.package.ACTION_CLASS_CABILY_MONEY_REFRESH");
                            sendBroadcast(broadcastIntent);

                            Intent intent1 = new Intent(NewCloudMoneyTransferUrlProgress.this, NewCloudMoneyTransferUrlResponse.class);
                            intent1.putExtra("reloadamount", reloadAmount);
                            intent1.putExtra("user_name", user_name);
                            intent1.putExtra("user_image", user_image);
                            intent1.putExtra("city", city);
                            intent1.putExtra("transaction_number", transaction_number);
                            intent1.putExtra("currency", currency);
                            intent1.putExtra("status", user_status);
                            intent1.putExtra("crosstransfer", crosstransfer);

                            startActivity(intent1);
                            overridePendingTransition(R.anim.enter, R.anim.exit);
                            finish();

                        } else {
                            Alert1(getResources().getString(R.string.action_error), sResponse);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                /*if (dialog != null) {
                    dialog.dismiss();
                }*/
            }

            @Override
            public void onErrorListener() {
                /*if (dialog != null) {
                    dialog.dismiss();
                }*/
            }
        });


    }

    @SuppressLint("WrongConstant")
    private void xenditcardMethod(String Url, HashMap<String, String> jsonParams) {

        /*dialog = new Dialog(NewCloudMoneyTransferUrlProgress.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));*/


        System.out.println("--------------xenditcardMethod Url-------------------" + Url);
        System.out.println("--------------xenditcardMethod jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(NewCloudMoneyTransferUrlProgress.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {


                System.out.println("--------------ForgotPinRequest Response-------------------" + response);
                String status = "", sResponse = "", wallet_amount = "", currency = "", transaction_id = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");
                        sResponse = object.getString("msg");


                        if (status.equalsIgnoreCase("1")) {
                            if (object.has("recharge_amount")) {
                                wallet_amount = object.getString("recharge_amount");
                            }
                            date = object.getString("date");
                            time = object.getString("time");
                            currency = object.getString("currency_code");
                            transaction_id = object.getString("transaction_no");
                            Intent broadcastIntent = new Intent();
                            broadcastIntent.setAction("com.package.ACTION_CLASS_CABILY_MONEY_REFRESH");
                            sendBroadcast(broadcastIntent);

                            Intent intent1 = new Intent(NewCloudMoneyTransferUrlProgress.this, ReloadXenditPaymentSuccessActivity.class);
                            intent1.putExtra("date", date);
                            intent1.putExtra("time", time);
                            intent1.putExtra("amount", wallet_amount);
                            intent1.putExtra("ref_no", transaction_id);
                            intent1.putExtra("currency", currency);
                            intent1.putExtra("flag", "xenditReaload");
                            startActivity(intent1);
                            overridePendingTransition(R.anim.enter, R.anim.exit);
                            finish();   // Alert(getResources().getString(R.string.action_success), sResponse);
                        } else {
                            Alert1(getResources().getString(R.string.action_error), sResponse);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                /*if (dialog != null) {
                    dialog.dismiss();
                }*/
            }

            @Override
            public void onErrorListener() {
                /*if (dialog != null) {
                    dialog.dismiss();
                }*/
            }
        });
    }

    @SuppressLint("WrongConstant")
    private void xenditBankMethod(String Url, HashMap<String, String> jsonParams) {

        /*dialog = new Dialog(NewCloudMoneyTransferUrlProgress.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));*/


        System.out.println("--------------xenditBankMethod Url-------------------" + Url);
        System.out.println("--------------xenditBankMethod jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(NewCloudMoneyTransferUrlProgress.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {


                System.out.println("--------------ForgotPinRequest Response-------------------" + response);
                String status = "", sResponse = "", wallet_amount = "", payment_code = "", currency = "", transaction_id = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");
                        sResponse = object.getString("msg");
                        if (object.has("payment_code")) {
                            payment_code = object.getString("payment_code");
                        }
                        currency = object.getString("currency_code");
                        transaction_id = object.getString("transaction_no");
                        date = object.getString("date");
                        time = object.getString("time");
                        if (object.has("wallet_amount")) {
                            wallet_amount = object.getString("amount");
                        }
                        if (status.equalsIgnoreCase("1")) {
                            Intent intent1 = new Intent(NewCloudMoneyTransferUrlProgress.this, ReloadXenditPaymentSuccessActivity.class);

                            intent1.putExtra("date", date);
                            intent1.putExtra("time", time);
                            intent1.putExtra("amount", reloadAmount);
                            intent1.putExtra("ref_no", transaction_id);
                            intent1.putExtra("currency", currency);
                            intent1.putExtra("xenditPaymentId", payment_code);
                            intent1.putExtra("flag", "xenditBankReaload");
                            startActivity(intent1);

                            overridePendingTransition(R.anim.enter, R.anim.exit);
                            finish();

                            // Alert(getResources().getString(R.string.action_success), sResponse);
                        } else {
                            Alert1(getResources().getString(R.string.action_error), sResponse);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                /*if (dialog != null) {
                    dialog.dismiss();
                }*/
            }

            @Override
            public void onErrorListener() {
                /*if (dialog != null) {
                    dialog.dismiss();
                }*/
            }
        });
    }



    private void Alert1(String title, String alert) {
        final PkDialog mDialog = new PkDialog(NewCloudMoneyTransferUrlProgress.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewCloudMoneyTransferUrlProgress.this, WalletMoneyPage1.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();


                mDialog.dismiss();

            }
        });
        mDialog.show();
    }

    private void PostRquestForConfirm(String Url) {

        System.out.println("-----------E Wallet confirm url--------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", UserID);
        jsonParams.put("amount", withdrawlAmount);
        jsonParams.put("mode", paymentMod);
        jsonParams.put("bank_code", bankCode);
        jsonParams.put("acc_holder_name", accHolderName1);
        jsonParams.put("acc_number", holderAccountNumber1);

        System.out.println("------------E Wallet confirm jsonParams--------------" + jsonParams);


        mRequest = new ServiceRequest(NewCloudMoneyTransferUrlProgress.this);

        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {

                System.out.println("------------E Wallet confirm response--------------" + response);

                String Ssatus = "", ref_no = "", currency = "", amount = "", mode = "", pay_to = "", account = "";

                try {

                    JSONObject object = new JSONObject(response);
                    Ssatus = object.getString("status");

                    if (Ssatus.equalsIgnoreCase("1")) {

                        ref_no = object.getString("ref_no");
                        currency = object.getString("currency");
                        amount = object.getString("amount");
                        mode = object.getString("mode");
                        pay_to = object.getString("pay_to");
                        account = object.getString("account");
                        Intent intent1 = new Intent(NewCloudMoneyTransferUrlProgress.this, PaymentSuccessInfoActivity.class);

                        intent1.putExtra("account", account);
                        intent1.putExtra("mode", pay_to);
                        intent1.putExtra("amount", amount);
                        intent1.putExtra("ref_no", ref_no);
                        intent1.putExtra("currency", currency);
                        intent1.putExtra("flag", "XenditbankTransfer");
                        startActivity(intent1);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                        finish();


                    } else {
                        String Sresponse = object.getString("response");
                        Alert1("SORRY! EXCESS LIMIT", "Please lower your amount");
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onErrorListener() {


            }
        });


    }
    // ---------------------PAYPAL WITHDRAEA--------------------------

    private void PostRquestForConfirm(String Url, final HashMap<String, String> jsonParams) {


        System.out.println("-----------CloudMoneypaypal withdraw url--------------" + Url);

        mRequest = new ServiceRequest(NewCloudMoneyTransferUrlProgress.this);

        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {

                System.out.println("------------CloudMoney withdraw response--------------" + response);

                String Ssatus = "", mode = "", ref_no = "", amount = "", currency = "";

                try {

                    JSONObject object = new JSONObject(response);
                    Ssatus = object.getString("status");


                    if (Ssatus.equalsIgnoreCase("1")) {
//                        Alert(getResources().getString(R.string.pushnotification_alert_label_ride_arrived_success), Sresponse);
                        String Sresponse = object.getString("response");
                        mode = object.getString("mode");
                        ref_no = object.getString("ref_no");
                        amount = object.getString("amount");
                        currency = object.getString("currency");


                        if (flag.equalsIgnoreCase("paypal_withdrawal")) {

                            Intent in = new Intent(NewCloudMoneyTransferUrlProgress.this, PaymentSuccessInfoActivity.class);
                            in.putExtra("flag", flag);
                            in.putExtra("mode", mode);
                            in.putExtra("ref_no", ref_no);
                            in.putExtra("amount", amount);
                            in.putExtra("currency", currency);
                            in.putExtra("paypal_id", paypal_id);
                            startActivity(in);
                            finish();
                            overridePendingTransition(R.anim.enter, R.anim.exit);

                        } else if (flag.equalsIgnoreCase("cash_withdrawal")) {

                            Intent in = new Intent(NewCloudMoneyTransferUrlProgress.this, PaymentSuccessInfoActivity.class);
                            in.putExtra("flag", flag);
                            in.putExtra("mode", mode);
                            in.putExtra("ref_no", ref_no);
                            in.putExtra("amount", amount);
                            in.putExtra("currency", currency);
                            startActivity(in);
                            finish();
                            overridePendingTransition(R.anim.enter, R.anim.exit);

                        }


                    } else {
                        String Sresponse = object.getString("response");
                        Alert1("SORRY! EXCESS LIMIT", "Please lower your amount");
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onErrorListener() {

            }
        });


    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(NewCloudMoneyTransferUrlProgress.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mDialog.dismiss();
            }
        });
        mDialog.show();
    }
}
