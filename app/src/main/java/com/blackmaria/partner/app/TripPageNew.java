package com.blackmaria.partner.app;

import android.Manifest;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.text.Html;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.CycleInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.latestpackageview.Model.CancelReasonPojo;
import com.blackmaria.partner.Pojo.CancelTripPojo;
import com.blackmaria.partner.Pojo.MultipleLocationPojo;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.CurrencySymbolConverter;
import com.blackmaria.partner.Utils.GMapV2GetRouteDirection;
import com.blackmaria.partner.Utils.GPSTracker;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.adapter.CancelAlertAdapter;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.view.Dashboard.OnlinepageConstrain;
import com.blackmaria.partner.mylibrary.latlnginterpolation.LatLngInterpolator;
import com.blackmaria.partner.mylibrary.latlnginterpolation.MarkerAnimation;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.blackmaria.partner.latestpackageview.Database.GEODBHelper;
import com.blackmaria.partner.services.GoogleNavigationService;
import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by user127 on 21-07-2017.
 */

public class TripPageNew extends ActivityHockeyApp implements RoutingListener, View.OnClickListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {

    private ImageView Iv_currentLocation, Iv_vehicle, Iv_time, Iv_distance;
    private Button Btn_navigate;
    private RelativeLayout RL_call, RL_cancel, RL_trip_info_bike, RL_sos_bike, RL_trip_info_car, RL_sos_car, RL_returnTrip, RL_multiDrop;
    private LinearLayout LL_time, LL_distance, LL_call_and_cancel, LL_trip_info_and_sos_bike, LL_trip_info_and_sos_car;

    private RelativeLayout RL_ride, RL_arrive, RL_profileArrive, RL_start, RL_profileStart, RL_finish;
    private TextView Tv_ride, Tv_arriveRiderName, Tv_arriveRideId, Tv_arriveFrom, Tv_startRiderName, Tv_startName, Tv_startAmount, Tv_startTo, Tv_finishRideStatus, Tv_finishRideStatus1, Tv_finishTo;
    private ImageView Iv_arriveRiderProfile, Iv_arrivePaymentOption, Iv_startRiderProfile, Iv_finishStatus1, Iv_finishStatus;

    private TextView Tv_returnTrip, Tv_multiDrop, Tv_time, Tv_distance;
    private ListView cancel_list;


    String MultiIsEnd = "", MultiMode = "", MultiRecordId = "";

    private Marker currentDriverMarker;
    private Dialog cancel_dialog;
    private ConnectionDetector cd;
    private GoogleMap googleMap;
    private GPSTracker gps;
    private ServiceRequest mRequest;
    Dialog dialog;
    private SessionManager sessionManager;
    private BroadcastReceiver finishReceiver;
    private CancelAlertAdapter CancelAlertAdapter;
    public static Location myLocation;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private GEODBHelper myDBHelper;
    private Handler travelTimeHandler;
    private Runnable travelTimeRunnable;
    GMapV2GetRouteDirection v2GetRouteDirection;


    private TextView Tv_close;
    public static boolean iscomingfromwaitingpage = false;

    public static Intent intent;

    private ArrayList<LatLng> wayPointList;
    private LatLngBounds.Builder wayPointBuilder;
    private List<Polyline> polyLines;
    private LatLng startWayPoint, endWayPoint;
    private ArrayList<MultipleLocationPojo> multipleDropList;

    //Moving Marker
    private LatLng newLatLng, oldLatLng;
    private LatLngInterpolator mLatLngInterpolator;
    private Location oldLocation;
    private double myMovingDistance = 0.0;


    private boolean isInternetPresent = false;
    private boolean show_progress_status = false;
    private float totalDistanceTravelled;
    private String driver_id = "", driver_name = "";
    private String Str_RideId = "", sPickUpAddress = "", Str_pickUp_Lat = "", Str_pickUp_Long = "", Str_username = "",
            Str_user_rating = "", Str_user_phoneno = "", Str_user_img = "", Str_droplat = "", Str_droplon = "",
            str_drop_location = "", sTripMode_local = "", Suser_Id = "", title_text = "", subtitle_text = "", eta_arrival = "",
            location = "", member_since = "", user_gender = "", est_cost = "", sCurrencySymbol = "", Trip_Status = "arrived",
            sContactNumber = "", reason_id = "", sVehicleType = "", sPaymentIcon = "", sCompAvailable = "", sCompStage = "", final_droplocation = "";
    private int multipleDropCount = 0;
    private String sRecordId = "";
    private String sEndNavigationLat = "", sEndNavigationLng = "", sMultipleDropStatus = "";
    private LatLng endTrip_NavigationLatLng;
    private String sTripMode = "", isReturnAvailable = "";
    private String sReturnState = "";
    private String sReturnLat = "", sReturnLng = "", sReturnPickUpLat = "", sReturnPickUpLng = "",
            sPaymentType = "", sCancelledRides = "", sCompletedRides = "", sRiderType = "", sReturnLocation = "";
    private boolean isReturnLatLngAvailable = false;
    private String Str_DistanceTo = "", Str_TimeTo = "";
    private String sCurrentLocationName = "";
    private LatLng pickup_latlong = null, drop_latlong = null, cur_latlong = null, return_latLong = null, return_pickUp_latLong = null;
    private LatLng pickup_multiStop_latlong = null;
    private boolean isMultipleDropAvailable = false;
    private final static int OVERLAY_PERMISSION_REQ_CODE = 1234;
    final int PERMISSION_REQUEST_NAVIGATION_CODE = 222;
    final int PERMISSION_REQUEST_CODE = 111, PERMISSION_REQUEST_CODE_SOS = 333;
    private final static int REQUEST_LOCATION = 199;
    private double MyCurrent_lat = 0.0, MyCurrent_long = 0.0;
    private ArrayList<CancelReasonPojo> Cancelreason_arraylist;
    private ArrayList<CancelTripPojo> cancelReasonsList;
    private PendingResult<LocationSettingsResult> result;

    MapFragment mapFragment;
    private Marker jumpingMarker = null;
    private Circle lastUserCircle, lastUserCircle1;
    private ValueAnimator lastPulseAnimator, lastPulseAnimator1;

    Bitmap carIcon = null;
    private String colorCodeStart = "<font color='#FFCC00'>";  // use any color as  your want
    private String colorCodeEnd = "</font>";


    private String return_mode = "", is_return_over = "", closing_status = "";
    public static boolean isTripArrived = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN |
                        WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                        WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON,
                WindowManager.LayoutParams.FLAG_FULLSCREEN |
                        WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                        WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        setContentView(R.layout.blackmaria_trip4);

        // Receiving the data from broadcast
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.app.finish.TripPage");
        filter.addAction("com.app.waiting.stop.handler");
        filter.addAction("com.app.waiting.stop");
        finishReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals("com.app.waiting.stop")) {
//                    startActivity(getIntent());


                } else if (intent.getAction().equals("com.app.waiting.stop.handler")) {
                    Intent intents = new Intent(context, WaitingTimePage.class);
                    intents.putExtra("rideID", sessionManager.getWaitingtime_rideid());
                    intents.putExtra("isContinue", false);
                    intents.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intents);
                } else {
                    finish();
                }

            }
        };
        registerReceiver(finishReceiver, filter);

        initializeViews();


        try {
            setLocationRequest();
            buildGoogleApiClient();
            startLocationUpdates();

            if (sCompStage.equalsIgnoreCase("onprocess")) {
                Intent intent = new Intent(TripPageNew.this, WaitingTimePage.class);
                intent.putExtra("rideID", Str_RideId);
                intent.putExtra("isContinue", true);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }

            if (sTripMode.equalsIgnoreCase("return")) {
                if (return_mode.equalsIgnoreCase("start") && is_return_over.equalsIgnoreCase("0") && closing_status.equalsIgnoreCase("open")) {
                    Intent intent = new Intent(TripPageNew.this, WaitingTimeDrop.class);
                    intent.putExtra("rideID", Str_RideId);
                    intent.putExtra("isContinue", true);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            } else if (sTripMode.equalsIgnoreCase("multistop")) {

                if (MultiMode.equalsIgnoreCase("end") && MultiIsEnd.equalsIgnoreCase("0") && closing_status.equalsIgnoreCase("open")) {
                    Intent intent = new Intent(TripPageNew.this, WaitingTimeDrop.class);
                    intent.putExtra("rideID", Str_RideId);
                    intent.putExtra("isContinue", true);
                    intent.putExtra("recordId", MultiRecordId);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            }


        } catch (NullPointerException e) {
        } catch (Exception e) {
        }


    }

    private void initializeViews() {

        sessionManager = new SessionManager(TripPageNew.this);
        String carImage = sessionManager.getCarMarker();
        if (carImage != null && carImage.length() > 0) {
            carIcon = base64ToBitmap(sessionManager.getCarMarker());
//            carIcon = Bitmap.createScaledBitmap(carIcon, 100, 100, false);
        } else {
            carIcon = BitmapFactory.decodeResource(getResources(), R.drawable.carmove);
        }
        gps = new GPSTracker(TripPageNew.this);
        cd = new ConnectionDetector(TripPageNew.this);
        v2GetRouteDirection = new GMapV2GetRouteDirection();
        isInternetPresent = cd.isConnectingToInternet();
        wayPointList = new ArrayList<LatLng>();
        polyLines = new ArrayList<Polyline>();
        myDBHelper = new GEODBHelper(TripPageNew.this);
        Cancelreason_arraylist = new ArrayList<CancelReasonPojo>();
        cancelReasonsList = new ArrayList<CancelTripPojo>();
        multipleDropList = new ArrayList<MultipleLocationPojo>();
        sVehicleType = sessionManager.getVehicleType();


        Iv_finishStatus1 = (ImageView) findViewById(R.id.type_ride_image);
        Iv_vehicle = (ImageView) findViewById(R.id.img_car);
        Iv_currentLocation = (ImageView) findViewById(R.id.Iv_current_location);
        Btn_navigate = (Button) findViewById(R.id.btn_navigate);
        RL_returnTrip = (RelativeLayout) findViewById(R.id.Rl_return_trip);
        RL_multiDrop = (RelativeLayout) findViewById(R.id.Rl_multidrop);
        Tv_returnTrip = (TextView) findViewById(R.id.txt_returntrip_label_drop);
        Tv_multiDrop = (TextView) findViewById(R.id.txt_multidrop_label_drop);
        RL_ride = (RelativeLayout) findViewById(R.id.Rl_car);
        LL_time = (LinearLayout) findViewById(R.id.Ll_time);
        LL_distance = (LinearLayout) findViewById(R.id.Ll_distance);
        Iv_time = (ImageView) findViewById(R.id.img_time);
        Iv_distance = (ImageView) findViewById(R.id.img_distance);
        LL_call_and_cancel = (LinearLayout) findViewById(R.id.Ll_call_and_cancel);
        RL_call = (RelativeLayout) findViewById(R.id.Rl_call);
        RL_cancel = (RelativeLayout) findViewById(R.id.Rl_cancel);
        RL_trip_info_bike = (RelativeLayout) findViewById(R.id.RL_trip_info_bike);
        RL_sos_bike = (RelativeLayout) findViewById(R.id.RL_sos_bike);
        RL_sos_car = (RelativeLayout) findViewById(R.id.Rl_sos_car);
        RL_trip_info_car = (RelativeLayout) findViewById(R.id.Rl_trip_info_car);
        LL_trip_info_and_sos_bike = (LinearLayout) findViewById(R.id.Ll_trip_info_and_sos_bike);
        LL_trip_info_and_sos_car = (LinearLayout) findViewById(R.id.Ll_trip_info_and_sos_car);
        Tv_time = (TextView) findViewById(R.id.txt_time);
        Tv_distance = (TextView) findViewById(R.id.txt_distance);
        Tv_ride = (TextView) findViewById(R.id.txt_car);

        RL_arrive = (RelativeLayout) findViewById(R.id.RL_details_arrive);
        RL_profileArrive = (RelativeLayout) findViewById(R.id.Rl_rider_profile_arrive);
        Tv_arriveRiderName = (TextView) findViewById(R.id.txt_rider_name_arrive);
        Tv_arriveRideId = (TextView) findViewById(R.id.txt_ride_id_arrive);
        Tv_arriveFrom = (TextView) findViewById(R.id.txt_address_from_arrive);
        Iv_arriveRiderProfile = (ImageView) findViewById(R.id.img_rider_profile_arrive);
        Iv_arrivePaymentOption = (ImageView) findViewById(R.id.img_payment_option_arrive);

        RL_start = (RelativeLayout) findViewById(R.id.RL_details_start);
        RL_profileStart = (RelativeLayout) findViewById(R.id.Rl_rider_profile_start);
        Tv_startRiderName = (TextView) findViewById(R.id.txt_rider_name_start);
        Tv_startName = (TextView) findViewById(R.id.txt_driver_name_start);
        Tv_startAmount = (TextView) findViewById(R.id.txt_amount_start);
        Tv_startTo = (TextView) findViewById(R.id.txt_address_to_start);
        Iv_startRiderProfile = (ImageView) findViewById(R.id.img_rider_profile_start);

        RL_finish = (RelativeLayout) findViewById(R.id.RL_details_finish);
        Tv_finishRideStatus = (TextView) findViewById(R.id.txt_trip_status_finish);
        Tv_finishRideStatus1 = (TextView) findViewById(R.id.type_ride_tv);

        Tv_finishTo = (TextView) findViewById(R.id.txt_address_to_finish);
        Iv_finishStatus = (ImageView) findViewById(R.id.img_trip_status_finish);


        RL_ride.setOnClickListener(this);
//        Tv_ride.setOnClickListener(this);
        Iv_currentLocation.setOnClickListener(this);
        Btn_navigate.setOnClickListener(this);
        RL_returnTrip.setOnClickListener(this);
        RL_multiDrop.setOnClickListener(this);
        LL_time.setOnClickListener(this);
        LL_distance.setOnClickListener(this);
        RL_cancel.setOnClickListener(this);
        RL_call.setOnClickListener(this);
        RL_trip_info_car.setOnClickListener(this);
        RL_trip_info_bike.setOnClickListener(this);
        RL_sos_car.setOnClickListener(this);
        RL_sos_bike.setOnClickListener(this);

        if (sVehicleType.equalsIgnoreCase("motorbike")) {
            Iv_vehicle.setImageResource(R.drawable.img_trip_bike);
        } else if (sVehicleType.equalsIgnoreCase("taxi")) {
            Iv_vehicle.setImageResource(R.drawable.trip_img_taxi);
        } else {
            Iv_vehicle.setImageResource(R.drawable.trip_img_car);
        }

        getIntentData();
        initializeMap();

        travelTimeRunnable = new Runnable() {
            @Override
            public void run() {

                System.out.println("-----------Ganesh Handler-----------");

                if (gps != null && gps.canGetLocation()) {

                    String currentLat = String.valueOf(gps.getLatitude());
                    String currentLong = String.valueOf(gps.getLongitude());

                    String destinationLat = "", destinationLong = "";

                    if (!Trip_Status.equalsIgnoreCase("end")) {

                        destinationLat = String.valueOf(Str_droplat);
                        destinationLong = String.valueOf(Str_droplon);

                    } else {

                        System.out.println("------------prem -------sEndNavigationLat----------" + sEndNavigationLat + "," + sEndNavigationLng);
                        if (sTripMode.equalsIgnoreCase("return")) {

                            if (isReturnAvailable.equalsIgnoreCase("1")) {

                                if (sReturnLat.length() > 0 && sReturnLng.length() > 0) {
                                    destinationLat = sReturnLat;
                                    destinationLong = sReturnLng;
                                }
                            } else {
                                if (sEndNavigationLat.length() > 0 && sEndNavigationLng.length() > 0) {
                                    destinationLat = sEndNavigationLat;
                                    destinationLong = sEndNavigationLng;
                                }
                            }

                        } else {
                            if (sEndNavigationLat.length() > 0 && sEndNavigationLng.length() > 0) {
                                destinationLat = sEndNavigationLat;
                                destinationLong = sEndNavigationLng;
                            }
                        }

                    }

                    String url = Iconstant.GetDistanceAndTime_url + "&origins=" + currentLat + "," + currentLong
                            + "&destinations=" + destinationLat + "," + destinationLong;
                    getDistanceAndTimeRequest(url);

                }

            }
        };

        HashMap<String, String> userDetails = sessionManager.getUserDetails();
        driver_id = userDetails.get(SessionManager.KEY_DRIVERID);
        driver_name = userDetails.get(SessionManager.KEY_DRIVER_NAME);
        sContactNumber = sessionManager.getContactNumber();

    }

    @SuppressWarnings("WrongConstant")
    private void getIntentData() {

        if (iscomingfromwaitingpage) {
            intent = intent;
        } else {
            intent = getIntent();
        }


        if (intent != null) {

            sPickUpAddress = intent.getStringExtra("address");
            Str_RideId = intent.getStringExtra("rideId");
            Str_pickUp_Lat = intent.getStringExtra("pickuplat");
            Str_pickUp_Long = intent.getStringExtra("pickup_long");
            Str_username = intent.getStringExtra("username");
            Str_user_rating = intent.getStringExtra("userrating");
            Str_user_phoneno = intent.getStringExtra("phoneno");
            Str_user_img = intent.getStringExtra("userimg");
            Suser_Id = intent.getStringExtra("UserId");
            Str_droplat = intent.getStringExtra("drop_lat");
            Str_droplon = intent.getStringExtra("drop_lon");
            str_drop_location = intent.getStringExtra("drop_location");
            sessionManager.setfinaldestinaltion(str_drop_location);
            Trip_Status = intent.getStringExtra("trip_status");

            return_mode = intent.getStringExtra("return_mode");
            is_return_over = intent.getStringExtra("is_return_over");
            closing_status = intent.getStringExtra("closing_status");


            if (Str_pickUp_Lat.length() > 0 && Str_pickUp_Long.length() > 0) {
                pickup_latlong = new LatLng(Double.parseDouble(Str_pickUp_Lat), Double.parseDouble(Str_pickUp_Long));
            }
            if (Str_pickUp_Lat.length() > 0 && Str_pickUp_Long.length() > 0) {
                pickup_multiStop_latlong = new LatLng(Double.parseDouble(Str_pickUp_Lat), Double.parseDouble(Str_pickUp_Long));
            }
            if (Str_droplat.length() > 0 && Str_droplon.length() > 0) {
                drop_latlong = new LatLng(Double.parseDouble(Str_droplat), Double.parseDouble(Str_droplon));
            }
            sTripMode = intent.getStringExtra("TripMode");

            isReturnAvailable = intent.getStringExtra("IsReturnAvailable");
            sReturnState = intent.getStringExtra("ReturnState");
            String sReturnLatLngAvailable = intent.getStringExtra("IsReturnLatLngAvailable");
            if (sReturnLatLngAvailable.equalsIgnoreCase("1")) {
                isReturnLatLngAvailable = true;
            } else {
                isReturnLatLngAvailable = false;
            }
            sReturnLat = intent.getStringExtra("ReturnLat");
            sReturnLng = intent.getStringExtra("ReturnLng");

            sReturnPickUpLat = intent.getStringExtra("ReturnPickUpLat");
            sReturnPickUpLng = intent.getStringExtra("ReturnPickUpLng");
            sPaymentType = intent.getStringExtra("payment_type");
            sCancelledRides = intent.getStringExtra("cancelled_rides");
            sCompletedRides = intent.getStringExtra("completed_rides");
            sRiderType = intent.getStringExtra("rider_type");

            if (sReturnLat.length() > 0 && sReturnLng.length() > 0 && sReturnLat != null && sReturnLng != null) {
                return_latLong = new LatLng(Double.parseDouble(sReturnLat), Double.parseDouble(sReturnLng));
            }

            if (sReturnPickUpLat.length() > 0 && sReturnPickUpLng.length() > 0 && sReturnPickUpLat != null && sReturnPickUpLng != null) {
                return_pickUp_latLong = new LatLng(Double.parseDouble(sReturnPickUpLat), Double.parseDouble(sReturnPickUpLng));
            }

            if (!Trip_Status.equals("arrived")) {
                title_text = intent.getStringExtra("title_text");
                subtitle_text = intent.getStringExtra("subtitle_text");
                est_cost = intent.getStringExtra("est_cost");
                sCurrencySymbol = intent.getStringExtra("sCurrencySymbol");

            } else {
                eta_arrival = intent.getStringExtra("eta_arrival");
                location = intent.getStringExtra("location");
                member_since = intent.getStringExtra("member_since");
                user_gender = intent.getStringExtra("user_gender");
            }

            if (intent.hasExtra("payment_icon")) {
                sPaymentIcon = intent.getStringExtra("payment_icon");
                if (sPaymentIcon.length() > 0) {
                    Picasso.with(TripPageNew.this).load(sPaymentIcon).error(R.drawable.img_cash).placeholder(R.drawable.img_cash).into(Iv_arrivePaymentOption);
                } else {
                    Iv_arrivePaymentOption.setVisibility(View.GONE);
                }
            } else {
                Iv_arrivePaymentOption.setVisibility(View.GONE);
            }


            if (intent.hasExtra("comp_status") && intent.hasExtra("comp_stage")) {
                sCompAvailable = intent.getStringExtra("comp_status");
                sCompStage = intent.getStringExtra("comp_stage");
            }


            if (intent.hasExtra("ReturnLocation")) {
                sReturnLocation = intent.getStringExtra("ReturnLocation");
            }


            if (Trip_Status.equals("end")) {
                if (intent.hasExtra("MultipleDropStatus")) {
                    sMultipleDropStatus = intent.getStringExtra("MultipleDropStatus");
                    if (sMultipleDropStatus.equalsIgnoreCase("1")) {

                        isMultipleDropAvailable = true;
                        RL_multiDrop.setVisibility(View.VISIBLE);
                        try {
                            Bundle bundleObject = getIntent().getExtras();
                            multipleDropList = (ArrayList<MultipleLocationPojo>) bundleObject.getSerializable("additionalDropLocation");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (multipleDropList != null) {
                            for (int j = 0; j < multipleDropList.size(); j++) {

                                System.out.println("------multipleDropList------------" + multipleDropList.get(j).getIsEnd());

                                if (multipleDropList.get(j).getIsEnd().equalsIgnoreCase("0")) {


                                    MultiIsEnd = multipleDropList.get(j).getIsEnd();
                                    MultiMode = multipleDropList.get(j).getMode();
                                    MultiRecordId = multipleDropList.get(j).getRecordId();

                                    sEndNavigationLat = multipleDropList.get(j).getDropLat();
                                    sEndNavigationLng = multipleDropList.get(j).getDropLon();
                                    endTrip_NavigationLatLng = new LatLng(Double.parseDouble(sEndNavigationLat), Double.parseDouble(sEndNavigationLng));

                                    sRecordId = multipleDropList.get(j).getRecordId();
                                    sTripMode_local = multipleDropList.get(j).getMode();

                                    if (multipleDropList.get(j).getMode().equalsIgnoreCase("start")) {
                                        RL_multiDrop.setBackgroundResource(R.drawable.red_square);
                                        Tv_multiDrop.setText(getResources().getString(R.string.trip_label_drop));
                                        multipleDropCount = 0;
                                        str_drop_location = multipleDropList.get(j).getDropLocation();

                                    } else {
                                        RL_multiDrop.setBackgroundResource(R.drawable.blue_square);
                                        Tv_multiDrop.setText(getResources().getString(R.string.trip_label_start));
                                        multipleDropCount = 1;
                                    }

                                    RL_multiDrop.setVisibility(View.VISIBLE);
                                    break;

//
                                } else {
                                    sEndNavigationLat = Str_droplat;
                                    sEndNavigationLng = Str_droplon;

                                    pickup_multiStop_latlong = new LatLng(Double.parseDouble(multipleDropList.get(j).getDropLat()), Double.parseDouble(multipleDropList.get(j).getDropLon()));
                                    endTrip_NavigationLatLng = new LatLng(Double.parseDouble(Str_droplat), Double.parseDouble(Str_droplon));
                                    RL_multiDrop.setVisibility(View.GONE);
                                }
                            }

                        } else {
                            sEndNavigationLat = Str_droplat;
                            sEndNavigationLng = Str_droplon;
                        }

                    } else {
                        sEndNavigationLat = Str_droplat;
                        sEndNavigationLng = Str_droplon;
                        isMultipleDropAvailable = false;
                        RL_multiDrop.setVisibility(View.GONE);
                    }
                } else {
                    sEndNavigationLat = Str_droplat;
                    sEndNavigationLng = Str_droplon;
                    isMultipleDropAvailable = false;
                }
            }

        }

    }


    @SuppressWarnings("WrongConstant")
    @Override
    public void onClick(View view) {

        cd = new ConnectionDetector(TripPageNew.this);
        if (cd.isConnectingToInternet()) {

            if (view == Iv_currentLocation) {

                if (googleMap != null) {
                    if (gps != null && gps.canGetLocation()) {

                        try {

                            double lat = gps.getLatitude();
                            double lon = gps.getLongitude();

                            if (lat != 0.0 && lon != 0.0) {
                                LatLng latLng = new LatLng(gps.getLatitude(), gps.getLongitude());

                                // Code To Move Camera To Current Location With Current Zoom Level
//                                float zoom = googleMap.getCameraPosition().zoom;
//                                CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(zoom).build();
//                                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                                // Code To Move Camera To Current Location With Specified Zoom Level
                                CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(18).build();
                                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else if (view == Btn_navigate) {

                checkNavigation();

            } else if (view == RL_ride) {

                if (Tv_ride.getText().toString().equalsIgnoreCase(getResources().getString(R.string.trip_page_label_tap_to_arrive))) {

//                    PostRequest(Iconstant.driverArrived_Url, "arrived");
                    isTripArrived = true;
                    confirmTripAlerts(0);


                } else if (Tv_ride.getText().toString().equalsIgnoreCase(getResources().getString(R.string.trip_page_label_start_now))) {

//                    PostRequest(Iconstant.driverBegin_Url, "begin");
                    confirmTripAlerts(1);

                } else {

                    if (RL_multiDrop.getVisibility() == View.VISIBLE) {

                        if (Tv_multiDrop.getText().toString().equalsIgnoreCase(getResources().getString(R.string.trip_label_start))) {

                            alert(getResources().getString(R.string.action_error), getResources().getString(R.string.trip_label_startRide_continue_alert));

                        } else if (Tv_multiDrop.getText().toString().equalsIgnoreCase(getResources().getString(R.string.trip_label_drop))) {

                            alert(getResources().getString(R.string.action_error), getResources().getString(R.string.trip_label_dropRide_continue_alert));

                        }

                    } else if (RL_returnTrip.getVisibility() == View.VISIBLE) {

                        if (Tv_returnTrip.getText().toString().equalsIgnoreCase(getResources().getString(R.string.trip_label_drop))) {

                            alert(getResources().getString(R.string.action_error), getResources().getString(R.string.trip_label_dropRide_continue_alert));

                        } else if (Tv_returnTrip.getText().toString().equalsIgnoreCase(getResources().getString(R.string.trip_label_return))) {

                            alert(getResources().getString(R.string.action_error), getResources().getString(R.string.trip_label_returnRide_continue_alert));

                        }

                    } else {

                        /*if (multipleDropCount == 1) {
                            alert(getResources().getString(R.string.action_error), getResources().getString(R.string.trip_label_startRide_continue_alert));
                        } else {

                            dialog = new Dialog(TripPageNew.this);
                            dialog.getWindow();
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog.setContentView(R.layout.custom_loading);
                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            dialog.setCanceledOnTouchOutside(false);
                            dialog.show();

                            GeoCoderAsyncTask asyncTask = new GeoCoderAsyncTask();
                            asyncTask.execute();
                        }*/

                        confirmTripAlerts(2);

                    }


                }

            } else if (view == RL_call) {

                if (Str_user_phoneno != null) {

//                    HashMap<String, String> phone = sessionManager.getPhoneMaskingStatus();
                    String sPhoneMaskingStatus = sessionManager.getPhoneMaskingStatus();

                    if (sPhoneMaskingStatus.equalsIgnoreCase("Yes")) {
                        postRequest_PhoneMasking(Iconstant.phoneMasking_url);
                    } else {
                        if (Build.VERSION.SDK_INT >= 23) {
                            // Marshmallow+

                            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + Str_user_phoneno));
                            startActivity(intent);

                        } else {
                            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + Str_user_phoneno));
                            startActivity(intent);
                        }
                    }

                } else {
                    alert(getResources().getString(R.string.action_error), getResources().getString(R.string.inavid_call));
                }

            } else if (view == RL_cancel) {

                if (cd.isConnectingToInternet()) {
                    cancelConfirm();
                } else {
                    alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                }

            } else if (view == RL_trip_info_car || view == RL_trip_info_bike) {

                Intent intent = new Intent(TripPageNew.this, TripSummary.class);
                intent.putExtra("rideID", Str_RideId);
                intent.putExtra("fromPage", "TripPage");
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);

            } else if (view == RL_sos_bike || view == RL_sos_car) {

                showSOSAlert();

            } else if (view == RL_multiDrop) {
                if (multipleDropCount == 1) {
                    if (cd.isConnectingToInternet()) {
                        postRequestDropAndStartRide(Iconstant.multipleDrop_Url, "end", sRecordId);
                    } else {
                        alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                    }
                } else {
                    if (cd.isConnectingToInternet()) {
                        postRequestDropAndStartRide(Iconstant.multipleDrop_Url, "start", sRecordId);
                    } else {
                        alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                    }
                }
            } else if (view == RL_returnTrip) {
                if (Tv_returnTrip.getText().toString().equalsIgnoreCase(getResources().getString(R.string.trip_label_drop))) {
                    if (cd.isConnectingToInternet()) {
                        postRequestReturnTrip(Iconstant.returnTrip_Url, "drop");
                    } else {
                        alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                    }

                } else {
                    if (cd.isConnectingToInternet()) {
                        postRequestReturnTrip(Iconstant.returnTrip_Url, "start");
                    } else {
                        alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                    }

                }
            }

        } else {
            alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }
    }

    private void initializeMap() {
        if (googleMap == null) {
            mapFragment = ((MapFragment) getFragmentManager().findFragmentById(R.id.tripPage_googleMap));
            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap arg) {
                    loadMap(arg);
                }
            });
        }
    }

    public void loadMap(GoogleMap arg) {

        googleMap = arg;
        googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getApplicationContext(), R.raw.style));
        googleMap.getUiSettings().setRotateGesturesEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        gps = new GPSTracker(TripPageNew.this);
        if (gps != null && gps.canGetLocation()) {
            double dLatitude = gps.getLatitude();
            double dLongitude = gps.getLongitude();
            MyCurrent_lat = dLatitude;
            MyCurrent_long = dLongitude;
            cur_latlong = new LatLng(MyCurrent_lat, MyCurrent_long);
        } else {
            enableGpsService();
        }

        if (!String.valueOf(MyCurrent_lat).equals(0.0) && !String.valueOf(MyCurrent_long).equals(0.0)) {
            if (googleMap != null) {
                CameraPosition cameraPosition = new CameraPosition.Builder().target(cur_latlong).zoom(13).build();
                googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                MarkerOptions marker = new MarkerOptions().position(cur_latlong);
//                marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.carmove));
                marker.icon(BitmapDescriptorFactory.fromBitmap(carIcon));
                currentDriverMarker = googleMap.addMarker(marker);

                if (Trip_Status.equals("arrived")) {
//                    arrived_layout.setVisibility(View.VISIBLE);
//                    Trip_layout.setVisibility(View.INVISIBLE);
                    arrived(sPickUpAddress, eta_arrival, Str_RideId, location, member_since, user_gender, Str_username, Str_user_img);
                } else if (Trip_Status.equals("begin")) {
//                    arrived_layout.setVisibility(View.INVISIBLE);
//                    Trip_layout.setVisibility(View.VISIBLE);
                    Begin(str_drop_location, est_cost + sCurrencySymbol, Str_RideId, title_text, subtitle_text);
                } else if (Trip_Status.equals("end")) {
//                    arrived_layout.setVisibility(View.INVISIBLE);
//                    Trip_layout.setVisibility(View.GONE);

                    End(str_drop_location, sCurrencySymbol + " " + est_cost, Str_RideId, title_text, subtitle_text, sTripMode_local);
                }
            }
        }

        sessionManager.setUpdateLocationServiceState("trip");

    }


    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (mGoogleApiClient != null)
            mGoogleApiClient.disconnect();
    }

    @SuppressLint("MissingPermission")
    @Override
    protected void onResume() {
        super.onResume();

        if (myLocation == null) {
            myLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        }

//        initializeViews();
    }

    private void setLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_LOW_POWER);
    }

    private void SetRouteMap(LatLng fromPosition, LatLng toPosition) {
        if (fromPosition != null && toPosition != null) {
            if (cd.isConnectingToInternet()) {
                GetRouteTask getRoute = new GetRouteTask(fromPosition, toPosition);
                getRoute.execute();
            } else {
                alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
            }
        }
    }

    @SuppressLint("MissingPermission")
    protected void startLocationUpdates() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    private void cancelConfirm() {
        final Dialog cancelConfirmDialog = new Dialog(TripPageNew.this);
        cancelConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        cancelConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        cancelConfirmDialog.setCancelable(true);
        cancelConfirmDialog.setContentView(R.layout.alert_sure_to_cancel);

        final Button Btn_yes = (Button) cancelConfirmDialog.findViewById(R.id.btn_yes);
        final Button Btn_no = (Button) cancelConfirmDialog.findViewById(R.id.btn_no);

        Btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelConfirmDialog.dismiss();

                if (cd.isConnectingToInternet()) {
                    PostRequestCancel(Iconstant.driverCancel_reason_Url);
                } else {
                    alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                }

            }
        });

        Btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelConfirmDialog.dismiss();
            }
        });

        cancelConfirmDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                RL_cancel.setEnabled(true);
            }
        });

        cancelConfirmDialog.show();
        RL_cancel.setEnabled(false);

    }

    @SuppressWarnings("WrongConstant")
    private void confirmTripAlerts(final int value) {
        final Dialog confirmDialog = new Dialog(TripPageNew.this);
        confirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmDialog.setCancelable(true);
        confirmDialog.setContentView(R.layout.alert_sure_to_cancel);

        final TextView Tv_title = (TextView) confirmDialog.findViewById(R.id.txt_label_title);
        final Button Btn_yes = (Button) confirmDialog.findViewById(R.id.btn_yes);
        final Button Btn_no = (Button) confirmDialog.findViewById(R.id.btn_no);

        // Value 0 is For Confirm Arriving The Trip
        // Value 1 is For Confirm Beginning The Trip
        // Value 2 is For Confirm Finishing The Trip
        if (value == 0) {
            Tv_title.setText(getResources().getString(R.string.trip_alert_sure_to_arrive));
        } else if (value == 1) {
            Tv_title.setText(getResources().getString(R.string.trip_alert_sure_to_start));
        } else if (value == 2) {
            Tv_title.setText(getResources().getString(R.string.trip_alert_sure_to_finish));
        }

        Btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmDialog.dismiss();

                if (cd.isConnectingToInternet()) {

                    if (value == 0) {

                        PostRequest(Iconstant.driverArrived_Url, "arrived");

                    } else if (value == 1) {

                        PostRequest(Iconstant.driverBegin_Url, "begin");

                    } else if (value == 2) {

                        if (multipleDropCount == 1) {
                            alert(getResources().getString(R.string.action_error), getResources().getString(R.string.trip_label_startRide_continue_alert));
                        } else {
                            dialog = new Dialog(TripPageNew.this);
                            dialog.getWindow();
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog.setContentView(R.layout.custom_loading);
                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            dialog.setCanceledOnTouchOutside(false);
                            dialog.show();

                            GeoCoderAsyncTask asyncTask = new GeoCoderAsyncTask();
                            asyncTask.execute();
                        }

                    }


                } else {
                    alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                }

            }
        });

        Btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmDialog.dismiss();
            }
        });

        confirmDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                RL_ride.setEnabled(true);
            }
        });

        confirmDialog.show();

    }


    private void showRiderProfile() {
        final Dialog dialog = new Dialog(TripPageNew.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.alert_trip_rider_details);

        /*Window window = dialog.getWindow();
        WindowManager.LayoutParams param = window.getAttributes();
        param.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;
        param.y = 100;
        window.setAttributes(param);
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));*/

        TextView Tv_riderName = (TextView) dialog.findViewById(R.id.txt_rider_name);
        RatingBar Rb_rider = (RatingBar) dialog.findViewById(R.id.rb_rider);
        ImageView Cv_riderProfile = (ImageView) dialog.findViewById(R.id.img_profile_alert_rider);
        TextView Tv_riderType = (TextView) dialog.findViewById(R.id.txt_rider_type);
        TextView Tv_member = (TextView) dialog.findViewById(R.id.txt_member);
        TextView Tv_dateJoin = (TextView) dialog.findViewById(R.id.txt_date_join);
        TextView Tv_gender = (TextView) dialog.findViewById(R.id.txt_gender);
        TextView Tv_tripCompleted = (TextView) dialog.findViewById(R.id.txt_trip_completed);
        TextView Tv_tripCancelled = (TextView) dialog.findViewById(R.id.txt_trip_cancelled);
        Button btn_close = (Button) dialog.findViewById(R.id.btn_close);

        Tv_riderName.setText(Str_username);
        Rb_rider.setRating(Float.parseFloat(Str_user_rating));
        Tv_member.setText(location);
        Tv_riderType.setText(sRiderType);
        Tv_tripCompleted.setText(sCompletedRides);
        Tv_tripCancelled.setText(sCancelledRides);
        Tv_dateJoin.setText(member_since);
        Tv_gender.setText(user_gender);

        Picasso.with(TripPageNew.this).load(Str_user_img).error(R.drawable.no_user_img).placeholder(R.drawable.no_user_img).memoryPolicy(MemoryPolicy.NO_CACHE).into(Cv_riderProfile);

        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });

        dialog.show();

//        RL_profile.setVisibility(View.VISIBLE);

    }


    private class GeoCoderAsyncTask extends AsyncTask<String, String, String> {

        String strAdd = "";

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... params) {

            Geocoder geocoder = new Geocoder(TripPageNew.this, Locale.getDefault());
            try {
                List<Address> addresses = geocoder.getFromLocation(MyCurrent_lat, MyCurrent_long, 1);
                if (addresses != null) {
                    Address returnedAddress = addresses.get(0);
                    StringBuilder strReturnedAddress = new StringBuilder("");
                    for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                        strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                    }
                    strAdd = strReturnedAddress.toString();
                } else {
                    strAdd = "error";
                    Log.e("Current loction address", "No Address returned!");
                }
            } catch (Exception e) {
                e.printStackTrace();
                strAdd = "error";
                Log.e("Current loction address", "Cannot get Address!");
            }
            return strAdd;
        }

        @Override
        protected void onPostExecute(String result) {
            if (result.equalsIgnoreCase("error")) {
                sCurrentLocationName = "";
                if (cd.isConnectingToInternet()) {
                    PostRequest(Iconstant.driverEnd_Url, "end");
                } else {
                    alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                }

            } else {
                sCurrentLocationName = result;
                if (cd.isConnectingToInternet()) {
                    PostRequest(Iconstant.driverEnd_Url, "end");
                } else {
                    alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                }

            }
        }

    }


    private void checkNavigation() {

        if (Build.VERSION.SDK_INT >= 23) {

            if (!checkWriteExternalStoragePermission()) {
                requestNavigationPermission();
            } else {
                if (!Settings.canDrawOverlays(TripPageNew.this)) {
                    Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                            Uri.parse("package:" + getPackageName()));
                    startActivityForResult(intent, OVERLAY_PERMISSION_REQ_CODE);
                } else {
                    moveNavigation();
                }
            }
        } else {
            moveNavigation();
        }
    }


    private void moveNavigation() {
        if (isGoogleMapsInstalled()) {

            if (Trip_Status.equals("arrived")) {
                startServiceNavigation();
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(String.format("google.navigation:ll=%s,%s%s", Double.parseDouble(Str_pickUp_Lat), Double.parseDouble(Str_pickUp_Long), "&mode=c")));
                startActivity(intent);
            } else if (Trip_Status.equals("begin")) {
                startServiceNavigation();
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(String.format("google.navigation:ll=%s,%s%s", Double.parseDouble(Str_droplat), Double.parseDouble(Str_droplon), "&mode=c")));
                startActivity(intent);
            } else if (Trip_Status.equals("end")) {

                System.out.println("------------prem -------sEndNavigationLat----------" + sEndNavigationLat + "," + sEndNavigationLng);

                if (Build.VERSION.SDK_INT >= 23) {
                    if (!Settings.canDrawOverlays(TripPageNew.this)) {
                        Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                                Uri.parse("package:" + getPackageName()));
                        startActivityForResult(intent, 1234);
                    }
                } else {
                    if (!isMyServiceRunning(GoogleNavigationService.class)) {
                        startService(new Intent(getApplicationContext(), GoogleNavigationService.class));
                    }
                }


                String sReturnStateString = "";
                if (isReturnLatLngAvailable) {
                    sReturnStateString = "1";
                } else {
                    sReturnStateString = "0";
                }

                sessionManager.setGoogleNavigationData(sPickUpAddress, Str_RideId, Str_pickUp_Lat, Str_pickUp_Long, Str_username, Str_user_rating, Str_user_phoneno,
                        Str_user_img, Suser_Id, Str_droplat, Str_droplon, str_drop_location, title_text, subtitle_text, Trip_Status, eta_arrival, location,
                        member_since, user_gender, est_cost, sCurrencySymbol, sMultipleDropStatus, sTripMode, multipleDropList, isReturnAvailable, sReturnState, sReturnStateString, sReturnLat, sReturnLng, sReturnPickUpLat, sReturnPickUpLng,
                        sPaymentType, sCancelledRides, sCompletedRides, sRiderType);


                if (sTripMode.equalsIgnoreCase("return")) {

                    if (isReturnAvailable.equalsIgnoreCase("1")) {

                        if (sReturnLat.length() > 0 && sReturnLng.length() > 0) {
                            Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(String.format("google.navigation:ll=%s,%s%s", Double.parseDouble(sReturnLat), Double.parseDouble(sReturnLng), "&mode=c")));
                            startActivity(intent);
                        }
                    } else {
                        if (sEndNavigationLat.length() > 0 && sEndNavigationLng.length() > 0) {
                            Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(String.format("google.navigation:ll=%s,%s%s", Double.parseDouble(sEndNavigationLat), Double.parseDouble(sEndNavigationLng), "&mode=c")));
                            startActivity(intent);
                        }
                    }

                } else {
                    if (sEndNavigationLat.length() > 0 && sEndNavigationLng.length() > 0) {
                        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(String.format("google.navigation:ll=%s,%s%s", Double.parseDouble(sEndNavigationLat), Double.parseDouble(sEndNavigationLng), "&mode=c")));
                        startActivity(intent);
                    }
                }

            }
        } else {
            alert(getResources().getString(R.string.alert_label_oops), getResources().getString(R.string.alert_label_no_google_map_installed));
        }
    }

    private void startServiceNavigation() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (!Settings.canDrawOverlays(TripPageNew.this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, 1234);
            }
        } else {
            if (!isMyServiceRunning(GoogleNavigationService.class)) {
                startService(new Intent(getApplicationContext(), GoogleNavigationService.class));
            }
        }

        String sReturnStateString = "";
        if (isReturnLatLngAvailable) {
            sReturnStateString = "1";
        } else {
            sReturnStateString = "0";
        }

        sessionManager.setGoogleNavigationData(sPickUpAddress, Str_RideId, Str_pickUp_Lat, Str_pickUp_Long, Str_username, Str_user_rating, Str_user_phoneno,
                Str_user_img, Suser_Id, Str_droplat, Str_droplon, str_drop_location, title_text, subtitle_text, Trip_Status, eta_arrival, location,
                member_since, user_gender, est_cost, sCurrencySymbol, sMultipleDropStatus, sTripMode, multipleDropList, isReturnAvailable, sReturnState, sReturnStateString, sReturnLat, sReturnLng, sReturnPickUpLat, sReturnPickUpLng,
                sPaymentType, sCancelledRides, sCompletedRides, sRiderType);

    }

    private boolean isGoogleMapsInstalled() {
        try {
            ApplicationInfo info = getPackageManager().getApplicationInfo("com.google.android.apps.maps", 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }


    @SuppressLint("WrongConstant")
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        boolean b = false;
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                b = true;
                break;
            } else {
                b = false;
            }
        }
        System.out.println("3 not running");
        return b;
    }


    private boolean checkWriteExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }


    private void requestNavigationPermission() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_NAVIGATION_CODE);
    }


    //-----------------------PhoneMasking Post Request-----------------
    private void postRequest_PhoneMasking(String Url) {

        dialog = new Dialog(TripPageNew.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_pleasewait));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("ride_id", Str_RideId);
        jsonParams.put("user_type", "driver");

        System.out.println("-------------PhoneMasking Url----------------" + Url);
        mRequest = new ServiceRequest(TripPageNew.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------PhoneMasking Response----------------" + response);

                String Str_status = "", SResponse = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");
                    SResponse = object.getString("response");
                    if (Str_status.equalsIgnoreCase("1")) {
                        alert(getResources().getString(R.string.action_success), SResponse);
                    } else {
                        alert(getResources().getString(R.string.action_error), SResponse);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    dialog.dismiss();
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }


    private void PostRequestCancel(String Url) {
        dialog = new Dialog(TripPageNew.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_pleasewait));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", driver_id);
        jsonParams.put("ride_id", Str_RideId);
        System.out.println("-------------------cancel-jsonParams----------------------" + jsonParams);
        mRequest = new ServiceRequest(TripPageNew.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                String Sstatus = "", Str_response = "";
                try {
                    System.out.println("-------------------cancel-response----------------------" + response);

                    JSONObject jobject = new JSONObject(response);
                    Sstatus = jobject.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject object = jobject.getJSONObject("response");
                        JSONArray jarry = object.getJSONArray("reason");

                        if (jarry.length() > 0) {
                            Cancelreason_arraylist.clear();
                            cancelReasonsList = new ArrayList<CancelTripPojo>();
                            for (int i = 0; i < jarry.length(); i++) {

                                JSONObject object1 = jarry.getJSONObject(i);

//                                CancelReasonPojo items = new CancelReasonPojo();

                                CancelTripPojo pojo = new CancelTripPojo();

                                pojo.setReason(object1.getString("reason"));
                                pojo.setReasonId(object1.getString("id"));
                                pojo.setSetPayment_selected_cancel_id("false");

//                                items.setReason(object1.getString("reason"));
//                                items.setCancelreason_id(object1.getString("id"));

                                System.out.println("reason----------" + object1.getString("reason"));

//                                Cancelreason_arraylist.add(items);
                                cancelReasonsList.add(pojo);

                            }
                            show_progress_status = true;

                        } else {
                            show_progress_status = false;
                        }
                    } else {
                        Str_response = jobject.getString("response");
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                if (Sstatus.equalsIgnoreCase("1")) {

                    if (show_progress_status) {
//                        cancelDialog();

                        Intent passIntent = new Intent(TripPageNew.this, TrackRideCancelTrip.class);
                        Bundle bundleObject = new Bundle();
                        bundleObject.putSerializable("Reason", cancelReasonsList);
                        passIntent.putExtras(bundleObject);

                        passIntent.putExtra("driverImage", Str_user_img);
                        passIntent.putExtra("RideID", Str_RideId);
                        startActivity(passIntent);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                    }

                } else {
                    alert(getResources().getString(R.string.action_error), Str_response);
                }


                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void cancelDialog() {

        cd = new ConnectionDetector(TripPageNew.this);
        isInternetPresent = cd.isConnectingToInternet();

        cancel_dialog = new Dialog(TripPageNew.this);
        cancel_dialog.getWindow();
        cancel_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        cancel_dialog.setContentView(R.layout.cancel_reason_alert);
        cancel_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        cancel_dialog.setCanceledOnTouchOutside(true);
        cancel_dialog.show();
        cancel_dialog.getWindow().setGravity(Gravity.CENTER);

        cancel_list = (ListView) cancel_dialog.findViewById(R.id.cancel_alert_listview);
        Tv_close = (TextView) cancel_dialog.findViewById(R.id.cancel_alert_back_textview);
        TextView Tv_ok = (TextView) cancel_dialog.findViewById(R.id.cancel_alert_confirm_textview);

        CancelAlertAdapter = new CancelAlertAdapter(TripPageNew.this, Cancelreason_arraylist);
        cancel_list.setAdapter(CancelAlertAdapter);

        cancel_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                reason_id = Cancelreason_arraylist.get(position).getCancelreason_id();

                for (int i = 0; i < Cancelreason_arraylist.size(); i++) {
                    if (i == position) {
                        Cancelreason_arraylist.get(i).setCancelstatus("1");
                    } else {
                        Cancelreason_arraylist.get(i).setCancelstatus("0");
                    }
                }
                CancelAlertAdapter.notifyDataSetChanged();
            }
        });
        Tv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                HashMap<String, String> jsonParams = new HashMap<String, String>();
                jsonParams.put("driver_id", driver_id);
                jsonParams.put("reason", reason_id);
                jsonParams.put("ride_id", Str_RideId);

                if (isInternetPresent) {
                    cancel_dialog.dismiss();
                    postRequestCancelurl(Iconstant.driverCancel_Url, jsonParams);
                } else {
                    alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));

                }
            }
        });

        Tv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel_dialog.dismiss();
            }
        });
    }

    private void postRequestCancelurl(String url, HashMap<String, String> jsonParams) {
        dialog = new Dialog(TripPageNew.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        System.out.println("-------------cancelling----------------" + url);


        System.out.println("jsonParams-------------" + jsonParams);

        mRequest = new ServiceRequest(TripPageNew.this);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------- Response----------------" + response);
                String Str_status = "", Str_message = "", Str_Id = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");
                    JSONObject jobject = object.getJSONObject("response");
                    Str_message = jobject.getString("message");
                    Str_Id = jobject.getString("ride_id");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (Str_status.equalsIgnoreCase("1")) {

                    sessionManager.setUpdateLocationServiceState("");
                    alert_Cancel(getResources().getString(R.string.action_success), Str_message);
                } else {
                    alert(getResources().getString(R.string.alert_label_title), Str_message);
                }
                dialog.dismiss();

            }

            @Override
            public void onErrorListener() {

                dialog.dismiss();
            }

        });
    }


    //-----------------------drop ride start ride  Post Request-----------------
    @SuppressLint("WrongConstant")
    private void postRequestDropAndStartRide(String Url, final String mode, String recordId) {

        dialog = new Dialog(TripPageNew.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        final TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_pleasewait));


        System.out.println("-------------start and drop url----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("ride_id", Str_RideId);
        jsonParams.put("latitude", String.valueOf(MyCurrent_lat));
        jsonParams.put("longitude", String.valueOf(MyCurrent_long));
        jsonParams.put("driver_id", driver_id);
        jsonParams.put("mode", mode);
        jsonParams.put("record_id", recordId);

        System.out.println("-------------start and drop jsonParams----------------" + jsonParams);

        mRequest = new ServiceRequest(TripPageNew.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------latlongupdate----------------" + response);

                String dropStatus = "", status = "";

                try {
                    JSONObject object = new JSONObject(response);
                    status = object.getString("status");
                    if ("1".equalsIgnoreCase(status)) {
                        JSONObject jsonObject = object.getJSONObject("response");
                        sRecordId = jsonObject.getString("record_id");
                        dropStatus = jsonObject.getString("multi_drop_button_status");

                        if (jsonObject.has("multistop_array")) {

                            System.out.println("--------prem inside multistop_array-----------");

                            Object multiDrop_array_check = jsonObject.get("multistop_array");
                            if (multiDrop_array_check instanceof JSONArray) {
                                JSONArray multiDropArray = jsonObject.getJSONArray("multistop_array");
                                if (multiDropArray.length() > 0) {
                                    multipleDropList.clear();
                                    for (int i = 0; i < multiDropArray.length(); i++) {
                                        JSONObject multipleDropArrayObject = multiDropArray.getJSONObject(i);

                                        MultipleLocationPojo pojo = new MultipleLocationPojo();
                                        pojo.setDropLocation(multipleDropArrayObject.getString("location"));
                                        pojo.setIsEnd(multipleDropArrayObject.getString("is_end"));
                                        pojo.setMode(multipleDropArrayObject.getString("mode"));
                                        pojo.setRecordId(multipleDropArrayObject.getString("record_id"));

                                        JSONObject latLngArrayObject = multipleDropArrayObject.getJSONObject("latlong");
                                        if (latLngArrayObject.length() > 0) {
                                            pojo.setDropLat(latLngArrayObject.getString("lat"));
                                            pojo.setDropLon(latLngArrayObject.getString("lon"));
                                        }

                                        multipleDropList.add(pojo);
                                    }

                                    for (int j = 0; j <= multipleDropList.size() - 1; j++) {
                                        String mode = multipleDropList.get(j).getMode();
                                        String value = multipleDropList.get(j).getMode();
                                        String valuea = multipleDropList.get(j).getMode();

                                        System.out.println("-------" + value + "_-----------");

                                        System.out.println("--------prem inside multistop_array-----------");


                                        if (mode.equalsIgnoreCase("start")) {
                                            String first_text = "DROP OFF   ";
                                            Tv_finishTo.setText(first_text + multipleDropList.get(j).getDropLocation());
                                            j = multipleDropList.size();
                                        } else {
                                            String first_text = "To  ";
                                            Tv_finishTo.setText(first_text + sessionManager.getfinaldestinaltion());
                                        }
                                    }

                                    isMultipleDropAvailable = true;
                                    sMultipleDropStatus = "1";

                                } else {
                                    multipleDropList.clear();
                                    sMultipleDropStatus = "0";
                                    isMultipleDropAvailable = false;
                                }
                            } else {
                                multipleDropList.clear();
                                sMultipleDropStatus = "0";
                                isMultipleDropAvailable = false;
                            }
                        } else {
                            sMultipleDropStatus = "0";
                            isMultipleDropAvailable = false;
                        }


                        if (isMultipleDropAvailable) {
                            for (int j = 0; j < multipleDropList.size(); j++) {

                                if (multipleDropList.get(j).getIsEnd().equalsIgnoreCase("0")) {

                                    MultiIsEnd = multipleDropList.get(j).getIsEnd();
                                    MultiMode = multipleDropList.get(j).getMode();
                                    MultiRecordId = multipleDropList.get(j).getRecordId();


                                    sEndNavigationLat = multipleDropList.get(j).getDropLat();
                                    sEndNavigationLng = multipleDropList.get(j).getDropLon();
                                    endTrip_NavigationLatLng = new LatLng(Double.parseDouble(sEndNavigationLat), Double.parseDouble(sEndNavigationLng));
                                    break;
                                } else {
                                    sEndNavigationLat = Str_droplat;
                                    sEndNavigationLng = Str_droplon;
                                    endTrip_NavigationLatLng = new LatLng(Double.parseDouble(Str_droplat), Double.parseDouble(Str_droplon));
                                }
                            }
                        } else {
                            sEndNavigationLat = Str_droplat;
                            sEndNavigationLng = Str_droplon;
                        }


                        if (mode.equalsIgnoreCase("end")) {
                            RL_multiDrop.setBackgroundResource(R.drawable.red_square);
                            Tv_multiDrop.setText(getResources().getString(R.string.trip_label_drop));
                            multipleDropCount = 0;
                            SetRouteMap(cur_latlong, endTrip_NavigationLatLng);
                        } else {
                            RL_multiDrop.setBackgroundResource(R.drawable.blue_square);
                            Tv_multiDrop.setText(getResources().getString(R.string.trip_label_start));
                            multipleDropCount = 1;
                        }

                        if (dropStatus.equalsIgnoreCase("0")) {
                            RL_multiDrop.setVisibility(View.GONE);
                        } else {
                            RL_multiDrop.setVisibility(View.VISIBLE);
                            if (!mode.equalsIgnoreCase("end")) {
                                Intent intent = new Intent(TripPageNew.this, WaitingTimeDrop.class);
                                intent.putExtra("rideID", Str_RideId);
                                intent.putExtra("isContinue", false);
                                intent.putExtra("recordId", MultiRecordId);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.enter, R.anim.exit);
                            }
                        }

                    }

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {

                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    //-----------------------Return Trip  Post Request-----------------
    @SuppressWarnings("WrongConstant")
    private void postRequestReturnTrip(String Url, final String mode) {

        dialog = new Dialog(TripPageNew.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        final TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_pleasewait));


        System.out.println("-------------Return Trip url----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("ride_id", Str_RideId);
        jsonParams.put("latitude", String.valueOf(MyCurrent_lat));
        jsonParams.put("longitude", String.valueOf(MyCurrent_long));
        jsonParams.put("driver_id", driver_id);
        jsonParams.put("mode", mode);

        System.out.println("------------Return Trip jsonParams----------------" + jsonParams);

        mRequest = new ServiceRequest(TripPageNew.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Return Trip response----------------" + response);

                String status = "", nextDropLocation = "";

                try {
                    JSONObject object = new JSONObject(response);
                    status = object.getString("status");
                    if ("1".equalsIgnoreCase(status)) {
                        JSONObject jsonObject = object.getJSONObject("response");
                        isReturnAvailable = jsonObject.getString("is_return_over");
                        sReturnState = jsonObject.getString("mode");


                        Object return_array_check = jsonObject.get("next_drop_location");
                        if (return_array_check instanceof JSONObject) {
                            JSONObject returnArrayObject = jsonObject.getJSONObject("next_drop_location");
                            JSONObject latLngArrayObject = returnArrayObject.getJSONObject("latlong");
                            if (latLngArrayObject.length() > 0) {
                                sReturnLat = latLngArrayObject.getString("lat");
                                sReturnLng = latLngArrayObject.getString("lon");

                                if (returnArrayObject.has("location")) {
                                    nextDropLocation = returnArrayObject.getString("location");
                                }

                                isReturnLatLngAvailable = true;
                            } else {
                                isReturnLatLngAvailable = false;
                            }
                        } else {
                            isReturnLatLngAvailable = false;
                        }

                        if (sReturnState.equalsIgnoreCase("start")) {
                            RL_returnTrip.setBackgroundResource(R.drawable.red_square);
                            Tv_returnTrip.setText(getResources().getString(R.string.trip_label_return));
                        } else if (sReturnState.equalsIgnoreCase("over")) {
                            if (isReturnLatLngAvailable) {
                                return_pickUp_latLong = new LatLng(MyCurrent_lat, MyCurrent_long);
                                return_latLong = new LatLng(Double.parseDouble(sReturnLat), Double.parseDouble(sReturnLng));
                                SetRouteMap(return_pickUp_latLong, return_latLong);
                            }
                        }

                        if (nextDropLocation.length() > 0) {
                            Tv_finishTo.setText(getResources().getString(R.string.trip_label_to) + nextDropLocation);
                        }


                        if (isReturnAvailable.equalsIgnoreCase("0")) {
                            RL_returnTrip.setVisibility(View.VISIBLE);
                        } else {
                            RL_returnTrip.setVisibility(View.GONE);
                        }

                        if (mode.equalsIgnoreCase("drop")) {
                            Intent intent = new Intent(TripPageNew.this, WaitingTimeDrop.class);
                            intent.putExtra("rideID", Str_RideId);
                            intent.putExtra("isContinue", false);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("recordId", MultiRecordId);
                            startActivity(intent);
                            overridePendingTransition(R.anim.enter, R.anim.exit);
                        }


                    }

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {

                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    @SuppressWarnings("WrongConstant")
    private void PostRequest(String Url, final String trip_status) {

        if (!trip_status.equals("end")) {
            dialog = new Dialog(TripPageNew.this);
            dialog.getWindow();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.custom_loading);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_pleasewait));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", driver_id);
        jsonParams.put("ride_id", Str_RideId);
        if (trip_status.equals("begin")) {
            jsonParams.put("pickup_lat", String.valueOf(MyCurrent_lat));
            jsonParams.put("pickup_lon", String.valueOf(MyCurrent_long));
        } else if (trip_status.equals("end")) {

            ArrayList<String> travel_history = myDBHelper.getDataEndTrip(Str_RideId);

            System.out.println("-----------jai---total_distance-------------------" + travel_history.toString().replace("[", "").replace("]", "").replace(" ", ""));
            StringBuilder builder = new StringBuilder();
            for (String string : travel_history) {
                builder.append("," + string);
            }
            ArrayList<LatLng> distance_travelled = myDBHelper.getDisEndTrip(Str_RideId);
            calculateDistance(distance_travelled);

            jsonParams.put("drop_lat", String.valueOf(MyCurrent_lat));
            jsonParams.put("drop_lon", String.valueOf(MyCurrent_long));
            jsonParams.put("drop_loc", sCurrentLocationName);
            jsonParams.put("distance", String.valueOf(totalDistanceTravelled / 1000));
            jsonParams.put("travel_history", builder.toString());
            jsonParams.put("wait_time", "");
        }

        System.out.println("--------------TRIP Url-------------------" + Url);
        System.out.println("--------------jsonParams-------------------" + jsonParams);
        System.out.println("--------------trip_status-------------------" + trip_status);
        mRequest = new ServiceRequest(TripPageNew.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------response-------------------" + response);
                String status = "", need_payment = "", ride_view = "", drop_location = "", /*est_cost = "",*/ currency_code = "", ride_id = "", sCurrencySymbol = "", rider_image = "", mode = "";

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {
                        status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {

                            if (!trip_status.equals("end")) {
                                ride_view = object.getString("ride_view");
                                drop_location = object.getString("drop_location");
                                final_droplocation = drop_location;
                                est_cost = object.getString("est_cost");
                                currency_code = object.getString("currency_code");
                                ride_id = object.getString("ride_id");
                                title_text = object.getString("title_text");
                                subtitle_text = object.getString("subtitle_text");

                                if (object.has("is_return_over")) {
                                    isReturnAvailable = object.getString("is_return_over");
                                }

                                if (object.has("return_mode")) {
                                    sReturnState = object.getString("return_mode");
                                }

                                if (object.has("mode")) {
                                    sTripMode = object.getString("mode");
                                }

                                sCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(currency_code);

                                if (object.has("additional_drop_loc")) {
                                    Object multiDrop_array_check = object.get("additional_drop_loc");
                                    if (multiDrop_array_check instanceof JSONArray) {
                                        JSONArray multiDropArray = object.getJSONArray("additional_drop_loc");
                                        if (multiDropArray.length() > 0) {
                                            multipleDropList.clear();
                                            for (int i = 0; i < multiDropArray.length(); i++) {
                                                JSONObject multipleDropArrayObject = multiDropArray.getJSONObject(i);

                                                MultipleLocationPojo pojo = new MultipleLocationPojo();
                                                pojo.setDropLocation(multipleDropArrayObject.getString("location"));
                                                pojo.setIsEnd(multipleDropArrayObject.getString("is_end"));
                                                pojo.setMode(multipleDropArrayObject.getString("mode"));
                                                pojo.setRecordId(multipleDropArrayObject.getString("record_id"));

                                                JSONObject latLngArrayObject = multipleDropArrayObject.getJSONObject("latlong");
                                                if (latLngArrayObject.length() > 0) {
                                                    pojo.setDropLat(latLngArrayObject.getString("lat"));
                                                    pojo.setDropLon(latLngArrayObject.getString("lon"));
                                                }

                                                multipleDropList.add(pojo);

                                            }

                                            for (int j = 0; j <= multipleDropList.size() - 1; j++) {
                                                String modes = multipleDropList.get(j).getMode();
                                                if (modes.equalsIgnoreCase("start")) {
                                                    String first_text = "DROP OFF   ";
                                                    Tv_finishTo.setText(first_text + multipleDropList.get(j).getDropLocation());
                                                    str_drop_location = multipleDropList.get(j).getDropLocation();
                                                    drop_location = multipleDropList.get(j).getDropLocation();
                                                    mode = modes;
                                                    j = multipleDropList.size();
                                                } else {
                                                    drop_location = drop_location;
                                                }
                                            }


                                            isMultipleDropAvailable = true;
                                            sMultipleDropStatus = "1";

                                        } else {
                                            multipleDropList.clear();
                                            sMultipleDropStatus = "0";
                                            isMultipleDropAvailable = false;
                                            sEndNavigationLat = Str_droplat;
                                            sEndNavigationLng = Str_droplon;
                                        }
                                    } else {
                                        multipleDropList.clear();
                                        sMultipleDropStatus = "0";
                                        isMultipleDropAvailable = false;
                                        sEndNavigationLat = Str_droplat;
                                        sEndNavigationLng = Str_droplon;
                                    }
                                } else {
                                    sMultipleDropStatus = "0";
                                    isMultipleDropAvailable = false;
                                    sEndNavigationLat = Str_droplat;
                                    sEndNavigationLng = Str_droplon;
                                }


                                for (int j = 0; j < multipleDropList.size(); j++) {

                                    System.out.println("------multipleDropList------------" + multipleDropList.get(j));

                                    if (multipleDropList.get(j).getIsEnd().equalsIgnoreCase("0")) {


                                        MultiIsEnd = multipleDropList.get(j).getIsEnd();
                                        MultiMode = multipleDropList.get(j).getMode();
                                        MultiRecordId = multipleDropList.get(j).getRecordId();


                                        sEndNavigationLat = multipleDropList.get(j).getDropLat();
                                        sEndNavigationLng = multipleDropList.get(j).getDropLon();
                                        endTrip_NavigationLatLng = new LatLng(Double.parseDouble(sEndNavigationLat), Double.parseDouble(sEndNavigationLng));
                                        sRecordId = multipleDropList.get(j).getRecordId();

                                        RL_multiDrop.setBackgroundResource(R.drawable.red_square);
                                        Tv_multiDrop.setText(getResources().getString(R.string.trip_label_drop));
                                        multipleDropCount = 0;

                                        RL_multiDrop.setVisibility(View.VISIBLE);

                                        break;
                                    } else {
                                        sEndNavigationLat = Str_droplat;
                                        sEndNavigationLng = Str_droplon;
                                        endTrip_NavigationLatLng = new LatLng(Double.parseDouble(Str_droplat), Double.parseDouble(Str_droplon));
                                        RL_multiDrop.setVisibility(View.GONE);
                                    }
                                }

                            } else {
                                sEndNavigationLat = Str_droplat;
                                sEndNavigationLng = Str_droplon;
                                JSONObject res_object = object.getJSONObject("response");
                                need_payment = res_object.getString("need_payment");
                                rider_image = res_object.getString("user_image");

                            }


                            if (trip_status.equals("arrived")) {
                                Trip_Status = "begin";
                                pickup_latlong = new LatLng(MyCurrent_lat, MyCurrent_long);
                                Begin(drop_location, sCurrencySymbol + " " + est_cost, ride_id, title_text, subtitle_text);
                            } else if (trip_status.equals("begin")) {

                                if (sTripMode.length() > 0) {
                                    if (sTripMode.equalsIgnoreCase("oneway")) {
//                                        Iv_tripWayIcon.setImageResource(R.drawable.one_way_icon);
                                    } else if (sTripMode.equalsIgnoreCase("return")) {
//                                        Iv_tripWayIcon.setImageResource(R.drawable.return_way_icon);
                                    }
                                }

                                Trip_Status = "end";
                                End(drop_location, sCurrencySymbol + " " + est_cost, ride_id, title_text, subtitle_text, mode);
                            } else if (trip_status.equals("end")) {
                                myDBHelper.insertDriverStatus("service_stop");
                                sessionManager.setUpdateLocationServiceState("");



                                Intent intent = new Intent(TripPageNew.this, FarePage.class);
                                intent.putExtra("rideId", Str_RideId);
                                startActivity(intent);
                                finish();
                                overridePendingTransition(R.anim.enter, R.anim.exit);

                            }
                        } else {
                            if (status.equalsIgnoreCase("0")) {
                                String sResponse = object.getString("response");
                                String ride_view1 = object.getString("ride_view");
                                if (ride_view1.equals("detail")) {
                                    sessionManager.setUpdateLocationServiceState("");
                                    alert_Cancel(getResources().getString(R.string.action_error), sResponse);
                                } else if (ride_view1.equalsIgnoreCase("stay")) {
                                    alert(getResources().getString(R.string.action_error), sResponse);
                                } else if (ride_view1.equalsIgnoreCase("next")) {

                                    if (trip_status.equals("arrived")) {
                                        Trip_Status = "begin";
                                        pickup_latlong = new LatLng(MyCurrent_lat, MyCurrent_long);
                                        Begin(drop_location, sCurrencySymbol + " " + est_cost, ride_id, title_text, subtitle_text);
                                    } else if (trip_status.equals("begin")) {
                                        Trip_Status = "end";
                                        End(drop_location, sCurrencySymbol + " " + est_cost, ride_id, title_text, subtitle_text, mode);
                                    } else if (trip_status.equals("end")) {
                                        myDBHelper.insertDriverStatus("service_stop");
                                        sessionManager.setUpdateLocationServiceState("");
                                        Intent intent = new Intent(TripPageNew.this, FarePage.class);
                                        intent.putExtra("rideId", Str_RideId);
                                        startActivity(intent);
                                        finish();
                                        overridePendingTransition(R.anim.enter, R.anim.exit);

                                    }
                                } else {
                                    alert(getResources().getString(R.string.action_error), sResponse);
                                }
                            }
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void calculateDistance(ArrayList<LatLng> points) {

        float tempTotalDistance = 0;

        for (int i = 0; i < points.size() - 1; i++) {
            LatLng pointA = points.get(i);
            LatLng pointB = points.get(i + 1);
            float[] results = new float[3];
            Location.distanceBetween(pointA.latitude, pointA.longitude, pointB.latitude, pointB.longitude, results);
            tempTotalDistance += results[0];
        }

        totalDistanceTravelled = tempTotalDistance;
    }

    @SuppressWarnings("WrongConstant")
    private void arrived(String drop, String eta, String ride_id, String location, String member_since, String user_gender, String Str_username, String Str_user_img) {

        myDBHelper.insertDriverStatus("arrived");

        RL_arrive.setVisibility(View.VISIBLE);
        RL_profileArrive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRiderProfile();
            }
        });
        Tv_arriveRiderName.setText(getResources().getString(R.string.mytrips_label_view_profile));
        Tv_arriveRideId.setText(/*getResources().getString(R.string.trip_label_crn_no) + Str_RideId*/Str_username);
        try {
//            String text1=str_drop_location.replaceAll("\n", " ").toUpperCase();
            String text1 = sPickUpAddress.replaceAll("\n", " ").toUpperCase();
            String colorCodeStart = "<font color='#ffcc00'>";  // use any color as  your want
            String colorCodeEnd = "</font>";
            String text = colorCodeStart + "PICKUP :" + " " + colorCodeEnd;
            Tv_arriveFrom.setText(Html.fromHtml(text + text1));

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (Str_user_img.length() > 0) {
            Picasso.with(TripPageNew.this).load(Str_user_img).error(R.drawable.no_user_img).error(R.drawable.no_user_img)/*.resize(120, 150)*/.fit().into(Iv_arriveRiderProfile);
        }

        Iv_time.setImageResource(R.drawable.clock_blue_new);
        Iv_distance.setImageResource(R.drawable.meter_blue);

        LL_call_and_cancel.setVisibility(View.VISIBLE);
        LL_trip_info_and_sos_bike.setVisibility(View.GONE);
        LL_trip_info_and_sos_car.setVisibility(View.GONE);

        RL_start.setVisibility(View.GONE);
        RL_finish.setVisibility(View.GONE);

        Tv_time.setText(eta);

        Tv_ride.setText(getResources().getString(R.string.trip_page_label_tap_to_arrive));
        RL_ride.setBackgroundResource(R.drawable.arrive_gradient_bg);

        SetRouteMap(cur_latlong, pickup_latlong);
    }

    @SuppressWarnings("WrongConstant")
    private void Begin(String drop, String eta, String ride_id, String title, String subtitle_text) {

        myDBHelper.insertDriverStatus("begin");

        RL_arrive.setVisibility(View.GONE);

        RL_start.setVisibility(View.VISIBLE);
        RL_profileStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRiderProfile();
            }
        });
        Tv_startRiderName.setText(getResources().getString(R.string.mytrips_label_view_profile));
        Tv_startName.setText(Str_username);/*driver_name*/


        /*if (est_cost.length() > 0) {
            Tv_startAmount.setText(eta);
            Tv_startAmount.setVisibility(View.VISIBLE);
        } else {
            Tv_startAmount.setVisibility(View.GONE);
        }*/


        String first_text = "<font color='#ffcc00'>" + "To " + " " + "</font>";
        String second_text = str_drop_location.replaceAll("\n", " ").toUpperCase();
        Tv_startTo.setText(Html.fromHtml(first_text + second_text), TextView.BufferType.SPANNABLE);


        if (sTripMode.length() > 0) {
            if (sTripMode.equalsIgnoreCase("oneway")) {
                Tv_finishRideStatus1.setText(getResources().getString(R.string.one_way_lable));
                Iv_finishStatus1.setImageResource(R.drawable.trip_oneway_arrow);
                RL_returnTrip.setVisibility(View.GONE);

            } else if (sTripMode.equalsIgnoreCase("return")) {

                Tv_finishRideStatus1.setText(getResources().getString(R.string.return_lable));
                Iv_finishStatus1.setImageResource(R.drawable.trip_return_arrow);
                System.out.println("-----------prem isReturnAvailable---------" + isReturnAvailable);
            } else if (sTripMode.equalsIgnoreCase("multistop")) {
                Tv_finishRideStatus1.setText(getResources().getString(R.string.trip_label_multistopover));
                Iv_finishStatus1.setImageResource(R.drawable.trip_stopover_arrow);
                RL_returnTrip.setVisibility(View.GONE);
            }
        }


        if (Str_user_img.length() > 0) {
            Picasso.with(TripPageNew.this).load(Str_user_img).error(R.drawable.no_user_img).error(R.drawable.no_user_img)/*.resize(120, 150)*/.fit().into(Iv_startRiderProfile);
        }

        Iv_time.setImageResource(R.drawable.green_clock);
        Iv_distance.setImageResource(R.drawable.meter_green);

        LL_call_and_cancel.setVisibility(View.VISIBLE);
        LL_trip_info_and_sos_bike.setVisibility(View.GONE);
        LL_trip_info_and_sos_car.setVisibility(View.GONE);

        RL_finish.setVisibility(View.GONE);

        Tv_ride.setText(getResources().getString(R.string.trip_page_label_start_now));
//        Tv_ride.setTextColor(Color.BLACK);
        RL_ride.setBackgroundResource(R.drawable.begin_gradient_bg);
//        Tv_ride.setBackgroundResource(R.drawable.bg_start);


        SetRouteMap(cur_latlong, drop_latlong);
    }

    @SuppressWarnings("WrongConstant")
    private void End(String drop, String eta, String ride_id, String title, String subtitle_text, String mode) {

        myDBHelper.insertDriverStatus("end");
//        arrived_layout.setVisibility(View.INVISIBLE);
//        Trip_layout.setVisibility(View.GONE);

        RL_arrive.setVisibility(View.GONE);
        RL_start.setVisibility(View.GONE);

        RL_finish.setVisibility(View.VISIBLE);
//        Tv_finishRideStatus = (TextView) findViewById(R.id.txt_trip_status_finish);
        if (mode.equalsIgnoreCase("start")) {
            String first_text = "DROP OFF   ";
            Tv_finishTo.setText(first_text + drop);
        } else {
            String first_text = "To ";
            if (str_drop_location.equalsIgnoreCase("")) {
                Tv_finishTo.setText(first_text + sessionManager.getfinaldestinaltion());
            } else {
                Tv_finishTo.setText(first_text + drop);
            }
        }

        Iv_time.setImageResource(R.drawable.clock_blue_new);
        Iv_distance.setImageResource(R.drawable.meter_blue);

        LL_call_and_cancel.setVisibility(View.GONE);


        LL_trip_info_and_sos_bike.setVisibility(View.VISIBLE);
        LL_trip_info_and_sos_car.setVisibility(View.GONE);

       /* if (sVehicleType.equalsIgnoreCase("motorbike")) {
            LL_trip_info_and_sos_bike.setVisibility(View.VISIBLE);
            LL_trip_info_and_sos_car.setVisibility(View.GONE);
        } else {
            LL_trip_info_and_sos_bike.setVisibility(View.GONE);
            LL_trip_info_and_sos_car.setVisibility(View.VISIBLE);
        }*/
        if (est_cost.length() > 0) {
            Tv_startAmount.setText(getResources().getString(R.string.estimated_lable) + " " + eta);
            Tv_startAmount.setVisibility(View.VISIBLE);
        } else {
            Tv_startAmount.setVisibility(View.GONE);
        }

//        Tv_ride.setBackgroundResource(R.drawable.bg_finish);
//        Tv_ride.setTextColor(Color.BLACK);
        RL_ride.setBackgroundResource(R.drawable.finish_gradient_bg);
        Tv_ride.setText(getResources().getString(R.string.trip_page_label_finish));


        if (sTripMode.length() > 0) {
            if (sTripMode.equalsIgnoreCase("oneway")) {

                Tv_finishRideStatus.setText(getResources().getString(R.string.on_trip_lable) /*+ " - " + getResources().getString(R.string.one_way_lable)*/);
                Iv_finishStatus.setImageResource(R.drawable.trip_oneway_arrow);
                RL_returnTrip.setVisibility(View.GONE);

            } else if (sTripMode.equalsIgnoreCase("return")) {

                Tv_finishRideStatus.setText(getResources().getString(R.string.on_trip_lable) /*+ " - " + getResources().getString(R.string.return_lable)*/);
                Iv_finishStatus.setImageResource(R.drawable.trip_return_arrow);

                System.out.println("-----------prem isReturnAvailable---------" + isReturnAvailable);
                if (isReturnAvailable.length() > 0) {
                    if (isReturnAvailable.equalsIgnoreCase("0")) {

                        System.out.println("---------prem sReturnState-----------" + sReturnState);

                        RL_returnTrip.setVisibility(View.VISIBLE);

                        if (sReturnState.equalsIgnoreCase("drop")) {
                            Tv_returnTrip.setText(getResources().getString(R.string.trip_label_drop));
                            RL_returnTrip.setBackgroundResource(R.drawable.blue_square);
                        } else if (sReturnState.equalsIgnoreCase("start")) {
                            Tv_returnTrip.setText(getResources().getString(R.string.trip_label_return));
                            RL_returnTrip.setBackgroundResource(R.drawable.red_square);
                        }

                    } else {
                        RL_returnTrip.setVisibility(View.GONE);
                    }
                } else {
                    RL_returnTrip.setVisibility(View.GONE);
                }

            } else if (sTripMode.equalsIgnoreCase("multistop")) {
                Tv_finishRideStatus.setText(getResources().getString(R.string.on_trip_lable) /*+ " - " + getResources().getString(R.string.trip_label_multistop)*/);
                Iv_finishStatus.setImageResource(R.drawable.trip_stopover_arrow);
                RL_returnTrip.setVisibility(View.GONE);
            }
        }


        if (sTripMode.equalsIgnoreCase("return")) {
            if (sReturnLocation.length() > 0) {
                Tv_finishTo.setText(getResources().getString(R.string.trip_label_to) + sReturnLocation);
            }
        }


        if (isMultipleDropAvailable) {
            SetRouteMap(pickup_multiStop_latlong, endTrip_NavigationLatLng);
        } else if (sTripMode.equalsIgnoreCase("return")) {
            if (isReturnAvailable.equalsIgnoreCase("1")) {
                SetRouteMap(return_pickUp_latLong, return_latLong);
            } else {
                SetRouteMap(pickup_latlong, drop_latlong);
            }
        } else {
            SetRouteMap(pickup_latlong, drop_latlong);
        }

    }

    private void alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(TripPageNew.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void alert_Cancel(String title, String alert) {
        final PkDialog mDialog = new PkDialog(TripPageNew.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setCancelOnTouchOutside(false);


        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();


                Intent finish_GoOnlinePage = new Intent();
                finish_GoOnlinePage.setAction("com.app.finish.GoOnlinePage");
                sendBroadcast(finish_GoOnlinePage);

                Intent intent = new Intent(TripPageNew.this, OnlinepageConstrain.class);
                startActivity(intent);

                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);


            }
        });
        mDialog.show();
    }

    public class GetRouteTask extends AsyncTask<String, Void, String> {

        String response = "";
        LatLng from_LatLng, to_LatLng;
        private ArrayList<LatLng> wayLatLng;

        GetRouteTask(LatLng from, LatLng to) {
            from_LatLng = to;
            to_LatLng = from;
            startWayPoint = from;
            endWayPoint = to;
            wayLatLng = addWayPointPoint(from, to, multipleDropList);
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... urls) {

            System.out.println("-------prem wayLatLng---------" + wayLatLng);

            Routing routing = new Routing.Builder()
                    .travelMode(AbstractRouting.TravelMode.DRIVING)
                    .withListener(TripPageNew.this)
                    .alternativeRoutes(true)
                    .key(Iconstant.place_search_key)
                    .waypoints(wayLatLng)
                    .build();
            routing.execute();


            response = "Success";
            return response;

        }

        @Override
        protected void onPostExecute(String result) {
            if (result.equalsIgnoreCase("Success")) {
            }
        }
    }


    //Enabling Gps Service
    private void enableGpsService() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final com.google.android.gms.common.api.Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        //...
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(TripPageNew.this, REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });
    }


    @SuppressLint("MissingPermission")
    @Override
    public void onConnected(Bundle bundle) {

        System.out.println("on---onConnected----------jai-");

        myLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

    }

    @Override
    public void onConnectionSuspended(int i) {
        System.out.println("on---onConnectionSuspended----------jai-");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        System.out.println("on---onConnectionFailed----------jai-");
    }

    @Override
    public void onLocationChanged(final Location location) {

        System.out.println("on---locationchange----------jai-");

//        addPulsatingEffect(new LatLng(location.getLatitude(), location.getLongitude()), location.getAccuracy());

        this.myLocation = location;
        if (myLocation != null) {

            try {
                LatLng latLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
                cur_latlong = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());

                MyCurrent_lat = myLocation.getLatitude();
                MyCurrent_long = myLocation.getLongitude();

                if (oldLatLng == null) {
                    oldLatLng = latLng;
                }
                newLatLng = latLng;

                if (mLatLngInterpolator == null) {
                    mLatLngInterpolator = new LatLngInterpolator.Linear();
                }

                oldLocation = new Location("");
                oldLocation.setLatitude(oldLatLng.latitude);
                oldLocation.setLongitude(oldLatLng.longitude);
                float bearingValue = oldLocation.bearingTo(location);

                myMovingDistance = oldLocation.distanceTo(location);

                //
                if (myMovingDistance > 10) {

                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            updateEta(oldLocation, location);
                        }
                    });


                }

                System.out.println("moving distance------------" + myMovingDistance);
                System.out.println("moving Trip_Status------------" + Trip_Status);
                if (Trip_Status.equals("end")) {

                    /*code to visible Trip layout after 300m*/
                    Location locationA = new Location("point A");
                    locationA.setLatitude(location.getLatitude());
                    locationA.setLongitude(location.getLongitude());
                    Location locationB = new Location("point B");

                    if (sTripMode.equalsIgnoreCase("return")) {
                        if (isReturnAvailable.equalsIgnoreCase("1")) {
                            SetRouteMap(return_pickUp_latLong, return_latLong);
                            locationB.setLatitude(Double.parseDouble(sReturnLat));
                            locationB.setLongitude(Double.parseDouble(sReturnLng));
                        } else {
                            locationB.setLatitude(Double.parseDouble(Str_droplat));
                            locationB.setLongitude(Double.parseDouble(Str_droplon));
                        }
                    } else {
                        locationB.setLatitude(Double.parseDouble(Str_droplat));
                        locationB.setLongitude(Double.parseDouble(Str_droplon));
                    }

                    float distance = locationA.distanceTo(locationB);
                    /*if (distance <= 300) {
                        Trip_layout.setVisibility(View.VISIBLE);
                    } else {
                        Trip_layout.setVisibility(View.GONE);
                    }*/

                }

                if (googleMap != null) {

                    if (myMovingDistance > 1) {
                        if (currentDriverMarker != null) {
                            if (!String.valueOf(bearingValue).equalsIgnoreCase("NaN")) {
                                if (location.getAccuracy() < 100.0 && location.getSpeed() < 6.95) {
                                    rotateMarker(currentDriverMarker, bearingValue, googleMap);
                                    MarkerAnimation.animateMarkerToGB(currentDriverMarker, latLng, mLatLngInterpolator);
                                    float zoom = googleMap.getCameraPosition().zoom;
                                    CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(zoom).build();
                                    googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                                }
                            }
                        } else {
                            currentDriverMarker.remove();
                            currentDriverMarker = googleMap.addMarker(new MarkerOptions()
                                    .position(latLng)
//                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.carmove))
                                    .icon(BitmapDescriptorFactory.fromBitmap(carIcon))
                                    .anchor(0.5f, 0.5f)
                                    .rotation(myLocation.getBearing())
                                    .flat(true));
                        }
                    }
                }

                oldLatLng = newLatLng;
            } catch (Exception e) {
            }

        }
    }

    //Method to smooth turn marker
    static public void rotateMarker(final Marker marker, final float toRotation, GoogleMap map) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final float startRotation = marker.getRotation();
        final long duration = 1555;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed / duration);

                float rot = t * toRotation + (1 - t) * startRotation;

                marker.setRotation(-rot > 180 ? rot / 2 : rot);
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
            }
        });
    }


    private ArrayList<LatLng> addWayPointPoint(LatLng start, LatLng end, ArrayList<MultipleLocationPojo> mMultipleDropLatLng) {

        if (googleMap != null) {
            googleMap.clear();
            wayPointList.clear();

            wayPointBuilder = new LatLngBounds.Builder();

            wayPointBuilder.include(start);
            wayPointList.add(start);

            if (mMultipleDropLatLng != null) {
                if (isMultipleDropAvailable) {
                    for (int i = 0; i < mMultipleDropLatLng.size(); i++) {

                        String sLat = mMultipleDropLatLng.get(i).getDropLat();
                        String sLng = mMultipleDropLatLng.get(i).getDropLon();

                        double Dlatitude = Double.parseDouble(sLat);
                        double Dlongitude = Double.parseDouble(sLng);

                        // wayPointList.add(new LatLng(Dlatitude, Dlongitude));
                        MarkerOptions marker = new MarkerOptions().position(new LatLng(Dlatitude, Dlongitude));
                        marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.red));

                        // wayPointBuilder.include(new LatLng(Dlatitude, Dlongitude));
                        googleMap.addMarker(marker);
                    }
                }
            }


            wayPointBuilder.include(end);
            wayPointList.add(end);
        }


        return wayPointList;
    }


    @Override
    public void onRoutingFailure(RouteException e) {
        System.out.println("-----------prem onRoutingFailure-----------------" + e);
    }

    @Override
    public void onRoutingStart() {

    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int i) {

        try {
            if (polyLines.size() > 0) {
                for (Polyline poly : polyLines) {
                    poly.remove();
                }
            }

            polyLines = new ArrayList<>();

            PolylineOptions polyOptions = new PolylineOptions();
            polyOptions.color(getResources().getColor(R.color.btn_green_color));
            polyOptions.width(10);
            polyOptions.addAll(route.get(0).getPoints());
            Polyline polyline = googleMap.addPolyline(polyOptions);
            polyLines.add(polyline);

            if (Trip_Status.equals("end")) {

                if (sTripMode.equalsIgnoreCase("oneway")) {
                    googleMap.addMarker(new MarkerOptions()
                            .position(pickup_latlong)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.green)));


                    googleMap.addMarker(new MarkerOptions()
                            .position(drop_latlong)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.red)));
                } else if (sTripMode.equalsIgnoreCase("return")) {

                    if (isReturnAvailable.equalsIgnoreCase("1")) {
                        googleMap.addMarker(new MarkerOptions()
                                .position(return_pickUp_latLong)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.green)));
                        googleMap.addMarker(new MarkerOptions()
                                .position(return_latLong)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.red)));


                    } else {
                        googleMap.addMarker(new MarkerOptions()
                                .position(pickup_latlong)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.green)));
                        googleMap.addMarker(new MarkerOptions()
                                .position(drop_latlong)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.red)));


                    }

                } else if (sTripMode.equalsIgnoreCase("multistop")) {
                    googleMap.addMarker(new MarkerOptions()
                            .position(pickup_latlong)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.green)));
                    googleMap.addMarker(new MarkerOptions()
                            .position(drop_latlong)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.red)));


                }
            } else if (Trip_Status.equals("arrived")) {
                googleMap.addMarker(new MarkerOptions()
                        .position(endWayPoint)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.green)));


            } else {
                googleMap.addMarker(new MarkerOptions()
                        .position(pickup_latlong)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.green)));
                googleMap.addMarker(new MarkerOptions()
                        .position(endWayPoint)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.red)));
            }

            currentDriverMarker = googleMap.addMarker(new MarkerOptions()
                    .position(cur_latlong)
                    .icon(BitmapDescriptorFactory.fromBitmap(carIcon)));

            if (wayPointBuilder != null) {
                LatLngBounds bounds = wayPointBuilder.build();
                int padding, paddingH, paddingW;
                final View mapview = mapFragment.getView();
                float maxX = mapview.getMeasuredWidth();
                float maxY = mapview.getMeasuredHeight();

                DisplayMetrics displayMetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                int height = displayMetrics.heightPixels;
                int width = displayMetrics.widthPixels;
                float percentageH = 50.0f;
                float percentageW = 80.0f;
                paddingH = (int) (maxY * (percentageH / 100.0f));
                paddingW = (int) (maxX * (percentageW / 100.0f));
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, paddingW, paddingH, 100);
                googleMap.animateCamera(cu);
            }
        } catch (NullPointerException ee) {
        } catch (Exception e) {
        }

    }

    @Override
    public void onRoutingCancelled() {

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_LOCATION:
                switch (resultCode) {
                    case Activity.RESULT_OK: {
                        Toast.makeText(TripPageNew.this, "Location enabled!", Toast.LENGTH_LONG).show();

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                gps = new GPSTracker(TripPageNew.this);
                                if (gps != null && gps.canGetLocation()) {
                                    double Dlatitude = gps.getLatitude();
                                    double Dlongitude = gps.getLongitude();
                                    MyCurrent_lat = Dlatitude;
                                    MyCurrent_long = Dlongitude;

                                    System.out.println("currntlat----------" + MyCurrent_lat);
                                    System.out.println("currntlon----------" + MyCurrent_long);

                                    if (!String.valueOf(MyCurrent_lat).equals(0.0) && !String.valueOf(MyCurrent_long).equals(0.0)) {
                                        if (googleMap != null) {
                                            LatLng latLng = new LatLng(MyCurrent_lat, MyCurrent_long);

                                            cur_latlong = new LatLng(MyCurrent_lat, MyCurrent_long);

//                                            currentDriverMarker = googleMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.carmove)));
                                            currentDriverMarker = googleMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromBitmap(carIcon)));
                                            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(13).build();
                                            CameraUpdate camUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
                                            googleMap.moveCamera(camUpdate);

                                            if (Trip_Status.equals("arrived")) {
                                                arrived(sPickUpAddress, eta_arrival, Str_RideId, location, member_since, user_gender, Str_username, Str_user_img);
                                            } else if (Trip_Status.equals("begin")) {
                                                Begin(str_drop_location, est_cost + sCurrencySymbol, Str_RideId, title_text, subtitle_text);
                                            } else {
                                                End(str_drop_location, sCurrencySymbol + " " + est_cost, Str_RideId, title_text, subtitle_text, "");
                                            }

                                        }
                                    }
                                }
                            }
                        }, 2000);

                        break;
                    }
                    case Activity.RESULT_CANCELED: {
                        finish();
                        break;
                    }
                    default: {
                        break;
                    }
                }
                break;


            case OVERLAY_PERMISSION_REQ_CODE:

                if (resultCode == Activity.RESULT_OK) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (!Settings.canDrawOverlays(TripPageNew.this)) {
                            Toast.makeText(TripPageNew.this, "SYSTEM_ALERT_WINDOW permission not granted...", Toast.LENGTH_SHORT).show();
                        } else {
                            moveNavigation();
                        }
                    }
                }

                break;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_NAVIGATION_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    checkNavigation();

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + Str_user_phoneno));
                    startActivity(callIntent);
                }
                break;
            case PERMISSION_REQUEST_CODE_SOS:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + sContactNumber));
                    startActivity(callIntent);
                }
                break;
        }
    }

    private void requestPermission() {
    }

    private void requestPermissionSOS() {
    }

    private boolean checkCallPhonePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkReadStatePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(finishReceiver);

        if (travelTimeHandler != null) {
            travelTimeHandler.removeCallbacks(travelTimeRunnable);
        }

        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            //Do nothing
            return true;
        }
        return false;
    }


    private void updateEta(Location currentLocation, Location destLocation) {

        String url = Iconstant.GetDistanceAndTime_url + "&origins=" + currentLocation.getLatitude() + "," + currentLocation.getLongitude()
                + "&destinations=" + destLocation.getLatitude() + "," + destLocation.getLongitude();

        getDistanceAndTimeRequest(url);

    }

    private void getDistanceAndTimeRequest(String url) {

        mRequest = new ServiceRequest(TripPageNew.this);
        mRequest.makeServiceRequest(url, Request.Method.GET, null, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                getDistanceAndTime(response);

                if (Tv_time != null) {
                    if (Str_TimeTo.length() > 0) {
                        Tv_time.setText(Str_TimeTo);
                    }
                }

                if (Tv_time != null) {
                    if (Str_DistanceTo.length() > 0) {
                        Tv_distance.setText(Str_DistanceTo);
                    }
                }

            }

            @Override
            public void onErrorListener() {

            }
        });


    }


    private void getDistanceAndTime(String result) {

        try {

            JSONObject jobjResult = new JSONObject(result);
            Object objRows = jobjResult.get("rows");

            if (objRows instanceof JSONArray) {
                JSONArray jarRows = jobjResult.getJSONArray("rows");
                if (jarRows.length() > 0) {
                    Object objSubResult = jarRows.get(0);

                    if (objSubResult instanceof JSONObject) {
                        JSONObject jobjSubResult = jarRows.getJSONObject(0);
                        if (jobjSubResult.length() > 0) {
                            if (jobjSubResult.has("elements")) {
                                Object objElements = jobjSubResult.get("elements");
                                if (objElements instanceof JSONArray) {
                                    JSONArray jarElements = jobjSubResult.getJSONArray("elements");
                                    if (jarElements.length() > 0) {
                                        Object objFinalResult = jarElements.get(0);
                                        if (objFinalResult instanceof JSONObject) {
                                            JSONObject jobjFinalResult = jarElements.getJSONObject(0);
                                            if (jobjFinalResult.length() > 0) {

                                                if (jobjFinalResult.has("distance")) {
                                                    Object objDistance = jobjFinalResult.get("distance");
                                                    if (objDistance instanceof JSONObject) {
                                                        JSONObject jobjDistance = jobjFinalResult.getJSONObject("distance");
                                                        if (jobjDistance.length() > 0) {
                                                            if (jobjDistance.has("text")) {
                                                                Str_DistanceTo = jobjDistance.getString("text");
                                                            }
                                                            if (jobjDistance.has("value")) {

                                                            }

                                                        }

                                                    }

                                                }

                                                if (jobjFinalResult.has("duration")) {
                                                    Object objDuration = jobjFinalResult.get("duration");
                                                    if (objDuration instanceof JSONObject) {
                                                        JSONObject jobjDuration = jobjFinalResult.getJSONObject("duration");
                                                        if (jobjDuration.length() > 0) {
                                                            if (jobjDuration.has("text")) {
                                                                Str_TimeTo = jobjDuration.getString("text");
                                                            }
                                                            if (jobjDuration.has("value")) {

                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void SOSRequest(String url) {
        dialog = new Dialog(TripPageNew.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_processing));

        HashMap<String, String> jsonParams = new HashMap<>();
        jsonParams.put("driver_id", driver_id);
        jsonParams.put("latitude", String.valueOf(gps.getLatitude()));
        jsonParams.put("longitude", String.valueOf(gps.getLongitude()));


        System.out.println("-------------SOSRequest Url----------------" + url);
        System.out.println("-------------SOSRequest jsonParams----------------" + jsonParams);

        mRequest = new ServiceRequest(TripPageNew.this);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------SOSRequest Response----------------" + response);

                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }

                String Sstatus = "", sResponse = "";
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    sResponse = object.getString("response");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        alert(getResources().getString(R.string.action_success), sResponse);
                    } else {
                        showSOSNoRecordAlert(sResponse);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });

    }

    private void showSOSAlert() {

        final Dialog sosDialog = new Dialog(TripPageNew.this, R.style.SlideUpDialog);
        sosDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        sosDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        sosDialog.setCancelable(true);
        sosDialog.setContentView(R.layout.sos_popup1);

        final TextView Tv_confirm = (TextView) sosDialog.findViewById(R.id.txt_confirm);
        final ImageView Iv_close = (ImageView) sosDialog.findViewById(R.id.img_close);

        Tv_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (sosDialog != null && sosDialog.isShowing()) {
                    sosDialog.dismiss();
                }

                if (gps != null && gps.canGetLocation()) {
                    if (cd.isConnectingToInternet()) {
                        SOSRequest(Iconstant.sos_url);
                    } else {
                        alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                    }
                } else {
                    alert(getResources().getString(R.string.action_error), getResources().getString(R.string.label_could_not_fetch_location));
                }

            }
        });

        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sosDialog != null && sosDialog.isShowing()) {
                    sosDialog.dismiss();
                }
            }
        });

        sosDialog.show();

    }

    private void showSOSNoRecordAlert(String response) {

        final Dialog sosNoRecordDialog = new Dialog(TripPageNew.this, R.style.SlideUpDialog);
        sosNoRecordDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        sosNoRecordDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        sosNoRecordDialog.setCancelable(true);
        sosNoRecordDialog.setContentView(R.layout.sos_popup2);

        final TextView Tv_mobileNumber = (TextView) sosNoRecordDialog.findViewById(R.id.tv_callassist);
        final ImageView Iv_close = (ImageView) sosNoRecordDialog.findViewById(R.id.img_close);
        final ImageView Iv_call = (ImageView) sosNoRecordDialog.findViewById(R.id.call_assistance);
        final TextView emergn_tv = (TextView) sosNoRecordDialog.findViewById(R.id.lemergency_textview);
        emergn_tv.setText(response.toString());

        Iv_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (sosNoRecordDialog != null && sosNoRecordDialog.isShowing()) {
                    sosNoRecordDialog.dismiss();
                }

                callCustomerCare();

            }
        });

        Tv_mobileNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (sosNoRecordDialog != null && sosNoRecordDialog.isShowing()) {
                    sosNoRecordDialog.dismiss();
                }

                callCustomerCare();

            }
        });

        Tv_mobileNumber.setText(sContactNumber);


        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sosNoRecordDialog != null && sosNoRecordDialog.isShowing()) {
                    sosNoRecordDialog.dismiss();
                }
            }
        });

        sosNoRecordDialog.show();

    }


    private void callCustomerCare() {

        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + sContactNumber));
            startActivity(intent);

        } else {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + sContactNumber));
            startActivity(intent);
        }

    }

    private void addPulsatingEffectRed(final LatLng userLatlng, final float accuracy) {


        if (lastPulseAnimator != null) {
            lastPulseAnimator.cancel();
            Log.d("onLocationUpdated: ", "cancelled");
        }
//        System.out.println("accuracy", "" + accuracy);
        if (lastUserCircle != null)
            lastUserCircle.setCenter(userLatlng);
        lastPulseAnimator = valueAnimate(accuracy, 1000, new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Log.d("Animation Value : ", "" + (Float) animation.getAnimatedValue());
                Log.d("Animation Zoom : ", "" + googleMap.getCameraPosition().zoom);
                if (lastUserCircle != null)
                    lastUserCircle.setRadius((Float) animation.getAnimatedValue());
                else {
                    lastUserCircle = googleMap.addCircle(new CircleOptions()
                            .center(userLatlng)
                            .radius((Float) animation.getAnimatedValue())
                            .strokeColor(Color.parseColor("#EECED4"))
                            .fillColor(Color.parseColor("#ED203C")));
                }
            }
        });






        /*Bitmap markerIcon = drawableToBitmap(ResourcesCompat.getDrawable(getResources(), R.drawable.red, null));
        pulseMarker(markerIcon, 750);*/

    }

    private void addPulsatingEffectGreen(final LatLng userLatlng, final float accuracy) {


        if (lastPulseAnimator1 != null) {
            lastPulseAnimator1.cancel();
            Log.d("onLocationUpdated: ", "cancelled");
        }
//        System.out.println("accuracy", "" + accuracy);
        if (lastUserCircle1 != null)
            lastUserCircle1.setCenter(userLatlng);
        lastPulseAnimator1 = valueAnimate(accuracy, 1000, new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Log.d("Animation Value : ", "" + (Float) animation.getAnimatedValue());
                Log.d("Animation Zoom : ", "" + googleMap.getCameraPosition().zoom);
                if (lastUserCircle1 != null)
                    lastUserCircle1.setRadius((Float) animation.getAnimatedValue());
                else {
                    lastUserCircle1 = googleMap.addCircle(new CircleOptions()
                            .center(userLatlng)
                            .radius((Float) animation.getAnimatedValue())

                            .strokeColor(Color.parseColor("#C3E2D7"))
                            .fillColor(Color.parseColor("#43B749")))
                    ;
                    lastUserCircle1.setStrokeWidth(15);
                }
            }
        });






        /*Bitmap markerIcon = drawableToBitmap(ResourcesCompat.getDrawable(getResources(), R.drawable.red, null));
        pulseMarker(markerIcon, 750);*/

    }

    protected ValueAnimator valueAnimate(float accuracy, long duration, ValueAnimator.AnimatorUpdateListener updateListener) {
        Log.d("valueAnimate: ", "called");
        ValueAnimator va = ValueAnimator.ofFloat(0, accuracy);
        va.setDuration(duration);
        va.addUpdateListener(updateListener);
        va.setRepeatCount(ValueAnimator.INFINITE);
        va.setRepeatMode(ValueAnimator.RESTART);

        va.start();
        return va;
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    void pulseMarker(final Bitmap markerIcon, final long onePulseDuration) {
        final Handler handler = new Handler();
        final long startTime = System.currentTimeMillis();
        final Interpolator interpolator = new CycleInterpolator(1f);
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (jumpingMarker != null /*&& jumpingMarker.isVisible()*/) {
                    long elapsed = System.currentTimeMillis() - startTime;
                    float t = interpolator.getInterpolation((float) elapsed / (float) onePulseDuration);
                    jumpingMarker.setIcon(BitmapDescriptorFactory.fromBitmap(scaleBitmap(markerIcon, 1.0f + 0.75f * t)));
                    handler.postDelayed(this, 100);
                }
            }
        });
    }

    public Bitmap scaleBitmap(Bitmap bitmap, float scaleFactor) {
        final int sizeX = Math.round(bitmap.getWidth() * scaleFactor);
        final int sizeY = Math.round(bitmap.getHeight() * scaleFactor);
        Bitmap bitmapResized = Bitmap.createScaledBitmap(bitmap, sizeX, sizeY, false);
        return bitmapResized;
    }


    private Bitmap base64ToBitmap(String image) {

        Bitmap decodedByte = null;

        try {

            byte[] decodedString = Base64.decode(image, Base64.DEFAULT);
            decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return decodedByte;

    }


}
