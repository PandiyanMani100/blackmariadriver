package com.blackmaria.partner.app;

import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.PowerManager;
import android.provider.Settings;

import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.Utils.GPSTracker_Driver;
import com.blackmaria.partner.adapter.RequestTimerAdapter;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.GPSTracker;
import com.blackmaria.partner.Utils.SessionManager;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by Prem Kumar and Anitha on 12/17/2016.
 */

public class TimerPage extends ActivityHockeyApp implements com.google.android.gms.location.LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private SessionManager sessionManager;
    private static ArrayList<JSONObject> details = new ArrayList<>();

    private CountDownTimer timer;
    public String EXTRA = "EXTRA";
    private int seconds = 0;
    public static MediaPlayer mediaPlayer;
    public JSONObject dataObject;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    public static Location myLocation;

    private LinearLayout listView;
    private RequestTimerAdapter continuousRequestAdapter;
    private int count = 0;
    private GPSTracker_Driver gps;
    private PendingResult<LocationSettingsResult> result;
    private final static int REQUEST_LOCATION = 199;
    public static String notifiacation = "";
    private PowerManager powerManager;
    public static Activity context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN |
//                        WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
//                        WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
//                        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN |
//                        WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
//                        WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
//                        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        setContentView(R.layout.request_timer);
        context = TimerPage.this;
        listView = (LinearLayout) findViewById(R.id.linearList);
        initView();
    }


    void initView() {
        sessionManager = new SessionManager(this);

        //screen locak on
        PowerManager.WakeLock screenLock = ((PowerManager) getSystemService(POWER_SERVICE)).newWakeLock(
                PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "TAG");
        screenLock.acquire();

        KeyguardManager keyguardManager = (KeyguardManager) getSystemService(Activity.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock lock = keyguardManager.newKeyguardLock(KEYGUARD_SERVICE);
        //later
        screenLock.release();
        lock.disableKeyguard();

        int MAX_VOLUME = 100;
        final float volume = (float) (1 - (Math.log(MAX_VOLUME - 80) / Math.log(MAX_VOLUME)));
        mediaPlayer = MediaPlayer.create(this, Settings.System.DEFAULT_RINGTONE_URI);

        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 20, 0);

        try {
            mediaPlayer.setVolume(volume, volume);
        } catch (NullPointerException e) {

        } catch (Exception e) {

        }
        //alertAdapter = new DriverAlertAdapter(sessionManager, this, myLocation);
        continuousRequestAdapter = new RequestTimerAdapter(TimerPage.this, myLocation, listView);
        continuousRequestAdapter.setTimerCompleteCallBack(timerCompleteCallback);
        setLocationRequest();
        buildGoogleApiClient();
        gps = new GPSTracker_Driver(TimerPage.this);
        if (gps != null && gps.canGetLocation() && gps.isgpsenabled()) {

            System.out.println("======gps enabled===========");

            myLocation = gps.getLocation();
            addData(getIntent());
        } else {
            gps.showSettingsAlert();
            enableGpsService();
        }

    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (gps != null && gps.canGetLocation() && gps.isgpsenabled()) {

            System.out.println("======gps enabled===========");

            myLocation = gps.getLocation();
            addData(intent);
        } else {
            gps.showSettingsAlert();
            enableGpsService();
        }
    }


    private TimerCompleteCallback timerCompleteCallback = new TimerCompleteCallback() {
        @Override
        public void timerCompleteCallBack(RequestTimerAdapter.ViewHolder holder) {
            try {

                HashMap<String, Integer> user = sessionManager.getRequestCount();
                int req_count = user.get(SessionManager.KEY_COUNT);
                req_count = req_count - 1;
                sessionManager.setRequestCount(req_count);

                System.out.println("------- req_count inside decline--------------" + req_count);

                if (req_count == 0) {
                    sessionManager.setRequestCount(0);
                    finish();
                    if (TimerPage.mediaPlayer != null && TimerPage.mediaPlayer.isPlaying()) {
                        TimerPage.mediaPlayer.stop();
                    }

                }

                listView.removeViewAt(holder.count);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public interface TimerCompleteCallback {
        void timerCompleteCallBack(RequestTimerAdapter.ViewHolder holder);
    }

    private void addData(Intent intent) {
        if (mediaPlayer != null) {
            if (!mediaPlayer.isPlaying()) {
                mediaPlayer.start();
                mediaPlayer.setLooping(true);
            }
        }

        if (intent != null) {

            HashMap<String, Integer> user = sessionManager.getRequestCount();
            int req_count = user.get(SessionManager.KEY_COUNT);
            req_count = req_count + 1;
            sessionManager.setRequestCount(req_count);

            System.out.println("=-----------------start req_count---------------" + req_count);


            if (getIntent().hasExtra("notification")) {
                notifiacation = getIntent().getStringExtra("notification");
            }
            Bundle extra = intent.getExtras();
            if (extra != null) {
                String data = (String) extra.get(EXTRA);
                System.out.println("-----------------JSONO DATA------------" + data);
                try {
                    if (data != null) {
                        String decodeData = URLDecoder.decode(data, "UTF-8");
                        System.out.println("decode" + decodeData);
                        count = count + 1;
                        JSONObject dataObject = new JSONObject(decodeData);
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        listView.addView(continuousRequestAdapter.getView(count, dataObject), params);
                    } else {
                        System.out.println("data" + data);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }


    private void setLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected void startLocationUpdates() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {
        myLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onLocationChanged(Location location) {
        this.myLocation = location;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (myLocation == null) {
            myLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    @Override
    protected void onDestroy() {
        sessionManager.setRequestCount(0);
        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
            }
        }

        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi
                    .removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }


        super.onDestroy();
    }

    private void enableGpsService() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        //...
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(TimerPage.this, REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_LOCATION:
                switch (resultCode) {
                    case Activity.RESULT_OK: {
                        myLocation = gps.getLocation();
                        break;
                    }
                    case Activity.RESULT_CANCELED: {
                        enableGpsService();
                        break;
                    }
                    default: {
                        break;
                    }
                }
                break;
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            //Do nothing
            return true;
        }
        return false;
    }
}

