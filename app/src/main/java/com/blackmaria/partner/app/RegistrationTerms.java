package com.blackmaria.partner.app;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.latestpackageview.view.signup.RegistrationSelectLocation_constrain;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;

import java.util.HashMap;

/**
 * Created by GANESH on 26-09-2017.
 */

public class RegistrationTerms extends ActivityHockeyApp implements View.OnClickListener {

    private SessionManager sessionManager;
    private ConnectionDetector cd;

    private String sDriverID = "";

    private ImageView RL_exit;
    //    private TextView Tv_terms, Tv_privacyPolicy, Tv_eula;
//    private CheckBox Cb_terms, Cb_privacyPolicy, Cb_eula;
    private TextView Btn_registerNow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_terms_and_conditions_constrain);

        initialize();

    }

    private void initialize() {

        sessionManager = new SessionManager(RegistrationTerms.this);
        cd = new ConnectionDetector(RegistrationTerms.this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);

        RL_exit = findViewById(R.id.img_back);

        Btn_registerNow = findViewById(R.id.btn_register_now);

        RL_exit.setOnClickListener(this);

        Btn_registerNow.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        if (view == RL_exit) {

            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);

        } /*else if (view == Btn_readTerms) {

            String url = "https://blackmaria.co/";
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
            startActivity(intent);

        } */ else if (view == Btn_registerNow) {

            Intent intent = new Intent(RegistrationTerms.this, RegistrationSelectLocation_constrain.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.exit);


//            if (Cb_terms.isChecked() && Cb_privacyPolicy.isChecked() && Cb_eula.isChecked()) {
//                // All 3 check boxes are checked
//
//
//
//            } else {
//                // All 3 check boxes are not checked
//                Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.please_accept_terms_and_conditions));
//            }

        }/* else if (view == Tv_terms) {
            if (Cb_terms.isChecked()) {
                Cb_terms.setChecked(false);
            } else {
                Cb_terms.setChecked(true);
            }
        } else if (view == Tv_privacyPolicy) {
            if (Cb_privacyPolicy.isChecked()) {
                Cb_privacyPolicy.setChecked(false);
            } else {
                Cb_privacyPolicy.setChecked(true);
            }
        } else if (view == Tv_eula) {
            if (Cb_eula.isChecked()) {
                Cb_eula.setChecked(false);
            } else {
                Cb_eula.setChecked(true);
            }
        }*/

    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(RegistrationTerms.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


}
