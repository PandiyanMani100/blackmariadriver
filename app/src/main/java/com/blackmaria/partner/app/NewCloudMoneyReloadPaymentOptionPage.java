package com.blackmaria.partner.app;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.Pojo.WalletMoneyPojo;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.adapter.ReloadPaymentList;
import com.blackmaria.partner.adapter.XenditBankDetailsAdapter_new;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.WalletMoneyWebview;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user144 on 9/18/2017.
 */

public class NewCloudMoneyReloadPaymentOptionPage extends ActivityHockeyApp implements View.OnClickListener, ReloadPaymentList.RecyclerItemClick {

    private RelativeLayout Rl1;
    private ImageView imgBack;
    private ImageView imgBwallet;
    private TextView txtLabelCurrentBalance;
    private RelativeLayout Rl2;
    private static EditText insertAmount1;
    private LinearLayout buyTv;
    private ImageView indicatingImage;
    private RelativeLayout verifylayout;
    private View dummyview;
    private TextView reloadAmount;
    private GridView walletPagePaymentListGridview;
    private String payPalID = "", withdrawedSuccessMoney = "";
    private EditText Et_enteramount;
    private String reloadAmount1 = "", currency = "", reloadAmount2 = "";
    private RefreshReceiver refreshReceiver;
    private HashMap<String, ArrayList<String>> paymentListMap;
    ReloadPaymentList adapter;
    private int Listposition = 0;

    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private static Context context;
    private SessionManager session;
    private String sDriverID = "";
    private ServiceRequest mRequest;
    private String Str_currentbalance = "", Sauto_charge_status = "", paymentName = "";
    private Dialog dialog;
    ArrayList<WalletMoneyPojo> paymentcardlist;
    private Boolean isDatavailable = false;
    private Boolean isPaymentListAvailable = false;
    XenditBankDetailsAdapter_new Eadaptter;
    private Double reloadAmoun1t = 0.0;
    private TextView redeemTv;
    private String strRadeenCode = "";
    Double amount = 0.0;
    public static String CURRENCYCONVERSIONKEY_KEY = "";

    //------------------------------Broadcost reciver---------------------------------------------
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();
            if (action.equalsIgnoreCase("com.package.ACTION_WALLET_RELOAD_SUCCESS")) {
                finish();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_cloud_money_reload_paymentselect);

        dialog = new Dialog(NewCloudMoneyReloadPaymentOptionPage.this);

        initialize();

    }

    private void initialize() {
        session = new SessionManager(NewCloudMoneyReloadPaymentOptionPage.this);
        cd = new ConnectionDetector(NewCloudMoneyReloadPaymentOptionPage.this);
        isInternetPresent = cd.isConnectingToInternet();
        paymentcardlist = new ArrayList<WalletMoneyPojo>();

        HashMap<String, String> user = session.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);

        Rl1 = (RelativeLayout) findViewById(R.id.Rl_1);
        imgBack = (ImageView) findViewById(R.id.img_back);
        imgBwallet = (ImageView) findViewById(R.id.img_bwallet);
        txtLabelCurrentBalance = (TextView) findViewById(R.id.txt_label_current_balance);
        Rl2 = (RelativeLayout) findViewById(R.id.Rl_2);
        insertAmount1 = (EditText) findViewById(R.id.edt_mobile_no);
        buyTv = (LinearLayout) findViewById(R.id.buy_tv);

        indicatingImage = (ImageView) findViewById(R.id.indicating_image);
 /*GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(indicatingImage);
        Glide.with(this).load(R.drawable.blinkgif).into(imageViewTarget);*/
        verifylayout = (RelativeLayout) findViewById(R.id.verifylayout);
        dummyview = (View) findViewById(R.id.dummyview);
        reloadAmount = (TextView) findViewById(R.id.reload_amount);
        walletPagePaymentListGridview = (GridView) findViewById(R.id.wallet_page_payment_list_gridview);
        redeemTv = (TextView) findViewById(R.id.redeem_tv);
        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_WALLET_RELOAD_SUCCESS");
        registerReceiver(refreshReceiver, intentFilter);

        imgBack.setOnClickListener(this);
        verifylayout.setOnClickListener(this);
        buyTv.setOnClickListener(this);


        new DownloadFilesTask().execute();


    }


    private class DownloadFilesTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new Dialog(NewCloudMoneyReloadPaymentOptionPage.this);
            dialog.getWindow();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.custom_loading);
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }


        protected String doInBackground(String... urls) {

            return "";
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

           /* if (dialog != null) {
                dialog.dismiss();
            }*/

            Intent in = getIntent();
            if (in.hasExtra("insertAmount")) {
                reloadAmount1 = in.getStringExtra("insertAmount");
                reloadAmount2 = reloadAmount1;
            }
            if (in.hasExtra("currency")) {
                currency = in.getStringExtra("currency");
                reloadAmount.setText(currency + " " + reloadAmount1);
            }
            if (in.hasExtra("autochargestatus")) {
                Sauto_charge_status = in.getStringExtra("autochargestatus");
            }
            if (in.hasExtra("currentBalance")) {
                Str_currentbalance = in.getStringExtra("currentBalance");
            }
            if (in.hasExtra("paymentListMap")) {
                paymentListMap = (HashMap<String, ArrayList<String>>) in.getSerializableExtra("paymentListMap");
                runOnUiThread(new Runnable() {
                    public void run() {
                        adapter = new ReloadPaymentList(NewCloudMoneyReloadPaymentOptionPage.this, paymentListMap, NewCloudMoneyReloadPaymentOptionPage.this);
                        walletPagePaymentListGridview.setAdapter(adapter);
                    }
                });

            }

            if (isInternetPresent) {

                PostRquestXenditBankList(Iconstant.getPaymentOPtion_url_new);
            } else {

                Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
            }
        }


    }


    @Override
    public void onItemClick(int position) {

        Listposition = position;

        adapter.notifyDataSetChanged();
        paymentName = paymentListMap.get("bankName").get(position);
        if (paymentListMap.get("bankCode").get(position).equalsIgnoreCase("Paypal")) {
            AlertPaypal("", "RELOAD" + " " + reloadAmount.getText().toString().trim() + " VIA PAYPAL?");
        } else if (paymentListMap.get("bankCode").get(position).equalsIgnoreCase("xendit-bankpayment")) {
            changeBankDetails();
        } else if (paymentListMap.get("bankCode").get(position).equalsIgnoreCase("banktransfer")) {
            Alert("", getResources().getString(R.string.new_wallet_lable_cash_tv1));
        } else if (paymentListMap.get("bankCode").get(position).equalsIgnoreCase("cash")) {
            // do nothing
        } else if (paymentListMap.get("bankCode").get(position).equalsIgnoreCase("Stripe")) {
            if (isInternetPresent) {

                if ("Stripe".equalsIgnoreCase(paymentListMap.get("bankCode").get(position))) {
                    if (Sauto_charge_status.equalsIgnoreCase("1")) {
                        postRequest_AddMoney(Iconstant.wallet_add_money_url);
                    } else {
                        Intent intent = new Intent(NewCloudMoneyReloadPaymentOptionPage.this, WalletMoneyWebview.class);
                        intent.putExtra("walletMoney_recharge_amount", reloadAmount1);
                        intent.putExtra("walletMoney_currency_symbol", currency);
                        intent.putExtra("walletMoney_currentBalance", Str_currentbalance);
                        intent.putExtra("walletMoney_paymentType", paymentListMap.get("bankName").get(position));
                        startActivity(intent);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                    }
                }
            } else {
                Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
            }
        } else if (paymentListMap.get("bankCode").get(position).equalsIgnoreCase("xendit-card")) {

            if (session.getXenditChargeStatus().equalsIgnoreCase("1")) {
                if (session.getXenditChargeStatus().equalsIgnoreCase("1")) {
                    CURRENCYCONVERSIONKEY_KEY = session.getcurrencyconversionKey();
                    amount = Double.parseDouble(reloadAmount1) * Double.parseDouble(CURRENCYCONVERSIONKEY_KEY);
                    amount = (Double) Math.ceil(amount);

                    Intent intent = new Intent(NewCloudMoneyReloadPaymentOptionPage.this, NewCloudMoneyTransferUrlProgress.class);
                    intent.putExtra("total_amount", reloadAmount2);
                    intent.putExtra("pay_amount", amount + "");
                    intent.putExtra("authentication_id", "");
                    intent.putExtra("token_id", "");
                    intent.putExtra("flag", "2");
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    finish();
                }
            } else {
                Intent intent1 = new Intent(NewCloudMoneyReloadPaymentOptionPage.this, NewCloudMoneyReloadCardDetailPage.class);
                intent1.putExtra("insertAmount", reloadAmount2);
                intent1.putExtra("insertAmountPay", reloadAmount1);
                startActivity(intent1);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        }

    }

    @Override
    public void onClick(View v) {
        if (v == imgBack) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (v == buyTv) {

            if (redeemTv.getText().toString().trim().equalsIgnoreCase(getResources().getString(R.string.redeem_txtview))) {
                if (isInternetPresent) {
                    if (insertAmount1.getText().toString().toString().length() < 0) {
                        erroredit(insertAmount1, getResources().getString(R.string.redeemcode_text));
                    } else {
                        redeemCodeApply(Iconstant.redeem_code_url);
                    }
                } else {
                    Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
                }
            } else {
                redeemTv.setText(getResources().getString(R.string.remove_txtview));
                reloadAmount.setText(reloadAmount2);
            }


           /* if (isInternetPresent) {
                if (insertAmount1.getText().toString().toString().length() <= 0) {
                    erroredit(insertAmount1, getResources().getString(R.string.redeemcode_text));
                } else {
                    redeemCodeApply(Iconstant.redeem_code_url);
                }
            } else {
                Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
            }*/
        } else if (v == verifylayout) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
            System.out.println();
        }
    }

    //--------------------Code to set error for EditText-----------------------
    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(NewCloudMoneyReloadPaymentOptionPage.this, R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }

    private void PayPalMethod() {
        final Dialog dialog = new Dialog(NewCloudMoneyReloadPaymentOptionPage.this, R.style.SlideUpDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.new_cloud_transfer_withdraw_reload_paypal_popup);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        Et_enteramount = (EditText) dialog.findViewById(R.id.email_et1);
        ImageView closeImage = (ImageView) dialog.findViewById(R.id.img_close);
        TextView paypal_conform = (TextView) dialog.findViewById(R.id.paypal_conform);


        // Et_enteramount.setHint(getResources().getString(R.string.walletmoney_lable_rechargemoney_edittext_hint) + " " + ScurrencySymbol + Str_minimum_amt + " " + "-" + " " + ScurrencySymbol + Str_maximum_amt);
        closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CloseKeyboard(Et_enteramount);
                dialog.dismiss();
            }
        });
        paypal_conform.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CloseKeyboard(Et_enteramount);
                String payPalIdValue = Et_enteramount.getText().toString();
                payPalID = payPalIdValue;
                if (!payPalID.equalsIgnoreCase("")) {
                    if (isValidEmail(Et_enteramount.getText().toString())) {
                        {
                            Intent intent = new Intent(NewCloudMoneyReloadPaymentOptionPage.this, NewCloudMoneyTransferUrlProgress.class);
                            intent.putExtra("flag", "1");
                            startActivity(intent);
                            overridePendingTransition(R.anim.enter, R.anim.exit);
                            finish();
                        }
                    } else {
                        Alert("", getResources().getString(R.string.valid_paypal_alert));
                    }
                } else {
                    Alert("", getResources().getString(R.string.paypal_alert));
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    //-------------------------code to Check Email Validation-----------------------
    public final static boolean isValidEmail(CharSequence target) {
        if (!TextUtils.isEmpty(target) && (android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches() || android.util.Patterns.PHONE.matcher(target).matches())) {
            return true;
        } else {

            return false;
        }
        //return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    private void CloseKeyboard(EditText edittext) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(edittext.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    private void AlertPaypal(String title, String alert) {
        final PkDialog mDialog = new PkDialog(NewCloudMoneyReloadPaymentOptionPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewCloudMoneyReloadPaymentOptionPage.this, WalletMoneyWebview.class);
                intent.putExtra("walletMoney_recharge_amount", reloadAmount1);
                intent.putExtra("walletMoney_currency_symbol", currency);
                intent.putExtra("walletMoney_currentBalance", Str_currentbalance);
                intent.putExtra("walletMoney_paymentType", paymentName);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);

                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void AlertSuccess(String title, String alert) {
        final PkDialog mDialog = new PkDialog(NewCloudMoneyReloadPaymentOptionPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewCloudMoneyReloadPaymentOptionPage.this, WalletMoneyPage1.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(NewCloudMoneyReloadPaymentOptionPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void changeBankDetails() {
        final Dialog dialog = new Dialog(NewCloudMoneyReloadPaymentOptionPage.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.new_cloud_money_reload_banklist);

        ImageView close_imag = (ImageView) dialog.findViewById(R.id.img_back);
        ImageView img_back1 = (ImageView) dialog.findViewById(R.id.img_back1);

        GridView grid_view = (GridView) dialog.findViewById(R.id.bank_gird);
        close_imag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        img_back1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        grid_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Eadaptter.notifyDataSetChanged();
                String bankCode = paymentcardlist.get(position).getXenditBankCode();
                Intent intent = new Intent(NewCloudMoneyReloadPaymentOptionPage.this, NewCloudMoneyTransferUrlProgress.class);
                intent.putExtra("reloadamount", reloadAmount1);
                intent.putExtra("bankcode", bankCode);
                intent.putExtra("flag", "3");
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
                dialog.dismiss();
            }
        });

        if (isPaymentListAvailable) {
            Eadaptter = new XenditBankDetailsAdapter_new(NewCloudMoneyReloadPaymentOptionPage.this, paymentcardlist);
            grid_view.setAdapter(Eadaptter);

        }
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    public static void redeemRefresh() {
        insertAmount1.setText("");
    }

    private void redeemCodeApply(String Url) {
        final Dialog dialog = new Dialog(NewCloudMoneyReloadPaymentOptionPage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        System.out.println("-----------redeemCodeApply url--------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        jsonParams.put("code", insertAmount1.getText().toString().toString());
        System.out.println("------------redeemCodeApply jsonParams--------------" + jsonParams);
        mRequest = new ServiceRequest(NewCloudMoneyReloadPaymentOptionPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------redeemCodeApply  jsonParams--------------" + response);
                String Ssatus = "", code = "", discount_amount = "", discount_type = "", coupen_id = "", currency_code = "", message = "";

                try {
                    JSONObject jsondata = new JSONObject(response);
                    Ssatus = jsondata.getString("status");
                    if (Ssatus.equalsIgnoreCase("1")) {
                        JSONObject objResponse = jsondata.getJSONObject("response");
                        if (objResponse.length() > 0) {
                            code = objResponse.getString("code");
                            discount_amount = objResponse.getString("discount_amount");
                            discount_type = objResponse.getString("discount_type");
                            coupen_id = objResponse.getString("coupen_id");
                            currency_code = objResponse.getString("currency_code");
                            message = objResponse.getString("message");
                        }
                    }
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                    if (Ssatus.equalsIgnoreCase("1")) {
                        strRadeenCode = code;
                        session.REDEEM_CODE = code;
                        if (discount_type.equalsIgnoreCase("flat")) {
                            reloadAmoun1t = Double.parseDouble(reloadAmount1) - Double.parseDouble(discount_amount);
                            reloadAmount1 = reloadAmoun1t + "";
                        } else {
                            System.out.println("===============reloadAmoun1t per============" + ((Double.parseDouble(discount_amount) / 100)) * Double.parseDouble(reloadAmount1));
                            reloadAmoun1t = Double.parseDouble(reloadAmount1) - ((Double.parseDouble(discount_amount) / 100)) * Double.parseDouble(reloadAmount1);
                            reloadAmount1 = reloadAmoun1t + "";
                        }
                        if (reloadAmoun1t <= 0.0) {
                            if (isInternetPresent) {
                                redeemCodeApply1(Iconstant.redeem_code_url1);
                            } else {
                                Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
                            }
                        } else {
                            reloadAmount.setText(reloadAmoun1t + "");
                            Alert("", getResources().getString(R.string.couponcode_text1) + " " + currency_code + " " + discount_amount + " " + getResources().getString(R.string.couponcode_text2));
                            redeemTv.setText(getResources().getString(R.string.remove_txtview));
                        }
                    } else {
                        String Sresponse = jsondata.getString("response");
                        Alert(getResources().getString(R.string.alert_label_title), Sresponse);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void redeemCodeApply1(String Url) {
        final Dialog dialog = new Dialog(NewCloudMoneyReloadPaymentOptionPage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        System.out.println("-----------free redeemCodeApply url--------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        jsonParams.put("total_amount", reloadAmount2);
        jsonParams.put("redeem_code", strRadeenCode);
        System.out.println("------------free redeemCodeApply jsonParams--------------" + jsonParams);
        mRequest = new ServiceRequest(NewCloudMoneyReloadPaymentOptionPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------free redeemCodeApply  jsonParams--------------" + response);
                String Ssatus = "", walletamount = "", message = "";

                try {
                    JSONObject jsondata = new JSONObject(response);
                    Ssatus = jsondata.getString("status");

                    if (Ssatus.equalsIgnoreCase("1")) {
                        message = jsondata.getString("msg");
                        walletamount = jsondata.getString("wallet_amount");
                    }

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                    if (Ssatus.equalsIgnoreCase("1")) {
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                        AlertSuccess("", message);
                    } else {
                        String Sresponse = jsondata.getString("message");
                        Alert(getResources().getString(R.string.alert_label_title), Sresponse);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void PostRquestXenditBankList(String Url) {

        System.out.println("-----------Xendit bank url--------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        System.out.println("------------Xendit bank jsonParams--------------" + jsonParams);
        mRequest = new ServiceRequest(NewCloudMoneyReloadPaymentOptionPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("------------Xendit bank jsonParams--------------" + response);
                String Ssatus = "";
                try {
                    JSONObject jsondata = new JSONObject(response);
                    Ssatus = jsondata.getString("status");
                    if (Ssatus.equalsIgnoreCase("1")) {
                        // JSONObject jsondata = object.getJSONObject("response");
                        Object check_object = jsondata.get("bank_list");
                        if (check_object instanceof JSONArray) {
                            JSONArray payment_list_jsonArray = jsondata.getJSONArray("bank_list");
                            if (payment_list_jsonArray.length() > 0) {
                                paymentcardlist.clear();
                                for (int i = 0; i < payment_list_jsonArray.length(); i++) {
                                    JSONObject payment_obj = payment_list_jsonArray.getJSONObject(i);
                                    WalletMoneyPojo wmpojo = new WalletMoneyPojo();
                                    wmpojo.setXenditBankName(payment_obj.getString("name"));
                                    wmpojo.setXenditBankCode(payment_obj.getString("code"));
                                    //   wmpojo.setPayment_normal_img(payment_obj.getString("inactive_icon"));
                                    wmpojo.setXendiActiveImage(payment_obj.getString("image"));
                                    //  wmpojo.setPayment_selected_payment_id("false");
                                    paymentcardlist.add(wmpojo);
                                }
                                isPaymentListAvailable = true;
                            } else {
                                isPaymentListAvailable = false;
                            }
                        }
                        isDatavailable = true;
                    }
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                    if (Ssatus.equalsIgnoreCase("1") && isDatavailable) {

                        // Tv_currency_code.setText(Scurrency);
                        //  Tv_last_trip.setText(Slast_withdrawwl);
                    } else {

                       /* String Sresponse = object.getString("response");
                        Alert(getResources().getString(R.string.alert_label_title), Sresponse);
*/
                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    //-----------------------Cabily Money Add Post Request-----------------
    private void postRequest_AddMoney(String Url) {
        final Dialog dialog = new Dialog(NewCloudMoneyReloadPaymentOptionPage.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_processing));

        System.out.println("-------------Wallet ADD Money Url----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        jsonParams.put("total_amount", reloadAmount1);

        System.out.println("-------------Wallet ADD Money jsonParams----------------" + jsonParams);
        mRequest = new ServiceRequest(NewCloudMoneyReloadPaymentOptionPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------Wallet ADD Money Response----------------" + response);

                String Sstatus = "", Smessage = "", Swallet_money = "";

                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    Smessage = object.getString("msg");
                    Swallet_money = object.getString("wallet_amount");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        if (dialog != null) {
                            dialog.dismiss();
                        }
                        session.createWalletAmount(currency + " " + Swallet_money);

                        Alert(getResources().getString(R.string.action_success), getResources().getString(R.string.action_loading_walletmoney_transaction_wallet_success));
                        Et_enteramount.setText("");


                    } else {
                        Alert(getResources().getString(R.string.action_error), Smessage);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void startBlinkingAnimation() {
        Animation startAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blinking_animation);
        indicatingImage.startAnimation(startAnimation);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
        if (indicatingImage != null) {
            indicatingImage.clearAnimation();
        }
    }
}
