package com.blackmaria.partner.app;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.Pojo.TodayDetails;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.adapter.EarningsDayAdapterNew;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Dashboard_constrain;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Ganesh on 13-07-2017.
 */

public class EarningsStatementDay extends ActivityHockeyApp implements View.OnClickListener {

    private TextView Tv_date;
    private ImageView Iv_back, Iv_home, Iv_prev, Iv_next;
    private ImageView Iv_pagePrev, Iv_pageNext;
    private ExpandableHeightListView Lv_statement;
    private TextView Tv_empty_view;

    private SessionManager sessionManager;
    private ConnectionDetector cd;
    private Dialog dialog;
    private ServiceRequest mRequest;
    private EarningsDayAdapterNew adapter;
    private Calendar calendar = null;
    private SimpleDateFormat mdFormat, mDisplayFormat;

    private String sCurrentDate = "", sFilterDate = "";
    private String currentPage = "", nextPage = "", totalRides = "", sDriveID = "", pageNo = "", contentPerPage = "", sDisplayDate = "";
    private ArrayList<TodayDetails> listTodayEarningDetails;
    private boolean isDataAvailable = false;
    private int currentPageNo = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.earnings_statement_day_new);

        initialize();

    }

    @SuppressLint("WrongConstant")
    private void initialize() {

        sessionManager = new SessionManager(EarningsStatementDay.this);
        cd = new ConnectionDetector(EarningsStatementDay.this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriveID = user.get(SessionManager.KEY_DRIVERID);
        listTodayEarningDetails = new ArrayList<TodayDetails>();

        getIntentData();

        // Steps to Get Current Date
        calendar = Calendar.getInstance();
        mdFormat = new SimpleDateFormat("yyyy-MM-dd");
        mDisplayFormat = new SimpleDateFormat("EEEE dd MMMM yyyy");
        sCurrentDate = mdFormat.format(calendar.getTime());

        try {
            Date filterDate = mdFormat.parse(sFilterDate);
            calendar.setTime(filterDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Tv_date = (TextView) findViewById(R.id.txt_date);
        Iv_back = (ImageView) findViewById(R.id.img_back);
        Iv_home = (ImageView) findViewById(R.id.img_home);
        Iv_prev = (ImageView) findViewById(R.id.img_prev);
        Iv_next = (ImageView) findViewById(R.id.img_next);
        Iv_pagePrev = (ImageView) findViewById(R.id.img_page_prev);
        Iv_pageNext = (ImageView) findViewById(R.id.img_page_next);
        Lv_statement = (ExpandableHeightListView) findViewById(R.id.lst_earnings_today);
        Tv_empty_view = (TextView) findViewById(R.id.txt_empty_view);

        Iv_back.setOnClickListener(this);
        Iv_home.setOnClickListener(this);
        Iv_prev.setOnClickListener(this);
        Iv_next.setOnClickListener(this);
        Iv_pagePrev.setOnClickListener(this);
        Iv_pageNext.setOnClickListener(this);

        if (cd.isConnectingToInternet()) {
            EarningsStatementDayNewRequest(Iconstant.earnings_daywise_url);
        } else {
            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }


        Lv_statement.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(EarningsStatementDay.this, TripSummary.class);
                intent.putExtra("rideID", listTodayEarningDetails.get(position).getRide_id());
                intent.putExtra("fromPage", "EarningsStatementDay");
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });

    }

    private void getIntentData() {

        Intent intent = getIntent();
        if (intent != null) {
            sFilterDate = intent.getStringExtra("filterDate");
        }

        // Setting page number to 1 by default
        if (pageNo.length() == 0) {
            pageNo = "1";
        }

        // Setting content per page
        contentPerPage = "5";

    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(EarningsStatementDay.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    @SuppressLint("WrongConstant")
    @Override
    public void onClick(View view) {

        if (view == Iv_back) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (view == Iv_home) {
            Intent intent = new Intent(EarningsStatementDay.this, Dashboard_constrain.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }else if (view == Iv_prev) {
            if (calendar != null) {
                if (cd.isConnectingToInternet()) {

                    // Getting previous date
                    Date date = null;
                    try {
                        date = mdFormat.parse(sFilterDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if (date != null) {
                        calendar.setTime(date);
                        calendar.add(Calendar.DATE, -1);
                        sFilterDate = mdFormat.format(calendar.getTime());
                    }

                    // Resetting page number to 1
                    pageNo = "1";

                    EarningsStatementDayNewRequest(Iconstant.earnings_daywise_url);

                } else {
                    Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                }

            }
        } else if (view == Iv_next) {
            if (calendar != null) {
                if (cd.isConnectingToInternet()) {

                    // Getting next date
                    Date date = null;
                    try {
                        date = mdFormat.parse(sFilterDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if (date != null) {
                        calendar.setTime(date);
                        calendar.add(Calendar.DATE, 1);
                        sFilterDate = mdFormat.format(calendar.getTime());
                    }

                    // Resetting page number to 1
                    pageNo = "1";

                    EarningsStatementDayNewRequest(Iconstant.earnings_daywise_url);

                } else {
                    Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                }

            }
        } else if (view == Iv_pagePrev) {

            if (cd.isConnectingToInternet()) {
                pageNo = String.valueOf(currentPageNo - 1);
                EarningsStatementDayNewRequest(Iconstant.earnings_daywise_url);
            } else {
                Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
            }

        } else if (view == Iv_pageNext) {

            if (cd.isConnectingToInternet()) {
                pageNo = String.valueOf(currentPageNo + 1);
                EarningsStatementDayNewRequest(Iconstant.earnings_daywise_url);
            } else {
                Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
            }

        }

    }

    @SuppressLint("WrongConstant")
    private void EarningsStatementDayNewRequest(String Url) {

        dialog = new Dialog(EarningsStatementDay.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriveID);
        jsonParams.put("page", pageNo);
        jsonParams.put("perPage", contentPerPage);
        jsonParams.put("filter_date", sFilterDate);

        System.out.println("--------------EarningsStatementDay Url-------------------" + Url);
        System.out.println("--------------EarningsStatementDay jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(EarningsStatementDay.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------EarningsStatementDay Response-------------------" + response);

                String status = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {
                            JSONObject jsonObject = object.getJSONObject("response");
                            if (jsonObject.length() > 0) {

                                nextPage = jsonObject.getString("next_page");
                                totalRides = jsonObject.getString("total_rides");
                                currentPage = jsonObject.getString("current_page");

                                Object ridesObj = jsonObject.get("rides");
                                if (ridesObj instanceof JSONArray) {

                                    JSONArray jarRides = jsonObject.getJSONArray("rides");

                                    if (jarRides.length() > 0) {

                                        listTodayEarningDetails = new ArrayList<>();
                                        for (int i = 0; i < jarRides.length(); i++) {

                                            JSONObject jobjRides = jarRides.getJSONObject(i);

                                            TodayDetails details = new TodayDetails();

                                            details.setRide_id(jobjRides.getString("ride_id"));
                                            details.setRide_time(jobjRides.getString("ride_time"));
                                            details.setRide_date(jobjRides.getString("ride_date"));
                                            details.setPickup(jobjRides.getString("pickup"));
                                            details.setRide_status(jobjRides.getString("ride_status"));
                                            details.setDistance(jobjRides.getString("distance"));
                                            details.setRide_type(jobjRides.getString("ride_type"));
                                            details.setRide_category(jobjRides.getString("ride_category"));
                                            details.setDisplay_status(jobjRides.getString("display_status"));
                                            details.setUser_image(jobjRides.getString("user_image"));
                                            details.setUser_name(jobjRides.getString("user_name"));
                                            details.setUser_review(jobjRides.getString("user_review"));
                                            details.setDriver_earning(jobjRides.getString("driver_earning"));
                                            details.setCurrency(jobjRides.getString("currency"));
                                            details.setGroup(jobjRides.getString("group"));
                                            details.setS_no(jobjRides.getString("s_no"));
                                            details.setDatetime(jobjRides.getString("datetime"));

                                            listTodayEarningDetails.add(details);

                                        }

                                        isDataAvailable = true;

                                    } else {
                                        isDataAvailable = false;
                                    }

                                } else {
                                    isDataAvailable = false;
                                }

                            } else {
                                isDataAvailable = false;
                            }
                        } else {
                            String sResponse = object.getString("response");
                            Alert(getResources().getString(R.string.action_error), sResponse);
                        }

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (status.equalsIgnoreCase("1")) {

                    if (isDataAvailable) {

                        Lv_statement.setVisibility(View.VISIBLE);
                        Tv_empty_view.setVisibility(View.GONE);
                        System.out.println("Adapter Set " + listTodayEarningDetails.size());
                        adapter = new EarningsDayAdapterNew(EarningsStatementDay.this, listTodayEarningDetails);
                        Lv_statement.setAdapter(adapter);

                    } else {
                        Iv_pagePrev.setVisibility(View.GONE);
                        Iv_pageNext.setVisibility(View.GONE);
                        Lv_statement.setVisibility(View.GONE);
                        Tv_empty_view.setVisibility(View.VISIBLE);

                    }

                    if (currentPage != null && currentPage.length() > 0) {
                        currentPageNo = 0;
                        try {
                            currentPageNo = Integer.parseInt(currentPage);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (currentPageNo > 1) {
                            Iv_pagePrev.setVisibility(View.VISIBLE);
                        } else {
                            Iv_pagePrev.setVisibility(View.GONE);
                        }
                    }

                    if (nextPage.length() > 0) {
                        Iv_pageNext.setVisibility(View.VISIBLE);
                    } else {
                        Iv_pageNext.setVisibility(View.GONE);
                    }

                    sDisplayDate = mDisplayFormat.format(calendar.getTime());
                    Tv_date.setText(sDisplayDate);

                    if (sCurrentDate.equalsIgnoreCase(sFilterDate)) {
                        Iv_next.setVisibility(View.INVISIBLE);
                    } else {
                        Iv_next.setVisibility(View.VISIBLE);
                    }


                } else {
                    Iv_pagePrev.setVisibility(View.GONE);
                    Iv_pageNext.setVisibility(View.GONE);
                    Lv_statement.setVisibility(View.GONE);
                    Tv_empty_view.setVisibility(View.VISIBLE);
                }

                if (dialog != null) {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
            return true;
        }
        return false;
    }

}
