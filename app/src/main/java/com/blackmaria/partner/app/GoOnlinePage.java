package com.blackmaria.partner.app;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.SystemClock;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Base64;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;

import com.blackmaria.partner.Pojo.MultipleLocationPojo;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ChatAvailabilityCheck;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.CurrencySymbolConverter;
import com.blackmaria.partner.Utils.GPSTracker;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Dashboard_constrain;
import com.blackmaria.partner.mylibrary.latlnginterpolation.LatLngInterpolator;
import com.blackmaria.partner.mylibrary.latlnginterpolation.MarkerAnimation;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.blackmaria.partner.latestpackageview.Database.GEODBHelper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

@SuppressWarnings("WrongConstant")
public class GoOnlinePage extends AppCompatActivity implements View.OnClickListener, com.google.android.gms.location.LocationListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    private RelativeLayout RL_go_offline;
    //    private TextView Tv_LastTrip;
    private TextView Tv_total_distance, Tv_timer, Tv_completedRides, Tv_cancelledRides, Tv_deniedRides;
    //    private TextView Tv_LastTrip, Tv_LastPaid;
    private ImageView Iv_vehicle, Iv_light;

    private String sNormalFareStatus = "", sBidFareStatus = "", sSosStatus = "";
    private ServiceRequest mRequest;
    private ServiceRequest mUpdateLocationRequest;
    Dialog dialog;

    private ConnectionDetector cd;
    private boolean isInternetPresent = false;
    private SessionManager sessionManager;
    private String sDriveID = "";

    private GoogleMap googleMap;
    private Marker carMarker;
    private String sDriverLat = "", sDriverLng = "";
    private GPSTracker gps;
    private Handler mapHandler = new Handler();

    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    PendingResult<LocationSettingsResult> result;
    final static int REQUEST_LOCATION = 199;
    public Location myLocation;
    private String MyCurrent_lat = "", MyCurrent_long = "";
    private double Gps_MyCurrent_lat = 0.0, Gps_MyCurrent_long = 0.0;

    private LatLng newLatLng, oldLatLng;
    private LatLngInterpolator mLatLngInterpolator;
    private Location oldLocation;

    private RelativeLayout Rl_PendingTrip, Rl_PendingTripContinue;
    private TextView Tv_pendingTrip;
    private String sRideId = "", sIsContinue = "", sDriverName = "", sVehicleNumber = "", sVehicleType = "",Iswallet_constraint = "",Isminwallet="";;
    private boolean isTripPending = false;

    private BroadcastReceiver finishReceiver;
    private ArrayList<MultipleLocationPojo> multipleDropList;
    private boolean isMultipleDropAvailable = false;

    private GEODBHelper myDBHelper;
    private MapFragment mapFragment;
    private String isReturnLatLngAvailable = "0", isReturnPickUPLatLngAvailable = "0";
    private String sReturnLat = "", sReturnLng = "", sReturnLocation = "", sReturnPickUpLat = "", sReturnPickUpLng = "";

    CountDownTimer currentTimeTimer;
    private TextView Tv_currentDate, Tv_driverName, Tv_currentTime, Tv_vehicleNumber;
    private ImageButton Ib_currentLocation;
    private SimpleDateFormat sdf_currentTime, sdf_currentDate;


    private Handler onlineTimeHandler = new Handler();
    private long startTime = 0L;
    private long upTime = 0L;
    private long timeInMilliseconds = 0L;
    private long timeSwapBuff = 0L;
    private long updatedTime = 0L;
    private int mins;
    private int secs;
    private int hours;
    private DecimalFormat df = new DecimalFormat("00");
    private SimpleDateFormat myDateTimeFormat = new SimpleDateFormat("EEE MMM dd hh:mm:ss zzz yyyy");
    private Date currentDate, onlineDate;
    private BroadcastReceiver timerReceiver;

    //    MapRipple mapRippleRed = null;
    Bitmap carIcon = null;

//    private MapRipple mapRippleRed, mapRippleGreen;
    private String frompageRequestadapter = "";

    public static Activity parent;
    public String message = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.go_online_page_new);

//        parent = GoOnlinePage.this;
//
//        sessionManager = new SessionManager(GoOnlinePage.this);

        // Receiving the data from broadcast
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.app.finish.GoOnlinePage");
        finishReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                finish();
            }
        };
        registerReceiver(finishReceiver, filter);

        try {
            setLocationRequest();
            buildGoogleApiClient();
        } catch (Exception e) {

        }

        initializeMap();
        initialize();

        if (getIntent().hasExtra("EXTRA")) {
            HashMap<String, Integer> user = sessionManager.getRequestCount();
            int req_count = user.get(SessionManager.KEY_COUNT);
            if(req_count < 1) {
                message = getIntent().getStringExtra("EXTRA");
                Intent intent = new Intent(GoOnlinePage.this, TimerPage.class);
                intent.putExtra("EXTRA", message);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }else
            if(getIntent().hasExtra("NewTrip")){
            Intent intent = new Intent(GoOnlinePage.this, NewTripPage.class);
            intent.putExtra("MessagePage", getIntent().getStringExtra("MessagePage").toString());
            intent.putExtra("Action", getIntent().getStringExtra("Action").toString());
            intent.putExtra("Username", getIntent().getStringExtra("Username").toString());
            intent.putExtra("Mobilenumber",getIntent().getStringExtra("Mobilenumber").toString());
            intent.putExtra("UserImage", getIntent().getStringExtra("UserImage").toString());
            intent.putExtra("RideId", getIntent().getStringExtra("RideId").toString());
            intent.putExtra("UserPickuplocation", getIntent().getStringExtra("UserPickuplocation").toString());
            intent.putExtra("UserPickupTime", getIntent().getStringExtra("UserPickupTime").toString());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
            startActivity(intent);
        }
    }


    private void initialize() {


        String carImage = sessionManager.getCarMarker();
        if (carImage != null && carImage.length() > 0) {
            carIcon = base64ToBitmap(sessionManager.getCarMarker());
//            carIcon = Bitmap.createScaledBitmap(carIcon, 100, 100, false);
        } else {
            carIcon = BitmapFactory.decodeResource(getResources(), R.drawable.carmove);
        }
        cd = new ConnectionDetector(GoOnlinePage.this);
        isInternetPresent = cd.isConnectingToInternet();
        multipleDropList = new ArrayList<MultipleLocationPojo>();
        myDBHelper = new GEODBHelper(GoOnlinePage.this);
        sVehicleType = sessionManager.getVehicleType();

//        sdf_currentDate = new SimpleDateFormat("dd MMMM yyyy");
        sdf_currentDate = new SimpleDateFormat("EEEE dd MMM yyyy - hh:mm:ss aa");
        sdf_currentTime = new SimpleDateFormat("hh:mm:ss aa");

        RL_go_offline = (RelativeLayout) findViewById(R.id.RL_quit);
        Rl_PendingTrip = (RelativeLayout) findViewById(R.id.go_onLine_pendingTrip_layout);
        Rl_PendingTripContinue = (RelativeLayout) findViewById(R.id.go_onLine_pendingTrip_continue_layout);
        Tv_pendingTrip = (TextView) findViewById(R.id.go_onLine_pendingTrip_label);
//        Tv_LastTrip = (TextView) findViewById(R.id.txt_last_trip);
        Tv_total_distance = (TextView) findViewById(R.id.txt_distance);
        Tv_timer = (TextView) findViewById(R.id.txt_timer);
        Tv_completedRides = (TextView) findViewById(R.id.txt_completed_trip);
        Tv_cancelledRides = (TextView) findViewById(R.id.txt_cancelled_trip);
        Tv_deniedRides = (TextView) findViewById(R.id.txt_denied_trip);

        Tv_currentDate = (TextView) findViewById(R.id.txt_date);
        Tv_driverName = (TextView) findViewById(R.id.txt_driver_name);
        Tv_currentTime = (TextView) findViewById(R.id.txt_time);
        Tv_vehicleNumber = (TextView) findViewById(R.id.txt_vehicle_number);
        Iv_vehicle = (ImageView) findViewById(R.id.img_vehicle);
        Iv_light = (ImageView) findViewById(R.id.img_light);
        startBlinkingAnimation();

        Ib_currentLocation = (ImageButton) findViewById(R.id.Ibtn_current_location);

        currentTimeTimer = new CountDownTimer(1000000000, 1000) {

            public void onTick(long millisUntilFinished) {
                setCurrentTime();
            }

            public void onFinish() {

            }
        };

        if (sVehicleType.equalsIgnoreCase("motorbike")) {
            Iv_vehicle.setImageResource(R.drawable.icn_round_bike);
        } else if (sVehicleType.equalsIgnoreCase("taxi")) {
            Iv_vehicle.setImageResource(R.drawable.icn_round_taxi);
        } else {
            Iv_vehicle.setImageResource(R.drawable.icn_round_car);
        }


        RL_go_offline.setOnClickListener(this);
        Rl_PendingTripContinue.setOnClickListener(this);
        Ib_currentLocation.setOnClickListener(this);

        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriveID = user.get(SessionManager.KEY_DRIVERID);
        sDriverName = user.get(SessionManager.KEY_DRIVER_NAME);
        sVehicleNumber = user.get(SessionManager.KEY_VEHICLENO);

        if (isInternetPresent) {
            GoOnlineRequest(Iconstant.driverMapView_Url);
        } else {
            alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }

        currentTimeTimer.start();
        Tv_driverName.setText(sDriverName);
        Tv_vehicleNumber.setText(sVehicleNumber);


    }

    private void setCurrentTime() {

        Calendar cal = Calendar.getInstance();

        if (Tv_currentDate != null) {
            String currentDate = sdf_currentDate.format(cal.getTime());
            Tv_currentDate.setText(currentDate);
        }

    }

    private void initializeMap() {
        if (googleMap == null) {
            mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.go_onLine_map_fragment);
            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    loadMap(googleMap);
                }
            });
        }
    }

    public void loadMap(GoogleMap arg) {
        googleMap = arg;
        boolean bs = googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getApplicationContext(), R.raw.style));
        googleMap.getUiSettings().setRotateGesturesEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);

    }


    @Override
    public void onClick(View view) {

        cd = new ConnectionDetector(GoOnlinePage.this);
        isInternetPresent = cd.isConnectingToInternet();

        if (isInternetPresent) {

            if (view == RL_go_offline) {
                if (isTripPending) {
                    alert("", getResources().getString(R.string.goonline_page_cannot_go_offline));
                } else {
                    confirmQuit(getResources().getString(R.string.label_sure_to_go_offline));
                }
            } else if (view == Rl_PendingTripContinue) {
                if (sIsContinue.equalsIgnoreCase("fare")) {
                    Intent intent = new Intent(GoOnlinePage.this, FarePage.class);
                    intent.putExtra("rideId", sRideId);
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                } else {
                    ContinueTripRequest(Iconstant.continuePendingTrip_Url);
                }
            } else if (view == Ib_currentLocation) {
                if (googleMap != null) {
                    if (gps != null && gps.canGetLocation()) {

                        try {

                            double lat = gps.getLatitude();
                            double lon = gps.getLongitude();

                            if (lat != 0.0 && lon != 0.0) {
                                LatLng latLng = new LatLng(gps.getLatitude(), gps.getLongitude());

                                float zoom = googleMap.getCameraPosition().zoom;
                                CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(zoom).build();
                                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }
            }

        } else {
            alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }

    }

    private void status_ON_OFF(String status, TextView mON, TextView mOFF) {
        if (status.equalsIgnoreCase("yes")) {
            mON.setTextColor(Color.parseColor("#57faff"));
            mOFF.setTextColor(Color.parseColor("#FFFFFF"));
            mON.setBackgroundResource(R.drawable.toggle_on);
            mOFF.setBackgroundResource(R.drawable.toggle_off);
        } else {
            mON.setTextColor(Color.parseColor("#FFFFFF"));
            mOFF.setTextColor(Color.parseColor("#CC0000"));
            mON.setBackgroundResource(R.drawable.toggle_off);
            mOFF.setBackgroundResource(R.drawable.toggle_on);
        }
    }


    //Update location
    private Runnable mapRunnable = new Runnable() {
        @Override
        public void run() {
            gps = new GPSTracker(GoOnlinePage.this);
            if (gps != null && gps.canGetLocation()) {
                Gps_MyCurrent_lat = gps.getLatitude();
                Gps_MyCurrent_long = gps.getLongitude();
                updateLocationRequest(Iconstant.updateDriverLocation);
            } else {
                enableGpsService();
            }
            mapHandler.postDelayed(this, 30000);
        }
    };


    //--------------Alert Method-----------
    private void alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(GoOnlinePage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void alertwalletcontraint(String title, String alert) {

        Toast.makeText(this, alert, Toast.LENGTH_LONG).show();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                GoOffLineRequest(Iconstant.updateAvailability_Url);
            }
        }, 5000);
    }



    private void confirmQuit(String title) {
        final Dialog confirmQuitDialog = new Dialog(GoOnlinePage.this);
        confirmQuitDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmQuitDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmQuitDialog.setCancelable(true);
        confirmQuitDialog.setContentView(R.layout.alert_sure_to_cancel);

        final TextView Tv_title = (TextView) confirmQuitDialog.findViewById(R.id.txt_label_title);
        final Button Btn_yes = (Button) confirmQuitDialog.findViewById(R.id.btn_yes);
        final Button Btn_no = (Button) confirmQuitDialog.findViewById(R.id.btn_no);

        Tv_title.setText(title);

        Btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmQuitDialog.dismiss();
                GoOffLineRequest(Iconstant.updateAvailability_Url);
            }
        });

        Btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmQuitDialog.dismiss();
            }
        });

        confirmQuitDialog.show();

    }

    //-------------------Go Online Request----------------
    private void GoOnlineRequest(String Url) {

        dialog = new Dialog(GoOnlinePage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriveID);

        System.out.println("--------------GoOnlineRequest Url-------------------" + Url);
        System.out.println("--------------GoOnlineRequest jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(GoOnlinePage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------GoOnlineRequest response-------------------" + response);
                String status = "", sCurrency = "", sLast_trip_dt = "", sLast_trip_distance = "", sLast_paid_mode = "", sLast_paid_amount = "", sCurrencySymbol = "", sTotalCompletedDistance = "", sDistanceUnit = "",
                        sOnlineHours = "", sTotalCompletedRides = "0", sCancelledRides = "0", sDeniedRides = "0";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {
                            JSONObject jsonObject = object.getJSONObject("response");
                            if (jsonObject.length() > 0) {
                                sNormalFareStatus = jsonObject.getString("normal_fare");
                                sBidFareStatus = jsonObject.getString("bid_fare");
                                sSosStatus = jsonObject.getString("sos");
                                sCurrency = jsonObject.getString("currency");
                                sCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(sCurrency);
                                sLast_trip_dt = jsonObject.getString("last_trip_dt");
                                sLast_paid_mode = jsonObject.getString("last_paid_mode");
                                sLast_paid_amount = jsonObject.getString("last_paid_amount");
                                sDriverLat = jsonObject.getString("driver_lat");
                                sDriverLng = jsonObject.getString("driver_lon");
                                sDistanceUnit = jsonObject.getString("distance_unit");
                                sTotalCompletedDistance = jsonObject.getString("total_completed_distance") + " " + sDistanceUnit;
                                sLast_trip_distance = jsonObject.getString("last_trip_distance") + " " + sDistanceUnit;
                                sOnlineHours = jsonObject.getString("online_hours");
                                sTotalCompletedRides = jsonObject.getString("total_completed_trip");
                                sCancelledRides = jsonObject.getString("cancelled_rides");
                                sDeniedRides = jsonObject.getString("declined_count");
                            }
                        } else {
                            String sResponse = object.getString("response");
                            alert(getResources().getString(R.string.action_error), sResponse);
                        }

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (status.equalsIgnoreCase("1")) {

                    /*if (sLast_trip_dt.length() > 0) {
                        Tv_LastTrip.setText(getResources().getString(R.string.goonline_page_label_last_trip) + "\n" + sLast_trip_dt);
                    } else {
                        Tv_LastTrip.setText(getResources().getString(R.string.goonline_page_label_last_trip) + "\n" + "--");
                    }*/

                    if (sLast_trip_distance.length() > 0) {
                        Tv_total_distance.setText(sLast_trip_distance);
                    }

                    Tv_completedRides.setText(sTotalCompletedRides);
                    Tv_cancelledRides.setText(sCancelledRides);
                    Tv_deniedRides.setText(sDeniedRides);

                    try {
                        if (sOnlineHours != null && sOnlineHours.length() > 0) {
                            try {
                                timeSwapBuff = Long.parseLong(sOnlineHours) * 1000;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        startTime = SystemClock.uptimeMillis();
                        onlineTimeHandler.post(updateTimerThread);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                if (dialog != null) {
                    dialog.dismiss();
                }

                ChatAvailabilityCheck chatAvailability = new ChatAvailabilityCheck(GoOnlinePage.this, "available");
                chatAvailability.postChatRequest();
            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }




    //-------------------Go OffLine Request----------------
    private void GoOffLineRequest(String Url) {

        dialog = new Dialog(GoOnlinePage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_pleasewait));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriveID);
        jsonParams.put("availability", "No");

        System.out.println("--------------GoOnlineRequest Url-------------------" + Url);
        System.out.println("--------------jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(GoOnlinePage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------response-------------------" + response);
                String status = "";

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {
                        status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {

                            /*sessionManager.setXmppServiceState("");
                            //Stop xmpp service
                            stopService(new Intent(GoOnlinePage.this, XmppService.class));*/


                            // Refresh Home Page
                            /*Intent intent = new Intent();
                            intent.setAction("com.app.Refresh.HomePage");
                            sendBroadcast(intent);*/

                            sessionManager.setOnlineTimeStatus("0");

                            /*if (isMyServiceRunning(TimerService.class)) {
                                stopService(new Intent(GoOnlinePage.this, TimerService.class));
                            }*/

                            Intent intent = new Intent(GoOnlinePage.this, Dashboard_constrain.class);
                            startActivity(intent);
                            finish();
                            overridePendingTransition(R.anim.enter, R.anim.exit);
                        } else {
                            String sResponse = object.getString("response");
                            alert(getResources().getString(R.string.action_error), sResponse);
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    //-------------------Continue Pending Trip Request----------------
    private void ContinueTripRequest(String Url) {

        dialog = new Dialog(GoOnlinePage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_pleasewait));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriveID);
        jsonParams.put("ride_id", sRideId);

        System.out.println("--------------Continue Pending Trip Url-------------------" + Url);
        System.out.println("--------------jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(GoOnlinePage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------Continue Pending Trip response-------------------" + response);
                String status = "", sTitle_text = "", sSubtitle_text = "", sRideStatus = "";
                String Str_Username = "", Str_UserId = "", Str_User_email = "", Str_Userphoneno = "", Str_Userrating = "", Str_userimg = "", Str_pickuplocation = "", Str_pickuplat = "", Str_pickup_long = "",
                        Str_pickup_time = "", Str_message = "", sCurrencySymbol = "", est_cost = "", location = "", currency_code = "", user_gender = "", eta_arrival = "", member_since = "", Str_droplat = "", Str_droplon = "", str_drop_location = "", Zero_res = "", sTripMode = "";
                String sReturnState = "", isReturnAvailable = "", Str_payment_type = "", Str_cancelled_rides = "", Str_completed_rides = "", Str_rider_type = "",
                        sPaymentIcon = "", sCompAvailable = "", sCompStage = "";


                String return_mode = "", is_return_over = "", closing_status = "";

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {
                        status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {
                            JSONObject jsonObject = object.getJSONObject("response");
                            if (jsonObject.length() > 0) {

                                JSONObject profile_jsonObject = jsonObject.getJSONObject("user_profile");
                                if (profile_jsonObject.length() > 0) {

                                    Str_Username = profile_jsonObject.getString("user_name");
                                    Str_UserId = profile_jsonObject.getString("user_id");
                                    Str_User_email = profile_jsonObject.getString("user_email");
                                    Str_Userphoneno = profile_jsonObject.getString("phone_number");
                                    Str_Userrating = profile_jsonObject.getString("user_review");
                                    Str_pickuplocation = profile_jsonObject.getString("pickup_location");
                                    Str_pickuplat = profile_jsonObject.getString("pickup_lat");
                                    Str_pickup_long = profile_jsonObject.getString("pickup_lon");
                                    Str_pickup_time = profile_jsonObject.getString("pickup_time");
                                    Str_userimg = profile_jsonObject.getString("user_image");
                                    Str_droplat = profile_jsonObject.getString("drop_lat");
                                    Str_droplon = profile_jsonObject.getString("drop_lon");
                                    str_drop_location = profile_jsonObject.getString("drop_loc");
                                    sTitle_text = profile_jsonObject.getString("title_text");
                                    sSubtitle_text = profile_jsonObject.getString("subtitle_text");
                                    sRideStatus = profile_jsonObject.getString("continue_trip");
                                    est_cost = profile_jsonObject.getString("est_cost");
                                    currency_code = profile_jsonObject.getString("currency");
                                    sCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(currency_code);
                                    eta_arrival = profile_jsonObject.getString("eta_arrival");
                                    member_since = profile_jsonObject.getString("member_since");
                                    user_gender = profile_jsonObject.getString("user_gender");
                                    location = profile_jsonObject.getString("location");
                                    Str_payment_type = profile_jsonObject.getString("payment_type");
                                    Str_cancelled_rides = profile_jsonObject.getString("cancelled_rides");
                                    Str_completed_rides = profile_jsonObject.getString("completed_rides");
                                    Str_rider_type = profile_jsonObject.getString("rider_type");

                                    if (profile_jsonObject.has("mode")) {
                                        sTripMode = profile_jsonObject.getString("mode");
                                    }

                                    sCompAvailable = profile_jsonObject.getString("comp_status");
                                    sCompStage = profile_jsonObject.getString("comp_stage");
                                    sPaymentIcon = profile_jsonObject.getString("payment_icon");

                                    if (profile_jsonObject.has("is_return_over")) {
                                        isReturnAvailable = profile_jsonObject.getString("is_return_over");
                                    }

                                    if (profile_jsonObject.has("return_mode")) {
                                        sReturnState = profile_jsonObject.getString("return_mode");
                                    }

                                    return_mode = profile_jsonObject.getString("return_mode");
                                    is_return_over = profile_jsonObject.getString("is_return_over");
                                    closing_status = profile_jsonObject.getString("closing_status");

                                    if (profile_jsonObject.has("multistop_array")) {

                                        Object multiDrop_array_check = profile_jsonObject.get("multistop_array");
                                        if (multiDrop_array_check instanceof JSONArray) {
                                            JSONArray multiDropArray = profile_jsonObject.getJSONArray("multistop_array");
                                            if (multiDropArray.length() > 0) {
                                                multipleDropList.clear();
                                                for (int i = 0; i < multiDropArray.length(); i++) {
                                                    JSONObject multipleDropArrayObject = multiDropArray.getJSONObject(i);

                                                    MultipleLocationPojo pojo = new MultipleLocationPojo();
                                                    pojo.setDropLocation(multipleDropArrayObject.getString("location"));
                                                    pojo.setIsEnd(multipleDropArrayObject.getString("is_end"));
                                                    pojo.setMode(multipleDropArrayObject.getString("mode"));
                                                    pojo.setRecordId(multipleDropArrayObject.getString("record_id"));

                                                    JSONObject latLngArrayObject = multipleDropArrayObject.getJSONObject("latlong");
                                                    if (latLngArrayObject.length() > 0) {
                                                        pojo.setDropLat(latLngArrayObject.getString("lat"));
                                                        pojo.setDropLon(latLngArrayObject.getString("lon"));
                                                    }

                                                    multipleDropList.add(pojo);
                                                }

                                                isMultipleDropAvailable = true;

                                            } else {
                                                multipleDropList.clear();
                                                isMultipleDropAvailable = false;
                                            }
                                        } else {
                                            multipleDropList.clear();
                                            isMultipleDropAvailable = false;
                                        }
                                    }


                                    Object return_array_check = profile_jsonObject.get("next_drop_location");
                                    if (return_array_check instanceof JSONObject) {
                                        JSONObject returnArrayObject = profile_jsonObject.getJSONObject("next_drop_location");
                                        JSONObject latLngArrayObject = returnArrayObject.getJSONObject("latlong");
                                        if (latLngArrayObject.length() > 0) {
                                            sReturnLat = latLngArrayObject.getString("lat");
                                            sReturnLng = latLngArrayObject.getString("lon");

                                            isReturnLatLngAvailable = "1";
                                        } else {
                                            isReturnLatLngAvailable = "0";
                                        }
                                    } else {
                                        isReturnLatLngAvailable = "0";
                                    }


                                    Object return_pickUp_array_check = profile_jsonObject.get("drop_location_array");
                                    if (return_pickUp_array_check instanceof JSONObject) {
                                        JSONObject returnArrayObject = profile_jsonObject.getJSONObject("drop_location_array");
                                        JSONObject latLngArrayObject = returnArrayObject.getJSONObject("latlong");
                                        if (latLngArrayObject.length() > 0) {
                                            sReturnPickUpLat = latLngArrayObject.getString("lat");
                                            sReturnPickUpLng = latLngArrayObject.getString("lon");

                                            if (returnArrayObject.has("location")) {
                                                sReturnLocation = returnArrayObject.getString("location");
                                            }

                                            isReturnPickUPLatLngAvailable = "1";
                                        } else {
                                            isReturnPickUPLatLngAvailable = "0";
                                        }
                                    } else {
                                        isReturnPickUPLatLngAvailable = "0";
                                    }


                                    myDBHelper.insertuser_id(Str_UserId);
                                }
                            }
                        } else {
                            String sResponse = object.getString("response");
                            alert(getResources().getString(R.string.action_error), sResponse);
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                if (status.equalsIgnoreCase("1")) {

                    Intent intent = new Intent(GoOnlinePage.this, TripPageNew.class);
                    intent.putExtra("address", Str_pickuplocation);
                    intent.putExtra("rideId", sRideId);
                    intent.putExtra("pickuplat", Str_pickuplat);
                    intent.putExtra("pickup_long", Str_pickup_long);
                    intent.putExtra("username", Str_Username);
                    intent.putExtra("userrating", Str_Userrating);
                    intent.putExtra("phoneno", Str_Userphoneno);
                    intent.putExtra("userimg", Str_userimg);
                    intent.putExtra("UserId", Str_UserId);
                    intent.putExtra("drop_lat", Str_droplat);
                    intent.putExtra("drop_lon", Str_droplon);
                    intent.putExtra("drop_location", str_drop_location);
                    intent.putExtra("title_text", sTitle_text);
                    intent.putExtra("subtitle_text", sSubtitle_text);
                    intent.putExtra("trip_status", sRideStatus);
                    intent.putExtra("eta_arrival", eta_arrival);
                    intent.putExtra("location", location);
                    intent.putExtra("member_since", member_since);
                    intent.putExtra("user_gender", user_gender);
                    intent.putExtra("est_cost", est_cost);
                    intent.putExtra("sCurrencySymbol", sCurrencySymbol);
                    intent.putExtra("TripMode", sTripMode);
                    intent.putExtra("IsReturnAvailable", isReturnAvailable);
                    intent.putExtra("ReturnState", sReturnState);
                    intent.putExtra("IsReturnLatLngAvailable", isReturnLatLngAvailable);
                    intent.putExtra("ReturnLat", sReturnLat);
                    intent.putExtra("ReturnLng", sReturnLng);
                    intent.putExtra("ReturnLocation", sReturnLocation);
                    intent.putExtra("ReturnPickUpLat", sReturnPickUpLat);
                    intent.putExtra("ReturnPickUpLng", sReturnPickUpLng);
                    intent.putExtra("payment_type", Str_payment_type);
                    intent.putExtra("cancelled_rides", Str_cancelled_rides);
                    intent.putExtra("completed_rides", Str_completed_rides);
                    intent.putExtra("rider_type", Str_rider_type);
                    intent.putExtra("comp_status", sCompAvailable);
                    intent.putExtra("comp_stage", sCompStage);
                    intent.putExtra("payment_icon", sPaymentIcon);

                    //Return multi waititime

                    intent.putExtra("return_mode", return_mode);
                    intent.putExtra("is_return_over", is_return_over);
                    intent.putExtra("closing_status", closing_status);

                    if (isMultipleDropAvailable) {
                        intent.putExtra("MultipleDropStatus", "1");
                        Bundle bundleObject = new Bundle();
                        bundleObject.putSerializable("additionalDropLocation", multipleDropList);
                        intent.putExtras(bundleObject);
                    } else {
                        intent.putExtra("MultipleDropStatus", "0");
                    }

                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    Rl_PendingTrip.setVisibility(View.GONE);
                }


                if (dialog != null) {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    //-------------------updateLocation Request----------------
    private void updateLocationRequest(String Url) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriveID);

        if (MyCurrent_lat.length() > 0 && MyCurrent_long.length() > 0) {
            jsonParams.put("latitude", MyCurrent_lat);
            jsonParams.put("longitude", MyCurrent_long);
        } else {
            jsonParams.put("latitude", String.valueOf(Gps_MyCurrent_lat));
            jsonParams.put("longitude", String.valueOf(Gps_MyCurrent_long));
        }


        mUpdateLocationRequest = new ServiceRequest(GoOnlinePage.this);
        mUpdateLocationRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------response-------------------" + response);
                String status = "", sAvailability = "", sVerifyStatus = "", sVerifyMessage = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {
                        status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {
                            JSONObject jsonObject = object.getJSONObject("response");
                            if (jsonObject.length() > 0) {
                                sAvailability = jsonObject.getString("availability");
                                sVerifyStatus = jsonObject.getString("verify_status");
                                sIsContinue = jsonObject.getString("is_continue");
                                sRideId = jsonObject.getString("ride_id");
                                Iswallet_constraint = jsonObject.getString("wallet_constraint");
                                Isminwallet = jsonObject.getString("min_wallet_amount ");
                                sVerifyMessage = jsonObject.getString("errorMsg");
                            }
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (status.equalsIgnoreCase("1")) {
                    if(Iswallet_constraint.equalsIgnoreCase("1")){
                        if (sAvailability.equalsIgnoreCase("Unavailable")) {
                            myDBHelper.insertRide_id(sRideId);
                            Rl_PendingTrip.setVisibility(View.VISIBLE);
                            isTripPending = true;
                        } else {
                            Rl_PendingTrip.setVisibility(View.GONE);
                            isTripPending = false;
                        }
                        Tv_pendingTrip.setText(sVerifyMessage);
                    }else{
                        alertwalletcontraint("Error",getResources().getString(R.string.wallet_amount_lowbalance));
                    }

                }

            }

            @Override
            public void onErrorListener() {
            }
        });
    }


    //Enabling the GoogleClient

    private void setLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    protected void startLocationUpdates() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }


    //Enabling Gps Service
    private void enableGpsService() {

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        //...
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(GoOnlinePage.this, REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_LOCATION:
                switch (resultCode) {
                    case Activity.RESULT_OK: {
                        Toast.makeText(GoOnlinePage.this, "Location enabled!", Toast.LENGTH_LONG).show();
                        break;
                    }

                    case Activity.RESULT_CANCELED: {
                        finish();
                        break;
                    }
                    default: {
                        break;
                    }
                }
                break;
        }
    }


    @Override
    public void onConnected(Bundle bundle) {
        myLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

        System.out.println("onconnect-------------------");

        this.myLocation = location;
        if (myLocation != null) {

            MyCurrent_lat = String.valueOf(myLocation.getLatitude());
            MyCurrent_long = String.valueOf(myLocation.getLongitude());

            try {
                LatLng latLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
                if (oldLatLng == null) {
                    oldLatLng = latLng;
                }
                newLatLng = latLng;
                if (mLatLngInterpolator == null) {
                    mLatLngInterpolator = new LatLngInterpolator.Linear();
                }
                oldLocation = new Location("");
                oldLocation.setLatitude(oldLatLng.latitude);
                oldLocation.setLongitude(oldLatLng.longitude);
                float bearingValue = oldLocation.bearingTo(location);

                if (googleMap != null) {
                    if (carMarker != null) {
                        if (!String.valueOf(bearingValue).equalsIgnoreCase("NaN")) {
                            if (location.getAccuracy() < 100.0 && location.getSpeed() < 6.95) {
                                rotateMarker(carMarker, bearingValue, googleMap);
                                MarkerAnimation.animateMarkerToGB(carMarker, latLng, mLatLngInterpolator);
                                float zoom = googleMap.getCameraPosition().zoom;
                                CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(zoom).build();
                                googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                            }
                        }
                    } else {
                        carMarker = googleMap.addMarker(new MarkerOptions()
                                .position(latLng)
//                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.carmove))
                                .icon(BitmapDescriptorFactory.fromBitmap(carIcon))
                                .anchor(0.5f, 0.5f)
                                .rotation(myLocation.getBearing())
                                .flat(true));
                        CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(18).build();
                        CameraUpdate camUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
                        googleMap.moveCamera(camUpdate);

                    }
                }
                oldLatLng = newLatLng;

            } catch (Exception e) {
            }
        }
    }

    //Method to smooth turn marker
    static public void rotateMarker(final Marker marker, final float toRotation, GoogleMap map) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final float startRotation = marker.getRotation();
        final long duration = 1555;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed / duration);

                float rot = t * toRotation + (1 - t) * startRotation;

                marker.setRotation(-rot > 180 ? rot / 2 : rot);
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (mapRunnable != null) {
            mapHandler.removeCallbacks(mapRunnable);
        }

        if (mGoogleApiClient != null)
            mGoogleApiClient.disconnect();

        /*if (mapRippleRed.isAnimationRunning()) {
            mapRippleRed.stopRippleMapAnimation();
        }*/

    }

    @Override
    protected void onResume() {
        super.onResume();
        mapRunnable.run();
        startLocationUpdates();
        if (myLocation == null) {
            myLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dialog != null) {
            dialog.dismiss();
        }

        if (mapRunnable != null) {
            mapHandler.removeCallbacks(mapRunnable);
        }
        if (currentTimeTimer != null) {
            currentTimeTimer.cancel();
        }
        unregisterReceiver(finishReceiver);
//        unregisterReceiver(timerReceiver);
        if (onlineTimeHandler != null) {
            onlineTimeHandler.removeCallbacks(updateTimerThread);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            //Do nothing
            return true;
        }
        return false;
    }

    private void startBlinkingAnimation() {
        Animation startAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blinking_animation);
        Iv_light.startAnimation(startAnimation);
    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        boolean b = false;
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                b = true;
                break;
            } else {
                b = false;
            }
        }
        return b;
    }

    private void registerBroadcast() {

        IntentFilter filter = new IntentFilter();
        filter.addAction("com.app.Timer.UpdateTime");
        timerReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent != null) {

                    String updatedTime = intent.getStringExtra("time");
                    if (Tv_timer != null) {
                        Tv_timer.setText(updatedTime);
                    }

                }

            }
        };
        registerReceiver(timerReceiver, filter);

    }

    private Runnable updateTimerThread = new Runnable() {

        public void run() {
            timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
            updatedTime = timeSwapBuff + timeInMilliseconds;
            secs = (int) (updatedTime / 1000);
            hours = secs / (60 * 60);
            mins = secs / 60;
            secs = secs % 60;
            if (mins >= 60) {
                if (mins == 60) {
                    hours = 1;
                    mins = 0;
                } else if (mins > 60) {
                    hours = (int) mins / 60;
                    mins = mins % 60;
                }
            }
            String time = String.format("%02d", hours) + ":" + String.format("%02d", mins) + ":"
                    + String.format("%02d", secs);
            Tv_timer.setText(time);
            onlineTimeHandler.postDelayed(this, 1 * 1000);
        }
    };

    private Bitmap base64ToBitmap(String image) {

        Bitmap decodedByte = null;

        try {

            byte[] decodedString = Base64.decode(image, Base64.DEFAULT);
            decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return decodedByte;

    }

    private void MapRippleRed(LatLng latLng, float zoom) {
//
//        mapRippleRed = new MapRipple(googleMap, latLng, GoOnlinePage.this);
//        mapRippleRed.withNumberOfRipples(3);
//        mapRippleRed.withFillColor(Color.parseColor("#ED203C"));
//        mapRippleRed.withStrokeColor(Color.parseColor("#EECED4"));
//        mapRippleRed.withStrokewidth(10);      // 10dp
//        mapRippleRed.withDistance(zoom * 10);      // 2000 metres radius
//        mapRippleRed.withRippleDuration(4000);    //12000ms
//        mapRippleRed.withTransparency(0.5f);

//        mapRippleRed.startRippleMapAnimation();

    }

    private void MapRippleGreen(LatLng latLng) {

//        mapRippleGreen = new MapRipple(googleMap, latLng, GoOnlinePage.this);
//        mapRippleGreen.withNumberOfRipples(3);
//        mapRippleGreen.withFillColor(Color.parseColor("#43B749"));
//        mapRippleGreen.withStrokeColor(Color.parseColor("#C3E2D7"));
//        mapRippleGreen.withStrokewidth(10);      // 10dp
//        mapRippleGreen.withDistance(100);      // 2000 metres radius
//        mapRippleGreen.withRippleDuration(4000);    //12000ms
//        mapRippleGreen.withTransparency(0.5f);
//        mapRippleGreen.startRippleMapAnimation();

    }

}

