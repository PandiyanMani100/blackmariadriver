package com.blackmaria.partner.app;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Mytrips.PastTripSummaryConstrain;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Tracking.StartTrip;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;


public class WaitingTimeDrop extends AppCompatActivity implements ApIServices.completelisner {

    private String sDriverID = "", sMessage = "", sRideId = "", strRecord_id = "";
    long min = 0, sec = 0;
    boolean isContinuing = false;
    private Dialog dialog;
    private ServiceRequest mRequest;
    private TextView Tv_waitingTime;
    private Button Btn_close;
    private RelativeLayout RL_tripInfo;
    private ImageView Iv_light;

    private ConnectionDetector cd;
    private SessionManager sessionManager;
    private Handler customHandler = new Handler();
    private Calendar calendar;
    private ProgressBar apiload;
    DecimalFormat precision = new DecimalFormat("00");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.waiting_page_drop);

        initialize();

        Btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertCloseTimer("Waiting Time", getResources().getString(R.string.waiting_label_sure_to_close));

            }
        });

        RL_tripInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cd.isConnectingToInternet()) {
                    Intent intent = new Intent(WaitingTimeDrop.this, PastTripSummaryConstrain.class);
                    intent.putExtra("rideid", sRideId);
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                } else {
                    Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                }
            }
        });

    }

    private void initialize() {

        sessionManager = new SessionManager(WaitingTimeDrop.this);
        cd = new ConnectionDetector(WaitingTimeDrop.this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);

        Tv_waitingTime = (TextView) findViewById(R.id.txt_waiting_time);
        apiload = findViewById(R.id.apiload);
        Btn_close = (Button) findViewById(R.id.btn_close);
        RL_tripInfo = (RelativeLayout) findViewById(R.id.Rl_trip_info);
        Iv_light = (ImageView) findViewById(R.id.img_light);

        startBlinkingAnimation();

        getIntentData();

        HashMap<String, String> getvalues = sessionManager.getWaitedTime();
        String waitingisthere = getvalues.get(SessionManager.KEY_WAITED_SECS);

        if(waitingisthere.equalsIgnoreCase("") || (waitingisthere.equalsIgnoreCase("0"))){
            customHandler.postDelayed(updateTimerThread, 1000);
        }else{
            HashMap<String, String> waitedTimeMap = sessionManager.getWaitedTime();
            String waitedTime = waitedTimeMap.get(SessionManager.KEY_CALENDAR_TIME);

            String minutes1 = waitedTimeMap.get(SessionManager.KEY_WAITED_MINS);
            String seconds1 = waitedTimeMap.get(SessionManager.KEY_WAITED_SECS);


            Date waitingTimeDate = new Date(waitedTime);
            Date currentDate = Calendar.getInstance().getTime();
            long diff = currentDate.getTime() - waitingTimeDate.getTime();

            long seconds = diff / 1000;
            long minutes = seconds / 60;
            long hours = minutes / 60;
            long days = hours / 24;

            sec = seconds + Long.parseLong(seconds1);
            min = minutes + Long.parseLong(minutes1);

            if (sec >= 60) {
                sec = 0;
                min = min + 1;
            }

            String updatedTime = precision.format(min) + ":" + precision.format(sec);
            calendar = Calendar.getInstance();
            sessionManager.setWaitedTime(min, sec, updatedTime, String.valueOf(calendar.getTime()));

            customHandler.postDelayed(updateTimerThread, 1000);
        }


    }

    private void getIntentData() {

        Intent intent = getIntent();
        if (intent != null) {
            sMessage = intent.getStringExtra("message");
            sRideId = intent.getStringExtra("rideID");
            isContinuing = getIntent().getBooleanExtra("isContinue", false);
            if (intent.hasExtra("recordId")) {
                strRecord_id = intent.getStringExtra("recordId");
            }
        }

    }

    private void startBlinkingAnimation() {
        Animation startAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blinking_animation);
        Iv_light.startAnimation(startAnimation);
    }

    private Runnable updateTimerThread = new Runnable() {

        @Override
        public void run() {

            sec = sec + 1;
            if (sec == 60) {
                sec = 0;
                min = min + 1;
            }

            String updatedTime = precision.format(min) + ":" + precision.format(sec);
            Tv_waitingTime.setText(updatedTime);

            calendar = Calendar.getInstance();
            sessionManager.setWaitedTime(min, sec, updatedTime, String.valueOf(calendar.getTime()));
            sessionManager.setWaitStatus("running");

            customHandler.postDelayed(updateTimerThread, 1000);

        }
    };


    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (customHandler != null) {
            customHandler.removeCallbacks(updateTimerThread);
            customHandler = null;
        }

    }

    //--------------Alert Function------------------
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(WaitingTimeDrop.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        mDialog.show();
    }


    private void AlertCloseTimer(String title, String alert) {
        final PkDialog mDialog = new PkDialog(WaitingTimeDrop.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (customHandler != null) {
                    customHandler.removeCallbacks(updateTimerThread);
                    customHandler = null;
                }
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                if (cd.isConnectingToInternet()) {
                    StopWaitingTimeRequest(Iconstant.waiting_close_drop);
                } else {
                    Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                }
                sessionManager.setWaitStatus("completed");


            }
        });

        mDialog.setNegativeButton(getResources().getString(R.string.action_cancel), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });

        mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                Btn_close.setEnabled(true);
            }
        });

        mDialog.show();
        Btn_close.setEnabled(false);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            //Do nothing
            return true;
        }
        return false;
    }

    //-------------Stop Waiting Time Request-----------
    private void StopWaitingTimeRequest(String Url) {
        apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(WaitingTimeDrop.this, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("record_id", strRecord_id);
        jsonParams.put("ride_id", sRideId);

        ApIServices apIServices = new ApIServices(WaitingTimeDrop.this, WaitingTimeDrop.this);
        apIServices.dooperation(Url, jsonParams);
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    private void alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(WaitingTimeDrop.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(WaitingTimeDrop.this, StartTrip.class));
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void sucessresponse(String response) {
        apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        String status = "", sResponse = "", sCompWaitingTime = "";
        try {
            JSONObject object = new JSONObject(response);
            sessionManager.setContinuetripjson(object.toString());
            status = object.getString("status");
            if (status.equalsIgnoreCase("1")) {
                if (customHandler != null) {
                    customHandler.removeCallbacks(updateTimerThread);
                    customHandler = null;
                }
                if (dialog != null) {
                    dialog.dismiss();
                }
                sessionManager.setWaitedTime(0, 0, "", "");
                alert(getResources().getString(R.string.action_success), "Waiting time closed sucessfully");
            } else {
                Alert(getResources().getString(R.string.action_error), sResponse);
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void errorreponse() {
        apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
    }

    @Override
    public void jsonexception() {
        apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        AppUtils.toastprint(WaitingTimeDrop.this, "Json Exception");
    }
}
