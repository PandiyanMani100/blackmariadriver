package com.blackmaria.partner.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;


/**
 * Created by user144 on 9/18/2017.
 */

public class NewCloudMoneyReloadBankResponse extends ActivityHockeyApp implements View.OnClickListener {

    private ImageView imgBack;
    private RelativeLayout verifylayout;
    private TextView tvAmountcode;
    private TextView dueAmount;
    private String reloadAmoun = "", paymentCode = "";

    SessionManager session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_cloud_money_payment_due);
        initialize();
    }

    private void initialize() {

        session = new SessionManager(NewCloudMoneyReloadBankResponse.this);
        imgBack = (ImageView) findViewById(R.id.img_back);
        dueAmount = (TextView) findViewById(R.id.due_amount);
        verifylayout = (RelativeLayout) findViewById(R.id.verifylayout);
        tvAmountcode = (TextView) findViewById(R.id.tv_amountcode);

        reloadAmoun = getIntent().getStringExtra("reloadamount");
        paymentCode = getIntent().getStringExtra("paymentcode");
        String currency = session.getCurrency();
        tvAmountcode.setText(paymentCode);
        dueAmount.setText("AMOUNT DUE :" + " " + currency + " " + reloadAmoun);

        imgBack.setOnClickListener(this);
        verifylayout.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v == imgBack) {

            Intent intent = new Intent();
            intent.setAction("com.package.ACTION_WALLET_RELOAD_SUCCESS");
            sendBroadcast(intent);

            Intent intent1 = new Intent(NewCloudMoneyReloadBankResponse.this, WalletMoneyPage1.class);
            intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent1);
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (v == verifylayout) {

            Intent intent = new Intent();
            intent.setAction("com.package.ACTION_WALLET_RELOAD_SUCCESS");
            sendBroadcast(intent);

            Intent intent1 = new Intent(NewCloudMoneyReloadBankResponse.this, WalletMoneyPage1.class);
            intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent1);
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
}
