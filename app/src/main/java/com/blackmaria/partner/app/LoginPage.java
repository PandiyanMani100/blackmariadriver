package com.blackmaria.partner.app;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.FragmentActivityHockeyApp;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.CountryDialCode;
import com.blackmaria.partner.Utils.GPSTracker;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.view.signup.RegistrationTerms_Constrain;
import com.blackmaria.partner.mylibrary.pushnotification.GCMInitializer;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.blackmaria.partner.latestpackageview.Countrycodepicker.countrycodepicker.CountryPicker;
import com.blackmaria.partner.latestpackageview.Countrycodepicker.countrycodepicker.CountryPickerListener;

import com.devspark.appmsg.AppMsg;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class LoginPage extends FragmentActivityHockeyApp {

    private TextView Tv_go;
    private EditText Et_MobileNo;
    private GPSTracker gpsTracker;
    private TextView Tv_HeaderText;
    private ConnectionDetector cd;
    private boolean isInternetPresent = false;
    private Dialog dialog;
    private String GCM_Id = "";
    private Handler mHandler;
    private SessionManager session;
    private TextView Tv_countryCode;
    private ServiceRequest mRequest;
    private String SdeviceToken = "";
    CountryPicker picker;
    private String SuserId = "", ScountryCode = "", SmobNo = "", Spage = "", Sotp_status = "", Sotp = "";
    String driver_id = "", sLatitude = "", sLongitude = "", app_identity_name = "", Language_code = "", sPrivacyStatus = "", sReferralStatus = "";
    final static int otp_request_code = 100;
    private boolean isAppInfoAvailable = false;

    ImageView img1, img2, img3, img4, img5, img6, img7, img8, img9, img0, img_hash, img_star;
    String number = "", sContact_mail = "", sCustomerServiceNumber = "", sSiteUrl = "", sXmppHostUrl = "", sHostName = "", sFacebookId = "", sGooglePlusId = "", sPhoneMasking = "";

    private ImageView Iv_flag, Iv_status;
    private Button Btn_register;

    AppMsg snack = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_4);
        initialize();

        Et_MobileNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(Et_MobileNo.getWindowToken(), 0);
            }
        });

        Tv_countryCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });

        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {
                picker.dismiss();
                Tv_countryCode.setText(dialCode);

                String drawableName = "flag_"
                        + code.toLowerCase(Locale.ENGLISH);
                Iv_flag.setImageResource(getResId(drawableName));
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(Tv_countryCode.getWindowToken(), 0);
            }
        });

        Tv_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!"Profile".equalsIgnoreCase(Spage)) {

                    if (!isValidPhoneNumber(Et_MobileNo.getText().toString())) {
//                        erroredit(Et_MobileNo, getResources().getString(R.string.login_page_alert_phoneNo));
                        AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.login_page_alert_phoneNo));
                    } else if (Tv_countryCode.getText().toString().equalsIgnoreCase("code")) {
//                        erroredit(Et_MobileNo, getResources().getString(R.string.login_page_alert_country_code));
                        AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.login_page_alert_country_code));
                    } else {
                        cd = new ConnectionDetector(LoginPage.this);
                        isInternetPresent = cd.isConnectingToInternet();
                        if (isInternetPresent) {
                            mHandler.post(dialogRunnable);

                            SdeviceToken = Settings.Secure.getString(getContentResolver(),
                                    Settings.Secure.ANDROID_ID);

                            System.out.println("-------SdeviceToken---" + SdeviceToken);
                            //---------Getting GCM Id----------
                            GCMInitializer initializer = new GCMInitializer(LoginPage.this, new GCMInitializer.CallBack() {
                                @Override
                                public void onRegisterComplete(String registrationId) {
                                    GCM_Id = registrationId;
                                    System.out.println("GCM_Id---------" + GCM_Id);
                                    System.out.println("login---------" + Iconstant.loginurl);
                                    PostRequest(Iconstant.loginurl);

                                }

                                @Override
                                public void onError(String errorMsg) {
                                    PostRequest(Iconstant.loginurl);
                                    System.out.println("login---------" + Iconstant.loginurl);
                                }
                            });
                            initializer.init();

                        } else {
                            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                        }
                    }
                } else {
                    if (!isValidPhoneNumber(Et_MobileNo.getText().toString())) {
//                        erroredit(Et_MobileNo, getResources().getString(R.string.login_page_alert_phoneNo));
                        AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.login_page_alert_phoneNo));
                    } else if (Tv_countryCode.getText().toString().equalsIgnoreCase("code")) {
//                        erroredit(Et_MobileNo, getResources().getString(R.string.login_page_alert_country_code));
                        AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.login_page_alert_country_code));
                    } else {
                        cd = new ConnectionDetector(LoginPage.this);
                        isInternetPresent = cd.isConnectingToInternet();
                        if (isInternetPresent) {
                            PostRequest_mobnoChange(Iconstant.Edit_profile_mobileNo_url);
                        } else {
                            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                        }
                    }
                }

            }
        });


        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                number = number + "1";
                Et_MobileNo.setText(number);
                Et_MobileNo.setSelection(Et_MobileNo.getText().length());
            }
        });
        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                number = number + "2";
                Et_MobileNo.setText(number);
                Et_MobileNo.setSelection(Et_MobileNo.getText().length());
            }
        });
        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                number = number + "3";
                Et_MobileNo.setText(number);
                Et_MobileNo.setSelection(Et_MobileNo.getText().length());
            }
        });
        img4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                number = number + "4";
                Et_MobileNo.setText(number);
                Et_MobileNo.setSelection(Et_MobileNo.getText().length());
            }
        });
        img5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                number = number + "5";
                Et_MobileNo.setText(number);
                Et_MobileNo.setSelection(Et_MobileNo.getText().length());
            }
        });
        img6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                number = number + "6";
                Et_MobileNo.setText(number);
                Et_MobileNo.setSelection(Et_MobileNo.getText().length());
            }
        });
        img7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                number = number + "7";
                Et_MobileNo.setText(number);
                Et_MobileNo.setSelection(Et_MobileNo.getText().length());
            }
        });
        img8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                number = number + "8";
                Et_MobileNo.setText(number);
                Et_MobileNo.setSelection(Et_MobileNo.getText().length());
            }
        });
        img9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                number = number + "9";
                Et_MobileNo.setText(number);
                Et_MobileNo.setSelection(Et_MobileNo.getText().length());
            }
        });
        img0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                number = number + "0";
                Et_MobileNo.setText(number);
                Et_MobileNo.setSelection(Et_MobileNo.getText().length());
                //   no.getText().append("0");
            }
        });
        img_hash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String SMobileNo = Et_MobileNo.getText().toString();
                if (SMobileNo != null && SMobileNo.length() > 0) {
                    Et_MobileNo.setText(SMobileNo.substring(0, SMobileNo.length() - 1));
                    Et_MobileNo.setSelection(Et_MobileNo.getText().length());
                    number = Et_MobileNo.getText().toString();

                }
            }
        });

        img_hash.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                String SMobileNo = Et_MobileNo.getText().toString();
                if (SMobileNo != null && SMobileNo.length() > 0) {
                    Et_MobileNo.setText("");
                    number = Et_MobileNo.getText().toString();

                }

                return false;
            }
        });


        Btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginPage.this, RegistrationTerms_Constrain.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);


            }
        });

    }

    private void initialize() {
        session = new SessionManager(LoginPage.this);
        cd = new ConnectionDetector(LoginPage.this);
        isInternetPresent = cd.isConnectingToInternet();
        mHandler = new Handler();
        picker = CountryPicker.newInstance(getResources().getString(R.string.select_country_lable));

        Et_MobileNo = (EditText) findViewById(R.id.edt_mobile_no);
        Tv_countryCode = (TextView) findViewById(R.id.txt_country_code);
        Tv_go = (TextView) findViewById(R.id.txt_go);
        Tv_HeaderText = (TextView) findViewById(R.id.txt_label_mobile_no);
        Iv_flag = (ImageView) findViewById(R.id.img_flag);
        Iv_status = (ImageView) findViewById(R.id.img_status);

        startBlinkingAnimation();

        img1 = (ImageView) findViewById(R.id.img1);
        img2 = (ImageView) findViewById(R.id.img2);
        img3 = (ImageView) findViewById(R.id.img3);
        img4 = (ImageView) findViewById(R.id.img4);
        img5 = (ImageView) findViewById(R.id.img5);
        img6 = (ImageView) findViewById(R.id.img6);
        img7 = (ImageView) findViewById(R.id.img7);
        img8 = (ImageView) findViewById(R.id.img8);
        img9 = (ImageView) findViewById(R.id.img9);
        img0 = (ImageView) findViewById(R.id.img0);
        img_hash = (ImageView) findViewById(R.id.img_hash);
        img_star = (ImageView) findViewById(R.id.img_star);
        Btn_register = (Button) findViewById(R.id.btn_register);


        Typeface face = Typeface.createFromAsset(getAssets(),
                "fonts/AdventPro-SemiBold.ttf");
        Tv_HeaderText.setTypeface(face);
        // close keyboard
        InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(Et_MobileNo.getWindowToken(), 0);

       /* LoginPageAdapter adapter = new LoginPageAdapter(LoginPage.this, number, imageId);
        Gv_mobileNo.setAdapter(adapter);*/

        Intent intent = getIntent();
        if (intent.hasExtra("page")) {
            SuserId = intent.getStringExtra("userId");
            ScountryCode = intent.getStringExtra("countrycode");
            SmobNo = intent.getStringExtra("MobNo");
            Spage = intent.getStringExtra("page");

            Tv_countryCode.setText(ScountryCode);
            Et_MobileNo.setText(SmobNo);
            Et_MobileNo.setSelection(Et_MobileNo.getText().length());

        }


        gpsTracker = new GPSTracker(LoginPage.this);
        if (gpsTracker.canGetLocation() && gpsTracker.isgpsenabled()) {

            double MyCurrent_lat = gpsTracker.getLatitude();
            sLatitude = String.valueOf(gpsTracker.getLatitude());
            double MyCurrent_long = gpsTracker.getLongitude();
            sLongitude = String.valueOf(gpsTracker.getLongitude());

            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            try {
                List<Address> addresses = geocoder.getFromLocation(MyCurrent_lat, MyCurrent_long, 1);
                if (addresses != null && !addresses.isEmpty()) {

                    String Str_getCountryCode = addresses.get(0).getCountryCode();
                    if (Str_getCountryCode.length() > 0 && !Str_getCountryCode.equals(null) && !Str_getCountryCode.equals("null")) {
                        String Str_countyCode = CountryDialCode.getCountryCode(Str_getCountryCode);
                        Tv_countryCode.setText(Str_countyCode);
                        String drawableName = "flag_"
                                + Str_getCountryCode.toLowerCase(Locale.ENGLISH);
                        Iv_flag.setImageResource(getResId(drawableName));
                        Et_MobileNo.setSelection(Et_MobileNo.getText().length());
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }

    // validating Phone Number
    public static final boolean isValidPhoneNumber(CharSequence target) {
        if (target == null || TextUtils.isEmpty(target) || target.length() <= 5) {
            return false;
        } else {
            return Patterns.PHONE.matcher(target).matches();
        }
    }

    //--------------------Code to set error for EditText-----------------------
    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(LoginPage.this, R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }


    //--------Handler Method------------
    Runnable dialogRunnable = new Runnable() {
        @Override
        public void run() {
            dialog = new Dialog(LoginPage.this);
            dialog.getWindow();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.custom_loading);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }
    };

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(LoginPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void AlertLoginFailed() {

        //--------Adjusting Dialog width-----
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 85% of the screen
        int screenHeight = (int) (metrics.heightPixels * 0.85);//fill only 80% of the screen

        final Dialog loginFailedDialog = new Dialog(LoginPage.this, R.style.SlideUpDialog);
        loginFailedDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loginFailedDialog.setContentView(R.layout.alert_login_fail);
        loginFailedDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        loginFailedDialog.getWindow().setLayout(screenWidth, screenHeight);

        final RelativeLayout RL_joinNow = (RelativeLayout) loginFailedDialog.findViewById(R.id.RL_join_now);
        final Button Btn_learnMore = (Button) loginFailedDialog.findViewById(R.id.btn_learn_more);
        final ImageView Iv_close = (ImageView) loginFailedDialog.findViewById(R.id.img_close);

        RL_joinNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Intent intent = new Intent(Intent.ACTION_VIEW);
//                intent.setData(Uri.parse(Iconstant.driver_signup_url));
//                startActivity(intent);
//                overridePendingTransition(R.anim.enter, R.anim.exit);
                Intent intent = new Intent(LoginPage.this, RegistrationTerms_Constrain.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        Btn_learnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(Iconstant.about_us_url));
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginFailedDialog.dismiss();
            }
        });

        loginFailedDialog.show();

    }

    private void PostRequest(String Url) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("phone_number", Et_MobileNo.getText().toString());
        jsonParams.put("country_code", Tv_countryCode.getText().toString());
        jsonParams.put("deviceToken", SdeviceToken);
        jsonParams.put("gcm_id", GCM_Id);
        jsonParams.put("lat", String.valueOf(gpsTracker.getLatitude()));
        jsonParams.put("lon", String.valueOf(gpsTracker.getLongitude()));
        System.out.println("-----------Login jsonParams--------------" + jsonParams);


        mRequest = new ServiceRequest(LoginPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------Login response-------------------" + response);

                String Sstatus = "", Smessage = "";

                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    Smessage = object.getString("message");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        Sotp_status = object.getString("otp_status");
                        Sotp = object.getString("otp");
                        sPrivacyStatus = object.getString("privacy_status");
                        sReferralStatus = object.getString("referal_code");
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    dialogDismiss();
                }

                dialogDismiss();

                if (Sstatus.equalsIgnoreCase("1")) {
                    postRequest_applaunch(Iconstant.getAppInfo);
                } else {
//                    Alert(getResources().getString(R.string.action_error), Smessage);
                    AlertLoginFailed();
                }
                // close keyboard
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(Et_MobileNo.getWindowToken(), 0);

            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });
    }

    //----------------------------------Mobile number change----------------------------------------------
    private void PostRequest_mobnoChange(String Url) {

        dialog = new Dialog(LoginPage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("phone_number", Et_MobileNo.getText().toString());
        jsonParams.put("country_code", Tv_countryCode.getText().toString());
        jsonParams.put("driver_id", SuserId);
        System.out.println("-----------Mobile number change jsonParams--------------" + jsonParams);


        mRequest = new ServiceRequest(LoginPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                Log.e("registr", response);

                System.out.println("--------------Mobile number change reponse-------------------" + response);

                String Sstatus = "", Smessage = "";

                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    Smessage = object.getString("response");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        Sotp_status = object.getString("otp_status");
                        Sotp = object.getString("otp");
                        sPrivacyStatus = object.getString("privacy_status");
                        sReferralStatus = object.getString("referal_code");
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();

                    dialogDismiss();


                }

                if (Sstatus.equalsIgnoreCase("1")) {
                    Intent intent = new Intent(LoginPage.this, OtpPage.class);
                    intent.putExtra("Otp_Status", Sotp_status);
                    intent.putExtra("Otp", Sotp);
                    intent.putExtra("Phone", Et_MobileNo.getText().toString());
                    intent.putExtra("CountryCode", Tv_countryCode.getText().toString());
                    intent.putExtra("userId", SuserId);
                    intent.putExtra("page", "Profile");
                    intent.putExtra("privacyStatus", sPrivacyStatus);
                    intent.putExtra("referralStatus", sReferralStatus);
                    startActivityForResult(intent, otp_request_code);
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

                } else {
                    Alert(getResources().getString(R.string.action_error), Smessage);
                }
                // close keyboard
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(Et_MobileNo.getWindowToken(), 0);
                dialogDismiss();
            }

            @Override
            public void onErrorListener() {
                dialogDismiss();
            }
        });
    }


    private void postRequest_applaunch(String Url) {

        System.out.println("-------------Splash App Information Url----------------" + Url);
        HashMap<String, String> user = session.getUserDetails();
        driver_id = user.get(SessionManager.KEY_DRIVERID);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_type", "driver");
        jsonParams.put("id", driver_id);
        jsonParams.put("lat", sLatitude);
        jsonParams.put("lon", sLongitude);
        mRequest = new ServiceRequest(LoginPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Splash App Information Response----------------" + response);


                String Str_status = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");
                    if (Str_status.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        if (response_object.length() > 0) {
                            JSONObject info_object = response_object.getJSONObject("info");
                            if (info_object.length() > 0) {
                                sContact_mail = info_object.getString("site_contact_mail");
                                sCustomerServiceNumber = info_object.getString("customer_service_number");
                                session.setCustomerServiceNo(sCustomerServiceNumber);
                                sSiteUrl = info_object.getString("site_url");
                                sXmppHostUrl = info_object.getString("xmpp_host_url");
                                sHostName = info_object.getString("xmpp_host_name");
                                app_identity_name = info_object.getString("app_identity_name");
                                sPhoneMasking = info_object.getString("phone_masking_status");
                                Language_code = info_object.getString("lang_code");
                                if (info_object.has("driver_review")) {
                                    session.setDriverReview(info_object.getString("driver_review"));
                                }
                                if (info_object.has("ride_earn")) {
                                    session.setReferelStatus(info_object.getString("ride_earn"));
                                }
                                isAppInfoAvailable = true;

                                try {
                                    JSONArray jsonarray_emergendy_no = info_object.getJSONArray("emergencyNumbers");
                                    for (int i = 0; i <= jsonarray_emergendy_no.length() - 1; i++) {
                                        String value = jsonarray_emergendy_no.getJSONObject(i).getString("title");
                                        if (value.equalsIgnoreCase("Police")) {
                                            session.setPolice(jsonarray_emergendy_no.getJSONObject(i).getString("number"));
                                        } else if (value.equalsIgnoreCase("Fire")) {
                                            session.setFire(jsonarray_emergendy_no.getJSONObject(i).getString("number"));
                                        } else if (value.equalsIgnoreCase("Ambulance")) {
                                            session.setAmbulance(jsonarray_emergendy_no.getJSONObject(i).getString("number"));
                                        }
                                    }

                                } catch (Exception e) {

                                }

                            } else {
                                isAppInfoAvailable = false;
                            }
                        } else {
                            isAppInfoAvailable = false;
                        }
                    } else {
                        isAppInfoAvailable = false;
                    }
                    if (Str_status.equalsIgnoreCase("1") && isAppInfoAvailable) {
                        HashMap<String, String> language = session.getLanaguage();
                        Locale locale = null;

                        switch (Language_code) {

                            case "en":
                                locale = new Locale("en");
                                session.setlamguage("en", "en");
                                break;
                            case "es":
                                locale = new Locale("es");
                                session.setlamguage("es", "es");
                                break;

                            default:
                                locale = new Locale("en");
                                session.setlamguage("en", "en");
                                break;
                        }

                        Locale.setDefault(locale);
                        Configuration config = new Configuration();
                        config.locale = locale;
                        getApplicationContext().getResources().updateConfiguration(config, getApplicationContext().getResources().getDisplayMetrics());


                        session.setXmpp(sXmppHostUrl, sHostName);
                        session.setAgent(app_identity_name);
                        session.setPhoneMaskingDetail(sPhoneMasking);
                        session.setContactNumber(sCustomerServiceNumber);
                        session.setContactEmail(sContact_mail);
                        dialogDismiss();

                        Intent intent = new Intent(LoginPage.this, OtpPage.class);
                        intent.putExtra("Otp_Status", Sotp_status);
                        intent.putExtra("Otp", Sotp);
                        intent.putExtra("Phone", Et_MobileNo.getText().toString());
                        intent.putExtra("CountryCode", Tv_countryCode.getText().toString());
                        intent.putExtra("GcmID", GCM_Id);
                        intent.putExtra("Device_Token", SdeviceToken);
                        intent.putExtra("privacyStatus", sPrivacyStatus);
                        intent.putExtra("referralStatus", sReferralStatus);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.enter, R.anim.exit);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {


            }
        });
    }


    private void dialogDismiss() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }


    //------------------------OnActivity Result-------------------------


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {

            case otp_request_code:

                if (requestCode == otp_request_code && resultCode == RESULT_OK && data != null) {

                    String Scountrycode = data.getStringExtra("countryCode");
                    String SmobNo = data.getStringExtra("phoneNo");
                    String Smessage = data.getStringExtra("message");
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("countryCode", Scountrycode);
                    returnIntent.putExtra("phoneNo", SmobNo);
                    returnIntent.putExtra("message", Smessage);
                    setResult(RESULT_OK, returnIntent);
                    overridePendingTransition(R.anim.slideup, R.anim.slidedown);
                    finish();

                }
                break;
        }


        super.onActivityResult(requestCode, resultCode, data);
    }


    private int getResId(String drawableName) {

        try {
            Class<com.blackmaria.partner.R.drawable> res = R.drawable.class;
            Field field = res.getField(drawableName);
            int drawableId = field.getInt(null);
            return drawableId;
        } catch (Exception e) {
            Log.e("CountryCodePicker", "Failure to get drawable id.", e);
        }
        return -1;
    }

    private void startBlinkingAnimation() {
        Animation startAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blinking_animation);
        Iv_status.startAnimation(startAnimation);
    }




    /*public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }*/


    private void AlertError(String title, String message) {

        /*String msg = title + "\n" + message;

        if (snack == null) {
            snack = AppMsg.makeText(LoginPage.this, msg.toUpperCase(), AppMsg.STYLE_CONFIRM);
        }

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

//        AppMsg.Style style = new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red_color);
        snack.setView(view);
        snack.setLayoutGravity(Gravity.TOP);
        snack.setPriority(AppMsg.PRIORITY_NORMAL);
//        snack.setAnimation(R.anim.enter, R.anim.exit);
        snack.setAnimation(R.anim.slide_down_anim, R.anim.slide_up_anim);
        System.out.println("GANESH ---> Is snack bar showing ---> " + snack.isFloating());


        snack.show();*/

        String msg = title + "\n" + message;

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        Snackbar bar = Snackbar.with(LoginPage.this); // context
        bar.addView(view);
        bar.colorResource(R.color.dark_red);
        bar.position(Snackbar.SnackbarPosition.TOP);
        bar.type(SnackbarType.MULTI_LINE); // Set is as a multi-line snackbar
//        bar.text(msg); // text to be displayed
        bar.duration(Snackbar.SnackbarDuration.LENGTH_SHORT); // make it shorter
        bar.animation(true); // don't animate it

        SnackbarManager.show(bar, LoginPage.this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

    private void AlertErr() {

        SnackbarManager.show(
                Snackbar.with(getApplicationContext()) // context
                        .colorResource(R.color.red_color)
                        .position(Snackbar.SnackbarPosition.TOP)
                        .type(SnackbarType.MULTI_LINE) // Set is as a multi-line snackbar
                        .text("This is a multi-line snackbar. Keep in mind that snackbars are " +
                                "meant for VERY short messages") // text to be displayed
                        .duration(Snackbar.SnackbarDuration.LENGTH_SHORT) // make it shorter
                        .animation(true) // don't animate it
                , this); // where it is displayed


    }

}


