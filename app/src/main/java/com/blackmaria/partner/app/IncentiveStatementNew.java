package com.blackmaria.partner.app;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.Pojo.IncentiveStatementDetail;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by GANESH on 25-09-2017.
 */

public class IncentiveStatementNew extends ActivityHockeyApp implements View.OnClickListener {

    private ImageView Iv_back;
    private TextView Tv_weeklyTarget, Tv_dailyTrip, Tv_dailyMile, Tv_scheduled, Tv_special, Tv_week1, Tv_week2,
            Tv_week3, Tv_week4, Tv_week5, Tv_month;

    private SessionManager sessionManager;
    private ConnectionDetector cd;
    private Dialog dialog;
    private ServiceRequest mRequest;

    private String sDriverID = "", sCurrency = "", sTotalIncentives = "", sTotalAmount = "", sTotalPayout = "", sDisplayMonth = "";
    private boolean isDataAvailable = false;
    private ArrayList<IncentiveStatementDetail> listIncentive;
    final int ColorTransWhite40 = Color.parseColor("#40ffffff");
    final int ColorTransWhite70 = Color.parseColor("#70ffffff");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.driver_incentive_statement);

        initialize();

    }

    private void initialize() {
        sessionManager = new SessionManager(IncentiveStatementNew.this);
        cd = new ConnectionDetector(IncentiveStatementNew.this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        listIncentive = new ArrayList<IncentiveStatementDetail>();

        Iv_back = (ImageView) findViewById(R.id.img_back);
        Tv_month = (TextView) findViewById(R.id.txt_month);

        Tv_weeklyTarget = (TextView) findViewById(R.id.txt_amount_weekly_target);
        Tv_dailyTrip = (TextView) findViewById(R.id.txt_amount_daily_trip_incentives);
        Tv_dailyMile = (TextView) findViewById(R.id.txt_amount_daily_mile_incentives);
        Tv_scheduled = (TextView) findViewById(R.id.txt_amount_scheduled_incentives);
        Tv_special = (TextView) findViewById(R.id.txt_amount_special_incentives);

        Tv_week1 = (TextView) findViewById(R.id.txt_label_week1);
        Tv_week2 = (TextView) findViewById(R.id.txt_label_week2);
        Tv_week3 = (TextView) findViewById(R.id.txt_label_week3);
        Tv_week4 = (TextView) findViewById(R.id.txt_label_week4);
        Tv_week5 = (TextView) findViewById(R.id.txt_label_week5);

        Iv_back.setOnClickListener(this);
        Tv_week1.setOnClickListener(this);
        Tv_week2.setOnClickListener(this);
        Tv_week3.setOnClickListener(this);
        Tv_week4.setOnClickListener(this);
        Tv_week5.setOnClickListener(this);

        try {

            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("MMMM yyyy");
            String currentMonthYear = sdf.format(cal.getTime());

            if (currentMonthYear.length() > 0) {
                sDisplayMonth = currentMonthYear;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        if (cd.isConnectingToInternet()) {
            IncentivesStatementRequest(Iconstant.incentives_list_url);
        } else {
            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }


    }


    @Override
    public void onClick(View view) {

        if (view == Iv_back) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (view == Tv_week1) {

            // Making Week1 as Selected
            Tv_week1.setBackgroundColor(ColorTransWhite70);
            Tv_week2.setBackgroundColor(ColorTransWhite40);
            Tv_week3.setBackgroundColor(ColorTransWhite40);
            Tv_week4.setBackgroundColor(ColorTransWhite40);
            Tv_week5.setBackgroundColor(ColorTransWhite40);

            // Setting week1 as selected
            setTextPosition(0);

        } else if (view == Tv_week2) {

            // Making Week2 as Selected
            Tv_week1.setBackgroundColor(ColorTransWhite40);
            Tv_week2.setBackgroundColor(ColorTransWhite70);
            Tv_week3.setBackgroundColor(ColorTransWhite40);
            Tv_week4.setBackgroundColor(ColorTransWhite40);
            Tv_week5.setBackgroundColor(ColorTransWhite40);

            // Setting week1 as selected
            setTextPosition(1);

        } else if (view == Tv_week3) {

            // Making Week3 as Selected
            Tv_week1.setBackgroundColor(ColorTransWhite40);
            Tv_week2.setBackgroundColor(ColorTransWhite40);
            Tv_week3.setBackgroundColor(ColorTransWhite70);
            Tv_week4.setBackgroundColor(ColorTransWhite40);
            Tv_week5.setBackgroundColor(ColorTransWhite40);

            // Setting week3 as selected
            setTextPosition(2);

        } else if (view == Tv_week4) {

            // Making Week4 as Selected
            Tv_week1.setBackgroundColor(ColorTransWhite40);
            Tv_week2.setBackgroundColor(ColorTransWhite40);
            Tv_week3.setBackgroundColor(ColorTransWhite40);
            Tv_week4.setBackgroundColor(ColorTransWhite70);
            Tv_week5.setBackgroundColor(ColorTransWhite40);

            // Setting week4 as selected
            setTextPosition(3);

        } else if (view == Tv_week5) {

            // Making Week5 as Selected
            Tv_week1.setBackgroundColor(ColorTransWhite40);
            Tv_week2.setBackgroundColor(ColorTransWhite40);
            Tv_week3.setBackgroundColor(ColorTransWhite40);
            Tv_week4.setBackgroundColor(ColorTransWhite40);
            Tv_week5.setBackgroundColor(ColorTransWhite70);

            // Setting week1 as selected
            setTextPosition(4);

        }

    }


    @SuppressLint("SetTextI18n")
    private void IncentivesStatementRequest(String Url) {

        dialog = new Dialog(IncentiveStatementNew.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);

        System.out.println("--------------DriverIncentivesStatement Url-------------------" + Url);
        System.out.println("--------------DriverIncentivesStatement jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(IncentiveStatementNew.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {

            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------DriverIncentivesStatement Response-------------------" + response);
                String status = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {
                            JSONObject jsonObject = object.getJSONObject("response");

                            if (jsonObject.length() > 0) {

                                sCurrency = jsonObject.getString("currency");
                                sTotalAmount = sCurrency + " " + jsonObject.getString("total_amount");
                                sTotalPayout = sCurrency + " " + jsonObject.getString("total_payout_amount");
                                sTotalIncentives = jsonObject.getString("total_count");

                                Object incentives_array_check = jsonObject.get("incetives");
                                if (incentives_array_check instanceof JSONArray) {
                                    JSONArray incentivesArray = jsonObject.getJSONArray("incetives");
                                    if (incentivesArray.length() > 0) {
                                        listIncentive = new ArrayList<IncentiveStatementDetail>();

                                        for (int i = 0; i < incentivesArray.length(); i++) {

                                            JSONObject incentiveObject = incentivesArray.getJSONObject(i);

                                            String week = "", week_trip = "", daily_trip = "-", daily_mile = "-", schedule_hours = "-", sos = "-";

                                            if (incentiveObject.has("week")) {
                                                week = sCurrency + " " + incentiveObject.getString("week");
                                            }
                                            if (incentiveObject.has("week_trip")) {
                                                week_trip = sCurrency + " " + incentiveObject.getString("week_trip");
                                            }
                                            if (incentiveObject.has("daily_trip")) {
                                                daily_trip = sCurrency + " " + incentiveObject.getString("daily_trip");
                                            }
                                            if (incentiveObject.has("daily_mile")) {
                                                daily_mile = sCurrency + " " + incentiveObject.getString("daily_mile");
                                            }
                                            if (incentiveObject.has("schedule_hours")) {
                                                schedule_hours = sCurrency + " " + incentiveObject.getString("schedule_hours");
                                            }
                                            if (incentiveObject.has("sos")) {
                                                sos = sCurrency + " " + incentiveObject.getString("sos");
                                            }

                                            listIncentive.add(new IncentiveStatementDetail(week, week_trip, daily_trip, daily_mile, schedule_hours, sos));

                                        }

                                        isDataAvailable = true;
                                    } else {
                                        isDataAvailable = false;
                                    }
                                } else {
                                    isDataAvailable = false;
                                }

                            }

                        } else {
                            String sResponse = object.getString("response");
                            Alert(getResources().getString(R.string.action_error), sResponse);
                        }


                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (status.equalsIgnoreCase("1")) {
                    if (isDataAvailable) {

                        if (sDisplayMonth.length() > 0) {
                            Tv_month.setText(sDisplayMonth);
                        }

                        if (listIncentive.size() > 0) {

                            // Making Week5 visible when there are 5 weeks in a month
                            if (listIncentive.size() > 4) {
                                Tv_week5.setVisibility(View.VISIBLE);
                            } else {
                                Tv_week5.setVisibility(View.GONE);
                            }

                            // Making Week1 as Default Selected
                            Tv_week1.setBackgroundColor(ColorTransWhite70);
                            Tv_week2.setBackgroundColor(ColorTransWhite40);
                            Tv_week3.setBackgroundColor(ColorTransWhite40);
                            Tv_week4.setBackgroundColor(ColorTransWhite40);
                            Tv_week5.setBackgroundColor(ColorTransWhite40);

                            // Setting week1 as selected
                            setTextPosition(0);
                        }
                    }
                }

                if (dialog != null) {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(IncentiveStatementNew.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    // Method to set text amount based on selected week
    private void setTextPosition(int position) {
        if (listIncentive != null && listIncentive.size() > 0) {
            if (listIncentive.size() > position) {
                Tv_dailyMile.setText(listIncentive.get(position).getDaily_mile());
                Tv_dailyTrip.setText(listIncentive.get(position).getDaily_trip());
                Tv_weeklyTarget.setText(listIncentive.get(position).getWeek_trip());
                Tv_scheduled.setText(listIncentive.get(position).getSchedule_hours());
                Tv_special.setText(listIncentive.get(position).getSos());
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {

            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);

            return true;
        }
        return false;
    }


}
