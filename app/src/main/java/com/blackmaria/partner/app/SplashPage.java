package com.blackmaria.partner.app;

import android.os.Build;
import android.os.Bundle;
import android.os.Process;
import androidx.annotation.RequiresApi;

import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;


public class SplashPage extends ActivityHockeyApp {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 5000;
    SessionManager session;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_latest);

        if (getIntent().hasExtra("frommylifecyle")) {
            finishAndRemoveTask();
            System.exit(0);
            Process.killProcess(Process.myPid());
        } else {
            session = new SessionManager(SplashPage.this);
//            Intent intent = new Intent(SplashPage.this, FetchingDataPage.class);
//            startActivity(intent);
//            overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
//            finish();

        }

    }

}
