package com.blackmaria.partner.app;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.partner.Pojo.ComplaintDetail;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Dashboard_constrain;
import com.blackmaria.partner.latestpackageview.widgets.CircularImageView;
import com.blackmaria.partner.latestpackageview.widgets.CustomTextView;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ComplaintPage extends AppCompatActivity implements View.OnClickListener {

    private ImageView Iv_back;
    private TextView Tv_complaint, Tv_closed, Tv_emptyView;

    private SessionManager sessionManager;
    private ConnectionDetector cd;
    private Dialog dialog;
    private ServiceRequest mRequest;

    String sDriverID = "", sTotalComplaints = "", sOpenComplaints = "", sClosedComplaints = "", sComplaintTime = "", sContactNumber = "", vehicle_number = "", driver_name = "", driver_image = "";
    private ArrayList<ComplaintDetail> listComplaint;
    private boolean isDataAvailable = false, isTicketAvailable = false;
    private static final int PERMISSION_REQUEST_CODE = 111;
    private String latestTicket = "",ticketStatus="",StotalTicketCount="",SclosedTicketCount="";
    private LinearLayout comtentLy;
    private CircularImageView driverImageview;
    private TextView drivername;
    private TextView trackIdTxt;
    private TextView subjId;
    private View viewCompilent;
    private TextView datetime, text_empty;
    private RelativeLayout clickview, new_reply;
    private CircularImageView Iv_user;
    private TextView Tv_user_name, Tv_user_loc;
    private RelativeLayout compalints, open, closed;
    boolean fromcomplainpage = false;
    AccessLanguagefromlocaldb db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
        setContentView(R.layout.complaintpagenewmockup);
//        if(getIntent().hasExtra("frompage")){
//            fromcomplainpage = true;
//        }
        initialize();

    }

    //------------------Initializing Variables-----------------
    private void initialize() {

        sessionManager = new SessionManager(ComplaintPage.this);
        cd = new ConnectionDetector(ComplaintPage.this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);


        listComplaint = new ArrayList<ComplaintDetail>();

        Iv_back = (ImageView) findViewById(R.id.img_back);
        Tv_complaint = (TextView) findViewById(R.id.txt_complaint);
        Tv_closed = (TextView) findViewById(R.id.txt_closed);

        TextView compl = (TextView) findViewById(R.id.compl);
        compl.setText(db.getvalue("complaint"));
        TextView closedd = (TextView) findViewById(R.id.closedd);
        closedd.setText(db.getvalue("closed"));
        TextView text_empty = (TextView) findViewById(R.id.text_empty);
        text_empty.setText(db.getvalue("nocomplaints"));


        Tv_emptyView = (TextView) findViewById(R.id.txt_list_empty_view);
//        Lv_complaint.setExpanded(true);
        compalints = (RelativeLayout) findViewById(R.id.compalints);
        closed = (RelativeLayout) findViewById(R.id.closed);

        Iv_user = (CircularImageView) findViewById(R.id.complaint_page_user_imageview);
        Tv_user_loc = (TextView) findViewById(R.id.complaint_page_userlocation_textview);
        Tv_user_name = (TextView) findViewById(R.id.complaint_page_username_textview);


        comtentLy = (LinearLayout) findViewById(R.id.comtent_ly);
        driverImageview = (CircularImageView) findViewById(R.id.driverImageview);
        drivername = (TextView) findViewById(R.id.drivername);
        trackIdTxt = (TextView) findViewById(R.id.track_id_txt);
        subjId = (TextView) findViewById(R.id.subj_id);
        viewCompilent = (View) findViewById(R.id.view_compilent);
        datetime = (TextView) findViewById(R.id.datetime);
        text_empty = (TextView) findViewById(R.id.text_empty);
        clickview = (RelativeLayout) findViewById(R.id.clickview);
        new_reply = (RelativeLayout) findViewById(R.id.new_reply);


        getIntentData();

        if (cd.isConnectingToInternet()) {
            GetComplaintList(Iconstant.complaint_list_url);
        } else {
            Alert(db.getvalue("alert_nointernet"),db.getvalue("alert_nointernet_message"));
        }

        Iv_back.setOnClickListener(this);
        compalints.setOnClickListener(this);
        closed.setOnClickListener(this);
        clickview.setOnClickListener(this);
    }

    private void getIntentData() {

        /*Intent intent = getIntent();
        if (intent != null) {
            rideID = intent.getStringExtra("rideID");
            sFromPage = intent.getStringExtra("fromPage");
        }*/

    }


    //-------------Trip Summary List Request-----------
    @SuppressLint("WrongConstant")
    private void GetComplaintList(String Url) {

        dialog = new Dialog(ComplaintPage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(db.getvalue("action_loading"));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);

        System.out.println("--------------ComplaintPage Url-------------------" + Url);
        System.out.println("--------------ComplaintPage jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(ComplaintPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------ComplaintPage Response-------------------" + response);
                String status = "";
                try {


                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {
                            JSONObject jsonObject = object.getJSONObject("response");

                            sTotalComplaints = jsonObject.getString("total_ticket_Count");
                            sOpenComplaints = jsonObject.getString("open_ticket_Count");
                            sClosedComplaints = jsonObject.getString("closed_ticket_Count");
                            sComplaintTime = jsonObject.getString("today_datetime");
                            if(sTotalComplaints.equalsIgnoreCase("0")){
                                dialog.dismiss();
                            }

                            driver_name = jsonObject.getString("driver_name");
                            driver_image = jsonObject.getString("driver_image");
                            vehicle_number = jsonObject.getString("vehicle_number");
                            isDataAvailable = true;


                            Object check_object2 = jsonObject.get("reply_array");
                            if (check_object2 instanceof JSONObject) {
                                JSONObject replyArray = jsonObject.getJSONObject("reply_array");
                                String driver_name = "", ticket = "", option_name = "", reply_date = "";

                                try {
                                    if (replyArray.has("user_name")) {
                                        driver_name = replyArray.getString("user_name");
                                        ticket = replyArray.getString("ticket");
                                        latestTicket = ticket;
                                        ticketStatus = replyArray.getString("ticket_status");
                                        option_name = replyArray.getString("option_name");
                                        driver_image = replyArray.getString("user_image");
                                        reply_date = replyArray.getString("reply_date");
                                        new_reply.setVisibility(View.VISIBLE);
                                        text_empty.setVisibility(View.GONE);
                                    } else {
                                        new_reply.setVisibility(View.GONE);
                                        text_empty.setVisibility(View.VISIBLE);
                                    }
                                }catch (Exception e){

                                }

                                trackIdTxt.setText("Ticket No :" + ticket);
                                subjId.setText(option_name);
                                datetime.setText(reply_date);
                                Picasso.with(ComplaintPage.this).load(driver_image).placeholder(R.drawable.no_user_img).into(driverImageview);
                                drivername.setText(driver_name);

                            } else {
                                new_reply.setVisibility(View.GONE);
                                text_empty.setVisibility(View.VISIBLE);
                            }

                            Tv_user_name.setText(driver_name);
                            Tv_user_loc.setText(vehicle_number);
                            Picasso.with(ComplaintPage.this)
                                    .load(driver_image)
                                    .placeholder(R.drawable.no_user_img)   // optional
                                    .error(R.drawable.no_user_img)      // optional
                                    .into(Iv_user);

                            StotalTicketCount=sTotalComplaints;
                            SclosedTicketCount=sClosedComplaints;
                            Tv_complaint.setText(StotalTicketCount);
                            Tv_closed.setText(SclosedTicketCount);

                        } else {
                            isDataAvailable = false;
                            String sResponse = object.getString("response");
                            Alert(db.getvalue("action_error"), sResponse);
                        }


                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                if (dialog != null) {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    //-----------Back Key Pressed-----------------------
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
            return true;
        }
        return false;
    }

    //--------------Alert Function------------------
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(ComplaintPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(db.getvalue("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        mDialog.show();
    }

    @Override
    public void onClick(View view) {
        if (view == Iv_back) {
            if(fromcomplainpage){
                Intent intent = new Intent(ComplaintPage.this, Dashboard_constrain.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            }else{
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }

        } else if (view == compalints) {
            Intent intent = new Intent(ComplaintPage.this, ComplaintListPage.class);
            intent.putExtra("typeOfCase", "COMPLAINTS");
            intent.putExtra("cmpCounts", StotalTicketCount);
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.exit);

        } else if (view == closed) {
            Intent intent = new Intent(ComplaintPage.this, ComplaintListPage.class);
            intent.putExtra("typeOfCase", "CLOSED");
            intent.putExtra("cmpCounts", SclosedTicketCount);
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.exit);

        } else if (view == clickview) {
            Intent intent1 = new Intent(ComplaintPage.this, ComplaintPageViewDetailPage.class);
            intent1.putExtra("ticket_id", latestTicket);
            intent1.putExtra("ticket_Status", ticketStatus);
            startActivity(intent1);
            overridePendingTransition(R.anim.enter, R.anim.exit);
            finish();
        }
    }


    private void callUser() {

        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+


                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + sContactNumber));
                startActivity(intent);

        } else {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + sContactNumber));
            startActivity(intent);

        }

    }

    private void requestPermission() {
    }



    private boolean checkReadStatePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + sContactNumber));
                    startActivity(callIntent);
                }
                break;
        }
    }

}

