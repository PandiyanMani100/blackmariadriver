package com.blackmaria.partner.app;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.DowloadPdfActivity;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.latestpackageview.widgets.CircularImageView;
import com.blackmaria.partner.latestpackageview.widgets.CustomTextView;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.blackmaria.partner.latestpackageview.widgets.TextRCBold;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by user144 on 9/18/2017.
 */
public class NewCloudMoneyTransferUrlResponse extends ActivityHockeyApp implements View.OnClickListener ,DowloadPdfActivity.downloadCompleted{
    private TextRCBold fareTv;
    private CustomTextView userrideresponse;
    private CustomTextView userTripnum;
    private TextView userRidenum;
    private CircularImageView ratingPageProfilephoto1;
    private CustomTextView userNameTv, Tv_current_username, Tv_date, Tv_time;
    private CustomTextView userCityTv;
    private CustomTextView userStatusTv;
    private RelativeLayout closeBtn;
    SessionManager session;
    private String current_userName = "";

    private String transNum = "", userNmae = "", userImage = "", userStatus = "", userCity = "", transAmount = "", crosstransfer="",currency = "";
    TextView type_cloude_trnfr;

private RelativeLayout Rl_save;
    LinearLayout pdflayout;
    RelativeLayout savelyt;
    private TextView sava_tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        new_cloud_transfer_complete_responsepage

        setContentView(R.layout.new_cloud_transfer_complete_responsepage);

        initialize();
    }

    private void initialize() {

        session = new SessionManager(NewCloudMoneyTransferUrlResponse.this);

        fareTv = (TextRCBold) findViewById(R.id.fare_tv);
//        userrideresponse = (CustomTextView) findViewById(R.id.userrideresponse);
//        userTripnum = (CustomTextView) findViewById(R.id.user_tripnum);
        userRidenum = (TextView) findViewById(R.id.user_ridenum);
        ratingPageProfilephoto1 = (CircularImageView) findViewById(R.id.rating_page_profilephoto1);
        userNameTv = (CustomTextView) findViewById(R.id.user_name_tv);
        userCityTv = (CustomTextView) findViewById(R.id.user_city_tv);
//        userStatusTv = (CustomTextView) findViewById(R.id.user_status_tv);
        closeBtn = (RelativeLayout) findViewById(R.id.close_btn);
        Tv_current_username = (CustomTextView) findViewById(R.id.current_username);
        Tv_date = (CustomTextView) findViewById(R.id.trans_date);
        Tv_time = (CustomTextView) findViewById(R.id.trans_time);
        type_cloude_trnfr = (CustomTextView) findViewById(R.id.type_cloude_trnfr);
        Rl_save=(RelativeLayout)findViewById(R.id.Rl_save);
        Intent in = getIntent();
        transNum = in.getStringExtra("transaction_number");
        userNmae = in.getStringExtra("user_name");
        userImage = in.getStringExtra("user_image");
        userStatus = in.getStringExtra("status");
        userCity = in.getStringExtra("city");
        transAmount = in.getStringExtra("reloadamount");
        currency = in.getStringExtra("currency");
        if(in.hasExtra("crosstransfer")) {
            crosstransfer = in.getStringExtra("crosstransfer");
        }
        HashMap<String, String> user_name = session.getUserDetails();
        current_userName = user_name.get(SessionManager.KEY_DRIVER_NAME);

        pdflayout=(LinearLayout) findViewById(R.id.pdflayout);
        savelyt= (RelativeLayout) findViewById(R.id.savelyt);
        sava_tv= (TextView) findViewById(R.id.sava_tv);


        if(crosstransfer.equalsIgnoreCase("1")){
            type_cloude_trnfr.setVisibility(View.VISIBLE);
        }


        userCityTv.setText(userCity);
//        userStatusTv.setText(userStatus);
        userNameTv.setText(userNmae);
        userRidenum.setText("TRANSACTION NO" + " " + ":" + " "+ transNum);
        fareTv.setText(currency + " " + transAmount);
        Tv_current_username.setText(current_userName);
        Tv_date.setText(GetCurrentDate());
        Tv_time.setText(GetCurrentTime());
        Picasso.with(NewCloudMoneyTransferUrlResponse.this).load(String.valueOf(userImage)).into(ratingPageProfilephoto1);




        Rl_save.setOnClickListener(this);
        closeBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == closeBtn) {

            // Handle clicks for closeBtn

            Intent intent = new Intent();
            intent.setAction("com.package.ACTION_WALLET_RELOAD_SUCCESS");
            sendBroadcast(intent);


            Intent intent1 = new Intent(NewCloudMoneyTransferUrlResponse.this, WalletMoneyPage1.class);
            intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent1);
            overridePendingTransition(R.anim.enter, R.anim.exit);
            finish();
            // Handle clicks for closeBtn
        }else if(v==Rl_save){
            savelyt.setVisibility(View.GONE);
            new DowloadPdfActivity(NewCloudMoneyTransferUrlResponse.this,pdflayout,"moneytransfer",NewCloudMoneyTransferUrlResponse.this);

        }
    }


    private String GetCurrentDate() {

        Calendar c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("dd MMMM yyyy");
        String formattedDate = df.format(c.getTime());

        return formattedDate;
    }


    private String GetCurrentTime() {

        Calendar c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss aaa");
        String formattedtime = df.format(c.getTime());

        return formattedtime;
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(NewCloudMoneyTransferUrlResponse.this, WalletMoneyPage1.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.enter, R.anim.exit);
        finish();
        super.onBackPressed();
    }

    @Override
    public void OnDownloadComplete(String fileName) {
        savelyt.setVisibility(View.VISIBLE);
        sava_tv.setText("Saved");

        ViewPdfAlert(""," Successfully Downloaded",fileName);
    }
    //--------------Alert Method-----------
    private void ViewPdfAlert( String title, String alert,final String fileName) {

        final PkDialog mDialog = new PkDialog(NewCloudMoneyTransferUrlResponse.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton("OPEN", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPDF(fileName);
                mDialog.dismiss();
            }
        });
        mDialog.setNegativeButton(getResources().getString(R.string.cancel_lable), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }
    public void viewPDF(String pdfFileName) {
        File pdfFile = new File(Environment.getExternalStorageDirectory() + "/Signature/" + pdfFileName);  // -> filename = maven.pdf
        Uri uri_path = Uri.fromFile(pdfFile);
        Uri uri = uri_path;

        try {
            File file = null;
            String path = uri.toString();// "/mnt/sdcard/FileName.mp3"
            try {
                file = new File(new URI(path));
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            final Intent intent = new Intent(Intent.ACTION_VIEW)//
                    .setDataAndType(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ?
                                    androidx.core.content.FileProvider.getUriForFile(NewCloudMoneyTransferUrlResponse.this, getPackageName() + ".provider", file) : Uri.fromFile(file),
                            "application/pdf").addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            try {
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(NewCloudMoneyTransferUrlResponse.this, "No Application available to view PDF", Toast.LENGTH_SHORT).show();
            }

        } catch (ActivityNotFoundException e) {
            Toast.makeText(NewCloudMoneyTransferUrlResponse.this, "Cannot read file error. ", Toast.LENGTH_SHORT).show();
        }

    }
}
