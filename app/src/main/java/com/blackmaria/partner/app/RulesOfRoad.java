package com.blackmaria.partner.app;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.R;

/**
 * Created by user127 on 31-08-2017.
 */

public class RulesOfRoad extends ActivityHockeyApp {

    private ImageView Iv_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rules_of_the_road);

        Iv_back = (ImageView) findViewById(R.id.img_home);

        Iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

    }
}
