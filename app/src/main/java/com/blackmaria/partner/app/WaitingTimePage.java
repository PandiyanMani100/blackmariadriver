package com.blackmaria.partner.app;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;


public class WaitingTimePage extends AppCompatActivity implements ApIServices.completelisner {

    private String sDriverID = "", sRideId = "", sUserName = "", sUserImage = "", sUserRating = "", sLocation = "", sRiderType = "", sCompletedRides = "",
            sCancelledRides = "", sMemberSince = "", sUserGender = "", sPaymentIcon = "", sPickupLocation = "";
    long min = 0, sec = 0;
    boolean isContinuing = false, isDataAvailable = false;

    private TextView Tv_waitingTime, Tv_arriveRiderName, Tv_arriveRideId, Tv_arriveFrom;
    private Button Btn_close;
    private ImageView Iv_light, Iv_arriveRiderProfile, Iv_arrivePaymentOption;
    private RelativeLayout RL_arrive, RL_profileArrive;

    private SessionManager sessionManager;
    private ConnectionDetector cd;
    private Handler customHandler = new Handler();
    private Calendar calendar;
    DecimalFormat precision = new DecimalFormat("00");
    private Dialog dialog;
    private ServiceRequest mRequest;
    private ProgressBar apiload;
    private int count  =0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.waiting_page);

        initialize();

        Btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertCloseTimer("Waiting time", getResources().getString(R.string.waiting_label_sure_to_close));

            }
        });

        RL_arrive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                showRiderProfile();
            }
        });

    }

    private void initialize() {

        sessionManager = new SessionManager(WaitingTimePage.this);
        cd = new ConnectionDetector(WaitingTimePage.this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);

        Tv_waitingTime = (TextView) findViewById(R.id.txt_waiting_time);
        apiload = findViewById(R.id.apiload);
        Btn_close = (Button) findViewById(R.id.btn_close);
        RL_arrive = (RelativeLayout) findViewById(R.id.RL_details_arrive);
        RL_profileArrive = (RelativeLayout) findViewById(R.id.Rl_rider_profile_arrive);
        Tv_arriveRiderName = (TextView) findViewById(R.id.txt_rider_name_arrive);
        Tv_arriveRideId = (TextView) findViewById(R.id.txt_ride_id_arrive);
        Tv_arriveFrom = (TextView) findViewById(R.id.txt_address_from_arrive);
        Iv_arriveRiderProfile = (ImageView) findViewById(R.id.img_rider_profile_arrive);
        Iv_arrivePaymentOption = (ImageView) findViewById(R.id.img_payment_option_arrive);
        Iv_light = (ImageView) findViewById(R.id.img_light);

        startBlinkingAnimation();

        getIntentData();

        if (cd.isConnectingToInternet()) {
            GetRiderDetails(Iconstant.get_rider_details_url);
        } else {
            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }

    }

    private void getIntentData() {

        Intent intent = getIntent();
        if (intent != null) {
            sRideId = intent.getStringExtra("rideID");
            isContinuing = getIntent().getBooleanExtra("isContinue", false);
        }

    }

    private void startBlinkingAnimation() {
        Animation startAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blinking_animation);
        Iv_light.startAnimation(startAnimation);
    }

    private Runnable updateTimerThread = new Runnable() {

        @Override
        public void run() {

            HashMap<String, String> waitedTimeMap = sessionManager.getWaitedTime();
            String waitedTime = waitedTimeMap.get(SessionManager.KEY_CALENDAR_TIME);
            if (waitedTime.isEmpty()) {
                String updatedTime = precision.format(min) + ":" + precision.format(sec);
                calendar = Calendar.getInstance();
                sessionManager.setWaitedTime(min, sec, updatedTime, String.valueOf(calendar.getTime()));
            }
            setSecodds(waitedTime);
            String updatedTime = precision.format(min) + ":" + precision.format(sec);
            Tv_waitingTime.setText(updatedTime);
            customHandler.postDelayed(updateTimerThread, 1000);
        }
    };


    //-------------Stop Waiting Time Request-----------
    @SuppressLint("WrongConstant")
    private void StopWaitingTimeRequest(String Url) {
        count =0;
        apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(WaitingTimePage.this, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        jsonParams.put("ride_id", sRideId);

        ApIServices apIServices = new ApIServices(WaitingTimePage.this, WaitingTimePage.this);
        apIServices.dooperation(Url, jsonParams);
    }


    @SuppressWarnings("WrongConstant")
    private void GetRiderDetails(String Url) {

        count =1;
        apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(WaitingTimePage.this, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        jsonParams.put("ride_id", sRideId);

        ApIServices apIServices = new ApIServices(WaitingTimePage.this, WaitingTimePage.this);
        apIServices.dooperation(Url, jsonParams);


    }

    private void setSecodds(String waitedTime) {
        if (waitedTime == null || waitedTime.isEmpty())
            return;
        Date waitingTimeDate = new Date(waitedTime);
        Date currentDate = Calendar.getInstance().getTime();
        long diff = currentDate.getTime() - waitingTimeDate.getTime();

        long seconds = diff / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long days = hours / 24;

        sec = seconds % 60;
        min = minutes;

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (customHandler != null) {
            customHandler.removeCallbacks(updateTimerThread);
            customHandler = null;
        }

    }

    private void showRiderProfile() {
        final Dialog dialog = new Dialog(WaitingTimePage.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.alert_trip_rider_details);

        TextView Tv_riderName = (TextView) dialog.findViewById(R.id.txt_rider_name);
        RatingBar Rb_rider = (RatingBar) dialog.findViewById(R.id.rb_rider);
        ImageView Cv_riderProfile = (ImageView) dialog.findViewById(R.id.img_profile_alert_rider);
        TextView Tv_riderType = (TextView) dialog.findViewById(R.id.txt_rider_type);
        TextView Tv_member = (TextView) dialog.findViewById(R.id.txt_member);
        TextView Tv_dateJoin = (TextView) dialog.findViewById(R.id.txt_date_join);
        TextView Tv_gender = (TextView) dialog.findViewById(R.id.txt_gender);
        TextView Tv_tripCompleted = (TextView) dialog.findViewById(R.id.txt_trip_completed);
        TextView Tv_tripCancelled = (TextView) dialog.findViewById(R.id.txt_trip_cancelled);
        Button btn_close = (Button) dialog.findViewById(R.id.btn_close);

        Tv_riderName.setText(sUserName);
        Rb_rider.setRating(Float.parseFloat(sUserRating));
        Tv_member.setText(sLocation);
        Tv_riderType.setText(sRiderType);
        Tv_tripCompleted.setText(sCompletedRides);
        Tv_tripCancelled.setText(sCancelledRides);
        Tv_dateJoin.setText(sMemberSince);
        Tv_gender.setText(sUserGender);

        if (sUserImage.length() > 0) {
            Picasso.with(WaitingTimePage.this).load(sUserImage).error(R.drawable.no_user_img).placeholder(R.drawable.no_user_img).memoryPolicy(MemoryPolicy.NO_CACHE).into(Cv_riderProfile);
        }

        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });

        dialog.show();


    }

    //--------------Alert Function------------------
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(WaitingTimePage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        mDialog.show();
    }


    private void AlertSuccess(String title, String alert) {
        final PkDialog mDialog = new PkDialog(WaitingTimePage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
                mDialog.dismiss();
            }
        });

        mDialog.show();
    }

    private void AlertCloseTimer(String title, String alert) {
        final PkDialog mDialog = new PkDialog(WaitingTimePage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                if (cd.isConnectingToInternet()) {
                    StopWaitingTimeRequest(Iconstant.waiting_close_url);

                } else {
                    Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                }
            }
        });

        mDialog.setNegativeButton(getResources().getString(R.string.action_cancel), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });

        mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                Btn_close.setEnabled(true);
            }
        });

        mDialog.show();
        Btn_close.setEnabled(false);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            //Do nothing
            return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void sucessresponse(String response) {
        apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();

        if(count ==1){
            String status="";
            try {
                JSONObject object = new JSONObject(response);
                if (object.length() > 0) {

                    status = object.getString("status");

                    if (status.equalsIgnoreCase("1")) {

                        JSONObject jobjResponse = object.getJSONObject("response");

                        sUserName = jobjResponse.getString("user_name");
                        sUserImage = jobjResponse.getString("user_image");
                        sUserGender = jobjResponse.getString("user_gender");
                        sUserRating = jobjResponse.getString("user_review");
                        sMemberSince = jobjResponse.getString("member_since");
                        sCompletedRides = jobjResponse.getString("completed_rides");
                        sCancelledRides = jobjResponse.getString("cancelled_rides");
                        sLocation = jobjResponse.getString("location");
                        sPaymentIcon = jobjResponse.getString("payment_icon");
                        sPickupLocation = jobjResponse.getString("pickup_location");

                        isDataAvailable = true;

                    } else {
                        isDataAvailable = false;
                    }


                } else {
                    isDataAvailable = false;
                }

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            if (status.equalsIgnoreCase("1") && isDataAvailable) {

                RL_arrive.setVisibility(View.VISIBLE);

                if (sPaymentIcon.length() > 0) {
                    Picasso.with(WaitingTimePage.this).load(sPaymentIcon).error(R.drawable.img_cash).placeholder(R.drawable.img_cash).into(Iv_arrivePaymentOption);
                }
                Tv_arriveRiderName.setText(sUserName);
                Tv_arriveRideId.setText(getResources().getString(R.string.trip_label_crn_no) + sRideId);
                Tv_arriveFrom.setText(getResources().getString(R.string.trip_label_from) + sPickupLocation.replaceAll("\n", " "));
                if (sUserImage.length() > 0) {
                    Picasso.with(WaitingTimePage.this).load(sUserImage).error(R.drawable.no_user_img).error(R.drawable.no_user_img).fit().into(Iv_arriveRiderProfile);
                }

            } else {
                RL_arrive.setVisibility(View.GONE);
            }

            if (isContinuing) {
                HashMap<String, String> waitedTimeMap = sessionManager.getWaitedTime();
                String waitedTime = waitedTimeMap.get(SessionManager.KEY_CALENDAR_TIME);

                if (waitedTime.isEmpty()) {
                    String updatedTime = precision.format(min) + ":" + precision.format(sec);
                    calendar = Calendar.getInstance();
                    sessionManager.setWaitedTime(min, sec, updatedTime, String.valueOf(calendar.getTime()));
                } else {

                }
                if (!waitedTime.equalsIgnoreCase("")) {
                    setSecodds(waitedTime);

                } else {

                }


            }
            customHandler.postDelayed(updateTimerThread, 1000);
        }else{
            try {
                JSONObject object = new JSONObject(response);
                if (object.length() > 0) {
                    String status = "";
                    status = object.getString("status");

                    if (status.equalsIgnoreCase("1")) {
                        String sCompWaitingTime = object.getString("response");
                        sCompWaitingTime = object.getString("comp_waiting_time");

                        if (customHandler != null) {
                            customHandler.removeCallbacks(updateTimerThread);
                            customHandler = null;
                        }
                        sessionManager.setWaitedTime(0, 0, "", "");
                        String sResponse = object.getString("response");
                        AlertSuccess(getResources().getString(R.string.action_success), "Waiting time closed successfully");
                    } else {
                        String sResponse = object.getString("response");
                        Alert(getResources().getString(R.string.action_error), sResponse);
                    }


                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

    }

    @Override
    public void errorreponse() {
        apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
    }

    @Override
    public void jsonexception() {
        apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        AppUtils.toastprint(WaitingTimePage.this, "Json Exception");
    }
}