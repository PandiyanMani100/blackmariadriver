package com.blackmaria.partner.app;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.mylibrary.volley.ServiceRequest;

import java.util.HashMap;

/**
 * Created by user127 on 17-10-2017.
 */

public class RegistrationSuccess extends ActivityHockeyApp implements View.OnClickListener {

    private SessionManager sessionManager;
    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    private Dialog dialog;

    private String sDriverID = "";
    private TextView txt_content_assistance, Loginnow;
    private RelativeLayout RL_exit, RL_readMe;
    public String sContactNumber = "";
    public ImageView imageView_call;
    final int PERMISSION_REQUEST_CODE = 111;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_success);

        initialize();

    }

    @SuppressLint("WrongConstant")
    private void initialize() {

        sessionManager = new SessionManager(RegistrationSuccess.this);
        sContactNumber = sessionManager.getContactNumber();
        cd = new ConnectionDetector(RegistrationSuccess.this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);

        txt_content_assistance = (TextView) findViewById(R.id.txt_content_assistance);
        //txt_content_assistance.setText(Html.fromHtml(getResources().getString(R.string.content_registration_need_assistance)));
        String sourceString = "<b>" + "Need assistance ? get in touch with us" + "</b> " + " <br/> " + "Tap the button to contact us or" + "<br/>" + "send email to : drive@blackmari.co";
        txt_content_assistance.setText(Html.fromHtml(sourceString));

        RL_exit = (RelativeLayout) findViewById(R.id.RL_exit);
        RL_readMe = (RelativeLayout) findViewById(R.id.RL_read_me);
        imageView_call = (ImageView)findViewById(R.id.img_call);
        imageView_call.setOnClickListener(this);
        Loginnow = (TextView) findViewById(R.id.btn_login_now);
        RL_exit.setOnClickListener(this);
        RL_readMe.setOnClickListener(this);
        Loginnow.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {

        if (view == RL_exit) {
            Intent intent = new Intent(RegistrationSuccess.this, LoginPage.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | /*Intent.FLAG_ACTIVITY_NO_HISTORY |*/ Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.exit);
            finish();
        } else if (view == RL_readMe) {
            showBasicProcedures();
        } else if (view == imageView_call) {
            callCustomerCare();
        } else if (view == Loginnow) {
            Intent intent = new Intent(RegistrationSuccess.this, LoginPage.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | /*Intent.FLAG_ACTIVITY_NO_HISTORY |*/ Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.exit);
            finish();
        }


    }

    private void callCustomerCare() {

        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+

            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + sessionManager.getCustomerServiceNo()));
            startActivity(intent);

        } else {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + sessionManager.getCustomerServiceNo()));
            startActivity(intent);
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void requestPermission() {
        requestPermissions(new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
    }

    private boolean checkCallPhonePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkReadStatePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + sContactNumber));
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    startActivity(callIntent);
                }
                break;
        }
    }


    private void showBasicProcedures() {

        final Dialog basicProceduresDialog = new Dialog(RegistrationSuccess.this, R.style.SlideUpDialog);
        basicProceduresDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        basicProceduresDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        basicProceduresDialog.setCancelable(true);
        basicProceduresDialog.setContentView(R.layout.alert_registration_read_me);


        //--------Adjusting Dialog width and height-----
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 85% of the screen
        int screenHeight = (int) (metrics.heightPixels * 0.85);//fill only 85% of the screen
        basicProceduresDialog.getWindow().setLayout(screenWidth, screenHeight);


        final ImageView Iv_close = (ImageView) basicProceduresDialog.findViewById(R.id.img_close);


        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                basicProceduresDialog.dismiss();
            }
        });

        basicProceduresDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                RL_readMe.setClickable(true);
            }
        });

        basicProceduresDialog.show();
        RL_readMe.setClickable(false);

    }


}