package com.blackmaria.partner.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.blackmaria.partner.Pojo.relaodpage;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.Xmpp.MyXMPP;
import com.blackmaria.partner.latestpackageview.Database.GEODBHelper;

import org.greenrobot.eventbus.EventBus;
import org.jivesoftware.smack.packet.Message;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLDecoder;
import java.util.HashMap;

public class Requestpass_Activity extends AppCompatActivity {
    private static Context context;
    private static GEODBHelper myDBHelper;
    private static SessionManager sessionManager;
    public static Activity activity;

    String value;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = Requestpass_Activity.this;
        setContentView(R.layout.activity_requestpasslayout);
        context = Requestpass_Activity.this;
        myDBHelper = new GEODBHelper(context);
        sessionManager = new SessionManager(context);

//        if(MyXMPP.messageglobal != null){
//            onHandleChatMessage(MyXMPP.messageglobal);
//        }

    }


    public  void onHandleChatMessage(Message message) {
        try {
            String data = URLDecoder.decode(message.getBody(), "UTF-8");
            System.out.println("-------------Xmpp response---------------------" + data);
            JSONObject messageObject = new JSONObject(data);
            String action = (String) messageObject.get(Iconstant.ACTION_LABEL);
            if (Iconstant.ACTION_TAG_RIDE_REQUEST.equalsIgnoreCase(action)) {
                rideRequest(data);
            } else if (Iconstant.ACTION_TAG_RIDE_CANCELLED.equalsIgnoreCase(action)) {
                rideCancelled(messageObject);
            } else if (Iconstant.ACTION_TAG_RECEIVE_CASH.equalsIgnoreCase(action)) {
                receiveCash(messageObject);
            } else if (Iconstant.ACTION_TAG_PAYMENT_PAID.equalsIgnoreCase(action)) {
                paymentPaid(messageObject);
            } else if (Iconstant.ACTION_TAG_NEW_TRIP.equalsIgnoreCase(action)) {
                newTipAlert(messageObject);
            } else if (Iconstant.pushNotification_Ads.equalsIgnoreCase(action)) {
                display_Ads(messageObject);
            } else if (Iconstant.ACTION_TAG_COMPLEMENTARY.equalsIgnoreCase(action)) {
                complementary(messageObject);
            } else if (Iconstant.ACTION_TAG_REFERRAL_CREDIT.equalsIgnoreCase(action)) {
                display_AdsReferralCredit(messageObject);
            } else if (Iconstant.ACTION_WALLET_SUCCESS.equalsIgnoreCase(action)) {
                walletSuccess(messageObject);
            } else if (Iconstant.ACTION_BANK_SUCCESS.equalsIgnoreCase(action)) {
                bankSuccess(messageObject);
            } else if (action.equalsIgnoreCase(Iconstant.ACTION_TAG_WEEK_TRIP) || action.equalsIgnoreCase(Iconstant.ACTION_TAG_DAILY_TRIP)
                    || action.equalsIgnoreCase(Iconstant.ACTION_TAG_DAILY_MILE) || action.equalsIgnoreCase(Iconstant.ACTION_TAG_SCHEDULE_INCENTIVE)) {
                incentivesAchieved(messageObject);
            } else if (Iconstant.ACTION_TAG_WALLET_CREDIT.equalsIgnoreCase(action)) {
                walletCredit(messageObject);
            } else if (Iconstant.userEndTrip.equalsIgnoreCase(action)) {
                RideCompletedByUser(messageObject);
            } else if (Iconstant.userRidePaymentTrip.equalsIgnoreCase(action)) {
                RideCompletedByUserPaymentCompleted(messageObject);
            }

        } catch (Exception e) {
            System.out.print(".................." + e.getMessage());
        }
    }


    private static void RideCompletedByUser(JSONObject messageObject) throws Exception {
        Intent intent = new Intent(context, PushNotificationAlert.class);
        intent.putExtra("RideId", messageObject.getString("key1"));
        intent.putExtra("driverId", messageObject.getString("key2"));
        intent.putExtra("Action", messageObject.getString("action"));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
        context.startActivity(intent);
    }

    private static void RideCompletedByUserPaymentCompleted(JSONObject messageObject) throws Exception {
        Intent intent = new Intent(context, PushNotificationAlert.class);
        intent.putExtra("RideId", messageObject.getString("key1"));
        intent.putExtra("driverId", messageObject.getString("key2"));
        intent.putExtra("Action", messageObject.getString("action"));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
    /*private void complementary(JSONObject messageObject) throws Exception {
        Intent intent = new Intent(context, PushNotificationAlert.class);
        intent.putExtra("MessagePage", messageObject.getString("message"));
        intent.putExtra("Action", messageObject.getString("action"));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }*/

    private void complementary(JSONObject messageObject) throws Exception {
        sessionManager.setWaitingtime_rideid(messageObject.getString("key1"));
        Intent intent = new Intent();
        intent.setAction("com.app.waiting.stop.handler");
        sendBroadcast(intent);
        finish();
    }


    private  void rideRequest(String message) {
        HashMap<String, Integer> user = sessionManager.getRequestCount();
        int req_count = user.get(SessionManager.KEY_COUNT);
        if(req_count < 1) {
            Intent intent = new Intent(Requestpass_Activity.this, TimerPage.class);
            intent.putExtra("EXTRA", message);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    private static void rideCancelled(JSONObject messageObject) throws Exception {
        myDBHelper.insertDriverStatus("service_stop");
        sessionManager.setUpdateLocationServiceState("");
        Intent intent = new Intent(context, PushNotificationAlert.class);
        intent.putExtra("MessagePage", messageObject.getString("message"));
        intent.putExtra("Action", messageObject.getString("action"));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
        context.startActivity(intent);
    }

    private static void receiveCash(JSONObject messageObject) throws Exception {
        Intent intent = new Intent(context, PushNotificationAlert.class);
        intent.putExtra("MessagePage", messageObject.getString("message"));
        intent.putExtra("Action", messageObject.getString("action"));
        intent.putExtra("amount", messageObject.getString("key3"));
        intent.putExtra("RideId", messageObject.getString("key1"));
        intent.putExtra("Currencycode", messageObject.getString("key4"));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
        context.startActivity(intent);
    }

    private static void incentivesAchieved(JSONObject messageObject) throws Exception {
        Intent intent = new Intent(context, PushNotificationAlert.class);
        intent.putExtra("MessagePage", messageObject.getString("message"));
        intent.putExtra("Action", messageObject.getString("action"));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
        context.startActivity(intent);
    }

    private static void paymentPaid(JSONObject messageObject) throws Exception {

        Intent intentRefresh = new Intent();
        intentRefresh.setAction("com.app.RefreshFarePage");
        context.sendBroadcast(intentRefresh);

        Intent intent = new Intent(context, PushNotificationAlert.class);
        intent.putExtra("MessagePage", messageObject.getString("message"));
        intent.putExtra("Action", messageObject.getString("action"));
        intent.putExtra("RideId", messageObject.getString("key1"));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
        context.startActivity(intent);
    }

    private static void newTipAlert(JSONObject messageObject) throws Exception {
        Intent intent = new Intent(context, GoOnlinePage.class);
        intent.putExtra("NewTrip", messageObject.getString("message"));
        intent.putExtra("MessagePage", messageObject.getString("message"));
        intent.putExtra("Action", messageObject.getString("action"));
        intent.putExtra("Username", messageObject.getString("key1"));
        intent.putExtra("Mobilenumber", messageObject.getString("key3"));
        intent.putExtra("UserImage", messageObject.getString("key4"));
        intent.putExtra("RideId", messageObject.getString("key6"));
        intent.putExtra("UserPickuplocation", messageObject.getString("key7"));
        intent.putExtra("UserPickupTime", messageObject.getString("key10"));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
        context.startActivity(intent);

    }

    private static void display_Ads(JSONObject messageObject) throws Exception {
        Intent i1 = new Intent(context, AdsPage.class);
        i1.putExtra("AdsTitle", messageObject.getString(Iconstant.Ads_title));
        i1.putExtra("AdsMessage", messageObject.getString(Iconstant.Ads_Message));
        if (messageObject.has(Iconstant.Ads_image)) {
            i1.putExtra("AdsBanner", messageObject.getString(Iconstant.Ads_image));
        }
        i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
        context.startActivity(i1);
        relaodpage obj  = new relaodpage();
        obj.setIstrue(true);
        EventBus.getDefault().post(obj);
    }

    private static void display_AdsReferralCredit(JSONObject messageObject) throws Exception {
        Intent i1 = new Intent(context, AdsPage.class);
        i1.putExtra("AdsTitle", context.getResources().getString(R.string.label_referral_credit));
        i1.putExtra("AdsMessage", messageObject.getString(Iconstant.Referral_Message));
        if (messageObject.has(Iconstant.Ads_image)) {
            i1.putExtra("AdsBanner", messageObject.getString(Iconstant.Ads_image));
        }
        i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
        context.startActivity(i1);
    }


    private static void walletSuccess(JSONObject messageObject) throws Exception {
        Intent intent = new Intent(context, PushNotificationAlert.class);
        intent.putExtra("Action", messageObject.getString("action"));
        intent.putExtra("MessagePage", messageObject.getString("message"));

        intent.putExtra("user_id", messageObject.getString("key1"));
        intent.putExtra("Amount", messageObject.getString("key4"));
        intent.putExtra("Currencycode", messageObject.getString("key5"));
        intent.putExtra("payment_via", messageObject.getString("key2"));
        intent.putExtra("from_number", messageObject.getString("key3"));

        intent.putExtra("from", messageObject.getString("key6"));
        intent.putExtra("date", messageObject.getString("key7"));
        intent.putExtra("time", messageObject.getString("key8"));
        intent.putExtra("trans_id", messageObject.getString("key9"));


        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
        context.startActivity(intent);
    }


    private static void walletCredit(JSONObject messageObject) throws JSONException {
        Intent i1 = new Intent(context, PushNotificationAlert.class);
        i1.putExtra("Action", messageObject.getString("action"));
        i1.putExtra("MessagePage", messageObject.getString("message"));


        i1.putExtra("user_id", messageObject.getString("key1"));
        i1.putExtra("Amount", messageObject.getString("key2"));
        i1.putExtra("Currencycode", messageObject.getString("key3"));
        i1.putExtra("paymenttype", messageObject.getString("key4"));
        i1.putExtra("payment_via", messageObject.getString("key5"));
        i1.putExtra("from_number", messageObject.getString("key6"));
        i1.putExtra("from", messageObject.getString("key7"));
        i1.putExtra("date", messageObject.getString("key8"));
        i1.putExtra("time", messageObject.getString("key9"));
        i1.putExtra("trans_id", messageObject.getString("key10"));

        i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
        context.startActivity(i1);
    }

    private static void bankSuccess(JSONObject messageObject) throws Exception {
        Intent intent = new Intent(context, PushNotificationAlert.class);
        intent.putExtra("Action", messageObject.getString("action"));
        intent.putExtra("MessagePage", messageObject.getString("message"));


        intent.putExtra("user_id", messageObject.getString("key1"));
        intent.putExtra("Amount", messageObject.getString("key5"));
        intent.putExtra("Currencycode", messageObject.getString("key6"));
        intent.putExtra("paymenttype", messageObject.getString("key2"));
        intent.putExtra("payment_via", messageObject.getString("key3"));
        intent.putExtra("from_number", messageObject.getString("key4"));

        intent.putExtra("from", messageObject.getString("key7"));
        intent.putExtra("date", messageObject.getString("key8"));
        intent.putExtra("time", messageObject.getString("key9"));
        intent.putExtra("trans_id", messageObject.getString("key10"));


        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
        context.startActivity(intent);
    }

}
