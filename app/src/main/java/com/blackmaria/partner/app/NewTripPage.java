package com.blackmaria.partner.app;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.PowerManager;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.latestpackageview.widgets.RoundedImageView;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Tracking.TrackArrive;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.blackmaria.partner.latestpackageview.Database.GEODBHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class NewTripPage extends AppCompatActivity implements ApIServices.completelisner {

    private RoundedImageView newTrip_userimg;
    private ImageView newTrip_call_userimg;
    private TextView Tv_newTrip_username, Tv_message, Tv_newTrip_user_address, Tv_newTrip_time;
    private String sUserName = "", sUser_Mobileno = "", SUser_Message = "", SUser_image = "", SUser_pickup_location = "", sUser_Pickup_time = "", sRideId = "";
    private RelativeLayout Rl_accept;
    final int PERMISSION_REQUEST_CODE = 111;
    private AppUtils appUtils;
    private ServiceRequest mRequest;
    private ConnectionDetector cd;
    private SessionManager sessionManager;
    private String sDriveID = "";
    public MediaPlayer mediaPlayer;
    private GEODBHelper myDBHelper;
    private ProgressBar apiload;
    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newtrip_alert);
        initialize();

        Rl_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewTripPage.this, TrackArrive.class);
                intent.putExtra("rideid", sRideId);
                startActivity(intent);
                finish();
            }
        });

        newTrip_call_userimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sUser_Mobileno != null) {
                    String sPhoneMaskingStatus = sessionManager.getPhoneMaskingStatus();
                    if (sPhoneMaskingStatus.equalsIgnoreCase("Yes")) {
                        postRequest_PhoneMasking(Iconstant.phoneMasking_url);
                    } else {
                        appUtils.call(NewTripPage.this, sUser_Mobileno);
                    }
                } else {
                    alert(NewTripPage.this.getResources().getString(R.string.timer_label_alert_sorry), NewTripPage.this.getResources().getString(R.string.new_trip_label_call_alert));
                }

            }
        });

    }


    private void initialize() {
        appUtils = AppUtils.getInstance(this);
        sessionManager = SessionManager.getInstance(this);
        myDBHelper = GEODBHelper.getInstance(this);
        //screen locak on
        @SuppressLint("InvalidWakeLockTag") PowerManager.WakeLock screenLock = ((PowerManager) getSystemService(POWER_SERVICE)).newWakeLock(
                PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "TAG");
        screenLock.acquire();

        KeyguardManager keyguardManager = (KeyguardManager) getSystemService(Activity.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock lock = keyguardManager.newKeyguardLock(KEYGUARD_SERVICE);
        //later
        screenLock.release();
        lock.disableKeyguard();

        int MAX_VOLUME = 100;
        final float volume = (float) (1 - (Math.log(MAX_VOLUME - 80) / Math.log(MAX_VOLUME)));
        mediaPlayer = MediaPlayer.create(this, R.raw.blacktone);

        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 20, 0);

        mediaPlayer.setVolume(volume, volume);

        if (mediaPlayer != null) {
            if (!mediaPlayer.isPlaying()) {
                mediaPlayer.start();
                mediaPlayer.setLooping(true);
            }
        }

        newTrip_userimg = (RoundedImageView) findViewById(R.id.newTrip_userImage);
        newTrip_call_userimg = (ImageView) findViewById(R.id.newTrip_call_Image);
        Tv_newTrip_username = (TextView) findViewById(R.id.newTrip_userName);
        Tv_newTrip_user_address = (TextView) findViewById(R.id.newTrip_locationAddress);
        Tv_newTrip_time = (TextView) findViewById(R.id.newTrip_pickUpTime);
        Tv_message = (TextView) findViewById(R.id.newTrip_header_message);
        Rl_accept = (RelativeLayout) findViewById(R.id.newTrip_accept_layout);

        Intent i = getIntent();
        SUser_Message = i.getStringExtra("MessagePage");
        sUserName = i.getStringExtra("Username");
        sUser_Mobileno = i.getStringExtra("Mobilenumber");
        SUser_image = i.getStringExtra("UserImage");
        SUser_pickup_location = i.getStringExtra("UserPickuplocation");
        sUser_Pickup_time = i.getStringExtra("UserPickupTime");
        sRideId = i.getStringExtra("RideId");

        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriveID = user.get(SessionManager.KEY_DRIVERID);

        Tv_message.setText(SUser_Message.toUpperCase());
        Tv_newTrip_username.setText(sUserName);
        Tv_newTrip_user_address.setText(SUser_pickup_location);
        Tv_newTrip_time.setText(sUser_Pickup_time);

        if (SUser_image.length() > 0) {
            AppUtils.setImageView(NewTripPage.this, String.valueOf(SUser_image), newTrip_userimg);
        }
    }

    //--------------Alert Method-----------
    private void alert(String title, String message) {
        final PkDialog mDialog = new PkDialog(NewTripPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(message);
        mDialog.setPositiveButton(getResources().getString(R.string.alert_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    //-----------------------PhoneMasking Post Request-----------------
    private void postRequest_PhoneMasking(String Url) {
        apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(NewTripPage.this, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("ride_id", sRideId);
        jsonParams.put("user_type", "driver");
        ApIServices apIServices = new ApIServices(NewTripPage.this, NewTripPage.this);
        apIServices.dooperation(Iconstant.continuePendingTrip_Url, jsonParams);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mediaPlayer != null) {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
            }
        }
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void sucessresponse(String response) {
        try {
            apiload.setVisibility(View.INVISIBLE);
            dialog.dismiss();
            JSONObject object = new JSONObject(response);
            String Str_status = object.getString("status");
            if (Str_status.equalsIgnoreCase("1")) {
                String SResponse = object.getString("response");
                alert(getResources().getString(R.string.action_success), SResponse);
            } else {
                String SResponse = object.getString("response");
                alert(getResources().getString(R.string.action_error), SResponse);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void errorreponse() {
        apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
    }

    @Override
    public void jsonexception() {
        apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        AppUtils.toastprint(this,"Json Exception");
    }
}
