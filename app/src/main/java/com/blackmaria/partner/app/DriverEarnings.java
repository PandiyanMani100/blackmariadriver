package com.blackmaria.partner.app;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.Pojo.ReferralImageBean;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.HorizontalListView;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.adapter.ReferralImageAdapter;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.devspark.appmsg.AppMsg;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by Ganesh on 16-05-2017.
 */

public class DriverEarnings extends ActivityHockeyApp implements View.OnClickListener {

    private ImageView Iv_back, Iv_light, Iv_driveAndEarnStatement, Iv_earningDetails;
    private RelativeLayout RL_withdraw;
    private LinearLayout LL_parent;
    private TextView Tv_today, Tv_thisWeek, Tv_thisMonth, Tv_todayCommission, Tv_availableAmount, Tv_referralCount, Tv_rates,
            Tv_todayEarnings, Tv_weekEarnings, Tv_monthEarnings;
    private HorizontalListView Lv_referralImages;
    private TextView Tv_emptyView;

    private String filterText = "";
    private String sDriverID = "", sCurrency = "", sReferralEarning = "", sAvailableAmount = "", sRates = "", sReferralCount = "",
            sStatementPeriod = "", sStatementAvailAmount = "", sIncomeStatementText = "", sCompletedTrip = "", sByCash = "", sByCard = "", ByWallet = "", sDeductAmount = "",
            sGrossEarning = "", sNettEarn = "", Scurrency="",sFeesAndCharges = "", sProceedWithdrawStatus = "", sProceedWithdrawError = "", sSecurePin = "",
            sPayAmount = "", sWithdrawStatus = "", sWithdrawError = "";
    private boolean isReferralAvailable = false;
    private ArrayList<ReferralImageBean> listReferral;

    private SessionManager sessionManager;
    private ConnectionDetector cd;
    private Dialog dialog;
    private ServiceRequest mRequest;
    private ReferralImageAdapter adapter;
    private BroadcastReceiver withdrawSuccessReceiver;
    private SimpleDateFormat mdFormat, mmFormat, myFormat;
    private Calendar mCalendar = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.driver_earnings2);

        initialize();

    }

    private void initialize() {

        sessionManager = new SessionManager(DriverEarnings.this);
        cd = new ConnectionDetector(DriverEarnings.this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        sSecurePin = sessionManager.getSecurePin();
        listReferral = new ArrayList<ReferralImageBean>();
        mdFormat = new SimpleDateFormat("yyyy-MM-dd");
        mmFormat = new SimpleDateFormat("MM");
        myFormat = new SimpleDateFormat("yyyy");

        // To Register Broadcast Receiver
        registerBroadcast();

        LL_parent = (LinearLayout) findViewById(R.id.LL_parent);
        Iv_back = (ImageView) findViewById(R.id.img_back);
        Iv_light = (ImageView) findViewById(R.id.img_blink);
        startBlinkingAnimation();
        Tv_today = (TextView) findViewById(R.id.txt_label_today);
        Tv_thisWeek = (TextView) findViewById(R.id.txt_label_this_week);
        Tv_thisMonth = (TextView) findViewById(R.id.txt_label_this_month);
        Tv_todayEarnings = (TextView) findViewById(R.id.txt_earning_today);
        Tv_weekEarnings = (TextView) findViewById(R.id.txt_earning_this_week);
        Tv_monthEarnings = (TextView) findViewById(R.id.txt_earning_this_month);
        Tv_todayCommission = (TextView) findViewById(R.id.txt_today_commission);
        Tv_availableAmount = (TextView) findViewById(R.id.txt_available_amount);
        Tv_referralCount = (TextView) findViewById(R.id.txt_referral);
        Tv_rates = (TextView) findViewById(R.id.txt_rates);
        RL_withdraw = (RelativeLayout) findViewById(R.id.Rl_image_withdraw);
        Iv_driveAndEarnStatement = (ImageView) findViewById(R.id.img_drive_and_earn_statement);

        Lv_referralImages = (HorizontalListView) findViewById(R.id.lst_referral_images);
        Tv_emptyView = (TextView) findViewById(R.id.txt_referral_empty);

        Iv_back.setOnClickListener(this);
        Iv_driveAndEarnStatement.setOnClickListener(this);
        RL_withdraw.setOnClickListener(this);
        Tv_today.setOnClickListener(this);
        Tv_thisWeek.setOnClickListener(this);
        Tv_thisMonth.setOnClickListener(this);

        if (cd.isConnectingToInternet()) {
            DriverEarningsNewRequest(Iconstant.earnings_dashboard_url);
        } else {
            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }

    }

    private void registerBroadcast() {

        IntentFilter filter = new IntentFilter();
        filter.addAction("com.app.Withdraw.Success");
        withdrawSuccessReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent != null) {

                    // Refresh Page to show Updated Content
                    if (cd.isConnectingToInternet()) {
                        DriverEarningsNewRequest(Iconstant.earnings_dashboard_url);
                    } else {
                        Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                    }

                }

            }
        };
        registerReceiver(withdrawSuccessReceiver, filter);

    }

    @Override
    public void onClick(View view) {

        if (view == Iv_back) {

            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);

        } else if (view == Iv_driveAndEarnStatement) {

            Intent intent = new Intent(DriverEarnings.this, DriveAndEarnStatement.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.exit);

        } else if (view == Tv_today) {

            mCalendar = Calendar.getInstance();
            String filterDate = mdFormat.format(mCalendar.getTime());

            Intent intent = new Intent(DriverEarnings.this, EarningsStatementDay.class);
            intent.putExtra("filterDate", filterDate);
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.exit);

        } else if (view == Tv_thisWeek) {

            getCurrentWeek();

        } else if (view == Tv_thisMonth) {

            getCurrentMonthAndYear();

        } else if (view == RL_withdraw) {

            withdrawAmount();

        }
    }

    // Steps to Get Current Week
    @SuppressLint("WrongConstant")
    private void getCurrentWeek() {

        mCalendar = Calendar.getInstance();

        mCalendar.set(Calendar.DAY_OF_WEEK, 1);
        String filterFromDate = mdFormat.format(mCalendar.getTime());

        mCalendar.set(Calendar.DAY_OF_WEEK, 7);
        String filterToDate = mdFormat.format(mCalendar.getTime());

        System.out.println("Current Week From Date = " + filterFromDate + "\nCurrent Week To Date = " + filterToDate);

        Intent intent = new Intent(DriverEarnings.this, EarningsStatementWeek.class);
        intent.putExtra("filterFromDate", filterFromDate);
        intent.putExtra("filterToDate", filterToDate);
        startActivity(intent);
        overridePendingTransition(R.anim.enter, R.anim.exit);

    }


    // Steps to Get Current Month And Year
    private void getCurrentMonthAndYear() {

        mCalendar = Calendar.getInstance();

        String filterMonth = mmFormat.format(mCalendar.getTime());
        String filterYear = myFormat.format(mCalendar.getTime());

        Intent intent = new Intent(DriverEarnings.this, EarningsStatementMonth.class);
        intent.putExtra("filterMonth", filterMonth);
        intent.putExtra("filterYear", filterYear);
        startActivity(intent);
        overridePendingTransition(R.anim.enter, R.anim.exit);

    }

    //-------------Help Dialog-----------------
    private void showHelpDialog() {

        //--------Adjusting Dialog width & Height-----
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 80% of the screen
        int screenHeight = (int) (metrics.heightPixels * 0.85);//fill only 80% of the screen

        final Dialog helpDialog = new Dialog(DriverEarnings.this, R.style.SlideUpDialog);
        helpDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        helpDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        helpDialog.setCancelable(true);
        helpDialog.setContentView(R.layout.alert_earnings_help_info);
        helpDialog.getWindow().setLayout(screenWidth, screenHeight);

        final ImageView Iv_close = (ImageView) helpDialog.findViewById(R.id.img_close);
        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                helpDialog.dismiss();
            }
        });

       /* helpDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                Tv_help.setClickable(true);
            }
        });

        helpDialog.show();
        Tv_help.setClickable(false);*/

        helpDialog.show();

    }

    //-------------Confirm Withdrawal Dialog-----------------
    private void confirmWithdrawDialog() {

        //--------Adjusting Dialog width & Height-----
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 80% of the screen
        int screenHeight = (int) (metrics.heightPixels * 0.85);//fill only 80% of the screen

        final Dialog confirmWithdrawDialog = new Dialog(DriverEarnings.this, R.style.SlideUpDialog);
        confirmWithdrawDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmWithdrawDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmWithdrawDialog.setCancelable(true);
        confirmWithdrawDialog.setContentView(R.layout.alert_earnings_withdrawal_proceed);
        confirmWithdrawDialog.getWindow().setLayout(screenWidth, screenHeight);

        final LinearLayout LL_debitEarnings = (LinearLayout) confirmWithdrawDialog.findViewById(R.id.Ll_debit_earnings);
        final TextView Tv_period = (TextView) confirmWithdrawDialog.findViewById(R.id.txt_period);
        final TextView Tv_availableBalance = (TextView) confirmWithdrawDialog.findViewById(R.id.txt_available_balance);
        final TextView Tv_incomeSummary = (TextView) confirmWithdrawDialog.findViewById(R.id.txt_date);
        final TextView Tv_completedTrip = (TextView) confirmWithdrawDialog.findViewById(R.id.txt_completed_trip);
        final TextView Tv_paidByCash = (TextView) confirmWithdrawDialog.findViewById(R.id.txt_paid_by_cash);
        final TextView Tv_byEwallet = (TextView) confirmWithdrawDialog.findViewById(R.id.txt_ewallet);
        final TextView Tv_byCards = (TextView) confirmWithdrawDialog.findViewById(R.id.txt_cards);
        final TextView Tv_grossEarnings = (TextView) confirmWithdrawDialog.findViewById(R.id.txt_gross_earnings);
        final TextView Tv_debitEarnings = (TextView) confirmWithdrawDialog.findViewById(R.id.txt_debit_earnings);
        final TextView Tv_feesAndCharges = (TextView) confirmWithdrawDialog.findViewById(R.id.txt_fees_and_charges);
        final TextView Tv_referralIncome = (TextView) confirmWithdrawDialog.findViewById(R.id.txt_referral_income);
        final TextView Tv_incentive = (TextView) confirmWithdrawDialog.findViewById(R.id.txt_incentive);
        final TextView Tv_nettEarn = (TextView) confirmWithdrawDialog.findViewById(R.id.txt_net_earn);
        final Button Btn_proceed = (Button) confirmWithdrawDialog.findViewById(R.id.btn_proceed);
        final ImageView Iv_close = (ImageView) confirmWithdrawDialog.findViewById(R.id.img_close);

        Tv_period.setText(sStatementPeriod);
        Tv_availableBalance.setText(Scurrency+" "+sStatementAvailAmount);
        Tv_incomeSummary.setText(sIncomeStatementText);
        Tv_completedTrip.setText(sCompletedTrip);
        Tv_paidByCash.setText(Scurrency+" "+sByCash);
        Tv_byEwallet.setText(Scurrency+" "+ByWallet);
        Tv_byCards.setText(Scurrency+" "+sByCard);
        Tv_grossEarnings.setText(Scurrency+" "+sGrossEarning);
        Tv_debitEarnings.setText(Scurrency+" "+sDeductAmount);
        Tv_feesAndCharges.setText(sFeesAndCharges+" "+"%");
        Tv_nettEarn.setText(Scurrency+" "+sNettEarn);

        if (sDeductAmount != null && sDeductAmount.length() > 0) {
            if (sDeductAmount.equalsIgnoreCase("0")) {
                LL_debitEarnings.setVisibility(View.GONE);
            } else {
                LL_debitEarnings.setVisibility(View.VISIBLE);
            }
        } else {
            LL_debitEarnings.setVisibility(View.GONE);
        }


        Btn_proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                confirmWithdrawDialog.dismiss();
                sPayAmount = sAvailableAmount.replace(sCurrency, "").trim();
                if (cd.isConnectingToInternet()) {
                    PostRequestWithdrawalAmount(Iconstant.earnings_withdraw_amount_url);
                } else {
                    Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                }


            }
        });

        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (confirmWithdrawDialog != null && confirmWithdrawDialog.isShowing()) {
                    confirmWithdrawDialog.dismiss();
                }
            }
        });

        confirmWithdrawDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                RL_withdraw.setClickable(true);
            }
        });

        confirmWithdrawDialog.show();
        RL_withdraw.setClickable(false);

    }


    private void withdrawAmount() {

        String amount = sAvailableAmount.replace(sCurrency, "").trim();

        if (amount.length() > 0) {

            double totalAmount = Double.parseDouble(amount);

            if (totalAmount > 0) {

                if (sProceedWithdrawStatus.equalsIgnoreCase("1")) {
                    if (sWithdrawStatus.equalsIgnoreCase("1")) {
                        confirmPin();
                    } else {
                        Alert(getResources().getString(R.string.action_error), sWithdrawError);
                    }
                } else {
                    Alert(getResources().getString(R.string.action_error), sProceedWithdrawError);
                }

            } else {
                String message = getResources().getString(R.string.earnings_page_label_no_amount_to_withdraw);
                Alert(getResources().getString(R.string.action_error), message);
            }

        }

    }


    private void confirmPin() {
        final Dialog confirmPinDialog = new Dialog(DriverEarnings.this, R.style.SlideRightDialog);
        confirmPinDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmPinDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        confirmPinDialog.setCancelable(true);
        confirmPinDialog.setContentView(R.layout.alert_enter_pin);

        final PinEntryEditText Ed_pin = (PinEntryEditText) confirmPinDialog.findViewById(R.id.edt_withdrawal_amount);
        final TextView Tv_forgotPin = (TextView) confirmPinDialog.findViewById(R.id.txt_label_forgot_pin);
        final RelativeLayout RL_confirm = (RelativeLayout) confirmPinDialog.findViewById(R.id.Rl_confirm);
        final ImageView Iv_close = (ImageView) confirmPinDialog.findViewById(R.id.img_close);

        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmPinDialog.dismiss();
            }
        });

        Tv_forgotPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (cd.isConnectingToInternet()) {
                    ForgotPinRequest(Iconstant.forgot_pin_url);
                } else {
                    Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                }
            }
        });

        RL_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    CloseKeyboard(Ed_pin);
                } catch (Exception e) {
                }
                confirmPinDialog.dismiss();

                if (sSecurePin.equalsIgnoreCase(Ed_pin.getText().toString())) {

                    try {
                        if (cd.isConnectingToInternet()) {
                            WithdrawalStatement(Iconstant.withdrawal_statement_url);
                        } else {
                            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                } else {
                    AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.label_incorrect_pin));
                }

            }
        });

        confirmPinDialog.show();

    }


    @SuppressLint("WrongConstant")
    private void DriverEarningsNewRequest(String Url) {

        dialog = new Dialog(DriverEarnings.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
//        jsonParams.put("filter_type", formatedFilterText);

        System.out.println("--------------DriverEarnings Url-------------------" + Url);
        System.out.println("--------------DriverEarnings jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(DriverEarnings.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------DriverEarnings Response-------------------" + response);
                String status = "", sFromDate = "", sToDate = "", sTodayEarning = "", sWeekEarning = "", sMonthEarning = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {
                            JSONObject jsonObject = object.getJSONObject("response");
                            if (jsonObject.length() > 0) {

                                sCurrency = jsonObject.getString("currency");
                                sReferralEarning = sCurrency + " " + jsonObject.getString("referral_earning");
                                sAvailableAmount = sCurrency + " " + jsonObject.getString("available_amount");
                                sRates = jsonObject.getString("rates");
                                sReferralCount = jsonObject.getString("referral_count");
                                sProceedWithdrawStatus = jsonObject.getString("proceed_status");
                                sProceedWithdrawError = jsonObject.getString("error_message");
                                sWithdrawStatus = jsonObject.getString("withdraw_status");
                                sWithdrawError = jsonObject.getString("withdraw_error");
                                sTodayEarning = sCurrency + " " + jsonObject.getString("today_earning");
                                sWeekEarning = sCurrency + " " + jsonObject.getString("week_earning");
                                sMonthEarning = sCurrency + " " + jsonObject.getString("month_earning");

                                Object objRef = jsonObject.get("ref_info");
                                if (objRef instanceof JSONArray) {

                                    JSONArray jarRef = jsonObject.getJSONArray("ref_info");

                                    if (jarRef.length() > 0) {

                                        listReferral = new ArrayList<>();
                                        for (int i = 0; i < jarRef.length(); i++) {

                                            JSONObject jobjRef = jarRef.getJSONObject(i);

                                            if (jobjRef.length() > 0) {

                                                String driver_name = jobjRef.getString("driver_name");
                                                String driver_image = jobjRef.getString("driver_image");

                                                listReferral.add(new ReferralImageBean(driver_name, driver_image));

                                            }
                                        }

                                        isReferralAvailable = true;

                                    } else {
                                        isReferralAvailable = false;
                                    }


                                } else {
                                    isReferralAvailable = false;
                                }

                            } else {
                                isReferralAvailable = false;
                            }
                        } else {
                            String sResponse = object.getString("response");
                            Alert(getResources().getString(R.string.action_error), sResponse);
                        }

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (status.equalsIgnoreCase("1")) {
                    Tv_todayCommission.setText(sReferralEarning);
                    Tv_availableAmount.setText(sAvailableAmount);
                    Tv_referralCount.setText(sReferralCount);
                    Tv_rates.setText(sRates);
                    Tv_todayEarnings.setText(sTodayEarning);
                    Tv_weekEarnings.setText(sWeekEarning);
                    Tv_monthEarnings.setText(sMonthEarning);

                    if (isReferralAvailable) {
                        adapter = new ReferralImageAdapter(DriverEarnings.this, listReferral);
                        Lv_referralImages.setAdapter(adapter);
                        Tv_emptyView.setVisibility(View.GONE);
                    } else {
                        Lv_referralImages.setVisibility(View.GONE);
                        Tv_emptyView.setVisibility(View.VISIBLE);
                    }

                }

                if (dialog != null) {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    @SuppressLint("WrongConstant")
    private void WithdrawalStatement(String Url) {

        dialog = new Dialog(DriverEarnings.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);

        System.out.println("--------------WithdrawalStatement Url-------------------" + Url);
        System.out.println("--------------WithdrawalStatement jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(DriverEarnings.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------WithdrawalStatement Response-------------------" + response);
                String status = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {
                            JSONObject jsonObject = object.getJSONObject("response");
                            if (jsonObject.length() > 0) {

                                sStatementPeriod = getResources().getString(R.string.earnings_page_label_period) + jsonObject.getString("period_statment");
                                sStatementAvailAmount =jsonObject.getString("available_amount");
                                sIncomeStatementText = jsonObject.getString("income_statement_txt");
                                sCompletedTrip = jsonObject.getString("completed_trip");
                                sByCash = jsonObject.getString("paid_by_cash");
                                sByCard = jsonObject.getString("by_card");
                                ByWallet = jsonObject.getString("by_wallet");
                                sDeductAmount = jsonObject.getString("deduct_amount");
                                sGrossEarning = jsonObject.getString("gross_earning");
                                sNettEarn = jsonObject.getString("nett_earn");
                                sFeesAndCharges = jsonObject.getString("fees_charge");
                                Scurrency=jsonObject.getString("currency");
                            } else {
                                isReferralAvailable = false;
                            }
                        } else {
                            String sResponse = object.getString("response");
                            Alert(getResources().getString(R.string.action_error), sResponse);
                        }

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (dialog != null) {
                    dialog.dismiss();
                }

                if (status.equalsIgnoreCase("1")) {
                    confirmWithdrawDialog();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    @SuppressLint("WrongConstant")
    private void ForgotPinRequest(String Url) {

        dialog = new Dialog(DriverEarnings.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);

        System.out.println("--------------ForgotPinRequest Url-------------------" + Url);
        System.out.println("--------------ForgotPinRequest jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(DriverEarnings.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                if (dialog != null) {
                    dialog.dismiss();
                }

                System.out.println("--------------ForgotPinRequest Response-------------------" + response);
                String status = "", sResponse = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");
                        sResponse = object.getString("message");

                        if (status.equalsIgnoreCase("1")) {
                            Alert(getResources().getString(R.string.action_success), sResponse);
                        } else {
                            Alert(getResources().getString(R.string.action_error), sResponse);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    @SuppressLint("WrongConstant")
    private void PostRequestWithdrawalAmount(String Url) {

        dialog = new Dialog(DriverEarnings.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        jsonParams.put("amount", sPayAmount);
        jsonParams.put("mode", "wallet");
        //jsonParams.put("paypal_id", sPaypalID);

        System.out.println("--------------WithdrawalAmount Url-------------------" + Url);
        System.out.println("--------------WithdrawalAmount jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(DriverEarnings.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------WithdrawalAmount Response-------------------" + response);
                String status = "", sResponse = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");
                        sResponse = object.getString("response");

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (dialog != null) {
                    dialog.dismiss();
                }

                if (status.equalsIgnoreCase("1")) {
                    WithdrawSuccessAlert(sPayAmount, "wallet");
                } else {
                    WithdrawErrorAlert(getResources().getString(R.string.action_error), sResponse);
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(DriverEarnings.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    /*private void AlertError(String title, String message) {

        String snackText = title + "\n" + message;
        int colorBg = getResources().getColor(R.color.homepage_alert_verify_account_red);

        Snacky.builder()
                .setBackgroundColor(colorBg)
                .setTextSize(14)
                .setTextColor(Color.WHITE)
                .setText(snackText.toUpperCase())
                .setMaxLines(4)
                .centerText()
                .setActivty(DriverEarnings.this)
                .setDuration(Snacky.LENGTH_LONG)
                .build()
                .show();

    }*/

    private void WithdrawSuccessAlert(String withdrawAmount, String paymentType) {
        final Dialog withdrawSuccessDialog = new Dialog(DriverEarnings.this, R.style.SlideUpDialog);
        withdrawSuccessDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        withdrawSuccessDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        withdrawSuccessDialog.setCancelable(true);
        withdrawSuccessDialog.setContentView(R.layout.alert_withdraw_success1);

        final ImageView Iv_close = (ImageView) withdrawSuccessDialog.findViewById(R.id.img_close);
        final TextView Tv_withdrawAmount = (TextView) withdrawSuccessDialog.findViewById(R.id.txt_alert_amount);
        final TextView Tv_paymentType = (TextView) withdrawSuccessDialog.findViewById(R.id.txt_alert_payment_type);

        Tv_withdrawAmount.setText(withdrawAmount);
        paymentType = getResources().getString(R.string.withdrawal_page_label_payment_type) + paymentType;
        Tv_paymentType.setText(paymentType);

        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (withdrawSuccessDialog != null && withdrawSuccessDialog.isShowing()) {
                    withdrawSuccessDialog.dismiss();
                }
            }
        });

        withdrawSuccessDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);

                Intent intent = new Intent();
                intent.setAction("com.app.Withdraw.Success");
                sendBroadcast(intent);
            }
        });
        withdrawSuccessDialog.show();
    }

    private void WithdrawErrorAlert(String message1, String message2) {
        final Dialog withdrawErrorDialog = new Dialog(DriverEarnings.this, R.style.SlideUpDialog);
        withdrawErrorDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        withdrawErrorDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        withdrawErrorDialog.setCancelable(true);
        withdrawErrorDialog.setContentView(R.layout.alert_withdrawal_errors);

        final ImageView Iv_close = (ImageView) withdrawErrorDialog.findViewById(R.id.img_close);
        final TextView Tv_message1 = (TextView) withdrawErrorDialog.findViewById(R.id.txt_alert_message1);
        final TextView Tv_message2 = (TextView) withdrawErrorDialog.findViewById(R.id.txt_alert_message2);

        Tv_message1.setText(message1);
        Tv_message2.setText(message2);

        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (withdrawErrorDialog != null && withdrawErrorDialog.isShowing()) {
                    withdrawErrorDialog.dismiss();
                }
            }
        });

        withdrawErrorDialog.show();

    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {

//            refreshHomePage();

            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
            return true;
        }
        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (withdrawSuccessReceiver != null) {
            unregisterReceiver(withdrawSuccessReceiver);
        }

    }

    // Sending Broadcast to Refresh Home Page
    private void refreshHomePage() {

        // Refresh Home Page
        Intent intent = new Intent();
        intent.setAction("com.app.Refresh.HomePage");
        sendBroadcast(intent);

    }

    private void startBlinkingAnimation() {
        Animation startAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blinking_animation);
        Iv_light.startAnimation(startAnimation);
    }

    @SuppressWarnings("WrongConstant")
    private void CloseKeyboardNew() {
        try {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow((null == getCurrentFocus()) ? null : getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("WrongConstant")
    private void CloseKeyboard(EditText edittext) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(edittext.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }


    private void AlertError(String title, String message) {

        String msg = title + "\n" + message;

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        AppMsg.Style style = new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red_color);
        AppMsg snack = AppMsg.makeText(DriverEarnings.this, msg.toUpperCase(), AppMsg.STYLE_ALERT);
        snack.setView(view);
        snack.setLayoutGravity(Gravity.TOP);
        snack.setPriority(AppMsg.PRIORITY_HIGH);
        snack.setAnimation(R.anim.enter, R.anim.exit);
        snack.show();

    }

}
