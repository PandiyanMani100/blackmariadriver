package com.blackmaria.partner.app;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.DowloadPdfActivity;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.CustomTextView;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.blackmaria.partner.latestpackageview.widgets.TextRCBold;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by Murugananatham on 3/2/2018.
 */

@SuppressLint("Registered")
public class ReloadXenditPaymentSuccessActivity extends ActivityHockeyApp implements  DowloadPdfActivity.downloadCompleted {

    private TextView Transection_id, paymentcode;
    private CustomTextView current_username, date, time;
    private TextRCBold Amount;
    RelativeLayout Rl_close, Rl_save;
    private SessionManager session;
    private boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    Dialog dialog;
    private String UserNmae = "", UserID = "", xenditPaymentId = "", Scurrent_user = "", Smode = "", amount = "", transection_id = "", currency = "", flag = "";
    private TextView username;
    private LinearLayout xenditcard, xenditbank;
    private RelativeLayout paypalpayment;
    private RefreshReceiver refreshReceiver;
    String from1="",date1="",time1="";
    LinearLayout pdflayout;
    RelativeLayout savelyt;
    private TextView sava_tv;
    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reloadxenditpayment_success);
        initiaztion();

    }


    private void initiaztion() {

        session = new SessionManager(ReloadXenditPaymentSuccessActivity.this);
        cd = new ConnectionDetector(ReloadXenditPaymentSuccessActivity.this);
        isInternetPresent = cd.isConnectingToInternet();

        HashMap<String, String> info = session.getUserDetails();
        UserID = info.get(SessionManager.KEY_DRIVERID);
        UserNmae = info.get(SessionManager.KEY_DRIVER_NAME);

        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.pushnotification.updateBottom_view");
        registerReceiver(refreshReceiver, intentFilter);

        Transection_id = (TextView) findViewById(R.id.txt_trasection_id);
        current_username = (CustomTextView) findViewById(R.id.current_username);
        date = (CustomTextView) findViewById(R.id.trans_date);
        time = (CustomTextView) findViewById(R.id.trans_time);
        Amount = (TextRCBold) findViewById(R.id.amount_text);
        Rl_close = (RelativeLayout) findViewById(R.id.close_btn);
        Rl_save = (RelativeLayout) findViewById(R.id.tarnsection_save);
        paymentcode = (TextView) findViewById(R.id.paymentcode);
        xenditcard = (LinearLayout) findViewById(R.id.xenditcard);
        xenditbank = (LinearLayout) findViewById(R.id.xenditbank);
        paypalpayment = (RelativeLayout) findViewById(R.id.paypalpayment);


        pdflayout=(LinearLayout) findViewById(R.id.pdflayout);
        savelyt= (RelativeLayout) findViewById(R.id.tarnsection_save);
        sava_tv= (TextView) findViewById(R.id.sava_tv);

        Intent in = getIntent();
        flag = in.getStringExtra("flag");
        amount = in.getStringExtra("amount");
        transection_id = in.getStringExtra("ref_no");
        currency = in.getStringExtra("currency");

        if (flag.equalsIgnoreCase("xenditReaload")) {
            date1 = in.getStringExtra("date");
            time1 = in.getStringExtra("time");
            xenditcard.setVisibility(View.VISIBLE);
            xenditbank.setVisibility(View.GONE);
        } else if (flag.equalsIgnoreCase("xenditBankReaload")) {
            date1 = in.getStringExtra("date");
            time1 = in.getStringExtra("time");
            xenditPaymentId = in.getStringExtra("xenditPaymentId");
            xenditcard.setVisibility(View.GONE);
            paymentcode.setVisibility(View.VISIBLE);
            xenditbank.setVisibility(View.VISIBLE);
            paymentcode.setText("Payment Code : " + xenditPaymentId);
        } else if (flag.equalsIgnoreCase("paypalpayment")) {
            from1 = in.getStringExtra("from");
            date1 = in.getStringExtra("date");
            time1 = in.getStringExtra("time");

            paypalpayment.setVisibility(View.VISIBLE);
            xenditcard.setVisibility(View.GONE);
            xenditbank.setVisibility(View.GONE);
            paymentcode.setVisibility(View.GONE);
        }


        setvalues();

        Rl_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ReloadXenditPaymentSuccessActivity.this,WalletMoneyPage1.class);
                startActivity(i);
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });

        Rl_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DowloadPdfActivity(ReloadXenditPaymentSuccessActivity.this,pdflayout,"moneytransfer",ReloadXenditPaymentSuccessActivity.this);
                savelyt.setVisibility(View.GONE);
            }
        });

    }


    private void setvalues() {
        Transection_id.setText("TRANSACTION NO" + " " + ":" + " " + transection_id);
        Amount.setText(currency + " " + amount);
        if(date1.equalsIgnoreCase("")) {
            date.setText(GetCurrentDate());
        }else{
            date.setText(date1);
        }
        if(date1.equalsIgnoreCase("")) {
            time.setText(GetCurrentTime());
        }else{
            time.setText(time1);
        }
        if(from1.equalsIgnoreCase("")) {
            current_username.setText(UserNmae);
        }else{
            current_username.setText(from1);
        }
    }

    private String GetCurrentDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd MMMM yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }


    private String GetCurrentTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss aaa");
        String formattedtime = df.format(c.getTime());
        return formattedtime;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(refreshReceiver);
    }
    @Override
    public void OnDownloadComplete(String fileName) {
        savelyt.setVisibility(View.VISIBLE);
        sava_tv.setText("Saved");

        ViewPdfAlert(""," Successfully Downloaded",fileName);
    }
    //--------------Alert Method-----------
    private void ViewPdfAlert( String title, String alert,final String fileName) {
        final PkDialog mDialog = new PkDialog(ReloadXenditPaymentSuccessActivity.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton("OPEN", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPDF(fileName);
                mDialog.dismiss();
            }
        });
        mDialog.setNegativeButton(getResources().getString(R.string.cancel_lable), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }



    public void viewPDF(String pdfFileName) {
//        File pdfFile = new File(Environment.getExternalStorageDirectory() + "/Signature/" + pdfFileName);  // -> filename = maven.pdf
//        Uri path = Uri.fromFile(pdfFile);
//        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
//        pdfIntent.setDataAndType(path, "application/pdf");
//        pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);


        File pdfFile = new File(Environment.getExternalStorageDirectory() + "/Signature/" + pdfFileName);  // -> filename = maven.pdf
        Uri uri_path = Uri.fromFile(pdfFile);
        Uri uri = uri_path;

//        Uri uri = Uri.parse(Environment.getExternalStorageDirectory() + "/Signature/" + pdfFileName);

        try {
            File file = null;
            String path = uri.toString();// "/mnt/sdcard/FileName.mp3"
            try {
                file = new File(new URI(path));
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }

            final Intent intent = new Intent(Intent.ACTION_VIEW)//
                    .setDataAndType(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ?
                                    androidx.core.content.FileProvider.getUriForFile(ReloadXenditPaymentSuccessActivity.this, getPackageName() + ".provider", file) : Uri.fromFile(file),
                            "application/pdf").addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            try {
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(ReloadXenditPaymentSuccessActivity.this, "No Application available to view PDF", Toast.LENGTH_SHORT).show();
            }

        } catch (ActivityNotFoundException e) {
//            Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.cannot_read_file_type));
        }
//        try {
//            startActivity(pdfIntent);
//        } catch (Exception e) {
//            Log.d("Log",e.getMessage());
//            Toast.makeText(getApplicationContext(), "No Application available to view PDF", Toast.LENGTH_SHORT).show();
//        }
    }
}
