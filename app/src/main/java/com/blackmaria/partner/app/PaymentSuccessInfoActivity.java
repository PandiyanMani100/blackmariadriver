package com.blackmaria.partner.app;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

;

import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.DowloadPdfActivity;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.CustomTextView;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.blackmaria.partner.latestpackageview.widgets.TextRCBold;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by Muruganantham on 3/1/2018.
 */
@SuppressLint("Registered")
public class PaymentSuccessInfoActivity extends ActivityHockeyApp implements DowloadPdfActivity.downloadCompleted {

    private TextView Transection_id,paypal_tv;
    private CustomTextView transection_mode, paypal_id, current_username, date, time;
    private TextRCBold Amount;
    RelativeLayout Rl_close, Rl_save;
    private SessionManager session;
    private boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    Dialog dialog;
    private String UserNmae = "",account="", UserID = "", Scurrent_user = "", Smode = "", Spaypalid = "", amount = "", transection_id = "", currency = "", flag = "";
    private TextView username;
TextView payid_dot, username1, usernamedot,usernameTv ;


    RelativeLayout savelyt;
    LinearLayout pdflayout;
    private TextView sava_tv,header_text;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xenditbank_payment_success);
        initiaztion();

    }


    private void initiaztion() {

        session = new SessionManager(PaymentSuccessInfoActivity.this);
        cd = new ConnectionDetector(PaymentSuccessInfoActivity.this);
        isInternetPresent = cd.isConnectingToInternet();

        HashMap<String, String> info = session.getUserDetails();
        UserID = info.get(SessionManager.KEY_DRIVERID);
        UserNmae = info.get(SessionManager.KEY_DRIVER_NAME);

        savelyt= (RelativeLayout) findViewById(R.id.savelyt);
        sava_tv= (TextView) findViewById(R.id.sava_tv);
        pdflayout=(LinearLayout) findViewById(R.id.pdflayout);
        header_text= (TextView) findViewById(R.id.header_text);
        Transection_id = (TextView) findViewById(R.id.txt_trasection_id);


        transection_mode = (CustomTextView) findViewById(R.id.paypal_to);
        paypal_id = (CustomTextView) findViewById(R.id.paypal_id_txt);
        current_username = (CustomTextView) findViewById(R.id.current_username);
        date = (CustomTextView) findViewById(R.id.trans_date);
        time = (CustomTextView) findViewById(R.id.trans_time);
        Amount = (TextRCBold) findViewById(R.id.amount_text);
        Rl_close = (RelativeLayout) findViewById(R.id.close_btn);
        Rl_save = (RelativeLayout) findViewById(R.id.tarnsection_save);
        paypal_tv = (TextView) findViewById(R.id.paypal_tv);
        payid_dot= (TextView) findViewById(R.id.payid_dot);
        Intent in = getIntent();
        username1 = (TextView) findViewById(R.id.name);
        usernamedot = (TextView) findViewById(R.id.payid_dote);
        usernameTv = (TextView) findViewById(R.id.name_tv);

        flag = in.getStringExtra("flag");

        if (flag.equalsIgnoreCase("XenditbankTransfer")) {
            Smode = in.getStringExtra("mode");
            amount = in.getStringExtra("amount");
            transection_id = in.getStringExtra("ref_no");
            currency = in.getStringExtra("currency");
            account = in.getStringExtra("account");
            paypal_id.setVisibility(View.VISIBLE);
            paypal_tv.setVisibility(View.VISIBLE);
            paypal_tv.setText("ACCOUNT");
            paypal_id.setText(account);

        }else if(flag.equalsIgnoreCase("paypal_withdrawal")) {
            paypal_tv.setVisibility(View.VISIBLE);
            paypal_id.setVisibility(View.VISIBLE);
            Smode = in.getStringExtra("mode");
            amount = in.getStringExtra("amount");
            transection_id = in.getStringExtra("ref_no");
            currency = in.getStringExtra("currency");
            Spaypalid = in.getStringExtra("paypal_id");
            paypal_id.setAllCaps(false);
            paypal_id.setText(Spaypalid);
            header_text.setText("payment success");
        } else  {
            Smode = in.getStringExtra("mode");
            amount = in.getStringExtra("amount");
            transection_id = in.getStringExtra("ref_no");
            currency = in.getStringExtra("currency");
            paypal_tv.setText("TRN NUMBER");
            payid_dot.setVisibility(View.GONE);
            paypal_id.setVisibility(View.VISIBLE);
            paypal_id.setText(transection_id);
            header_text.setText("payment success");

            username1 .setVisibility(View.VISIBLE);
            usernamedot.setVisibility(View.VISIBLE);
            usernameTv.setVisibility(View.VISIBLE);
            usernameTv.setText(UserNmae);

        }


        setvalues();

        Rl_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PaymentSuccessInfoActivity.this,WalletMoneyPage1.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);

            }
        });

        Rl_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                savelyt.setVisibility(View.GONE);
                new DowloadPdfActivity(PaymentSuccessInfoActivity.this,pdflayout,Smode,PaymentSuccessInfoActivity.this);


            }
        });
    }


    private void setvalues() {
        Transection_id.setText("TRANSACTION NO" + " " + ":" + " " + transection_id);
        transection_mode.setText(Smode);
        current_username.setText(UserNmae);
        Amount.setText(currency + " " + amount);

        date.setText(GetCurrentDate());
        time.setText(GetCurrentTime());
    }

    private String GetCurrentDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd MMMM yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }


    private String GetCurrentTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss aaa");
        String formattedtime = df.format(c.getTime());
        return formattedtime;
    }
    @Override
    public void OnDownloadComplete(String fileName) {
        savelyt.setVisibility(View.VISIBLE);
        sava_tv.setText("Saved");

        ViewPdfAlert(""," Successfully Downloaded",fileName);
    }
    //--------------Alert Method-----------
    private void ViewPdfAlert( String title, String alert,final String fileName) {

        final PkDialog mDialog = new PkDialog(PaymentSuccessInfoActivity.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton("OPEN", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPDF(fileName);
                mDialog.dismiss();
            }
        });
        mDialog.setNegativeButton(getResources().getString(R.string.cancel_lable), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();

    }
    public void viewPDF(String pdfFileName) {
        try {
            File pdfFile = new File(Environment.getExternalStorageDirectory() + "/Signature/" + pdfFileName);  // -> filename = maven.pdf
            Uri uri_path = Uri.fromFile(pdfFile);
            Uri uri = uri_path;

            try {
                File file = null;
                String path = uri.toString();// "/mnt/sdcard/FileName.mp3"
                try {
                    file = new File(new URI(path));
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }

                final Intent intent = new Intent(Intent.ACTION_VIEW)//
                        .setDataAndType(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ?
                                        androidx.core.content.FileProvider.getUriForFile(PaymentSuccessInfoActivity.this, getPackageName() + ".provider", file) : Uri.fromFile(file),
                                "application/pdf").addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(PaymentSuccessInfoActivity.this, "No Application available to view PDF", Toast.LENGTH_SHORT).show();
                }

            } catch (ActivityNotFoundException e) {
                Toast.makeText(PaymentSuccessInfoActivity.this, "Cannot read file error. ", Toast.LENGTH_SHORT).show();
            }

        }catch (NullPointerException e){

        }catch (Exception ew){}
    }

}
