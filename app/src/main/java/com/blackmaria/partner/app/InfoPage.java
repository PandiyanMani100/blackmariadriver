package com.blackmaria.partner.app;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.Pojo.MessagePojo;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.adapter.InfoAdapter;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.CustomTextView;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Ganesh on 01-06-2017.
 */

public class InfoPage extends ActivityHockeyApp implements View.OnClickListener {

    private RelativeLayout RL_home;
    private ImageView Iv_previous, Iv_next;
    private ExpandableHeightListView Lv_notifications;
    private TextView Tv_emptyView;

    private ConnectionDetector cd;
    private SessionManager sessionManager;
    private ServiceRequest mRequest;
    private Dialog dialog;
    private InfoAdapter adapter;
    SimpleDateFormat input = new SimpleDateFormat("dd MMMM yyyy HH:mm a");
    SimpleDateFormat output = new SimpleDateFormat("dd MMM yyyy");

    private String sDriveID = "";
    private boolean isDataPresent = false;
    private ArrayList<MessagePojo> notificationsList;
    private int currentPage = 1;
    private String perPage = "5";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info_page_new);
        initialize();
    }

    private void initialize() {
        sessionManager = new SessionManager(InfoPage.this);
        cd = new ConnectionDetector(InfoPage.this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriveID = user.get(SessionManager.KEY_DRIVERID);
        notificationsList = new ArrayList<>();

        RL_home = (RelativeLayout) findViewById(R.id.RL_home);
        Iv_previous = (ImageView) findViewById(R.id.img_prev);
        Iv_next = (ImageView) findViewById(R.id.img_next);
        Lv_notifications = (ExpandableHeightListView) findViewById(R.id.Lv_notifications);
        Tv_emptyView = (TextView) findViewById(R.id.Tv_empty);

        RL_home.setOnClickListener(this);
        Iv_previous.setOnClickListener(this);
        Iv_next.setOnClickListener(this);

        Lv_notifications.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                /*if (notificationsList.get(position).getType().equalsIgnoreCase("earning")) {
                    withdrawalAdvice(position);
                } else {
                    MessagePopUp(position);
                }*/
                ViewNotificationRequest(Iconstant.read_message_url, position);
            }
        });

        if (cd.isConnectingToInternet()) {
            InfoRequest(Iconstant.message_url);
        } else {
            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }


    }

    @Override
    public void onClick(View view) {

        if (cd.isConnectingToInternet()) {

            if (view == RL_home) {
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            } else if (view == Iv_previous) {
                currentPage = currentPage - 1;
                InfoRequest(Iconstant.message_url);
            } else if (view == Iv_next) {
                currentPage = currentPage + 1;
                InfoRequest(Iconstant.message_url);
            }

        } else {
            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }

    }

    //-------------Confirm Withdrawal Dialog-----------------
    @SuppressLint("SetTextI18n")
    public void withdrawalAdvice(int position) {

        //--------Adjusting Dialog width & Height-----
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 80% of the screen
        int screenHeight = (int) (metrics.heightPixels * 0.85);//fill only 80% of the screen

        final Dialog withdrawalAdviceDialog = new Dialog(InfoPage.this, R.style.SlideUpDialog);
        withdrawalAdviceDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        withdrawalAdviceDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        withdrawalAdviceDialog.setCancelable(true);
        withdrawalAdviceDialog.setContentView(R.layout.alert_withdrawal_advice);
        withdrawalAdviceDialog.getWindow().setLayout(screenWidth, screenHeight);

        final ImageView Iv_close = (ImageView) withdrawalAdviceDialog.findViewById(R.id.img_close);
        final TextView Tv_refNo = (TextView) withdrawalAdviceDialog.findViewById(R.id.txt_ref_no);
        final TextView Tv_locationId = (TextView) withdrawalAdviceDialog.findViewById(R.id.txt_location_id);
        final TextView Tv_partnerId = (TextView) withdrawalAdviceDialog.findViewById(R.id.txt_partner_id);
        final TextView Tv_driverId = (TextView) withdrawalAdviceDialog.findViewById(R.id.txt_driver_id);
        final TextView Tv_companyId = (TextView) withdrawalAdviceDialog.findViewById(R.id.txt_company_id);
        final TextView Tv_amount = (TextView) withdrawalAdviceDialog.findViewById(R.id.txt_amount);
        final TextView Tv_period = (TextView) withdrawalAdviceDialog.findViewById(R.id.txt_period);
        final TextView Tv_payment = (TextView) withdrawalAdviceDialog.findViewById(R.id.txt_payment);
        final TextView Tv_completedTrip = (TextView) withdrawalAdviceDialog.findViewById(R.id.txt_completed_trip);
        final TextView Tv_paidByCash = (TextView) withdrawalAdviceDialog.findViewById(R.id.txt_paid_by_cash);
        final TextView Tv_byWallet = (TextView) withdrawalAdviceDialog.findViewById(R.id.txt_ewallet);
        final TextView Tv_byCards = (TextView) withdrawalAdviceDialog.findViewById(R.id.txt_cards);
        final TextView Tv_grossEarnings = (TextView) withdrawalAdviceDialog.findViewById(R.id.txt_gross_earnings);
        final TextView Tv_feesAndCharges = (TextView) withdrawalAdviceDialog.findViewById(R.id.txt_fees_and_charges);
        final TextView Tv_referralIncome = (TextView) withdrawalAdviceDialog.findViewById(R.id.txt_referral_income);
        final TextView Tv_incentives = (TextView) withdrawalAdviceDialog.findViewById(R.id.txt_incentives);
        final TextView Tv_nettEarn = (TextView) withdrawalAdviceDialog.findViewById(R.id.txt_net_earn);

        Tv_refNo.setText(notificationsList.get(position).getEarningDetail().getRef_no());
        Tv_locationId.setText(notificationsList.get(position).getEarningDetail().getLocation_id());
        Tv_driverId.setText(notificationsList.get(position).getEarningDetail().getDriver_id());
        Tv_companyId.setText(notificationsList.get(position).getEarningDetail().getCompany_id());
        Tv_amount.setText(notificationsList.get(position).getEarningDetail().getCurrency() + " " + notificationsList.get(position).getEarningDetail().getAmount());
        Tv_period.setText(notificationsList.get(position).getEarningDetail().getPeriod());
        Tv_payment.setText(notificationsList.get(position).getEarningDetail().getPayment());
        Tv_completedTrip.setText(notificationsList.get(position).getEarningDetail().getCompleted_trip());
        Tv_paidByCash.setText(notificationsList.get(position).getEarningDetail().getCurrency() + " " + notificationsList.get(position).getEarningDetail().getPaid_by_cash());
        Tv_byWallet.setText(notificationsList.get(position).getEarningDetail().getCurrency() + " " + notificationsList.get(position).getEarningDetail().getBy_wallet());
        Tv_byCards.setText(notificationsList.get(position).getEarningDetail().getCurrency() + " " + notificationsList.get(position).getEarningDetail().getBy_card());
        Tv_grossEarnings.setText(notificationsList.get(position).getEarningDetail().getCurrency() + " " + notificationsList.get(position).getEarningDetail().getGross_earning());
        Tv_feesAndCharges.setText(notificationsList.get(position).getEarningDetail().getFees_charge());
        Tv_nettEarn.setText(notificationsList.get(position).getEarningDetail().getCurrency() + " " + notificationsList.get(position).getEarningDetail().getNett_earn());

        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                withdrawalAdviceDialog.dismiss();
            }
        });

        withdrawalAdviceDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                Lv_notifications.setEnabled(true);

                if (cd.isConnectingToInternet()) {
                    InfoRequest(Iconstant.message_url);
                } else {
                    Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                }
            }
        });

        withdrawalAdviceDialog.show();
        Lv_notifications.setEnabled(false);

    }


    private void MessagePopUp(final int postion) {
        final Dialog dialog = new Dialog(InfoPage.this, R.style.SlideUpDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.message_popup_new);
        CustomTextView title = (CustomTextView) dialog.findViewById(R.id.title);
        CustomTextView description = (CustomTextView) dialog.findViewById(R.id.description);
        ImageView Ok_Profilechange = (ImageView) dialog.findViewById(R.id.ok_profilechange);

        title.setText(notificationsList.get(postion).getTitle().toUpperCase());
        description.setText(notificationsList.get(postion).getDescription().toUpperCase());
        Ok_Profilechange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null) {
                    dialog.dismiss();
                }

            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                Lv_notifications.setEnabled(true);
                /*notificationsList.remove(postion);
                adapter.notifyDataSetChanged();*/
                if (cd.isConnectingToInternet()) {
                    InfoRequest(Iconstant.message_url);
                } else {
                    Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                }
            }
        });

        dialog.show();
        Lv_notifications.setEnabled(false);
    }


    //----------------------Notification List Request------------------------------
    @SuppressWarnings("WrongConstant")
    private void InfoRequest(String Url) {

        showDialog();

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("id", sDriveID);
        jsonParams.put("page", String.valueOf(currentPage));
        jsonParams.put("perPage", perPage);
        jsonParams.put("type", "notification");

        System.out.println("-------------InfoRequest  Url----------------" + Url);


        mRequest = new ServiceRequest(InfoPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------InfoRequest  Response----------------" + response);

                String Sstatus = "", str_NextPage = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");

                        if (response_object.length() > 0) {
                            String totlanotification = response_object.getString("total_record");
                            sessionManager.setInfoBadge(totlanotification);

                            Object check_trans_object = response_object.get("notification");
                            notificationsList = new ArrayList<>();
                            if (check_trans_object instanceof JSONArray) {
                                JSONArray trans_array = response_object.getJSONArray("notification");
                                if (trans_array.length() > 0) {

                                    for (int i = 0; i < trans_array.length(); i++) {
                                        JSONObject trans_object = trans_array.getJSONObject(i);
                                        MessagePojo pojo = new MessagePojo();

                                        String inputDate = trans_object.getString("date");
                                        String outputDate = inputDate;
                                        try {
                                            Date date = input.parse(inputDate);   // parse input
                                            outputDate = output.format(date);     // format output
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }


                                        pojo.setDate(outputDate);
                                        pojo.setIMage(trans_object.getString("image"));
                                        pojo.setTitle(trans_object.getString("title"));
                                        pojo.setDescription(trans_object.getString("description"));
                                        pojo.setNotification_id(trans_object.getString("notification_id"));
                                        pojo.setType(trans_object.getString("type"));

                                        if (trans_object.getString("type").equalsIgnoreCase("earning")) {

                                            JSONObject jobjEarning = trans_object.getJSONObject("earning_statement");
                                            if (jobjEarning.length() > 0) {

                                                MessagePojo.EarningDetail earningDetail = pojo.new EarningDetail();
                                                earningDetail.setRef_no(jobjEarning.getString("ref_no"));
                                                earningDetail.setLocation_id(jobjEarning.getString("location_id"));
                                                earningDetail.setDriver_id(jobjEarning.getString("driver_id"));
                                                earningDetail.setCompany_id(jobjEarning.getString("company_id"));
                                                earningDetail.setAmount(jobjEarning.getString("amount"));
                                                earningDetail.setPeriod(jobjEarning.getString("period"));
                                                earningDetail.setPayment(jobjEarning.getString("payment"));
                                                earningDetail.setCompleted_trip(jobjEarning.getString("completed_trip"));
                                                earningDetail.setPaid_by_cash(jobjEarning.getString("paid_by_cash"));
                                                earningDetail.setBy_card(jobjEarning.getString("by_card"));
                                                earningDetail.setBy_wallet(jobjEarning.getString("by_wallet"));
                                                earningDetail.setGross_earning(jobjEarning.getString("gross_earning"));
                                                earningDetail.setNett_earn(jobjEarning.getString("nett_earn"));
                                                earningDetail.setCurrency(jobjEarning.getString("currency"));
                                                earningDetail.setFees_charge(jobjEarning.getString("fees_charge"));

                                                pojo.setEarningDetail(earningDetail);

                                            }

                                        }

                                        notificationsList.add(pojo);
                                    }
                                    isDataPresent = true;
                                } else {
                                    isDataPresent = false;
                                }
                            } else {
                                isDataPresent = false;
                            }
                            currentPage = Integer.parseInt(response_object.getString("current_page"));
                            str_NextPage = response_object.getString("next_page");
                            perPage = response_object.getString("perPage");
                        }
                    }

                    if (currentPage == 1) {
                        Iv_previous.setVisibility(View.GONE);
                    } else {
                        Iv_previous.setVisibility(View.VISIBLE);
                    }

                    if (Sstatus.equalsIgnoreCase("1")) {

                        if (isDataPresent) {

                            Tv_emptyView.setVisibility(View.GONE);
                            Lv_notifications.setVisibility(View.VISIBLE);
                            adapter = new InfoAdapter(InfoPage.this, notificationsList);
                            Lv_notifications.setAdapter(adapter);

                            if (str_NextPage.length() > 0) {
                                Iv_next.setVisibility(View.VISIBLE);
                            } else {
                                Iv_next.setVisibility(View.GONE);
                            }

                        } else {
                            Tv_emptyView.setVisibility(View.VISIBLE);
                            Lv_notifications.setVisibility(View.GONE);
                            Iv_next.setVisibility(View.GONE);
                        }
                    } else {
                        String Sresponse = object.getString("response");
                        Alert(getResources().getString(R.string.alert_label_title), Sresponse);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                dismissDialog();

            }

            @Override
            public void onErrorListener() {
                dismissDialog();
            }
        });

    }

    private void ViewNotificationRequest(String Url, final int position) {

        showDialog();

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("id", sDriveID);
        jsonParams.put("notification_id", notificationsList.get(position).getNotification_id());

        System.out.println("-------------ViewNotificationRequest Url----------------" + Url);
        System.out.println("-------------ViewNotificationRequest Params----------------" + jsonParams);


        mRequest = new ServiceRequest(InfoPage.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------ViewNotificationRequest Response----------------" + response);

                dismissDialog();

                String Sstatus = "";

                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
//                    String sResponse = object.getString("response");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        if (notificationsList.get(position).getType().equalsIgnoreCase("earning")) {
                            withdrawalAdvice(position);
                        } else {
                            MessagePopUp(position);
                        }
                    } /*else {
                        Alert(getResources().getString(R.string.action_error), sResponse);
                    }*/

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


            }

            @Override
            public void onErrorListener() {
                dismissDialog();
            }
        });

    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(InfoPage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void showDialog() {

        dialog = new Dialog(InfoPage.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

    }

    private void dismissDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }


}


