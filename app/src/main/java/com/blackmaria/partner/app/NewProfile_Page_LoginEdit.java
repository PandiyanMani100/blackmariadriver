package com.blackmaria.partner.app;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.FragmentActivityHockeyApp;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.GPSTracker;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.CustomEdittext;
import com.blackmaria.partner.latestpackageview.widgets.CustomTextView;
import com.blackmaria.partner.latestpackageview.widgets.CustomTextViewForm;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.blackmaria.partner.latestpackageview.Countrycodepicker.countrycodepicker.CountryPicker;
import com.blackmaria.partner.latestpackageview.Countrycodepicker.countrycodepicker.CountryPickerListener;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;

/**
 * Created by Muruganantham on 12/28/2017.
 */

@SuppressLint("Registered")
public class NewProfile_Page_LoginEdit extends FragmentActivityHockeyApp implements View.OnClickListener {
    private ServiceRequest mRequest;
    private ConnectionDetector cd;
    private boolean isInternetPresent = false;
    private SessionManager session;
    private GPSTracker gpsTracker;
    private RelativeLayout loginEditLyHeader;
    private ImageView cancelLayout;
    private LinearLayout myloginDialogFormWholeLayout, myloginDialogOtpLayout;
    private CustomEdittext myloginDialogFormEmailidEdittext;
    private CustomTextViewForm myloginDialogFormCountryCodeTextview;
    private CustomEdittext myloginDialogFormMobileNoEdittext;
    private CustomTextViewForm myloginDialogFormPasswordTextview;
    private CustomEdittext myloginDialogFormPasswordEdittext;
    public static CustomTextView myloginDialogFormPincodeEdittext;
    private RelativeLayout profileLoginInfoBtn;
    private LinearLayout loginEditOtp;
    private CustomEdittext myloginDialogOtp;
    private RelativeLayout loginDetailsOtpConfirm;
    private SmoothProgressBar profileLoadingProgressbar;

    private CountryPicker picker;
    private String UserID = "";
    private String gcmID = "";
    private String sAuthKey = "";
    String countrycode = "", mobilenumber = "", email = "", pincode = "";
    public static String newpincode = "";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_login_details_edit_layout);
        initialize();
        OtherClickEvents();
    }

    private void initialize() {

        session = new SessionManager(NewProfile_Page_LoginEdit.this);
        cd = new ConnectionDetector(NewProfile_Page_LoginEdit.this);
        isInternetPresent = cd.isConnectingToInternet();


        HashMap<String, String> info = session.getUserDetails();
        UserID = info.get(SessionManager.KEY_DRIVERID);

        loginEditLyHeader = (RelativeLayout) findViewById(R.id.login_edit_ly_header);
        cancelLayout = (ImageView) findViewById(R.id.cancel_layout);
        myloginDialogFormWholeLayout = (LinearLayout) findViewById(R.id.mylogin_dialog_form_whole_layout);
        myloginDialogOtpLayout = (LinearLayout) findViewById(R.id.mobile_otp_layout);
        myloginDialogFormEmailidEdittext = (CustomEdittext) findViewById(R.id.mylogin_dialog_form_emailid_edittext);
        myloginDialogFormCountryCodeTextview = (CustomTextViewForm) findViewById(R.id.mylogin_dialog_form_country_code_textview);
        myloginDialogFormMobileNoEdittext = (CustomEdittext) findViewById(R.id.mylogin_dialog_form_mobile_no_edittext);
        myloginDialogFormPasswordTextview = (CustomTextViewForm) findViewById(R.id.mylogin_dialog_form_password_textview);
        myloginDialogFormPasswordEdittext = (CustomEdittext) findViewById(R.id.mylogin_dialog_form_password_edittext);
        myloginDialogFormPincodeEdittext = (CustomTextView) findViewById(R.id.mylogin_dialog_form_pincode_edittext);
        myloginDialogFormPincodeEdittext.setOnClickListener(this);
        profileLoginInfoBtn = (RelativeLayout) findViewById(R.id.profile_login_info_btn);
        loginEditOtp = (LinearLayout) findViewById(R.id.login_edit_otp);
        myloginDialogOtp = (CustomEdittext) findViewById(R.id.mylogin_dialog_otp);
        loginDetailsOtpConfirm = (RelativeLayout) findViewById(R.id.login_details_otp_confirm);
        profileLoadingProgressbar = (SmoothProgressBar) findViewById(R.id.profile_loading_progressbar);
        picker = CountryPicker.newInstance(getResources().getString(R.string.select_country_lable));

        cancelLayout.setOnClickListener(this);
        loginDetailsOtpConfirm.setOnClickListener(this);
        profileLoginInfoBtn.setOnClickListener(this);


        Intent in = getIntent();
        countrycode = in.getStringExtra("countrycode");
        mobilenumber = in.getStringExtra("mobilenumber");
        email = in.getStringExtra("email");
        pincode = in.getStringExtra("pincode");
        session.setSecurePin(pincode);

        myloginDialogFormCountryCodeTextview.setText(countrycode);
        myloginDialogFormMobileNoEdittext.setText(mobilenumber);
        myloginDialogFormEmailidEdittext.setText(email);
        myloginDialogFormPincodeEdittext.setText(pincode);


    }

    @Override
    public void onClick(View v) {
        if (v == cancelLayout) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (v == loginDetailsOtpConfirm) {

            if (myloginDialogOtp.getText().toString().trim().equalsIgnoreCase(session.getOtpPin())) {
                LoginUpdate();
            } else {
                Alert(getResources().getString(R.string.alert_label_title), "Entered Invalid Otp");
            }
        } else if (v == profileLoginInfoBtn) {
            CloseKeyboardNew();
            OtpUpdate();

        }else if(v == myloginDialogFormPincodeEdittext){
            Intent i = new Intent(NewProfile_Page_LoginEdit.this,NewProfile_Page_Login_Pincode_Edit.class);
            i.putExtra("oldpincode",pincode);
            i.putExtra("countrycode",countrycode);
            i.putExtra("email",email);
            i.putExtra("phonemnumber",mobilenumber);
            startActivity(i);
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }
    }

    private void OtherClickEvents() {
        myloginDialogFormCountryCodeTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });

        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {
                picker.dismiss();
                myloginDialogFormCountryCodeTextview.setText(dialCode);
                // close keyboard
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(myloginDialogFormCountryCodeTextview.getWindowToken(), 0);
            }
        });
    }

    private void OtpUpdate() {
        try {
            myloginDialogFormEmailidEdittext.setText(Html.fromHtml(myloginDialogFormEmailidEdittext.getText().toString()).toString());
            myloginDialogFormMobileNoEdittext.setText(Html.fromHtml(myloginDialogFormMobileNoEdittext.getText().toString()).toString());
            myloginDialogFormPincodeEdittext.setText(Html.fromHtml(myloginDialogFormPincodeEdittext.getText().toString()).toString());

            if (myloginDialogFormEmailidEdittext.getText().toString().length() == 0) {
                erroredit(myloginDialogFormEmailidEdittext, getResources().getString(R.string.profile_label_alert_email));
            } else if (myloginDialogFormCountryCodeTextview.getText().toString().equalsIgnoreCase("code")) {
                erroredit(myloginDialogFormMobileNoEdittext, getResources().getString(R.string.login_page_alert_country_code));
            } else if (myloginDialogFormMobileNoEdittext.getText().toString().length() == 0) {
                erroredit(myloginDialogFormMobileNoEdittext, "Please enter the phone number");
            } else if (!isValidPhoneNumber(myloginDialogFormMobileNoEdittext.getText().toString())) {
                erroredit(myloginDialogFormMobileNoEdittext, getResources().getString(R.string.profile_lable_error_mobile));
            } else if (myloginDialogFormPincodeEdittext.getText().toString().length() == 0) {
                AlertError(getResources().getString(R.string.action_error), "Pincode can't be empty");
            } else {
                cd = new ConnectionDetector(NewProfile_Page_LoginEdit.this);
                isInternetPresent = cd.isConnectingToInternet();

                if (!myloginDialogFormMobileNoEdittext.getText().toString().equalsIgnoreCase(mobilenumber)) {
                    myloginDialogOtpLayout.setVisibility(View.VISIBLE);
                    if (isInternetPresent) {
                        HashMap<String, String> jsonParams = new HashMap<String, String>();
                        jsonParams.put("driver_id", UserID);
                        jsonParams.put("country_code", myloginDialogFormCountryCodeTextview.getText().toString());
                        jsonParams.put("phone_number", myloginDialogFormMobileNoEdittext.getText().toString());
                        PostRequest_updateOtp_info(Iconstant.User_Otp_LoginUpdate, jsonParams);
                    } else {
                        Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
                    }
                } else {
                    LoginUpdate();
                    myloginDialogOtpLayout.setVisibility(View.GONE);
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void LoginUpdate() {
        try {
            cd = new ConnectionDetector(NewProfile_Page_LoginEdit.this);
            isInternetPresent = cd.isConnectingToInternet();
            if (myloginDialogFormEmailidEdittext.getText().toString().length() == 0) {
                AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.profile_label_alert_email));
            } else if (myloginDialogFormCountryCodeTextview.getText().toString().equalsIgnoreCase("code")) {
                AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.login_page_alert_country_code));
            } else if (myloginDialogFormMobileNoEdittext.getText().toString().length() == 0) {
                AlertError(getResources().getString(R.string.action_error), "Please enter the phone number");
            } else if (!isValidPhoneNumber(myloginDialogFormMobileNoEdittext.getText().toString())) {
                AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.profile_lable_error_mobile));
            } else if (myloginDialogFormPincodeEdittext.getText().toString().length() == 0) {
                AlertError(getResources().getString(R.string.action_error), "Pincode can't be empty");
            } else {
                if (isInternetPresent) {
                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    jsonParams.put("mode", "update");
                    jsonParams.put("driver_id", UserID);
                    jsonParams.put("email", myloginDialogFormEmailidEdittext.getText().toString());
                    jsonParams.put("country_code", myloginDialogFormCountryCodeTextview.getText().toString());
                    jsonParams.put("phone_number", myloginDialogFormMobileNoEdittext.getText().toString());
                    if (!newpincode.equalsIgnoreCase("")) {
                        jsonParams.put("passcode", newpincode);
                    } else {
                        jsonParams.put("passcode", session.getSecurePin());
                    }
                    PostRequest_updatelogin_info(Iconstant.userprofile_update_login_info_url, jsonParams);
                } else {
                    Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void PostRequest_updateOtp_info(String Url, final HashMap<String, String> jsonParams) {
        profileLoadingProgressbar.setVisibility(View.VISIBLE);
        System.out.println("--------------PostRequest_updateOtp_info-------------------" + Url);
        System.out.println("--------------PostRequest_updateOtp_info jsonParams-------------------" + jsonParams);
        mRequest = new ServiceRequest(NewProfile_Page_LoginEdit.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("--------------PostRequest_updateOtp_info reponse-------------------" + response);
                String Sstatus = "", Smessage = "", otp_status = "", Up_home_passcode = "", otp = "", Up_login_phone_no = "", Up_login_country_code = "", Up_home_pho_no = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        Smessage = object.getString("response");
                        Up_login_phone_no = object.getString("phone_number");
                        Up_login_country_code = object.getString("country_code");
                        otp_status = object.getString("otp_status");
                        otp = object.getString("otp");
                        session.setOtpstatusAndPin(otp_status, otp);
                    } else {
                        Smessage = object.getString("response");
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    profileLoadingProgressbar.setVisibility(View.GONE);
                }
                if (Sstatus.equalsIgnoreCase("1")) {

                    if (otp_status.equalsIgnoreCase("development")) {
                        myloginDialogOtp.setText(otp);
                    } else {
                        myloginDialogOtp.setText("");
                    }

                } else {
                    Alert(getResources().getString(R.string.action_error), Smessage);
                    profileLoadingProgressbar.setVisibility(View.GONE);
                }
                profileLoadingProgressbar.setVisibility(View.GONE);
            }

            @Override
            public void onErrorListener() {
                profileLoadingProgressbar.setVisibility(View.GONE);
            }
        });
    }

    //    --------------------login info update---------------------------
    private void PostRequest_updatelogin_info(String Url, final HashMap<String, String> jsonParams) {

        profileLoadingProgressbar.setVisibility(View.VISIBLE);
        System.out.println("--------------updateProfile-------------------" + Url);
        System.out.println("--------------updateProfile jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(NewProfile_Page_LoginEdit.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("--------------updateProfile reponse-------------------" + response);
                String Sstatus = "", Smessage = "", Up_login_user_name = "", Up_home_passcode = "", Up_home_email_id = "", Up_login_phone_no = "", Up_login_country_code = "", Up_home_pho_no = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        Smessage = object.getString("response");
                        JSONObject jsonObject_userprofile = object.getJSONObject("driver_profile");
                        if (jsonObject_userprofile.length() > 0) {
//                            Up_login_phone_no = jsonObject_userprofile.getString("mobile_number");
//                            Up_login_country_code = jsonObject_userprofile.getString("country_code");
//                            Up_home_email_id = jsonObject_userprofile.getString("email");
                            Up_home_passcode = jsonObject_userprofile.getString("with_pincode");
                            session.setSecurePin(Up_home_passcode);
                        }
                    } else {
                        Smessage = object.getString("response");
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    profileLoadingProgressbar.setVisibility(View.GONE);
                }
                if (Sstatus.equalsIgnoreCase("1")) {
                    Alert1(getResources().getString(R.string.action_success), Smessage);
                } else {
                    Alert(getResources().getString(R.string.action_error), Smessage);
                }
                profileLoadingProgressbar.setVisibility(View.GONE);
            }

            @Override
            public void onErrorListener() {
                profileLoadingProgressbar.setVisibility(View.GONE);
            }
        });
    }

    //-------------------------code to Check Email Validation-----------------------
    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    // validating Phone Number
    public static final boolean isValidPhoneNumber(CharSequence target) {
        if (target == null || TextUtils.isEmpty(target) || target.length() <= 5) {
            return false;
        } else {
            return android.util.Patterns.PHONE.matcher(target).matches();
        }
    }

    //--------------------Code to set error for EditText-----------------------
    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(NewProfile_Page_LoginEdit.this, R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(NewProfile_Page_LoginEdit.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    //--------------Alert Method-----------
    private void Alert1(String title, String alert) {

        final PkDialog mDialog = new PkDialog(NewProfile_Page_LoginEdit.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent local = new Intent();
                local.setAction("com.app.PaymentListPage.refreshPage");
                local.putExtra("refresh", "yes");
                sendBroadcast(local);
                finish();
                overridePendingTransition(R.anim.exit, R.anim.enter);
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }
    private void AlertError(String title, String message) {
        String msg = title + "\n" + message;
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        Snackbar bar = Snackbar.with(NewProfile_Page_LoginEdit.this); // context
        bar.addView(view);
        bar.colorResource(R.color.dark_red);
        bar.position(Snackbar.SnackbarPosition.TOP);
        bar.type(SnackbarType.MULTI_LINE); // Set is as a multi-line snackbar
//        bar.text(msg); // text to be displayed
        bar.duration(Snackbar.SnackbarDuration.LENGTH_SHORT); // make it shorter
        bar.animation(true); // don't animate it
        SnackbarManager.show(bar, NewProfile_Page_LoginEdit.this);
    }
    private void CloseKeyboardNew() {
        try {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow((null == getCurrentFocus()) ? null : getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
