package com.blackmaria.partner.app;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.Pojo.TripSummaryPojo;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.latestpackageview.Adapter.TripSummaryAdapter;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user127 on 18-05-2017.
 */

public class TripSummary extends ActivityHockeyApp implements View.OnClickListener {

    private ImageView Iv_back;
    private ExpandableHeightListView Lv_tripDetails, Lv_bookingDetails;
    private RelativeLayout RL_fare, RL_close;
    private TextView Tv_fareInfo;

    private SessionManager sessionManager;
    private ConnectionDetector cd;
    private Dialog dialog;
    private ServiceRequest mRequest;
    private TripSummaryAdapter tripAdapter, bookingAdapter, fareAdapter;

    private String sDriverID = "", sCurrency = "", rideID = "", sFromPage = "";
    private ArrayList<TripSummaryPojo> listTripDetails, listBookingDetails, listFareDetails;
    private boolean isTripDetailsAvailable = false, isBookingDetailsAvailable = false, isFareDetailsAvailable = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trip_summary);

        initialize();

    }

    //------------------Initializing Variables-----------------
    private void initialize() {

        sessionManager = new SessionManager(TripSummary.this);
        cd = new ConnectionDetector(TripSummary.this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        listTripDetails = new ArrayList<>();
        listBookingDetails = new ArrayList<>();
        listFareDetails = new ArrayList<>();

        Iv_back = (ImageView) findViewById(R.id.img_back);
        Lv_tripDetails = (ExpandableHeightListView) findViewById(R.id.lst_trip_details);
        Lv_bookingDetails = (ExpandableHeightListView) findViewById(R.id.lst_booking_details);
        Tv_fareInfo = (TextView) findViewById(R.id.txt_label_fare_information);
        RL_fare = (RelativeLayout) findViewById(R.id.RL_fare);
        RL_close = (RelativeLayout) findViewById(R.id.RL_close);

        getIntentData();

        if (cd.isConnectingToInternet()) {
            GetTripSummary(Iconstant.complete_trip_detail_url);
        } else {
            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }

        Iv_back.setOnClickListener(this);
        RL_fare.setOnClickListener(this);
        RL_close.setOnClickListener(this);

    }

    private void getIntentData() {

        Intent intent = getIntent();
        if (intent != null) {
            rideID = intent.getStringExtra("rideID");
            sFromPage = intent.getStringExtra("fromPage");
        }

    }

    private void showFareInformation() {

        final Dialog fareInfoDialog = new Dialog(TripSummary.this, R.style.SlideUpDialog);
        fareInfoDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        fareInfoDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        fareInfoDialog.setCancelable(true);
        fareInfoDialog.setContentView(R.layout.alert_fare_information);

        ExpandableHeightListView Lv_fareDetails = (ExpandableHeightListView) fareInfoDialog.findViewById(R.id.lst_fare_details);
        Button btn_close = (Button) fareInfoDialog.findViewById(R.id.btn_close);

//        fareAdapter = new TripSummaryAdapter(TripSummary.this, sCurrency, Color.BLACK, listFareDetails);
//        Lv_fareDetails.setAdapter(fareAdapter);

        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fareInfoDialog != null && fareInfoDialog.isShowing()) {
                    fareInfoDialog.dismiss();
                }
            }
        });

        fareInfoDialog.show();

    }

    //-------------Trip Summary List Request-----------
    @SuppressLint("WrongConstant")
    private void GetTripSummary(String Url) {

        dialog = new Dialog(TripSummary.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        jsonParams.put("ride_id", rideID);

        System.out.println("--------------TripSummary Url-------------------" + Url);
        System.out.println("--------------TripSummary jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(TripSummary.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------TripSummary Response-------------------" + response);
                String status = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {
                            JSONObject jsonObject = object.getJSONObject("response");

                            JSONObject jobjDetails = jsonObject.getJSONObject("details");

                            sCurrency = jobjDetails.getString("currency");

                            Object objTrip = jobjDetails.get("tripArr");
                            if (objTrip instanceof JSONArray) {

                                JSONArray jarTrip = jobjDetails.getJSONArray("tripArr");

                                if (jarTrip.length() > 0) {

                                    listTripDetails = new ArrayList<>();
                                    for (int i = 0; i < jarTrip.length(); i++) {

                                        JSONObject jobjTrip = jarTrip.getJSONObject(i);

                                        String title = jobjTrip.getString("title");
                                        String value = jobjTrip.getString("value");
                                        String currency_status = jobjTrip.getString("currency_status");

                                        listTripDetails.add(new TripSummaryPojo(title, value, currency_status));

                                    }

                                    isTripDetailsAvailable = true;

                                }

                            }


                            Object objFare = jobjDetails.get("fareArrL");
                            if (objFare instanceof JSONArray) {

                                JSONArray jarFare = jobjDetails.getJSONArray("fareArrL");

                                if (jarFare.length() > 0) {

                                    listFareDetails = new ArrayList<>();
                                    for (int i = 0; i < jarFare.length(); i++) {

                                        JSONObject jobjFare = jarFare.getJSONObject(i);

                                        String title = jobjFare.getString("title");
                                        String value = jobjFare.getString("value");
                                        String currency_status = jobjFare.getString("currency_status");

                                        listFareDetails.add(new TripSummaryPojo(title, value, currency_status));

                                    }

                                    isFareDetailsAvailable = true;

                                }

                            }

                            Object objBooking = jobjDetails.get("booking_summary");
                            if (objBooking instanceof JSONArray) {

                                JSONArray jarBooking = jobjDetails.getJSONArray("booking_summary");

                                if (jarBooking.length() > 0) {

                                    listBookingDetails = new ArrayList<>();
                                    for (int i = 0; i < jarBooking.length(); i++) {

                                        JSONObject jobjBooking = jarBooking.getJSONObject(i);

                                        String title = jobjBooking.getString("title");
                                        String value = jobjBooking.getString("value");
                                        String currency_status = jobjBooking.getString("currency_status");

                                        listBookingDetails.add(new TripSummaryPojo(title, value, currency_status));

                                    }

                                    isBookingDetailsAvailable = true;

                                }

                            }


                        } else {
                            String sResponse = object.getString("response");
                            Alert(getResources().getString(R.string.action_error), sResponse);
                        }

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (status.equalsIgnoreCase("1")) {

//                    if (isTripDetailsAvailable) {
//                        tripAdapter = new TripSummaryAdapter(TripSummary.this, sCurrency, Color.WHITE, listTripDetails);
//                        Lv_tripDetails.setAdapter(tripAdapter);
//                        Lv_tripDetails.setExpanded(true);
//                    }

//                    if (isBookingDetailsAvailable) {
//                        bookingAdapter = new TripSummaryAdapter(TripSummary.this, sCurrency, Color.WHITE, listBookingDetails);
//                        Lv_bookingDetails.setAdapter(bookingAdapter);
//                        Lv_bookingDetails.setExpanded(true);
//                    }


                    if (isFareDetailsAvailable) {
                        RL_fare.setVisibility(View.VISIBLE);
                    } else {
                        RL_fare.setVisibility(View.GONE);
                    }

                    /*if (sFromPage.equalsIgnoreCase("MyTripTodayDetail")) {

                    } else if (sFromPage.equalsIgnoreCase("TripPage")) {
                        RL_fare.setVisibility(View.GONE);
                    }*/

                }

                if (dialog != null) {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    //-----------Back Key Pressed-----------------------
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
            return true;
        }
        return false;
    }

    //--------------Alert Function------------------
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(TripSummary.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
            }
        });
        mDialog.show();
    }

    @Override
    public void onClick(View view) {

        if (view == Iv_back) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        } else if (view == RL_fare) {

            if (isFareDetailsAvailable) {
                showFareInformation();
            } else {
                Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.trip_summary_fare_info_not_available));
            }

        } else if (view == RL_close) {

            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);

        }

    }
}
