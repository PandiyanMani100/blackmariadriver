package com.blackmaria.partner.app;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blackmaria.partner.Pojo.WalletMoneyPojo;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.CurrencySymbolConverter;
import com.blackmaria.partner.Utils.ExpandableHeightGridView;
import com.blackmaria.partner.Utils.ImageLoader;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Dashboard_constrain;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.RechargeAmountConstrain;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Transfer.Transfermobilenumbersearch;
import com.blackmaria.partner.latestpackageview.widgets.CustomTextView;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class WalletMoneyPage1 extends AppCompatActivity implements ApIServices.completelisner {
    private RelativeLayout back, Rl_enter_pin, Rl_otp;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private SessionManager session;
    private String sDriverID = "";
    private ServiceRequest mRequest;
    private ImageView Rl_drawer;
    private TextView Tv_available_balnce, Tv_currency;
    private TextView tv_rideamount, credit_tv_rideamount;

    RelativeLayout Tv_confirm;
    private RelativeLayout Rl_debit, Rl_credit;
    public ExpandableHeightGridView GV_payment_list;

    String sFromPage = "";

    private boolean isRechargeAvailable = false;
    private String Sauto_charge_status = "";
    private String Str_currentbalance = "", min_wallet_amount = "", Str_minimum_amt = "", Str_maximum_amt = "", Str_midle_amt = "", ScurrencySymbol = "";
    ArrayList<WalletMoneyPojo> paymnet_list = new ArrayList<WalletMoneyPojo>();

    private ProgressBar apiload;
    private Dialog dialog;

    private ImageLoader imageLoader;
    private ImageView Iv_home, popup_close_wallet, Im_backpress;
    public static String Scurrency = "", sSecurePin = "";

    @Override
    public void sucessresponse(String response) {
        dialog.dismiss();
        apiload.setVisibility(View.INVISIBLE);
        String Sstatus = "", Scurrency_code = "", Scurrentbalance = "", Str_currentMonthCredit = "", Str_currentMonthDebit = "", Str_monthYear = "";

        try {
            JSONObject object = new JSONObject(response);
            Sstatus = object.getString("status");
            Sauto_charge_status = object.getString("auto_charge_status");
            if (Sstatus.equalsIgnoreCase("1")) {
                JSONObject response_object = object.getJSONObject("response");
                if (response_object.length() > 0) {

                    Scurrentbalance = response_object.getString("current_balance");
                    Str_currentbalance = response_object.getString("current_balance");
                    min_wallet_amount = response_object.getString("min_wallet_amount ");
                    ScurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(response_object.getString("currency"));
                    Scurrency = response_object.getString("currency");
                    Str_currentMonthCredit = response_object.getString("current_month_credit");
                    Str_currentMonthDebit = response_object.getString("current_month_debit");
                    Str_monthYear = response_object.getString("month_year");
                    Object check_recharge_boundary_object = response_object.get("recharge_boundary");
                    if (check_recharge_boundary_object instanceof JSONObject) {
                        JSONObject recharge_object = response_object.getJSONObject("recharge_boundary");
                        if (recharge_object.length() > 0) {
                            isRechargeAvailable = true;
                        } else {
                            isRechargeAvailable = false;
                        }
                    }

                    Object check_payment_list_object = response_object.get("payment_list");
                    if (check_payment_list_object instanceof JSONArray) {
                        JSONArray payment_list_array = response_object.getJSONArray("payment_list");

                        if (payment_list_array.length() > 0) {
                            paymnet_list.clear();
                            for (int i = 0; i < payment_list_array.length(); i++) {
                                JSONObject payment_list_object = payment_list_array.getJSONObject(i);
                                WalletMoneyPojo walletMoneyPojo = new WalletMoneyPojo();
                                walletMoneyPojo.setPayment_code(payment_list_object.getString("code"));
                                walletMoneyPojo.setPaymnet_name(payment_list_object.getString("name"));
                                walletMoneyPojo.setPayment_active_img(payment_list_object.getString("icon"));
                                walletMoneyPojo.setPayment_normal_img(payment_list_object.getString("inactive_icon"));
                                paymnet_list.add(walletMoneyPojo);
                            }
                        }
                    }

                }
            } else {
                isRechargeAvailable = false;
            }
            if (Sstatus.equalsIgnoreCase("1") && isRechargeAvailable) {
                session.createWalletAmount(ScurrencySymbol + " " + Str_currentbalance);
                Tv_available_balnce.setText(ScurrencySymbol + " " + Scurrentbalance);
                tv_rideamount.setText(ScurrencySymbol + " " + Str_currentMonthDebit);
                credit_tv_rideamount.setText(ScurrencySymbol + " " + Str_currentMonthCredit);
            } else {
                String Sresponse = object.getString("response");
                Alert(getResources().getString(R.string.alert_label_title), Sresponse);
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void errorreponse() {
        dialog.dismiss();
        apiload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void jsonexception() {
        dialog.dismiss();
        apiload.setVisibility(View.INVISIBLE);
    }

    public class RefreshReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.package.ACTION_CLASS_CABILY_MONEY_REFRESH")) {
                if (isInternetPresent) {
                    postRequest_WalletMoneyPage1(Iconstant.wallet_money_url);
                } else {
                    Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
                }
            }
        }
    }

    private RefreshReceiver refreshReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_wallet_money_page);
        initialize();

        Im_backpress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sFromPage.equalsIgnoreCase("GCMPushNotification")) {
                    Intent intent = new Intent(WalletMoneyPage1.this, Dashboard_constrain.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                } else {
                    Intent intent = new Intent(WalletMoneyPage1.this, Dashboard_constrain.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                }
            }
        });


        Rl_debit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WalletMoneyPage1.this, CreditDebitStatement.class);
                intent.putExtra("type", "debit");
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        Rl_credit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WalletMoneyPage1.this, CreditDebitStatement.class);
                intent.putExtra("type", "credit");
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });


        Tv_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WalletMoneyPage1.this, RechargeAmountConstrain.class);
                startActivity(intent);
            }
        });


        Rl_enter_pin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                withdrawOrTransfer();
            }
        });
    }


    private void initialize() {
        session = new SessionManager(WalletMoneyPage1.this);
        cd = new ConnectionDetector(WalletMoneyPage1.this);
        isInternetPresent = cd.isConnectingToInternet();
        imageLoader = new ImageLoader(WalletMoneyPage1.this);
        apiload = findViewById(R.id.apiload);
        Tv_available_balnce = (TextView) findViewById(R.id.wallet_page_available_balance_textview);
        Rl_debit = (RelativeLayout) findViewById(R.id.wallet_page_debit_layout);
        Rl_credit = (RelativeLayout) findViewById(R.id.wallet_page_credit_layout);
        Im_backpress = (ImageView) findViewById(R.id.img_back);

        Tv_confirm = (RelativeLayout) findViewById(R.id.wallet_page_confirm_textview);
        tv_rideamount = (CustomTextView) findViewById(R.id.tv_rideamount);
        credit_tv_rideamount = (CustomTextView) findViewById(R.id.credit_tv_rideamount);

        Typeface tf1 = Typeface.createFromAsset(getAssets(), "fonts/RobotoCondensed-Regular.ttf");
        credit_tv_rideamount.setTypeface(tf1, Typeface.NORMAL);
        tv_rideamount.setTypeface(tf1, Typeface.NORMAL);
        Rl_enter_pin = (RelativeLayout) findViewById(R.id.wallet_enter_pin);

        HashMap<String, String> user = session.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        sSecurePin = session.getSecurePin();
        // -----code to refresh drawer using broadcast receiver-----
        refreshReceiver = new RefreshReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_CLASS_WALLET_MONEY_REFRESH");
        intentFilter.addAction("com.package.ACTION_WALLET_WITHDRAWAL_SUCCESS");
        intentFilter.addAction("com.package.ACTION_WALLET_RELOAD_SUCCESS");
        registerReceiver(refreshReceiver, intentFilter);
        Rl_drawer = (ImageView) findViewById(R.id.indicating_image);
        if (isInternetPresent) {
            postRequest_WalletMoneyPage1(Iconstant.wallet_money_url);
        } else {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
        }

    }


    private void withdrawOrTransfer() {

        final Dialog withdrawOrTransferDialog = new Dialog(WalletMoneyPage1.this, R.style.DialogAnimation);
        withdrawOrTransferDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        withdrawOrTransferDialog.setContentView(R.layout.new_cloud_transfer_withdraw_popup);
        withdrawOrTransferDialog.setCanceledOnTouchOutside(true);
        withdrawOrTransferDialog.setCancelable(false);
        withdrawOrTransferDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final RelativeLayout RL_withdraw = (RelativeLayout) withdrawOrTransferDialog.findViewById(R.id.RL_withdrawal);
        final RelativeLayout RL_transfer = (RelativeLayout) withdrawOrTransferDialog.findViewById(R.id.RL_transfer);
        final Button Btn_close = (Button) withdrawOrTransferDialog.findViewById(R.id.btn_close);
        final ImageView close_Iv = (ImageView) withdrawOrTransferDialog.findViewById(R.id.closeimage);
        close_Iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                withdrawOrTransferDialog.dismiss();
            }
        });
        RL_withdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    double minwallet = Double.parseDouble(min_wallet_amount);
                    double currentbalance = Double.parseDouble(Str_currentbalance);
                    System.out.println("....."+minwallet+" "+currentbalance);
                    long minwallets =  Math.round(minwallet);
                    long currentbalances =  Math.round(currentbalance);

                    if (currentbalances >= minwallets) {
                        withdrawOrTransferDialog.dismiss();
                        Intent intent = new Intent(WalletMoneyPage1.this, NewCloudMoneyWithDrawHome.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                    } else {
                        withdrawOrTransferDialog.dismiss();
                        Alert("Error", "Your wallet amount is less then of minimum balance. Kindly recharge to withdrawl");
                    }
                } catch (Exception e) {
                    Log.d("Exception", "onClick: " + e.getMessage());
                }

            }
        });

        RL_transfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                withdrawOrTransferDialog.dismiss();
//                Intent intent = new Intent(WalletMoneyPage1.this, NewCloudMoneyTransferHome.class);
                Intent intent = new Intent(WalletMoneyPage1.this, Transfermobilenumbersearch.class);
                startActivity(intent);
            }
        });

        Btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                withdrawOrTransferDialog.dismiss();
            }
        });

        withdrawOrTransferDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                Rl_enter_pin.setClickable(true);
            }
        });

        withdrawOrTransferDialog.show();
        Rl_enter_pin.setClickable(false);

    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(WalletMoneyPage1.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    //-----------------------Cabily Money Post Request-----------------
    private void postRequest_WalletMoneyPage1(String Url) {

        apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(this, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        ApIServices apIServices = new ApIServices(this, WalletMoneyPage1.this);
        apIServices.dooperation(Url, jsonParams);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {
            onBackPressed();
            return true;
        }
        return false;
    }


    @Override
    public void onBackPressed() {
        if (sFromPage.equalsIgnoreCase("GCMPushNotification")) {
            Intent intent = new Intent(WalletMoneyPage1.this, Dashboard_constrain.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }
        finish();
        overridePendingTransition(R.anim.enter, R.anim.exit);

    }

    @Override
    public void onDestroy() {
        // Unregister the logout receiver
        unregisterReceiver(refreshReceiver);
        super.onDestroy();
    }
}
