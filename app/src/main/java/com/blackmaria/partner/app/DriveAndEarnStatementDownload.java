package com.blackmaria.partner.app;

import android.app.Dialog;
import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;



public class DriveAndEarnStatementDownload extends AppCompatActivity implements View.OnClickListener {

    private ImageView Iv_back, Iv_profile;
    private TextView Tv_info, Tv_referralCode;
    private RelativeLayout RL_tapToDownload;

    BroadcastReceiver onComplete;
    private ConnectionDetector cd;
    private SessionManager sessionManager;
    private ServiceRequest mRequest;

    private String sDriveID = "", sReferralCode = "", sDriverImage = "", sDownloadLink = "";
    private boolean isDataPresent = false;
    private boolean isReceiversRegistered = false, isAlertShowing = false;
    AccessLanguagefromlocaldb db;
    DownloadManager downloadManager;
    private long myDownloadReference;
    private BroadcastReceiver receiverDownloadComplete, receiverNotificationClicked;
    DownloadManager.Request request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drive_and_earn_download_statement);
        db = new AccessLanguagefromlocaldb(this);

        initialize();

        IntentFilter intentFilter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);

        receiverDownloadComplete = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                long reference = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
                if (myDownloadReference == reference) {
                    DownloadManager.Query query = new DownloadManager.Query();
                    query.setFilterById(reference);
                    Cursor cursor = downloadManager.query(query);

                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);

                    int status = cursor.getInt(columnIndex);

//                    int fileNameIndex = cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_FILENAME);

//                    String filePath = cursor.getString(fileNameIndex);

//                    int columnReason = cursor.getColumnIndex(DownloadManager.COLUMN_REASON);
//
//                    int reason = cursor.getInt(columnReason);

                    switch (status) {
                        case DownloadManager.STATUS_SUCCESSFUL:
                            RL_tapToDownload.setClickable(true);
//                                Toast.makeText(DriveAndEarnStatementDownload.this, getString(R.string.download_success), Toast.LENGTH_SHORT).show();
                            String action = intent.getAction();
                            if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                                AlertDownloadCompleted(getResources().getString(R.string.action_success), getResources().getString(R.string.drive_earn_download_completed), intent);
                            }
                            break;
                        case DownloadManager.STATUS_FAILED:
                            RL_tapToDownload.setClickable(true);
                            Toast.makeText(DriveAndEarnStatementDownload.this, getString(R.string.download_success), Toast.LENGTH_SHORT).show();
                            break;
                        case DownloadManager.STATUS_PAUSED:
                            Toast.makeText(DriveAndEarnStatementDownload.this, getString(R.string.download_paused), Toast.LENGTH_SHORT).show();
                            break;
                        case DownloadManager.STATUS_PENDING:
                            Toast.makeText(DriveAndEarnStatementDownload.this, getString(R.string.download_pending), Toast.LENGTH_SHORT).show();
                            break;
                        case DownloadManager.STATUS_RUNNING:
//                                Toast.makeText(DriveAndEarnStatementDownload.this, getString(R.string.downloading), Toast.LENGTH_SHORT).show();
                            break;
                    }

                }
            }
        };

        IntentFilter filter = new IntentFilter(DownloadManager.ACTION_NOTIFICATION_CLICKED);

        receiverNotificationClicked = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String extraId = DownloadManager.EXTRA_NOTIFICATION_CLICK_DOWNLOAD_IDS;
                long references[] = intent.getLongArrayExtra(extraId);
                for (long reference : references) {
                    // do something with the download file
                }
            }
        };

//
        registerReceiver(receiverNotificationClicked, filter);
        registerReceiver(receiverDownloadComplete, intentFilter);
    }

    private void initialize() {

        sessionManager = new SessionManager(DriveAndEarnStatementDownload.this);
        cd = new ConnectionDetector(DriveAndEarnStatementDownload.this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriveID = user.get(SessionManager.KEY_DRIVERID);

        downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);

        Iv_back = (ImageView) findViewById(R.id.img_back);
        Tv_info = (TextView) findViewById(R.id.txt_label_info);
        Tv_referralCode = (TextView) findViewById(R.id.txt_referral_code);
        RL_tapToDownload = (RelativeLayout) findViewById(R.id.Rl_tap_to_dwonload);
        Iv_profile = (ImageView) findViewById(R.id.img_profile);

        TextView txt_label_tap_to_download = (TextView) findViewById(R.id.txt_label_tap_to_download);
        txt_label_tap_to_download.setText(db.getvalue("tap_to_download"));

        TextView txt_referral_code = (TextView) findViewById(R.id.txt_referral_code);
        txt_referral_code.setText(db.getvalue("referral_code"));

        Iv_back.setOnClickListener(this);
        Tv_info.setOnClickListener(this);
        RL_tapToDownload.setOnClickListener(this);

        getIntentData();
        sReferralCode = getString(R.string.drive_earn_download_label_referral_code) + sReferralCode;
        Tv_referralCode.setText(sReferralCode);
        if (sDriverImage.length() > 0) {
            Picasso.with(DriveAndEarnStatementDownload.this).load(sDriverImage).error(R.drawable.no_user_profile).placeholder(R.drawable.no_user_profile).memoryPolicy(MemoryPolicy.NO_CACHE).resize(150, 150).into(Iv_profile);
        }

        /*if(cd.isConnectingToInternet()){
            DriveAndEarnStatementDownloadRequest("");
        }else{
            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }*/

    }

    private void getIntentData() {

        Intent intent = getIntent();

        if (intent != null) {

            sDriverImage = intent.getStringExtra("driverImage");
            sReferralCode = intent.getStringExtra("referralCode");
            sDownloadLink = intent.getStringExtra("downloadLink");

        }

    }


    @Override
    public void onClick(View view) {

        if (cd.isConnectingToInternet()) {

            if (view == Iv_back) {
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            } else if (view == Tv_info) {
                infoDialog();
            } else if (view == RL_tapToDownload) {

                if (sDownloadLink.length() > 0) {
                    downloadFile(sDownloadLink);
                }
            }

        } else {
            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }

    }


    private void infoDialog() {
        final Dialog withdrawErrorDialog = new Dialog(DriveAndEarnStatementDownload.this, R.style.SlideUpDialog);
        withdrawErrorDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        withdrawErrorDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        withdrawErrorDialog.setCancelable(true);
        withdrawErrorDialog.setContentView(R.layout.alert_withdrawal_errors);

        final TextView Tv_title = (TextView) withdrawErrorDialog.findViewById(R.id.txt_label_withdrawal);
        final TextView Tv_message1 = (TextView) withdrawErrorDialog.findViewById(R.id.txt_alert_message1);
        final TextView Tv_message2 = (TextView) withdrawErrorDialog.findViewById(R.id.txt_alert_message2);
        final ImageView Iv_close = (ImageView) withdrawErrorDialog.findViewById(R.id.img_close);

        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (withdrawErrorDialog != null && withdrawErrorDialog.isShowing()) {
                    withdrawErrorDialog.dismiss();
                }
            }
        });

        withdrawErrorDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                Tv_info.setClickable(true);
            }
        });

        Tv_title.setText(getResources().getString(R.string.drive_earn_download_label_download_statmnt));
        Tv_message1.setText(getResources().getString(R.string.drive_earn_download_label_info));
        Tv_message1.setGravity(Gravity.LEFT);
        Tv_message2.setText(getResources().getString(R.string.drive_earn_download_label_mail));
        Tv_message2.setGravity(Gravity.LEFT);
        Tv_message2.setTextColor(Color.WHITE);

        withdrawErrorDialog.show();
        Tv_info.setClickable(false);

    }


    //--------------Alert----------------
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(DriveAndEarnStatementDownload.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    private void downloadFile(String docLink) {

        if (cd.isConnectingToInternet()) {

            RL_tapToDownload.setClickable(false);
            Uri uri = Uri.parse(docLink);

            String docName = "IncomeStatement_" + System.currentTimeMillis();

            request = new DownloadManager.Request(uri);
            /*MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
            String mimeString = mimeTypeMap.getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(docLink));
            System.out.println("MimeString = "+mimeString);
            request.setMimeType(mimeString);*/
            request.setMimeType("application/pdf");
            request.setDescription("Downloading" + " " + docName);
            request.setTitle(docName);
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, docName);
            request.setVisibleInDownloadsUi(true);
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE);
//            request.setShowRunningNotification(true);
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE | DownloadManager.Request.NETWORK_WIFI);

            myDownloadReference = downloadManager.enqueue(request);
            Toast.makeText(DriveAndEarnStatementDownload.this, getString(R.string.downloading), Toast.LENGTH_SHORT).show();

//            final Handler handler = new Handler();
//            handler.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    if (cd.isConnectingToInternet()) {
//                        Toast.makeText(DriveAndEarnStatementDownload.this, getString(R.string.download_success), Toast.LENGTH_SHORT).show();
//                        RL_tapToDownload.setClickable(true);
//                    }
//                }
//            }, 2000);


            isReceiversRegistered = true;
        } else {
            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }

    }

    private void AlertDownloadCompleted(String title, String alert, final Intent intent) {
        final PkDialog mDialog = new PkDialog(DriveAndEarnStatementDownload.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_yes), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

                long downloadId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0);
                DownloadManager.Query query = new DownloadManager.Query();
                query.setFilterById(downloadId);
                downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                Cursor c = downloadManager.query(query);
                if (c.moveToFirst()) {
                    int columnIndex = c.getColumnIndex(DownloadManager.COLUMN_STATUS);
                    if (DownloadManager.STATUS_SUCCESSFUL == c.getInt(columnIndex)) {
                        String uriString = c.getString(c.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
                        //TODO : Use this local uri and launch intent to open file

//                        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
//                        String mimeString = mimeTypeMap.getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(sDownloadLink));

                        Uri uri = Uri.parse(uriString);

                        try {
                            File file = null;
                            String path = uri.toString();// "/mnt/sdcard/FileName.mp3"
                            try {
                                 file = new File(new URI(path));
                            } catch (URISyntaxException e) {
                                e.printStackTrace();
                            }

                            final Intent intent = new Intent(Intent.ACTION_VIEW)//
                                    .setDataAndType(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ?
                                                    androidx.core.content.FileProvider.getUriForFile(DriveAndEarnStatementDownload.this, getPackageName() + ".provider", file) : Uri.fromFile(file),
                                            "application/pdf").addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);


                            try {
                                startActivity(intent);
                            } catch (ActivityNotFoundException e) {
                                Toast.makeText(DriveAndEarnStatementDownload.this, "No Application available to view PDF", Toast.LENGTH_SHORT).show();
                            }

                        } catch (ActivityNotFoundException e) {
                            Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.cannot_read_file_type));
                        }

                    }
                }

            }
        });
        mDialog.setNegativeButton(getResources().getString(R.string.action_no), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });

        mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                isAlertShowing = false;
            }
        });

        if (!isAlertShowing) {
            mDialog.show();
            isAlertShowing = true;
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (receiverNotificationClicked != null && isReceiversRegistered) {
            unregisterReceiver(receiverNotificationClicked);
        }
        if (receiverDownloadComplete != null && isReceiversRegistered) {
            unregisterReceiver(receiverDownloadComplete);
        }
    }


    /*public void downloadFile(final Context context, String docLink){
//        String destination = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/";
//        String fileName = "IncomeStatement_" + System.currentTimeMillis();
//        destination += fileName;
//        final Uri uri = Uri.parse(docLink);

        //Delete update file if exists
//        File file = new File(docLink);
//        if (file.exists())
//            file.delete();

        //set download manager
           DownloadManager.Request request ;
            Uri uri = Uri.parse(docLink);

//            Calendar cal = Calendar.getInstance();
//            SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
            String docName = "IncomeStatement_" + System.currentTimeMillis();

            request = new DownloadManager.Request(uri);
            *//*MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
            String mimeString = mimeTypeMap.getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(docLink));
            System.out.println("MimeString = "+mimeString);
            request.setMimeType(mimeString);*//*
            request.setMimeType("application/pdf");
            request.setDescription("Downloading" + " " + docName);
            request.setTitle(docName);
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, docName);
            request.setVisibleInDownloadsUi(true);
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE);
//            request.setShowRunningNotification(true);
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE | DownloadManager.Request.NETWORK_WIFI);

            myDownloadReference = downloadManager.enqueue(request);

        // get download service and enqueue file
        final DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        final long downloadId = manager.enqueue(request);
        manager.getMimeTypeForDownloadedFile(downloadId);

        //set BroadcastReceiver to install app when .apk is downloaded
        onComplete = new BroadcastReceiver() {
            public void onReceive(Context ctxt, Intent intent) {
                                RL_tapToDownload.setClickable(true);
//                                Toast.makeText(DriveAndEarnStatementDownload.this, getString(R.string.download_success), Toast.LENGTH_SHORT).show();
                                String action = intent.getAction();
                                if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                                    AlertDownloadCompleted(getResources().getString(R.string.action_success), getResources().getString(R.string.drive_earn_download_completed), intent);
                                }
            }
        };
        //register receiver for when .apk download is compete
        context.registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }*/



}