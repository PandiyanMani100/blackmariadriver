package com.blackmaria.partner.app;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;

import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AsteriskPasswordTransformationMethod;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.blackmaria.partner.latestpackageview.widgets.TextRCLight;
import com.devspark.appmsg.AppMsg;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


/**
 * Created by user144 on 2/27/2018.
 */

public class CloudTransferAmountPinEnter1 extends ActivityHockeyApp implements View.OnClickListener {


    private ImageButton Imb_one, Imb_two, Imb_three, Imb_four, Imb_five, Imb_six, Imb_seven, Imb_tight, Imb_nine, Imb_aero, Imb_minus, Imb_plus;
    private ImageButton Imb_cancel, Imb_enter, Imb_forgetpin, Imb_clear;

    private SessionManager session;
    private boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    Dialog dialog;
    private String sSecurePin = "";
    private String UserID = "", numberamount = "", numberpin = "", user_name = "", user_img = "", user_city = "", user_satus = "", friend_id = "",
            flag = "",            payment_code = "",            amount = "",            paypal_id = "", strCrossBorderStatus = "", mobile_number = "";
    private boolean isAmountEnable = false;
    private boolean isPinEnable = false;


    private EditText password_Edt;
    String status = "", respose = "", user_image = "", city = "", user_status = "", transfer_amount = "", transfer_fee = "", debit_amount = "", currency_transfer = "", received_amount = "", currency_received = "", conversion_rate = "";
    String withdrawlAmount = "", paymentMod = "" ,accHolderName1 = "", bankCode="", holderAccountNumber1 = "";
    private LinearLayout cross_value;
    private TextRCLight userNameTv1, userCityTv1, userStatusTv1, transfer_amount_tv, transferfee_tv, debit_amount_tv, received_amount_tv, conversion_tv, currency_conversion, tv_transfer_title;
    TextView enter_pin_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cloud_transfer_amount_pin);
        initialize();
    }

    private void initialize() {
        session = new SessionManager(CloudTransferAmountPinEnter1.this);
        cd = new ConnectionDetector(CloudTransferAmountPinEnter1.this);
        isInternetPresent = cd.isConnectingToInternet();
        sSecurePin = session.getSecurePin();
        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_DRIVERID);




        Intent in = getIntent();

        if(in.hasExtra("flag")) {
            flag = in.getStringExtra("flag");
            if (flag.equalsIgnoreCase("paypal_withdrawal")) {
                payment_code = in.getStringExtra("payment_code");
                amount = in.getStringExtra("amount");
                paypal_id = in.getStringExtra("paypal_id");
            }
            if (flag.equalsIgnoreCase("cash_withdrawal")) {
                payment_code = in.getStringExtra("payment_code");
                amount = in.getStringExtra("amount");
            }
        }



        withdrawlAmount = getIntent().getStringExtra("amount");
        paymentMod = getIntent().getStringExtra("mode");

        if(in.hasExtra("accHolderName1")) {
            accHolderName1 = getIntent().getStringExtra("accHolderName1");
            holderAccountNumber1 = getIntent().getStringExtra("holderAccountNumber1");
            bankCode = getIntent().getStringExtra("bankCode");

        }

        //  Log.d("strCrossBorderStatus", strCrossBorderStatus);


        Imb_one = (ImageButton) findViewById(R.id.btn_one);
        Imb_two = (ImageButton) findViewById(R.id.btn_two);
        Imb_three = (ImageButton) findViewById(R.id.btn_three);
        Imb_four = (ImageButton) findViewById(R.id.btn_four);
        Imb_five = (ImageButton) findViewById(R.id.btn_five);
        Imb_six = (ImageButton) findViewById(R.id.btn_six);
        Imb_seven = (ImageButton) findViewById(R.id.btn_seven);
        Imb_tight = (ImageButton) findViewById(R.id.btn_tight);
        Imb_nine = (ImageButton) findViewById(R.id.btn_nine);
        Imb_aero = (ImageButton) findViewById(R.id.btn_zero);
        Imb_minus = (ImageButton) findViewById(R.id.btn_minus);
        Imb_plus = (ImageButton) findViewById(R.id.btn_pluse);
        Imb_cancel = (ImageButton) findViewById(R.id.btn_cancel);
        Imb_clear = (ImageButton) findViewById(R.id.btn_clear);
        Imb_forgetpin = (ImageButton) findViewById(R.id.btn_forget);
        Imb_enter = (ImageButton) findViewById(R.id.btn_enter);
        password_Edt = (EditText) findViewById(R.id.cloudmoney_pin);
        password_Edt.setTransformationMethod(new AsteriskPasswordTransformationMethod());

        enter_pin_title = (TextView) findViewById(R.id.enter_pin_title);
        InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(password_Edt.getWindowToken(), 0);
        enter_pin_title.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/italicvladmir.TTF"));

        Imb_one.setOnClickListener(this);
        Imb_two.setOnClickListener(this);
        Imb_three.setOnClickListener(this);
        Imb_four.setOnClickListener(this);
        Imb_five.setOnClickListener(this);
        Imb_six.setOnClickListener(this);
        Imb_seven.setOnClickListener(this);
        Imb_tight.setOnClickListener(this);
        Imb_nine.setOnClickListener(this);
        Imb_aero.setOnClickListener(this);
        Imb_cancel.setOnClickListener(this);
        Imb_clear.setOnClickListener(this);
        Imb_forgetpin.setOnClickListener(this);
        Imb_enter.setOnClickListener(this);
        password_Edt.setOnClickListener(this);


        password_Edt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (hasFocus) {

                    InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    mgr.hideSoftInputFromWindow(password_Edt.getWindowToken(), 0);

                    isPinEnable = true;

                }

            }
        });


    }

    @Override
    public void onClick(View v) {

        if (v == password_Edt) {


            System.out.println("-------kannan--------");

            InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            mgr.hideSoftInputFromWindow(password_Edt.getWindowToken(), 0);

        } else if (v == Imb_one) {

            if (isPinEnable) {

                numberpin = numberpin + "1";
                password_Edt.setText(numberpin);
            }


        } else if (v == Imb_two) {


            if (isPinEnable) {
                numberpin = numberpin + "2";
                password_Edt.setText(numberpin);
            }


        } else if (v == Imb_three) {


            if (isPinEnable) {
                numberpin = numberpin + "3";
                password_Edt.setText(numberpin);
            }


        } else if (v == Imb_four) {


            if (isPinEnable) {
                numberpin = numberpin + "4";
                password_Edt.setText(numberpin);
            }


        } else if (v == Imb_five) {


            if (isPinEnable) {
                numberpin = numberpin + "5";
                password_Edt.setText(numberpin);
            }

        } else if (v == Imb_six) {

            if (isPinEnable) {
                numberpin = numberpin + "6";
                password_Edt.setText(numberpin);
            }

        } else if (v == Imb_seven) {


            if (isPinEnable) {
                numberpin = numberpin + "7";
                password_Edt.setText(numberpin);
            }

        } else if (v == Imb_tight) {

            if (isPinEnable) {
                numberpin = numberpin + "8";
                password_Edt.setText(numberpin);
            }


        } else if (v == Imb_nine) {


            if (isPinEnable) {
                numberpin = numberpin + "9";
                password_Edt.setText(numberpin);
            }


        } else if (v == Imb_aero) {
            if (isPinEnable) {
                numberpin = numberpin + "0";
                password_Edt.setText(numberpin);
            }
        } else if (v == Imb_minus) {


        } else if (v == Imb_plus) {


        } else if (v == Imb_cancel) {


            AlertCancel("", "ARE YOU SURE TO CANCEL? Tap OK to confirm");


        } else if (v == Imb_clear) {


            if (isPinEnable) {

                String Spin = password_Edt.getText().toString();
                if (Spin != null && Spin.length() > 0) {
                    password_Edt.setText(Spin.substring(0, Spin.length() - 1));
                    password_Edt.setSelection(password_Edt.getText().length());
                    numberpin = password_Edt.getText().toString();
                }


            }


        } else if (v == Imb_forgetpin) {


            if (cd.isConnectingToInternet()) {
                ForgotPinRequest(Iconstant.forgot_pin_url);
            } else {
                Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
            }


        } else if (v == Imb_enter) {


            String pin = password_Edt.getText().toString().trim();
            if (!sSecurePin.equalsIgnoreCase(pin)) {
                AlertError(getResources().getString(R.string.action_error), getResources().getString(R.string.label_incorrect_pin));
            }else {

                if (WalletMoneyPage1.Scurrency != null) {
                    if (!withdrawlAmount.equalsIgnoreCase(""))
                        AlertEnter("Send money" + " " + WalletMoneyPage1.Scurrency + " " + withdrawlAmount + "?", "Please tap Yes to confirm");
                    else if (amount.equalsIgnoreCase(""))
                        AlertEnter("Send money" + " " + WalletMoneyPage1.Scurrency + " " + withdrawlAmount + "?", "Please tap Yes to confirm");
                }

            }
        }

    }


    private void AlertError(String title, String message) {

        String msg = title + "\n" + message;

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        AppMsg.Style style = new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red_color);
        AppMsg snack = AppMsg.makeText(CloudTransferAmountPinEnter1.this, msg.toUpperCase(), AppMsg.STYLE_ALERT);
        snack.setView(view);
        snack.setLayoutGravity(Gravity.TOP);
        snack.setPriority(AppMsg.PRIORITY_HIGH);
        snack.setAnimation(R.anim.slidedown, R.anim.slideup);
        snack.show();

    }


    //--------------Alert Method-----------
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(CloudTransferAmountPinEnter1.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog. setDialogUpper(false);
        mDialog.setCloseButton(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

            }
        });
        mDialog.show();
    }


    //--------------Alert Method-----------
    private void AlertEnter(String title, String alert) {
        final PkDialog mDialog = new PkDialog(CloudTransferAmountPinEnter1.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);

//        mDialog. setDialogUpper(false);
        mDialog.setCloseButton(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.setPositiveButton(getResources().getString(R.string.timer_label_alert_yes), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();

                if (flag.equalsIgnoreCase("paypal_withdrawal")) {
                    Intent in=new Intent(CloudTransferAmountPinEnter1.this, NewCloudMoneyTransferUrlProgress.class);
                    in.putExtra("flag", "paypal_withdrawal");
                    in.putExtra("amount",amount);
                    in.putExtra("payment_code",payment_code);
                    in.putExtra("paypal_id",paypal_id);
                    startActivity(in);
                    finish();
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                }else if(flag.equalsIgnoreCase("cash_withdrawal")){
                    Intent in=new Intent(CloudTransferAmountPinEnter1.this, NewCloudMoneyTransferUrlProgress.class);
                    in.putExtra("flag", "cash_withdrawal");
                    in.putExtra("amount",amount);
                    in.putExtra("payment_code",payment_code);
                    startActivity(in);
                    finish();
                    overridePendingTransition(R.anim.enter, R.anim.exit);

                }else {

                    Intent intent = new Intent(CloudTransferAmountPinEnter1.this, NewCloudMoneyTransferUrlProgress.class);
                    intent.putExtra("user_id", UserID);
                    intent.putExtra("amount", withdrawlAmount);
                    intent.putExtra("mode", paymentMod);


                    intent.putExtra("accHolderName1", accHolderName1);
                    intent.putExtra("holderAccountNumber1", holderAccountNumber1);
                    intent.putExtra("bankCode", bankCode);


                    intent.putExtra("flag", "XenditbankTransfer");
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                    finish();
                }

            }
        });
        mDialog.show();
    }


    //--------------Alert Method-----------
    private void Alertsuccess(String title, String alert) {
        final PkDialog mDialog = new PkDialog(CloudTransferAmountPinEnter1.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        mDialog.show();
    }

    private void ForgotPinRequest(String Url) {

        dialog = new Dialog(CloudTransferAmountPinEnter1.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", UserID);

        System.out.println("--------------ForgotPinRequest Url-------------------" + Url);
        System.out.println("--------------ForgotPinRequest jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(CloudTransferAmountPinEnter1.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                if (dialog != null) {
                    dialog.dismiss();
                }

                System.out.println("--------------ForgotPinRequest Response-------------------" + response);
                String status = "", sResponse = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");
                        sResponse = object.getString("message");

                        if (status.equalsIgnoreCase("1")) {
                            Alert("PIN CODE HAS BEEN SENT", "Please check your email");
                        } else {
                            Alert(getResources().getString(R.string.action_error), sResponse);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    //--------------Alert Method-----------
    private void AlertCancel(String title, String alert) {
        final PkDialog mDialog = new PkDialog(CloudTransferAmountPinEnter1.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog. setDialogUpper(false);
        mDialog.setCloseButton(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                finish();

            }
        });
        mDialog.show();
    }


    //--------------------Code to set error for EditText-----------------------
    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(CloudTransferAmountPinEnter1.this, R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }


}
