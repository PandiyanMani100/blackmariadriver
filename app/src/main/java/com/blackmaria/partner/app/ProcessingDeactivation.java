package com.blackmaria.partner.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.Pojo.KeyValueBean;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by GANESH on 29-08-2017.
 */

public class ProcessingDeactivation extends ActivityHockeyApp {

    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    private SessionManager session;

    private String sDriverID = "", sCurrency = "", sTotalAmount = "";
    private ArrayList<KeyValueBean> statementList;
    private boolean isDataPresent = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.processing_deactivation);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                statementList = new ArrayList<>();
                cd = new ConnectionDetector(ProcessingDeactivation.this);
                session = new SessionManager(ProcessingDeactivation.this);
                sDriverID = session.getUserDetails().get(SessionManager.KEY_DRIVERID);

                if (cd.isConnectingToInternet()) {
                    PostLogout(Iconstant.deactivate_account_statement_url);
                } else {
                    Intent resultIntent = new Intent();
                    resultIntent.putExtra("Status", -1);
                    setResult(Activity.RESULT_OK, resultIntent);
                    finish();
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                }

            }
        }, 3000);

    }


    private void PostLogout(String url) {

        HashMap<String, String> jsonParams = new HashMap<>();
        jsonParams.put("driver_id", sDriverID);

        System.out.println("-------------Deactivation Statement Url----------------" + url);
        System.out.println("-------------Deactivation Statement Params----------------" + jsonParams);

        mRequest = new ServiceRequest(ProcessingDeactivation.this);
        mRequest.makeServiceRequest(url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Deactivation Statement Response----------------" + response);

                String Sstatus = "";
                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {

                        JSONObject jobjResponse = object.getJSONObject("response");

                        if (jobjResponse.length() > 0) {
                            sCurrency = jobjResponse.getString("currency");
                            sTotalAmount = sCurrency + " " + jobjResponse.getString("total_amount");

                            Object objStatement = jobjResponse.get("statement");
                            if (objStatement instanceof JSONArray) {
                                JSONArray jarStatement = jobjResponse.getJSONArray("statement");

                                if (jarStatement.length() > 0) {

                                    statementList = new ArrayList<>();
                                    for (int i = 0; i < jarStatement.length(); i++) {

                                        JSONObject jobjValue = jarStatement.getJSONObject(i);

                                        String title = jobjValue.getString("title");
                                        String value = sCurrency + " " + jobjValue.getString("value");

                                        statementList.add(new KeyValueBean(title, value));

                                    }

                                    isDataPresent = true;

                                } else {
                                    isDataPresent = false;
                                }

                            } else {
                                isDataPresent = false;
                            }

                        } else {
                            isDataPresent = false;
                        }


                    } else {
                        isDataPresent = false;
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (isDataPresent) {
                    if (statementList.size() > 0) {
                        Intent resultIntent = new Intent();
                        resultIntent.putExtra("Status", 1);
                        resultIntent.putExtra("Currency", sCurrency);
                        resultIntent.putExtra("TotalAmount", sTotalAmount);
                        resultIntent.putExtra("StatementList", statementList);
                        setResult(Activity.RESULT_OK, resultIntent);
                        finish();
                        overridePendingTransition(R.anim.enter, R.anim.exit);
                    }
                } else {
                    Intent resultIntent = new Intent();
                    resultIntent.putExtra("Status", 0);
                    setResult(Activity.RESULT_OK, resultIntent);
                    finish();
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                }

            }

            @Override
            public void onErrorListener() {
                Intent resultIntent = new Intent();
                resultIntent.putExtra("Status", 0);
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });


    }

    /*private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(ProcessingDeactivation.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }*/

    /*private void showDialog() {

        dialog = new Dialog(ProcessingDeactivation.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_processing));

    }

    private void dismissDialog() {

        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }

    }*/

}
