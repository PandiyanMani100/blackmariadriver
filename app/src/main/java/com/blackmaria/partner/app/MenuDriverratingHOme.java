package com.blackmaria.partner.app;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.Pojo.MenuHomeratingPojo;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.adapter.MenuHomeRateAdapter;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.CustomTextView;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.blackmaria.partner.latestpackageview.widgets.TextRCLight;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by user144 on 1/3/2018.
 */

@SuppressLint("Registered")
public class MenuDriverratingHOme extends ActivityHockeyApp implements View.OnClickListener {

    private SessionManager session;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private Dialog dialog;
    private ServiceRequest mRequest;


    private ImageView backImag;//ratingPageProfilephoto;
    private TextRCLight ratingPageDriverCartypeTextview;
    private View dummyview5;
    private RatingBar myrideRatingSingleRatingbar;
    private CustomTextView myrideRatingSingleTitle;
    private RatingBar myrideRatingSingleRatingbar4;
    private CustomTextView myrideRatingSingleTitle1;
    private RatingBar myrideRatingSingleRatingbar1;
    private CustomTextView myrideRatingSingleTitle2;
    private RatingBar myrideRatingSingleRatingbar3;
    private ImageView ratingPageProfilephoto1;
    private TextView dateName;
    private TextView comments;
    private RelativeLayout viewAllReviews;
    private Button infoBtn;
    private ListView menuHomeListView;
    MenuHomeRateAdapter adapter;
    private String UserID = "";
    ArrayList<MenuHomeratingPojo> itemlist;
    private boolean isDataAvailable = false;
    private String rideId1 = "";
    private TextView lastcomment_text;
    private LinearLayout lastcomment_ll;
    private String type ="";

    //------------------------------Broadcost reciver---------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_rating_home_new);
        Initialize();

        if (getIntent().hasExtra("frompage")) {
            type = getIntent().getStringExtra("frompage");
        }
    }

    private void Initialize() {
        session = new SessionManager(MenuDriverratingHOme.this);
        cd = new ConnectionDetector(MenuDriverratingHOme.this);
        isInternetPresent = cd.isConnectingToInternet();


        itemlist = new ArrayList<MenuHomeratingPojo>();

        backImag = (ImageView) findViewById(R.id.back_imag);
        //  ratingPageProfilephoto = (ImageView) findViewById(R.id.rating_page_profilephoto);
        ratingPageDriverCartypeTextview = (TextRCLight) findViewById(R.id.rating_page_driver_cartype_textview);
        dummyview5 = (View) findViewById(R.id.dummyview5);
        myrideRatingSingleRatingbar = (RatingBar) findViewById(R.id.myride_rating_single_ratingbar);
        myrideRatingSingleTitle = (CustomTextView) findViewById(R.id.myride_rating_single_title);
        ratingPageProfilephoto1 = (ImageView) findViewById(R.id.rating_page_profilephoto1);
        viewAllReviews = (RelativeLayout) findViewById(R.id.view_all_reviews);


        lastcomment_text = (TextView) findViewById(R.id.noreview_tv);
        lastcomment_ll = (LinearLayout) findViewById(R.id.noreviews_available);
        infoBtn = (Button) findViewById(R.id.info_btn);
        dateName = (TextView) findViewById(R.id.date_name);
        comments = (TextView) findViewById(R.id.comments);
        menuHomeListView = (ListView) findViewById(R.id.menu_rating_home_list);


        backImag.setOnClickListener(this);
        viewAllReviews.setOnClickListener(this);
        infoBtn.setOnClickListener(this);



        // get user data from session
        HashMap<String, String> user = session.getUserDetails();
        UserID = user.get(SessionManager.KEY_DRIVERID);

        if (isInternetPresent) {
            postRequest_MenuratingHome(Iconstant.menupage_ratinglist_home);
        } else {
            Alert(getResources().getString(R.string.alert_label_title), getResources().getString(R.string.alert_nointernet));
        }
    }
    @Override
    public void onClick(View v) {
        if (v == viewAllReviews) {
            Intent intent = new Intent(MenuDriverratingHOme.this, MenuRatingListActivity.class);
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);

        } else if (v == infoBtn) {
            Intent intent = new Intent(MenuDriverratingHOme.this, TripSummary.class);
            intent.putExtra("rideID", rideId1);
            intent.putExtra("fromPage", "MenuDriverratingHOme");
            startActivity(intent);
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);

        } else if (v == backImag) {
            if (type.equalsIgnoreCase("Home")) {
                Intent intent = new Intent(MenuDriverratingHOme.this, NavigationPage.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            } else {
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        }
    }


    //-----------------------Rating List Post Request-----------------
    private void postRequest_MenuratingHome(String Url) {
        dialog = new Dialog(MenuDriverratingHOme.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));


        System.out.println("-------------MenuratingHome  Url----------------" + Url);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", UserID);


        mRequest = new ServiceRequest(MenuDriverratingHOme.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                String userName = "", userEmail = "", userGender = "", userAge = "", userImage = "", userAvgReview = "", driverName = "", driveromments = "", DriverImage = "", reviewDate1 = "";
                Log.e("rateing", response);

                System.out.println("-------------MenuratingHome Response----------------" + response);

                String Sstatus = "";
                String SRating_status = "";

                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    JSONObject jsonObj = object.getJSONObject("response");

                    if (Sstatus.equalsIgnoreCase("1")) {

                        JSONObject driver_profile_object = jsonObj.getJSONObject("driver_profile");

                        if (driver_profile_object.length() > 0) {
                            userName = driver_profile_object.getString("driver_name");
                            userEmail = driver_profile_object.getString("email");
                            userGender = driver_profile_object.getString("gender");
                            userImage = driver_profile_object.getString("driver_image");
                            userAvgReview = driver_profile_object.getString("avg_review");

                        }

                        Object intervention = jsonObj.get("last_comment_review");
                        if (intervention instanceof JSONArray) {
                            // It's an array
                            //viewAllReviews.setVisibility(View.GONE);
                            lastcomment_ll.setVisibility(View.GONE);
                            lastcomment_text.setVisibility(View.VISIBLE);

                        } else if (intervention instanceof JSONObject) {
                            // It's an object
                            JSONObject objLastreview = jsonObj.getJSONObject("last_comment_review");
                            if (objLastreview.length() > 0) {
                                driverName = objLastreview.getString("driver_name");
                                driveromments = objLastreview.getString("comments");
                                DriverImage = objLastreview.getString("image");
                                rideId1 = objLastreview.getString("ride_id");
                                reviewDate1 = objLastreview.getString("review_date");
                            }
                            lastcomment_ll.setVisibility(View.VISIBLE);
                            lastcomment_text.setVisibility(View.GONE);
                        }

                        JSONArray payment_array = jsonObj.getJSONArray("reason_review");
                        if (payment_array.length() > 0) {
                            itemlist.clear();
                            for (int i = 0; i < payment_array.length(); i++) {
                                JSONObject reason_object = payment_array.getJSONObject(i);
                                MenuHomeratingPojo pojo = new MenuHomeratingPojo();
                                pojo.SetRatingTitle(reason_object.getString("title"));
                                pojo.getRatingAvarage(reason_object.getString("avg_ratting"));
                                pojo.SetRatingCount(reason_object.getString("total_count"));
                                itemlist.add(pojo);
                            }
                            isDataAvailable = true;
                        } else {
                            isDataAvailable = false;
                        }
                    }

                    if (Sstatus.equalsIgnoreCase("1") && isDataAvailable) {

                        if (SRating_status.equalsIgnoreCase("1")) {

                            Toast.makeText(getApplicationContext(), "Already submitted your rating", Toast.LENGTH_SHORT).show();

                        } else {

                            if (!DriverImage.equalsIgnoreCase("")) {
                                Picasso.with(MenuDriverratingHOme.this)
                                        .load(DriverImage)
                                        .into(ratingPageProfilephoto1);
                                myrideRatingSingleRatingbar.setRating(Float.parseFloat(userAvgReview));
                            }

                            dateName.setText(driverName + " on " + reviewDate1);
                            comments.setText(driveromments);

//                            adapter = new MenuHomeRateAdapter(MenuDriverratingHOme.this, itemlist);
//                            menuHomeListView.setAdapter(adapter);
                        }

                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(MenuDriverratingHOme.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

}