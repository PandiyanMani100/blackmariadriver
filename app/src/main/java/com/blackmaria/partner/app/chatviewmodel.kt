package com.blackmaria.partner.app

import android.app.Application
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.blackmaria.partner.Utils.SessionManager
import com.blackmaria.partner.latestpackageview.Utils.AppUtils
import com.blackmaria.partner.latestpackageview.Xmpp.MyXMPP
import org.json.JSONException
import org.json.JSONObject
import java.util.*


class chatviewmodel(application: Application) : AndroidViewModel(application)
{


    private val closepage = MutableLiveData<Int>()
    private val sendmessage = MutableLiveData<Int>()
    private val templateclick = MutableLiveData<Int>()
    private val attachimage = MutableLiveData<Int>()


    fun closemodelobserver(): MutableLiveData<Int>
    {
        return closepage
    }
    fun attachimageobsrvere(): MutableLiveData<Int>
    {
        return attachimage
    }
    fun templateclickobserver(): MutableLiveData<Int>
    {
        return templateclick
    }

    fun sendmessageobserver(): MutableLiveData<Int>
    {
        return sendmessage
    }
    fun closeoperation()
    {
        closepage.value = 1
    }
    fun sendmessage()
    {
        sendmessage.value = 1
    }
    fun attachimage()
    {
        attachimage.value = 1
    }
    fun tempateclick()
    {
        templateclick.value = 1
    }

    fun sendxmppmessage(mcontext:Context,jid:String,useridtosend:String,rideid:String,ts:String,message:String,currentDateandTime:String,driverid:String,ios_fcm_key:String,user_fcm_token:String)
    {
        val sessionManager = SessionManager.getInstance(mcontext)
        val domain: HashMap<String, String> = sessionManager.getXmpp()
        val ServiceName = domain[SessionManager.KEY_HOST_NAME]
        val HostAddress = domain[SessionManager.KEY_HOST_URL]
        val user: HashMap<String, String> = sessionManager.getUserDetails()
        val USERNAME = user[SessionManager.KEY_DRIVERID]
        val PASSWORD = user[SessionManager.KEY_SEC_KEY]
        val myXMPP = MyXMPP.getInstance(mcontext, ServiceName, HostAddress, USERNAME, PASSWORD)
        val chatID: String = useridtosend + "@" + ServiceName
        val job = JSONObject()

        job.put("message", message)
        job.put("timeStamp", ts)
        job.put("rideID", rideid)
        job.put("action", "blackmariachat")
        job.put("senderID", driverid)
        job.put("receiverID", useridtosend)
        job.put("datetime", currentDateandTime)
        if(message.startsWith("http"))
        {
            job.put("msgType", "image")
        }
        else
        {
            job.put("msgType", "text")
        }

        val status = myXMPP.sendMessage(chatID, job.toString())
        if (status == 0) {
           //AppUtils.toastprint(mcontext, "Message Not sent")
        }

        val intent1 = Intent(mcontext, fcmsendingprocess::class.java)
        try {
            intent1.putExtra("serverKey", ios_fcm_key)
            intent1.putExtra("fcmtoken", user_fcm_token)
            intent1.putExtra("content", job.toString())
            intent1.putExtra("rideID", rideid)
            mcontext.startService(intent1)
        } catch (e: JSONException) {
            e.printStackTrace()
        }


    }
    fun updateallstatsu(mContext:Context,rideid:String)
    {
        var mHelper: DbHelper? = null
        var dataBase: SQLiteDatabase? = null
        mHelper= DbHelper(mContext)
        var values: ContentValues = ContentValues()
        values.put(DbHelper.status,"1")
        dataBase = mHelper!!.getWritableDatabase();
        var mCursor: Cursor = dataBase!!.rawQuery("SELECT * FROM " + DbHelper.TABLE_NAME+" WHERE rideid='"+rideid+"'", null);
        if (mCursor.moveToFirst()) {
            do {
                var id:Int= mCursor.getString(mCursor.getColumnIndex(DbHelper.KEY_ID)).toInt()
                dataBase!!.update(DbHelper.TABLE_NAME, values, DbHelper.KEY_ID+"="+id, null);
            } while (mCursor.moveToNext());
        }
        mCursor.close()
    }
    fun savemessagetodb(mContext:Context,rideid:String,message:String,senderid:String,timestamp:String,currentDateandTime:String)
    {
        var mHelper: DbHelper? = null
        var dataBase: SQLiteDatabase? = null
        mHelper= DbHelper(mContext)
        dataBase=mHelper!!.getWritableDatabase()
        var values:ContentValues= ContentValues()
        values.put(DbHelper.rideid,rideid)
        values.put(DbHelper.message,message )
        values.put(DbHelper.senderid,senderid )
        values.put(DbHelper.timestamp,timestamp)
        values.put(DbHelper.status,"1")
        values.put(DbHelper.datetime,currentDateandTime)

        dataBase!!.insert(DbHelper.TABLE_NAME, null, values)
    }
}