package com.blackmaria.partner.app;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.Request;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.mylibrary.volley.ServiceRequest;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class MyDialogFragment extends BottomSheetDialogFragment {

    private SessionManager session;
    Communicator communicator;


    AccessLanguagefromlocaldb db;


    public static MyDialogFragment newInstance() {
        return new MyDialogFragment();
    }


    @Override
    public void onAttach(Activity activity) {

        super.onAttach(activity);

        if (activity instanceof Communicator) {
            communicator = (Communicator) getActivity();
        } else {
            throw new ClassCastException(activity.toString()
                    + " must implemenet MyListFragment.communicator");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setCancelable(true);
        getDialog().setTitle("Title");
        db = new AccessLanguagefromlocaldb(getActivity());

        View view = inflater.inflate(R.layout.dialog_fragments, null, false);

        TextView languagess = view.findViewById(R.id.languagess);
        languagess.setText(db.getvalue("language"));

        TextView textView1 = view.findViewById(R.id.textView1);
        textView1.setText(db.getvalue("choose_your_default_language"));

        RadioGroup rb = (RadioGroup) view.findViewById(R.id.radioGroup);
        TextView radioButton = view.findViewById(R.id.radioButton);
        radioButton.setText(db.getvalue("english"));
        TextView radioButton2 = view.findViewById(R.id.radioButton2);
        radioButton2.setText(db.getvalue("indonesian"));

        TextView tamil = view.findViewById(R.id.tamil);
        tamil.setText(db.getvalue("india_tamil"));

        session = new SessionManager(getActivity());
        String languageToLoad  = session.getCurrentlanguage(); // your language

        radioButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                callupdate("en");

            }
        });

        tamil.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                callupdate("ta");

            }
        });
        radioButton2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                callupdate("IND");

            }
        });




        return view;
    }

    private void callupdate(String code)
    {
        PostRequest(Iconstant.updatelanguagee, code);
    }

    private void PostRequest(String Url, final String code) {
        HashMap<String, String> info;
        info = session.getUserDetails();
        String UserID = info.get(SessionManager.KEY_DRIVERID);

        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("id", UserID);
        jsonParams.put("lang_code", code);
        jsonParams.put("user_type", "user");

        ServiceRequest mRequest = new ServiceRequest(getActivity());
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                dialog.dismiss();
                System.out.println("--------------HomePage reponse-------------------" + response);
                Log.e("dsf",response);
                 try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equalsIgnoreCase("1"))
                    {
                        communicator.message(code);
                    }
                    else
                    {
                        communicator.message(code);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });

    }



    public interface Communicator {
        public void message(String data);
    }


}