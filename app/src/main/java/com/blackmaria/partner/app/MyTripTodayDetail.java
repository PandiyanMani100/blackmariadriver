package com.blackmaria.partner.app;

/**
 * Created by GANESH on 4/27/2017.
 */

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.blackmaria.partner.Hockeyapp.ActivityHockeyApp;
import com.blackmaria.partner.Pojo.MultipleLocationPojo;
import com.blackmaria.partner.Pojo.TodayDetails;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.CurrencySymbolConverter;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.adapter.TodayDetailListAdapter;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.blackmaria.partner.latestpackageview.Database.GEODBHelper;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class MyTripTodayDetail extends ActivityHockeyApp implements View.OnClickListener {

    TextView Tv_todayTrips, Tv_date;
    TextView Tv_empty_view;
    //    GridView gridTodayTrips;
    ExpandableHeightListView Hlv_todayTrips;
    ImageView Iv_prev, Iv_next, Iv_back;
    TodayDetailListAdapter adapter;
    private ArrayList<TodayDetails> listTodayTripDetails;
    SessionManager sessionManager;
    ConnectionDetector cd;
    private Dialog dialog;
    private ServiceRequest mRequest;
    private String currentPage = "", nextPage = "", totalRides = "", sDriveID = "", pageNo = "", contentPerPage = "", flagToday = "", filterDate = "", sDisplayDate = "";
    private boolean isDataAvailable = false, isAdapterSet = false;
    private int currentPageNo = 0;


    private ArrayList<MultipleLocationPojo> multipleDropList;
    private boolean isMultipleDropAvailable = false;
    private GEODBHelper myDBHelper;
    private String isReturnLatLngAvailable = "0", isReturnPickUPLatLngAvailable = "0";
    private String sRideId = "", sReturnLat = "", sReturnLng = "", sReturnPickUpLat = "", sReturnPickUpLng = "";
    private TextView todayDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trip_today_detail);
        initialize();
    }

    @Override
    public void onClick(View view) {

        if (view == Iv_prev) {
            if (cd.isConnectingToInternet()) {
                pageNo = String.valueOf(currentPageNo - 1);
                GetTodayTripDetails(Iconstant.today_trips_details_url);
            } else {
                Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
            }
        } else if (view == Iv_next) {
            if (cd.isConnectingToInternet()) {
                pageNo = String.valueOf(currentPageNo + 1);
                GetTodayTripDetails(Iconstant.today_trips_details_url);
            } else {
                Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
            }
        } else if (view == Iv_back) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }

    }

    private void initialize() {

        sessionManager = new SessionManager(MyTripTodayDetail.this);
        myDBHelper = new GEODBHelper(MyTripTodayDetail.this);
        cd = new ConnectionDetector(MyTripTodayDetail.this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriveID = user.get(SessionManager.KEY_DRIVERID);
        multipleDropList = new ArrayList<MultipleLocationPojo>();

        Tv_todayTrips = (TextView) findViewById(R.id.txt_trips_count);
        Tv_date = (TextView) findViewById(R.id.txt_date);
        Tv_empty_view = (TextView) findViewById(R.id.txt_list_empty_view);
//        gridTodayTrips = (GridView) findViewById(R.id.list_today_trips);
        Hlv_todayTrips = (ExpandableHeightListView) findViewById(R.id.lv_today_details);
        Iv_prev = (ImageView) findViewById(R.id.img_page_prev);
        Iv_next = (ImageView) findViewById(R.id.img_page_next);
        Iv_back = (ImageView) findViewById(R.id.img_back);
        todayDate = (TextView) findViewById(R.id.txt_label_this_week_trips);
        Iv_back.setOnClickListener(this);
        Iv_prev.setOnClickListener(this);
        Iv_next.setOnClickListener(this);

        getIntentData();

        Hlv_todayTrips.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String rideID = listTodayTripDetails.get(position).getRide_id();
                Intent intent = new Intent(MyTripTodayDetail.this, TripSummary.class);
                intent.putExtra("rideID", rideID);
                intent.putExtra("fromPage", "MyTripTodayDetail");
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        if (cd.isConnectingToInternet()) {
            GetTodayTripDetails(Iconstant.today_trips_details_url);
        } else {
            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }


    }

    private void getIntentData() {

        Intent intent = getIntent();
        if (intent != null) {
//            contentPerPage = intent.getStringExtra("itemsPerPage");
            filterDate = intent.getStringExtra("filterDate");
            flagToday = intent.getStringExtra("todayFlag");
//            sDisplayDate = intent.getStringExtra("displayDate");

            SimpleDateFormat fromFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat myFormat = new SimpleDateFormat("EEEE dd MMMM yyyy");



//            String strDate = fromFormat.format(calendar.getTime());


//            System.out.println("===kannan===ciurrent date======"+strDate);



            try {

                Calendar calendar = Calendar.getInstance();

                Date currentdate = fromFormat.parse(fromFormat.format(calendar.getTime()));

                Date coimingdate = fromFormat.parse(filterDate);



                if(currentdate.equals(coimingdate)){


                    System.out.println("-----kannan------equals------");

                    flagToday="0";

                }else {

                    System.out.println("-----kannan-----not-equals------");

                    flagToday="1";

                }



                sDisplayDate = myFormat.format(fromFormat.parse(filterDate));



            } catch (ParseException e) {
                e.printStackTrace();
            }

        }

        if (pageNo.length() == 0) {
            pageNo = "1";
        }

        contentPerPage = "4";

    }

    @SuppressLint("WrongConstant")
    private void GetTodayTripDetails(String Url) {

        dialog = new Dialog(MyTripTodayDetail.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriveID);
        jsonParams.put("page", pageNo);
        jsonParams.put("perPage", contentPerPage);
        jsonParams.put("filter_date", filterDate);

        System.out.println("--------------TodayTripDetails Url-------------------" + Url);
        System.out.println("--------------TodayTripDetails jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(MyTripTodayDetail.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------TodayTripDetails Response-------------------" + response);

                String status = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {
                            JSONObject jsonObject = object.getJSONObject("response");
                            if (jsonObject.length() > 0) {

                                nextPage = jsonObject.getString("next_page");
                                totalRides = jsonObject.getString("total_rides");
                                currentPage = jsonObject.getString("current_page");

                                Object ridesObj = jsonObject.get("rides");
                                if (ridesObj instanceof JSONArray) {

                                    JSONArray jarRides = jsonObject.getJSONArray("rides");

                                    if (jarRides.length() > 0) {

                                        listTodayTripDetails = new ArrayList<>();
                                        listTodayTripDetails.clear();
                                        for (int i = 0; i < jarRides.length(); i++) {

                                            JSONObject jobjRides = jarRides.getJSONObject(i);

                                            TodayDetails details = new TodayDetails();

                                            details.setRide_id(jobjRides.getString("ride_id"));
                                            details.setRide_time(jobjRides.getString("ride_time"));
                                            details.setRide_date(jobjRides.getString("ride_date"));
                                            details.setPickup(jobjRides.getString("pickup"));
                                            details.setRide_status(jobjRides.getString("ride_status"));
                                            details.setDistance(jobjRides.getString("distance"));
                                            details.setRide_type(jobjRides.getString("ride_type"));
                                            details.setRide_category(jobjRides.getString("ride_category"));
                                            details.setDisplay_status(jobjRides.getString("display_status"));
                                            details.setUser_image(jobjRides.getString("user_image"));
                                            details.setUser_review(jobjRides.getString("user_review"));
                                            details.setUser_name(jobjRides.getString("user_name"));
                                            details.setGroup(jobjRides.getString("group"));
                                            details.setS_no(jobjRides.getString("s_no"));
                                            details.setDatetime(jobjRides.getString("datetime"));

                                            listTodayTripDetails.add(details);

                                        }

                                        isDataAvailable = true;

                                    } else {
                                        isDataAvailable = false;
                                    }

                                } else {
                                    isDataAvailable = false;
                                }

                            } else {
                                isDataAvailable = false;
                            }
                        } else {
                            String sResponse = object.getString("response");
                            Alert(getResources().getString(R.string.action_error), sResponse);
                        }

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (status.equalsIgnoreCase("1")) {

                    if (isDataAvailable) {

                        /*if (!isAdapterSet) {
                            System.out.println("Adapter Set "+listTodayTripDetails.size());
                            adapter = new TodayDetailGridAdapter(MyTripTodayDetail.this, listTodayTripDetails);
                            gridTodayTrips.setAdapter(adapter);
                            isAdapterSet = true;
                        } else {
                            System.out.println("Adapter Notify Data Set Changed "+listTodayTripDetails.size());
                            adapter = new TodayDetailGridAdapter(MyTripTodayDetail.this, listTodayTripDetails);
                            adapter.notifyDataSetChanged();
                        }*/

                        System.out.println("Adapter Set " + listTodayTripDetails.size());
                        adapter = new TodayDetailListAdapter(MyTripTodayDetail.this, listTodayTripDetails, new TodayDetailListAdapter.ButtonClickInterface() {
                            @Override
                            public void onButtonClick(int position) {
                                String rideStatus = listTodayTripDetails.get(position).getRide_status();
                                sRideId = listTodayTripDetails.get(position).getRide_id();
                                if (rideStatus.equalsIgnoreCase("Finished")) {
                                    Intent intent = new Intent(MyTripTodayDetail.this, FarePage.class);
                                    intent.putExtra("rideId", sRideId);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.enter, R.anim.exit);
                                } else {
                                    /*Intent intent = new Intent(MyTripTodayDetail.this, TripPage.class);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.enter, R.anim.exit);*/
                                    if (cd.isConnectingToInternet()) {
                                        ContinueTripRequest(Iconstant.continuePendingTrip_Url);
                                    } else {
                                        Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
                                    }
                                }
                            }
                        });
//                        gridTodayTrips.setAdapter(adapter);
                        Hlv_todayTrips.setAdapter(adapter);

                    } else {
                        Iv_prev.setVisibility(View.GONE);
                        Iv_next.setVisibility(View.GONE);
                        Hlv_todayTrips.setVisibility(View.GONE);
                        Tv_empty_view.setVisibility(View.VISIBLE);
                    }

                    if (totalRides != null && totalRides.length() > 0) {
                        int rides = 0;
                        try {
                            rides = Integer.parseInt(totalRides);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (rides > 0) {
                            Tv_todayTrips.setText(totalRides + " " + getResources().getString(R.string.mytrips_label_trips));
                        } else {
                            Tv_todayTrips.setText(totalRides + " " + getResources().getString(R.string.mytrips_label_trip));
                        }
                    }

                    if (currentPage != null && currentPage.length() > 0) {
                        currentPageNo = 0;
                        try {
                            currentPageNo = Integer.parseInt(currentPage);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (currentPageNo > 1) {
                            Iv_prev.setVisibility(View.VISIBLE);
                        } else {
                            Iv_prev.setVisibility(View.GONE);
                        }
                    }

                    if (nextPage.length() > 0) {
                        Iv_next.setVisibility(View.VISIBLE);
                    } else {
                        Iv_next.setVisibility(View.GONE);
                    }
                    if (flagToday.equalsIgnoreCase("1")) {
                        todayDate.setText(sDisplayDate);
                    }else{
                        todayDate.setText(getResources().getString(R.string.myTrip_label_today));
                    }
                    Tv_date.setText(sDisplayDate);

                } else {
                    Iv_prev.setVisibility(View.GONE);
                    Iv_next.setVisibility(View.GONE);
                    Hlv_todayTrips.setVisibility(View.GONE);
                    Tv_empty_view.setVisibility(View.VISIBLE);
                }

                if (dialog != null) {
                    dialog.dismiss();
                }

            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }

    //-------------------Continue Pending Trip Request----------------
    private void ContinueTripRequest(String Url) {

        dialog = new Dialog(MyTripTodayDetail.this);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_pleasewait));

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriveID);
        jsonParams.put("ride_id", sRideId);

        System.out.println("--------------Continue Pending Trip Url-------------------" + Url);
        System.out.println("--------------jsonParams-------------------" + jsonParams);

        mRequest = new ServiceRequest(MyTripTodayDetail.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------Continue Pending Trip response-------------------" + response);
                String status = "", sTitle_text = "", sSubtitle_text = "", sRideStatus = "";
                String Str_Username = "", Str_UserId = "", Str_User_email = "", Str_Userphoneno = "", Str_Userrating = "", Str_userimg = "", Str_pickuplocation = "", Str_pickuplat = "", Str_pickup_long = "",
                        Str_pickup_time = "", Str_message = "", sCurrencySymbol = "", est_cost = "", location = "", currency_code = "", user_gender = "", eta_arrival = "", member_since = "", Str_droplat = "", Str_droplon = "", str_drop_location = "", Zero_res = "", sTripMode = "";
                String sReturnState = "", isReturnAvailable = "", Str_payment_type = "", Str_cancelled_rides = "", Str_completed_rides = "", Str_rider_type = "",
                        sPaymentIcon = "", sCompAvailable = "", sCompStage = "";

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {
                        status = object.getString("status");

                        if (status.equalsIgnoreCase("1")) {
                            JSONObject jsonObject = object.getJSONObject("response");
                            if (jsonObject.length() > 0) {

                                JSONObject profile_jsonObject = jsonObject.getJSONObject("user_profile");
                                if (profile_jsonObject.length() > 0) {

                                    Str_Username = profile_jsonObject.getString("user_name");
                                    Str_UserId = profile_jsonObject.getString("user_id");
                                    Str_User_email = profile_jsonObject.getString("user_email");
                                    Str_Userphoneno = profile_jsonObject.getString("phone_number");
                                    Str_Userrating = profile_jsonObject.getString("user_review");
                                    Str_pickuplocation = profile_jsonObject.getString("pickup_location");
                                    Str_pickuplat = profile_jsonObject.getString("pickup_lat");
                                    Str_pickup_long = profile_jsonObject.getString("pickup_lon");
                                    Str_pickup_time = profile_jsonObject.getString("pickup_time");
                                    Str_userimg = profile_jsonObject.getString("user_image");
                                    Str_droplat = profile_jsonObject.getString("drop_lat");
                                    Str_droplon = profile_jsonObject.getString("drop_lon");
                                    str_drop_location = profile_jsonObject.getString("drop_loc");
                                    sTitle_text = profile_jsonObject.getString("title_text");
                                    sSubtitle_text = profile_jsonObject.getString("subtitle_text");
                                    sRideStatus = profile_jsonObject.getString("continue_trip");
                                    est_cost = profile_jsonObject.getString("est_cost");
                                    currency_code = profile_jsonObject.getString("currency");
                                    sCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(currency_code);
                                    eta_arrival = profile_jsonObject.getString("eta_arrival");
                                    member_since = profile_jsonObject.getString("member_since");
                                    user_gender = profile_jsonObject.getString("user_gender");
                                    location = profile_jsonObject.getString("location");
                                    Str_payment_type = profile_jsonObject.getString("payment_type");
                                    Str_cancelled_rides = profile_jsonObject.getString("cancelled_rides");
                                    Str_completed_rides = profile_jsonObject.getString("completed_rides");
                                    Str_rider_type = profile_jsonObject.getString("rider_type");

                                    if (profile_jsonObject.has("mode")) {
                                        sTripMode = profile_jsonObject.getString("mode");
                                    }

                                    sCompAvailable = profile_jsonObject.getString("comp_status");
                                    sCompStage = profile_jsonObject.getString("comp_stage");
                                    sPaymentIcon = profile_jsonObject.getString("payment_icon");

                                    if (profile_jsonObject.has("is_return_over")) {
                                        isReturnAvailable = profile_jsonObject.getString("is_return_over");
                                    }

                                    if (profile_jsonObject.has("return_mode")) {
                                        sReturnState = profile_jsonObject.getString("return_mode");
                                    }

                                    if (profile_jsonObject.has("multistop_array")) {

                                        Object multiDrop_array_check = profile_jsonObject.get("multistop_array");
                                        if (multiDrop_array_check instanceof JSONArray) {
                                            JSONArray multiDropArray = profile_jsonObject.getJSONArray("multistop_array");
                                            if (multiDropArray.length() > 0) {
                                                multipleDropList.clear();
                                                for (int i = 0; i < multiDropArray.length(); i++) {
                                                    JSONObject multipleDropArrayObject = multiDropArray.getJSONObject(i);

                                                    MultipleLocationPojo pojo = new MultipleLocationPojo();
                                                    pojo.setDropLocation(multipleDropArrayObject.getString("location"));
                                                    pojo.setIsEnd(multipleDropArrayObject.getString("is_end"));
                                                    pojo.setMode(multipleDropArrayObject.getString("mode"));
                                                    pojo.setRecordId(multipleDropArrayObject.getString("record_id"));

                                                    JSONObject latLngArrayObject = multipleDropArrayObject.getJSONObject("latlong");
                                                    if (latLngArrayObject.length() > 0) {
                                                        pojo.setDropLat(latLngArrayObject.getString("lat"));
                                                        pojo.setDropLon(latLngArrayObject.getString("lon"));
                                                    }

                                                    multipleDropList.add(pojo);
                                                }

                                                isMultipleDropAvailable = true;

                                            } else {
                                                multipleDropList.clear();
                                                isMultipleDropAvailable = false;
                                            }
                                        } else {
                                            multipleDropList.clear();
                                            isMultipleDropAvailable = false;
                                        }
                                    }


                                    Object return_array_check = profile_jsonObject.get("next_drop_location");
                                    if (return_array_check instanceof JSONObject) {
                                        JSONObject returnArrayObject = profile_jsonObject.getJSONObject("next_drop_location");
                                        JSONObject latLngArrayObject = returnArrayObject.getJSONObject("latlong");
                                        if (latLngArrayObject.length() > 0) {
                                            sReturnLat = latLngArrayObject.getString("lat");
                                            sReturnLng = latLngArrayObject.getString("lon");

                                            isReturnLatLngAvailable = "1";
                                        } else {
                                            isReturnLatLngAvailable = "0";
                                        }
                                    } else {
                                        isReturnLatLngAvailable = "0";
                                    }


                                    Object return_pickUp_array_check = profile_jsonObject.get("drop_location_array");
                                    if (return_pickUp_array_check instanceof JSONObject) {
                                        JSONObject returnArrayObject = profile_jsonObject.getJSONObject("drop_location_array");
                                        JSONObject latLngArrayObject = returnArrayObject.getJSONObject("latlong");
                                        if (latLngArrayObject.length() > 0) {
                                            sReturnPickUpLat = latLngArrayObject.getString("lat");
                                            sReturnPickUpLng = latLngArrayObject.getString("lon");

                                            isReturnPickUPLatLngAvailable = "1";
                                        } else {
                                            isReturnPickUPLatLngAvailable = "0";
                                        }
                                    } else {
                                        isReturnPickUPLatLngAvailable = "0";
                                    }


                                    myDBHelper.insertuser_id(Str_UserId);
                                }
                            }
                        } else {
                            String sResponse = object.getString("response");
                            Alert(getResources().getString(R.string.action_error), sResponse);
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                if (status.equalsIgnoreCase("1")) {
                    Intent intent = new Intent(MyTripTodayDetail.this, TripPageNew.class);
                    intent.putExtra("address", Str_pickuplocation);
                    intent.putExtra("rideId", sRideId);
                    intent.putExtra("pickuplat", Str_pickuplat);
                    intent.putExtra("pickup_long", Str_pickup_long);
                    intent.putExtra("username", Str_Username);
                    intent.putExtra("userrating", Str_Userrating);
                    intent.putExtra("phoneno", Str_Userphoneno);
                    intent.putExtra("userimg", Str_userimg);
                    intent.putExtra("UserId", Str_UserId);
                    intent.putExtra("drop_lat", Str_droplat);
                    intent.putExtra("drop_lon", Str_droplon);
                    intent.putExtra("drop_location", str_drop_location);
                    intent.putExtra("title_text", sTitle_text);
                    intent.putExtra("subtitle_text", sSubtitle_text);
                    intent.putExtra("trip_status", sRideStatus);
                    intent.putExtra("eta_arrival", eta_arrival);
                    intent.putExtra("location", location);
                    intent.putExtra("member_since", member_since);
                    intent.putExtra("user_gender", user_gender);
                    intent.putExtra("est_cost", est_cost);
                    intent.putExtra("sCurrencySymbol", sCurrencySymbol);
                    intent.putExtra("TripMode", sTripMode);

                    intent.putExtra("IsReturnAvailable", isReturnAvailable);
                    intent.putExtra("ReturnState", sReturnState);
                    intent.putExtra("IsReturnLatLngAvailable", isReturnLatLngAvailable);
                    intent.putExtra("ReturnLat", sReturnLat);
                    intent.putExtra("ReturnLng", sReturnLng);
                    intent.putExtra("ReturnPickUpLat", sReturnPickUpLat);
                    intent.putExtra("ReturnPickUpLng", sReturnPickUpLng);
                    intent.putExtra("payment_type", Str_payment_type);
                    intent.putExtra("cancelled_rides", Str_cancelled_rides);
                    intent.putExtra("completed_rides", Str_completed_rides);
                    intent.putExtra("rider_type", Str_rider_type);
                    intent.putExtra("comp_status", sCompAvailable);
                    intent.putExtra("comp_stage", sCompStage);
                    intent.putExtra("payment_icon", sPaymentIcon);

                    if (isMultipleDropAvailable) {
                        intent.putExtra("MultipleDropStatus", "1");
                        Bundle bundleObject = new Bundle();
                        bundleObject.putSerializable("additionalDropLocation", multipleDropList);
                        intent.putExtras(bundleObject);
                    } else {
                        intent.putExtra("MultipleDropStatus", "0");
                    }

                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                }


                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
    }


    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(MyTripTodayDetail.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

}