package com.blackmaria.partner.services;

import android.app.Service;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.IBinder;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.blackmaria.partner.Pojo.MultipleLocationPojo;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.app.TripPage;
import com.blackmaria.partner.app.TripPageNew;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Prem Kumar and Anitha on 12/28/2016.
 */

public class GoogleNavigationService extends Service {

    private WindowManager mWindowManager;
    private ImageView mImgFloatingView;
    private boolean mIsFloatingViewAttached = false;
    private GestureDetector gestureDetector;
    private SessionManager sessionManager;

    private String Str_RideId = "", Str_address = "", Str_pickUp_Lat = "",Str_pickUp_Long = "", Str_username = "",
            Str_user_rating = "", Str_user_phoneno = "", Str_user_img = "", Str_droplat = "", Str_droplon = "",
            str_drop_location = "", Suser_Id = "", title_text = "", subtitle_text = "", eta_arrival = "",
            location = "", member_since = "", user_gender = "", est_cost = "", sCurrencySymbol = "",Trip_Status="";
    private ArrayList<MultipleLocationPojo> multipleDropList;
    private String sMultipleDropStatus="",sTripMode="";

    private String sReturnState = "", isReturnAvailable = "";
    private String isReturnLatLngAvailable = "0";
    private String sReturnLat = "", sReturnLng = "",sReturnPickUpLat="",sReturnPickUpLng="", sPayment_type = "",
                   sCancelledRides = "", sCompletedRides = "", sRiderType = "", sMember = "";

    @Override
    public IBinder onBind(Intent intent) {
        //Not use this method
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(!mIsFloatingViewAttached){
            mWindowManager.addView(mImgFloatingView, mImgFloatingView.getLayoutParams());
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        gestureDetector = new GestureDetector(this, new SingleTapConfirm());
        sessionManager=new SessionManager(getApplicationContext());

        mImgFloatingView = new ImageView(this);
        mImgFloatingView.setImageResource(R.drawable.google_navigation_back_icon);

        final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                PixelFormat.TRANSLUCENT);

        params.gravity = Gravity.TOP | Gravity.LEFT;
        params.x = 50;
        params.y = 600;

        mWindowManager.addView(mImgFloatingView, params);

        mImgFloatingView.setOnTouchListener(new View.OnTouchListener() {

            private int initialX;
            private int initialY;
            private float initialTouchX;
            private float initialTouchY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                /*switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        initialX = params.x;
                        initialY = params.y;
                        initialTouchX = event.getRawX();
                        initialTouchY = event.getRawY();
                        return true;
                    case MotionEvent.ACTION_UP:
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        params.x = initialX + (int) (event.getRawX() - initialTouchX);
                        params.y = initialY + (int) (event.getRawY() - initialTouchY);
                        mWindowManager.updateViewLayout(mImgFloatingView, params);
                        return true;
                }*/

                if (gestureDetector.onTouchEvent(event)) {

                    HashMap<String, String> data = sessionManager.getGoogleNavigationData();
                    Str_address = data.get(SessionManager.KEY_GOOGLE_NAVIGATION_ADDRESS);
                    Str_RideId = data.get(SessionManager.KEY_GOOGLE_NAVIGATION_RIDE_ID);
                    Str_pickUp_Lat = data.get(SessionManager.KEY_GOOGLE_NAVIGATION_PICKUP_LAT);
                    Str_pickUp_Long = data.get(SessionManager.KEY_GOOGLE_NAVIGATION_PICKUP_LONG);
                    Str_username = data.get(SessionManager.KEY_GOOGLE_NAVIGATION_USERNAME);
                    Str_user_rating = data.get(SessionManager.KEY_GOOGLE_NAVIGATION_USER_RATING);
                    Str_user_phoneno = data.get(SessionManager.KEY_GOOGLE_NAVIGATION_PHONE);
                    Str_user_img = data.get(SessionManager.KEY_GOOGLE_NAVIGATION_USER_IMAGE);
                    Suser_Id = data.get(SessionManager.KEY_GOOGLE_NAVIGATION_USER_ID);
                    Str_droplat = data.get(SessionManager.KEY_GOOGLE_NAVIGATION_DROP_LAT);
                    Str_droplon = data.get(SessionManager.KEY_GOOGLE_NAVIGATION_DROP_LONG);
                    str_drop_location = data.get(SessionManager.KEY_GOOGLE_NAVIGATION_DROP_LOCATION);
                    title_text = data.get(SessionManager.KEY_GOOGLE_NAVIGATION_TITLE);
                    subtitle_text = data.get(SessionManager.KEY_GOOGLE_NAVIGATION_SUBTITLE);
                    Trip_Status = data.get(SessionManager.KEY_GOOGLE_NAVIGATION_TRIP_STATUS);
                    eta_arrival = data.get(SessionManager.KEY_GOOGLE_NAVIGATION_ETA_ARRIVAL);
                    location = data.get(SessionManager.KEY_GOOGLE_NAVIGATION_LOCATION);
                    member_since = data.get(SessionManager.KEY_GOOGLE_NAVIGATION_MEMBER_SINCE);
                    user_gender = data.get(SessionManager.KEY_GOOGLE_NAVIGATION_GENDER);
                    est_cost = data.get(SessionManager.KEY_GOOGLE_NAVIGATION_ETA_COST);
                    sCurrencySymbol = data.get(SessionManager.KEY_GOOGLE_NAVIGATION_CURRENCY_SYMBOL);
                    sMultipleDropStatus = data.get(SessionManager.KEY_GOOGLE_NAVIGATION_MULTIPLE_DROP_STATUS);
                    sTripMode = data.get(SessionManager.KEY_GOOGLE_NAVIGATION_TRIP_MODE);
                    isReturnAvailable = data.get(SessionManager.KEY_GOOGLE_NAVIGATION_IS_RETURN_AVAILABLE);
                    sReturnState = data.get(SessionManager.KEY_GOOGLE_NAVIGATION_RETURN_STATE);
                    isReturnLatLngAvailable = data.get(SessionManager.KEY_GOOGLE_NAVIGATION_IS_RETURN_LAT_LNG_AVAILABLE);
                    sReturnLat = data.get(SessionManager.KEY_GOOGLE_NAVIGATION_RETURN_LAT);
                    sReturnLng = data.get(SessionManager.KEY_GOOGLE_NAVIGATION_RETURN_LNG);
                    sReturnPickUpLat = data.get(SessionManager.KEY_GOOGLE_NAVIGATION_RETURN_PICKUP_LAT);
                    sReturnPickUpLng = data.get(SessionManager.KEY_GOOGLE_NAVIGATION_RETURN_PICKUP_LNG);
                    sPayment_type = data.get(SessionManager.KEY_GOOGLE_NAVIGATION_RETURN_PAYMENT_TYPE);
                    sCancelledRides = data.get(SessionManager.KEY_GOOGLE_NAVIGATION_CANCELLED_RIDES);
                    sCompletedRides = data.get(SessionManager.KEY_GOOGLE_NAVIGATION_COMPLETED_RIDES);
                    sRiderType = data.get(SessionManager.KEY_GOOGLE_NAVIGATION_RIDER_TYPE);
//                    sMember = data.get(SessionManager.KEY_GOOGLE_NAVIGATION_MEMBER);

                    String sMultipleDrop = data.get(SessionManager.KEY_GOOGLE_NAVIGATION_MULTIPLE_DROP_LOCATION);
                    Gson gson = new Gson();
                    Type type = new TypeToken<List<MultipleLocationPojo>>(){}.getType();
                    multipleDropList= gson.fromJson(sMultipleDrop,type);

                    Intent finish_TripPage = new Intent();
                    finish_TripPage.setAction("com.app.finish.TripPage");
                    sendBroadcast(finish_TripPage);

                    stopSelf();

                    Intent intent = new Intent(getApplicationContext(), TripPageNew.class);
                    intent.addCategory(Intent.CATEGORY_HOME);
                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                    intent.addCategory(Intent.CATEGORY_MONKEY);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("address", Str_address);
                    intent.putExtra("rideId", Str_RideId);
                    intent.putExtra("pickuplat", Str_pickUp_Lat);
                    intent.putExtra("pickup_long", Str_pickUp_Long);
                    intent.putExtra("username", Str_username);
                    intent.putExtra("userrating", Str_user_rating);
                    intent.putExtra("phoneno", Str_user_phoneno);
                    intent.putExtra("userimg", Str_user_img);
                    intent.putExtra("UserId", Suser_Id);
                    intent.putExtra("drop_lat", Str_droplat);
                    intent.putExtra("drop_lon", Str_droplon);
                    intent.putExtra("drop_location", str_drop_location);
                    intent.putExtra("title_text", title_text);
                    intent.putExtra("subtitle_text", subtitle_text);
                    intent.putExtra("trip_status", Trip_Status);
                    intent.putExtra("eta_arrival", eta_arrival);
                    intent.putExtra("location", location);
                    intent.putExtra("member_since", member_since);
                    intent.putExtra("user_gender", user_gender);
                    intent.putExtra("est_cost", est_cost);
                    intent.putExtra("sCurrencySymbol", sCurrencySymbol);
                    intent.putExtra("MultipleDropStatus", sMultipleDropStatus);
                    intent.putExtra("TripMode", sTripMode);
                    intent.putExtra("IsReturnAvailable", isReturnAvailable);
                    intent.putExtra("ReturnState", sReturnState);
                    intent.putExtra("IsReturnLatLngAvailable", isReturnLatLngAvailable);
                    intent.putExtra("ReturnLat", sReturnLat);
                    intent.putExtra("ReturnLng", sReturnLng);
                    intent.putExtra("ReturnPickUpLat", sReturnPickUpLat);
                    intent.putExtra("ReturnPickUpLng", sReturnPickUpLng);
                    intent.putExtra("payment_type", sPayment_type);
                    intent.putExtra("cancelled_rides", sCancelledRides);
                    intent.putExtra("completed_rides", sCompletedRides);
                    intent.putExtra("rider_type", sRiderType);
//                    intent.putExtra("member", sMember);
                    Bundle bundleObject = new Bundle();
                    bundleObject.putSerializable("additionalDropLocation", multipleDropList);
                    intent.putExtras(bundleObject);
                    startActivity(intent);

                    return true;
                }


                return false;
            }
        });

        mIsFloatingViewAttached = true;
    }


    private class SingleTapConfirm extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapUp(MotionEvent event) {
            return true;
        }
    }


    public void removeView() {
        if (mImgFloatingView != null){
            mWindowManager.removeView(mImgFloatingView);
            mIsFloatingViewAttached = false;
        }
    }

    @Override
    public void onDestroy() {
        System.out.println("-----------google Navigation service onDestroy----------------");
        super.onDestroy();
        removeView();
    }
}
