package com.blackmaria.partner.Hockeyapp;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.WindowManager;

import com.blackmaria.partner.iconstant.Iconstant;




public class ActionBarActivityHockeyApp extends AppCompatActivity
{

    private static  String APP_ID = Iconstant.ACTION_ACTION_HOCKYAPPID;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN |
                    WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                    WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                    WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON,
            WindowManager.LayoutParams.FLAG_FULLSCREEN |
                    WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                    WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                    WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

//        checkForUpdates();

    }

    @Override
    protected void onResume() {
        super.onResume();
        System.gc();
//        checkForCrashes();
    }

    @Override
    protected void onPause() {
        super.onPause();
//        unregisterManagers();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        unregisterManagers();
    }

    private void checkForCrashes() {
//        CrashManager.register(this, APP_ID);
    }

    private void checkForUpdates() {
        // Remove this for store builds!
//        UpdateManager.register(this, APP_ID);
    }

    private void unregisterManagers() {
//        UpdateManager.unregister();
        // unregister other managers if necessary...
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();

        System.gc();

    }
}

