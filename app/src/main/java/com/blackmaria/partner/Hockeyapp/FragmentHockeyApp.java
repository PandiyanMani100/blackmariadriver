package com.blackmaria.partner.Hockeyapp;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;


import com.blackmaria.partner.iconstant.Iconstant;


public class FragmentHockeyApp extends Fragment
{


    private static  String APP_ID = Iconstant.ACTION_ACTION_HOCKYAPPID;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//        checkForUpdates();
        return null;
    }

    @Override
    public void onResume() {
        super.onResume();
//        checkForCrashes();
    }

    @Override
    public void onPause() {
        super.onPause();
//        unregisterManagers();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        unregisterManagers();
    }

    private void checkForCrashes() {
//        CrashManager.register(getActivity(), APP_ID);
    }

    private void checkForUpdates() {
        // Remove this for store builds!
//        UpdateManager.register(getActivity(), APP_ID);
    }

    private void unregisterManagers() {
//        UpdateManager.unregister();
        // unregister other managers if necessary...
    }
}

