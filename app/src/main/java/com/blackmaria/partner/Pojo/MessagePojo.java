package com.blackmaria.partner.Pojo;



public class MessagePojo {
    public String date;
    public String IMage;
    public String description;
    public String title, type;
    private boolean ischecked;
    public EarningDetail earningDetail;

    public MessagePojo() {
        super();
    }

    public String getNotification_id() {
        return notification_id;
    }

    public void setNotification_id(String notification_id) {
        this.notification_id = notification_id;
    }

    private String notification_id;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    public String getIMage() {
        return IMage;
    }

    public void setIMage(String IMage) {
        this.IMage = IMage;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public EarningDetail getEarningDetail() {
        return earningDetail;
    }

    public void setEarningDetail(EarningDetail earningDetail) {
        this.earningDetail = earningDetail;
    }

    public class EarningDetail {

        String ref_no, location_id, driver_id, company_id, amount, period,
                payment, completed_trip, paid_by_cash, by_card, by_wallet,
                gross_earning, fees_charge, nett_earn, currency;

        public EarningDetail() {
            super();
        }

        public String getRef_no() {
            return ref_no;
        }

        public void setRef_no(String ref_no) {
            this.ref_no = ref_no;
        }

        public String getLocation_id() {
            return location_id;
        }

        public void setLocation_id(String location_id) {
            this.location_id = location_id;
        }

        public String getDriver_id() {
            return driver_id;
        }

        public void setDriver_id(String driver_id) {
            this.driver_id = driver_id;
        }

        public String getCompany_id() {
            return company_id;
        }

        public void setCompany_id(String company_id) {
            this.company_id = company_id;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getPeriod() {
            return period;
        }

        public void setPeriod(String period) {
            this.period = period;
        }

        public String getPayment() {
            return payment;
        }

        public void setPayment(String payment) {
            this.payment = payment;
        }

        public String getCompleted_trip() {
            return completed_trip;
        }

        public void setCompleted_trip(String completed_trip) {
            this.completed_trip = completed_trip;
        }

        public String getPaid_by_cash() {
            return paid_by_cash;
        }

        public void setPaid_by_cash(String paid_by_cash) {
            this.paid_by_cash = paid_by_cash;
        }

        public String getBy_card() {
            return by_card;
        }

        public void setBy_card(String by_card) {
            this.by_card = by_card;
        }

        public String getBy_wallet() {
            return by_wallet;
        }

        public void setBy_wallet(String by_wallet) {
            this.by_wallet = by_wallet;
        }

        public String getGross_earning() {
            return gross_earning;
        }

        public void setGross_earning(String gross_earning) {
            this.gross_earning = gross_earning;
        }

        public String getNett_earn() {
            return nett_earn;
        }

        public void setNett_earn(String nett_earn) {
            this.nett_earn = nett_earn;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getFees_charge() {
            return fees_charge;
        }

        public void setFees_charge(String fees_charge) {
            this.fees_charge = fees_charge;
        }
    }

}
