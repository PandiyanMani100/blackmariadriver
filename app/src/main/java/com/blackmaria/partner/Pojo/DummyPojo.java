package com.blackmaria.partner.Pojo;

/**
 * Created by user127 on 02-06-2017.
 */

public class DummyPojo {

    String type, userImage, date, amount, currencyCode;

    public DummyPojo() {
    }


    public DummyPojo(String type, String userImage, String date, String amount, String currencyCode) {
        this.type = type;
        this.userImage = userImage;
        this.date = date;
        this.amount = amount;
        this.currencyCode = currencyCode;

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }
}
