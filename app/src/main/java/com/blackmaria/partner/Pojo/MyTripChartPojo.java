package com.blackmaria.partner.Pojo;

/**
 * Created by Prem Kumar and Anitha on 12/30/2016.
 */

public class MyTripChartPojo
{
    String Day,RideCount,Distance;

    public String getDay() {
        return Day;
    }

    public void setDay(String day) {
        Day = day;
    }

    public String getDistance() {
        return Distance;
    }

    public void setDistance(String distance) {
        Distance = distance;
    }

    public String getRideCount() {
        return RideCount;
    }

    public void setRideCount(String rideCount) {
        RideCount = rideCount;
    }
}
