package com.blackmaria.partner.Pojo;

/**
 * Created by Prem Kumar and Anitha on 3/6/2017.
 */

public class EarningsPojo {
    private String sNo, sDate, sTrip, sEarnings, sDistance, sStatus, sRideId = "", sDuration = "";

    public String getsNo() {
        return sNo;
    }

    public void setsNo(String sNo) {
        this.sNo = sNo;
    }

    public String getsDate() {
        return sDate;
    }

    public void setsDate(String sDate) {
        this.sDate = sDate;
    }

    public String getsTrip() {
        return sTrip;
    }

    public void setsTrip(String sTrip) {
        this.sTrip = sTrip;
    }

    public String getsEarnings() {
        return sEarnings;
    }

    public void setsEarnings(String sEarnings) {
        this.sEarnings = sEarnings;
    }

    public String getsDistance() {
        return sDistance;
    }

    public void setsDistance(String sDistance) {
        this.sDistance = sDistance;
    }

    public String getsStatus() {
        return sStatus;
    }

    public void setsStatus(String sStatus) {
        this.sStatus = sStatus;
    }

    public String getsRideId() {
        return sRideId;
    }

    public void setsRideId(String sRideId) {
        this.sRideId = sRideId;
    }

    public String getsDuration() {
        return sDuration;
    }

    public void setsDuration(String sDuration) {
        this.sDuration = sDuration;
    }
}
