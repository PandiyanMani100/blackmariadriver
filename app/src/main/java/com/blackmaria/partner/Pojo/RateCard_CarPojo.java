package com.blackmaria.partner.Pojo;

/**
 * Created by Prem Kumar and Anitha on 10/14/2015.
 */
public class RateCard_CarPojo
{
    private String category,id,selectStatus,icon_car_image;


    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSelectStatus() {
        return selectStatus;
    }

    public void setSelectStatus(String selectStatus) {
        this.selectStatus = selectStatus;
    }

    public String getIcon_car_image() {
        return icon_car_image;
    }

    public void setIcon_car_image(String icon_car_image) {
        this.icon_car_image = icon_car_image;
    }
}
