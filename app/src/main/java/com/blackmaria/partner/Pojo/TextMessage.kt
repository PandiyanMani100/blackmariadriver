package com.blackmaria.partner.Pojo

class TextMessage
{
    var rideid: String? = null
    var message: String? = null
    var senderid: String? = null
    var timestamp: String? = null
    var status: String? = null
    var date: String? = null


    constructor(rideid: String,message: String,senderid: String,timestamp: String,status: String,date: String)
    {
        this.rideid = rideid
        this.message = message
        this.senderid = senderid
        this.timestamp = timestamp
        this.status = status
        this.date = date
    }
}