package com.blackmaria.partner.Pojo;

/**
 * Created by user127 on 12-05-2017.
 */

public class WeekDetails {

    String day, ridecount, distance, driver_earning, currency, order, date_f;

    public WeekDetails() {
        super();
    }

    public WeekDetails(String day, String ridecount, String distance, String driver_earning, String currency, String order, String date_f) {
        this.day = day;
        this.ridecount = ridecount;
        this.distance = distance;
        this.driver_earning = driver_earning;
        this.currency = currency;
        this.order = order;
        this.date_f = date_f;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getRidecount() {
        return ridecount;
    }

    public void setRidecount(String ridecount) {
        this.ridecount = ridecount;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getDriver_earning() {
        return driver_earning;
    }

    public void setDriver_earning(String driver_earning) {
        this.driver_earning = driver_earning;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getDate_f() {
        return date_f;
    }

    public void setDate_f(String date_f) {
        this.date_f = date_f;
    }
}
