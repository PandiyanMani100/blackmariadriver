package com.blackmaria.partner.Pojo;

/**
 * Created by user127 on 12-05-2017.
 */

public class TodayDetails {

    String ride_id, ride_time, ride_date, pickup, ride_status, distance, ride_type,
            ride_category, display_status, user_image, user_name, user_review, driver_earning, currency, group, s_no, datetime;

    public TodayDetails() {
        super();
    }

    public String getDriver_earning() {
        return driver_earning;
    }

    public void setDriver_earning(String driver_earning) {
        this.driver_earning = driver_earning;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public TodayDetails(String ride_id, String ride_time, String ride_date, String pickup,
                        String ride_status, String distance, String ride_type, String ride_category,
                        String display_status, String user_image, String user_review, String driver_earning,
                        String currency, String group, String s_no, String datetime) {
        this.ride_id = ride_id;
        this.ride_time = ride_time;
        this.ride_date = ride_date;
        this.pickup = pickup;
        this.ride_status = ride_status;
        this.distance = distance;
        this.ride_type = ride_type;
        this.ride_category = ride_category;
        this.display_status = display_status;
        this.user_image = user_image;
        this.user_name = user_name;
        this.user_review = user_review;
        this.driver_earning = driver_earning;
        this.currency = currency;
        this.group = group;
        this.s_no = s_no;
        this.datetime = datetime;
    }

    public String getRide_id() {
        return ride_id;
    }

    public void setRide_id(String ride_id) {
        this.ride_id = ride_id;
    }

    public String getRide_time() {
        return ride_time;
    }

    public void setRide_time(String ride_time) {
        this.ride_time = ride_time;
    }

    public String getRide_date() {
        return ride_date;
    }

    public void setRide_date(String ride_date) {
        this.ride_date = ride_date;
    }

    public String getPickup() {
        return pickup;
    }

    public void setPickup(String pickup) {
        this.pickup = pickup;
    }

    public String getRide_status() {
        return ride_status;
    }

    public void setRide_status(String ride_status) {
        this.ride_status = ride_status;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getRide_type() {
        return ride_type;
    }

    public void setRide_type(String ride_type) {
        this.ride_type = ride_type;
    }

    public String getRide_category() {
        return ride_category;
    }

    public void setRide_category(String ride_category) {
        this.ride_category = ride_category;
    }

    public String getDisplay_status() {
        return display_status;
    }

    public void setDisplay_status(String display_status) {
        this.display_status = display_status;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_review() {
        return user_review;
    }

    public void setUser_review(String user_review) {
        this.user_review = user_review;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getS_no() {
        return s_no;
    }

    public void setS_no(String s_no) {
        this.s_no = s_no;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }
}
