package com.blackmaria.partner.Pojo;

import java.io.Serializable;

/**
 * Created by Prem Kumar and Anitha on 8/16/2016.
 */
public class MultiDropPojo implements Serializable
{
    String placeName,placeLat,placeLong;

    public String getPlaceLat() {
        return placeLat;
    }

    public void setPlaceLat(String placeLat) {
        this.placeLat = placeLat;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getPlaceLong() {
        return placeLong;
    }

    public void setPlaceLong(String placeLong) {
        this.placeLong = placeLong;
    }
}
