package com.blackmaria.partner.Pojo;

import java.io.Serializable;


public class MultipleLocationPojo implements Serializable {

    private String DropLocation, DropLat, DropLon, isEnd, mode, recordId;

    public String getIsEnd() {
        return isEnd;
    }

    public void setIsEnd(String isEnd) {
        this.isEnd = isEnd;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getDropLat() {
        return DropLat;
    }

    public void setDropLat(String dropLat) {
        DropLat = dropLat;
    }

    public String getDropLocation() {
        return DropLocation;
    }

    public void setDropLocation(String dropLocation) {
        DropLocation = dropLocation;
    }

    public String getDropLon() {
        return DropLon;
    }

    public void setDropLon(String dropLon) {
        DropLon = dropLon;
    }
}
