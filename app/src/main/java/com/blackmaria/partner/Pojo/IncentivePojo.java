package com.blackmaria.partner.Pojo;

/**
 * Created by Prem Kumar and Anitha on 3/13/2017.
 */

public class IncentivePojo
{
    String sNo,sDate,sTrip,sMileage,sHours,sReferral,sOthers;

    public String getsDate() {
        return sDate;
    }

    public void setsDate(String sDate) {
        this.sDate = sDate;
    }

    public String getsTrip() {
        return sTrip;
    }

    public void setsTrip(String sTrip) {
        this.sTrip = sTrip;
    }

    public String getsMileage() {
        return sMileage;
    }

    public void setsMileage(String sMileage) {
        this.sMileage = sMileage;
    }

    public String getsHours() {
        return sHours;
    }

    public void setsHours(String sHours) {
        this.sHours = sHours;
    }

    public String getsReferral() {
        return sReferral;
    }

    public void setsReferral(String sReferral) {
        this.sReferral = sReferral;
    }

    public String getsOthers() {
        return sOthers;
    }

    public void setsOthers(String sOthers) {
        this.sOthers = sOthers;
    }

    public String getsNo() {
        return sNo;
    }

    public void setsNo(String sNo) {
        this.sNo = sNo;
    }
}
