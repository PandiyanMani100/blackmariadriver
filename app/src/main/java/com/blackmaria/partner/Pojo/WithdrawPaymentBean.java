package com.blackmaria.partner.Pojo;

/**
 * Created by user127 on 19-05-2017.
 */

public class WithdrawPaymentBean {

    String name, code, active_icon, inactive_icon;

    public WithdrawPaymentBean() {
        super();
    }

    public WithdrawPaymentBean(String name, String code, String active_icon, String inactive_icon) {
        this.name = name;
        this.code = code;
        this.active_icon = active_icon;
        this.inactive_icon = inactive_icon;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    public String getActive_icon() {
        return active_icon;
    }

    public String getInactive_icon() {
        return inactive_icon;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setActive_icon(String active_icon) {
        this.active_icon = active_icon;
    }

    public void setInactive_icon(String inactive_icon) {
        this.inactive_icon = inactive_icon;
    }
}
