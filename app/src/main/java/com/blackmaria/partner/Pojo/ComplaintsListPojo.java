package com.blackmaria.partner.Pojo;

/**
 * Created by user144 on 3/8/2018.
 */

public class ComplaintsListPojo {
    String subName;
    String tickNumer;
    String ticketStatus;
    String dateTime;
    String UserImage;
    public String getUserImage() {
        return UserImage;
    }

    public void setUserImage(String userImage) {
        UserImage = userImage;
    }


    public String getSubName() {
        return subName;
    }

    public void setSubName(String subName) {
        this.subName = subName;
    }


    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }


    public String getTicketStatus() {
        return ticketStatus;
    }

    public void setTicketStatus(String ticketStatus) {
        this.ticketStatus = ticketStatus;
    }



    public String getTickNumer() {
        return tickNumer;
    }

    public void setTickNumer(String tickNumer) {
        this.tickNumer = tickNumer;
    }



}
