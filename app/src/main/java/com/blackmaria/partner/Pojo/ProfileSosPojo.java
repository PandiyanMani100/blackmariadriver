package com.blackmaria.partner.Pojo;

/**
 * Created by user14 on 3/16/2016.
 */
public class ProfileSosPojo {

    private String sosName ="";
    private String sosMobileNo ="";
    private String soscountryCode ="";
    private String sosEmaiId ="";
    private String verifyStatus ="";
    private String deleteStatus ="";
    private String index ="";

    public String getSosEmaiId() {
        return sosEmaiId;
    }

    public void setSosEmaiId(String sosEmaiId) {
        this.sosEmaiId = sosEmaiId;
    }

    public String getSosMobileNo() {
        return sosMobileNo;
    }

    public void setSosMobileNo(String sosMobileNo) {
        this.sosMobileNo = sosMobileNo;
    }

    public String getSosName() {
        return sosName;
    }

    public void setSosName(String sosName) {
        this.sosName = sosName;
    }

    public String getVerifyStatus() {
        return verifyStatus;
    }

    public void setVerifyStatus(String verifyStatus) {
        this.verifyStatus = verifyStatus;
    }

    public String getSoscountryCode() {
        return soscountryCode;
    }

    public void setSoscountryCode(String soscountryCode) {
        this.soscountryCode = soscountryCode;
    }

    public String getDeleteStatus() {
        return deleteStatus;
    }

    public void setDeleteStatus(String deleteStatus) {
        this.deleteStatus = deleteStatus;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }
}
