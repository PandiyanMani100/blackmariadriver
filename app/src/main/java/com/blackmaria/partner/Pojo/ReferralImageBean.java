package com.blackmaria.partner.Pojo;

/**
 * Created by user127 on 18-05-2017.
 */

public class ReferralImageBean {

    String driver_name, driver_image;

    public ReferralImageBean() {
        super();
    }

    public ReferralImageBean(String driver_name, String driver_image) {
        this.driver_name = driver_name;
        this.driver_image = driver_image;
    }

    public String getDriver_name() {
        return driver_name;
    }

    public void setDriver_name(String driver_name) {
        this.driver_name = driver_name;
    }

    public String getDriver_image() {
        return driver_image;
    }

    public void setDriver_image(String driver_image) {
        this.driver_image = driver_image;
    }
}
