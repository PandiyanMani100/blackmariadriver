package com.blackmaria.partner.Pojo;

/**
 * Created by user127 on 15-05-2017.
 */

public class MonthDetail {

    String week, WeekCount, ridecount, distance, income, currency, startdate, endate, startdate_f, endate_f;

    public MonthDetail() {

    }

    public MonthDetail(String week, String weekCount, String ridecount, String distance, String income, String currency, String startdate, String endate, String startdate_f, String endate_f) {
        this.week = week;
        WeekCount = weekCount;
        this.ridecount = ridecount;
        this.distance = distance;
        this.income = income;
        this.currency = currency;
        this.startdate = startdate;
        this.endate = endate;
        this.startdate_f = startdate_f;
        this.endate_f = endate_f;
    }

    public String getWeek() {
        return week;
    }

    public void setWeek(String week) {
        this.week = week;
    }

    public String getWeekCount() {
        return WeekCount;
    }

    public void setWeekCount(String weekCount) {
        WeekCount = weekCount;
    }

    public String getRidecount() {
        return ridecount;
    }

    public void setRidecount(String ridecount) {
        this.ridecount = ridecount;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getEndate() {
        return endate;
    }

    public void setEndate(String endate) {
        this.endate = endate;
    }

    public String getStartdate_f() {
        return startdate_f;
    }

    public void setStartdate_f(String startdate_f) {
        this.startdate_f = startdate_f;
    }

    public String getEndate_f() {
        return endate_f;
    }

    public void setEndate_f(String endate_f) {
        this.endate_f = endate_f;
    }
}
