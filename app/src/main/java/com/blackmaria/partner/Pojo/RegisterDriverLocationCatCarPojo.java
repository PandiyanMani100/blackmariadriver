package com.blackmaria.partner.Pojo;

/**
 * Created by user144 on 11/7/2017.
 */

public class RegisterDriverLocationCatCarPojo {
    String catId;
    String catName;
    String seatsCount;
    String offerType;
    String vehicleType;
    String description;
    String catIcon;

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    String cc;

    public RegisterDriverLocationCatCarPojo() {
        super();
    }

    public RegisterDriverLocationCatCarPojo(String cc,String catId, String catName, String seatsCount, String offerType, String vehicleType, String description, String catIcon) {
        this.cc = cc;
        this.catId = catId;
        this.catName = catName;
        this.seatsCount = seatsCount;
        this.offerType = offerType;
        this.vehicleType = vehicleType;
        this.description = description;
        this.catIcon = catIcon;
    }

    public String getCatIcon() {
        return catIcon;
    }

    public void setCatIcon(String catIcon) {
        this.catIcon = catIcon;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }


    public String getOfferType() {
        return offerType;
    }

    public void setOfferType(String offerType) {
        this.offerType = offerType;
    }


    public String getSeatsCount() {
        return seatsCount;
    }

    public void setSeatsCount(String seatsCount) {
        this.seatsCount = seatsCount;
    }


    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }


    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }


}
