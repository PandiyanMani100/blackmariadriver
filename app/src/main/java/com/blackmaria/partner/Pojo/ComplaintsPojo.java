package com.blackmaria.partner.Pojo;

/**
 * Created by user88 on 1/27/2017.
 */

public class ComplaintsPojo {
    private String subjectName, subjectId, optionsId, status;

    public String getsubjectName() {
        return subjectName;
    }

    public void setsubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getsubjectId() {
        return subjectId;
    }

    public void setsubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getoptionsId() {
        return optionsId;
    }

    public void setoptionsId(String optionsId) {
        this.optionsId = optionsId;
    }

    public String getReasonStatus() {
        return status;
    }

    public void setReasonStatus(String status) {
        this.status = status;
    }

}
