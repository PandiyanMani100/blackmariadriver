package com.blackmaria.partner.Pojo;

/**
 * Created by user127 on 12-06-2017.
 */

public class IncentiveStatementDetail {

    String week, week_trip, daily_trip, daily_mile, schedule_hours, sos;

    public IncentiveStatementDetail() {
        super();
    }

    public IncentiveStatementDetail(String week, String week_trip, String daily_trip, String daily_mile, String schedule_hours, String sos) {
        this.week = week;
        this.week_trip = week_trip;
        this.daily_trip = daily_trip;
        this.daily_mile = daily_mile;
        this.schedule_hours = schedule_hours;
        this.sos = sos;
    }

    public String getWeek() {
        return week;
    }

    public void setWeek(String week) {
        this.week = week;
    }

    public String getWeek_trip() {
        return week_trip;
    }

    public void setWeek_trip(String week_trip) {
        this.week_trip = week_trip;
    }

    public String getDaily_trip() {
        return daily_trip;
    }

    public void setDaily_trip(String daily_trip) {
        this.daily_trip = daily_trip;
    }

    public String getDaily_mile() {
        return daily_mile;
    }

    public void setDaily_mile(String daily_mile) {
        this.daily_mile = daily_mile;
    }

    public String getSchedule_hours() {
        return schedule_hours;
    }

    public void setSchedule_hours(String schedule_hours) {
        this.schedule_hours = schedule_hours;
    }

    public String getSos() {
        return sos;
    }

    public void setSos(String sos) {
        this.sos = sos;
    }
}
