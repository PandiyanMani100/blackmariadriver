package com.blackmaria.partner.Pojo;

/**
 * Created by user127 on 12-06-2017.
 */

public class DriveEarnStatementChart {

    String day, ridecount, distance;

    public DriveEarnStatementChart() {
        super();
    }

    public DriveEarnStatementChart(String day, String ridecount, String distance) {
        this.day = day;
        this.ridecount = ridecount;
        this.distance = distance;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getRidecount() {
        return ridecount;
    }

    public void setRidecount(String ridecount) {
        this.ridecount = ridecount;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }
}
