package com.blackmaria.partner.Pojo;

import java.io.Serializable;

/**
 * Created by user127 on 22-05-2017.
 */

public class KeyValueBean implements Serializable {

    String key, value;

    public KeyValueBean() {
        super();
    }

    public KeyValueBean(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
