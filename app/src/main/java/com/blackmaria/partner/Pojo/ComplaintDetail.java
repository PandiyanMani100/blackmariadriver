package com.blackmaria.partner.Pojo;

import java.io.Serializable;

/**
 * Created by user127 on 12-07-2017.
 */

public class ComplaintDetail implements Serializable{

    String user_image, ride_id, user_name, ticket_number, location_name, title, subtitle, comments, phone_number;

    public ComplaintDetail() {
        super();
    }

    public ComplaintDetail(String user_image, String ride_id, String user_name, String ticket_number, String location_name, String title, String subtitle, String comments, String phone_number) {
        this.user_image = user_image;
        this.ride_id = ride_id;
        this.user_name = user_name;
        this.ticket_number = ticket_number;
        this.location_name = location_name;
        this.title = title;
        this.subtitle = subtitle;
        this.comments = comments;
        this.phone_number = phone_number;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getRide_id() {
        return ride_id;
    }

    public void setRide_id(String ride_id) {
        this.ride_id = ride_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getTicket_number() {
        return ticket_number;
    }

    public void setTicket_number(String ticket_number) {
        this.ticket_number = ticket_number;
    }

    public String getLocation_name() {
        return location_name;
    }

    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }
}
