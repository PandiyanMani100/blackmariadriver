package com.blackmaria.partner.Pojo;

/**
 * Created by Ganesh on 12-06-2017.
 */

public class IncentiveDetail {

    String incetives_type, target, completed_level, target_km, target_distance, incetive_amount, currency, incentive_period, current_period, day_time;

    public IncentiveDetail() {
        super();
    }

    public IncentiveDetail(String incetives_type, String target, String completed_level, String target_km, String target_distance, String incetive_amount, String currency, String incentive_period, String current_period, String day_time) {
        this.incetives_type = incetives_type;
        this.target = target;
        this.completed_level = completed_level;
        this.target_km = target_km;
        this.target_distance = target_distance;
        this.incetive_amount = incetive_amount;
        this.currency = currency;
        this.incentive_period = incentive_period;
        this.current_period = current_period;
        this.day_time = day_time;
    }

    public String getIncetives_type() {
        return incetives_type;
    }

    public void setIncetives_type(String incetives_type) {
        this.incetives_type = incetives_type;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getCompleted_level() {
        return completed_level;
    }

    public void setCompleted_level(String completed_level) {
        this.completed_level = completed_level;
    }

    public String getTarget_km() {
        return target_km;
    }

    public void setTarget_km(String target_km) {
        this.target_km = target_km;
    }

    public String getTarget_distance() {
        return target_distance;
    }

    public void setTarget_distance(String target_distance) {
        this.target_distance = target_distance;
    }

    public String getIncetive_amount() {
        return incetive_amount;
    }

    public void setIncetive_amount(String incetive_amount) {
        this.incetive_amount = incetive_amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getIncentive_period() {
        return incentive_period;
    }

    public void setIncentive_period(String incentive_period) {
        this.incentive_period = incentive_period;
    }

    public String getCurrent_period() {
        return current_period;
    }

    public void setCurrent_period(String current_period) {
        this.current_period = current_period;
    }

    public String getDay_time() {
        return day_time;
    }

    public void setDay_time(String day_time) {
        this.day_time = day_time;
    }
}
