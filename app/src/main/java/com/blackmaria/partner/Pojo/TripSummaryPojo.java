package com.blackmaria.partner.Pojo;

/**
 * Created by user127 on 18-05-2017.
 */

public class TripSummaryPojo {

    String title, value, currency_status;

    public TripSummaryPojo() {
        super();
    }

    public TripSummaryPojo(String title, String value, String currency_status) {
        this.title = title;
        this.value = value;
        this.currency_status = currency_status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCurrency_status() {
        return currency_status;
    }

    public void setCurrency_status(String currency_status) {
        this.currency_status = currency_status;
    }
}
