package com.blackmaria.partner.Pojo;

/**
 * Created by Prem Kumar and Anitha on 10/23/2015.
 */
public class WalletMoneyTransactionPojo {
    private String trans_type;
    private String trans_amount;
    private String title;
    private String trans_date;
    private String trans_time;
    private String formatedDate;
    private String balance_amount;
    private String currencySymbol;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    private String image;

    public WalletMoneyTransactionPojo() {
    }

    public WalletMoneyTransactionPojo(String trans_type, String trans_amount, String title, String trans_date, String trans_time, String formatedDate, String balance_amount, String currencySymbol) {
        this.trans_type = trans_type;
        this.trans_amount = trans_amount;
        this.title = title;
        this.trans_date = trans_date;
        this.trans_time = trans_time;
        this.formatedDate = formatedDate;
        this.balance_amount = balance_amount;
        this.currencySymbol = currencySymbol;
    }

    public String getTrans_type() {
        return trans_type;
    }

    public void setTrans_type(String trans_type) {
        this.trans_type = trans_type;
    }

    public String getTrans_amount() {
        return trans_amount;
    }

    public void setTrans_amount(String trans_amount) {
        this.trans_amount = trans_amount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTrans_date() {
        return trans_date;
    }

    public void setTrans_date(String trans_date) {
        this.trans_date = trans_date;
    }

    public String getTrans_time() {
        return trans_time;
    }

    public void setTrans_time(String trans_time) {
        this.trans_time = trans_time;
    }

    public String getBalance_amount() {
        return balance_amount;
    }

    public void setBalance_amount(String balance_amount) {
        this.balance_amount = balance_amount;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }


    public String getFormatedDate() {
        return formatedDate;
    }

    public void setFormatedDate(String formatedDate) {
        this.formatedDate = formatedDate;
    }
}
