package com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.mytrips;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityMonthviewdetailConstrainBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;

import java.util.HashMap;

public class MonthviewdetailViewModel extends ViewModel implements ApIServices.completelisner {

    private Activity context;
    private Dialog dialog;
    private MutableLiveData<String> monthresponse = new MutableLiveData<>();
    private SessionManager sessionManager;
    private String driver_id="";
    private ActivityMonthviewdetailConstrainBinding binding;

    public MonthviewdetailViewModel(Activity context) {
        this.context = context;
        sessionManager = SessionManager.getInstance(context);
        HashMap<String, String> user = sessionManager.getUserDetails();
        driver_id = user.get(SessionManager.KEY_DRIVERID);
    }

    public MutableLiveData<String> getMonthresponse() {
        return monthresponse;
    }

    public void getmonthwiseapihit(String sFilterMonth,String sFilterYear,boolean value){
        if(value) {
            binding.apiload.setVisibility(View.VISIBLE);
            dialog = new Dialog(context, R.style.CustomDialogTheme);
            dialog.getWindow();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.loading_model);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", driver_id);
        jsonParams.put("month", sFilterMonth);
        jsonParams.put("year", sFilterYear);
        ApIServices apIServices = new ApIServices(context, MonthviewdetailViewModel.this);
        apIServices.dooperation(Iconstant.month_trips_details_url, jsonParams);
    }

    public void back(){
        context.onBackPressed();
    }

    @Override
    public void sucessresponse(String val) {
        if (dialog != null) {
            dialog.dismiss();
            binding.apiload.setVisibility(View.INVISIBLE);
        }
        monthresponse.postValue(val);
    }

    @Override
    public void errorreponse() {
        if (dialog != null) {
            dialog.dismiss();
            binding.apiload.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void jsonexception() {
        if (dialog != null) {
            dialog.dismiss();
            binding.apiload.setVisibility(View.INVISIBLE);
        }
    }

    public void setIds(ActivityMonthviewdetailConstrainBinding binding) {
        this.binding = binding;
    }
}
