package com.blackmaria.partner.latestpackageview.Factory.Sidemenu.Maintanence;

import android.app.Activity;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackmaria.partner.latestpackageview.vieewmodel.sidemenu.maintanence.MaintanenceopendashboardViewModel;
import com.blackmaria.partner.latestpackageview.vieewmodel.sidemenu.maintanence.MaintanenceopendashboardViewModel1;


public class MaintanenceopendashboardFactory1 extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public MaintanenceopendashboardFactory1(Activity context) {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new MaintanenceopendashboardViewModel1(context);
    }
}
