package com.blackmaria.partner.latestpackageview.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.Model.MessagesPojo;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.HashMap;

public class MessageListviewAdapter1 extends BaseAdapter {
    Context context;

    LayoutInflater inflater;
    View itemView;
    int flag;

    private ConnectionDetector cd;
    private ServiceRequest mRequest;
    private Boolean isInternetPresent = false;
    private SessionManager sessionManager;
    private String UserId = "";
    ArrayList<MessagesPojo> data;
    MessageView messageView;

    public MessageListviewAdapter1(Context context, int flag, ArrayList<MessagesPojo> d, MessageView messageView) {
        this.context = context;

        this.flag = flag;
        this.data = d;
        this.messageView = messageView;

        cd = new ConnectionDetector(context);
        isInternetPresent = cd.isConnectingToInternet();
        sessionManager = new SessionManager(context);
        HashMap<String, String> user = sessionManager.getUserDetails();
        UserId = user.get(SessionManager.KEY_DRIVERID);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.size();
    }

    @Override
    public long getItemId(int i) {

        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        TextView Smessage_title_textview, date, readmore;
        final RadioButton radio;
        ImageView banner;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        itemView = inflater.inflate(R.layout.info_message_single_contrain, viewGroup, false);
        Smessage_title_textview = itemView.findViewById(R.id.message_title_textview);
        banner = itemView.findViewById(R.id.banner);
        date = itemView.findViewById(R.id.date);
        readmore = itemView.findViewById(R.id.readmore);
        radio = itemView.findViewById(R.id.radio);

        Smessage_title_textview.setText(data.get(i).getTitle());
        date.setText(data.get(i).getDate());

        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.be_partner_banner)
                .error(R.drawable.be_partner_banner)
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        Glide.with(context).load(data.get(i).getIMage()).apply(options).into(banner);

        radio.setChecked(data.get(i).isIschecked());


        radio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checked = ((RadioButton) v).isChecked();
                // Check which radiobutton was pressed
                if (data.get(i).isIschecked()) {
                    MessagesPojo pojo = new MessagesPojo();
                    pojo.setDate(data.get(i).getDate());
                    pojo.setIMage(data.get(i).getIMage());
                    pojo.setTitle(data.get(i).getTitle());
                    pojo.setDescription(data.get(i).getDescription());
                    pojo.setNotification_id(data.get(i).getNotification_id());
                    pojo.setIschecked(false);
                    data.set(i, pojo);
                    notifyDataSetChanged();
                } else {
                    MessagesPojo pojo = new MessagesPojo();
                    pojo.setDate(data.get(i).getDate());
                    pojo.setIMage(data.get(i).getIMage());
                    pojo.setTitle(data.get(i).getTitle());
                    pojo.setDescription(data.get(i).getDescription());
                    pojo.setNotification_id(data.get(i).getNotification_id());
                    pojo.setIschecked(true);
                    data.set(i, pojo);
                    notifyDataSetChanged();
                }
            }
        });


        readmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                messageView.onClickview(i, data.get(i));
            }
        });

        return itemView;
    }


    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(context);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(context.getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    public interface MessageView {
        void onClickview(int position, MessagesPojo pojo);
    }

}
