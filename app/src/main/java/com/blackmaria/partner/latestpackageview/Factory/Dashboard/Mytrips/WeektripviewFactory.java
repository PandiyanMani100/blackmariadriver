package com.blackmaria.partner.latestpackageview.Factory.Dashboard.Mytrips;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.mytrips.WeektripViewModel;


public class WeektripviewFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public WeektripviewFactory(Activity context) {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new WeektripViewModel(context);
    }
}
