package com.blackmaria.partner.latestpackageview.Factory.Sidemenu;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackmaria.partner.latestpackageview.vieewmodel.sidemenu.RatinghomeViewModel;


public class RatingHomepageFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public RatingHomepageFactory(Activity context)
    {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass)
    {
        return (T) new RatinghomeViewModel(context);
    }
}
