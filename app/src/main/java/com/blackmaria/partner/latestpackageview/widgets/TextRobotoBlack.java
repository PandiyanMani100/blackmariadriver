package com.blackmaria.partner.latestpackageview.widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by user127 on 02-06-2017.
 */

@SuppressLint("AppCompatCustomView")
public class TextRobotoBlack extends TextView {

    public TextRobotoBlack(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public TextRobotoBlack(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextRobotoBlack(Context context) {
        super(context);
        init();
    }


    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Roboto-Black.ttf");
        setTypeface(tf);
    }
}
