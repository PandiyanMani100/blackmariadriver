package com.blackmaria.partner.latestpackageview.Factory.Sidemenu.Profile;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackmaria.partner.latestpackageview.vieewmodel.sidemenu.profile.BankcardviewpagerViewModel;


public class BankFragmentFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public BankFragmentFactory(Activity context) {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new BankcardviewpagerViewModel(context);
    }
}
