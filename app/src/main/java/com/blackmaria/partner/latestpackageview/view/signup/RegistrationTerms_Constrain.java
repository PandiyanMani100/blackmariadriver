package com.blackmaria.partner.latestpackageview.view.signup;

import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.databinding.RegisterTermsAndConditionsConstrainBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApiIntentService;
import com.blackmaria.partner.latestpackageview.Factory.SignUp.RegisterationTermsFactory;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.vieewmodel.signup.RegistrationTermsViewModel;

import java.util.HashMap;

public class RegistrationTerms_Constrain extends AppCompatActivity {

    AccessLanguagefromlocaldb db;

    private RegistrationTermsViewModel registrationTermsViewModel;
    private AppUtils appUtils;
    private String url = "http://blackmaria.casperon.co";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
        appUtils = AppUtils.getInstance(this);
        final RegisterTermsAndConditionsConstrainBinding binding = DataBindingUtil.setContentView(this, R.layout.register_terms_and_conditions_constrain);
        registrationTermsViewModel = ViewModelProviders.of(this, new RegisterationTermsFactory(this)).get(RegistrationTermsViewModel.class);
        binding.setRegistrationTermsViewModel(registrationTermsViewModel);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        Intent intents = new Intent(RegistrationTerms_Constrain.this, ApiIntentService.class);
        intents.putExtra("params", jsonParams);
        intents.putExtra("url", Iconstant.getvehicledetails);
        startService(intents);
        binding.btnRegisterNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!binding.eula.isChecked()) {
                    appUtils.AlertError(RegistrationTerms_Constrain.this,db.getvalue("action_error"),db.getvalue("agreeeula"));
                } else if (!binding.privacy.isChecked()) {
                    appUtils.AlertError(RegistrationTerms_Constrain.this,db.getvalue("action_error"),db.getvalue("agreeprivacy"));
                } else if (!binding.terms.isChecked()) {
                    appUtils.AlertError(RegistrationTerms_Constrain.this,db.getvalue("action_error"),db.getvalue("agreetera"));
                } else {
                    Intent intent = new Intent(RegistrationTerms_Constrain.this, RegistrationSelectLocation_constrain.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
                }
            }
        });

        binding.txtLabelMagic.setText(db.getvalue("magicofdriver"));
        binding.signupasparter.setText(db.getvalue("signupparter"));
        binding.joiningpartner.setText(db.getvalue("joining"));
        binding.eulatv.setText(db.getvalue("label_eula"));
        binding.privacytv.setText(db.getvalue("label_privacy_policy"));
        binding.ivtermstv.setText(db.getvalue("label_terms_and_conditions"));
        binding.btnRegisterNow.setText(db.getvalue("label_register"));

        binding.eulatv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appUtils.intentbrowser(RegistrationTerms_Constrain.this,Iconstant.privacyurl);
            }
        });

        binding.privacytv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appUtils.intentbrowser(RegistrationTerms_Constrain.this,Iconstant.privacyurl);
            }
        });

        binding.ivtermstv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appUtils.intentbrowser(RegistrationTerms_Constrain.this,Iconstant.termsurl);
            }
        });

    }
}
