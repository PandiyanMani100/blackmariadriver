package com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet.withdraw.bank;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import android.content.Intent;

import com.blackmaria.partner.databinding.ActivityNobankaccountConstrainBinding;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Dashboard_constrain;

public class NobankaccountViewModel extends ViewModel {
    private Activity context;
    private ActivityNobankaccountConstrainBinding binding;

    public NobankaccountViewModel(Activity context) {
        this.context = context;
    }

    public void setIds(ActivityNobankaccountConstrainBinding binding) {
        this.binding = binding;
    }

    public void back() {
        context.onBackPressed();
    }

    public void cancel() {
        context.startActivity(new Intent(context, Dashboard_constrain.class));
        context.finish();
    }
}
