package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class uploadimagepojo implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }
    public class Response {
        @SerializedName("image_name")
        private String image_name;
        @SerializedName("image_path")
        private String image_path;


        // Getter Methods

        public String getImage_name() {
            return image_name;
        }

        public String getImage_path() {
            return image_path;
        }

        // Setter Methods

        public void setImage_name(String image_name) {
            this.image_name = image_name;
        }

        public void setImage_path(String image_path) {
            this.image_path = image_path;
        }
    }

}

