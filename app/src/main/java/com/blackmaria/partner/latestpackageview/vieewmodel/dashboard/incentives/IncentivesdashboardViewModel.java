package com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.incentives;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import com.blackmaria.partner.R;
import com.blackmaria.partner.databinding.ActivityIncentivedashbaordConstrainBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Incentives.MyincentivesConstrain;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

public class IncentivesdashboardViewModel extends ViewModel implements ApIServices.completelisner {
    private Activity context;
    private ActivityIncentivedashbaordConstrainBinding binding;
    private Dialog dialog;
    private int count = 0;
    private MutableLiveData<String> incentiveresponse = new MutableLiveData<>();
    private MutableLiveData<String> incentivewithdrawresponse = new MutableLiveData<>();

    public IncentivesdashboardViewModel(Activity context) {
        this.context = context;
    }

    public void setIds(ActivityIncentivedashbaordConstrainBinding binding) {
        this.binding = binding;
    }

    public MutableLiveData<String> getIncentivewithdrawresponse() {
        return incentivewithdrawresponse;
    }

    public MutableLiveData<String> getIncentiveresponse() {
        return incentiveresponse;
    }

    public void incentivedasboardapihit(HashMap<String, String> jsonParams) {
        count = 0;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ApIServices apIServices = new ApIServices(context, IncentivesdashboardViewModel.this);
        apIServices.dooperation(Iconstant.incentives_dashboard_url, jsonParams);
    }

    public void incentivewithdrawapihit(HashMap<String, String> jsonParams) {
        count = 1;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ApIServices apIServices = new ApIServices(context, IncentivesdashboardViewModel.this);
        apIServices.dooperation(Iconstant.incentives_withdraw_amount_url, jsonParams);
    }

    @Override
    public void sucessresponse(String val) {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        if (count == 0) {
            incentiveresponse.postValue(val);
        } else if (count == 1) {
            incentivewithdrawresponse.postValue(val);
        }

    }

    @Override
    public void errorreponse() {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
    }

    @Override
    public void jsonexception() {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        AppUtils.toastprint(context, "Json Exception");
    }

    public void back() {
        context.onBackPressed();
    }

    public void viewstatement() {
        context.startActivity(new Intent(context, MyincentivesConstrain.class));
//        EventBus.getDefault().unregister(this);
        if (EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);}
    }
}
