package com.blackmaria.partner.latestpackageview.view.sidemenu.Maintanence;

import android.app.Activity;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.databinding.EditVehicleDetailsConstrainBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.Factory.Sidemenu.Maintanence.EditvehicledetailsFactory;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Dashboard_constrain;
import com.blackmaria.partner.latestpackageview.vieewmodel.sidemenu.maintanence.EditvehicledetailsViewModel;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.yalantis.ucrop.UCrop;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;

public class EditvehicledetailsConstrain extends AppCompatActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener, EditvehicledetailsViewModel.registerclickreturn {

    private EditVehicleDetailsConstrainBinding binding;
    private EditvehicledetailsViewModel editvehicledetailsViewModel;
    private static final int PERMISSION_REQUEST_CODE = 111;
    private static final int SELECT_IMAGE_REQUEST = 011;
    private static final int TAKE_PHOTO_REQUEST = 022;
    private int year, month, day, pickerNumber = 0;
    private Calendar calendar;
    private DatePickerDialog pickerDialog;
    private String strRoadTax = "", strLicenseTax = "", strInsuranceExp = "";
    private AppUtils appUtils;
    AccessLanguagefromlocaldb db;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new   AccessLanguagefromlocaldb(this);
        binding = DataBindingUtil.setContentView(this, R.layout.edit_vehicle_details_constrain);
        editvehicledetailsViewModel = ViewModelProviders.of(this, new EditvehicledetailsFactory(this)).get(EditvehicledetailsViewModel.class);
        binding.setEditvehicledetailsViewModel(editvehicledetailsViewModel);

        editvehicledetailsViewModel.setIds(binding);
        editvehicledetailsViewModel.setregisterclickreturn(this);

        appUtils = AppUtils.getInstance(this);
        binding.roadtax.setOnClickListener(this);
        binding.drivinglicense.setOnClickListener(this);
        binding.insurance.setOnClickListener(this);
        binding.imgBack.setOnClickListener(this);

        binding.personalinfo.setText(db.getvalue("vehicleinfo"));
        binding.expidate.setText(db.getvalue("expiry_date"));
        binding.vehiclename.setText(db.getvalue("vehicle_photo_n_upload"));

        binding.roadtax.setHint(db.getvalue("roadtax"));
        binding.insurance.setHint(db.getvalue("insurance"));
        binding.drivinglicense.setHint(db.getvalue("driving_license"));
        binding.platenumber.setText(db.getvalue("plate_number"));
        binding.vetype.setText(db.getvalue("vehicle_type"));
        binding.vemaker.setText(db.getvalue("vehicle_maker"));
        binding.txtvehicletype.setText(db.getvalue("select"));
        binding.txtvehiclemaker.setText(db.getvalue("select"));
        binding.txtvehiclmodel.setText(db.getvalue("vehicle_model"));
        binding.modelnotlisted.setHint(db.getvalue("type_model_if_not_listed"));
        binding.txtyears.setText(db.getvalue("select"));
        binding.yom.setText(db.getvalue("manufacturer"));
        binding.btnConfirm.setText(db.getvalue("update"));

        editvehicledetailsViewModel.getvehicledetails(Iconstant.getdrivervehicledetails);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // If permissionCode is 0 -> Its For Camera
                    // If permissionCode is 1 -> Its For Pick From Gallery
                    editvehicledetailsViewModel.uploadpohoto();
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_IMAGE_REQUEST) {
                editvehicledetailsViewModel.onSelectFromGalleryResult(data);
            } else if (requestCode == TAKE_PHOTO_REQUEST) {
                editvehicledetailsViewModel.onCaptureImageResult(data);
            } else if (requestCode == UCrop.REQUEST_CROP) {
                editvehicledetailsViewModel.onCroppedImageResult(data);
            }
        } else if (resultCode == UCrop.RESULT_ERROR) {
            final Throwable cropError = UCrop.getError(data);
        }
    }

    private void showDatePicker() {

        pickerDialog = DatePickerDialog.newInstance(this, year, month, day);
        pickerDialog.setThemeDark(false);
        pickerDialog.showYearPickerFirst(false);
        pickerDialog.setAccentColor(getResources().getColor(R.color.signin_and_signup_slider_ball_blue));
        pickerDialog.setMinDate(Calendar.getInstance());
        pickerDialog.show(getFragmentManager(), "DatePickerDialog");
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
        datePickerDialog.dismiss();
        int months = month + 1;

        this.year = year;
        this.month = month;
        this.day = day;

        if (pickerNumber == 1) {
            binding.roadtax.setText(day + "/" + months + "/" + year);
            strRoadTax = day + "/" + months + "/" + year;
        } else if (pickerNumber == 2) {
            binding.drivinglicense.setText(day + "/" + months + "/" + year);
            strLicenseTax = day + "/" + months + "/" + year;
        } else if (pickerNumber == 3) {
            binding.insurance.setText(day + "/" + months + "/" + year);
            strInsuranceExp = day + "/" + months + "/" + year;
        }
    }

    @Override
    public void onClick(View view) {
        if (view == binding.roadtax) {
            CloseKeyboardNew();
            pickerNumber = 1;
            showDatePicker();
        } else if (view == binding.drivinglicense) {
            CloseKeyboardNew();
            pickerNumber = 2;
            showDatePicker();
        } else if (view == binding.insurance) {
            CloseKeyboardNew();
            pickerNumber = 3;
            showDatePicker();
        } else if (view == binding.imgBack) {
            onBackPressed();
        }
    }

    private void CloseKeyboardNew() {
        try {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow((null == getCurrentFocus()) ? null : getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void movetonextscreen(HashMap<String, String> jsonParams) {
        editvehicledetailsViewModel.updatedrivervehicledetails(Iconstant.getdrivervehicledetails,jsonParams);
        editvehicledetailsViewModel.getUpdatevehicledetailsresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try{
                    JSONObject jsonObject = new JSONObject(response);
                    if(jsonObject.getString("status").equalsIgnoreCase("1")){
                        appUtils.AlertSuccess(EditvehicledetailsConstrain.this,getResources().getString(R.string.action_success),jsonObject.getString("response"));
                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                               startActivity(new Intent(EditvehicledetailsConstrain.this, Dashboard_constrain.class));
                            }
                        }, 1000);
                    }else{
                        appUtils.AlertError(EditvehicledetailsConstrain.this,getResources().getString(R.string.action_error),jsonObject.getString("response"));
                    }
                }catch (JSONException e){e.printStackTrace();}
            }
        });
    }
}
