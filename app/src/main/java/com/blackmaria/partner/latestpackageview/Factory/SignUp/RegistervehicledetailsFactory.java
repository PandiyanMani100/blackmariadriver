package com.blackmaria.partner.latestpackageview.Factory.SignUp;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackmaria.partner.latestpackageview.vieewmodel.signup.RegistervehicledetailsViewModel;

public class RegistervehicledetailsFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public RegistervehicledetailsFactory(Activity context) {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new RegistervehicledetailsViewModel(context);
    }
}
