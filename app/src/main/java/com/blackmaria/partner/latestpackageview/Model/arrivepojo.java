package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class arrivepojo implements Serializable  {
    @SerializedName("status")
    private String status;
    @SerializedName("ride_view")
    private String ride_view;
    @SerializedName("response")
    private String response;
    @SerializedName("mode")
    private String mode;
    @SerializedName("drop_location")
    private String drop_location;
    @SerializedName("ride_id")
    private String ride_id;
    @SerializedName("title_text")
    private String title_text;
    @SerializedName("subtitle_text")
    private String subtitle_text;
    @SerializedName("est_cost")
    private String est_cost;
    @SerializedName("currency_code")
    private String currency_code;


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public String getRide_view() {
        return ride_view;
    }

    public String getResponse() {
        return response;
    }

    public String getMode() {
        return mode;
    }

    public String getDrop_location() {
        return drop_location;
    }

    public String getRide_id() {
        return ride_id;
    }

    public String getTitle_text() {
        return title_text;
    }

    public String getSubtitle_text() {
        return subtitle_text;
    }

    public String getEst_cost() {
        return est_cost;
    }

    public String getCurrency_code() {
        return currency_code;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setRide_view(String ride_view) {
        this.ride_view = ride_view;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public void setDrop_location(String drop_location) {
        this.drop_location = drop_location;
    }

    public void setRide_id(String ride_id) {
        this.ride_id = ride_id;
    }

    public void setTitle_text(String title_text) {
        this.title_text = title_text;
    }

    public void setSubtitle_text(String subtitle_text) {
        this.subtitle_text = subtitle_text;
    }

    public void setEst_cost(String est_cost) {
        this.est_cost = est_cost;
    }

    public void setCurrency_code(String currency_code) {
        this.currency_code = currency_code;
    }
}
