package com.blackmaria.partner.latestpackageview.Model;

public class Weekwisetrippojo {
    public String getWeek() {
        return week;
    }

    public void setWeek(String week) {
        this.week = week;
    }

    public String getTripcount() {
        return tripcount;
    }

    public void setTripcount(String tripcount) {
        this.tripcount = tripcount;
    }

    public String getTripdistance() {
        return tripdistance;
    }

    public void setTripdistance(String tripdistance) {
        this.tripdistance = tripdistance;
    }

    private String week;
    private String tripcount;
    private String tripdistance;
}
