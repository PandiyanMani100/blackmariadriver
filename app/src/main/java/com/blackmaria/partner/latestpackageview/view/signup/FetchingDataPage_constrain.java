package com.blackmaria.partner.latestpackageview.view.signup;

import android.app.Activity;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.pm.PackageManager;
import androidx.databinding.DataBindingUtil;
import android.location.Location;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.app.CloudTransferAmountPinEnter1;
import com.blackmaria.partner.databinding.ActivityFetchingDataPageConstrainBinding;
import com.blackmaria.partner.latestpackageview.Factory.SignUp.FetchingDataPage_factory;
import com.blackmaria.partner.latestpackageview.Utils.CheckGPSProvider;
import com.blackmaria.partner.latestpackageview.vieewmodel.signup.FetchingDataPageViewModel;
import com.blackmaria.partner.latestpackageview.location.Service.LocationUpdatesService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class FetchingDataPage_constrain extends AppCompatActivity implements FetchingDataPageViewModel.FetchingInterface, FetchingDataPageViewModel.Locationonalready {


    private FetchingDataPageViewModel fetchingDataPageViewModel;
    final static int REQUEST_LOCATION = 199;
    final int PERMISSION_REQUEST_CODE = 111;
    private ActivityFetchingDataPageConstrainBinding binding;
    private SessionManager sessionManager;
    AccessLanguagefromlocaldb db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = SessionManager.getInstance(this);
        db = new AccessLanguagefromlocaldb(FetchingDataPage_constrain.this);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_fetching_data_page_constrain);
        fetchingDataPageViewModel = ViewModelProviders.of(this, new FetchingDataPage_factory(this)).get(FetchingDataPageViewModel.class);
        binding.setFetchingDataPageViewModel(fetchingDataPageViewModel);

        EventBus.getDefault().register(this);
        binding.txtLabelFetchingData.setText(db.getvalue("fetching_data_label_fetching_data"));
        fetchingDataPageViewModel.startlocation(FetchingDataPage_constrain.this,  FetchingDataPage_constrain.this);
    }

    @Subscribe
    public void onMessageEvent(Location loc) {
        sessionManager.setLatitude(String.valueOf(loc.getLatitude()));
        sessionManager.setLongitude(String.valueOf(loc.getLongitude()));
        EventBus.getDefault().unregister(this);
        binding.txtLabelFetchingData.setText(db.getvalue("fetching_data_label_updating_location"));
        stopService(new Intent(this, LocationUpdatesService.class));
        fetchingDataPageViewModel.getversioncode(String.valueOf(loc.getLatitude()), String.valueOf(loc.getLongitude()));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void usercancelled(boolean val) {
        if (val) {
            fetchingDataPageViewModel.triggeragainlocation();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_LOCATION:
                switch (resultCode) {
                    case Activity.RESULT_OK: {
                        Toast.makeText(FetchingDataPage_constrain.this, "Location enabled!", Toast.LENGTH_LONG).show();
                        startService(new Intent(this, LocationUpdatesService.class));
                        break;
                    }
                    case Activity.RESULT_CANCELED: {
                        finish();
                        break;
                    }
                    default: {
                        break;
                    }
                }
                break;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    CheckGPSProvider checkGPSProvider = new CheckGPSProvider(FetchingDataPage_constrain.this);
                    if (checkGPSProvider.checkprovider()) {
                        startService(new Intent(this, LocationUpdatesService.class));
                    } else {
                        fetchingDataPageViewModel.triggeragainlocation();
                    }
                } else {
                    finish();
                }
                break;
        }
    }

    @Override
    public void locationonoff(boolean val) {
        startService(new Intent(this, LocationUpdatesService.class));
    }
}
