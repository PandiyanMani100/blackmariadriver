package com.blackmaria.partner.latestpackageview.widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Ganesh on 02-06-2017.
 */

@SuppressLint("AppCompatCustomView")
public class ButtonRobotoBold extends Button {

    public ButtonRobotoBold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public ButtonRobotoBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ButtonRobotoBold(Context context) {
        super(context);
        init();
    }


    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Roboto-Bold.ttf");
        setTypeface(tf);
    }
}
