package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class monthearningspojo implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }

    public class Response {
        @SerializedName("month")
        private String month;
        @SerializedName("year")
        private String year;

        public ArrayList<Monthreport> getMonthreport() {
            return monthreport;
        }

        public void setMonthreport(ArrayList<Monthreport> monthreport) {
            this.monthreport = monthreport;
        }

        @SerializedName("monthreport")
        ArrayList < Monthreport > monthreport = new ArrayList< Monthreport >();
        @SerializedName("total_rides")
        private String total_rides;
        @SerializedName("total_income")
        private String total_income;
        @SerializedName("currency")
        private String currency;


        public class Monthreport{
            @SerializedName("week")
            private String week;
            @SerializedName("WeekCount")
            private String WeekCount;
            @SerializedName("ridecount")
            private String ridecount;
            @SerializedName("distance")
            private String distance;
            @SerializedName("income")
            private String income;
            @SerializedName("currency")
            private String currency;
            @SerializedName("startdate")
            private String startdate;
            @SerializedName("endate")
            private String endate;
            @SerializedName("startdate_f")
            private String startdate_f;
            @SerializedName("endate_f")
            private String endate_f;


            // Getter Methods

            public String getWeek() {
                return week;
            }

            public String getWeekCount() {
                return WeekCount;
            }

            public String getRidecount() {
                return ridecount;
            }

            public String getDistance() {
                return distance;
            }

            public String getIncome() {
                return income;
            }

            public String getCurrency() {
                return currency;
            }

            public String getStartdate() {
                return startdate;
            }

            public String getEndate() {
                return endate;
            }

            public String getStartdate_f() {
                return startdate_f;
            }

            public String getEndate_f() {
                return endate_f;
            }

            // Setter Methods

            public void setWeek(String week) {
                this.week = week;
            }

            public void setWeekCount(String WeekCount) {
                this.WeekCount = WeekCount;
            }

            public void setRidecount(String ridecount) {
                this.ridecount = ridecount;
            }

            public void setDistance(String distance) {
                this.distance = distance;
            }

            public void setIncome(String income) {
                this.income = income;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public void setStartdate(String startdate) {
                this.startdate = startdate;
            }

            public void setEndate(String endate) {
                this.endate = endate;
            }

            public void setStartdate_f(String startdate_f) {
                this.startdate_f = startdate_f;
            }

            public void setEndate_f(String endate_f) {
                this.endate_f = endate_f;
            }
        }
        // Getter Methods

        public String getMonth() {
            return month;
        }

        public String getYear() {
            return year;
        }

        public String getTotal_rides() {
            return total_rides;
        }

        public String getTotal_income() {
            return total_income;
        }

        public String getCurrency() {
            return currency;
        }

        // Setter Methods

        public void setMonth(String month) {
            this.month = month;
        }

        public void setYear(String year) {
            this.year = year;
        }

        public void setTotal_rides(String total_rides) {
            this.total_rides = total_rides;
        }

        public void setTotal_income(String total_income) {
            this.total_income = total_income;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }
    }

}

