package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class crossborderratespojo implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    private String response;
    @SerializedName("driver_name")
    private String driver_name;
    @SerializedName("dail_code")
    private String dail_code;
    @SerializedName("mobile_number")
    private String mobile_number;
    @SerializedName("friend_id")
    private String friend_id;
    @SerializedName("driver_image")
    private String driver_image;
    @SerializedName("city")
    private String city;
    @SerializedName("sender_city")
    private String sender_city;
    @SerializedName("user_status")
    private String user_status;
    @SerializedName("transfer_amount")
    private String transfer_amount;
    @SerializedName("transfer_fee")
    private String transfer_fee;
    @SerializedName("transfer_fee_percentage")
    private String transfer_fee_percentage;
    @SerializedName("debit_amount")
    private String debit_amount;
    @SerializedName("send_amount")
    private String send_amount;
    @SerializedName("currency_transfer")
    private String currency_transfer;
    @SerializedName("received_amount")
    private String received_amount;
    @SerializedName("currency_received")
    private String currency_received;
    @SerializedName("conversion_rate")
    private String conversion_rate;


    // Getter Methods 

    public String getStatus() {
        return status;
    }

    public String getResponse() {
        return response;
    }

    public String getDriver_name() {
        return driver_name;
    }

    public String getDail_code() {
        return dail_code;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public String getFriend_id() {
        return friend_id;
    }

    public String getDriver_image() {
        return driver_image;
    }

    public String getCity() {
        return city;
    }

    public String getSender_city() {
        return sender_city;
    }

    public String getUser_status() {
        return user_status;
    }

    public String getTransfer_amount() {
        return transfer_amount;
    }

    public String getTransfer_fee() {
        return transfer_fee;
    }

    public String getTransfer_fee_percentage() {
        return transfer_fee_percentage;
    }

    public String getDebit_amount() {
        return debit_amount;
    }

    public String getSend_amount() {
        return send_amount;
    }

    public String getCurrency_transfer() {
        return currency_transfer;
    }

    public String getReceived_amount() {
        return received_amount;
    }

    public String getCurrency_received() {
        return currency_received;
    }

    public String getConversion_rate() {
        return conversion_rate;
    }

    // Setter Methods 

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public void setDriver_name(String driver_name) {
        this.driver_name = driver_name;
    }

    public void setDail_code(String dail_code) {
        this.dail_code = dail_code;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public void setFriend_id(String friend_id) {
        this.friend_id = friend_id;
    }

    public void setDriver_image(String driver_image) {
        this.driver_image = driver_image;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setSender_city(String sender_city) {
        this.sender_city = sender_city;
    }

    public void setUser_status(String user_status) {
        this.user_status = user_status;
    }

    public void setTransfer_amount(String transfer_amount) {
        this.transfer_amount = transfer_amount;
    }

    public void setTransfer_fee(String transfer_fee) {
        this.transfer_fee = transfer_fee;
    }

    public void setTransfer_fee_percentage(String transfer_fee_percentage) {
        this.transfer_fee_percentage = transfer_fee_percentage;
    }

    public void setDebit_amount(String debit_amount) {
        this.debit_amount = debit_amount;
    }

    public void setSend_amount(String send_amount) {
        this.send_amount = send_amount;
    }

    public void setCurrency_transfer(String currency_transfer) {
        this.currency_transfer = currency_transfer;
    }

    public void setReceived_amount(String received_amount) {
        this.received_amount = received_amount;
    }

    public void setCurrency_received(String currency_received) {
        this.currency_received = currency_received;
    }

    public void setConversion_rate(String conversion_rate) {
        this.conversion_rate = conversion_rate;
    }
}
