package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class rideidpojo implements Serializable {
    @SerializedName("message")
    private String message;
    @SerializedName("action")
    private String action;
    @SerializedName("key1")
    private String key1;
    @SerializedName("key2")
    private String key2;


    // Getter Methods

    public String getMessage() {
        return message;
    }

    public String getAction() {
        return action;
    }

    public String getKey1() {
        return key1;
    }

    public String getKey2() {
        return key2;
    }

    // Setter Methods

    public void setMessage(String message) {
        this.message = message;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public void setKey1(String key1) {
        this.key1 = key1;
    }

    public void setKey2(String key2) {
        this.key2 = key2;
    }
}
