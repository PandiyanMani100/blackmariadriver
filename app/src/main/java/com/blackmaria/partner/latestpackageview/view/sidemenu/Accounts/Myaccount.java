package com.blackmaria.partner.latestpackageview.view.sidemenu.Accounts;


import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.blackmaria.partner.R;
import com.blackmaria.partner.databinding.ActivityMyaccountBinding;
import com.blackmaria.partner.latestpackageview.Factory.Sidemenu.Account.MyaccountFactory;
import com.blackmaria.partner.latestpackageview.Model.profilepojo;
import com.blackmaria.partner.latestpackageview.vieewmodel.sidemenu.account.MyaccountViewModel;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;

public class Myaccount extends AppCompatActivity {

    private ActivityMyaccountBinding binding;
    private MyaccountViewModel myaccountViewModel;
    private profilepojo pojo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(Myaccount.this, R.layout.activity_myaccount);
        myaccountViewModel = ViewModelProviders.of(this, new MyaccountFactory(this)).get(MyaccountViewModel.class);
        binding.setMyaccountViewModel(myaccountViewModel);
        myaccountViewModel.setIds(binding);

        myaccountViewModel.getprofile();
        setResponse();
    }

    private void setResponse() {
        myaccountViewModel.getGetdriverporfile().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    Type type = new TypeToken<profilepojo>() {
                    }.getType();
                    pojo = new GsonBuilder().create().fromJson(jsonObj.toString(), type);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
