package com.blackmaria.partner.latestpackageview.view.Dashboard.Mytrips;

import androidx.appcompat.app.AppCompatActivity;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.Nullable;

import android.os.Bundle;
import android.view.View;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.adapter.Multistoplistadapter;
import com.blackmaria.partner.adapter.multistop_pojo;
import com.blackmaria.partner.databinding.ActivityTripSummaryConstrainBinding;
import com.blackmaria.partner.latestpackageview.Adapter.HomeRateCardFareSummaryAdapter;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Mytrips.TripSummaryFactory;
import com.blackmaria.partner.latestpackageview.Model.tripsummarypojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.Database.RidesDB;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.mytrips.TripSummaryViewModel;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

public class TripSummaryConstrain extends AppCompatActivity {

    private ActivityTripSummaryConstrainBinding binding;
    private TripSummaryViewModel tripSummaryViewModel;
    private SessionManager sessionManager;
    private String driverid = "";
    private tripsummarypojo object;
    private AppUtils appUtils;
    private RidesDB ridesDBobj;
    AccessLanguagefromlocaldb db;



    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
        initView();
        binding = DataBindingUtil.setContentView(TripSummaryConstrain.this, R.layout.activity_trip_summary_constrain);
        tripSummaryViewModel = ViewModelProviders.of(this, new TripSummaryFactory(this)).get(TripSummaryViewModel.class);
        binding.setTripSummaryViewModel(tripSummaryViewModel);
        tripSummaryViewModel.setIds(binding);

        binding.headingg.setText(db.getvalue("tripsummary"));
        binding.pickup.setText(db.getvalue("pickup_location"));
        binding.drop.setText(db.getvalue("drop_location"));
        binding.vehicletype.setText(db.getvalue("vehicle_standard"));
        binding.diatancelabel.setText(db.getvalue("distance"));
        binding.estimatedfarelable.setText(db.getvalue("estimated_fare"));
        binding.etalabel.setText(db.getvalue("eta"));
        binding.kilometerlabel.setText(db.getvalue("km"));
        binding.mintues.setText(db.getvalue("minutes"));

        binding.estimatedfaretripinfo.setText(db.getvalue("estimated_fare_and_trip_information"));
        binding.exluded.setText(db.getvalue("excluded"));
        binding.tollroadfee.setText(db.getvalue("toll_road_fees"));
        binding.parking.setText(db.getvalue("parking_fees"));
        binding.fareinfo.setText(db.getvalue("fare_information"));

        binding.entryfee.setText(db.getvalue("entry_fees_if_any"));
        binding.finalfare.setText(db.getvalue("final_fare_may_different_from_estimated_fare"));
        binding.secondfinalfare.setText(db.getvalue("subject_to_distance_and_time_while_on_trip"));

        String ridegetfromdb = ridesDBobj.getride(getIntent().getStringExtra("rideid"));
        if (!ridegetfromdb.equals("0")) {
            try {
                JSONObject jsonObject = new JSONObject(ridegetfromdb);
                Type type = new TypeToken<tripsummarypojo>() {
                }.getType();
                object = new GsonBuilder().create().fromJson(ridegetfromdb.toString(), type);
                setText(object, jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            tripSummaryViewModel.tripinfoapihit(getIntent().getStringExtra("rideid"), driverid);
        }

        setTextresponse();

    }

    private void initView() {
        sessionManager = SessionManager.getInstance(this);
        appUtils = AppUtils.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        driverid = user.get(SessionManager.KEY_DRIVERID);
        ridesDBobj = new RidesDB(TripSummaryConstrain.this);

    }

    private void setTextresponse() {
        tripSummaryViewModel.getResponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    // save local db
                    String timestamp = "";
                    try {
                        JSONObject obj = new JSONObject(response);
                        timestamp = obj.getJSONObject("response").getJSONObject("details").getString("pickup_timestamp");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    RidesDB ridesDB = new RidesDB(TripSummaryConstrain.this);
                    ridesDB.insertride(driverid, getIntent().getStringExtra("rideid"), timestamp, response,"","","","","");
                    // save local db

                    JSONObject jsonObject = new JSONObject(response);
                    Type type = new TypeToken<tripsummarypojo>() {
                    }.getType();
                    object = new GsonBuilder().create().fromJson(response.toString(), type);
                    setText(object, jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setText(final tripsummarypojo object, JSONObject jsonObject) {
        AppUtils.setImageView(this, object.getResponse().getDetails().getCat_image(), binding.ratecardCarImageview);
        binding.tripdate.setText("TRIP DATE " + object.getResponse().getDetails().getTrip_date()+" "+object.getResponse().getDetails().getDrop_time());
        binding.pickupvalue.setText(object.getResponse().getDetails().getTripArr().get(0).getValue());
        binding.dropvalue.setText(object.getResponse().getDetails().getTripArr().get(1).getValue());
        try {
            if (jsonObject.getJSONObject("response").getJSONObject("details").getJSONObject("summary").length() > 0) {
                binding.kilometer.setText(object.getResponse().getDetails().getSummary().getRide_distance());
                binding.rideamount.setText(object.getResponse().getDetails().getTrip_fare());
                binding.currencylabel.setText(object.getResponse().getDetails().getCurrency());
                binding.etaTime.setText(object.getResponse().getDetails().getSummary().getRide_duration());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (object.getResponse().getDetails().getFareArrL().size() > 0) {
            HomeRateCardFareSummaryAdapter fare_adapter = new HomeRateCardFareSummaryAdapter(TripSummaryConstrain.this, object.getResponse().getDetails().getFareArrL(), object.getResponse().getDetails().getCurrency());
            binding.ratecardFareInfoListView.setAdapter(fare_adapter);
            binding.ratecardFareInfoListView.setExpanded(true);
        }

        if (object.getResponse().getDetails().getMulti_stops().size() > 0) {

            binding.multistoplist.setVisibility(View.VISIBLE);
            Multistoplistadapter paymentListAdapter = new Multistoplistadapter(TripSummaryConstrain.this, object.getResponse().getDetails().getMulti_stops());
            binding.multistoplist.setAdapter(paymentListAdapter);
            paymentListAdapter.notifyDataSetChanged();
            binding.multistoplist.setExpanded(true);

        }
        else
        {
            binding.multistoplist.setVisibility(View.GONE);
        }
    }
}
