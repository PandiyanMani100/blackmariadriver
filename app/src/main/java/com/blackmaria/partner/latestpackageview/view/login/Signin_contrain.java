package com.blackmaria.partner.latestpackageview.view.login;


import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;

import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Handler;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.CountryDialCode;
import com.blackmaria.partner.Utils.GPSTracker;
import com.blackmaria.partner.Utils.SessionManager;

import com.blackmaria.partner.app.MyDialogFragment;
import com.blackmaria.partner.databinding.NewloginpageBinding;
import com.blackmaria.partner.latestpackageview.Factory.Login.SigninFactory;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Dashboard_constrain;
import com.blackmaria.partner.latestpackageview.view.signup.RegistrationTerms_Constrain;
import com.blackmaria.partner.latestpackageview.vieewmodel.login.SigninViewModel;
import com.blackmaria.partner.latestpackageview.view.signup.Splashscreen;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;

import com.blackmaria.partner.latestpackageview.Countrycodepicker.countrycodepicker.CountryPicker;
import com.blackmaria.partner.latestpackageview.Countrycodepicker.countrycodepicker.CountryPickerListener;
import com.google.firebase.FirebaseApp;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;

import static com.blackmaria.partner.latestpackageview.Utils.AppUtils.getResId;

public class Signin_contrain extends AppCompatActivity implements  MyDialogFragment.Communicator {

    private SigninViewModel signinViewModel;
    private NewloginpageBinding binding;
    public CountryPicker picker;
    private String number = "", enumber = "", sPrivacyStatus = "", Sis_alive_other = "";
    private boolean ismobile = true;
    private SessionManager session;
    private AppUtils appUtils;
    AccessLanguagefromlocaldb db;
    private GPSTracker gpsTracker;

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseApp.initializeApp(this);
        db = new AccessLanguagefromlocaldb(this);
        binding = DataBindingUtil.setContentView(Signin_contrain.this, R.layout.newloginpage);
        signinViewModel = ViewModelProviders.of(this, new SigninFactory(this)).get(SigninViewModel.class);
        binding.setSigninViewModel(signinViewModel);

        signinViewModel.setIds(binding);
        binding.txtSignup.setText(db.getvalue("app_name_header"));
        binding.signisnn.setText(db.getvalue("sign_in_button_text"));
        binding.edtmobileno.setHint(db.getvalue("edit_mobilenumber"));
        binding.editpinnumnber.setHint(db.getvalue("edit_pinnumber"));
        binding.txtGo.setText(db.getvalue("login"));
        binding.lyForgetpin.setText(db.getvalue("forgetpin"));
        binding.languagetext.setText(db.getvalue("langauge"));
        binding.dontha.setText(db.getvalue("donothaveacoount"));
        binding.signinn.setText(db.getvalue("signup"));



        initView();
        listener();
    }

    private void initView() {
        session = SessionManager.getInstance(this);
        appUtils = AppUtils.getInstance(this);
        gpsTracker = GPSTracker.getInstance(this);
        picker = CountryPicker.newInstance(getResources().getString(R.string.select_country_lable));

        signinViewModel.getLogincheck().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                String Sstatus = "", Smessage = "";
                String Sdriver_image = "", Sdriver_name = "", Semail = "", Svehicle_number = "", Svehicle_model = "", key = "", gcmid = "";
                String SsecretKey = "", sDriverID = "", gcmId = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    Smessage = object.getString("response");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        sDriverID = object.getString("driver_id");
                        Sdriver_name = object.getString("driver_name");
                        Sdriver_image = object.getString("driver_image");
                        SsecretKey = object.getString("sec_key");
                        gcmId = object.getString("key");
                        Sis_alive_other = object.getString("is_alive_other");
                        Semail = object.getString("email");
                        Svehicle_number = object.getString("vehicle_number");
                        Svehicle_model = object.getString("vehicle_model");

                        session.createLoginSession(Sdriver_image, sDriverID, Sdriver_name, Semail, Svehicle_number, Svehicle_model, gcmId, SsecretKey, gcmId);
                        session.setUserVehicle(Svehicle_model);

                        signinViewModel.Appinfosecond();
                    } else {
                        appUtils.AlertError(Signin_contrain.this, getResources().getString(R.string.alert_label_title), Smessage);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        signinViewModel.getSetuserlocation().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                String Str_status = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");
                    if (Str_status.equalsIgnoreCase("1")) {
                        if (Sis_alive_other.equalsIgnoreCase("Yes")) {
                            final PkDialog mDialog = new PkDialog(Signin_contrain.this);
                            mDialog.setDialogTitle(getResources().getString(R.string.app_name));
                            mDialog.setDialogMessage(getResources().getString(R.string.alert_multiple_login));
                            mDialog.setPositiveButton(getResources().getString(R.string.alert_ok), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mDialog.dismiss();
                                    session.setXmppServiceState("online");
                                    session.setUserloggedIn(true);
                                    Intent intent = new Intent(Signin_contrain.this, Dashboard_constrain.class);
                                    startActivity(intent);
                                    finish();
                                    overridePendingTransition(R.anim.enter, R.anim.exit);
                                }
                            });
                            mDialog.show();
                        } else {
                            session.setXmppServiceState("online");
                            session.setUserloggedIn(true);
                            Intent intent = new Intent(Signin_contrain.this, Dashboard_constrain.class);
                            startActivity(intent);
                            finish();
                            overridePendingTransition(R.anim.enter, R.anim.exit);
                        }
                    } else {
                        appUtils.AlertError(Signin_contrain.this, getResources().getString(R.string.alert_label_title), object.getString("response"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private void listener() {
        binding.txtcountrycode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });
        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {
                picker.dismiss();
                binding.txtcountrycode.setText(dialCode);

                String drawableName = "flag_"
                        + code.toLowerCase(Locale.ENGLISH);
                binding.imgflag.setImageResource(AppUtils.getResId(drawableName));
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            }
        });


binding.currentlanguega.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        MyDialogFragment addPhotoBottomDialogFragment =
                MyDialogFragment.newInstance();
        addPhotoBottomDialogFragment.show(getSupportFragmentManager(),
                "add_photo_dialog_fragment");
    }
});
        binding.img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ismobile) {
                    number = number + "1";
                    binding.edtmobileno.setText(number);
                    binding.edtmobileno.setSelection(binding.edtmobileno.getText().length());
                } else {
                    enumber = enumber + "1";
                    binding.editpinnumnber.setText(enumber);
                    binding.editpinnumnber.setSelection(binding.editpinnumnber.getText().length());
                }

                setbackground(binding.img1, v);
            }
        });

        binding.img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ismobile) {
                    number = number + "2";
                    binding.edtmobileno.setText(number);
                    binding.edtmobileno.setSelection(binding.edtmobileno.getText().length());
                } else {
                    enumber = enumber + "2";
                    binding.editpinnumnber.setText(enumber);
                    binding.editpinnumnber.setSelection(binding.editpinnumnber.getText().length());
                }
                setbackground(binding.img2, v);
            }
        });

        binding.img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ismobile) {
                    number = number + "3";
                    binding.edtmobileno.setText(number);
                    binding.edtmobileno.setSelection(binding.edtmobileno.getText().length());
                } else {
                    enumber = enumber + "3";
                    binding.editpinnumnber.setText(enumber);
                    binding.editpinnumnber.setSelection(binding.editpinnumnber.getText().length());
                }
                setbackground(binding.img3, v);
            }
        });

        binding.img4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ismobile) {
                    number = number + "4";
                    binding.edtmobileno.setText(number);
                    binding.edtmobileno.setSelection(binding.edtmobileno.getText().length());
                } else {
                    enumber = enumber + "4";
                    binding.editpinnumnber.setText(enumber);
                    binding.editpinnumnber.setSelection(binding.editpinnumnber.getText().length());
                }
                setbackground(binding.img4, v);
            }
        });
        binding.img5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ismobile) {
                    number = number + "5";
                    binding.edtmobileno.setText(number);
                    binding.edtmobileno.setSelection(binding.edtmobileno.getText().length());
                } else {
                    enumber = enumber + "5";
                    binding.editpinnumnber.setText(enumber);
                    binding.editpinnumnber.setSelection(binding.editpinnumnber.getText().length());
                }
                setbackground(binding.img5, v);
            }
        });
        binding.img6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ismobile) {
                    number = number + "6";
                    binding.edtmobileno.setText(number);
                    binding.edtmobileno.setSelection(binding.edtmobileno.getText().length());
                } else {
                    enumber = enumber + "6";
                    binding.editpinnumnber.setText(enumber);
                    binding.editpinnumnber.setSelection(binding.editpinnumnber.getText().length());
                }
                setbackground(binding.img6, v);
            }
        });
        binding.img7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ismobile) {
                    number = number + "7";
                    binding.edtmobileno.setText(number);
                    binding.edtmobileno.setSelection(binding.edtmobileno.getText().length());
                } else {
                    enumber = enumber + "7";
                    binding.editpinnumnber.setText(enumber);
                    binding.editpinnumnber.setSelection(binding.editpinnumnber.getText().length());
                }
                setbackground(binding.img7, v);
            }
        });
        binding.img8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ismobile) {
                    number = number + "8";
                    binding.edtmobileno.setText(number);
                    binding.edtmobileno.setSelection(binding.edtmobileno.getText().length());
                } else {
                    enumber = enumber + "8";
                    binding.editpinnumnber.setText(enumber);
                    binding.editpinnumnber.setSelection(binding.editpinnumnber.getText().length());
                }
                setbackground(binding.img8, v);
            }
        });
        binding.img9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ismobile) {
                    number = number + "9";
                    binding.edtmobileno.setText(number);
                    binding.edtmobileno.setSelection(binding.edtmobileno.getText().length());
                } else {
                    enumber = enumber + "9";
                    binding.editpinnumnber.setText(enumber);
                    binding.editpinnumnber.setSelection(binding.editpinnumnber.getText().length());
                }
                setbackground(binding.img9, v);
            }
        });
        binding.img0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ismobile) {
                    number = number + "0";
                    binding.edtmobileno.setText(number);
                    binding.edtmobileno.setSelection(binding.edtmobileno.getText().length());
                } else {
                    enumber = enumber + "0";
                    binding.editpinnumnber.setText(enumber);
                    binding.editpinnumnber.setSelection(binding.editpinnumnber.getText().length());
                }
                setbackground(binding.img0, v);
            }
        });
        binding.imgHash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ismobile) {
                    String SMobileNo = binding.edtmobileno.getText().toString();
                    if (SMobileNo != null && SMobileNo.length() > 0) {
                        binding.edtmobileno.setText(SMobileNo.substring(0, SMobileNo.length() - 1));
                        binding.edtmobileno.setSelection(binding.edtmobileno.getText().length());
                        number = binding.edtmobileno.getText().toString();
                    }
                } else {
                    String SMobileNo = binding.editpinnumnber.getText().toString();
                    if (SMobileNo != null && SMobileNo.length() > 0) {
                        binding.editpinnumnber.setText(SMobileNo.substring(0, SMobileNo.length() - 1));
                        binding.editpinnumnber.setSelection(binding.editpinnumnber.getText().length());
                        enumber = binding.editpinnumnber.getText().toString();
                    }
                }
            }
        });


    }


    private void setbackground(final ImageView image, View view) {

        ImageView iv = (ImageView) view;
        final Drawable d = iv.getBackground();
        image.setBackground(getResources().getDrawable(R.drawable.rect_green));
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                image.setBackground(d.getCurrent());
            }
        }, 100);
    }

    private void Alertregister(String title, String alert) {
        final PkDialog mDialog = new PkDialog(this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                Intent intent = new Intent(Signin_contrain.this, RegistrationTerms_Constrain.class);
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

            }
        });
        mDialog.show();
        getResources().getString(getResources().getIdentifier("footerLink1", "string", getPackageName()));
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        SmartLocation.with(this).location().stop();
    }

    @Override
    public void message(String data) {

        String languageToLoad  = data; // your language
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

        session.setCurrentlanguage(data);

        Intent intent = new Intent(Signin_contrain.this, Splashscreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }
}
