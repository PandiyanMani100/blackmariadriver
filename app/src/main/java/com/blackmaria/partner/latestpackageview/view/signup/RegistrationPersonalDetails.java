package com.blackmaria.partner.latestpackageview.view.signup;



import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.latestpackageview.Adapter.Type_of_registrationadapter;
import com.blackmaria.partner.databinding.RegisterPersonalDetailsConstrainBinding;
import com.blackmaria.partner.latestpackageview.Factory.SignUp.RegistrationPersonaldetailsFactory;
import com.blackmaria.partner.latestpackageview.vieewmodel.signup.RegistrationPersonalDetailsViewModel;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.blackmaria.partner.latestpackageview.Countrycodepicker.countrycodepicker.CountryPicker;
import com.blackmaria.partner.latestpackageview.Countrycodepicker.countrycodepicker.CountryPickerListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;


public class RegistrationPersonalDetails extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private boolean isSpnrTypegenderOfRegFirstTime = true, isSpnrSelectExOfRegFirstTime = true;
    private String experienceyr = "", driverTypeofregistration = "", companyid = "", driverVehicleId = "", driverLocationPlaceId = "", driverProImage = "", driverCountryCode = "", driverPhoneNumber = "", driverReferelCode = "", driverPinCode = "";
    private boolean val = false;
    private String CountryCode = "";
    private CountryPicker picker;

    private RegistrationPersonalDetailsViewModel registrationPersonalDetailsViewModel;
    private RegisterPersonalDetailsConstrainBinding binding;
    AccessLanguagefromlocaldb db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.register_personal_details_constrain);
        registrationPersonalDetailsViewModel = ViewModelProviders.of(this, new RegistrationPersonaldetailsFactory(this)).get(RegistrationPersonalDetailsViewModel.class);
        binding.setRegistrationPersonalDetailsViewModel(registrationPersonalDetailsViewModel);
        db = new AccessLanguagefromlocaldb(this);
        registrationPersonalDetailsViewModel.setvalidation(binding);

        initialize();

        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {
                picker.dismiss();
                binding.txtspnrcountry.setText(name);
                CountryCode = dialCode;
                String drawableName = "flag_"
                        + code.toLowerCase(Locale.ENGLISH);
                binding.flag.setImageResource(getResId(drawableName));
                CloseKeyBoard();
            }
        });
        binding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        binding.personalinfo.setText(db.getvalue("personalinfo"));
        binding.nameTv.setText(db.getvalue("name"));
        binding.emailTv.setText(db.getvalue("email"));
        binding.txtspnrgender.setText(db.getvalue("gender"));
        binding.exptxt.setText(db.getvalue("experience"));
        binding.addressinfo.setText(db.getvalue("addressinfo"));
        binding.editStreetname.setHint(db.getvalue("street_name"));
        binding.editCity.setHint(db.getvalue("city_name"));
        binding.editState.setHint(db.getvalue("state_province"));
        binding.editPostalcode.setHint(db.getvalue("post_code"));
        binding.txtspnrcountry.setText(db.getvalue("select_country"));
        binding.btnConfirm.setText(db.getvalue("confirm_label"));

    }

    private void initialize() {

        picker = CountryPicker.newInstance(db.getvalue("select_country_lable"));

        binding.txtspnrcountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });

        binding.exptxt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                CloseKeyboardNew();
                return false;
            }
        });
        binding.txtspnrgender.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                CloseKeyboardNew();
                return false;
            }
        });

        Intent in = getIntent();
        if (in != null) {
            driverVehicleId = in.getStringExtra("driverVehicleId");
            driverLocationPlaceId = in.getStringExtra("driverLocationPlaceId");
            driverProImage = in.getStringExtra("driverProImage");
            driverCountryCode = in.getStringExtra("driverCountryCode");
            driverPhoneNumber = in.getStringExtra("driverPhoneNumber");
            driverReferelCode = in.getStringExtra("driverReferelCode");
            driverPinCode = in.getStringExtra("driverPinCode");
            driverTypeofregistration = in.getStringExtra("driverTypeofregistration");
            companyid= in.getStringExtra("companyid");
        }

        DrivingExperienceSpinner();

        GenderSelection();

        registrationPersonalDetailsViewModel.getEmailchecking().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                if (!response.equalsIgnoreCase("")) {
                    try {
                        JSONObject object = new JSONObject(response);
                        String status = object.getString("status");
                        if (status.equalsIgnoreCase("1")) {
                            val = true;
                            Intent intent = new Intent(RegistrationPersonalDetails.this, RegistervehicledetailsConstrain.class);
                            intent.putExtra("driverVehicleId", driverVehicleId);
                            intent.putExtra("driverLocationPlaceId", driverLocationPlaceId);
                            intent.putExtra("driverProImage", driverProImage);
                            intent.putExtra("driverCountryCode", driverCountryCode);
                            intent.putExtra("driverPhoneNumber", driverPhoneNumber);
                            intent.putExtra("driverReferelCode", driverReferelCode);
                            intent.putExtra("driverPinCode", driverPinCode);
                            intent.putExtra("driverCompanyType", driverTypeofregistration);
                            intent.putExtra("driverCompanyId", companyid);
                            intent.putExtra("driverName", binding.editName.getText().toString().trim());
                            intent.putExtra("driverEmailId", binding.editEmail.getText().toString().trim());
                            intent.putExtra("driverGender", binding.txtspnrgender.getText().toString().trim());
                            intent.putExtra("driverExperience", experienceyr);
                            intent.putExtra("driverAddress", binding.editStreetname.getText().toString().trim());
                            intent.putExtra("driverCityname", binding.editCity.getText().toString().trim());
                            intent.putExtra("driverPostCode", binding.editPostalcode.getText().toString().trim());
                            intent.putExtra("driverState", binding.editState.getText().toString().trim());
                            intent.putExtra("driverCountry", binding.txtspnrcountry.getText().toString().trim());
                            startActivity(intent);
                            overridePendingTransition(R.anim.enter, R.anim.exit);

                        } else {
                            val = false;
                            Alert(db.getvalue("action_error"), object.getString("response"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Alert(db.getvalue("action_error"), "Server Error");
                }
            }
        });

    }


    @Override
    public void onClick(View view) {

        if (view == binding.imgBack) {
            onBackPressed();
        }
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(RegistrationPersonalDetails.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(db.getvalue("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    private void DrivingExperienceSpinner() {
        isSpnrSelectExOfRegFirstTime = true;
        String spinnerData[] = getResources().getStringArray(R.array.drivingexperience);
        ArrayList<String> regTypeList = new ArrayList<String>();
        regTypeList.addAll(Arrays.asList(spinnerData));
        Type_of_registrationadapter customSpinnerbank = new Type_of_registrationadapter(RegistrationPersonalDetails.this, regTypeList);
        binding.selectexp.setAdapter(customSpinnerbank);
        binding.selectexp.setOnItemSelectedListener(this);
    }


    private void GenderSelection() {
        String spinnerData[] = getResources().getStringArray(R.array.genders);
        ArrayList<String> regTypeList = new ArrayList<String>();
        regTypeList.addAll(Arrays.asList(spinnerData));
        isSpnrTypegenderOfRegFirstTime = true;
        Type_of_registrationadapter customSpinnerbank = new Type_of_registrationadapter(RegistrationPersonalDetails.this, regTypeList);
        binding.selectgender.setAdapter(customSpinnerbank);
        binding.selectgender.setOnItemSelectedListener(this);
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        System.out.println("++++++++++++position=+++++++++++++" + position);
        if (parent == binding.selectexp) {
            String sSelectedItem = binding.selectexp.getSelectedItem().toString().trim();
            String[] expYr = sSelectedItem.split(" ");
            experienceyr = expYr[0];
            System.out.println("===========expYr===========" + experienceyr);
            if (!isSpnrSelectExOfRegFirstTime) {
                binding.exptxt.setText(sSelectedItem);
            }
            isSpnrSelectExOfRegFirstTime = false;
        } else if (parent == binding.selectgender) {
            String sSelectedItem = binding.selectgender.getSelectedItem().toString();
            if (!isSpnrTypegenderOfRegFirstTime) {
                binding.txtspnrgender.setText(sSelectedItem);
            }
            isSpnrTypegenderOfRegFirstTime = false;
        }
    }


    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    private int getResId(String drawableName) {

        try {
            Class<com.blackmaria.partner.R.drawable> res = R.drawable.class;
            Field field = res.getField(drawableName);
            int drawableId = field.getInt(null);
            return drawableId;
        } catch (Exception e) {
            Log.e("CountryCodePicker", "Failure to get drawable id.", e);
        }
        return -1;
    }

    private void CloseKeyboardNew() {
        try {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow((null == getCurrentFocus()) ? null : getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

    private void CloseKeyBoard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
