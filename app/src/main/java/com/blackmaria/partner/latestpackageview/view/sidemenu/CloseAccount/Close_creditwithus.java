package com.blackmaria.partner.latestpackageview.view.sidemenu.CloseAccount;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Model.Payment_closeaccount_pojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Pin_activity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;


import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

public class Close_creditwithus extends AppCompatActivity implements ApIServices.completelisner {
    private ImageView img_back;
    private TextView amount, RL_close_account;
    private LinearLayout payments;
    private String UserID = "";
    private SessionManager sessionManager;
    private Dialog dialog;
    private AppUtils appUtils;
    private ProgressBar apiload;
    AccessLanguagefromlocaldb db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_close_creditwithus);
        db = new AccessLanguagefromlocaldb(this);
        sessionManager = SessionManager.getInstance(Close_creditwithus.this);
        appUtils = AppUtils.getInstance(Close_creditwithus.this);
        apiload = findViewById(R.id.apiload);

        HashMap<String, String> user = sessionManager.getUserDetails();
        UserID = user.get(SessionManager.KEY_DRIVERID);

        img_back = findViewById(R.id.img_back);

        TextView closesummary = findViewById(R.id.closesummary);
        closesummary.setText(db.getvalue("creditwithus"));
        TextView severaldays = findViewById(R.id.severaldays);
        severaldays.setText(db.getvalue("allowseveraldays"));
        TextView getpaid = findViewById(R.id.getpaid);
        getpaid.setText(db.getvalue("howdoyoupay"));

        amount = findViewById(R.id.amount);
        if (getIntent().hasExtra("amount")) {
            amount.setText(getIntent().getStringExtra("amount"));
            sessionManager.setaccountcloseamount(getIntent().getStringExtra("amount"));
        }
        RL_close_account = findViewById(R.id.RL_close_account);
        RL_close_account.setText(db.getvalue("cancelss"));
        payments = findViewById(R.id.payments);

        RL_close_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", UserID);
        paymentlist(Iconstant.closeaccountpaymentslist, jsonParams);
    }


    private void paymentlist(String Url, final HashMap<String, String> jsonParams) {
        apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(this, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ApIServices apIServices = new ApIServices(this, Close_creditwithus.this);
        apIServices.dooperation(Url, jsonParams);
    }

    private void paymentlistconstrian(final Payment_closeaccount_pojo refer_friendslists) {
        try {
            payments.removeAllViews();
        } catch (NullPointerException e) {
        }
        for (int i = 0; i <= refer_friendslists.getResponse().getPayment().size() - 1; i++) {
            View view = getLayoutInflater().inflate(R.layout.closeaccountadapter_paymentslist, null);

            ImageView imageView = view.findViewById(R.id.payment);
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.new_no_user_img)
                    .error(R.drawable.new_no_user_img)
                    .diskCacheStrategy(DiskCacheStrategy.ALL);
            if (!refer_friendslists.getResponse().getPayment().get(i).getIcon().equalsIgnoreCase("")) {
                Glide.with(this).load(refer_friendslists.getResponse().getPayment().get(i).getIcon()).apply(options).into(imageView);
            }

            final int finalI = i;
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent ii = new Intent(Close_creditwithus.this, Pin_activity.class);
                    ii.putExtra("mode", refer_friendslists.getResponse().getPayment().get(finalI).getCode());
                    startActivity(ii);
                }
            });

            payments.addView(view);
        }
    }

    @Override
    public void sucessresponse(String response) {
        dialog.dismiss();
        apiload.setVisibility(View.INVISIBLE);
        try {

            JSONObject object = new JSONObject(response);
            String Sstatus = object.getString("status");
            if (Sstatus.equalsIgnoreCase("1")) {
                Type listType = new TypeToken<Payment_closeaccount_pojo>() {
                }.getType();
                Payment_closeaccount_pojo obj = new GsonBuilder().create().fromJson(object.toString(), listType);
                paymentlistconstrian(obj);
            } else {
                appUtils.AlertError(this, getResources().getString(R.string.action_error), object.getString("response"));
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            System.out.println("eee------------------" + e);
            e.printStackTrace();

        }
    }

    @Override
    public void errorreponse() {
        dialog.dismiss();
        apiload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void jsonexception() {
        dialog.dismiss();
        apiload.setVisibility(View.INVISIBLE);
        AppUtils.toastprint(this,"Json Exception");
    }
}


