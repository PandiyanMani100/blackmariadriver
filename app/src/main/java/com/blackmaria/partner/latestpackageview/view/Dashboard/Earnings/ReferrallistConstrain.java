package com.blackmaria.partner.latestpackageview.view.Dashboard.Earnings;


import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.latestpackageview.widgets.RoundedImageView;
import com.blackmaria.partner.databinding.ActivityReferrallistConstrainBinding;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Earnings.ReferaallistFactory;
import com.blackmaria.partner.latestpackageview.Model.referrallist;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.earnings.ReferrallistViewModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;

public class ReferrallistConstrain extends AppCompatActivity {

    private ActivityReferrallistConstrainBinding binding;
    private ReferrallistViewModel referrallistViewModel;
    AccessLanguagefromlocaldb db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
        binding = DataBindingUtil.setContentView(ReferrallistConstrain.this, R.layout.activity_referrallist_constrain);
        referrallistViewModel = ViewModelProviders.of(this, new ReferaallistFactory(this)).get(ReferrallistViewModel.class);
//        binding.setReferrallistViewModel(referrallistViewModel);

        referrallistViewModel.setIds(binding);

        binding.datemonthyear.setText(db.getvalue("totalreferal"));
        binding.datetxt.setText(db.getvalue("my_referral_list"));
        binding.rideListNoRidesTextview1.setText(db.getvalue("no_rides_lable"));


        referrallistViewModel.getReferallistresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Type type = new TypeToken<referrallist>() {
                    }.getType();
                    referrallist referrallist = new GsonBuilder().create().fromJson(jsonObject.toString(), type);
                    setreferrallist(referrallist);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        binding.backimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                referrallistViewModel.back();
            }
        });
        binding.rideListNextTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                referrallistViewModel.right();
            }
        });
        binding.rideListPreviousTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                referrallistViewModel.left();
            }
        });

    }

    public void setreferrallist(referrallist strings) {
        if(strings.getResponse().getRef_info().size()>0){
            binding.referallistlistview.setVisibility(View.VISIBLE);
            binding.rideListNoRidesTextview1.setVisibility(View.GONE);
        }else{
            binding.referallistlistview.setVisibility(View.GONE);
            binding.rideListNoRidesTextview1.setVisibility(View.VISIBLE);
        }
        try {
            binding.referallistlistview.removeAllViews();
        } catch (NullPointerException e) {
        }
        binding.tripcount.setText(strings.getResponse().getRef_count());
        binding.datetxt.setText("my referral list" + "\n" + "CODE : " + strings.getResponse().getReferal_code());
        for (int i = 0; i <= strings.getResponse().getRef_info().size() - 1; i++) {
            View view = getLayoutInflater().inflate(R.layout.referrallistadapter, null);
            RoundedImageView userimageview = view.findViewById(R.id.userimageview);
            TextView username = view.findViewById(R.id.username);
            TextView signupdate = view.findViewById(R.id.signupdate);
            TextView location = view.findViewById(R.id.location);
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.profileicon_dashboard)
                    .error(R.drawable.profileicon_dashboard)
                    .diskCacheStrategy(DiskCacheStrategy.ALL);
            Glide.with(this).load(strings.getResponse().getRef_info().get(i).getDriver_image()).apply(options).into(userimageview);
            username.setText(strings.getResponse().getRef_info().get(i).getDriver_name());
            signupdate.setText("SIGNUP DATE : "+strings.getResponse().getRef_info().get(i).getCreated());
            location.setText("LOCATION : "+strings.getResponse().getRef_info().get(i).getLocation());
            binding.referallistlistview.addView(view);
        }
    }
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }

}
