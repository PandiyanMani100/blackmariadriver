package com.blackmaria.partner.latestpackageview.Factory.Dashboard.Tracking;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.tracking.TrackpageViewModel;


public class TrackpageFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public TrackpageFactory(Activity context) {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new TrackpageViewModel(context);
    }
}
