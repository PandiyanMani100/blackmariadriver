package com.blackmaria.partner.latestpackageview.view.Dashboard.Earnings;


import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;

import androidx.databinding.DataBindingUtil;

import android.graphics.Color;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityRideearnConstrainBinding;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Earnings.RideearnFactory;
import com.blackmaria.partner.latestpackageview.Model.rideearnpojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Pin_activity;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.earnings.RideearnViewModel;
import com.blackmaria.partner.latestpackageview.view.sidemenu.Invitefriends.Invitefriends_constrain;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Column;
import lecho.lib.hellocharts.model.ColumnChartData;
import lecho.lib.hellocharts.model.SubcolumnValue;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.util.ChartUtils;

public class RideearnConstrain extends AppCompatActivity {

    private ActivityRideearnConstrainBinding binding;
    private RideearnViewModel rideearnViewModel;
    private AppUtils appUtils;
    private ColumnChartData data;
    private rideearnpojo rideearnpojo;
    private SessionManager sessionManager;
    private String sDriverID = "", sSecurePin = "";
    AccessLanguagefromlocaldb db;

    @Override
    protected void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
//        EventBus.getDefault().register(this);
        binding = DataBindingUtil.setContentView(RideearnConstrain.this, R.layout.activity_rideearn_constrain);
        rideearnViewModel = ViewModelProviders.of(this, new RideearnFactory(this)).get(RideearnViewModel.class);
//        binding.setRideearnViewModel(rideearnViewModel);

        rideearnViewModel.setIds(binding);

        binding.title.setText(db.getvalue("rideearn"));
        binding.titleone.setText(db.getvalue("refreallnetworktext"));
        binding.today.setText(db.getvalue("today"));
        binding.week.setText(db.getvalue("thisweek"));
        binding.month.setText(db.getvalue("thismonth"));
        binding.youaregetpaid.setText(db.getvalue("getpaidyourare"));
        binding.withdraw.setText(db.getvalue("withdraw_lable"));
        binding.toprefrealthisweek.setText(db.getvalue("topreferalthiseek"));
        binding.refrealweeklyperformance.setText(db.getvalue("referralweeklyperformance"));
        binding.tripstxt.setText(db.getvalue("trips"));
        binding.invitenetwork.setText(db.getvalue("invitenetwork"));
        binding.nocharts.setText(db.getvalue("no_chart_data"));
        binding.commissiontext.setText(db.getvalue("commisiontext"));
        binding.downloadstatement.setText(db.getvalue("downloadstatement"));

        rideearnresponse();
        clicklistener();
    }

    private void clicklistener() {
        binding.withdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                withdrawAmount();
            }
        });
        binding.downloadstatement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rideearnViewModel.downloadstatement(sessionManager.getDriverImageUpdate(), rideearnpojo.getResponse().getReferal_code(), rideearnpojo.getResponse().getDownload_url());
            }
        });
        binding.backimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rideearnViewModel.back();
            }
        });

        binding.constriantoday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                rideearnViewModel.today();
            }
        });
        binding.invitenetwork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Invitefriends_constrain.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
        binding.constrianweek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                rideearnViewModel.week();
            }
        });
        binding.constrianmonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                rideearnViewModel.month();
            }
        });
    }

    private void rideearnresponse() {
        appUtils = AppUtils.getInstance(this);
        sessionManager = SessionManager.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        sSecurePin = sessionManager.getSecurePin();
        rideearnViewModel.getRideearnresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Type type = new TypeToken<rideearnpojo>() {
                    }.getType();
                    rideearnpojo = new GsonBuilder().create().fromJson(jsonObject.toString(), type);
                    System.out.println("" + rideearnpojo.getStatus());
                    setResponse(rideearnpojo);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setResponse(rideearnpojo rideearnpojo) {
        binding.datemonthyear.setText(rideearnpojo.getResponse().getMonth());
        binding.todaytripscount.setText(rideearnpojo.getResponse().getToday_count() + " Trips");
        binding.weektripscount.setText(rideearnpojo.getResponse().getWeek_count() + " Trips");
        binding.monthtripscount.setText(rideearnpojo.getResponse().getMonth_count() + " Trips");
        binding.youaregetpaidamount.setText(rideearnpojo.getResponse().getCurrency() + " " + rideearnpojo.getResponse().getAvailable_amount());
        binding.todayamount.setText(rideearnpojo.getResponse().getCurrency() + " " + rideearnpojo.getResponse().getToday_amount());
        binding.weekamount.setText(rideearnpojo.getResponse().getCurrency() + " " + rideearnpojo.getResponse().getWeek_amount());
        binding.monthamount.setText(rideearnpojo.getResponse().getCurrency() + " " + rideearnpojo.getResponse().getMonth_amount());
        if (!rideearnpojo.getResponse().getLast_withdrawal_date().equalsIgnoreCase("")) {
            binding.lastgetpaiddate.setText("last getpaid on " + rideearnpojo.getResponse().getLast_withdrawal_date());
        } else {
            binding.lastgetpaiddate.setText("No Withdrawal Yet");
        }
        if (rideearnpojo.getResponse().getChart_array().size() > 0) {
            binding.nocharts.setVisibility(View.GONE);
            binding.tripstxt.setVisibility(View.VISIBLE);
            binding.myTripweeklyPerformancechart.setVisibility(View.VISIBLE);
            setBarchart(rideearnpojo);
        } else {
            binding.tripstxt.setVisibility(View.GONE);
            binding.myTripweeklyPerformancechart.setVisibility(View.GONE);
            binding.nocharts.setVisibility(View.VISIBLE);
        }
        AppUtils.setImageView(this, rideearnpojo.getResponse().getTop_income_this_weekobj().getImage(), binding.toprerralimage);
        binding.topreferalname.setText(rideearnpojo.getResponse().getTop_income_this_weekobj().getDriver_name());
        binding.topreferalplace.setText(rideearnpojo.getResponse().getTop_income_this_weekobj().getLocation());
    }

    private void setBarchart(rideearnpojo object) {
        List<Column> columns = new ArrayList<Column>();
        List<SubcolumnValue> values;
        int numSubcolumns = 1;
        for (int i = 0; i < object.getResponse().getChart_array().size(); i++) {
            values = new ArrayList<SubcolumnValue>();
            for (int j = 0; j < numSubcolumns; j++) {
                values.add(new SubcolumnValue(Float.parseFloat(String.valueOf(object.getResponse().getChart_array().get(i).getRidecount())), ChartUtils.pickColor1()));
            }
            Column column = new Column(values);
            column.setHasLabels(false);
            columns.add(column);
        }

        data = new ColumnChartData(columns);

        Axis axisX;
        Axis axisy = new Axis();
        axisy.setTextColor(Color.BLACK);
        axisy.setTextSize(8);
        List<AxisValue> axisValues = new ArrayList<AxisValue>();
        for (int j = 0; j < object.getResponse().getChart_array().size(); ++j) {
            axisValues.add(new AxisValue(j).setLabel(object.getResponse().getChart_array().get(j).getDay()));
        }
        axisX = new Axis(axisValues);
        axisX.setTextColor(Color.BLACK);
        axisX.setTextSize(8);
        data.setAxisXBottom(axisX);
        data.setAxisYLeft(axisy);
        binding.myTripweeklyPerformancechart.setColumnChartData(data);

        final Viewport v = new Viewport(binding.myTripweeklyPerformancechart.getMaximumViewport());
        final Viewport maxViewport = new Viewport(v.left, v.top, v.right, v.bottom);
        binding.myTripweeklyPerformancechart.setMaximumViewport(maxViewport);
        final Viewport currentViewport = new Viewport(v.left, v.top, v.right, v.bottom);//smaller one
        binding.myTripweeklyPerformancechart.setCurrentViewport(currentViewport);
        binding.myTripweeklyPerformancechart.setViewportCalculationEnabled(false);
        //chart.setBackground(getContext().getDrawable(R.drawable.colorpick));
        binding.myTripweeklyPerformancechart.setScrollEnabled(true);
    }


    private void withdrawAmount() {
        String amount = rideearnpojo.getResponse().getAvailable_amount();
        if (amount.length() > 0) {
            double totalAmount = Double.parseDouble(amount);
            if (totalAmount > 0) {
                if (rideearnpojo.getResponse().getProceed_status().equalsIgnoreCase("1")) {
                    if (rideearnpojo.getResponse().getWithdraw_status().equalsIgnoreCase("1")) {
                        startActivity(new Intent(RideearnConstrain.this, Pin_activity.class));
                    } else {
                        appUtils.AlertError(this, db.getvalue("action_error"), rideearnpojo.getResponse().getWithdraw_error());
                    }
                } else {
                    appUtils.AlertError(this, db.getvalue("action_error"), rideearnpojo.getResponse().getError_message());
                }
            } else {
                String message = db.getvalue("earnings_page_label_no_amount_to_withdraw");
                appUtils.AlertError(this, db.getvalue("action_error"), message);
            }
        }
    }


    @Subscribe
    public void withdrawnapihi(String success) {
        rideearnViewModel.withdrawnapihit(rideearnpojo.getResponse().getAvailable_amount());
        rideearnViewModel.getWithdrawalresponse().observe(RideearnConstrain.this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    String status = "", sResponse = "";
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {
                        status = object.getString("status");
                        sResponse = object.getString("response");
                    }
                    if (status.equalsIgnoreCase("1")) {
                        WithdrawSuccessAlert(sResponse, rideearnpojo.getResponse().getCurrency(), rideearnpojo.getResponse().getAvailable_amount(), "wallet");
                    } else {
                        appUtils.AlertError(RideearnConstrain.this,db.getvalue("action_error"), sResponse);
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });
    }

    private void WithdrawSuccessAlert(String title, String currency, String withdrawAmount, String paymentType) {
        final PkDialog mDialog = new PkDialog(RideearnConstrain.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage("\n" + currency + " " + withdrawAmount + "\n\n" + "MODE : " + paymentType);
        mDialog.setPositiveButton(db.getvalue("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                rideearnViewModel.rideearnapihit();
            }
        });
        mDialog.show();

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }
}
