package com.blackmaria.partner.latestpackageview.Factory.Ratings;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackmaria.partner.latestpackageview.vieewmodel.ratings.RatingsViewModel;


public class RatingsFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public RatingsFactory(Activity context)
    {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass)
    {
        return (T) new RatingsViewModel(context);
    }
}
