package com.blackmaria.partner.latestpackageview.view.Dashboard.Incentives;

import android.app.Dialog;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityIncentivedashbaordConstrainBinding;
import com.blackmaria.partner.latestpackageview.Adapter.Incentiveadapter;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Incentives.IncentivesdashboardFactory;
import com.blackmaria.partner.latestpackageview.Model.incentivesdasboardpojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Dashboard_constrain;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Pin_activity;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.incentives.IncentivesdashboardViewModel;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

public class IncentivedashbaordConstrain extends AppCompatActivity {

    private ActivityIncentivedashbaordConstrainBinding binding;
    private IncentivesdashboardViewModel incentivesdashboardViewModel;

    private SessionManager sessionManager;
    private String sDriverID = "", sDriverimage = "";
    private AppUtils appUtils;
    AccessLanguagefromlocaldb db;
    private incentivesdasboardpojo pojo;
    private boolean incententivewithdrawndone = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
        EventBus.getDefault().register(this);
        binding = DataBindingUtil.setContentView(IncentivedashbaordConstrain.this, R.layout.activity_incentivedashbaord_constrain);
        incentivesdashboardViewModel = ViewModelProviders.of(this, new IncentivesdashboardFactory(this)).get(IncentivesdashboardViewModel.class);
        binding.setIncentivesdashboardViewModel(incentivesdashboardViewModel);
        incentivesdashboardViewModel.setIds(binding);

        initView();

        binding.estimatedmonth.setText(db.getvalue("estimatedthismonth"));
        binding.withdraw.setText(db.getvalue("withdraw_lable"));
        binding.incelist.setText(db.getvalue("incentivelist"));
        binding.viewstatement.setText(db.getvalue("viewstatement"));

    }

    private void initView() {
        sessionManager = SessionManager.getInstance(this);
        appUtils = AppUtils.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        sDriverimage = user.get(SessionManager.KEY_DRIVER_IMAGE);
        AppUtils.setImageView(IncentivedashbaordConstrain.this, sDriverimage, binding.userimage);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        incentivesdashboardViewModel.incentivedasboardapihit(jsonParams);
        setResponse();
        clicklistener();
    }

    private void clicklistener() {
        binding.withdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String amount = pojo.getResponse().getTotal_amount();
                if (amount.length() > 0) {
                    double totalAmount = Double.parseDouble(amount);
                    if (totalAmount > 0) {
                        if (pojo.getResponse().getProceed_status().equalsIgnoreCase("0")) {
                            appUtils.AlertError(IncentivedashbaordConstrain.this, db.getvalue("action_error"), pojo.getResponse().getError_message());
                      } else {
                            startActivity(new Intent(IncentivedashbaordConstrain.this, Pin_activity.class));
                        }
                    } else {
                        String message = db.getvalue("earnings_page_label_no_amount_to_withdraw");
                        appUtils.AlertError(IncentivedashbaordConstrain.this, db.getvalue("action_error"), message);

                    }
                } else {
                    String message = db.getvalue("earnings_page_label_no_amount_to_withdraw");
                    appUtils.AlertError(IncentivedashbaordConstrain.this, db.getvalue("action_error"), message);

                }
            }
        });
    }

    @Subscribe
    public void onIncentiveswithdraw(String success) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        jsonParams.put("amount", pojo.getResponse().getTotal_amount());
        jsonParams.put("mode", "wallet");
        incentivesdashboardViewModel.incentivewithdrawapihit(jsonParams);
        incentivesdashboardViewModel.getIncentivewithdrawresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                        incententivewithdrawndone = true;
                        WithdrawSuccessAlert(jsonObject.getString("response"), pojo.getResponse().getCurrency(), pojo.getResponse().getTotal_amount(), "wallet");
                    } else {
                        appUtils.AlertError(IncentivedashbaordConstrain.this, db.getvalue("action_error"), jsonObject.getString("response"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void WithdrawSuccessAlert(String title, String currency, String withdrawAmount, String paymentType) {
        final PkDialog mDialog = new PkDialog(IncentivedashbaordConstrain.this);
        mDialog.setDialogTitledublicate(title);
        mDialog.setDialogMessage("\n" + currency + " " + withdrawAmount + "\n\n" + db.getvalue("moide") + paymentType);
        mDialog.setPositiveButton(db.getvalue("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                HashMap<String, String> jsonParams = new HashMap<String, String>();
                jsonParams.put("driver_id", sDriverID);
                incentivesdashboardViewModel.incentivedasboardapihit(jsonParams);
                setResponse();
            }
        });
        mDialog.show();
    }

    private void setResponse() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat month_date = new SimpleDateFormat("MMM");
        String month_name = month_date.format(cal.getTime());
        binding.datemonthyear.setText(month_name + " " + Calendar.getInstance().get(Calendar.YEAR));
        incentivesdashboardViewModel.getIncentiveresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Type type = new TypeToken<incentivesdasboardpojo>() {
                    }.getType();
                    pojo = new GsonBuilder().create().fromJson(jsonObject.toString(), type);
                    setText(pojo);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setText(incentivesdasboardpojo pojo) {
        if(pojo.getStatus() == null)
            return;
        binding.estimatedmonthvalue.setText(pojo.getResponse().getCurrency() + " " + pojo.getResponse().getTotal_amount());
        Incentiveadapter customAdapter = new Incentiveadapter(getApplicationContext(), pojo.getResponse().getIncetives_count());
        binding.gridview.setAdapter(customAdapter);
        binding.gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // set an Intent to Another Activity
                showtriptarget(position);
            }
        });
    }

    public void showtriptarget(int count) {
        final Dialog dialog = new Dialog(IncentivedashbaordConstrain.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.incentiveamountdialog);
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.95);//fill only 85% of the screen
        int screenHeight = (int) (metrics.heightPixels * 0.95);//fill only 85% of the screen
        dialog.getWindow().setLayout(screenWidth, screenHeight);

        TextView incetivesamountvalue = dialog.findViewById(R.id.incetivesamountvalue);
        TextView incetivesamountdate = dialog.findViewById(R.id.incetivesamountdate);
        TextView triptypetargetcount = dialog.findViewById(R.id.triptypetargetcount);
        TextView triptypetarget = dialog.findViewById(R.id.triptypetarget);
        TextView targettrips = dialog.findViewById(R.id.targettrips);
        TextView currenttrips = dialog.findViewById(R.id.currenttrips);
        ImageView close = dialog.findViewById(R.id.close);
        ImageView bgimage = dialog.findViewById(R.id.bgimage);

        incetivesamountvalue.setText(pojo.getResponse().getCurrency() + " " + pojo.getResponse().getIncetives_count().get(count).getIncetive_amount());
        AppUtils.carsetImageView(IncentivedashbaordConstrain.this, R.drawable.dailytripbg, bgimage);
        triptypetarget.setText(pojo.getResponse().getIncetives_count().get(count).getTitle());
        triptypetargetcount.setText(pojo.getResponse().getIncetives_count().get(count).getIncentive_target());
        currenttrips.setText(pojo.getResponse().getIncetives_count().get(count).getCompleted_title());
        targettrips.setText(pojo.getResponse().getIncetives_count().get(count).getCompleted_hours());

        if (pojo.getResponse().getIncetives_count().get(count).getTarget_date() != null) {
            if (!pojo.getResponse().getIncetives_count().get(count).getTarget_date().equalsIgnoreCase("")) {
                incetivesamountdate.setText("Date " + pojo.getResponse().getIncetives_count().get(count).getTarget_date());
            }
        }

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(incententivewithdrawndone){
            startActivity(new Intent(this, Dashboard_constrain.class));
            EventBus.getDefault().unregister(this);
        }
    }
}
