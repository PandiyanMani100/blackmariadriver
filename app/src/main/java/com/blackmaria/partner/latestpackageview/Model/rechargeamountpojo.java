package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class rechargeamountpojo implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;
    @SerializedName("xendit_charge_status")
    private String xendit_charge_status;

    public String getXendit_card_have() {
        return xendit_card_have;
    }

    public void setXendit_card_have(String xendit_card_have) {
        this.xendit_card_have = xendit_card_have;
    }

    @SerializedName("xendit_card_have")
    private String xendit_card_have;
    @SerializedName("auto_charge_status")
    private String auto_charge_status;

    @SerializedName("xendit_card_details")
    Xendit_card_details Xendit_card_detailsObject;

    public Xendit_card_details getXendit_card_details() {
        return Xendit_card_detailsObject;
    }

    public void setXendit_card_details(Xendit_card_details xendit_card_detailsObject) {
        this.Xendit_card_detailsObject = xendit_card_detailsObject;
    }

    public class Xendit_card_details {
        @SerializedName("id")
        private String id;
        @SerializedName("number")
        private String number;
        @SerializedName("brand")
        private String brand;
        @SerializedName("type")
        private String type;


        // Getter Methods

        public String getId() {
            return id;
        }

        public String getNumber() {
            return number;
        }

        public String getBrand() {
            return brand;
        }

        public String getType() {
            return type;
        }

        // Setter Methods

        public void setId(String id) {
            this.id = id;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    public String getXendit_charge_status() {
        return xendit_charge_status;
    }

    public String getAuto_charge_status() {
        return auto_charge_status;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }

    public void setXendit_charge_status(String xendit_charge_status) {
        this.xendit_charge_status = xendit_charge_status;
    }

    public void setAuto_charge_status(String auto_charge_status) {
        this.auto_charge_status = auto_charge_status;
    }

    public class Response {
        @SerializedName("currency")
        private String currency;
        @SerializedName("current_balance")
        private String current_balance;
        @SerializedName("recharge_boundary")
        Recharge_boundary Recharge_boundaryObject;

        public ArrayList<Payment_list> getPayment_list() {
            return payment_list;
        }

        public void setPayment_list(ArrayList<Payment_list> payment_list) {
            this.payment_list = payment_list;
        }

        @SerializedName("payment_list")
        ArrayList<Payment_list> payment_list = new ArrayList<Payment_list>();
        @SerializedName("xendit_secret_key")
        private String xendit_secret_key;
        @SerializedName("xendit_public_key")
        private String xendit_public_key;
        @SerializedName("exchange_value")
        private String exchange_value;
        @SerializedName("current_month_credit")
        private String current_month_credit;
        @SerializedName("current_month_debit")
        private String current_month_debit;
        @SerializedName("month")
        private String month;
        @SerializedName("year")
        private String year;
        @SerializedName("month_year")
        private String month_year;
        @SerializedName("min_wallet_amount")
        private String min_wallet_amount;

        public class Payment_list {
            @SerializedName("name")
            private String name;
            @SerializedName("code")
            private String code;
            @SerializedName("icon")
            private String icon;
            @SerializedName("inactive_icon")
            private String inactive_icon;
            @SerializedName("id")
            private String id;


            // Getter Methods 

            public String getName() {
                return name;
            }

            public String getCode() {
                return code;
            }

            public String getId(){
                return id;
            }

            public String getIcon() {
                return icon;
            }

            public String getInactive_icon() {
                return inactive_icon;
            }

            // Setter Methods 

            public void setName(String name) {
                this.name = name;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public void setIcon(String icon) {
                this.icon = icon;
            }

            public void setId(String id){
                this.id = id;
            }

            public void setInactive_icon(String inactive_icon) {
                this.inactive_icon = inactive_icon;
            }
        }

        // Getter Methods

        public String getCurrency() {
            return currency;
        }

        public String getCurrent_balance() {
            return current_balance;
        }

        public Recharge_boundary getRecharge_boundary() {
            return Recharge_boundaryObject;
        }

        public String getXendit_secret_key() {
            return xendit_secret_key;
        }

        public String getXendit_public_key() {
            return xendit_public_key;
        }

        public String getExchange_value() {
            return exchange_value;
        }

        public String getCurrent_month_credit() {
            return current_month_credit;
        }

        public String getCurrent_month_debit() {
            return current_month_debit;
        }

        public String getMonth() {
            return month;
        }

        public String getYear() {
            return year;
        }

        public String getMonth_year() {
            return month_year;
        }

        public String getMin_wallet_amount() {
            return min_wallet_amount;
        }

        // Setter Methods

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public void setCurrent_balance(String current_balance) {
            this.current_balance = current_balance;
        }

        public void setRecharge_boundary(Recharge_boundary recharge_boundaryObject) {
            this.Recharge_boundaryObject = recharge_boundaryObject;
        }

        public void setXendit_secret_key(String xendit_secret_key) {
            this.xendit_secret_key = xendit_secret_key;
        }

        public void setXendit_public_key(String xendit_public_key) {
            this.xendit_public_key = xendit_public_key;
        }

        public void setExchange_value(String exchange_value) {
            this.exchange_value = exchange_value;
        }

        public void setCurrent_month_credit(String current_month_credit) {
            this.current_month_credit = current_month_credit;
        }

        public void setCurrent_month_debit(String current_month_debit) {
            this.current_month_debit = current_month_debit;
        }

        public void setMonth(String month) {
            this.month = month;
        }

        public void setYear(String year) {
            this.year = year;
        }

        public void setMonth_year(String month_year) {
            this.month_year = month_year;
        }

        public void setMin_wallet_amount(String min_wallet_amount) {
            this.min_wallet_amount = min_wallet_amount;
        }

        public class Recharge_boundary {
            @SerializedName("wallet_amount_one")
            private String wallet_amount_one;
            @SerializedName("wallet_amount_two")
            private String wallet_amount_two;
            @SerializedName("wallet_amount_three")
            private String wallet_amount_three;
            @SerializedName("wallet_amount_four")
            private String wallet_amount_four;


            // Getter Methods

            public String getWallet_amount_one() {
                return wallet_amount_one;
            }

            public String getWallet_amount_two() {
                return wallet_amount_two;
            }

            public String getWallet_amount_three() {
                return wallet_amount_three;
            }

            public String getWallet_amount_four() {
                return wallet_amount_four;
            }

            // Setter Methods

            public void setWallet_amount_one(String wallet_amount_one) {
                this.wallet_amount_one = wallet_amount_one;
            }

            public void setWallet_amount_two(String wallet_amount_two) {
                this.wallet_amount_two = wallet_amount_two;
            }

            public void setWallet_amount_three(String wallet_amount_three) {
                this.wallet_amount_three = wallet_amount_three;
            }

            public void setWallet_amount_four(String wallet_amount_four) {
                this.wallet_amount_four = wallet_amount_four;
            }
        }
    }
}


