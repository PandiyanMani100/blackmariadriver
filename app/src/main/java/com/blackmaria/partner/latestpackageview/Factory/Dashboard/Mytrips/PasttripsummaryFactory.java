package com.blackmaria.partner.latestpackageview.Factory.Dashboard.Mytrips;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.mytrips.PasttripsummaryViewModel;


public class PasttripsummaryFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public PasttripsummaryFactory(Activity context) {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new PasttripsummaryViewModel(context);
    }
}
