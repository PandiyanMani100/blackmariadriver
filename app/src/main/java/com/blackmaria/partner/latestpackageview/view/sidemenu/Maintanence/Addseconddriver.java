package com.blackmaria.partner.latestpackageview.view.sidemenu.Maintanence;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import androidx.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferService;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.CountryDialCode;
import com.blackmaria.partner.Utils.HeaderSessionManager;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.latestpackageview.Adapter.Type_of_registrationadapter;
import com.blackmaria.partner.databinding.ActivityAddseconddriverBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.Factory.Sidemenu.Maintanence.AddseconddriverFactory;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.vieewmodel.sidemenu.maintanence.AddseconddriverViewModel;
import com.countrycodepicker.CountryPicker;
import com.countrycodepicker.CountryPickerListener;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.yalantis.ucrop.UCrop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;

import static com.yalantis.ucrop.util.BitmapLoadUtils.exifToDegrees;

public class Addseconddriver extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private ActivityAddseconddriverBinding binding;
    private AddseconddriverViewModel addseconddriverViewModel;
    private HeaderSessionManager headerSessionManager;
    private SessionManager sessionManager;
    private AppUtils appUtils;
    private DatePickerDialog pickerDialog;
    private int year, month, day, iswhatlicensechoosed = 4;
    private static final int PERMISSION_REQUEST_CODE = 111;
    private int permissionCode = -1;
    private static final int SELECT_IMAGE_REQUEST = 011;
    private static final int TAKE_PHOTO_REQUEST = 022;
    private File imageRoot, destination;
    private Uri mImageCaptureUri, outputUri;
    private String appDirectoryName = "";
    private byte[] array = null;
    private CountryPicker countrypicker;

    String File_names = "";
    String Extension = "";
    String uploaded_file_name = "";
    private String s3_bucket_access_key = "";
    private String s3_bucket_secret_key = "";
    private String s3_bucket_name = "";
    private AmazonS3Client s3Client;
    private BasicAWSCredentials credentials;
    TransferObserver uploadObserver;
    AccessLanguagefromlocaldb db;


    private String sDriverID = "", Agent_Name = "", language_code = "", gcmID = "", imagename = "", drivinglic_imagename = "", drivinglic_expy = "", psvlic_imagename = "", psvlic_expy = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        binding = DataBindingUtil.setContentView(Addseconddriver.this, R.layout.activity_addseconddriver);
        addseconddriverViewModel = ViewModelProviders.of(this, new AddseconddriverFactory(this)).get(AddseconddriverViewModel.class);
        binding.setAddseconddriverViewModel(addseconddriverViewModel);
        addseconddriverViewModel.setIds(binding);

        binding.addusertxt.setText(db.getvalue("adddriver"));
        binding.username.setText(db.getvalue("uploadphoto"));
        binding.nameTv.setText(db.getvalue("name"));
        binding.mobileTv.setText(db.getvalue("mobileno"));
        binding.emailTv.setText(db.getvalue("email"));
        binding.genderTv.setText(db.getvalue("gender"));
        binding.txtspnrgender.setText(db.getvalue("selectgender"));
        binding.driverlicense.setText(db.getvalue("label_driving_license"));
        binding.driverlicenseupload.setText(db.getvalue("upload"));
        binding.psvlicense.setText(db.getvalue("label_psv_license"));
        binding.psvlicenseupload.setText(db.getvalue("upload"));
        binding.seconduseragreement.setText(db.getvalue("seconduseragreement"));
        binding.agreement.setText(db.getvalue("label_agreed"));
        binding.agreementcontent.setText(db.getvalue("addusercontent"));
        binding.submitadduser.setText(db.getvalue("submitnow"));

        initView();
        clicklistener();
        setresponse();
        SmartLocation.with(this).location()
                .start(new OnLocationUpdatedListener() {
                    @Override
                    public void onLocationUpdated(Location location) {
                        Geocoder geocoder = new Geocoder(Addseconddriver.this, Locale.getDefault());
                        try {
                            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                            if (addresses != null && !addresses.isEmpty()) {

                                String Str_getCountryCode = addresses.get(0).getCountryCode();
                                if (Str_getCountryCode.length() > 0 && !Str_getCountryCode.equals(null) && !Str_getCountryCode.equals("null")) {
                                    String Str_countyCode = CountryDialCode.getCountryCode(Str_getCountryCode);
                                    binding.dialcode.setText(Str_countyCode);
                                }
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });

    }

    private void setresponse() {
        addseconddriverViewModel.getAdddriverresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    final JSONObject jsonObject = new JSONObject(response);
                    String sStatus = jsonObject.getString("status");
                    if (sStatus.equalsIgnoreCase("1")) {
                        sessionManager.setseonddriverid(jsonObject.getString("additional_driver_id"));
                        appUtils.AlertSuccess(Addseconddriver.this, db.getvalue("action_success"), jsonObject.getString("response"));
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Intent i = new Intent(Addseconddriver.this, Maintanencedashboard.class);
                                    i.putExtra("driverid", jsonObject.getString("additional_driver_id"));
                                    startActivity(i);
                                    finish();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        }, 1000);
                    } else {
                        String Smsg = jsonObject.getString("response");
                        appUtils.AlertError(Addseconddriver.this, db.getvalue("action_error"), Smsg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


        addseconddriverViewModel.getImageuploadresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObjectwhole = new JSONObject(response);
                    String sStatus = jsonObjectwhole.getString("status");
                    if (sStatus.equalsIgnoreCase("1")) {
                        JSONObject jsonObject = jsonObjectwhole.getJSONObject("response");
                        appUtils.AlertSuccess(Addseconddriver.this, db.getvalue("action_success"), "Image Uploaded Successfully");
                        if (iswhatlicensechoosed == 0) {
                            drivinglic_imagename = jsonObject.getString("image_name");
                            opencalender();
                        } else if (iswhatlicensechoosed == 1) {
                            psvlic_imagename = jsonObject.getString("image_name");
                            opencalender();
                        } else if (iswhatlicensechoosed == 4) {
                            String image_path = jsonObject.getString("image_path");
                            imagename = jsonObject.getString("image_name");
                            sessionManager.setsecondDriverImageUpdate(image_path);
                        }
                    } else {
                        String Smsg = jsonObjectwhole.getString("response");
                        appUtils.AlertError(Addseconddriver.this, db.getvalue("action_error"), Smsg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }


    private void initView() {
        sessionManager = SessionManager.getInstance(this);
        headerSessionManager = new HeaderSessionManager(this);
        appUtils = new AppUtils(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        appDirectoryName = getResources().getString(R.string.app_name);
        HashMap<String, String> header = headerSessionManager.getHeaderSession();
        Agent_Name = header.get(HeaderSessionManager.KEY_ID_NAME);
        language_code = header.get(HeaderSessionManager.KEY_LANGUAGE_CODE);
        gcmID = user.get(SessionManager.KEY_GCM_ID);
        countrypicker = CountryPicker.newInstance(db.getvalue("select_country_lable"));
        binding.carno.setText("CAR NO : " + sessionManager.getdrivercarnumber());
        GenderSelection();

        countrypicker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {
                countrypicker.dismiss();
                binding.dialcode.setText(dialCode);
                // close keyboard
                AppUtils.CloseKeyboard(Addseconddriver.this);
            }
        });
    }

    private void GenderSelection() {
        String spinnerData[] = getResources().getStringArray(R.array.genders);
        ArrayList<String> regTypeList = new ArrayList<String>();
        regTypeList.addAll(Arrays.asList(spinnerData));
        Type_of_registrationadapter customSpinnerbank = new Type_of_registrationadapter(Addseconddriver.this, regTypeList);
        binding.selectgender.setAdapter(customSpinnerbank);
        binding.selectgender.setOnItemSelectedListener(this);
    }

    private void opencalender() {
        pickerDialog = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerDialog va, int years, int montha, int days) {
                pickerDialog.dismiss();
                int months = montha + 1;
                year = years;
                month = montha;
                day = days;
                if (iswhatlicensechoosed == 0) {
                    drivinglic_expy = day + "/" + months + "/" + year;
                } else if (iswhatlicensechoosed == 1) {
                    psvlic_expy = day + "/" + months + "/" + year;
                }
            }
        }, year, month, day);
        pickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                pickerDialog.dismiss();
            }
        });

        pickerDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                pickerDialog.dismiss();
            }
        });
        pickerDialog.setMinDate(Calendar.getInstance());
        pickerDialog.setThemeDark(false);
        pickerDialog.showYearPickerFirst(false);
        pickerDialog.setAccentColor(getResources().getColor(R.color.signin_and_signup_slider_ball_blue));
        pickerDialog.show(getFragmentManager(), "DatePickerDialog");
    }

    private void clicklistener() {
        binding.dialcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countrypicker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });

        binding.userimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iswhatlicensechoosed = 4;
                uploadpohoto();
            }
        });

        binding.submitadduser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validate();
            }
        });

        binding.driverlicenseupload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iswhatlicensechoosed = 0;
                uploadpohoto();
            }
        });

        binding.psvlicenseupload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iswhatlicensechoosed = 1;
                uploadpohoto();
            }
        });
    }

    private void validate() {
        if (imagename.equalsIgnoreCase("")) {
            appUtils.AlertError(this, db.getvalue("action_error"), db.getvalue("driver_reg_anotherdriverimage"));
        } else if (binding.editName.getText().toString().length() == 0) {
            appUtils.AlertError(this, db.getvalue("action_error"),db.getvalue("namecanntbeem"));
        } else if (binding.dialcode.getText().toString().length() == 0) {
            appUtils.AlertError(this, db.getvalue("action_error"), db.getvalue("dialcanemp"));
        } else if (binding.editMobile.getText().toString().length() == 0) {
            appUtils.AlertError(this, db.getvalue("action_error"), db.getvalue("mbnucaep"));
        } else if (binding.editMobile.getText().toString().length() < 5) {
            appUtils.AlertError(this, db.getvalue("action_error"), db.getvalue("manin"));
        } else if (binding.editEmail.getText().toString().length() == 0) {
            appUtils.AlertError(this, db.getvalue("action_error"),db.getvalue("emaican"));
        } else if (!AppUtils.isValidEmail(binding.editEmail.getText().toString().trim().replace(" ", ""))) {
            appUtils.AlertError(this, db.getvalue("action_error"), db.getvalue("enaida"));
        } else if (binding.txtspnrgender.getText().toString().equalsIgnoreCase("Select Gender")) {
            appUtils.AlertError(this, db.getvalue("action_error"), db.getvalue("selct"));
        } else if (drivinglic_imagename.equalsIgnoreCase("")) {
            appUtils.AlertError(this, db.getvalue("action_error"),db.getvalue("uploadr"));
        } else if (psvlic_imagename.equalsIgnoreCase("")) {
            appUtils.AlertError(this, db.getvalue("action_error"),db.getvalue("psvli"));
        } else if (!binding.agree.isChecked()) {
            appUtils.AlertError(this,db.getvalue("action_error"),db.getvalue("usag"));
        } else {
            HashMap<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("driver_id", sDriverID);
            jsonParams.put("additional_driver_id", "");
            jsonParams.put("mode", "update");
            jsonParams.put("profile_image", imagename);
            jsonParams.put("profile_image_status", "1");
            jsonParams.put("mobile_number", binding.editMobile.getText().toString().trim());
            jsonParams.put("dail_code", binding.dialcode.getText().toString().trim());
            jsonParams.put("driver_name", binding.editName.getText().toString());
            jsonParams.put("gender_type", binding.txtspnrgender.getText().toString());
            jsonParams.put("email", binding.editEmail.getText().toString());
            jsonParams.put("driving_license", drivinglic_imagename);
            jsonParams.put("driving_license_status", "1");
            jsonParams.put("psv_license", psvlic_imagename);
            jsonParams.put("psv_license_status", "1");
            jsonParams.put("agreement", "yes");
            jsonParams.put("licence_expiry", drivinglic_expy);
            jsonParams.put("psv_license_expiry", psvlic_expy);
            addseconddriverViewModel.updateanotherdriver(Iconstant.updateanotherdriver, jsonParams);
        }
    }


    public void uploadpohoto() {
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            if (!checkCameraPermission() || !checkReadExternalStoragePermission() || !checkWriteExternalStoragePermission()) {
                requestPermission();
            } else {
                chooseimage();
            }
        } else {
            chooseimage();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (permissionCode == 0) {
                        cameraIntent();
                    } else if (permissionCode == 1) {
                        galleryIntent();
                    }
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_IMAGE_REQUEST) {
                onSelectFromGalleryResult(data);
            } else if (requestCode == TAKE_PHOTO_REQUEST) {
                onCaptureImageResult(data);
            } else if (requestCode == UCrop.REQUEST_CROP) {
                onCroppedImageResult(data);
            } else if (resultCode == UCrop.RESULT_ERROR) {
                final Throwable cropError = UCrop.getError(data);
            }
        }
    }


    public void chooseimage() {
        final Dialog photo_dialog = new Dialog(this);
        photo_dialog.getWindow();
        photo_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        photo_dialog.setContentView(R.layout.image_upload_dialog);
        photo_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        photo_dialog.setCanceledOnTouchOutside(true);
        photo_dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_photo_Picker;
        photo_dialog.show();
        photo_dialog.getWindow().setGravity(Gravity.CENTER);

        RelativeLayout camera = (RelativeLayout) photo_dialog
                .findViewById(R.id.profilelayout_takephotofromcamera);
        RelativeLayout gallery = (RelativeLayout) photo_dialog
                .findViewById(R.id.profilelayout_takephotofromgallery);

        TextView howdo = (TextView) photo_dialog
                .findViewById(R.id.howdo);
        TextView tph = (TextView) photo_dialog
                .findViewById(R.id.tph);
        TextView chosex = (TextView) photo_dialog
                .findViewById(R.id.chosex);
        howdo.setText(db.getvalue("takephoto"));
        tph.setText(db.getvalue("camra"));
        chosex.setText(db.getvalue("existingcamera"));

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraIntent();
                photo_dialog.dismiss();
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                galleryIntent();
                photo_dialog.dismiss();
            }
        });
    }

    public void cameraIntent() {
        try {
            Intent pictureIntent = new Intent(
                    MediaStore.ACTION_IMAGE_CAPTURE);
            if (pictureIntent.resolveActivity(getPackageManager()) != null) {
                //Create a file to store the image
                imageRoot = new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES), appDirectoryName);
            }
            if (imageRoot != null) {
                String name = dateToString(new Date(), "yyyy-MM-dd-hh-mm-ss");
                destination = new File(imageRoot, name + ".jpg");
                Uri photoURI = FileProvider.getUriForFile(this, "com.blackmaria.provider", destination);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        photoURI);
                pictureIntent.putExtra("android.intent.extras.CAMERA_FACING", 1);
                startActivityForResult(pictureIntent,
                        TAKE_PHOTO_REQUEST);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String dateToString(Date date, String format) {
        SimpleDateFormat df = new SimpleDateFormat(format);
        return df.format(date);
    }

    public void galleryIntent() {
        try {
            imageRoot = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES), appDirectoryName);
            if (!imageRoot.exists()) {
                imageRoot.mkdir();
            }
            Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setType("image/*");
            startActivityForResult(intent, SELECT_IMAGE_REQUEST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean checkCameraPermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkReadExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkWriteExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
    }

    public void onCaptureImageResult(Intent data) {
        try {
            String imagePath = destination.getAbsolutePath();
            mImageCaptureUri = Uri.fromFile(new File(imagePath));
            outputUri = mImageCaptureUri;

            UCrop.Options options = new UCrop.Options();
            options.setStatusBarColor(getResources().getColor(R.color.black_color));
            options.setToolbarColor(getResources().getColor(R.color.app_primary_color));
            options.setMaxBitmapSize(800);

            UCrop.of(mImageCaptureUri, outputUri)
                    .withAspectRatio(1, 1)
                    .withMaxResultSize(8000, 8000)
                    .withOptions(options)
                    .start(this);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onSelectFromGalleryResult(Intent data) {

        try {
            mImageCaptureUri = data.getData();

            if (!imageRoot.exists()) {
                imageRoot.mkdir();
            }

            String name = dateToString(new Date(), "yyyy-MM-dd-hh-mm-ss");
            destination = new File(imageRoot, name + ".jpg");

            outputUri = Uri.fromFile(destination);

            UCrop.Options options = new UCrop.Options();
            options.setStatusBarColor(getResources().getColor(R.color.black_color));
            options.setToolbarColor(getResources().getColor(R.color.app_primary_color));
            options.setMaxBitmapSize(800);

            UCrop.of(mImageCaptureUri, outputUri)
                    .withAspectRatio(1, 1)
                    .withMaxResultSize(8000, 8000)
                    .withOptions(options)
                    .start(this);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void onCroppedImageResult(Intent data) {

        final Uri resultUri = UCrop.getOutput(data);

        Log.d("Crop success", "" + resultUri);
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), resultUri);

            if (bitmap == null) {
                Log.d("Bitmap", "null");
            } else {
                Log.d("Bitmap", "not null");
                if (iswhatlicensechoosed == 4) {
                    binding.userimage.setImageBitmap(bitmap);
                } else if (iswhatlicensechoosed == 0) {
                    binding.driverlicenseupload.setText("Uploaded");
                } else if (iswhatlicensechoosed == 1) {
                    binding.psvlicenseupload.setText("Uploaded");
                }
            }

            Bitmap thumbnail = bitmap;
            final String picturePath = resultUri.getPath();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            File curFile = new File(picturePath);
            try {
                ExifInterface exif = new ExifInterface(curFile.getPath());
                int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                int rotationInDegrees = exifToDegrees(rotation);

                Matrix matrix = new Matrix();
                if (rotation != 0f) {
                    matrix.preRotate(rotationInDegrees);
                }
                thumbnail = Bitmap.createBitmap(thumbnail, 0, 0, thumbnail.getWidth(), thumbnail.getHeight(), matrix, true);
            } catch (IOException ex) {
                Log.e("TAG", "Failed to get Exif data", ex);
            }

            thumbnail.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
            array = byteArrayOutputStream.toByteArray();

            if(sessionManager.getimagestatus().equals("1"))
            {
                s3_bucket_name = sessionManager.getbucketname();
                s3_bucket_access_key = sessionManager.getaccesskey();
                s3_bucket_secret_key = sessionManager.getsecretkey();

                startService(new Intent(getApplicationContext(), TransferService.class));
                credentials = new BasicAWSCredentials(s3_bucket_access_key, s3_bucket_secret_key);
                s3Client = new AmazonS3Client(credentials);
                s3Client.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));
                uploadToS3Bucket(curFile,Addseconddriver.this);
            }
            else
            {
                addseconddriverViewModel.uploadimage(Iconstant.DriverReg_UploadVehicleImage_URl, array, "blackmariavehicle.jpg");

            }




        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void uploadToS3Bucket(File fileUri,Activity context)
    {

        if(fileUri.exists())
        {
            System.out.println("file exist");
        }
        else
        {
            System.out.println("file exist not");
        }


        String[] path_split;
        if (fileUri != null) {

            final Dialog dialog = new Dialog(context);
            dialog.getWindow();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.custom_loading);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();

            uploaded_file_name = "";
            String path = String.valueOf(fileUri);
            String file_name_full = path.substring(path.lastIndexOf("/") + 1);
            path_split = file_name_full.split(".");
            if (path_split.length == 2) {
                File_names = path_split[0];
                Extension = path_split[1];

            } else {
                path_split = file_name_full.split(".jpg");
                if (path_split.length == 1) {
                    File_names = path_split[0];
                }
            }

            String file_name_upload = File_names;
            //current time stamp
            Long tsLong = System.currentTimeMillis() / 1000;
            String str_current_ts = tsLong.toString();
            String concat_image_name = file_name_upload + "" + str_current_ts;
            final String fileName = md5Encryption(concat_image_name) + "." + "jpg";


            TransferUtility transferUtility =
                    TransferUtility.builder()
                            .context(context)
                            .defaultBucket(s3_bucket_name)
                            .s3Client(s3Client)
                            .build();
            uploaded_file_name = fileName;

            if (iswhatlicensechoosed == 0) {
                uploadObserver = transferUtility.upload("drivers_documents/" + fileName, fileUri, CannedAccessControlList.PublicRead);

            } else if (iswhatlicensechoosed == 1) {
                uploadObserver = transferUtility.upload("drivers_documents/" + fileName, fileUri, CannedAccessControlList.PublicRead);

            } else if (iswhatlicensechoosed == 4) {
                uploadObserver = transferUtility.upload("images/users/" + fileName, fileUri, CannedAccessControlList.PublicRead);

            }
            // Attach a listener to the observer to get state update and progress notifications
            uploadObserver.setTransferListener(new TransferListener() {

                @Override
                public void onStateChanged(int id, TransferState state) {
                    // Handle a completed upload.
                    if (TransferState.COMPLETED == state) {
                        dialog.dismiss();
                        String path = uploadObserver.getAbsoluteFilePath();

                        if (iswhatlicensechoosed == 0) {
                            drivinglic_imagename = fileName;
                            opencalender();
                        } else if (iswhatlicensechoosed == 1) {
                            psvlic_imagename =fileName;
                            opencalender();
                        } else if (iswhatlicensechoosed == 4) {
                            imagename = fileName;
                        }

                    } else if (TransferState.FAILED == state) {
                        // dialog.dismiss();
                        dialog.dismiss();
                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                    float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                    int percentDone = (int) percentDonef;
                    Log.d("YourActivity", "ID:" + id + " bytesCurrent: " + bytesCurrent + " bytesTotal: " + bytesTotal + " " + percentDone + "%");
                }

                @Override
                public void onError(int id, Exception ex) {
                    // Handle errors
                    ex.printStackTrace();
                    uploaded_file_name = "";

                    dialog.dismiss();
                }

            });

            // If you prefer to poll for the data, instead of attaching a
            // listener, check for the state and progress in the observer.
            if (TransferState.COMPLETED == uploadObserver.getState()) {
                // Handle a completed upload.
                Log.d("completed upload", "completed upload");
            }

        }
    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String sSelectedItem = binding.selectgender.getSelectedItem().toString();
        binding.txtspnrgender.setText(sSelectedItem);
    }

    public String md5Encryption(String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SmartLocation.with(this).location().stop();

    }
}
