package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class monthtrippojo implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }

    public class Response {
        @SerializedName("month")
        Month MonthObject;
        @SerializedName("from_date")
        private String from_date;
        @SerializedName("to_date")
        private String to_date;
        @SerializedName("filter_month")
        private String filter_month;
        @SerializedName("filter_year")
        private String filter_year;
        @SerializedName("month_desc")
        private String month_desc;


        // Getter Methods

        public Month getMonth() {
            return MonthObject;
        }

        public String getFrom_date() {
            return from_date;
        }

        public String getTo_date() {
            return to_date;
        }

        public String getFilter_month() {
            return filter_month;
        }

        public String getFilter_year() {
            return filter_year;
        }

        public String getMonth_desc() {
            return month_desc;
        }

        // Setter Methods

        public void setMonth(Month monthObject) {
            this.MonthObject = monthObject;
        }

        public void setFrom_date(String from_date) {
            this.from_date = from_date;
        }

        public void setTo_date(String to_date) {
            this.to_date = to_date;
        }

        public void setFilter_month(String filter_month) {
            this.filter_month = filter_month;
        }

        public void setFilter_year(String filter_year) {
            this.filter_year = filter_year;
        }

        public void setMonth_desc(String month_desc) {
            this.month_desc = month_desc;
        }

        public class Month {
            @SerializedName("completed_rides")
            private String completed_rides;
            @SerializedName("bidding_trip")
            private String bidding_trip;
            @SerializedName("sos_trip")
            private String sos_trip;
            @SerializedName("cancel_rides")
            private String cancel_rides;

            public String getDriver_cancel_rides() {
                return driver_cancel_rides;
            }

            public void setDriver_cancel_rides(String driver_cancel_rides) {
                this.driver_cancel_rides = driver_cancel_rides;
            }

            @SerializedName("driver_cancel_rides")
            private String driver_cancel_rides;


            // Getter Methods

            public String getCompleted_rides() {
                return completed_rides;
            }

            public String getBidding_trip() {
                return bidding_trip;
            }

            public String getSos_trip() {
                return sos_trip;
            }

            public String getCancel_rides() {
                return cancel_rides;
            }

            // Setter Methods

            public void setCompleted_rides(String completed_rides) {
                this.completed_rides = completed_rides;
            }

            public void setBidding_trip(String bidding_trip) {
                this.bidding_trip = bidding_trip;
            }

            public void setSos_trip(String sos_trip) {
                this.sos_trip = sos_trip;
            }

            public void setCancel_rides(String cancel_rides) {
                this.cancel_rides = cancel_rides;
            }
        }
    }
}


