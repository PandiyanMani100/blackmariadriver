package com.blackmaria.partner.latestpackageview.view.signup;

import android.app.Dialog;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blackmaria.partner.Interface.Slider;
import com.blackmaria.partner.Pojo.RegisterDriverLocationCatCarPojo;
import com.blackmaria.partner.Pojo.selectlocation_pojo;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.adapter.CustomSpinnerAdapter;
import com.blackmaria.partner.latestpackageview.Adapter.SlidingImage_Adapter_homepage;
import com.blackmaria.partner.databinding.ActivityRegistrationSelectLocationConstrainBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.Factory.SignUp.RegistrationSelectLocationFactory;
import com.blackmaria.partner.latestpackageview.vieewmodel.signup.RegistrationSelectLocationViewModel;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

public class RegistrationSelectLocation_constrain extends AppCompatActivity {

    private RegistrationSelectLocationViewModel registrationSelectLocationViewModel;

    private SessionManager sessionManager;
    private ConnectionDetector cd;
    private String sDriverID = "";
    private boolean isInternetPresent = false;
    private String locationLat = "", locationLon = "", driverLocationPlaceId = "";
    private ArrayList<RegisterDriverLocationCatCarPojo> listCarCatDetails;

    private selectlocation_pojo locationlist;
    private ActivityRegistrationSelectLocationConstrainBinding binding;
    private int selectlocation_position = 0;

    AccessLanguagefromlocaldb db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_registration_select_location_constrain);
        registrationSelectLocationViewModel = ViewModelProviders.of(this, new RegistrationSelectLocationFactory(this)).get(RegistrationSelectLocationViewModel.class);
        binding.setRegistrationSelectLocationViewModel(registrationSelectLocationViewModel);

        registrationSelectLocationViewModel.setIds(binding);

        binding.chooselocation.setText(db.getvalue("chooselocation"));
        binding.locationtext.setText(db.getvalue("select_location"));
        binding.tapyourvehicle.setText(db.getvalue("tabvehicle"));
        binding.slidemorevehicle.setText(db.getvalue("slide_for_more_vehicles"));

        initView();
        clicklistener();
    }


    private void initView() {
        sessionManager = SessionManager.getInstance(this);
        cd = new ConnectionDetector(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);

        registrationSelectLocationViewModel.getlocationslist();

        registrationSelectLocationViewModel.getGetlocationlist().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    String Sstatus = object.getString("status");
                    String Smessage = object.getString("response");
                    locationlist = new selectlocation_pojo();
                    if (Sstatus.equalsIgnoreCase("1")) {
                        Type type = new TypeToken<selectlocation_pojo>() {
                        }.getType();
                        locationlist = new GsonBuilder().create().fromJson(object.toString(), type);

                        CustomSpinnerAdapter customSpinnerbank = new CustomSpinnerAdapter(getApplicationContext(), locationlist.getResponse().getLocations());
                        binding.selectlocation.setAdapter(customSpinnerbank);
                    } else {
                        Alert(db.getvalue("action_error"), Smessage);
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                }
            }
        });

        registrationSelectLocationViewModel.getLocationbasedcategory().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        String status = object.getString("status");
                        String message = object.getString("message");

                        if (status.equalsIgnoreCase("1")) {
                            JSONObject jsonObject = object.getJSONObject("response");
                            driverLocationPlaceId = jsonObject.getString("location_id");
                            if (jsonObject.length() > 0) {
                                Object intervention = jsonObject.get("category");
                                if (intervention instanceof JSONArray) {
                                    JSONArray categoryArray = jsonObject.getJSONArray("category");
                                    if (categoryArray.length() > 0) {
                                        listCarCatDetails = new ArrayList<RegisterDriverLocationCatCarPojo>();
                                        if(categoryArray.length() > 0 )
                                        {
                                            binding.slidemorevehicle.setVisibility(View.VISIBLE);
                                        }
                                        else
                                        {
                                            binding.slidemorevehicle.setVisibility(View.GONE);
                                        }
                                        for (int i = 0; i < categoryArray.length(); i++) {
                                            JSONObject locCtaListObj = categoryArray.getJSONObject(i);
                                            String cc = locCtaListObj.getString("cc");
                                            String id = locCtaListObj.getString("id");
                                            String name = locCtaListObj.getString("name");
                                            String no_of_seats = locCtaListObj.getString("no_of_seats");
                                            String offer_type = locCtaListObj.getString("offer_type");
                                            String vehicle_type = locCtaListObj.getString("vehicle_type");
                                            String description = locCtaListObj.getString("description");
                                            String icon_normal = locCtaListObj.getString("icon_normal");
                                            listCarCatDetails.add(new RegisterDriverLocationCatCarPojo(cc, id, name, no_of_seats, offer_type, vehicle_type, description, icon_normal));
                                        }
                                    }
                                }
                                init_slidebanner(listCarCatDetails);
                            }
                        } else {
                            String sResponse = object.getString("response");
                            Alert(db.getvalue("action_error"), sResponse);
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });
    }

    private void clicklistener() {
        binding.selectlocation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                selectlocation_position = position;
                binding.locationtext.setText(locationlist.getResponse().getLocations().get(position).getCity());
                if(locationlist.getResponse().getLocations().get(position).getCity().equalsIgnoreCase("select location"))
                {

                }
                else
                {
                    isInternetPresent = cd.isConnectingToInternet();
                    if (isInternetPresent) {
                        if (locationlist.getResponse().getLocations().size() > 0) {
                            registrationSelectLocationViewModel.getlocationbasedcateogry(Iconstant.DriverReg_Locationwise_Carcategory_URL, locationlist.getResponse().getLocations().get(selectlocation_position).getLat(), locationlist.getResponse().getLocations().get(selectlocation_position).getLong());
                        }
                    } else {
                        Alert(db.getvalue("alert_nointernet"), db.getvalue("alert_nointernet_message"));
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    private void init_slidebanner(ArrayList<RegisterDriverLocationCatCarPojo> listCarCatDetails) {

        binding.pager.setAdapter(new SlidingImage_Adapter_homepage(slider, getApplicationContext(), listCarCatDetails));

        binding.indicator.setViewPager(binding.pager);
        final float density = getResources().getDisplayMetrics().density;
        binding.indicator.setRadius(6 * density);

        // Pager listener over indicator
        binding.indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }

    Slider slider = new Slider() {
        @Override
        public void onComplete(int position) {
            AlertConfirmation(db.getvalue("confiemation"), db.getvalue("signupfo")+" " + listCarCatDetails.get(position).getCatName() +" "+ db.getvalue("yesp"), position);
        }
    };

    private void AlertConfirmation(String title, String message, final int pos) {
        final Dialog confirmDialog ;
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 85% of the screen

        View view = View.inflate(this, R.layout.alert_yes_or_no, null);
        confirmDialog = new Dialog(this);
        confirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        confirmDialog.setContentView(view);
        confirmDialog.setCanceledOnTouchOutside(false);
        confirmDialog.getWindow().setLayout(screenWidth, LinearLayout.LayoutParams.WRAP_CONTENT);

        final TextView Tv_title =  confirmDialog.findViewById(R.id.custom_dialog_library_title_textview);
        final TextView Tv_message =  confirmDialog.findViewById(R.id.custom_dialog_library_message_textview);
        final TextView Btn_yes =  confirmDialog.findViewById(R.id.custom_dialog_library_ok_button);
        final TextView Btn_no =  confirmDialog.findViewById(R.id.custom_dialog_library_cancel_button);

        Tv_title.setText(title);
        Tv_message.setText(message);
        Btn_yes.setText(db.getvalue("action_yes"));
        Btn_no.setText(db.getvalue("action_no"));

        Btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegistrationSelectLocation_constrain.this, RegistrationLoginDetails.class);
                intent.putExtra("vehicleId", listCarCatDetails.get(pos).getCatId());
                intent.putExtra("driverLocationPlaceId", driverLocationPlaceId);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                confirmDialog.dismiss();

            }
        });

        Btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmDialog.dismiss();
            }
        });

        confirmDialog.show();

    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(db.getvalue("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }
}
