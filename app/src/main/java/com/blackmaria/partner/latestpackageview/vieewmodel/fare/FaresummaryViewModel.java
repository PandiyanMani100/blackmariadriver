package com.blackmaria.partner.latestpackageview.vieewmodel.fare;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import com.blackmaria.partner.R;
import com.blackmaria.partner.databinding.ActivityFareSummaryConstrainBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;

import java.util.HashMap;

public class FaresummaryViewModel extends ViewModel implements ApIServices.completelisner {
    private Activity context;
    private ActivityFareSummaryConstrainBinding binding;
    private Dialog dialog;
    private MutableLiveData<String> fareresponse = new MutableLiveData<>();
    private MutableLiveData<String> saveplusresponse = new MutableLiveData<>();
    private int count = 0;

    public FaresummaryViewModel(Activity context) {
        this.context = context;
    }

    public void setIds(ActivityFareSummaryConstrainBinding binding) {
        this.binding = binding;
    }

    public MutableLiveData<String> getFareresponse() {
        return fareresponse;
    }

    public MutableLiveData<String> getSaveplusresponse() {
        return saveplusresponse;
    }

    public void farebreakupapihit(HashMap<String, String> jsonParams) {
        count = 0;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ApIServices apIServices = new ApIServices(context, FaresummaryViewModel.this);
        apIServices.dooperation(Iconstant.faredetail_Url, jsonParams);
    }

    public void saveplusapihit(HashMap<String, String> jsonParams) {
        count = 1;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ApIServices apIServices = new ApIServices(context, FaresummaryViewModel.this);
        apIServices.dooperation(Iconstant.Transfersaveplus, jsonParams);
    }

    @Override
    public void sucessresponse(String val) {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        if (count == 0) {
            fareresponse.postValue(val);
        } else if (count == 1) {
            saveplusresponse.postValue(val);
        }

    }

    @Override
    public void errorreponse() {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
    }

    @Override
    public void jsonexception() {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        AppUtils.toastprint(context, "Json Exception");
    }
}
