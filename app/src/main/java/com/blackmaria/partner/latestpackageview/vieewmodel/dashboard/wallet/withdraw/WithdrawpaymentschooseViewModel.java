package com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet.withdraw;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import com.blackmaria.partner.R;
import com.blackmaria.partner.databinding.ActivityWithdrawpaymentschooseConstrainBinding;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;

import java.util.HashMap;

public class WithdrawpaymentschooseViewModel extends ViewModel implements ApIServices.completelisner {
    private Activity context;
    private Dialog dialog;
    private ActivityWithdrawpaymentschooseConstrainBinding binding;
    private int count = 0;
    private MutableLiveData<String> walletwithdrawresponse = new MutableLiveData<>();
    private MutableLiveData<String> choosepaymentswithdrawalsresponse = new MutableLiveData<>();

    public WithdrawpaymentschooseViewModel(Activity context) {
        this.context = context;
    }

    public MutableLiveData<String> getWalletwithdrawresponse() {
        return walletwithdrawresponse;
    }

    public MutableLiveData<String> getChoosepaymentswithdrawalsresponse() {
        return choosepaymentswithdrawalsresponse;
    }

    public void walletwithdrawhome(String url, HashMap<String, String> jsonParams) {
        count = 0;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        ApIServices apIServices = new ApIServices(context, WithdrawpaymentschooseViewModel.this);
        apIServices.dooperation(url, jsonParams);
    }

    public void choosepaymentwithdrawals(String url, HashMap<String, String> jsonParams) {
        count = 1;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        ApIServices apIServices = new ApIServices(context, WithdrawpaymentschooseViewModel.this);
        apIServices.dooperation(url, jsonParams);
    }


    @Override
    public void sucessresponse(String val) {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        if (count == 0) {
            walletwithdrawresponse.postValue(val);
        } else if (count == 1) {
            choosepaymentswithdrawalsresponse.postValue(val);
        }
    }

    @Override
    public void errorreponse() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void jsonexception() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    public void setIds(ActivityWithdrawpaymentschooseConstrainBinding binding) {
        this.binding = binding;
    }

    public void back(){
        context.onBackPressed();
    }
}
