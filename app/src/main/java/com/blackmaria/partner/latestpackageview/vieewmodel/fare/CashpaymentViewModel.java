package com.blackmaria.partner.latestpackageview.vieewmodel.fare;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import com.blackmaria.partner.R;
import com.blackmaria.partner.databinding.ActivityCashpaymentBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;

import java.util.HashMap;

public class CashpaymentViewModel extends ViewModel implements ApIServices.completelisner {
    private Activity context;
    private ActivityCashpaymentBinding binding;
    private Dialog dialog;
    private MutableLiveData<String> fareresponse = new MutableLiveData<>();
    private MutableLiveData<String> receivepaymentresponse= new MutableLiveData<>();
    private int count =0;

    public CashpaymentViewModel(Activity context){
        this.context = context;
    }

    public void setIds(ActivityCashpaymentBinding binding) {
        this.binding = binding;
    }

    public MutableLiveData<String> getFareresponse() {
        return fareresponse;
    }

    public MutableLiveData<String> getReceivepaymentresponse() {
        return receivepaymentresponse;
    }

    public void farebreakupapihit(HashMap<String, String> jsonParams) {
        count = 0;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ApIServices apIServices = new ApIServices(context, CashpaymentViewModel.this);
        apIServices.dooperation(Iconstant.faredetail_Url, jsonParams);
    }



    public void receivepaymentapihit(HashMap<String, String> jsonParams) {
        count =1;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ApIServices apIServices = new ApIServices(context, CashpaymentViewModel.this);
        apIServices.dooperation(Iconstant.payment_received_Url, jsonParams);
    }

    @Override
    public void sucessresponse(String val) {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        if(count ==0){
            fareresponse.postValue(val);
        }else if(count ==1){
            receivepaymentresponse.postValue(val);
        }

    }

    @Override
    public void errorreponse() {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
    }

    @Override
    public void jsonexception() {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        AppUtils.toastprint(context, "Json Exception");
    }
}
