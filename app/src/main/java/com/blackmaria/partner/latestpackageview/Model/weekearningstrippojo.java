package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class weekearningstrippojo implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }

    public class Response {
        public ArrayList<Week> getWeek() {
            return week;
        }

        public void setWeek(ArrayList<Week> week) {
            this.week = week;
        }
        @SerializedName("week")
        ArrayList < Week > week = new ArrayList< Week >();
        @SerializedName("total_rides")
        private String total_rides;
        @SerializedName("date_desc")
        private String date_desc;

        public class Week{
            @SerializedName("day")
            private String day;
            @SerializedName("ridecount")
            private String ridecount;
            @SerializedName("distance")
            private String distance;
            @SerializedName("driver_earning")
            private String driver_earning;
            @SerializedName("currency")
            private String currency;
            @SerializedName("order")
            private String order;
            @SerializedName("date_f")
            private String date_f;


            // Getter Methods 

            public String getDay() {
                return day;
            }

            public String getRidecount() {
                return ridecount;
            }

            public String getDistance() {
                return distance;
            }

            public String getDriver_earning() {
                return driver_earning;
            }

            public String getCurrency() {
                return currency;
            }

            public String getOrder() {
                return order;
            }

            public String getDate_f() {
                return date_f;
            }

            // Setter Methods 

            public void setDay(String day) {
                this.day = day;
            }

            public void setRidecount(String ridecount) {
                this.ridecount = ridecount;
            }

            public void setDistance(String distance) {
                this.distance = distance;
            }

            public void setDriver_earning(String driver_earning) {
                this.driver_earning = driver_earning;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public void setOrder(String order) {
                this.order = order;
            }

            public void setDate_f(String date_f) {
                this.date_f = date_f;
            }
        }

        // Getter Methods

        public String getTotal_rides() {
            return total_rides;
        }

        public String getDate_desc() {
            return date_desc;
        }

        // Setter Methods

        public void setTotal_rides(String total_rides) {
            this.total_rides = total_rides;
        }

        public void setDate_desc(String date_desc) {
            this.date_desc = date_desc;
        }
    }
}

