package com.blackmaria.partner.latestpackageview.view.fare;


import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityCashpaymentBinding;
import com.blackmaria.partner.latestpackageview.Factory.Fare.CashpaymentFactory;
import com.blackmaria.partner.latestpackageview.Model.continuetrippojo;
import com.blackmaria.partner.latestpackageview.Model.farepojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Dashboard_constrain;
import com.blackmaria.partner.latestpackageview.vieewmodel.fare.CashpaymentViewModel;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

public class Cashpayment extends AppCompatActivity {

    private ActivityCashpaymentBinding binding;
    private CashpaymentViewModel cashpaymentViewModel;
    private SessionManager sessionManager;
    private String sDriverID = "", grandfare = "";
    private AppUtils appUtils;
    private continuetrippojo object;
    private farepojo fareobjec;

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(Cashpayment.this, R.layout.activity_cashpayment);
        cashpaymentViewModel = ViewModelProviders.of(this, new CashpaymentFactory(this)).get(CashpaymentViewModel.class);
        binding.setCashpaymentViewModel(cashpaymentViewModel);
        cashpaymentViewModel.setIds(binding);


        initView();
        clicklistener();
    }

    private void clicklistener() {
        binding.btnReceive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HashMap<String, String> jsonParams = new HashMap<String, String>();
                jsonParams.put("ride_id", object.getResponse().getUser_profile().getRide_id());
                jsonParams.put("driver_id", sDriverID);
                jsonParams.put("amount", fareobjec.getResponse().getFare().getGrand_fare());
                cashpaymentViewModel.receivepaymentapihit(jsonParams);
                cashpaymentViewModel.getReceivepaymentresponse().observe(Cashpayment.this, new Observer<String>() {
                    @Override
                    public void onChanged(@Nullable String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                appUtils.AlertSuccess(Cashpayment.this, getResources().getString(R.string.action_success), jsonObject.getString("response"));
                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        //Do something after 100ms
                                        Intent intent = new Intent(Cashpayment.this, FareSummaryConstrain.class);
                                        intent.putExtra("rideId", object.getResponse().getUser_profile().getRide_id());
                                        startActivity(intent);
                                        finish();
                                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                    }
                                }, 1500);
                            } else {
                                appUtils.AlertError(Cashpayment.this, getResources().getString(R.string.action_error), jsonObject.getString("response"));
                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        //Do something after 100ms
                                        Intent intent = new Intent(Cashpayment.this, Dashboard_constrain.class);
                                        startActivity(intent);
                                        finish();
                                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                    }
                                }, 1500);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }

    private void initView() {
        sessionManager = SessionManager.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        appUtils = AppUtils.getInstance(this);

        try {
            JSONObject jsonObject = new JSONObject(sessionManager.getContinuetripjson());
            Type type = new TypeToken<continuetrippojo>() {
            }.getType();
            object = new GsonBuilder().create().fromJson(jsonObject.toString(), type);
            HashMap<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("ride_id", object.getResponse().getUser_profile().getRide_id());
            jsonParams.put("driver_id", sDriverID);
            cashpaymentViewModel.farebreakupapihit(jsonParams);
            cashpaymentViewModel.getFareresponse().observe(this, new Observer<String>() {
                @Override
                public void onChanged(@Nullable String response) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        Type type = new TypeToken<farepojo>() {
                        }.getType();
                        fareobjec = new GsonBuilder().create().fromJson(jsonObject.toString(), type);
                        if (fareobjec.getStatus().equalsIgnoreCase("1")) {
                            sessionManager.setisride("");
                            binding.txtTotalFare.setText(fareobjec.getResponse().getCurrency() + " " + fareobjec.getResponse().getFare().getGrand_fare());
                        } else {
                            sessionManager.setisride("");
                            appUtils.AlertError(Cashpayment.this, getResources().getString(R.string.action_error), jsonObject.getString("response"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
