package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class withdrawpaymentsendpojo implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    private String response;
    @SerializedName("mode")
    private String mode;
    @SerializedName("ref_no")
    private String ref_no;
    @SerializedName("amount")
    private String amount;
    @SerializedName("currency")
    private String currency;
    @SerializedName("from")
    private String from;
    @SerializedName("date")
    private String date;
    @SerializedName("time")
    private String time;
    @SerializedName("pay_to")
    private String pay_to;
    @SerializedName("account")
    private String account;
    @SerializedName("country_code")
    private String country_code;

    @SerializedName("qrcode")
    private String qrcode;

    public String getQrcode() {
        return qrcode;
    }

    public void setQrcode(String qrcode) {
        this.qrcode = qrcode;
    }

    public String getCash_code() {
        return cash_code;
    }

    public void setCash_code(String cash_code) {
        this.cash_code = cash_code;
    }

    @SerializedName("cash_code")
    private String cash_code;


    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    @SerializedName("phone_number")
    private String phone_number;

    public String getPaypal_id() {
        return paypal_id;
    }

    public void setPaypal_id(String paypal_id) {
        this.paypal_id = paypal_id;
    }

    @SerializedName("paypal_id")
    private String paypal_id;


    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    private String bank_name;


    // Getter Methods 

    public String getStatus() {
        return status;
    }

    public String getResponse() {
        return response;
    }

    public String getMode() {
        return mode;
    }

    public String getRef_no() {
        return ref_no;
    }

    public String getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public String getFrom() {
        return from;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getPay_to() {
        return pay_to;
    }

    public String getAccount() {
        return account;
    }

    // Setter Methods 

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public void setRef_no(String ref_no) {
        this.ref_no = ref_no;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setPay_to(String pay_to) {
        this.pay_to = pay_to;
    }

    public void setAccount(String account) {
        this.account = account;
    }
}
