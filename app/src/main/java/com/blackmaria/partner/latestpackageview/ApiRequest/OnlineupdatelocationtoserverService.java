package com.blackmaria.partner.latestpackageview.ApiRequest;


import android.app.IntentService;
import android.content.Intent;

import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.Model.eariningsdashboardpojo;
import com.blackmaria.partner.latestpackageview.Model.onlineupdatelocation;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

public class OnlineupdatelocationtoserverService extends IntentService implements ApIServices.completelisner {

    private String url = "";

    public OnlineupdatelocationtoserverService() {
        super("MyService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        HashMap<String, String> hashMap = (HashMap<String, String>) intent.getSerializableExtra("params");
        url = intent.getStringExtra("url");
        ApIServices apIServices = new ApIServices(getApplicationContext(), OnlineupdatelocationtoserverService.this);
        apIServices.dooperation(url, hashMap);
    }

    @Override
    public void sucessresponse(String val) {
        try {
            JSONObject jsonObject = new JSONObject(val);
            Type type = new TypeToken<onlineupdatelocation>() {
            }.getType();
            onlineupdatelocation onlineupdatelocation = new GsonBuilder().create().fromJson(jsonObject.toString(), type);
            EventBus.getDefault().post(onlineupdatelocation);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void errorreponse() {
        AppUtils.toastprint(this,"Update location error response");
    }

    @Override
    public void jsonexception() {
        AppUtils.toastprint(this,"Update location error response");
    }
}
