package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class todaytripviewdetailpojo implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }

    public class Response {
        @SerializedName("current_page")
        private String current_page;
        @SerializedName("next_page")
        private String next_page;
        @SerializedName("perPage")
        private String perPage;

        public ArrayList<Rides> getRides() {
            return rides;
        }

        public void setRides(ArrayList<Rides> rides) {
            this.rides = rides;
        }

        @SerializedName("rides")
        ArrayList<Rides> rides = new ArrayList<Rides>();
        @SerializedName("total_rides")
        private String total_rides;
        @SerializedName("filter_date")
        private String filter_date;



        // Getter Methods

        public String getCurrent_page() {
            return current_page;
        }

        public String getNext_page() {
            return next_page;
        }

        public String getPerPage() {
            return perPage;
        }

        public String getTotal_rides() {
            return total_rides;
        }

        public String getFilter_date() {
            return filter_date;
        }

        // Setter Methods

        public void setCurrent_page(String current_page) {
            this.current_page = current_page;
        }

        public void setNext_page(String next_page) {
            this.next_page = next_page;
        }

        public void setPerPage(String perPage) {
            this.perPage = perPage;
        }

        public void setTotal_rides(String total_rides) {
            this.total_rides = total_rides;
        }

        public void setFilter_date(String filter_date) {
            this.filter_date = filter_date;
        }
    }

}
