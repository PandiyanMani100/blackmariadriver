package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class rideearnpojo implements Serializable {

    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }

    public class Response {
        @SerializedName("currency")
        private String currency;
        @SerializedName("available_amount")
        private String available_amount;
        @SerializedName("today_count")
        private String today_count;
        @SerializedName("week_count")
        private String week_count;
        @SerializedName("month_count")
        private String month_count;

        @SerializedName("today_amount")
        private String today_amount;

        @SerializedName("week_amount")
        private String week_amount;

        public String getToday_amount() {
            return today_amount;
        }

        public void setToday_amount(String today_amount) {
            this.today_amount = today_amount;
        }

        public String getWeek_amount() {
            return week_amount;
        }

        public void setWeek_amount(String week_amount) {
            this.week_amount = week_amount;
        }

        public String getMonth_amount() {
            return month_amount;
        }

        public void setMonth_amount(String month_amount) {
            this.month_amount = month_amount;
        }

        @SerializedName("month_amount")
        private String month_amount;

        public String getLast_withdrawal_date() {
            return last_withdrawal_date;
        }

        public void setLast_withdrawal_date(String last_withdrawal_date) {
            this.last_withdrawal_date = last_withdrawal_date;
        }

        @SerializedName("last_withdrawal_date")
        private String last_withdrawal_date;

        public Top_income_this_week getTop_income_this_weekobj() {
            return top_income_this_weekobj;
        }

        public void setTop_income_this_weekobj(Top_income_this_week top_income_this_weekobj) {
            this.top_income_this_weekobj = top_income_this_weekobj;
        }

        @SerializedName("top_income_this_week")
        Top_income_this_week top_income_this_weekobj;


        public ArrayList<Chart_array> getChart_array() {
            return chart_array;
        }

        public void setChart_array(ArrayList<Chart_array> chart_array) {
            this.chart_array = chart_array;
        }

        @SerializedName("chart_array")
        ArrayList<Chart_array> chart_array = new ArrayList<Chart_array>();
        @SerializedName("referal_code")
        private String referal_code;
        @SerializedName("month")
        private String month;
        @SerializedName("min_amount")
        private String min_amount;
        @SerializedName("max_amount")
        private String max_amount;
        @SerializedName("image")
        private String image;
        @SerializedName("withdraw_status")
        private String withdraw_status;
        @SerializedName("withdraw_error")
        private String withdraw_error;
        @SerializedName("proceed_status")
        private String proceed_status;
        @SerializedName("error_message")
        private String error_message;
        @SerializedName("download_url")
        private String download_url;


        public class Top_income_this_week {
            @SerializedName("driver_name")
            private String driver_name;
            @SerializedName("image")
            private String image;
            @SerializedName("location")
            private String location;


            // Getter Methods

            public String getDriver_name() {
                return driver_name;
            }

            public String getImage() {
                return image;
            }

            public String getLocation() {
                return location;
            }

            // Setter Methods

            public void setDriver_name(String driver_name) {
                this.driver_name = driver_name;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public void setLocation(String location) {
                this.location = location;
            }
        }

        public class Chart_array {
            @SerializedName("day")
            private String day;
            @SerializedName("ridecount")
            private String ridecount;
            @SerializedName("distance")
            private String distance;


            // Getter Methods

            public String getDay() {
                return day;
            }

            public String getRidecount() {
                return ridecount;
            }

            public String getDistance() {
                return distance;
            }

            // Setter Methods

            public void setDay(String day) {
                this.day = day;
            }

            public void setRidecount(String ridecount) {
                this.ridecount = ridecount;
            }

            public void setDistance(String distance) {
                this.distance = distance;
            }
        }
        // Getter Methods

        public String getCurrency() {
            return currency;
        }

        public String getAvailable_amount() {
            return available_amount;
        }

        public String getToday_count() {
            return today_count;
        }

        public String getWeek_count() {
            return week_count;
        }

        public String getMonth_count() {
            return month_count;
        }

        public String getReferal_code() {
            return referal_code;
        }

        public String getMonth() {
            return month;
        }

        public String getMin_amount() {
            return min_amount;
        }

        public String getMax_amount() {
            return max_amount;
        }

        public String getImage() {
            return image;
        }

        public String getWithdraw_status() {
            return withdraw_status;
        }

        public String getWithdraw_error() {
            return withdraw_error;
        }

        public String getProceed_status() {
            return proceed_status;
        }

        public String getError_message() {
            return error_message;
        }

        public String getDownload_url() {
            return download_url;
        }

        // Setter Methods

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public void setAvailable_amount(String available_amount) {
            this.available_amount = available_amount;
        }

        public void setToday_count(String today_count) {
            this.today_count = today_count;
        }

        public void setWeek_count(String week_count) {
            this.week_count = week_count;
        }

        public void setMonth_count(String month_count) {
            this.month_count = month_count;
        }

        public void setReferal_code(String referal_code) {
            this.referal_code = referal_code;
        }

        public void setMonth(String month) {
            this.month = month;
        }

        public void setMin_amount(String min_amount) {
            this.min_amount = min_amount;
        }

        public void setMax_amount(String max_amount) {
            this.max_amount = max_amount;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public void setWithdraw_status(String withdraw_status) {
            this.withdraw_status = withdraw_status;
        }

        public void setWithdraw_error(String withdraw_error) {
            this.withdraw_error = withdraw_error;
        }

        public void setProceed_status(String proceed_status) {
            this.proceed_status = proceed_status;
        }

        public void setError_message(String error_message) {
            this.error_message = error_message;
        }

        public void setDownload_url(String download_url) {
            this.download_url = download_url;
        }

    }
}

