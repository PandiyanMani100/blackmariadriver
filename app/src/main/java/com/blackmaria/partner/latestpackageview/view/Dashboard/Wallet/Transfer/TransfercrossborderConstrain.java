package com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Transfer;


import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityTransfercrossborderConstrainBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Wallet.Transfer.TransfercrossborderFactory;
import com.blackmaria.partner.latestpackageview.Model.crossborderpintenter;
import com.blackmaria.partner.latestpackageview.Model.crossborderratespojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Pin_activity;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet.transfer.TransfercrossborderViewModel;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

public class TransfercrossborderConstrain extends AppCompatActivity {

    private ActivityTransfercrossborderConstrainBinding binding;
    private TransfercrossborderViewModel transfercrossborderViewModel;
    private crossborderratespojo pojo;
    private SessionManager sessionManager;
    private String sDriverID = "";
    private AppUtils appUtils;

    @Override
    protected void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);}
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);}
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(TransfercrossborderConstrain.this, R.layout.activity_transfercrossborder_constrain);
        transfercrossborderViewModel = ViewModelProviders.of(this, new TransfercrossborderFactory(this)).get(TransfercrossborderViewModel.class);
        binding.setTransfercrossborderViewModel(transfercrossborderViewModel);
        transfercrossborderViewModel.setIds(binding);

        initView();
        clicklistener();
        setResponse();
    }

    private void setResponse() {
        transfercrossborderViewModel.getCrossvordertransferresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                        Intent intent = new Intent(TransfercrossborderConstrain.this, TransfersuccessConstrain.class);
                        intent.putExtra("json", jsonObject.toString());
                        startActivity(intent);
                        finish();
                        if (EventBus.getDefault().isRegistered(this)){
                            EventBus.getDefault().unregister(this);}
                    } else {
                        appUtils.AlertError(TransfercrossborderConstrain.this, getResources().getString(R.string.action_error), jsonObject.getString("response"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Subscribe
    public void pinentered(crossborderpintenter pojos) {
        if (pojos.isIspinter()) {
            HashMap<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("driver_id", sDriverID);
            jsonParams.put("transfer_amount", pojo.getSend_amount());
            jsonParams.put("friend_id", pojo.getFriend_id());
            jsonParams.put("cross_transfer", "1");
            transfercrossborderViewModel.transferamounttofriend(Iconstant.driver_wallet_to_wallet_transfer_url, jsonParams);
        }
    }

    private void clicklistener() {
        binding.sendnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ii = new Intent(TransfercrossborderConstrain.this, Pin_activity.class);
                ii.putExtra("mode", "crossborderamount");
                startActivity(ii);
            }
        });
    }

    private void initView() {
        sessionManager = SessionManager.getInstance(this);
        appUtils = AppUtils.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);

        if (getIntent().hasExtra("json")) {
            Type type = new TypeToken<crossborderratespojo>() {
            }.getType();
            pojo = new GsonBuilder().create().fromJson((getIntent().getStringExtra("json").toString()), type);
            setText(pojo);
        }
    }

    private void setText(crossborderratespojo pojo) {
        binding.senderfrom.setText("SEND FROM " + pojo.getSender_city());
        binding.senderamount.setText(pojo.getCurrency_transfer() + " " + pojo.getTransfer_amount());
        binding.receivedfrom.setText("RECEIVED IN " + pojo.getCity());
        binding.receivedamount.setText(pojo.getCurrency_received() + " " + pojo.getReceived_amount());
        AppUtils.setImageView(TransfercrossborderConstrain.this, pojo.getDriver_image(), binding.userimage);
        binding.usercontent.setText("TRANSFER TO : \n" + "NAME : " + pojo.getDriver_name() + "\n" + "MOBILE NO : " + pojo.getDail_code() + "" + pojo.getMobile_number() + "\n" + "LOCATION : " + pojo.getCity());
        binding.amountsender.setText("Amount tender " + "\n" + "Fees & Charges " + "\n" + "Send amount ");
        binding.amountsenderwithcurrency.setText(": " + pojo.getCurrency_transfer() + "  " + pojo.getTransfer_amount() + "\n" + ": " + pojo.getCurrency_transfer() + "  " + pojo.getTransfer_fee() + "\n" + ": " + pojo.getCurrency_transfer() + "  " + pojo.getSend_amount());
        binding.conversationratesvalue.setText("1 " + pojo.getCurrency_transfer() + "  =  " + pojo.getConversion_rate() + " " + pojo.getCurrency_received() + "\n" + pojo.getSend_amount() +" "+ pojo.getCurrency_transfer() + "  =  " + pojo.getReceived_amount() + " " + pojo.getCurrency_received());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);}
    }
}
