package com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityRechargeAmountConstrainBinding;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Model.rechargeamountpojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Rechargechoosepayment;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

public class RechargeamountViewModel extends ViewModel implements ApIServices.completelisner {

    private ActivityRechargeAmountConstrainBinding binding;
    private Activity context;
    private Dialog dialog;
    private AppUtils appUtils;
    private SessionManager sessionManager;
    private String sDriverID = "";
    private int count = 0;
    private JSONObject json;
    private rechargeamountpojo pojo;
    private MutableLiveData<String> getmoneypagedriverresponse = new MutableLiveData<>();

    public RechargeamountViewModel(Activity context) {
        this.context = context;
        sessionManager = SessionManager.getInstance(context);
        appUtils = AppUtils.getInstance(context);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);

    }

    public MutableLiveData<String> getGetmoneypagedriverresponse() {
        return getmoneypagedriverresponse;
    }

    public void getmoneypagedriver(String url, HashMap<String, String> jsonParams) {
        count = 0;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        ApIServices apIServices = new ApIServices(context, RechargeamountViewModel.this);
        apIServices.dooperation(url, jsonParams);
    }

    @Override
    public void sucessresponse(String val) {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        if (count == 0) {
            getmoneypagedriverresponse.postValue(val);
        }
    }

    @Override
    public void errorreponse() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void jsonexception() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    public void setIds(ActivityRechargeAmountConstrainBinding binding) {
        this.binding = binding;
    }

    public void back() {
        context.onBackPressed();
    }

    public void setpojo(JSONObject json) {
        this.json = json;
        Type type = new TypeToken<rechargeamountpojo>() {
        }.getType();
        pojo = new GsonBuilder().create().fromJson(json.toString(), type);
    }

    public void walletone() {
        Intent intent = new Intent(context, Rechargechoosepayment.class);
        intent.putExtra("insertAmount", pojo.getResponse().getRecharge_boundary().getWallet_amount_one());
        intent.putExtra("jsonpojo", json.toString());
        context.startActivity(intent);
    }

    public void wallettwo() {
        Intent intent = new Intent(context, Rechargechoosepayment.class);
        intent.putExtra("insertAmount", pojo.getResponse().getRecharge_boundary().getWallet_amount_two());
        intent.putExtra("jsonpojo", json.toString());
        context.startActivity(intent);
    }

    public void walletthree() {
        Intent intent = new Intent(context, Rechargechoosepayment.class);
        intent.putExtra("insertAmount", pojo.getResponse().getRecharge_boundary().getWallet_amount_three());
        intent.putExtra("jsonpojo", json.toString());
        context.startActivity(intent);
    }

    public void walletfour() {
        Intent intent = new Intent(context, Rechargechoosepayment.class);
        intent.putExtra("insertAmount", pojo.getResponse().getRecharge_boundary().getWallet_amount_four());
        intent.putExtra("jsonpojo", json.toString());
        context.startActivity(intent);
    }
}
