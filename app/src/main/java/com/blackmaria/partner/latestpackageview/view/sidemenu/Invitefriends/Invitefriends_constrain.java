package com.blackmaria.partner.latestpackageview.view.sidemenu.Invitefriends;

import android.app.Dialog;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import androidx.annotation.Nullable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityInvitefriendsConstrainBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.Factory.Sidemenu.Invitefriends.InvitefriendsFactory;
import com.blackmaria.partner.latestpackageview.vieewmodel.sidemenu.invitefriends.InvitefriendsViewModel;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class Invitefriends_constrain extends AppCompatActivity {
    private ActivityInvitefriendsConstrainBinding binding;
    private InvitefriendsViewModel invitefriendsViewModel;
    private SessionManager session;
    private ConnectionDetector cd;
    private String driveid = "";
    private boolean check_status = false;
    AccessLanguagefromlocaldb db;

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
        binding = DataBindingUtil.setContentView(Invitefriends_constrain.this, R.layout.activity_invitefriends_constrain);
        invitefriendsViewModel = ViewModelProviders.of(this, new InvitefriendsFactory(this)).get(InvitefriendsViewModel.class);
        binding.setInvitefriendsViewModel(invitefriendsViewModel);

        invitefriendsViewModel.setIds(binding);

        binding.text.setText(db.getvalue("myreferral"));
        binding.myreferalcode.setText(db.getvalue("referral_code"));
        binding.signupbonustxt.setText(db.getvalue("signupbonus"));
        binding.invitefriedstxt.setText(db.getvalue("invitefriedstxt"));

        initView();
    }

    private void initView() {
        session = SessionManager.getInstance(this);
        cd = ConnectionDetector.getInstance(this);

        HashMap<String, String> user = session.getUserDetails();
        driveid = user.get(SessionManager.KEY_DRIVERID);

        if (!session.getReferelStatus().equalsIgnoreCase("0")) {
            showReferralDetails();
        }

        invitefriendsViewModel.getinvitedriver(driveid);

        invitefriendsViewModel.getReferalprogram().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                String Sstatus = "", Smessage = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    Smessage = object.getString("response");
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                }
                if (Sstatus.equalsIgnoreCase("1")) {
                    session.setReferelStatus("0");
                    Alert("", Smessage);
                } else {
                    Alert("", Smessage);
                }
            }
        });

        binding.imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void showReferralDetails() {
        final Dialog dialog = new Dialog(Invitefriends_constrain.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.invite_and_earn_popip_new);
        final Button ok_button = (Button) dialog.findViewById(R.id.btn_ok);


        final TextView custom_text1 = (TextView) dialog.findViewById(R.id.custom_text1);
        custom_text1.setText(db.getvalue("ride_earn"));
        final TextView custom_text3 = (TextView) dialog.findViewById(R.id.custom_text3);
        custom_text3.setText(db.getvalue("cloud_custom_text5"));
        final TextView custom_text6 = (TextView) dialog.findViewById(R.id.custom_text6);
        custom_text6.setText(db.getvalue("cloud_custom_text6"));



        final CheckBox chck_agree = (CheckBox) dialog.findViewById(R.id.agree_checkBox);
        final Button cancelbtn = (Button) dialog.findViewById(R.id.cancel);
        cancelbtn.setText(db.getvalue("cancel_lable"));
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        cancelbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });
        chck_agree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    check_status = true;
                } else {
                    check_status = false;
                }
            }
        });
        ok_button.setText(db.getvalue("activate"));
        ok_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check_status) {
                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    jsonParams.put("driver_id", driveid);
                    jsonParams.put("confirmed", "yes");
                    invitefriendsViewModel.updatereferalprogram(Iconstant.Driver_Activiate_Referal, jsonParams);
                    dialog.dismiss();
                } else {
                    Alert(db.getvalue("sdd"), db.getvalue("agt"));
                }
            }
        });
        dialog.show();
    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(Invitefriends_constrain.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setDialogUpper(false);
        mDialog.setPositiveButton(db.getvalue("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();

    }
}
