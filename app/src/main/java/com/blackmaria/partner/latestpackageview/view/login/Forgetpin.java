package com.blackmaria.partner.latestpackageview.view.login;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.app.FetchingDataPage;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Countrycodepicker.countrycodepicker.CountryPicker;
import com.blackmaria.partner.latestpackageview.Countrycodepicker.countrycodepicker.CountryPickerListener;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class Forgetpin extends AppCompatActivity implements ApIServices.completelisner {

    private EditText edt_email;
    private TextView submit, dialcode;
    private ImageView booking_back_imgeview;
    private Dialog dialog;
    public CountryPicker picker;
    AccessLanguagefromlocaldb db;
    ProgressBar progressBar;
    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgetpin);
        db = new AccessLanguagefromlocaldb(this);
        session = new SessionManager(Forgetpin.this);
        init();
    }

    private void init() {
        progressBar = new ProgressBar(this);
        picker = CountryPicker.newInstance(db.getvalue("select_country_lable"));
        edt_email = findViewById(R.id.edt_email);
        edt_email.setHint(db.getvalue("entermobile"));
        dialcode = findViewById(R.id.dialcode);
        submit = findViewById(R.id.submit);
        submit.setText(db.getvalue("submit"));

        TextView pickup_lable = findViewById(R.id.pickup_lable);
        pickup_lable.setText(db.getvalue("forgetpin"));

        booking_back_imgeview = findViewById(R.id.booking_back_imgeview);
        booking_back_imgeview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (edt_email.getText().toString().length() == 0) {
                    erroredit(edt_email, db.getvalue("profile_lable_error_mobile"));
                } else {
                    checkemail(Iconstant.forgot_pin_url, edt_email.getText().toString());
                }
            }
        });

        dialcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });
        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {
                picker.dismiss();
                dialcode.setText(dialCode);
                InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(dialcode.getWindowToken(), 0);
            }
        });

    }


    private void erroredit(EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(Forgetpin.this, R.anim.shake);
        editname.startAnimation(shake);

        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }

    private void CloseKeyboardNew() {
        try {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow((null == getCurrentFocus()) ? null : getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //----------------------------------Mobile number change----------------------------------------------
    private void checkemail(String Url, String email) {
        CloseKeyboardNew();
        progressBar.setVisibility(View.VISIBLE);
        dialog = new Dialog(this, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("mobile_number", edt_email.getText().toString().trim());
        jsonParams.put("dail_code", dialcode.getText().toString().trim());

        ApIServices apIServices = new ApIServices(Forgetpin.this, Forgetpin.this);
        apIServices.dooperation(Url, jsonParams);
    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(Forgetpin.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(db.getvalue("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void Alertsucess(String title, String alert) {
        final PkDialog mDialog = new PkDialog(Forgetpin.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(db.getvalue("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                onBackPressed();
                finish();
            }
        });
        mDialog.show();
    }

    @Override
    public void sucessresponse(String response) {
        dialog.dismiss();
        progressBar.setVisibility(View.INVISIBLE);
        String Sstatus = "", Smessage = "";
        try {
            JSONObject object = new JSONObject(response);
            Sstatus = object.getString("status");

            if (Sstatus.equalsIgnoreCase("1")) {
                dialog.dismiss();
                String with_pincode = object.getString("pin_number");
                session.setSecurePin(with_pincode);
                Smessage = object.getString("message");
                Alertsucess(db.getvalue("action_success"), Smessage);
            } else {
                Smessage = object.getString("message");
                Alert(db.getvalue("action_error"), Smessage);
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            System.out.println("eee------------------" + e);
            e.printStackTrace();
            dialog.dismiss();
        }
    }

    @Override
    public void errorreponse() {
        dialog.dismiss();
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void jsonexception() {
        progressBar.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        AppUtils.toastprint(this, "Json Exception");
    }
}
