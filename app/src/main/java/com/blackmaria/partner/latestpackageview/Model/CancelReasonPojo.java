package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by user88 on 10/28/2015.
 */
public class CancelReasonPojo implements Serializable {

    public String getCancelreason_id() {
        return cancelreason_id;
    }

    public void setCancelreason_id(String cancelreason_id) {
        this.cancelreason_id = cancelreason_id;
    }

    public String getReason() {
        return reason;
    }
    public void setCancelstatus(String cancelreason_id) {
        this.setStatus = cancelreason_id;
    }

    public String getReasonstatus() {
        return setStatus;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @SerializedName("cancelreason_id")
    private String cancelreason_id;
    @SerializedName("reason")
    private String reason;
    @SerializedName("setStatus")
    private String setStatus;
}
