package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class invitefriends implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;


    // Getter Methods 

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods 

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }
    public class Response {
        @SerializedName("details")
        Details DetailsObject;


        // Getter Methods 

        public Details getDetails() {
            return DetailsObject;
        }

        // Setter Methods 

        public void setDetails(Details detailsObject) {
            this.DetailsObject = detailsObject;
        }

        public class Details {
            @SerializedName("friends_earn_amount")
            private String friends_earn_amount;
            @SerializedName("your_earn")
            private String your_earn;
            @SerializedName("ride_earn")
            private String ride_earn;
            @SerializedName("your_earn_condition")
            private String your_earn_condition;
            @SerializedName("your_earn_amount")
            private String your_earn_amount;
            @SerializedName("ref_welcome_amount")
            private String ref_welcome_amount;
            @SerializedName("referral_code")
            private String referral_code;
            @SerializedName("currency")
            private String currency;
            @SerializedName("share_image")
            private String share_image;
            @SerializedName("subject")
            private String subject;
            @SerializedName("message")
            private String message;
            @SerializedName("url")
            private String url;


            // Getter Methods 

            public String getFriends_earn_amount() {
                return friends_earn_amount;
            }

            public String getYour_earn() {
                return your_earn;
            }

            public String getRide_earn() {
                return ride_earn;
            }

            public String getYour_earn_condition() {
                return your_earn_condition;
            }

            public String getYour_earn_amount() {
                return your_earn_amount;
            }

            public String getRef_welcome_amount() {
                return ref_welcome_amount;
            }

            public String getReferral_code() {
                return referral_code;
            }

            public String getCurrency() {
                return currency;
            }

            public String getShare_image() {
                return share_image;
            }

            public String getSubject() {
                return subject;
            }

            public String getMessage() {
                return message;
            }

            public String getUrl() {
                return url;
            }

            // Setter Methods 

            public void setFriends_earn_amount(String friends_earn_amount) {
                this.friends_earn_amount = friends_earn_amount;
            }

            public void setYour_earn(String your_earn) {
                this.your_earn = your_earn;
            }

            public void setRide_earn(String ride_earn) {
                this.ride_earn = ride_earn;
            }

            public void setYour_earn_condition(String your_earn_condition) {
                this.your_earn_condition = your_earn_condition;
            }

            public void setYour_earn_amount(String your_earn_amount) {
                this.your_earn_amount = your_earn_amount;
            }

            public void setRef_welcome_amount(String ref_welcome_amount) {
                this.ref_welcome_amount = ref_welcome_amount;
            }

            public void setReferral_code(String referral_code) {
                this.referral_code = referral_code;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public void setShare_image(String share_image) {
                this.share_image = share_image;
            }

            public void setSubject(String subject) {
                this.subject = subject;
            }

            public void setMessage(String message) {
                this.message = message;
            }

            public void setUrl(String url) {
                this.url = url;
            }
        }
    }
    
    
}


