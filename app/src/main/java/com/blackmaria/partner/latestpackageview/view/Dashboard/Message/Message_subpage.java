package com.blackmaria.partner.latestpackageview.view.Dashboard.Message;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class Message_subpage extends AppCompatActivity implements ApIServices.completelisner {

    private ImageView img_back, image;
    private TextView txt_page_title, content;
    private String UserId = "";
    private ConnectionDetector cd;
    private SessionManager sessionManager;
    private Boolean isInternetPresent = false;
    private ProgressBar apiload;
    private Dialog dialog;
    private AppUtils appUtils;
    AccessLanguagefromlocaldb db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
        setContentView(R.layout.activity_notification_subpage);


        initView();
    }

    private void initView() {
        sessionManager = SessionManager.getInstance(Message_subpage.this);
        cd = ConnectionDetector.getInstance(Message_subpage.this);
        isInternetPresent = cd.isConnectingToInternet();
        appUtils = AppUtils.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        UserId = user.get(SessionManager.KEY_DRIVERID);
        img_back = findViewById(R.id.img_back);
        apiload = findViewById(R.id.apiload);
        image = findViewById(R.id.image);
        content = findViewById(R.id.content);
        txt_page_title = findViewById(R.id.txt_page_title);
        txt_page_title.setText(db.getvalue("message"));
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (getIntent().hasExtra("image")) {

            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.be_partner_banner)
                    .error(R.drawable.be_partner_banner)
                    .diskCacheStrategy(DiskCacheStrategy.ALL);
            Glide.with(Message_subpage.this).load(getIntent().getStringExtra("image")).apply(options).into(image);
            content.setText(getIntent().getStringExtra("message"));

            postData2(getIntent().getStringExtra("id"));
        }


    }


    private void postData2(final String notifiId) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("id", UserId);
        jsonParams.put("notification_id", notifiId);

        if (isInternetPresent) {
            postRequest_MessageUpdate(Iconstant.message_update_list, jsonParams);
        } else {
            Alert(db.getvalue("action_error"), db.getvalue("alert_nointernet"));
        }
    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(Message_subpage.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(db.getvalue("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    private void postRequest_MessageUpdate(String Url, HashMap<String, String> jsonParams) {
        apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(this, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ApIServices apIServices = new ApIServices(this, Message_subpage.this);
        apIServices.dooperation(Url, jsonParams);
    }

    @Override
    public void sucessresponse(String response) {
        apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        try {
            JSONObject object = new JSONObject(response);
            String Sstatus = object.getString("status");

            if (Sstatus.equalsIgnoreCase("1")) {
                String Sresponse = object.getString("response");
                appUtils.AlertSuccess(this, db.getvalue("action_success"), Sresponse);
            } else {
                String Sresponse = object.getString("response");
                appUtils.AlertError(this, db.getvalue("action_error"), Sresponse);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void errorreponse() {
        apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
    }

    @Override
    public void jsonexception() {
        apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        AppUtils.toastprint(this, "Json Exception");
    }
}
