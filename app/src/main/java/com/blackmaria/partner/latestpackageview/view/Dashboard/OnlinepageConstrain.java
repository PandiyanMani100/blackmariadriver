package com.blackmaria.partner.latestpackageview.view.Dashboard;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.KeyguardManager;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.Intent;

import androidx.databinding.DataBindingUtil;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.PowerManager;
import android.os.SystemClock;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Earnings.Earningsdashboardconstrain;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Fastpayhome;
import com.blackmaria.partner.latestpackageview.widgets.RoundedImageView;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityOnlinepageConstrainBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.OnlineupdatelocationtoserverService;
import com.blackmaria.partner.latestpackageview.Database.GEODBHelper;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.GoonlineFactory;
import com.blackmaria.partner.latestpackageview.Model.onlineupdatelocation;
import com.blackmaria.partner.latestpackageview.Model.riderequestpojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Tracking.TrackArrive;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.GoOnlineViewModel;
import com.blackmaria.partner.latestpackageview.Xmpp.MyXMPP;
import com.blackmaria.partner.latestpackageview.location.Service.LocationUpdatesService;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.blackmaria.partner.mylibrary.latlnginterpolation.LatLngInterpolator;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class OnlinepageConstrain extends AppCompatActivity implements View.OnClickListener, ComponentCallbacks2 {

    private ActivityOnlinepageConstrainBinding binding;
    private GoOnlineViewModel goOnlineViewModel;
    private GoogleMap googleMap;
    private MapFragment mapFragment;
    private LatLng newLatLng, oldLatLng;
    private LatLngInterpolator mLatLngInterpolator;
    private Location oldLocation;
    private Marker carMarker;
    private BitmapDescriptor carIcon;
    public MediaPlayer mediaPlayer = null;
    private AppUtils appUtils;
    private Handler onlineTimeHandler = new Handler();
    private Handler mapHandler = new Handler();
    private long startTime = 0L;
    private long upTime = 0L;
    private long timeInMilliseconds = 0L;
    private long timeSwapBuff = 0L;
    private long updatedTime = 0L;
    private int mins;
    private int secs;
    private int hours;
    private String currentlat = "", currentlon = "", sDriverID = "";
    private SessionManager sessionManager;
    private GEODBHelper myDBHelper;
    private Dialog riderequestdialog;
    public int requestcount = 0;
    public Location Str_mylocation;
    public float Str_bearingvalue;
    public LatLng Str_latlng;
    public String current_balance = "",driver_earning= "";
    AccessLanguagefromlocaldb db;

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
        binding = DataBindingUtil.setContentView(OnlinepageConstrain.this, R.layout.activity_onlinepage_constrain);
        goOnlineViewModel = ViewModelProviders.of(this, new GoonlineFactory(this)).get(GoOnlineViewModel.class);
        binding.setGoOnlineViewModel(goOnlineViewModel);

        goOnlineViewModel.setIds(binding);


        binding.performancestring.setText(db.getvalue("performance"));
        binding.gooflinestring.setText(db.getvalue("gooffline"));
        goOnlineViewModel.getOnlineresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    JSONObject jsonObject = json.getJSONObject("response");
                    if (json.getString("status").equalsIgnoreCase("1")) {
                        mapRunnable.run();
                        String sOnlineHours = jsonObject.getString("online_hours");
                        String performance = jsonObject.getString("performance");
                        String month_distance = jsonObject.getString("month_distance");

                        String currency_code = jsonObject.getString("currency");
                        String currenbancel = "";
                        if (jsonObject.has("current_balance ")){
                            currenbancel = jsonObject.getString("current_balance ");
                        } else if (jsonObject.has("current_balance")){
                            currenbancel = jsonObject.getString("current_balance");
                        }

                        String driverearning = jsonObject.getString("driver_earning");
                        current_balance = currency_code+" "+currenbancel;
                        driver_earning = currency_code+" "+driverearning;

                        if (jsonObject.getString("pending_ride_status").equalsIgnoreCase("1")) {
                            Intent i = new Intent(OnlinepageConstrain.this, TrackArrive.class);
                            i.putExtra("rideid", jsonObject.getString("pending_ride_id"));
                            startActivity(i);
                            finish();
                            if (EventBus.getDefault().isRegistered(this)) {
                                EventBus.getDefault().unregister(this);
                            }
                        } else {
                            binding.performancetxt.setText(performance + "%");
                            binding.enofmonthkilometer.setText(month_distance + "KM");
                            if (sOnlineHours != null && sOnlineHours.length() > 0) {
                                try {
                                    timeSwapBuff = Long.parseLong(sOnlineHours) * 1000;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                startTime = SystemClock.uptimeMillis();
                                onlineTimeHandler.post(updateTimerThread);
                            }
                        }
                    } else {
                        appUtils.AlertError(OnlinepageConstrain.this, db.getvalue("action_error"), json.getString("response"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        initview();
        initializeMap();
        goOnlineViewModel.onlineapihit();


    }

    private void getriderequestfromfirebase() {
        try {
            if (getIntent().hasExtra("notification")) {
                JSONObject notiobject = new JSONObject(getIntent().getStringExtra("EXTRA"));
                Type type = new TypeToken<riderequestpojo>() {
                }.getType();
                riderequestpojo object = new GsonBuilder().create().fromJson(notiobject.toString(), type);
                riderequest(object);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        startService(new Intent(this, LocationUpdatesService.class));
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopService(new Intent(this, LocationUpdatesService.class));

    }

    private void initview() {
//        EventBus.getDefault().register(this);
        carIcon = (BitmapDescriptorFactory.fromResource(R.drawable.carmove));
        appUtils = AppUtils.getInstance(this);
        binding.wallet.setOnClickListener(this);
        binding.gooflinestring.setOnClickListener(this);
        binding.gps.setOnClickListener(this);
        binding.earnings.setOnClickListener(this);
        sessionManager = SessionManager.getInstance(this);
        myDBHelper = GEODBHelper.getInstance(OnlinepageConstrain.this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        if (currentlat.equalsIgnoreCase("")) {
            currentlat = String.valueOf(sessionManager.getLatitude());
            currentlon = String.valueOf(sessionManager.getLongitude());
        }

        getriderequestfromfirebase();

        HashMap<String, String> domain = sessionManager.getXmpp();
        String ServiceName = domain.get(SessionManager.KEY_HOST_NAME);
        String HostAddress = domain.get(SessionManager.KEY_HOST_URL);

        HashMap<String, String> users = sessionManager.getUserDetails();
        String USERNAME = users.get(SessionManager.KEY_DRIVERID);
        String PASSWORD = users.get(SessionManager.KEY_SEC_KEY);

        if (sessionManager.getUserloggedIn()) {
            MyXMPP xmpp = MyXMPP.getInstance(this, ServiceName, HostAddress, USERNAME, PASSWORD);
            try {
                xmpp.connect("onCreate");
            } catch (IOException e) {
                e.printStackTrace();
            } catch (XMPPException e) {
                e.printStackTrace();
            } catch (SmackException e) {
                e.printStackTrace();
            }
        }
    }

    private void initializeMap() {
        if (googleMap == null) {
            mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.onlinemap);
            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap Map) {
                    googleMap = Map;
                    googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getApplicationContext(), R.raw.style));
                    googleMap.getUiSettings().setRotateGesturesEnabled(true);
                    googleMap.getUiSettings().setCompassEnabled(true);
                  /*  SmartLocation.with(OnlinepageConstrain.this).location()
                            .start(new OnLocationUpdatedListener() {
                                @Override
                                public void onLocationUpdated(Location location) {
                                    sessionManager.setLatitude(String.valueOf(location.getLatitude()));
                                    sessionManager.setLongitude(String.valueOf(location.getLongitude()));
                                    onMessageEvent(location);
                                }
                            });*/

                }
            });
        }
    }

    @Subscribe
    public void updatelocationtoserver(final onlineupdatelocation onlineupdatelocation) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                if (onlineupdatelocation.getStatus().equalsIgnoreCase("1")) {
                    if (onlineupdatelocation.getResponse().getWallet_constraint().equalsIgnoreCase("1")) {
                        if (onlineupdatelocation.getResponse().getAvailability().equalsIgnoreCase("Unavailable")) {
                            //continue trip
                            myDBHelper.insertRide_id(onlineupdatelocation.getResponse().getRide_id());
//                    if (onlineupdatelocation.getResponse().getIs_continue().equalsIgnoreCase("fare")) {
//                        Intent i  = new Intent(OnlinepageConstrain.this, TrackArrive.class);
//                        i.putExtra("rideid",onlineupdatelocation.getResponse().getRide_id());
//                        startActivity(i);
//                        finish();
//                    }
                        }
                    } else {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                Alertminwalletamount(db.getvalue("action_error"), onlineupdatelocation.getResponse().getWallet_message());
                            }
                        });
                    }
                }
            }
        };
        thread.start();

    }


    @Subscribe
    public void riderequest(final riderequestpojo riderequestpojo) {
        try {
            if (requestcount == 0) {
                requestcount = 1;
                runOnUiThread(new Thread(new Runnable() {
                    public void run() {
                        playringtone();
                        riderequestdialog = new Dialog(OnlinepageConstrain.this);
                        riderequestdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        riderequestdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                        riderequestdialog.setCancelable(true);
                        riderequestdialog.setCanceledOnTouchOutside(true);
                        riderequestdialog.setContentView(R.layout.requestviewconstrain);
                        DisplayMetrics metrics = getResources().getDisplayMetrics();
                        int screenWidth = (int) (metrics.widthPixels * 0.99);//fill only 85% of the screen
                        int screenHeight = (int) (metrics.heightPixels * 0.99);//fill only 85% of the screen
                        riderequestdialog.getWindow().setLayout(screenWidth, screenHeight);
                        final ImageView closeicon = riderequestdialog.findViewById(R.id.closeicon);
                        RoundedImageView userimage = riderequestdialog.findViewById(R.id.userimage);
                        TextView triptype = riderequestdialog.findViewById(R.id.triptype);
                        TextView passengercount = riderequestdialog.findViewById(R.id.passengercount);
                        final ProgressBar apiload = riderequestdialog.findViewById(R.id.apiload);
                        final TextView accept = riderequestdialog.findViewById(R.id.accept);
                        AppUtils.setImageView(OnlinepageConstrain.this, riderequestpojo.getKey8(), userimage);
                        if (riderequestpojo.getKey7().equalsIgnoreCase("1")) {
                            passengercount.setText(riderequestpojo.getKey7() + " PASSENGER");
                        } else {
                            passengercount.setText(riderequestpojo.getKey7() + " PASSENGERS");
                        }
                        triptype.setText(riderequestpojo.getKey6() + " TRIP");
                        final TextView timer = riderequestdialog.findViewById(R.id.timer);
                        long result = TimeUnit.SECONDS.toMillis(Long.parseLong(riderequestpojo.getKey2()));
                        final CountDownTimer yourCountDownTimer = new CountDownTimer(result, 1000) {                     //geriye sayma

                            public void onTick(long millisUntilFinished) {
                                timer.setText("" + millisUntilFinished / 1000);
                            }

                            public void onFinish() {
                                requestcount = 0;
                                HashMap<String, String> jsonParams = new HashMap<>();
                                jsonParams.put("driver_id", sDriverID);
                                jsonParams.put("ride_id", riderequestpojo.getKey1());
                                jsonParams.put("driver_lat", currentlat);
                                jsonParams.put("driver_lon", currentlon);
                                Intent intents = new Intent(OnlinepageConstrain.this, OnlineupdatelocationtoserverService.class);
                                intents.putExtra("params", jsonParams);
                                intents.putExtra("url", Iconstant.driverDeclineRequest_Url);
                                startService(intents);
                                if (riderequestdialog != null && riderequestdialog.isShowing()) {
                                    riderequestdialog.dismiss();
                                }
                                if (mediaPlayer != null) {
                                    if (mediaPlayer.isPlaying()) {
                                        mediaPlayer.stop();
                                    }
                                }
                            }
                        }.start();

                        closeicon.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                requestcount = 0;
                                HashMap<String, String> jsonParams = new HashMap<>();
                                jsonParams.put("driver_id", sDriverID);
                                jsonParams.put("ride_id", riderequestpojo.getKey1());
                                jsonParams.put("driver_lat", currentlat);
                                jsonParams.put("driver_lon", currentlon);
                                Intent intents = new Intent(OnlinepageConstrain.this, OnlineupdatelocationtoserverService.class);
                                intents.putExtra("params", jsonParams);
                                intents.putExtra("url", Iconstant.driverDeclineRequest_Url);
                                startService(intents);
                                yourCountDownTimer.cancel();
                                if (mediaPlayer != null) {
                                    if (mediaPlayer.isPlaying()) {
                                        mediaPlayer.stop();
                                    }
                                }
                                riderequestdialog.dismiss();
                            }
                        });
                        accept.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                requestcount = 0;
                                apiload.setVisibility(View.VISIBLE);
                                accept.setClickable(false);
                                closeicon.setClickable(false);
                                goOnlineViewModel.rideaceptapihit(riderequestpojo.getKey1(), currentlat, currentlon);
                                goOnlineViewModel.getAcceptride().observe(OnlinepageConstrain.this, new Observer<String>() {
                                    @Override
                                    public void onChanged(@Nullable String response) {
                                        try {
                                            yourCountDownTimer.cancel();
                                            apiload.setVisibility(View.GONE);
                                            accept.setClickable(true);
                                            closeicon.setClickable(true);
                                            JSONObject jsonObject = new JSONObject(response);
                                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                                if (mediaPlayer != null) {
                                                    if (mediaPlayer.isPlaying()) {
                                                        mediaPlayer.stop();
                                                    }
                                                }
                                                riderequestdialog.dismiss();
                                                sessionManager.setisride(riderequestpojo.getKey1());
                                                Intent i = new Intent(OnlinepageConstrain.this, TrackArrive.class);
                                                i.putExtra("rideid", riderequestpojo.getKey1());
                                                startActivity(i);
                                                finish();
                                                if (EventBus.getDefault().isRegistered(this)) {
                                                    EventBus.getDefault().unregister(this);
                                                }
                                            } else {
                                                if (mediaPlayer != null) {
                                                    if (mediaPlayer.isPlaying()) {
                                                        mediaPlayer.stop();
                                                    }
                                                }
                                                riderequestdialog.dismiss();
                                                appUtils.AlertError(OnlinepageConstrain.this, db.getvalue("action_error"), jsonObject.getString("response"));
                                            }

                                        } catch (JSONException e) {
                                            if (mediaPlayer != null) {
                                                if (mediaPlayer.isPlaying()) {
                                                    mediaPlayer.stop();
                                                }
                                            }
                                            e.printStackTrace();
                                        }
                                    }
                                });

                            }
                        });
                        riderequestdialog.show();
                    }
                }));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }

    private void playringtone() {
        //screen locak on
        @SuppressLint("InvalidWakeLockTag") PowerManager.WakeLock screenLock = ((PowerManager) getSystemService(POWER_SERVICE)).newWakeLock(
                PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "TAG");
        screenLock.acquire();

        KeyguardManager keyguardManager = (KeyguardManager) getSystemService(Activity.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock lock = keyguardManager.newKeyguardLock(KEYGUARD_SERVICE);
        //later
        screenLock.release();
        lock.disableKeyguard();

        int MAX_VOLUME = 100;
        final float volume = (float) (1 - (Math.log(MAX_VOLUME - 80) / Math.log(MAX_VOLUME)));
        if(mediaPlayer == null)
        {
            mediaPlayer = MediaPlayer.create(this, R.raw.blacktone);
            AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 20, 0);

            try {
                mediaPlayer.setVolume(volume, volume);
                if (mediaPlayer != null) {
                    if (!mediaPlayer.isPlaying()) {
                        mediaPlayer.start();
                        mediaPlayer.setLooping(true);
                    }/*else{t
                    if (mediaPlayer.isPlaying()) {
                        mediaPlayer.stop();
                        mediaPlayer.start();
                        mediaPlayer.setLooping(true);
                    }
                }*/
                }
            } catch (NullPointerException e) {

            } catch (Exception e) {

            }
        }



    }

    public void onMessageEvent(final Location myLocation) {
        try {
            sessionManager.setLatitude(String.valueOf(myLocation.getLatitude()));
            sessionManager.setLongitude(String.valueOf(myLocation.getLongitude()));
            if (currentlat.equalsIgnoreCase("")) {
                currentlat = String.valueOf(sessionManager.getLatitude());
                currentlon = String.valueOf(sessionManager.getLongitude());
            }
            final LatLng latLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
            if (oldLatLng == null) {
                oldLatLng = latLng;
            }
            newLatLng = latLng;
            if (mLatLngInterpolator == null) {
                mLatLngInterpolator = new LatLngInterpolator.Linear();
            }
            oldLocation = new Location("");
            oldLocation.setLatitude(oldLatLng.latitude);
            oldLocation.setLongitude(oldLatLng.longitude);
            final float bearingValue = oldLocation.bearingTo(myLocation);
            Str_mylocation = myLocation;
            Str_bearingvalue = bearingValue;
            Str_latlng = latLng;
            runOnUiThread(new Runnable() {
                public void run() {
                    if (googleMap != null) {
                        if (carMarker != null) {
                            appUtils.movemarker(myLocation, bearingValue, carMarker, googleMap, latLng, mLatLngInterpolator);
                        } else {
                            carMarker = appUtils.Addmarker(googleMap, latLng, myLocation, carIcon);
                        }
                    }
                }
            });

            oldLatLng = newLatLng;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Alertminwalletamount(String string, String wallet_message) {
        final PkDialog mDialog = new PkDialog(OnlinepageConstrain.this);
        mDialog.setDialogTitle(string);
        mDialog.setDialogMessage(wallet_message);
        mDialog.setPositiveButton(db.getvalue("ok_lable"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                goOnlineViewModel.updateavailablity();
                goOnlineViewModel.getUpdateavailability().observe(OnlinepageConstrain.this, new Observer<String>() {
                    @Override
                    public void onChanged(@Nullable String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                startActivity(new Intent(OnlinepageConstrain.this, Dashboard_constrain.class));
                                if (EventBus.getDefault().isRegistered(this)) {
                                    EventBus.getDefault().unregister(this);
                                }
                            } else {
                                appUtils.AlertError(OnlinepageConstrain.this, db.getvalue("action_error"), jsonObject.getString("response"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        mDialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (riderequestdialog != null) {
            riderequestdialog.dismiss();
        }
        if (onlineTimeHandler != null) {
            onlineTimeHandler.removeCallbacks(updateTimerThread);
        }
        if (mapRunnable != null) {
            mapHandler.removeCallbacks(mapRunnable);
        }
        if (mediaPlayer != null) {
            try {
                mediaPlayer.reset();
                mediaPlayer.prepare();
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer=null;
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }


//        SmartLocation.with(this).location().stop();
        stopService(new Intent(OnlinepageConstrain.this, OnlineupdatelocationtoserverService.class));
    }

    private Runnable updateTimerThread = new Runnable() {

        public void run() {
            timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
            updatedTime = timeSwapBuff + timeInMilliseconds;
            secs = (int) (updatedTime / 1000);
            hours = secs / (60 * 60);
            mins = secs / 60;
            secs = secs % 60;
            if (mins >= 60) {
                if (mins == 60) {
                    hours = 1;
                    mins = 0;
                } else if (mins > 60) {
                    hours = (int) mins / 60;
                    mins = mins % 60;
                }
            }
            String time = String.format("%02d", hours) + ":" + String.format("%02d", mins) + ":"
                    + String.format("%02d", secs);
            binding.txttimer.setText(time);
            onlineTimeHandler.postDelayed(this, 1 * 1000);
        }
    };


    public void openwallet() {
        Intent intent = new Intent(OnlinepageConstrain.this, Fastpayhome.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    public void goofline() {
        final PkDialog mDialog = new PkDialog(OnlinepageConstrain.this);
        mDialog.setDialogTitle("Confirmation");
        mDialog.setDialogMessage(db.getvalue("label_sure_to_go_offline"));
        mDialog.setPositiveButton(db.getvalue("action_yes"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                goOnlineViewModel.updateavailablity();
                goOnlineViewModel.getUpdateavailability().observe(OnlinepageConstrain.this, new Observer<String>() {
                    @Override
                    public void onChanged(@Nullable String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                startActivity(new Intent(OnlinepageConstrain.this, Dashboard_constrain.class));
                                finish();
                                if (EventBus.getDefault().isRegistered(this)) {
                                    EventBus.getDefault().unregister(this);
                                }
                            } else {
                                appUtils.AlertError(OnlinepageConstrain.this, db.getvalue("action_error"), jsonObject.getString("response"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

            }
        });
        mDialog.setNegativeButton(db.getvalue("action_no"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.gooflinestring:
                goofline();
                break;
            case R.id.wallet:
//                openwallet();
                showEarning_Wallet(db.getvalue("ewallet"),db.getvalue("ewalletbalance")+"\n"+current_balance);
                break;
            case R.id.earnings:
//                openearnings();
                showEarning_Wallet(db.getvalue("Todayearning"),driver_earning);
                break;
            case R.id.gps:
                currentlocation();
                break;
        }
    }

    private void showEarning_Wallet(String title, String message) {
        final Dialog dialog = new Dialog(OnlinepageConstrain.this);
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 85% of the screen
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_could_not_drive);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setLayout(screenWidth, LinearLayout.LayoutParams.WRAP_CONTENT);
        TextView Tv_title = (TextView) dialog.findViewById(R.id.custom_dialog_library_title_textview);
        TextView Tv_message = (TextView) dialog.findViewById(R.id.custom_dialog_library_message_textview);
        Tv_title.setText(title);
        Tv_message.setText(message);

        TextView okButton = dialog.findViewById(R.id.custom_dialog_library_ok_button);
        okButton.setText(db.getvalue("close_lable"));
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });

        dialog.show();
    }
    public void openearnings() {
        Intent intent = new Intent(getApplicationContext(), Earningsdashboardconstrain.class);
        startActivity(intent);
        overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    public void currentlocation(){
        if (googleMap != null) {
            if (carMarker != null) {
                appUtils.movemarker(Str_mylocation, Str_bearingvalue, carMarker, googleMap, Str_latlng, mLatLngInterpolator);
                appUtils.animateCamera(googleMap, Str_latlng,15);
            } else {
                carMarker = appUtils.Addmarker(googleMap, Str_latlng, Str_mylocation, carIcon);
            }
        }
    }

    //Update location
    private Runnable mapRunnable = new Runnable() {
        @Override
        public void run() {
            Thread thread = new Thread() {
                @Override
                public void run() {
                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    jsonParams.put("driver_id", sDriverID);
                    jsonParams.put("latitude", currentlat);
                    jsonParams.put("longitude", currentlon);
                    Intent intents = new Intent(OnlinepageConstrain.this, OnlineupdatelocationtoserverService.class);
                    intents.putExtra("params", jsonParams);
                    intents.putExtra("url", Iconstant.updateDriverLocation);
                    startService(intents);
                }
            };
            thread.start();
            mapHandler.postDelayed(this, 45000);
        }
    };

    @Override
    public void onBackPressed() {
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Log.v("TRIMMEMORY", " " + level);
    }


    @Subscribe
    public void gettinglcoationfromonlinepage(final Location location) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                sessionManager.setLatitude(String.valueOf(location.getLatitude()));
                sessionManager.setLongitude(String.valueOf(location.getLongitude()));
                onMessageEvent(location);
            }
        };
        thread.start();


    }
}
