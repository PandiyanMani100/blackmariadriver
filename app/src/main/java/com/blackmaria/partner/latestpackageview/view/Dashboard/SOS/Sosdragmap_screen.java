package com.blackmaria.partner.latestpackageview.view.Dashboard.SOS;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.android.volley.Request;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.GPSTracker;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.adapter.PlaceSearchAdapter;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.blackmaria.partner.latestpackageview.widgets.PkDialogtryagain;
import com.blackmaria.partner.latestpackageview.widgets.RoundedImageView;
import com.blackmaria.partner.mylibrary.volley.ServiceRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import fr.castorflex.android.smoothprogressbar.SmoothProgressBar;

import static android.view.View.GONE;

public class Sosdragmap_screen extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private SessionManager session;
    private String sUserId = "";
    private TextView book_my_ride_set_destination_textview1,username;
    private CardView police, ambulance, fire;
    private String SselectedAddress = "", Slatitude = "", Slongitude = "", Stitle = "", SlocationKey = "", SidentityKey = "";
    private ServiceRequest mRequest, editRequest;
    private GoogleMap googleMap;
    GPSTracker gps;
    static final int REQUEST_CODE_RECOVER_PLAY_SERVICES = 1001;
    private String sPage = "";
    String strAdd = "";
    String address = "";
    private boolean isAddressAvailable = false;

    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    PendingResult<LocationSettingsResult> result;
    final static int REQUEST_LOCATION = 299;

    private String sLatitude = "";
    private String sLongitude = "";
    MapFragment mapFragment;
    public static final int FavoriteRequestCode = 6666;
    private TextView book_my_ride_set_destination_textview;
    private boolean dropclick = true;
    private SmoothProgressBar loading_spinner;
    private ListView listview;
    ArrayList<String> itemList_location = new ArrayList<String>();
    ArrayList<String> itemList_placeId = new ArrayList<String>();
    private boolean isdataAvailable = false;
    private com.blackmaria.partner.adapter.PlaceSearchAdapter PlaceSearchAdapter;
    private String Sselected_location = "", placeId = "";
    private String Slocation = "";
    private ImageView img_back ;
    private RoundedImageView userimage;
    private Dialog dialog;
    CardView currentlocation;
    AccessLanguagefromlocaldb db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sosdragmap_screen_new);
        db = new AccessLanguagefromlocaldb(this);
        initView();
        initializeMap();

    }

    private void initView() {
        session = new SessionManager(this);
        HashMap<String, String> user = session.getUserDetails();
        sUserId = user.get(SessionManager.KEY_DRIVERID);
        String UserName = user.get(SessionManager.KEY_DRIVER_NAME);

        book_my_ride_set_destination_textview1 = findViewById(R.id.book_my_ride_set_destination_textview1);
        username = findViewById(R.id.username);
        img_back = findViewById(R.id.img_back);
        userimage = findViewById(R.id.userimage);
        currentlocation= findViewById(R.id.currentlocation);
        String userImage = user.get(SessionManager.KEY_DRIVER_IMAGE);
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.new_no_user_img)
                .error(R.drawable.new_no_user_img)
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        Glide.with(this).load(userImage).apply(options).into(userimage);


        username.setText(UserName);
        police = findViewById(R.id.police);
        ambulance = findViewById(R.id.ambulance);
        fire = findViewById(R.id.fire);

        TextView areyoursuretext = findViewById(R.id.areyoursuretext);
        areyoursuretext.setText(db.getvalue("are_you_sure_in_emergency_mode"));

        TextView tapforassitence = findViewById(R.id.tapforassitence);
        tapforassitence.setText(db.getvalue("tap_for_assistance"));
        TextView polic = findViewById(R.id.polic);
        polic.setText(db.getvalue("police"));
        TextView amb = findViewById(R.id.amb);
        amb.setText(db.getvalue("ambulance"));
        TextView firi = findViewById(R.id.firi);
        firi.setText(db.getvalue("fire_brigade"));
        TextView dragdrop_text = findViewById(R.id.dragdrop_text);
        dragdrop_text.setText(db.getvalue("drag_pin_to_exact_location"));



        cd = new ConnectionDetector(this);
        isInternetPresent = cd.isConnectingToInternet();
        gps = new GPSTracker(this);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        police.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SOS("police");
//                Popupalert("police");
            }
        });

        ambulance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SOS("medic");
//                Popupalert("medic");
            }
        });

        fire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SOS("fire");
//                Popupalert("fire");
            }
        });

        currentlocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (gps.canGetLocation() && gps.isgpsenabled()) {
                    double Dlatitude = gps.getLatitude();
                    double Dlongitude = gps.getLongitude();

                    // Move the camera to last position with a zoom level
                    CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Dlatitude, Dlongitude)).zoom(17).build();
                    googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                } else {
                    enableGpsService();
                }
            }
        });
    }

    private void SOS(final String serivce) {
        System.out.println("----------------muruga trackingPage sos-----------------");
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.sos_popup1);

        TextView emeerrtitle = (TextView) dialog.findViewById(R.id.emeerrtitle);
        emeerrtitle.setText(db.getvalue("trip_emergency"));

        TextView lemergency_textview = (TextView) dialog.findViewById(R.id.lemergency_textview);
        lemergency_textview.setText(db.getvalue("are_you__emergency_new"));

        ImageView bkng_closeImge = (ImageView) dialog.findViewById(R.id.label_close_imageview);
        bkng_closeImge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        TextView cnformTv = (TextView) dialog.findViewById(R.id.txt_confirm);
        cnformTv.setText(db.getvalue("send"));
        cnformTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (isInternetPresent) {
                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    jsonParams.put("driver_id", sUserId);
                    jsonParams.put("latitude", Slatitude);
                    jsonParams.put("longitude", Slongitude);
                    postRequest_Sos(Iconstant.sos_alert_url, jsonParams,serivce);
                } else {
                    Alert(db.getvalue("alert_label_title"), db.getvalue("alert_nointernet"));
                }
            }
        });
        dialog.show();
    }

    //--------------------Post Request for Sos alert-------------------------------------

    private void postRequest_Sos(String Url, HashMap<String, String> jsonParams, final String serivce) {
        dialog = new Dialog(this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(db.getvalue("action_pleasewait"));
        System.out.println("-------------Sos alert Url----------------" + Url);
        System.out.println("-------------Sos alert jsonParams----------------" + jsonParams);
        mRequest = new ServiceRequest(this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                System.out.println("-------------Sos alert Response----------------" + response);
                String Sstatus = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    String Sresponse = object.getString("response");
                    if ("1".equalsIgnoreCase(Sstatus)) {
                        dialog.dismiss();
                        SOSAlert(db.getvalue("info_lable"),Sresponse,serivce);
                    } else {
                        dialog.dismiss();
                        Toast.makeText(getApplicationContext(), Sresponse, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                dialog.dismiss();
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    private void initializeMap() {
        if (googleMap == null) {
            mapFragment = ((MapFragment) getFragmentManager().findFragmentById(R.id.favorite_add_mapview));
            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap arg) {
                    loadMap(arg);
                }
            });
        }

    }

    public void loadMap(GoogleMap arg) {
        googleMap = arg;
        if (CheckPlayService()) {
            // Changing map type
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            // Showing / hiding your current location
            googleMap.setMyLocationEnabled(false);
            googleMap.getUiSettings().setMyLocationButtonEnabled(false);
            // Enable / Disable zooming controls
            googleMap.getUiSettings().setZoomControlsEnabled(false);
            // Enable / Disable my location button
            // Enable / Disable Compass icon
            googleMap.getUiSettings().setCompassEnabled(false);
            // Enable / Disable Rotate gesture
            googleMap.getUiSettings().setRotateGesturesEnabled(true);
            // Enable / Disable zooming functionality
            googleMap.getUiSettings().setZoomGesturesEnabled(true);

            if (gps.canGetLocation() && gps.isgpsenabled()) {
                double Dlatitude = gps.getLatitude();
                double Dlongitude = gps.getLongitude();

                // Move the camera to last position with a zoom level
                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Dlatitude, Dlongitude)).zoom(17).build();
                googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            } else {
                enableGpsService();
            }


            //New Chsnges
            GoogleMap.OnCameraChangeListener mOnCameraChangeListener = new GoogleMap.OnCameraChangeListener() {
                @Override
                public void onCameraChange(CameraPosition cameraPosition) {
                    double latitude = cameraPosition.target.latitude;
                    double longitude = cameraPosition.target.longitude;

                    cd = new ConnectionDetector(Sosdragmap_screen.this);
                    isInternetPresent = cd.isConnectingToInternet();


                    if (latitude != 0.0) {
                        googleMap.clear();

                        Slatitude = String.valueOf(latitude);
                        Slongitude = String.valueOf(longitude);

                        if (isInternetPresent) {

                            if (Slatitude != null && Slongitude != null) {
                                if (!placeId.equalsIgnoreCase("")) {
                                    LatLongRequest(Iconstant.GetAddressFrom_LatLong_url + placeId, "2");
                                } else {
                                    GetCompleteAddressAsyncTask asyncTask = new GetCompleteAddressAsyncTask();
                                    asyncTask.execute();
                                }
                            } else {
                            }

                        } else {
                        }
                    }
                }
            };
            if (CheckPlayService()) {
                if (googleMap != null) {
                    googleMap.setOnCameraChangeListener(mOnCameraChangeListener);
                    googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                        @Override
                        public boolean onMarkerClick(Marker marker) {
                            String tittle = marker.getTitle();
                            return true;
                        }
                    });
                }
            } else {
                Alert(db.getvalue("alert_label_title"), "Install Google Play service To View Location !!!");
            }


        } else {

            final PkDialog mDialog = new PkDialog(Sosdragmap_screen.this);
            mDialog.setDialogTitle(db.getvalue("alert_label_title"));
            mDialog.setDialogMessage(db.getvalue("action_unable_to_create_map"));
            mDialog.setPositiveButton(db.getvalue("action_ok"), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                    finish();
                }
            });
            mDialog.show();
        }
    }

    public class GetCompleteAddressAsyncTask extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
//            book_my_ride_set_destination_textview1.setVisibility(GONE);
            book_my_ride_set_destination_textview1.setText(db.getvalue("favorite_add_label_gettingAddress"));
        }

        @Override
        protected String doInBackground(Void... params) {

            try {
                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(Sosdragmap_screen.this, Locale.getDefault());

                addresses = geocoder.getFromLocation(Double.parseDouble(Slatitude), Double.parseDouble(Slongitude), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                try {
                    String addresss = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();
                    String country = addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getPostalCode();
                    String knownName = addresses.get(0).getFeatureName();
                    strAdd = addresss;
                    address = addresss;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (IOException ex) {
                ex.printStackTrace();

                isAddressAvailable = false;
            }

            if (!strAdd.equalsIgnoreCase("")) {
                isAddressAvailable = true;
            }

            return address;
        }

        @Override
        protected void onPostExecute(String address) {
            if (isAddressAvailable) {
                book_my_ride_set_destination_textview1.setVisibility(View.VISIBLE);
                book_my_ride_set_destination_textview1.setText(strAdd);
                Slocation = address;
            } else {
                book_my_ride_set_destination_textview1.setVisibility(View.VISIBLE);
                book_my_ride_set_destination_textview1.setText("");

            }
        }
    }


    private void CloseKeyboard(EditText edittext) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(edittext.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }


    //-----------Check Google Play Service--------
    private boolean CheckPlayService() {
        final int connectionStatusCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(Sosdragmap_screen.this);
        if (GooglePlayServicesUtil.isUserRecoverableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
            return false;
        }
        return true;
    }

    void showGooglePlayServicesAvailabilityErrorDialog(final int connectionStatusCode) {
        runOnUiThread(new Runnable() {
            public void run() {
                final Dialog dialog = GooglePlayServicesUtil.getErrorDialog(
                        connectionStatusCode, Sosdragmap_screen.this, REQUEST_CODE_RECOVER_PLAY_SERVICES);
                if (dialog == null) {
                    Toast.makeText(Sosdragmap_screen.this, "incompatible version of Google Play Services", Toast.LENGTH_LONG).show();
                }
            }
        });
    }


    //Enabling Gps Service
    private void enableGpsService() {

        mGoogleApiClient = new GoogleApiClient.Builder(Sosdragmap_screen.this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        mGoogleApiClient.connect();

        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);

        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        //...
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(Sosdragmap_screen.this, REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    //-----------------Move Back on pressed phone back button------------------
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0)) {


            onBackPressed();
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
            return true;
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (resultCode == RESULT_OK) {
            if (requestCode == FavoriteRequestCode) {
                String sAddress = data.getStringExtra("Selected_Location");
                sLatitude = data.getStringExtra("Selected_Latitude");
                sLongitude = data.getStringExtra("Selected_Longitude");
                book_my_ride_set_destination_textview1.setText(sAddress);
                // Move the camera to last position with a zoom level
                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Double.parseDouble(sLatitude), Double.parseDouble(sLongitude))).zoom(17).build();
                googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            }
        }
    }


    private void LatLongRequest(String Url, final String flag) {

        final Dialog dialog = new Dialog(Sosdragmap_screen.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(db.getvalue("action_processing"));

        System.out.println("--------------LatLong url-------------------" + Url);

        ServiceRequest mRequest = new ServiceRequest(Sosdragmap_screen.this);
        mRequest.makeServiceRequest(Url, Request.Method.GET, null, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {


                System.out.println("--------------LatLong  reponse-------------------" + response);
                String status = "", SLatitude1 = "", SLongitude1 = "";

                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");
                        JSONObject place_object = object.getJSONObject("result");
                        if (status.equalsIgnoreCase("OK")) {
                            if (place_object.length() > 0) {
                                JSONObject geometry_object = place_object.getJSONObject("geometry");
                                if (geometry_object.length() > 0) {
                                    JSONObject location_object = geometry_object.getJSONObject("location");
                                    if (location_object.length() > 0) {
                                        SLatitude1 = location_object.getString("lat");
                                        SLongitude1 = location_object.getString("lng");
                                        isdataAvailable = true;
                                    } else {
                                        isdataAvailable = false;
                                    }
                                } else {
                                    isdataAvailable = false;
                                }
                            } else {
                                isdataAvailable = false;
                            }
                        } else {
                            isdataAvailable = false;
                        }
                    }

                    if (isdataAvailable) {


                        Slocation = Sselected_location;
                        Slatitude = SLatitude1;
                        Slongitude = SLongitude1;


                        System.out.println("=========set value1=================");
                        book_my_ride_set_destination_textview1.setText(Slocation);
                        book_my_ride_set_destination_textview1.setText("");
                        book_my_ride_set_destination_textview1.setVisibility(View.VISIBLE);
//                        book_my_ride_set_destination_textview1.setVisibility(View.GONE);
                        if (flag.equalsIgnoreCase("1")) {
                            CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(Double.parseDouble(Slatitude), Double.parseDouble(Slongitude))).zoom(17).build();
                            googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                            listview.setVisibility(GONE);
                        } else if (flag.equalsIgnoreCase("2")) {
                            placeId = "";
                        }


                        dialog.dismiss();
                    } else {
                        dialog.dismiss();
                        Alert(/*getResources().getString(R.string.ale)*/"sorry", status);
                    }


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    //-------------------Search Place Request----------------
    private void CitySearchRequest(String Url) {
        listview.setVisibility(View.VISIBLE);
        loading_spinner.setVisibility(View.VISIBLE);
        System.out.println("--------------Search city url-------------------" + Url);

        mRequest = new ServiceRequest(Sosdragmap_screen.this);
        mRequest.makeServiceRequest(Url, Request.Method.GET, null, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------Search city  reponse-------------------" + response);
                String status = "";
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.length() > 0) {

                        status = object.getString("status");
                        JSONArray place_array = object.getJSONArray("predictions");
                        if (status.equalsIgnoreCase("OK")) {
                            if (place_array.length() > 0) {
                                itemList_location.clear();
                                itemList_placeId.clear();
                                for (int i = 0; i < place_array.length(); i++) {
                                    JSONObject place_object = place_array.getJSONObject(i);
                                    itemList_location.add(place_object.getString("description"));
                                    itemList_placeId.add(place_object.getString("place_id"));
                                }
                                isdataAvailable = true;
                            } else {
                                itemList_location.clear();
                                itemList_placeId.clear();
                                isdataAvailable = false;
                            }
                        } else {
                            itemList_location.clear();
                            itemList_placeId.clear();
                            isdataAvailable = false;
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                loading_spinner.setVisibility(View.INVISIBLE);

                PlaceSearchAdapter = new PlaceSearchAdapter(Sosdragmap_screen.this, itemList_location);
                listview.setAdapter(PlaceSearchAdapter);
                PlaceSearchAdapter.notifyDataSetChanged();
                listview.setVisibility(GONE);

            }

            @Override
            public void onErrorListener() {
                loading_spinner.setVisibility(View.INVISIBLE);

            }
        });

    }

    private void Alert(String title, String alert) {

        final PkDialogtryagain mDialog = new PkDialogtryagain(Sosdragmap_screen.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setCloseButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        mDialog.setPositiveButton(db.getvalue("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void SOSAlert(String title, String alert, final String serivce) {

        final PkDialogtryagain mDialog = new PkDialogtryagain(Sosdragmap_screen.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setCloseButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        mDialog.setPositiveButton(db.getvalue("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                if (serivce.equalsIgnoreCase("police")){
                    Popupalert("police");
                } else if (serivce.equalsIgnoreCase("medic")){
                    Popupalert("medic");
                } else if (serivce.equalsIgnoreCase("fire")){
                    Popupalert("fire");
                }
            }
        });
        mDialog.show();
    }


    private void Popupalert(final String service) {
        final Dialog dialog = new Dialog(Sosdragmap_screen.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.sos_singledialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final ImageView closeImage = dialog.findViewById(R.id.close);
        ImageView imageservice = dialog.findViewById(R.id.imageservice);
        TextView title = dialog.findViewById(R.id.title);
        TextView yes = dialog.findViewById(R.id.yes);
        TextView connecttoservice = dialog.findViewById(R.id.connecttoservice);
        TextView servicetext = dialog.findViewById(R.id.servicetext);
        ConstraintLayout contrainvirew = dialog.findViewById(R.id.contrainvirew);

        if (service.equalsIgnoreCase("police")) {
            Glide.with(Sosdragmap_screen.this).load(R.drawable.sirensos).into(imageservice);
            title.setText(db.getvalue("policeondemand"));
            connecttoservice.setText(db.getvalue("nearbypolice"));
            servicetext.setText(db.getvalue("police"));
            contrainvirew.setBackground(getResources().getDrawable(R.drawable.bg_navi_right));
        } else if (service.equalsIgnoreCase("fire")) {
            Glide.with(Sosdragmap_screen.this).load(R.drawable.firesos).into(imageservice);
            title.setText(db.getvalue("generaleme"));
            connecttoservice.setText(db.getvalue("confi"));
            servicetext.setText(db.getvalue("files"));
            contrainvirew.setBackground(getResources().getDrawable(R.drawable.bg_navi_red));
        } else if (service.equalsIgnoreCase("medic")) {
            Glide.with(Sosdragmap_screen.this).load(R.drawable.stethsos).into(imageservice);
            title.setText(db.getvalue("mediaclass"));
            title.setTextColor(getResources().getColor(R.color.grey));
            connecttoservice.setText(db.getvalue("connectamb"));
            connecttoservice.setTextColor(getResources().getColor(R.color.grey));
            servicetext.setText(db.getvalue("medic"));
            contrainvirew.setBackground(getResources().getDrawable(R.drawable.bg_navi_white));
            yes.setBackground(getResources().getDrawable(R.drawable.white_curve_grey));
        }
        yes.setText(db.getvalue("yes"));
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isInternetPresent) {
                    if (service.equalsIgnoreCase("police")) {
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + session.getPolice()));
                        startActivity(intent);
//                        startActivity(new Intent(Sosdragmap_screen.this, Searchpolice.class));
//                        overridePendingTransition(R.anim.enter, R.anim.exit);
                    } else if (service.equalsIgnoreCase("fire")) {
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + session.getFire()));
                        startActivity(intent);
                    } else if (service.equalsIgnoreCase("medic")) {
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + session.getAmbulance()));
                        startActivity(intent);
                    }
                } else {
                    Alert(db.getvalue("alert_label_title"),db.getvalue("alert_nointernet"));
                }
                dialog.dismiss();
            }
        });

        closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        dialog.show();
    }
}
