package com.blackmaria.partner.latestpackageview.view.sidemenu.CloseAccount;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.latestpackageview.widgets.RoundedImageView;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.Adapter.DeActivationListStatementAdapter;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.TimeZone;


public class DeActiviationAccount extends AppCompatActivity implements ApIServices.completelisner, View.OnClickListener {
    private ExpandableHeightListView LvLastStatement;
    private TextView txt_total_amount,txt_label_total_amount;
    private TextView RL_close_account, txt_username;
    private TextView closesummary;
    private Dialog dialog;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private SessionManager session;
    private String UserID = "", value = "", name = "";
    ArrayList<String> arrTitle = null;
    ArrayList<String> arrValue = null;
    private DeActivationListStatementAdapter adapter;
    private ImageView img_back;
    private ProgressBar apiload;
    private RoundedImageView iv_user;
    private AppUtils appUtils;
    AccessLanguagefromlocaldb db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.deactivationloading_constrain);
        db = new AccessLanguagefromlocaldb(this);
        Iinitialize();
    }

    private void Iinitialize() {
        cd = ConnectionDetector.getInstance(DeActiviationAccount.this);
        isInternetPresent = cd.isConnectingToInternet();
        session = SessionManager.getInstance(DeActiviationAccount.this);
        HashMap<String, String> user = session.getUserDetails();
        name = user.get(SessionManager.KEY_DRIVER_NAME);
        UserID = user.get(SessionManager.KEY_DRIVERID);
        LvLastStatement = (ExpandableHeightListView) findViewById(R.id.Lv_last_statement);
        RL_close_account = findViewById(R.id.RL_close_account);
        txt_username = findViewById(R.id.txt_username);
        txt_username.setText(name);

        TextView con = findViewById(R.id.con);
        con.setText(db.getvalue("acccountclosecontent"));
        TextView accountsummary = findViewById(R.id.accountsummary);
        accountsummary.setText(db.getvalue("account_summary"));


        appUtils = AppUtils.getInstance(this);
        apiload = findViewById(R.id.apiload);
        closesummary = findViewById(R.id.closesummary);
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        int year = calendar.get(Calendar.YEAR);
        int currentDay = calendar.get(Calendar.DAY_OF_MONTH);
        SimpleDateFormat month_date = new SimpleDateFormat("MMM");
        String month_name = month_date.format(calendar.getTime());
        closesummary.setText(db.getvalue("closeac")+ currentDay + " " + month_name + " " + year);
        img_back = findViewById(R.id.img_back);
        txt_total_amount = findViewById(R.id.txt_total_amount);
        txt_label_total_amount = findViewById(R.id.txt_label_total_amount);
        iv_user = findViewById(R.id.iv_user);
        String userImage = session.getDriverImageUpdate();
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.new_no_user_img)
                .error(R.drawable.new_no_user_img)
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        Glide.with(this).load(userImage).apply(options).into(iv_user);

        RL_close_account.setOnClickListener(this);
        RL_close_account.setText(db.getvalue("ctn"));
        img_back.setOnClickListener(this);

        getstatementsofacounts();
    }

    private void getstatementsofacounts() {
        apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(this, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", UserID);
        ApIServices apIServices = new ApIServices(this, DeActiviationAccount.this);
        apIServices.dooperation(Iconstant.deactivate_account_statement_url, jsonParams);
    }

    @Override
    public void onClick(View v) {
        if (v == RL_close_account) {
            if (isInternetPresent) {
                Intent i = new Intent(DeActiviationAccount.this, Close_creditwithus.class);
                i.putExtra("amount", value);
                startActivity(i);
            } else {
                Alert(getResources().getString(R.string.action_error), getResources().getString(R.string.alert_nointernet));
            }
        } else if (v == img_back) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);
        }
    }


    //--------------Alert Method-----------
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(DeActiviationAccount.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void sucessresponse(String response) {
        dialog.dismiss();
        apiload.setVisibility(View.INVISIBLE);
        String Sstatus = "", titles = "", values = "", sCurrency = "";
        try {
            JSONObject object = new JSONObject(response);
            Sstatus = object.getString("status");
            if (Sstatus.equalsIgnoreCase("1")) {
                txt_label_total_amount.setVisibility(View.VISIBLE);
                JSONObject jobjResponse = object.getJSONObject("response");
                if (jobjResponse.length() > 0) {
                    sCurrency = jobjResponse.getString("currency");
                    value = sCurrency + " " + jobjResponse.getString("total_amount");
                    Object objStatement = jobjResponse.get("statement");
                    if (objStatement instanceof JSONArray) {
                        JSONArray jarStatement = jobjResponse.getJSONArray("statement");
                        if (jarStatement.length() > 0) {
                            arrTitle = new ArrayList<>();
                            arrValue = new ArrayList<>();
                            for (int i = 0; i < jarStatement.length(); i++) {
                                JSONObject jobjValue = jarStatement.getJSONObject(i);
                                String title = jobjValue.getString("title");
                                String value = sCurrency + " " + jobjValue.getString("value");
                                arrTitle.add(title);
                                arrValue.add(value);
                            }
                        }
                    }
                    txt_total_amount.setText(value);
                    adapter = new DeActivationListStatementAdapter(DeActiviationAccount.this, arrTitle, arrValue);
                    LvLastStatement.setAdapter(adapter);
                    LvLastStatement.setExpanded(true);
                }
            } else {
                appUtils.AlertError(this, getResources().getString(R.string.action_error), object.getString("response"));
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void errorreponse() {
        dialog.dismiss();
        apiload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void jsonexception() {
        dialog.dismiss();
        apiload.setVisibility(View.INVISIBLE);
        AppUtils.toastprint(this, "Json Exception");
    }
}
