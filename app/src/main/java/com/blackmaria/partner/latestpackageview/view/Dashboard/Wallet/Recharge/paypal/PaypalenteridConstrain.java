package com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Recharge.paypal;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.databinding.ActivityPaypalenteridConstrainBinding;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Wallet.PaypalenteridFactory;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.WalletMoneyWebview;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet.PaypalenteridViewModel;

import org.json.JSONException;
import org.json.JSONObject;

public class PaypalenteridConstrain extends AppCompatActivity {

    private ActivityPaypalenteridConstrainBinding binding;
    private PaypalenteridViewModel paypalenteridViewModel;
    private JSONObject json;
    private AppUtils appUtils;
    AccessLanguagefromlocaldb db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
        binding = DataBindingUtil.setContentView(PaypalenteridConstrain.this, R.layout.activity_paypalenterid_constrain);
        paypalenteridViewModel = ViewModelProviders.of(this, new PaypalenteridFactory(this)).get(PaypalenteridViewModel.class);
        binding.setPaypalenteridViewModel(paypalenteridViewModel);
        paypalenteridViewModel.setIds(binding);

        initView();

        binding.datemonthyear.setText(db.getvalue("fastwallet"));
        binding.addcredit.setText(db.getvalue("addcredit"));
        binding.enterpaypalid.setText(db.getvalue("enterpaypalid"));
        binding.paypalemailid.setHint(db.getvalue("enteremailcom"));
        binding.paypalcontent.setText(db.getvalue("paypalcontent"));
        binding.confirm.setText(db.getvalue("confirm_lable"));

        binding.confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.paypalemailid.getText().toString().trim().length() == 0) {
                    appUtils.AlertError(PaypalenteridConstrain.this,db.getvalue("timer_label_alert_sorry"), db.getvalue("profile_label_alert_emailempty"));
                } else if (!AppUtils.isValidEmail(binding.paypalemailid.getText().toString().trim().replace(" ", ""))) {
                    appUtils.AlertError(PaypalenteridConstrain.this, db.getvalue("timer_label_alert_sorry"), db.getvalue("profile_label_alert_email"));
                } else {
                    try {
                        Intent intent = new Intent(PaypalenteridConstrain.this, WalletMoneyWebview.class);
                        intent.putExtra("walletMoney_recharge_amount", json.getString("walletMoney_recharge_amount"));
                        intent.putExtra("walletMoney_currency_symbol", json.getString("walletMoney_currency_symbol"));
                        intent.putExtra("walletMoney_currentBalance", json.getString("walletMoney_currentBalance"));
                        intent.putExtra("walletMoney_paymentType", json.getString("walletMoney_paymentType"));
                        intent.putExtra("email", binding.paypalemailid.getText().toString().trim());
                        startActivity(intent);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Log.v("TRIMMEMORY", " " + level);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }



    private void initView() {
        appUtils = AppUtils.getInstance(this);
        if (getIntent().hasExtra("json")) {
            try {
                json = new JSONObject(getIntent().getStringExtra("json"));
                binding.creditamount.setText(json.getString("walletMoney_currency_symbol") + " " + json.getString("walletMoney_recharge_amount"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
