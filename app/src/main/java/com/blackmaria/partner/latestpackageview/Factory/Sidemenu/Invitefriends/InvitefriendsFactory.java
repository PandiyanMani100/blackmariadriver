package com.blackmaria.partner.latestpackageview.Factory.Sidemenu.Invitefriends;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackmaria.partner.latestpackageview.vieewmodel.sidemenu.invitefriends.InvitefriendsViewModel;

public class InvitefriendsFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public InvitefriendsFactory(Activity context)
    {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass)
    {
        return (T) new InvitefriendsViewModel(context);
    }
}
