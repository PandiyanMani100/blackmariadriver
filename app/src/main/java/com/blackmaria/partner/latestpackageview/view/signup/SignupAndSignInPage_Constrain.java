package com.blackmaria.partner.latestpackageview.view.signup;

import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.databinding.ActivitySignupAndSignInPageConstrainBinding;
import com.blackmaria.partner.latestpackageview.Factory.SignUp.SignupAndSignInPageFactory;
import com.blackmaria.partner.latestpackageview.vieewmodel.signup.SignupAndSignInPageViewModel;


public class SignupAndSignInPage_Constrain extends AppCompatActivity {

    private SignupAndSignInPageViewModel signupAndSignInPageViewModel;
    AccessLanguagefromlocaldb db ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivitySignupAndSignInPageConstrainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_signup_and_sign_in_page__constrain);
        signupAndSignInPageViewModel = ViewModelProviders.of(this, new SignupAndSignInPageFactory(this)).get(SignupAndSignInPageViewModel.class);
        binding.setSignupAndSignInPageViewModel(signupAndSignInPageViewModel);
        db = new AccessLanguagefromlocaldb(SignupAndSignInPage_Constrain.this);
        binding.txtLogin.setText(db.getvalue("login"));
        signupAndSignInPageViewModel.sliderMethod(binding.pager, binding.indicator, binding.RlLogin, SignupAndSignInPage_Constrain.this);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        signupAndSignInPageViewModel.stopscrolling();
    }
}
