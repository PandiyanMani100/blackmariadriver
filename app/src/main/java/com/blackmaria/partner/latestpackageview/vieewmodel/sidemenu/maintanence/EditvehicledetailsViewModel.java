package com.blackmaria.partner.latestpackageview.vieewmodel.sidemenu.maintanence;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;

import androidx.core.content.FileProvider;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferService;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.HeaderSessionManager;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.EditVehicleDetailsConstrainBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.Adapter.SpinnerAdapter;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Model.editvehicledetailspojo;
import com.blackmaria.partner.latestpackageview.Model.vehicledetailspojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.sidemenu.Maintanence.Addseconddriver;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;
import com.yalantis.ucrop.UCrop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;
import static com.yalantis.ucrop.util.BitmapLoadUtils.exifToDegrees;

public class EditvehicledetailsViewModel extends ViewModel implements AdapterView.OnItemSelectedListener, ApIServices.completelisner {

    private Activity context;
    private File imageRoot, destination;
    private Uri mImageCaptureUri, outputUri;
    private String appDirectoryName = "";
    private static final int PERMISSION_REQUEST_CODE = 111;
    private int permissionCode = -1;
    private static final int SELECT_IMAGE_REQUEST = 011;
    private static final int TAKE_PHOTO_REQUEST = 022;
    private byte[] array = null;
    private boolean isInternetPresent = false;
    private HeaderSessionManager headerSessionManager;
    private SessionManager sessionManager;
    private ConnectionDetector cd;
    AccessLanguagefromlocaldb db;

    private String sDriverID = "", SVehicleimageName = "", Agent_Name = "", language_code = "", gcmID = "", sOtpPin = "", sOtpPinStatus = "";
    private EditVehicleDetailsConstrainBinding binding;
    private String experienceyr = "", companyname = "", companyid = "", driverVehicleId = "", driverLocationPlaceId = "", driverProImage = "", driverCountryCode = "", driverPhoneNumber = "", driverReferelCode = "", driverPinCode = "";
    private String driverCompanyType = "", driverCompanyId = "", driverName = "", driverEmailId = "", driverGender = "", driverExperience = "", driverAddress = "",
            driverCityname = "", driverPostCode = "", driverState = "", driverCountry = "";

    private ArrayList<String> arrVehicleId;
    private ArrayList<String> arrVehicleType;
    private ArrayList<String> arrMarkerId;
    private ArrayList<String> arrBrandName;
    private ArrayList<String> arrModelId;
    private ArrayList<String> arrModelName;
    private ArrayList<String> arrModelYr;
    private boolean isSpnrSelectVehicleMakerFirstTime = true, isSpnrSelectVehicleFirstTime = true;
    private String driverVehicleYearId = "", driverVehicleModelId = "", slectMarkerId = "", selctVehicleId = "";
    private registerclickreturn registerclickreturn;
    private vehicledetailspojo pojoglobal;
    private int vehicletypeposition = 0, makerposition = 0, modelposition = 0, yearposition = 0;

    private Dialog dialog;
    private int count = 0;


    String File_names = "";
    String Extension = "";
    String uploaded_file_name = "";
    private String s3_bucket_access_key = "";
    private String s3_bucket_secret_key = "";
    private String s3_bucket_name = "";
    private AmazonS3Client s3Client;
    private BasicAWSCredentials credentials;
    TransferObserver uploadObserver;


    private MutableLiveData<String> getvehicledetailsresponse = new MutableLiveData<>();
    private MutableLiveData<String> updatevehicledetailsresponse = new MutableLiveData<>();
    private editvehicledetailspojo editvehiclepojo;

    public MutableLiveData<String> getGetvehicledetailsresponse() {
        return getvehicledetailsresponse;
    }

    public MutableLiveData<String> getUpdatevehicledetailsresponse() {
        return updatevehicledetailsresponse;
    }

    @Override
    public void sucessresponse(String response) {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        if (count == 0) {
            String status = "", Sresponse = "";
            try {
                JSONObject object = new JSONObject(response);
                status = object.getString("status");
                JSONObject objResponse = object.getJSONObject("response");
                if (objResponse.length() > 0) {
                    Object intervention = objResponse.get("vehicle");
                    if (intervention instanceof JSONArray) {
                        JSONArray categoryArray = objResponse.getJSONArray("vehicle");
                        arrVehicleId = new ArrayList<String>();
                        arrVehicleType = new ArrayList<String>();
                        arrVehicleId.add("0");
                        arrVehicleType.add("Vehicle Type");
                        for (int i = 0; i < categoryArray.length(); i++) {
                            JSONObject locCtaListObj = categoryArray.getJSONObject(i);
                            String id = locCtaListObj.getString("id");
                            String name = locCtaListObj.getString("vehicle_type");
                            arrVehicleId.add(id);
                            arrVehicleType.add(name);
                        }
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (status.equalsIgnoreCase("1")) {
                VehiceleTeypeMethos();
                if (isInternetPresent) {
                    DriverRegMarkerList(Iconstant.DriverReg_MarkerList_URl);
                } else {
                    Alert(db.getvalue("alert_nointernet"), db.getvalue("alert_nointernet_message"));
                }
            } else {
                Alert(db.getvalue("action_error"), Sresponse);
            }
        } else if (count == 1) {
            String status = "", Sresponse = "";
            try {
                JSONObject object = new JSONObject(response);
                status = object.getString("status");
                JSONObject objResponse = object.getJSONObject("response");
                if (objResponse.length() > 0) {
                    Object intervention = objResponse.get("maker");
                    if (intervention instanceof JSONArray) {
                        JSONArray categoryArray = objResponse.getJSONArray("maker");
                        arrMarkerId = new ArrayList<String>();
                        arrBrandName = new ArrayList<String>();
                        arrMarkerId.add("0");
                        arrBrandName.add("Brand Name");
                        for (int i = 0; i < categoryArray.length(); i++) {
                            JSONObject locCtaListObj = categoryArray.getJSONObject(i);
                            String id = locCtaListObj.getString("id");
                            String name = locCtaListObj.getString("brand_name");
                            arrMarkerId.add(id);
                            arrBrandName.add(name);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (status.equalsIgnoreCase("1")) {
                VehiceleMakerMethos();
            } else {
                Alert(db.getvalue("action_error"), Sresponse);
            }
        } else if (count == 2) {
            String status = "", Sresponse = "";
            try {
                JSONObject object = new JSONObject(response);
                status = object.getString("status");
                JSONObject objResponse = object.getJSONObject("response");
                if (objResponse.length() > 0) {
                    Object intervention = objResponse.get("model");
                    if (intervention instanceof JSONArray) {
                        JSONArray categoryArray = objResponse.getJSONArray("model");
                        arrModelYr = new ArrayList<String>();
                        arrModelYr.add("Model year");
                        for (int i = 0; i < categoryArray.length(); i++) {
                            String name = categoryArray.getString(i);
                            arrModelYr.add(name);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (status.equalsIgnoreCase("1")) {
                VehiceleModelYearMethos();
            } else {
                Alert(db.getvalue("action_error"), Sresponse);
            }
        } else if (count == 3) {
            String status = "", Sresponse = "";
            try {
                JSONObject object = new JSONObject(response);
                status = object.getString("status");
                JSONObject objResponse = object.getJSONObject("response");
                if (objResponse.length() > 0) {
                    Object intervention = objResponse.get("model");
                    if (intervention instanceof JSONArray) {
                        JSONArray categoryArray = objResponse.getJSONArray("model");
                        arrModelId = new ArrayList<String>();
                        arrModelName = new ArrayList<String>();
                        arrModelId.add("0");
                        arrModelName.add("Model Name");
                        for (int i = 0; i < categoryArray.length(); i++) {
                            JSONObject locCtaListObj = categoryArray.getJSONObject(i);
                            String id = locCtaListObj.getString("id");
                            String name = locCtaListObj.getString("name");
                            arrModelId.add(id);
                            arrModelName.add(name);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (status.equalsIgnoreCase("1")) {
                VehiceleModelMethos();
            } else {
                Alert(db.getvalue("action_error"), Sresponse);
            }
        } else if (count == 4) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                String sStatus = jsonObject.getString("status");
                JSONObject imagObj = jsonObject.getJSONObject("response");
                if (sStatus.equalsIgnoreCase("1")) {
                    SVehicleimageName = imagObj.getString("image_name");
                } else {
                    String Smsg = jsonObject.getString("response");
                    Alert(db.getvalue("action_error"), Smsg);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (count == 5) {
            Type type = new TypeToken<editvehicledetailspojo>() {
            }.getType();
            editvehiclepojo = new GsonBuilder().create().fromJson(response.toString(), type);
            setalldetails(editvehiclepojo);
        }else if(count ==6){
            updatevehicledetailsresponse.postValue(response);
        }
    }

    private void setalldetails(final editvehicledetailspojo editvehiclepojo) {
        try {
            for (int i = 0; i <= pojoglobal.getResponse().getVehicles().size() - 1; i++) {
                if (pojoglobal.getResponse().getVehicles().get(i).getId().equalsIgnoreCase(editvehiclepojo.getResponse().getVehicle_type())) {
                    binding.selectvehicletype.setSelection(i + 1);
                    final int finalI = i;
                    new Handler().postDelayed(new Runnable() {
                        public void run() {
                            for (int j = 0; j <= pojoglobal.getResponse().getVehicles().get(finalI).getMakers().size() - 1; j++) {
                                if (pojoglobal.getResponse().getVehicles().get(finalI).getMakers().get(j).getId().equalsIgnoreCase(editvehiclepojo.getResponse().getVehicle_maker())) {
                                    binding.selectvehiclemaker.setSelection(j + 1);
                                    final int finalJ = j;
                                    new Handler().postDelayed(new Runnable() {
                                        public void run() {
                                            for (int k = 0; k <= pojoglobal.getResponse().getVehicles().get(finalI).getMakers().get(finalJ).getModels().size() - 1; k++) {
                                                if (pojoglobal.getResponse().getVehicles().get(finalI).getMakers().get(finalJ).getModels().get(k).getId().equalsIgnoreCase(editvehiclepojo.getResponse().getVehicle_model())) {
                                                    binding.selectvehiclmodel.setSelection(k + 1);
                                                    final int finalK = k;
                                                    new Handler().postDelayed(new Runnable() {
                                                        public void run() {
                                                            for (int h = 0; h <= pojoglobal.getResponse().getVehicles().get(finalI).getMakers().get(finalJ).getModels().get(finalK).getYears().size() - 1; h++) {
                                                                if (pojoglobal.getResponse().getVehicles().get(finalI).getMakers().get(finalJ).getModels().get(finalK).getYears().get(h).getValue().equalsIgnoreCase(editvehiclepojo.getResponse().getVehicle_model_year())) {
                                                                    binding.selectyears.setSelection(h + 1);
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                    }, 500);
                                                }
                                            }
                                        }
                                    }, 500);
                                }
                            }
                        }
                    }, 500);
                }
            }
            SVehicleimageName = editvehiclepojo.getResponse().getVehicle_image();
            AppUtils.setImageView(context,editvehiclepojo.getResponse().getVehicle_image_path(), binding.vehiclephoto);
            binding.platenumber.setText(editvehiclepojo.getResponse().getVehicle_number());
            binding.roadtax.setText(editvehiclepojo.getResponse().getRoad_tax_expiry());
            binding.insurance.setText(editvehiclepojo.getResponse().getInsurance_expiry());
            binding.drivinglicense.setText(editvehiclepojo.getResponse().getLicence_expiry());
            binding.vehiclename.setVisibility(View.INVISIBLE);
        } catch (IndexOutOfBoundsException e) {

        } catch (NullPointerException e) {
        }
    }

    @Override
    public void errorreponse() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }


    @Override
    public void jsonexception() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        AppUtils.toastprint(context, "Json Exception");
    }


    public interface registerclickreturn {
        void movetonextscreen(HashMap<String, String> jsonParams);
    }

    public EditvehicledetailsViewModel(Activity context) {
        this.context = context;
        db = new   AccessLanguagefromlocaldb(context);
        headerSessionManager = new HeaderSessionManager(context);
        sessionManager = SessionManager.getInstance(context);
        cd = new ConnectionDetector(context);
        isInternetPresent = cd.isConnectingToInternet();
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        appDirectoryName = context.getResources().getString(R.string.app_name);
        HashMap<String, String> header = headerSessionManager.getHeaderSession();
        Agent_Name = header.get(HeaderSessionManager.KEY_ID_NAME);
        language_code = header.get(HeaderSessionManager.KEY_LANGUAGE_CODE);
        gcmID = user.get(SessionManager.KEY_GCM_ID);

        HashMap<String, String> user1 = sessionManager.getOtpstatusAndPin();
        sOtpPin = user1.get(SessionManager.KEY_REG_OTP_PIN);
        sOtpPinStatus = user1.get(SessionManager.KEY_REG_OTP_STATUS);
        driverVehicleId = sessionManager.getcategory_id();
    }

    public void setregisterclickreturn(registerclickreturn registerclickreturn) {
        this.registerclickreturn = registerclickreturn;
    }


    private void DriverRegMarkerList(String Url) {
        count = 1;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        //jsonParams.put("category_id",driverVehicleId);
        System.out.println("--------------DriverRegMarkerList Url-------------------" + Url);
        // System.out.println("--------------DriverRegMarkerList jsonParams-------------------" + jsonParams);
        ApIServices apIServices = new ApIServices(context, EditvehicledetailsViewModel.this);
        apIServices.dooperation(Url, jsonParams);
    }

    public void getvehicledetails(String Url) {
        count = 5;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        jsonParams.put("mode", "get");
        ApIServices apIServices = new ApIServices(context, EditvehicledetailsViewModel.this);
        apIServices.dooperation(Url, jsonParams);
    }

    public void updatedrivervehicledetails(String Url,HashMap<String, String> jsonParams) {
        count = 6;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ApIServices apIServices = new ApIServices(context, EditvehicledetailsViewModel.this);
        apIServices.dooperation(Url, jsonParams);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent == binding.selectvehicletype) {
            try {
                String sSelectedItem = binding.selectvehicletype.getSelectedItem().toString();
                selctVehicleId = arrVehicleId.get(position);
                vehicletypeposition = position;
                binding.txtvehicletype.setText(sSelectedItem);
                boolean istrue = false;

                if (vehicletypeposition == 0) {
                    arrMarkerId = new ArrayList<String>();
                    arrBrandName = new ArrayList<String>();
                    arrMarkerId.add("0");
                    arrBrandName.add("Brand Name");
                    VehiceleMakerMethos();
                } else {
                    for (int jj = 0; jj < pojoglobal.getResponse().getVehicles().get(vehicletypeposition - 1).getCat_id().size(); jj++) {
                        if (pojoglobal.getResponse().getVehicles().get(vehicletypeposition - 1).getCat_id().get(jj).getCat_id().equalsIgnoreCase(driverVehicleId)) {
                            istrue = true;
                            break;
                        }
                    }
                }


                if (istrue) {
                    arrMarkerId = new ArrayList<String>();
                    arrBrandName = new ArrayList<String>();
                    arrMarkerId.add("0");
                    arrBrandName.add("Brand Name");
                    for (int k = 0; k < pojoglobal.getResponse().getVehicles().get(vehicletypeposition - 1).getMakers().size(); k++) {
                        String ids = pojoglobal.getResponse().getVehicles().get(vehicletypeposition - 1).getMakers().get(k).getId();
                        String names = pojoglobal.getResponse().getVehicles().get(vehicletypeposition - 1).getMakers().get(k).getName();
                        arrMarkerId.add(ids);
                        arrBrandName.add(names);
                    }
                    VehiceleMakerMethos();
                } else {
                    arrMarkerId = new ArrayList<String>();
                    arrBrandName = new ArrayList<String>();
                    arrMarkerId.add("0");
                    arrBrandName.add("Brand Name");
                    VehiceleMakerMethos();
                }
            } catch (Exception e) {
            }
        } else if (parent == binding.selectvehiclemaker) {
            try {
                slectMarkerId = arrMarkerId.get(position);
                String sSelectedItem = binding.selectvehiclemaker.getSelectedItem().toString();
                binding.txtvehiclemaker.setText(sSelectedItem);
                makerposition = position;
                if (!isSpnrSelectVehicleMakerFirstTime) {
                    if (isInternetPresent) {
                        if (arrModelId != null) {
                            arrModelId.clear();
                            arrModelName.clear();
                        }

                        if (makerposition != 0) {
                            arrModelId = new ArrayList<String>();
                            arrModelName = new ArrayList<String>();
                            arrModelId.add("0");
                            arrModelName.add("Model Name");
                            for (int i = 0; i < pojoglobal.getResponse().getVehicles().get(vehicletypeposition - 1).getMakers().get(makerposition - 1).getModels().size(); i++) {
                                String idsd = pojoglobal.getResponse().getVehicles().get(vehicletypeposition - 1).getMakers().get(makerposition - 1).getModels().get(i).getId();
                                String namsade = pojoglobal.getResponse().getVehicles().get(vehicletypeposition - 1).getMakers().get(makerposition - 1).getModels().get(i).getName();
                                arrModelId.add(idsd);
                                arrModelName.add(namsade);
                            }

                            VehiceleModelMethos();
                        }
//                        DriverRegModelList(Iconstant.DriverReg_ModelList_URl);
                    } else {
                        Alert(db.getvalue("alert_nointernet"), db.getvalue("alert_nointernet_message"));
                    }
                }
                isSpnrSelectVehicleMakerFirstTime = false;
            } catch (Exception e) {
            }
        } else if (parent == binding.selectvehiclmodel) {
            try {
                driverVehicleModelId = arrModelId.get(position);
                modelposition = position;
                String sSelectedItem = "";
                if (binding.selectvehiclmodel != null)
                    sSelectedItem = binding.selectvehiclmodel.getSelectedItem().toString().trim();
                if (!isSpnrSelectVehicleFirstTime) {
                    binding.txtvehiclmodel.setText(sSelectedItem);
//                    DriverRegYrList(Iconstant.DriverReg_YearList_URl);
                    if (modelposition != 0) {
                        arrModelYr = new ArrayList<String>();
                        arrModelYr.add("Model year");
                        for (int i = 0; i < pojoglobal.getResponse().getVehicles().get(vehicletypeposition - 1).getMakers().get(makerposition - 1).getModels().get(modelposition - 1).getYears().size(); i++) {
                            String name = pojoglobal.getResponse().getVehicles().get(vehicletypeposition - 1).getMakers().get(makerposition - 1).getModels().get(modelposition - 1).getYears().get(i).getValue();
                            arrModelYr.add(name);
                        }
                        VehiceleModelYearMethos();
                    }
                }

                isSpnrSelectVehicleFirstTime = false;
            } catch (Exception e) {
            }
        } else if (parent == binding.selectyears) {
            try {
                String sSelectedItem = binding.selectyears.getSelectedItem().toString();
                yearposition = position;
                binding.txtyears.setText(sSelectedItem);
                driverVehicleYearId = sSelectedItem;
            } catch (Exception e) {
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    private void VehiceleModelMethos() {
        try {
            if (arrModelName.size() > 0) {

                SpinnerAdapter customSpinnerbank = new SpinnerAdapter(context, arrModelName);
                binding.selectvehiclmodel.setAdapter(customSpinnerbank);
                isSpnrSelectVehicleFirstTime = true;
                binding.selectvehiclmodel.setOnItemSelectedListener(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void VehiceleModelYearMethos() {
        try {
            if (arrModelYr.size() > 0) {
                SpinnerAdapter customSpinnerbank = new SpinnerAdapter(context, arrModelYr);
                binding.selectyears.setAdapter(customSpinnerbank);
                binding.selectyears.setOnItemSelectedListener(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void VehiceleMakerMethos() {
        try {
            if (arrBrandName.size() > 0) {
                SpinnerAdapter customSpinnerbank = new SpinnerAdapter(context, arrBrandName);
                binding.selectvehiclemaker.setAdapter(customSpinnerbank);
                isSpnrSelectVehicleMakerFirstTime = true;
                binding.selectvehiclemaker.setOnItemSelectedListener(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void VehiceleTeypeMethos() {
        try {
            SpinnerAdapter customSpinnerbank = new SpinnerAdapter(context, arrVehicleType);
            binding.selectvehicletype.setAdapter(customSpinnerbank);
            binding.selectvehicletype.setOnItemSelectedListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void uploadpohoto() {
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            if (!checkCameraPermission() || !checkReadExternalStoragePermission() || !checkWriteExternalStoragePermission()) {
                requestPermission();
            } else {
                chooseimage();
            }
        } else {
            chooseimage();
        }
    }

    public void chooseimage() {
        final Dialog photo_dialog = new Dialog(context);
        photo_dialog.getWindow();
        photo_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        photo_dialog.setContentView(R.layout.image_upload_dialog);
        photo_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        photo_dialog.setCanceledOnTouchOutside(true);
        photo_dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_photo_Picker;
        photo_dialog.show();
        photo_dialog.getWindow().setGravity(Gravity.CENTER);

        RelativeLayout camera = (RelativeLayout) photo_dialog
                .findViewById(R.id.profilelayout_takephotofromcamera);
        RelativeLayout gallery = (RelativeLayout) photo_dialog
                .findViewById(R.id.profilelayout_takephotofromgallery);

        TextView howdo = (TextView) photo_dialog
                .findViewById(R.id.howdo);
        TextView tph = (TextView) photo_dialog
                .findViewById(R.id.tph);
        TextView chosex = (TextView) photo_dialog
                .findViewById(R.id.chosex);
        /*howdo.setText(db.getvalue("takephoto"));
        tph.setText(db.getvalue("camra"));
        chosex.setText(db.getvalue("existingcamera"));*/

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraIntent();
                photo_dialog.dismiss();
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                galleryIntent();
                photo_dialog.dismiss();
            }
        });
    }

    private void cameraIntent() {
        try {
            Intent pictureIntent = new Intent(
                    MediaStore.ACTION_IMAGE_CAPTURE);
            if (pictureIntent.resolveActivity(context.getPackageManager()) != null) {
                //Create a file to store the image
                imageRoot = new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES), appDirectoryName);
            }
            if (imageRoot != null) {
                String name = dateToString(new Date(), "yyyy-MM-dd-hh-mm-ss");
                destination = new File(imageRoot, name + ".jpg");
                Uri photoURI = FileProvider.getUriForFile(context, "com.blackmaria.provider", destination);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        photoURI);
                pictureIntent.putExtra("android.intent.extras.CAMERA_FACING", 1);
                context.startActivityForResult(pictureIntent,
                        TAKE_PHOTO_REQUEST);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public String dateToString(Date date, String format) {
        SimpleDateFormat df = new SimpleDateFormat(format);
        return df.format(date);
    }

    private void galleryIntent() {
        try {
            imageRoot = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES), appDirectoryName);
            if (!imageRoot.exists()) {
                imageRoot.mkdir();
            }
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setType("image/*");
            context.startActivityForResult(intent, SELECT_IMAGE_REQUEST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private boolean checkCameraPermission() {
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkReadExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkWriteExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(context, new String[]{Manifest.permission.CAMERA, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
    }

    public void onCaptureImageResult(Intent data) {
        try {
            String imagePath = destination.getAbsolutePath();
            mImageCaptureUri = Uri.fromFile(new File(imagePath));
            outputUri = mImageCaptureUri;

            UCrop.Options options = new UCrop.Options();
            options.setStatusBarColor(context.getResources().getColor(R.color.black_color));
            options.setToolbarColor(context.getResources().getColor(R.color.app_primary_color));
            options.setMaxBitmapSize(800);

            UCrop.of(mImageCaptureUri, outputUri)
                    .withAspectRatio(1, 1)
                    .withMaxResultSize(8000, 8000)
                    .withOptions(options)
                    .start(context);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onSelectFromGalleryResult(Intent data) {

        try {
            mImageCaptureUri = data.getData();

            if (!imageRoot.exists()) {
                imageRoot.mkdir();
            }

            String name = dateToString(new Date(), "yyyy-MM-dd-hh-mm-ss");
            destination = new File(imageRoot, name + ".jpg");

            outputUri = Uri.fromFile(destination);

            UCrop.Options options = new UCrop.Options();
            options.setStatusBarColor(context.getResources().getColor(R.color.black_color));
            options.setToolbarColor(context.getResources().getColor(R.color.app_primary_color));
            options.setMaxBitmapSize(800);

            UCrop.of(mImageCaptureUri, outputUri)
                    .withAspectRatio(1, 1)
                    .withMaxResultSize(8000, 8000)
                    .withOptions(options)
                    .start(context);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void onCroppedImageResult(Intent data) {

        final Uri resultUri = UCrop.getOutput(data);

        Log.d("Crop success", "" + resultUri);
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), resultUri);

            if (bitmap == null) {
                Log.d("Bitmap", "null");
            } else {
                Log.d("Bitmap", "not null");
                binding.vehiclephoto.setImageBitmap(bitmap);
                binding.vehiclename.setVisibility(View.GONE);
            }
            binding.vehiclename.setVisibility(View.GONE);

            Bitmap thumbnail = bitmap;
            final String picturePath = resultUri.getPath();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            File curFile = new File(picturePath);
            try {
                ExifInterface exif = new ExifInterface(curFile.getPath());
                int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                int rotationInDegrees = exifToDegrees(rotation);

                Matrix matrix = new Matrix();
                if (rotation != 0f) {
                    matrix.preRotate(rotationInDegrees);
                }
                thumbnail = Bitmap.createBitmap(thumbnail, 0, 0, thumbnail.getWidth(), thumbnail.getHeight(), matrix, true);
            } catch (IOException ex) {
                Log.e("TAG", "Failed to get Exif data", ex);
            }

            thumbnail.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
            array = byteArrayOutputStream.toByteArray();

            if (isInternetPresent) {
                UploadDriverImage(Iconstant.DriverReg_UploadVehicleImage_URl,curFile);
            } else {
                Alert(db.getvalue("alert_nointernet"), db.getvalue("alert_nointernet_message"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void UploadDriverImage(String url,File curFile) {
        count = 4;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        if(sessionManager.getimagestatus().equals("1"))
        {
            s3_bucket_name = sessionManager.getbucketname();
            s3_bucket_access_key = sessionManager.getaccesskey();
            s3_bucket_secret_key = sessionManager.getsecretkey();

            context.startService(new Intent(context, TransferService.class));
            credentials = new BasicAWSCredentials(s3_bucket_access_key, s3_bucket_secret_key);
            s3Client = new AmazonS3Client(credentials);
            s3Client.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));
            uploadToS3Bucket(curFile, context);
        }
        else
        {
            ApIServices apIServices = new ApIServices(context, EditvehicledetailsViewModel.this);
            apIServices.imageupload(url, array, "blackmariavehicle.jpg");
        }



    }



    private void uploadToS3Bucket(File fileUri, final Activity context)
    {

        if(fileUri.exists())
        {
            System.out.println("file exist");
        }
        else
        {
            System.out.println("file exist not");
        }


        String[] path_split;
        if (fileUri != null) {

            final Dialog dialog = new Dialog(context);
            dialog.getWindow();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.custom_loading);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();

            uploaded_file_name = "";
            String path = String.valueOf(fileUri);
            String file_name_full = path.substring(path.lastIndexOf("/") + 1);
            path_split = file_name_full.split(".");
            if (path_split.length == 2) {
                File_names = path_split[0];
                Extension = path_split[1];

            } else {
                path_split = file_name_full.split(".jpg");
                if (path_split.length == 1) {
                    File_names = path_split[0];
                }
            }

            String file_name_upload = File_names;
            //current time stamp
            Long tsLong = System.currentTimeMillis() / 1000;
            String str_current_ts = tsLong.toString();
            String concat_image_name = file_name_upload + "" + str_current_ts;
            final String fileName = md5Encryption(concat_image_name) + "." + "jpg";


            TransferUtility transferUtility =
                    TransferUtility.builder()
                            .context(context)
                            .defaultBucket(s3_bucket_name)
                            .s3Client(s3Client)
                            .build();
            uploaded_file_name = fileName;

            uploadObserver = transferUtility.upload("images/vehicle_image/" + fileName, fileUri, CannedAccessControlList.PublicRead);

            // Attach a listener to the observer to get state update and progress notifications
            uploadObserver.setTransferListener(new TransferListener() {

                @Override
                public void onStateChanged(int id, TransferState state) {
                    // Handle a completed upload.
                    if (TransferState.COMPLETED == state) {
                        dialog.dismiss();

                        HashMap<String, String> jsonParams = new HashMap<>();
                        jsonParams.put("vehicle_image", fileName);

                        String path = uploadObserver.getAbsoluteFilePath();

                        ApIServices apIServices = new ApIServices(context, EditvehicledetailsViewModel.this);
                        apIServices.amazoniagealoinesend(Iconstant.DriverReg_UploadVehicleImage_URl,jsonParams);


                    } else if (TransferState.FAILED == state) {
                        // dialog.dismiss();
                        dialog.dismiss();
                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                    float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                    int percentDone = (int) percentDonef;
                    Log.d("YourActivity", "ID:" + id + " bytesCurrent: " + bytesCurrent + " bytesTotal: " + bytesTotal + " " + percentDone + "%");
                }

                @Override
                public void onError(int id, Exception ex) {
                    // Handle errors
                    ex.printStackTrace();
                    uploaded_file_name = "";

                    dialog.dismiss();
                }

            });

            // If you prefer to poll for the data, instead of attaching a
            // listener, check for the state and progress in the observer.
            if (TransferState.COMPLETED == uploadObserver.getState()) {
                // Handle a completed upload.
                Log.d("completed upload", "completed upload");
            }

        }
    }


    public String md5Encryption(String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(context);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(db.getvalue("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    public void setIds(EditVehicleDetailsConstrainBinding binding) {
        this.binding = binding;

        if (isInternetPresent) {
            try {
                Type type = new TypeToken<vehicledetailspojo>() {
                }.getType();
                pojoglobal = new GsonBuilder().create().fromJson(sessionManager.getvehicleinfo(), type);

                if (pojoglobal.getStatus().equalsIgnoreCase("1")) {
                    if (pojoglobal.getResponse().getVehicles().size() > 0) {
                        arrVehicleId = new ArrayList<String>();
                        arrVehicleType = new ArrayList<String>();
                        arrVehicleId.add("0");
                        arrVehicleType.add("Vehicle Type");
                        for (int i = 0; i < pojoglobal.getResponse().getVehicles().size(); i++) {
                            String id = pojoglobal.getResponse().getVehicles().get(i).getId();
                            String name = pojoglobal.getResponse().getVehicles().get(i).getName();
                            arrVehicleId.add(id);
                            arrVehicleType.add(name);
                        }
                        VehiceleTeypeMethos();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Alert(db.getvalue("alert_nointernet"), db.getvalue("alert_nointernet_message"));
        }
    }

    private void AlertError(String title, String message) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        Snackbar bar = Snackbar.with(context);
        bar.addView(view);
        bar.colorResource(R.color.dark_red);
        bar.position(Snackbar.SnackbarPosition.TOP);
        bar.type(SnackbarType.MULTI_LINE);
        bar.duration(Snackbar.SnackbarDuration.LENGTH_SHORT);
        bar.animation(true);
        SnackbarManager.show(bar, context);
    }


    public void confirm() {
        if (SVehicleimageName.equalsIgnoreCase("")) {
            AlertError(db.getvalue("timer_label_alert_sorry"), db.getvalue("driver_reg_vehicleimage"));
        } else if (binding.roadtax.getText().toString().equalsIgnoreCase("")) {
            AlertError(db.getvalue("timer_label_alert_sorry"), db.getvalue("driver_reg_roadtaxt_exp"));
        } else if (binding.insurance.getText().toString().equalsIgnoreCase("")) {
            AlertError(db.getvalue("timer_label_alert_sorry"), db.getvalue("driver_reg_insurance"));
        } else if (binding.drivinglicense.getText().toString().equalsIgnoreCase("")) {
            AlertError(db.getvalue("timer_label_alert_sorry"), db.getvalue("driver_reg_License_exp"));
        } else if (binding.platenumber.getText().toString().trim().equalsIgnoreCase("")) {
            AlertError(db.getvalue("timer_label_alert_sorry"), db.getvalue("driver_reg_vehiclereg_num"));
        } else if (selctVehicleId.equalsIgnoreCase("0")) {
            AlertError(db.getvalue("timer_label_alert_sorry"), db.getvalue("driver_reg_vehicle_type"));
        } else if (slectMarkerId.equalsIgnoreCase("0")) {
            AlertError(db.getvalue("timer_label_alert_sorry"), db.getvalue("driver_reg_vehicle_maker1"));
        } else if (driverVehicleModelId.equalsIgnoreCase("0")) {
            AlertError(db.getvalue("timer_label_alert_sorry"), db.getvalue("driver_reg_vehicle_model"));
        } else if (driverVehicleYearId.equalsIgnoreCase("Model year")) {
            AlertError(db.getvalue("timer_label_alert_sorry"), db.getvalue("driver_reg_vehicle_year_model"));
        } else {
            if (isInternetPresent) {
                HashMap<String, String> jsonParams = new HashMap<String, String>();

                jsonParams.put("driver_id", sDriverID);
                jsonParams.put("mode", "update");
                jsonParams.put("vehicle_image", SVehicleimageName);
                jsonParams.put("road_tax_expiry", binding.roadtax.getText().toString());
                jsonParams.put("licence_expiry", binding.drivinglicense.getText().toString());
                jsonParams.put("insurance_expiry", binding.insurance.getText().toString());
                jsonParams.put("vehicle_number", binding.platenumber.getText().toString().trim());
                jsonParams.put("vehicle_type", selctVehicleId);
                jsonParams.put("vehicle_maker", slectMarkerId);
                jsonParams.put("vehicle_model", driverVehicleModelId);
                jsonParams.put("vehicle_model_year", driverVehicleYearId);

                registerclickreturn.movetonextscreen(jsonParams);
            } else {
                Alert(db.getvalue("alert_nointernet"), db.getvalue("alert_nointernet_message"));
            }
        }
    }


}
