package com.blackmaria.partner.latestpackageview.view.sidemenu.Warnings;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;

import androidx.core.content.FileProvider;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import androidx.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityWarningsconstrainBinding;
import com.blackmaria.partner.latestpackageview.Factory.Sidemenu.Warnings.WarningsFactory;
import com.blackmaria.partner.latestpackageview.Model.uploadimagepojo;
import com.blackmaria.partner.latestpackageview.Model.warningspojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.vieewmodel.sidemenu.warnings.WarningsViewModel;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.yalantis.ucrop.UCrop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import static com.yalantis.ucrop.util.BitmapLoadUtils.exifToDegrees;

public class Warningsconstrain extends AppCompatActivity {

    private ActivityWarningsconstrainBinding binding;
    private WarningsViewModel warningsViewModel;
    private SessionManager sessionManager;
    private AppUtils appUtils;
    private String driverid = "", driverimage = "", documentdate = "";
    private warningspojo pojowarnings;
    private File imageRoot, destination;
    private String appDirectoryName = "";
    private static final int TAKE_PHOTO_REQUEST = 022;
    private static final int SELECT_IMAGE_REQUEST = 011;
    private static final int PERMISSION_REQUEST_CODE = 111;
    private int permissionCode = -1, postionofdocument = 0;
    private Uri mImageCaptureUri, outputUri;
    private byte[] array = null;
    AccessLanguagefromlocaldb db;

    private DatePickerDialog pickerDialog;
    private int year, month, day, pickerNumber = 0;

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        db = new AccessLanguagefromlocaldb(this);
        binding = DataBindingUtil.setContentView(Warningsconstrain.this, R.layout.activity_warningsconstrain);
        warningsViewModel = ViewModelProviders.of(this, new WarningsFactory(this)).get(WarningsViewModel.class);
        binding.setWarningsViewModel(warningsViewModel);
        warningsViewModel.setIds(binding);

        initView();
        binding.message.setText(db.getvalue("message"));
        binding.warningstext.setText(db.getvalue("warnings"));
        binding.nowarnings.setText(db.getvalue("nowarnings"));

    }

    private void initView() {
        sessionManager = SessionManager.getInstance(this);
        appUtils = AppUtils.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        driverid = user.get(SessionManager.KEY_DRIVERID);
        driverimage = user.get(SessionManager.KEY_DRIVER_IMAGE);
//        AppUtils.setImageView(Warningsconstrain.this, driverimage, binding.warningsimage);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", driverid);
        warningsViewModel.warningsgetlistapihit(jsonParams);
        setresponse();
    }

    private void setresponse() {

        warningsViewModel.getImageuplaodresponse().observe(Warningsconstrain.this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    opencalender(response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        warningsViewModel.getWarningsresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String respones) {
                if(respones.contains("warning_count"))
                {
                    try {
                        JSONObject jsonObject = new JSONObject(respones);
                        Type type = new TypeToken<warningspojo>() {
                        }.getType();
                        pojowarnings = new GsonBuilder().create().fromJson(jsonObject.toString(), type);
                        setText(pojowarnings);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


            }
        });
    }

    private void setText(final warningspojo object) {
        try {
            binding.warningslist.removeAllViews();
            if (object.getResponse().getWarning_list().size() > 0) {
                binding.warningsscroll.setVisibility(View.VISIBLE);
                binding.nowarnings.setVisibility(View.GONE);
            } else {
                binding.warningsscroll.setVisibility(View.INVISIBLE);
                binding.nowarnings.setVisibility(View.VISIBLE);
            }
        } catch (NullPointerException e) {
        }
        for (int i = 0; i <= object.getResponse().getWarning_list().size() - 1; i++) {
            View view = getLayoutInflater().inflate(R.layout.warningslistcustomlayout, null);
            TextView content = view.findViewById(R.id.content);
            TextView upload = view.findViewById(R.id.upload);
            upload.setText(db.getvalue("upload"));
            content.setText(object.getResponse().getWarning_list().get(i).getText());
            final int finalI = i;
            upload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    postionofdocument = finalI;
                    checkAndGrantPermission();
                }
            });
            binding.warningslist.addView(view);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    chooseimage();
                }
                break;
        }
    }


    private void checkAndGrantPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            if (!checkCameraPermission() || !checkReadExternalStoragePermission() || !checkWriteExternalStoragePermission()) {
                requestPermission();
            } else {
                chooseimage();
            }
        } else {
            chooseimage();
        }
    }

    private void chooseimage() {
        final Dialog photo_dialog = new Dialog(this);
        photo_dialog.getWindow();
        photo_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        photo_dialog.setContentView(R.layout.image_upload_dialog);
        photo_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        photo_dialog.setCanceledOnTouchOutside(true);
        photo_dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_photo_Picker;
        photo_dialog.show();
        photo_dialog.getWindow().setGravity(Gravity.CENTER);

        RelativeLayout camera = (RelativeLayout) photo_dialog
                .findViewById(R.id.profilelayout_takephotofromcamera);
        RelativeLayout gallery = (RelativeLayout) photo_dialog
                .findViewById(R.id.profilelayout_takephotofromgallery);

        TextView howdo = (TextView) photo_dialog
                .findViewById(R.id.howdo);
        TextView tph = (TextView) photo_dialog
                .findViewById(R.id.tph);
        TextView chosex = (TextView) photo_dialog
                .findViewById(R.id.chosex);
        howdo.setText(db.getvalue("takephoto"));
        tph.setText(db.getvalue("camra"));
        chosex.setText(db.getvalue("existingcamera"));

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraIntent();
                photo_dialog.dismiss();
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                galleryIntent();
                photo_dialog.dismiss();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_IMAGE_REQUEST) {
                onSelectFromGalleryResult(data);
            } else if (requestCode == TAKE_PHOTO_REQUEST) {
                onCaptureImageResult(data);
            } else if (requestCode == UCrop.REQUEST_CROP) {
                onCroppedImageResult(data);
            } else if (resultCode == UCrop.RESULT_ERROR) {
                final Throwable cropError = UCrop.getError(data);
            }
        }
    }

    public void onCaptureImageResult(Intent data) {
        try {
            String imagePath = destination.getAbsolutePath();
            mImageCaptureUri = Uri.fromFile(new File(imagePath));
            outputUri = mImageCaptureUri;
            UCrop.Options options = new UCrop.Options();
            options.setStatusBarColor(getResources().getColor(R.color.black_color));
            options.setToolbarColor(getResources().getColor(R.color.app_primary_color));
            options.setMaxBitmapSize(800);
            UCrop.of(mImageCaptureUri, outputUri)
                    .withAspectRatio(1, 1)
                    .withMaxResultSize(8000, 8000)
                    .withOptions(options)
                    .start(this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void onSelectFromGalleryResult(Intent data) {
        try {
            mImageCaptureUri = data.getData();
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), mImageCaptureUri);
            if (!imageRoot.exists()) {
                imageRoot.mkdir();
            }
            String name = dateToString(new Date(), "yyyy-MM-dd-hh-mm-ss");
            destination = new File(imageRoot, name + ".jpg");
            outputUri = Uri.fromFile(destination);
            UCrop.Options options = new UCrop.Options();
            options.setStatusBarColor(getResources().getColor(R.color.black_color));
            options.setToolbarColor(getResources().getColor(R.color.app_primary_color));
            options.setMaxBitmapSize(800);

            UCrop.of(mImageCaptureUri, outputUri)
                    .withAspectRatio(1, 1)
                    .withMaxResultSize(8000, 8000)
                    .withOptions(options)
                    .start(this);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void onCroppedImageResult(Intent data) {

        final Uri resultUri = UCrop.getOutput(data);

        Log.d("Crop success", "" + resultUri);
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), resultUri);

            if (bitmap == null) {
                Log.d("Bitmap", "null");
            } else {
                Log.d("Bitmap", "not null");
            }

            Bitmap thumbnail = bitmap;
            final String picturePath = resultUri.getPath();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            File curFile = new File(picturePath);
            try {
                ExifInterface exif = new ExifInterface(curFile.getPath());
                int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                int rotationInDegrees = exifToDegrees(rotation);

                Matrix matrix = new Matrix();
                if (rotation != 0f) {
                    matrix.preRotate(rotationInDegrees);
                }
                thumbnail = Bitmap.createBitmap(thumbnail, 0, 0, thumbnail.getWidth(), thumbnail.getHeight(), matrix, true);
            } catch (IOException ex) {
                Log.e("TAG", "Failed to get Exif data", ex);
            }

            thumbnail.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
            array = byteArrayOutputStream.toByteArray();

            warningsViewModel.UploadDriverImage(array,curFile);

            //uploadimage
//            if (isInternetPresent) {
//                UploadDriverImage(Iconstant.DriverReg_upload_Image_URl);
//            } else {
//                Alert(context.getResources().getString(R.string.alert_nointernet), context.getResources().getString(R.string.alert_nointernet_message));
//            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void opencalender(final String imagepath) {
        pickerDialog = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerDialog va, int years, int montha, int days) {
                pickerDialog.dismiss();
                int months = montha + 1;
                year = years;
                month = montha;
                day = days;
                documentdate = months + "/" + day + "/" + year;
                uploaddriverdoc(documentdate, imagepath);
            }
        }, year, month, day);
        pickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                pickerDialog.dismiss();
            }
        });

        pickerDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                pickerDialog.dismiss();
            }
        });
        pickerDialog.setMinDate(Calendar.getInstance());
        pickerDialog.setThemeDark(false);
        pickerDialog.showYearPickerFirst(false);
        pickerDialog.setAccentColor(getResources().getColor(R.color.signin_and_signup_slider_ball_blue));
        pickerDialog.show(getFragmentManager(), "DatePickerDialog");
    }

    private void uploaddriverdoc(String documentdate, String imagepath) {
        /*Type type = new TypeToken<uploadimagepojo>() {
        }.getType();
        uploadimagepojo pojo = new GsonBuilder().create().fromJson(jsonObject.toString(), type);
*/
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", driverid);
        jsonParams.put("document_id", pojowarnings.getResponse().getWarning_list().get(postionofdocument).getId());
        jsonParams.put("document", imagepath);
        jsonParams.put("doc_name", pojowarnings.getResponse().getWarning_list().get(postionofdocument).getTypeName());
        jsonParams.put("docType", pojowarnings.getResponse().getWarning_list().get(postionofdocument).getType());
        jsonParams.put("expiryDate", documentdate);

        warningsViewModel.updatedriverdoc(jsonParams);
        warningsViewModel.getUpdatedriverdoc().observe(Warningsconstrain.this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject1 = new JSONObject(response);
                    if (jsonObject1.getString("status").equalsIgnoreCase("1")) {


                        final Handler handler = new Handler();

                        handler.postDelayed(new Runnable() {

                            @Override

                            public void run() {
                                HashMap<String, String> jsonParams = new HashMap<String, String>();
                                jsonParams.put("driver_id", driverid);
                                warningsViewModel.warningsgetlistapihit(jsonParams);

                            }

                        }, 1000);
                        appUtils.AlertSuccess(Warningsconstrain.this, db.getvalue("action_success"), jsonObject1.getString("response"));

                    } else {
                        appUtils.AlertSuccess(Warningsconstrain.this,db.getvalue("action_error"), jsonObject1.getString("response"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void cameraIntent() {
        try {
            Intent pictureIntent = new Intent(
                    MediaStore.ACTION_IMAGE_CAPTURE);
            if (pictureIntent.resolveActivity(getPackageManager()) != null) {
                //Create a file to store the image
                imageRoot = new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES), appDirectoryName);
            }
            if (imageRoot != null) {
                String name = dateToString(new Date(), "yyyy-MM-dd-hh-mm-ss");
                destination = new File(imageRoot, name + ".jpg");
                Uri photoURI = FileProvider.getUriForFile(this, "com.blackmaria.provider", destination);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        photoURI);
                pictureIntent.putExtra("android.intent.extras.CAMERA_FACING", 1);
                startActivityForResult(pictureIntent,
                        TAKE_PHOTO_REQUEST);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String dateToString(Date date, String format) {
        SimpleDateFormat df = new SimpleDateFormat(format);
        return df.format(date);
    }

    public void galleryIntent() {
        try {

            imageRoot = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES), appDirectoryName);
            if (!imageRoot.exists()) {
                imageRoot.mkdir();
            }

            Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setType("image/*");
            startActivityForResult(intent, SELECT_IMAGE_REQUEST);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
    }

    private boolean checkCameraPermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkReadExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkWriteExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }


}
