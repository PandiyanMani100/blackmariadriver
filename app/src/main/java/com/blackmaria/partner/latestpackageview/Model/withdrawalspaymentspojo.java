package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class withdrawalspaymentspojo implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;

    // Getter Methods
    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }


    public class Response {
        @SerializedName("available_amount")
        private String available_amount;
        @SerializedName("currency")
        private String currency;
        @SerializedName("last_withdrawal_txt")
        private String last_withdrawal_txt;
        @SerializedName("withdraw_fee")
        private String withdraw_fee;
        @SerializedName("driver_name")
        private String driver_name;
        @SerializedName("mobile_number")
        private String mobile_number;

        public String getDriver_name() {
            return driver_name;
        }

        public void setDriver_name(String driver_name) {
            this.driver_name = driver_name;
        }

        public String getMobile_number() {
            return mobile_number;
        }

        public void setMobile_number(String mobile_number) {
            this.mobile_number = mobile_number;
        }

        public String getDail_code() {
            return dail_code;
        }

        public void setDail_code(String dail_code) {
            this.dail_code = dail_code;
        }

        @SerializedName("dail_code")
        private String dail_code;


        public String getWithdraw_fee() {
            return withdraw_fee;
        }

        public void setWithdraw_fee(String withdraw_fee) {
            this.withdraw_fee = withdraw_fee;
        }

        public Banking_details getBanking_detailsobj() {
            return banking_detailsobj;
        }

        public void setBanking_detailsobj(Banking_details banking_detailsobj) {
            this.banking_detailsobj = banking_detailsobj;
        }

        @SerializedName("banking_details")
        private Banking_details banking_detailsobj;

        public class Banking_details{
            @SerializedName("acc_holder_name")
            private String acc_holder_name;
            @SerializedName("acc_number")
            private String acc_number;
            @SerializedName("bank_name")
            private String bank_name;

            public String getBank_image() {
                return bank_image;
            }

            public void setBank_image(String bank_image) {
                this.bank_image = bank_image;
            }

            @SerializedName("bank_image")
            private String bank_image;

            public String getAcc_holder_name() {
                return acc_holder_name;
            }

            public void setAcc_holder_name(String acc_holder_name) {
                this.acc_holder_name = acc_holder_name;
            }

            public String getAcc_number() {
                return acc_number;
            }

            public void setAcc_number(String acc_number) {
                this.acc_number = acc_number;
            }

            public String getBank_name() {
                return bank_name;
            }

            public void setBank_name(String bank_name) {
                this.bank_name = bank_name;
            }

            public String getBank_code() {
                return bank_code;
            }

            public void setBank_code(String bank_code) {
                this.bank_code = bank_code;
            }

            @SerializedName("bank_code")
            private String bank_code;
        }

        public ArrayList<Payment> getPayment() {
            return payment;
        }

        public void setPayment(ArrayList<Payment> payment) {
            this.payment = payment;
        }
        @SerializedName("payment")
        ArrayList < Payment > payment = new ArrayList< Payment >();
        @SerializedName("proceed_status")
        private String proceed_status;
        @SerializedName("error_message")
        private String error_message;
        @SerializedName("wallet_min_amount")
        private String wallet_min_amount;
        @SerializedName("banking")
        private String banking;

        public ArrayList<Withdraw_location> getWithdraw_location() {
            return withdraw_location;
        }

        public void setWithdraw_location(ArrayList<Withdraw_location> withdraw_location) {
            this.withdraw_location = withdraw_location;
        }

        @SerializedName("withdraw_location")
        ArrayList < Withdraw_location > withdraw_location = new ArrayList< Withdraw_location >();

        public class Withdraw_location{
            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getLocation_address() {
                return location_address;
            }

            public void setLocation_address(String location_address) {
                this.location_address = location_address;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            @SerializedName("name")
            private String name;
            @SerializedName("location_address")
            private String location_address;
            @SerializedName("id")
            private String id;

            public Location getLocationobj() {
                return locationobj;
            }

            public void setLocationobj(Location locationobj) {
                this.locationobj = locationobj;
            }

            @SerializedName("location")
            private Location locationobj;

            public class Location{
                @SerializedName("lng")
                private String lng;

                public String getLng() {
                    return lng;
                }

                public void setLng(String lng) {
                    this.lng = lng;
                }

                public String getLat() {
                    return lat;
                }

                public void setLat(String lat) {
                    this.lat = lat;
                }

                @SerializedName("lat")
                private String lat;
            }

        }

        public class Payment{
            @SerializedName("name")
            private String name;
            @SerializedName("code")
            private String code;
            @SerializedName("icon")
            private String icon;
            @SerializedName("inactive_icon")
            private String inactive_icon;


            // Getter Methods 

            public String getName() {
                return name;
            }

            public String getCode() {
                return code;
            }

            public String getIcon() {
                return icon;
            }

            public String getInactive_icon() {
                return inactive_icon;
            }

            // Setter Methods 

            public void setName(String name) {
                this.name = name;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public void setIcon(String icon) {
                this.icon = icon;
            }

            public void setInactive_icon(String inactive_icon) {
                this.inactive_icon = inactive_icon;
            }
        }

        // Getter Methods

        public String getAvailable_amount() {
            return available_amount;
        }

        public String getCurrency() {
            return currency;
        }

        public String getLast_withdrawal_txt() {
            return last_withdrawal_txt;
        }

        public String getProceed_status() {
            return proceed_status;
        }

        public String getError_message() {
            return error_message;
        }

        public String getWallet_min_amount() {
            return wallet_min_amount;
        }

        public String getBanking() {
            return banking;
        }

        // Setter Methods

        public void setAvailable_amount(String available_amount) {
            this.available_amount = available_amount;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public void setLast_withdrawal_txt(String last_withdrawal_txt) {
            this.last_withdrawal_txt = last_withdrawal_txt;
        }

        public void setProceed_status(String proceed_status) {
            this.proceed_status = proceed_status;
        }

        public void setError_message(String error_message) {
            this.error_message = error_message;
        }

        public void setWallet_min_amount(String wallet_min_amount) {
            this.wallet_min_amount = wallet_min_amount;
        }

        public void setBanking(String banking) {
            this.banking = banking;
        }
    }
}
