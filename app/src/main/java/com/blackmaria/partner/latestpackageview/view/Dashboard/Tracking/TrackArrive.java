package com.blackmaria.partner.latestpackageview.view.Dashboard.Tracking;

import android.app.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.ComponentCallbacks2;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;

import androidx.databinding.DataBindingUtil;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;

import androidx.annotation.Nullable;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.app.ChatIntentServiceResult;
import com.blackmaria.partner.app.DbHelper;
import com.blackmaria.partner.app.MessagesActivity;
import com.blackmaria.partner.app.TrackRideCancelTrip;

import com.blackmaria.partner.databinding.ActivityTrackArriveBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.OnlineupdatelocationtoserverService;
import com.blackmaria.partner.latestpackageview.ApiRequest.RidedbApiIntentService;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Tracking.TrackArriveFactory;
import com.blackmaria.partner.latestpackageview.Model.continuetrippojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Dashboard_constrain;
import com.blackmaria.partner.latestpackageview.view.fare.Cashpayment;
import com.blackmaria.partner.latestpackageview.view.fare.FareSummaryConstrain;
import com.blackmaria.partner.latestpackageview.view.Dashboard.OnlinepageConstrain;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.tracking.TrackArriveViewModel;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.blackmaria.partner.mylibrary.latlnginterpolation.LatLngInterpolator;
import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;

public class TrackArrive extends AppCompatActivity implements RoutingListener, ComponentCallbacks2 {

    private ActivityTrackArriveBinding binding;
    private TrackArriveViewModel trackArriveViewModel;
    private SessionManager sessionManager;
    ConstraintLayout chatlayout;
    private String sDriveID = "", driverimage = "";
    private continuetrippojo object;
    private AppUtils appUtils;
    private Animation layoutanimation;
    private ArrayList<LatLng> wayPointList;
    private LatLngBounds.Builder wayPointBuilder;
    private GoogleMap googleMap;
    private MapFragment mapFragment;
    private LatLng newLatLng, oldLatLng;
    private LatLngInterpolator mLatLngInterpolator;
    private Location oldLocation, changelatLocations;
    private double myMovingDistance = 0.0;
    private Marker carMarker;
    private BitmapDescriptor carIcon;
    private String currentlat = "", currentlon = "";
    private Handler mapHandler = new Handler();
    private final static int OVERLAY_PERMISSION_REQ_CODE = 1234;
    private int countcallresponse = 0;
    private float globalbearing = 0;

    private LatLng routefromlatlng = null, routetolatlon = null;

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(TrackArrive.this, R.layout.activity_track_arrive);
        trackArriveViewModel = ViewModelProviders.of(this, new TrackArriveFactory(this)).get(TrackArriveViewModel.class);
        binding.setTrackArriveViewModel(trackArriveViewModel);
        trackArriveViewModel.setIds(binding);
        initView();
        clicklistener();
        binding.chatlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getApplicationContext(), MessagesActivity.class);
                intent.putExtra("name",object.getResponse().getUser_profile().getUser_name());
                intent.putExtra("userimage",object.getResponse().getUser_profile().getUser_image());
                intent.putExtra("rideid",object.getResponse().getUser_profile().getRide_id());
                intent.putExtra("useridtosend",object.getResponse().getUser_profile().getUser_id());
                intent.putExtra("userphonenumber",object.getResponse().getUser_profile().getPhone_number());

                intent.putExtra("ios_fcm_key",object.getResponse().getUser_profile().getNotification_details().getIos_fcm_key());
                intent.putExtra("user_fcm_token",object.getResponse().getUser_profile().getNotification_details().getUser_fcm_token());

                startActivity(intent);
            }
        });
    }

    private void initView() {
        appUtils = AppUtils.getInstance(this);
        sessionManager = SessionManager.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriveID = user.get(SessionManager.KEY_DRIVERID);
        driverimage = user.get(SessionManager.KEY_DRIVER_IMAGE);
        carIcon = (BitmapDescriptorFactory.fromResource(R.drawable.carmove));
        if (currentlat.equalsIgnoreCase("")) {
            currentlat = String.valueOf(sessionManager.getLatitude());
            currentlon = String.valueOf(sessionManager.getLongitude());
        }
        initializeMap();
        trackArriveViewModel.continuretripapihit(sDriveID, getIntent().getStringExtra("rideid"));
        setResponse();
    }


    private void clicklistener() {
        binding.minimize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutanimation = AnimationUtils.loadAnimation(TrackArrive.this, R.anim.slidegodown);
                binding.trackview.startAnimation(layoutanimation);
                binding.trackview.setVisibility(View.GONE);
                binding.userview.setVisibility(View.GONE);
                layoutanimation = AnimationUtils.loadAnimation(TrackArrive.this, R.anim.shake);
                binding.maximizewindow.startAnimation(layoutanimation);
                binding.maximizewindow.setVisibility(View.VISIBLE);
            }
        });

        binding.maximizewindow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutanimation = AnimationUtils.loadAnimation(TrackArrive.this, R.anim.slidegoup);
                binding.trackview.startAnimation(layoutanimation);
                binding.maximizewindow.setVisibility(View.GONE);
                binding.trackview.setVisibility(View.VISIBLE);
                binding.userview.setVisibility(View.VISIBLE);
            }
        });

        binding.navigate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appUtils.navigationtogooglemap(TrackArrive.this, sessionManager.getLatitude(), sessionManager.getLongitude(), object.getResponse().getUser_profile().getPickup_lat(), object.getResponse().getUser_profile().getPickup_lon());
            }
        });

        binding.calllayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HashMap<String, String> jsonParams = new HashMap<String, String>();
                jsonParams.put("ride_id", object.getResponse().getUser_profile().getRide_id());
                jsonParams.put("user_type", "driver");
                trackArriveViewModel.callsuppport(jsonParams);
                trackArriveViewModel.getPhonemasking().observe(TrackArrive.this, new Observer<String>() {
                    @Override
                    public void onChanged(@Nullable String response) {
                        try {
                            JSONObject object = new JSONObject(response);
                            String Str_status = object.getString("status");
                            String SResponse = object.getString("response");
                            if (Str_status.equalsIgnoreCase("1")) {
                                alert(getResources().getString(R.string.action_success), SResponse);
                            } else {
                                alert(getResources().getString(R.string.action_error), SResponse);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        binding.cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final PkDialog mDialog = new PkDialog(TrackArrive.this);
                mDialog.setDialogTitle(getResources().getString(R.string.tripcancellabel));
                mDialog.setDialogMessage(getResources().getString(R.string.doyouwanttocanceltrip));
                mDialog.setPositiveButton(getResources().getString(R.string.action_yes), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.showprogress(true);
                        mDialog.clickenabeled(false);
                        HashMap<String, String> jsonParams = new HashMap<String, String>();
                        jsonParams.put("driver_id", sDriveID);
                        jsonParams.put("ride_id", object.getResponse().getUser_profile().getRide_id());
                        trackArriveViewModel.canceltripreasons(jsonParams);
                        trackArriveViewModel.getCancelresons().observe(TrackArrive.this, new Observer<String>() {
                            @Override
                            public void onChanged(@Nullable String response) {
                                mDialog.showprogress(false);
                                mDialog.clickenabeled(true);
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    Intent passIntent = new Intent(TrackArrive.this, TrackRideCancelTrip.class);
                                    passIntent.putExtra("Reason", jsonObject.toString());
                                    passIntent.putExtra("driverImage", driverimage);
                                    passIntent.putExtra("RideID", object.getResponse().getUser_profile().getRide_id());
                                    startActivity(passIntent);
                                    mDialog.dismiss();
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                    }
                });
                mDialog.setNegativeButton(getResources().getString(R.string.action_no), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mDialog.dismiss();
                    }
                });
                mDialog.show();
            }
        });

        binding.taptoarrive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final PkDialog mDialog = new PkDialog(TrackArrive.this);
                mDialog.setDialogTitle(getResources().getString(R.string.arrivelabel));
                mDialog.setDialogMessage(getResources().getString(R.string.doyouwanttoarrive));
                mDialog.setPositiveButton(getResources().getString(R.string.action_yes), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mDialog.dismiss();
                        taptoarrive();
                    }
                });
                mDialog.setNegativeButton(getResources().getString(R.string.action_no), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mDialog.dismiss();
                    }
                });
                mDialog.show();
            }
        });
    }

    private void alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(TrackArrive.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void taptoarrive() {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriveID);
        jsonParams.put("ride_id", object.getResponse().getUser_profile().getRide_id());
        trackArriveViewModel.arriveapihit(jsonParams);
        trackArriveViewModel.getArriverespons().observe(TrackArrive.this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Type type = new TypeToken<continuetrippojo>() {
                    }.getType();
                    continuetrippojo pojo = new GsonBuilder().create().fromJson(jsonObject.toString(), type);
                    if (!pojo.getStatus().equalsIgnoreCase("1")) {
                        appUtils.AlertError(TrackArrive.this, getResources().getString(R.string.action_error), jsonObject.getString("response"));
                    } else {
                        sessionManager.setContinuetripjson(jsonObject.toString());
                        startActivity(new Intent(TrackArrive.this, StartTrip.class));
                        finish();
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case OVERLAY_PERMISSION_REQ_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (!Settings.canDrawOverlays(TrackArrive.this)) {
                            AppUtils.toastprint(TrackArrive.this, "SYSTEM_ALERT_WINDOW permission not granted...");
                        }
                    }
                }
        }
    }

    private void initializeMap() {
        if (googleMap == null) {
            mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.onlinemap);
            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap Map) {
                    googleMap = Map;
                    googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getApplicationContext(), R.raw.style));
                    googleMap.getUiSettings().setRotateGesturesEnabled(true);
                    googleMap.getUiSettings().setCompassEnabled(true);

                }
            });
        }

        SmartLocation.with(this).location()
                .start(new OnLocationUpdatedListener() {
                    @Override
                    public void onLocationUpdated(Location location) {
                        sessionManager.setLatitude(String.valueOf(location.getLatitude()));
                        sessionManager.setLongitude(String.valueOf(location.getLongitude()));
                        onMessageEvent(location);
                    }
                });
    }


    private void setResponse() {
        trackArriveViewModel.getContinuetripresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                        sessionManager.setContinuetripjson(jsonObject.toString());
                        Type type = new TypeToken<continuetrippojo>() {
                        }.getType();
                        object = new GsonBuilder().create().fromJson(jsonObject.toString(), type);
                        if (object.getResponse().getUser_profile().getRide_status().toLowerCase().equalsIgnoreCase("cancelled")) {
                            final PkDialog mDialog = new PkDialog(TrackArrive.this);
                            mDialog.setDialogTitle(getResources().getString(R.string.tripcancellabel));
                            mDialog.setDialogMessage("Trip cancelled already");
                            mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    sessionManager.setisride("");
                                    mDialog.dismiss();
                                    startActivity(new Intent(TrackArrive.this, Dashboard_constrain.class));
                                    finish();
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                }
                            });
                            mDialog.show();
                        } else if (object.getResponse().getUser_profile().getRide_status().toLowerCase().equalsIgnoreCase("completed")) {
                            if (object.getResponse().getUser_profile().getDriver_rating().equalsIgnoreCase("1")) {
                                startActivity(new Intent(TrackArrive.this, Dashboard_constrain.class));
                                finish();
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            } else {
                                Intent intent1 = new Intent(TrackArrive.this, FareSummaryConstrain.class);
                                intent1.putExtra("rideId", object.getResponse().getUser_profile().getRide_id());
                                startActivity(intent1);
                                finish();
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            }
                        } else if (object.getResponse().getUser_profile().getRide_status().toLowerCase().equalsIgnoreCase("finished")) {
                            if (object.getResponse().getUser_profile().getPayment_type().toLowerCase().equalsIgnoreCase("wallet")) {
                                Intent intent = new Intent(TrackArrive.this, Cashpayment.class);
                                intent.putExtra("rideId", object.getResponse().getUser_profile().getRide_id());
                                startActivity(intent);
                                finish();
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            } else if (object.getResponse().getUser_profile().getPayment_type().toLowerCase().equalsIgnoreCase("cash")) {
                                Intent intent = new Intent(TrackArrive.this, Cashpayment.class);
                                intent.putExtra("rideId", object.getResponse().getUser_profile().getRide_id());
                                startActivity(intent);
                                finish();
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            }

                        } else {
                            if (object.getResponse().getUser_profile().getMode().toLowerCase().equalsIgnoreCase("oneway")) {
                                if (object.getResponse().getUser_profile().getRide_status().toLowerCase().equalsIgnoreCase("confirmed")) {
                                    mapRunnable.run();
                                    trackArriveViewModel.passresponsetomodel(object);
                                    setcontinureresponse(object);
                                } else if (object.getResponse().getUser_profile().getRide_status().toLowerCase().equalsIgnoreCase("arrived")) {
                                    startActivity(new Intent(TrackArrive.this, StartTrip.class));
                                    finish();
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                } else if (object.getResponse().getUser_profile().getRide_status().toLowerCase().equalsIgnoreCase("begin") || object.getResponse().getUser_profile().getRide_status().toLowerCase().equalsIgnoreCase("onride")) {
                                    startActivity(new Intent(TrackArrive.this, TrackPage.class));
                                    finish();
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                } else if (object.getResponse().getUser_profile().getRide_status().toLowerCase().equalsIgnoreCase("finished")) {
                                    if (object.getResponse().getUser_profile().getContinue_trip().toLowerCase().equalsIgnoreCase("no")) {
                                        sessionManager.setisride("");
                                        if (object.getResponse().getUser_profile().getDriver_rating().equalsIgnoreCase("1")) {
                                            startActivity(new Intent(TrackArrive.this, Dashboard_constrain.class));
                                            finish();
                                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                        } else {
                                            Intent intent1 = new Intent(TrackArrive.this, FareSummaryConstrain.class);
                                            intent1.putExtra("rideId", object.getResponse().getUser_profile().getRide_id());
                                            startActivity(intent1);
                                            finish();
                                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                        }
                                    } else {
                                        if (object.getResponse().getUser_profile().getPayment_type().toLowerCase().equalsIgnoreCase("cash")) {
                                            Intent intent = new Intent(TrackArrive.this, Cashpayment.class);
                                            intent.putExtra("rideId", object.getResponse().getUser_profile().getRide_id());
                                            startActivity(intent);
                                            finish();
                                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                        }
                                    }
                                } else {
                                    sessionManager.setisride("");
                                    startActivity(new Intent(TrackArrive.this, Dashboard_constrain.class));
                                    finish();
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                }
                            } else if (object.getResponse().getUser_profile().getMode().toLowerCase().equalsIgnoreCase("return")) {
                                if (object.getResponse().getUser_profile().getRide_status().toLowerCase().equalsIgnoreCase("confirmed")) {
                                    mapRunnable.run();
                                    trackArriveViewModel.passresponsetomodel(object);
                                    setcontinureresponse(object);
                                } else if (object.getResponse().getUser_profile().getRide_status().toLowerCase().equalsIgnoreCase("arrived")) {
                                    startActivity(new Intent(TrackArrive.this, StartTrip.class));
                                    finish();
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                } else if (object.getResponse().getUser_profile().getRide_status().toLowerCase().equalsIgnoreCase("begin") || object.getResponse().getUser_profile().getRide_status().toLowerCase().equalsIgnoreCase("onride")) {
                                    if (object.getResponse().getUser_profile().getReturn_mode().toLowerCase().equalsIgnoreCase("drop")) {
                                        startActivity(new Intent(TrackArrive.this, TrackPage.class));
                                        finish();
                                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                    } else if (object.getResponse().getUser_profile().getReturn_mode().toLowerCase().equalsIgnoreCase("start")) {
                                        startActivity(new Intent(TrackArrive.this, StartTrip.class));
                                        finish();
                                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                    } else if (object.getResponse().getUser_profile().getReturn_mode().toLowerCase().equalsIgnoreCase("over")) {
                                        startActivity(new Intent(TrackArrive.this, TrackPage.class));
                                        finish();
                                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                    }
                                }
                            } else if (object.getResponse().getUser_profile().getMode().toLowerCase().equalsIgnoreCase("multistop")) {
                                if (object.getResponse().getUser_profile().getRide_status().toLowerCase().equalsIgnoreCase("confirmed")) {
                                    mapRunnable.run();
                                    trackArriveViewModel.passresponsetomodel(object);
                                    setcontinureresponse(object);
                                } else if (object.getResponse().getUser_profile().getRide_status().toLowerCase().equalsIgnoreCase("arrived")) {
                                    startActivity(new Intent(TrackArrive.this, StartTrip.class));
                                    finish();
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                } else if (object.getResponse().getUser_profile().getRide_status().toLowerCase().equalsIgnoreCase("onride")) {
                                    startActivity(new Intent(TrackArrive.this, TrackPage.class));
                                    finish();
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                } else if (object.getResponse().getUser_profile().getRide_status().toLowerCase().equalsIgnoreCase("finished")) {
                                    if (object.getResponse().getUser_profile().getContinue_trip().toLowerCase().equalsIgnoreCase("no")) {
                                        sessionManager.setisride("");
                                        if (object.getResponse().getUser_profile().getDriver_rating().equalsIgnoreCase("1")) {
                                            startActivity(new Intent(TrackArrive.this, Dashboard_constrain.class));
                                            finish();
                                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                        } else {
                                            Intent intent1 = new Intent(TrackArrive.this, FareSummaryConstrain.class);
                                            intent1.putExtra("rideId", object.getResponse().getUser_profile().getRide_id());
                                            startActivity(intent1);
                                            finish();
                                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                        }
                                    } else {
                                        if (object.getResponse().getUser_profile().getPayment_type().toLowerCase().equalsIgnoreCase("cash")) {
                                            Intent intent = new Intent(TrackArrive.this, Cashpayment.class);
                                            intent.putExtra("rideId", object.getResponse().getUser_profile().getRide_id());
                                            startActivity(intent);
                                            finish();
                                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        appUtils.AlertError(TrackArrive.this, getResources().getString(R.string.action_error), jsonObject.getString("response"));
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                HashMap<String, String> jsonParams = new HashMap<String, String>();
                                jsonParams.put("driver_id", sDriveID);
                                jsonParams.put("ride_id", getIntent().getStringExtra("rideid"));

                                Intent intents = new Intent(TrackArrive.this, RidedbApiIntentService.class);
                                intents.putExtra("driverid", sDriveID);
                                intents.putExtra("isdrivercancel", "false");
                                intents.putExtra("rideid", getIntent().getStringExtra("rideid"));
                                intents.putExtra("params", jsonParams);
                                intents.putExtra("url", Iconstant.complete_trip_detail_url);
                                startService(intents);

                                startActivity(new Intent(TrackArrive.this, Dashboard_constrain.class));
                                finish();
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            }
                        }, 1500);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setcontinureresponse(continuetrippojo object) {
        if (object.getResponse().getUser_profile().getRide_status().toLowerCase().equalsIgnoreCase("cancelled")) {
            final PkDialog mDialog = new PkDialog(TrackArrive.this);
            mDialog.setDialogTitle(getResources().getString(R.string.tripcancellabel));
            mDialog.setDialogMessage("Trip cancelled already");
            mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sessionManager.setisride("");
                    mDialog.dismiss();
                    startActivity(new Intent(TrackArrive.this, OnlinepageConstrain.class));
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });
            mDialog.show();
        } else {
            globalbearing = appUtils.findbearing(Double.parseDouble(currentlat), Double.parseDouble(currentlon), Double.parseDouble(object.getResponse().getUser_profile().getPickup_lat()), Double.parseDouble(object.getResponse().getUser_profile().getPickup_lon()));
            routefromlatlng = new LatLng(Double.parseDouble(currentlat), Double.parseDouble(currentlon));
            routetolatlon = new LatLng(Double.parseDouble(object.getResponse().getUser_profile().getPickup_lat()), Double.parseDouble(object.getResponse().getUser_profile().getPickup_lon()));
            AppUtils.setImageViewd(TrackArrive.this, sessionManager.getcategoryicon(), binding.arrivecaricon);
            AppUtils.setImageView(TrackArrive.this, object.getResponse().getUser_profile().getUser_image(), binding.trackusericonnormalbooking);
            binding.trackusername.setText(object.getResponse().getUser_profile().getUser_name());
            binding.pickuplocationvalue.setText(object.getResponse().getUser_profile().getPickup_location());
            binding.etaminutes.setText("IN " + object.getResponse().getUser_profile().getEta_arrival());
            binding.kmaway.setText(object.getResponse().getUser_profile().getApprox_dist() + " AWAY");
            drawroute(routefromlatlng, routetolatlon);
        }
    }


    public void onMessageEvent(final Location myLocation) {
        try {
            currentlat = String.valueOf(myLocation.getLatitude());
            currentlon = String.valueOf(myLocation.getLongitude());
            if (currentlat.equalsIgnoreCase("")) {
                currentlat = String.valueOf(sessionManager.getLatitude());
                currentlon = String.valueOf(sessionManager.getLongitude());
            }
            final LatLng latLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
            if (oldLatLng == null) {
                oldLatLng = latLng;
            }
            newLatLng = latLng;
            if (mLatLngInterpolator == null) {
                mLatLngInterpolator = new LatLngInterpolator.Linear();
            }
            oldLocation = new Location("");
            oldLocation.setLatitude(oldLatLng.latitude);
            oldLocation.setLongitude(oldLatLng.longitude);
            final LatLng changelat = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
            changelatLocations = new Location("");
            changelatLocations.setLatitude(changelat.latitude);
            changelatLocations.setLongitude(changelat.longitude);
            final float bearingValue = oldLocation.bearingTo(changelatLocations);
            myMovingDistance = oldLocation.distanceTo(changelatLocations);

            if (myMovingDistance > 40) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (googleMap != null) {
                            if (carMarker != null) {
                                appUtils.movemarker(myLocation, bearingValue, carMarker, googleMap, latLng, mLatLngInterpolator);
                            } else {
                                carMarker = appUtils.Addmarker(googleMap, latLng, myLocation, carIcon);
                            }
                        }
                        oldLatLng = newLatLng;
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Update location
    private Runnable mapRunnable = new Runnable() {
        @Override
        public void run() {
            HashMap<String, String> user = sessionManager.getUserDetails();
            sDriveID = user.get(SessionManager.KEY_DRIVERID);
            HashMap<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("driver_id", sDriveID);
            jsonParams.put("latitude", currentlat);
            jsonParams.put("longitude", currentlon);
            Intent intents = new Intent(TrackArrive.this, OnlineupdatelocationtoserverService.class);
            intents.putExtra("params", jsonParams);
            intents.putExtra("url", Iconstant.updateDriverLocation);
            startService(intents);
            mapHandler.postDelayed(this, 45000);
        }
    };


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mapRunnable != null) {
            mapHandler.removeCallbacks(mapRunnable);
        }
        if(EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
        SmartLocation.with(this).location().stop();
        stopService(new Intent(TrackArrive.this, OnlineupdatelocationtoserverService.class));
    }

    private void drawroute(LatLng routefromlatlng, LatLng routetolatlon) {
        addWayPointPoint(routefromlatlng, routetolatlon, object.getResponse().getUser_profile().getMultistop_array());
    }


    private void addWayPointPoint(LatLng start, LatLng end, ArrayList<continuetrippojo.Response.User_profile.Multistop_array> mMultipleDropLatLng) {
        if (googleMap != null) {
            googleMap.clear();
            wayPointList = new ArrayList<LatLng>();
            wayPointList.add(start);

            wayPointList.add(end);
            Routing routing = new Routing.Builder()
                    .travelMode(AbstractRouting.TravelMode.DRIVING)
                    .withListener(TrackArrive.this)
                    .alternativeRoutes(true)
                    .key(Iconstant.place_search_key)
                    .waypoints(wayPointList)
                    .build();
            routing.execute();
        }

    }

    @Override
    public void onRoutingFailure(RouteException e) {
        appUtils.AlertError(this, getResources().getString(R.string.action_error), e.getMessage());
    }

    @Override
    public void onRoutingStart() {
    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int i) {
        try {
            List<Polyline> polyLines = new ArrayList<>();
            PolylineOptions polyOptions = new PolylineOptions();
            polyOptions.color(getResources().getColor(R.color.btn_green_color));
            polyOptions.width(8);
            polyOptions.addAll(route.get(0).getPoints());
            Polyline polyline = googleMap.addPolyline(polyOptions);
            polyLines.add(polyline);
            wayPointBuilder = new LatLngBounds.Builder();
            for (int k = 0; k <= route.get(0).getPoints().size() - 1; k++) {
                wayPointBuilder.include(route.get(0).getPoints().get(k));
            }

            if (object.getResponse().getUser_profile().getRide_status().toLowerCase().equalsIgnoreCase("confirmed")) {
                if (object.getResponse().getUser_profile().getMode().toLowerCase().equalsIgnoreCase("oneway")) {
                    googleMap.addMarker(new MarkerOptions()
                            .position(routefromlatlng)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.green)));
                    googleMap.addMarker(new MarkerOptions()
                            .position(routetolatlon)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.red)));
                } else if (object.getResponse().getUser_profile().getMode().toLowerCase().equalsIgnoreCase("return")) {
                    googleMap.addMarker(new MarkerOptions()
                            .position(routefromlatlng)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.green)));
                    googleMap.addMarker(new MarkerOptions()
                            .position(routetolatlon)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.red)));
                } else if (object.getResponse().getUser_profile().getMode().toLowerCase().equalsIgnoreCase("multistop")) {
                    googleMap.addMarker(new MarkerOptions()
                            .position(routefromlatlng)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.green)));
                    googleMap.addMarker(new MarkerOptions()
                            .position(routetolatlon)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.red)));
                }
            }

            if (wayPointBuilder != null) {
                LatLngBounds bounds = wayPointBuilder.build();
                int padding, paddingH, paddingW;
                final View mapview = mapFragment.getView();
                float maxX = mapview.getMeasuredWidth();
                float maxY = mapview.getMeasuredHeight();

                DisplayMetrics displayMetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                int height = displayMetrics.heightPixels;
                int width = displayMetrics.widthPixels;
                float percentageH = 50.0f;
                float percentageW = 80.0f;
                paddingH = (int) (maxY * (percentageH / 100.0f));
                paddingW = (int) (maxX * (percentageW / 100.0f));
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, paddingW, paddingH, 100);
                googleMap.animateCamera(cu, new GoogleMap.CancelableCallback() {
                    @Override
                    public void onFinish() {
                        if (globalbearing <= -35 && globalbearing >= -135) {
                            appUtils.updateCameraBearing(googleMap, -90);
                        } else if (globalbearing >= 35 && globalbearing <= 135) {
                            appUtils.updateCameraBearing(googleMap, -90);
                        }
                    }

                    @Override
                    public void onCancel() {

                    }
                });
            }
        } catch (NullPointerException ee) {
        } catch (Exception e) {
        }
    }

    @Override
    public void onRoutingCancelled() {
        appUtils.AlertError(this, getResources().getString(R.string.action_error), "Route draw cancelled");
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Log.v("TRIMMEMORY", " " + level);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
        getchatunreadcount(getApplicationContext());

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void doThis(ChatIntentServiceResult intentServiceResult )
    {
        getchatunreadcount(getApplicationContext());
    }


    private void getchatunreadcount(Context mContext)
    {
        int unreadcount=0;
        DbHelper mHelper= null;
        SQLiteDatabase dataBase = null;
        mHelper= new DbHelper(mContext);
        ContentValues values  = new ContentValues();
        values.put(DbHelper.status,"1");
        dataBase = mHelper.getWritableDatabase();
        Cursor mCursor = dataBase.rawQuery("SELECT * FROM " + DbHelper.TABLE_NAME+" WHERE status='0'", null);
        if (mCursor.moveToFirst()) {
            do {
                unreadcount++;
            } while (mCursor.moveToNext());
        }
        mCursor.close();
        if(unreadcount == 0)
        {
            binding.countofunread.setText("");
        }
        else
        {
            binding.countofunread.setText(""+unreadcount);
        }

    }




}
