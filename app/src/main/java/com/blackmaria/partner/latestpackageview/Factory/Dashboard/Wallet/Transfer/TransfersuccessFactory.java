package com.blackmaria.partner.latestpackageview.Factory.Dashboard.Wallet.Transfer;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet.transfer.TransfersuccessViewModel;


public class TransfersuccessFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public TransfersuccessFactory(Activity context) {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new TransfersuccessViewModel(context);
    }
}
