package com.blackmaria.partner.latestpackageview.Adapter;

import android.content.Context;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.latestpackageview.Model.incentivesdasboardpojo;

import java.util.ArrayList;

public class Incentiveadapter extends BaseAdapter {
    private Context context;
    private LayoutInflater inflter;
    private ArrayList<incentivesdasboardpojo.Response.Incetives_count> incetives_count;

    public Incentiveadapter(Context applicationContext, ArrayList<incentivesdasboardpojo.Response.Incetives_count> incetives_count) {
        this.context = applicationContext;
        inflter = (LayoutInflater.from(applicationContext));
        this.incetives_count = incetives_count;
    }

    @Override
    public int getCount() {
        return incetives_count.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.incentivesadapterlayout, null);
        TextView incentivestext = view.findViewById(R.id.incentivestext);
        CardView milage = view.findViewById(R.id.milage);
        incentivestext.setText(incetives_count.get(i).getIncetives_type());
        if ((i % 2) == 0) {
            //Is even
            incentivestext.setTextColor(context.getResources().getColor(R.color.black));
            milage.setCardBackgroundColor(context.getResources().getColor(R.color.white));
        } else {
            //Is odd
            incentivestext.setTextColor(context.getResources().getColor(R.color.white));
            milage.setCardBackgroundColor(context.getResources().getColor(R.color.myincentivesweekly));
        }
        return view;
    }
}
