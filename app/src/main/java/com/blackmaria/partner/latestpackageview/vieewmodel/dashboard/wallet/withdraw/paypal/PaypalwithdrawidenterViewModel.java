package com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet.withdraw.paypal;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.ViewModel;

import com.blackmaria.partner.databinding.ActivityPaypalwithdrawidenterconstrainBinding;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;

public class PaypalwithdrawidenterViewModel extends ViewModel implements ApIServices.completelisner {
    private Activity context;
    private Dialog dialog;
    private ActivityPaypalwithdrawidenterconstrainBinding binding;

    public PaypalwithdrawidenterViewModel(Activity context) {
        this.context = context;
    }

    @Override
    public void sucessresponse(String val) {

    }

    @Override
    public void errorreponse() {

    }

    @Override
    public void jsonexception() {

    }

    public void setIds(ActivityPaypalwithdrawidenterconstrainBinding binding) {
        this.binding = binding;
    }

    public void back() {
        context.onBackPressed();
    }

}
