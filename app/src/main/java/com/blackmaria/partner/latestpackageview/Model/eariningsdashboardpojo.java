package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class eariningsdashboardpojo implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }

    public class Response {
        @SerializedName("today_earning")
        private String today_earning;
        @SerializedName("week_earning")
        private String week_earning;
        @SerializedName("month_earning")
        private String month_earning;
        @SerializedName("today_rides")
        private String today_rides;
        @SerializedName("week_rides")
        private String week_rides;
        @SerializedName("month_rides")
        private String month_rides;
        @SerializedName("currency")
        private String currency;
        @SerializedName("date")
        private String date;

        public String getReferal_view_btn() {
            return referal_view_btn;
        }

        public void setReferal_view_btn(String referal_view_btn) {
            this.referal_view_btn = referal_view_btn;
        }

        @SerializedName("referal_view_btn")
        private String referal_view_btn;

        @SerializedName("available_amount")
        private String available_amount;

        @SerializedName("total_earning_amount")
        private String total_earning_amount;

        public ArrayList<Ref_info> getRef_info() {
            return ref_info;
        }

        public void setRef_info(ArrayList<Ref_info> ref_info) {
            this.ref_info = ref_info;
        }
        @SerializedName("ref_info")
        ArrayList< Ref_info > ref_info = new ArrayList < Ref_info > ();
        @SerializedName("rates")
        private String rates;
        @SerializedName("referral_count")
        private String referral_count;
        @SerializedName("referral_earning")
        private String referral_earning;
        @SerializedName("proceed_status")
        private String proceed_status;
        @SerializedName("withdraw_status")
        private String withdraw_status;
        @SerializedName("today_payment")
        Today_payment Today_paymentObject;
        @SerializedName("week_payment")
        Week_payment Week_paymentObject;
        @SerializedName("month_payment")
        Month_payment Month_paymentObject;
        @SerializedName("withdraw_error")
        private String withdraw_error;
        @SerializedName("error_message")
        private String error_message;


        @SerializedName("today_payment_ride_count")
        Today_payment_ride Today_paymentObject_ride;
        @SerializedName("week_payment_ride_count")
        Week_payment_ride Week_paymentObject_ride;
        @SerializedName("month_payment_ride_count")
        Month_payment_ride Month_paymentObject_ride;

        public class Ref_info{
            @SerializedName("driver_name")
            private String driver_name;
            @SerializedName("driver_image")
            private String driver_image;
            // Getter Methods 

            public String getDriver_name() {
                return driver_name;
            }

            public String getDriver_image() {
                return driver_image;
            }

            // Setter Methods 

            public void setDriver_name(String driver_name) {
                this.driver_name = driver_name;
            }

            public void setDriver_image(String driver_image) {
                this.driver_image = driver_image;
            }
        }

        // Getter Methods

        public String getToday_earning() {
            return today_earning;
        }

        public String getWeek_earning() {
            return week_earning;
        }

        public String getMonth_earning() {
            return month_earning;
        }

        public String getToday_rides() {
            return today_rides;
        }

        public String getWeek_rides() {
            return week_rides;
        }

        public String getMonth_rides() {
            return month_rides;
        }

        public String getCurrency() {
            return currency;
        }

        public String getDate() {
            return date;
        }

        public String getAvailable_amount() {
            return available_amount;
        }

        public String getTotal_earning_amount() {
            return total_earning_amount;
        }

        public String getRates() {
            return rates;
        }

        public String getReferral_count() {
            return referral_count;
        }

        public String getReferral_earning() {
            return referral_earning;
        }

        public String getProceed_status() {
            return proceed_status;
        }

        public String getWithdraw_status() {
            return withdraw_status;
        }

        public Today_payment getToday_payment() {
            return Today_paymentObject;
        }

        public Week_payment getWeek_payment() {
            return Week_paymentObject;
        }

        public Month_payment getMonth_payment() {
            return Month_paymentObject;
        }





        public Today_payment_ride getToday_payment_ride() {
            return Today_paymentObject_ride;
        }

        public Week_payment_ride getWeek_payment_ride() {
            return Week_paymentObject_ride;
        }

        public Month_payment_ride getMonth_payment_ride() {
            return Month_paymentObject_ride;
        }




        public String getWithdraw_error() {
            return withdraw_error;
        }

        public String getError_message() {
            return error_message;
        }

        // Setter Methods

        public void setToday_earning(String today_earning) {
            this.today_earning = today_earning;
        }

        public void setWeek_earning(String week_earning) {
            this.week_earning = week_earning;
        }

        public void setMonth_earning(String month_earning) {
            this.month_earning = month_earning;
        }

        public void setToday_rides(String today_rides) {
            this.today_rides = today_rides;
        }

        public void setWeek_rides(String week_rides) {
            this.week_rides = week_rides;
        }

        public void setMonth_rides(String month_rides) {
            this.month_rides = month_rides;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public void setAvailable_amount(String available_amount) {
            this.available_amount = available_amount;
        }
        public void setTotal_earning_amount(String total_earning_amount) {
            this.total_earning_amount = total_earning_amount;
        }


        public void setRates(String rates) {
            this.rates = rates;
        }

        public void setReferral_count(String referral_count) {
            this.referral_count = referral_count;
        }

        public void setReferral_earning(String referral_earning) {
            this.referral_earning = referral_earning;
        }

        public void setProceed_status(String proceed_status) {
            this.proceed_status = proceed_status;
        }

        public void setWithdraw_status(String withdraw_status) {
            this.withdraw_status = withdraw_status;
        }

        public void setToday_payment(Today_payment today_paymentObject) {
            this.Today_paymentObject = today_paymentObject;
        }

        public void setWeek_payment(Week_payment week_paymentObject) {
            this.Week_paymentObject = week_paymentObject;
        }

        public void setMonth_payment(Month_payment month_paymentObject) {
            this.Month_paymentObject = month_paymentObject;
        }


        public void setToday_paymentObject_ride(Today_payment_ride today_paymentObject_ride) {
            this.Today_paymentObject_ride = today_paymentObject_ride;
        }

        public void setWeek_paymentObject_ride(Week_payment_ride week_paymentObject_ride) {
            this.Week_paymentObject_ride = week_paymentObject_ride;
        }

        public void setMonth_paymentObject_ride(Month_payment_ride month_paymentObject_ride) {
            this.Month_paymentObject_ride = month_paymentObject_ride;
        }

        public void setWithdraw_error(String withdraw_error) {
            this.withdraw_error = withdraw_error;
        }

        public void setError_message(String error_message) {
            this.error_message = error_message;
        }

        public class Month_payment {
            @SerializedName("cash")
            private String cash;
            @SerializedName("wallet")
            private String wallet;
            @SerializedName("card")
            private String card;


            // Getter Methods

            public String getCash() {
                return cash;
            }

            public String getWallet() {
                return wallet;
            }

            public String getCard() {
                return card;
            }

            // Setter Methods

            public void setCash(String cash) {
                this.cash = cash;
            }

            public void setWallet(String wallet) {
                this.wallet = wallet;
            }

            public void setCard(String card) {
                this.card = card;
            }


        }
        public class Week_payment {
            @SerializedName("cash")
            private String cash;
            @SerializedName("wallet")
            private String wallet;
            @SerializedName("card")
            private String card;


            // Getter Methods

            public String getCash() {
                return cash;
            }

            public String getWallet() {
                return wallet;
            }

            public String getCard() {
                return card;
            }

            // Setter Methods

            public void setCash(String cash) {
                this.cash = cash;
            }

            public void setWallet(String wallet) {
                this.wallet = wallet;
            }

            public void setCard(String card) {
                this.card = card;
            }


        }
        public class Today_payment {
            @SerializedName("cash")
            private String cash;
            @SerializedName("wallet")
            private String wallet;
            @SerializedName("card")
            private String card;


            // Getter Methods

            public String getCash() {
                return cash;
            }

            public String getWallet() {
                return wallet;
            }

            public String getCard() {
                return card;
            }

            // Setter Methods

            public void setCash(String cash) {
                this.cash = cash;
            }

            public void setWallet(String wallet) {
                this.wallet = wallet;
            }

            public void setCard(String card) {
                this.card = card;
            }
        }















        public class Month_payment_ride {
            @SerializedName("cash")
            private String cash;
            @SerializedName("wallet")
            private String wallet;
            @SerializedName("card")
            private String card;


            // Getter Methods

            public String getCash() {
                return cash;
            }

            public String getWallet() {
                return wallet;
            }

            public String getCard() {
                return card;
            }

            // Setter Methods

            public void setCash(String cash) {
                this.cash = cash;
            }

            public void setWallet(String wallet) {
                this.wallet = wallet;
            }

            public void setCard(String card) {
                this.card = card;
            }


        }
        public class Week_payment_ride {
            @SerializedName("cash")
            private String cash;
            @SerializedName("wallet")
            private String wallet;
            @SerializedName("card")
            private String card;


            // Getter Methods

            public String getCash() {
                return cash;
            }

            public String getWallet() {
                return wallet;
            }

            public String getCard() {
                return card;
            }

            // Setter Methods

            public void setCash(String cash) {
                this.cash = cash;
            }

            public void setWallet(String wallet) {
                this.wallet = wallet;
            }

            public void setCard(String card) {
                this.card = card;
            }


        }
        public class Today_payment_ride {
            @SerializedName("cash")
            private String cash;
            @SerializedName("wallet")
            private String wallet;
            @SerializedName("card")
            private String card;


            // Getter Methods

            public String getCash() {
                return cash;
            }

            public String getWallet() {
                return wallet;
            }

            public String getCard() {
                return card;
            }

            // Setter Methods

            public void setCash(String cash) {
                this.cash = cash;
            }

            public void setWallet(String wallet) {
                this.wallet = wallet;
            }

            public void setCard(String card) {
                this.card = card;
            }
        }

    }
}




