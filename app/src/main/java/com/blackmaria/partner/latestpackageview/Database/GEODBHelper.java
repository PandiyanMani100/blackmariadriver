package com.blackmaria.partner.latestpackageview.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.blackmaria.partner.Utils.SessionManager;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

public class GEODBHelper  {

    private String TAG = GEODBHelper.class.getSimpleName();
    private DataBaseHelper myDBHelper;
    private SQLiteDatabase myDataBase;
    private Context myContext;
    private int DATABASE_VERSION = 1;
    private SessionManager manager;
    String LATLONG_INFO_TABLE_NAME = "geolatlong";
    String DATABASE_NAME = "geo_loc.db";
    public static GEODBHelper instance=null;

  //  private static final String CREATE_TABLE_TODO = "CREATE TABLE IF NOT EXISTS " + RIDE_STATUS_TABLE + "(" + "status" + " INTEGER" + ")";
    private  final String CREATE_TABLE_LAT_LONG = "CREATE TABLE IF NOT EXISTS " + LATLONG_INFO_TABLE_NAME + "(" + "ride_id" + " INTEGER," + "geo_lat" + " TEXT," + "geo_long" + " TEXT," + "geo_time" + " TEXT" + ")";

    public GEODBHelper(Context context) {
        myContext = context;
        myDBHelper = new DataBaseHelper(context);
        myDataBase = myDBHelper.getWritableDatabase();
        manager = SessionManager.getInstance(context);
        open();
    }

    public static GEODBHelper getInstance(Context context){
        if(instance == null){
            instance  = new GEODBHelper(context);
        }
        return instance;
    }


    public class DataBaseHelper extends SQLiteOpenHelper {

        public DataBaseHelper(Context context) {

            super(context, DATABASE_NAME, null, DATABASE_VERSION);

        }

        @Override
        public void onCreate(SQLiteDatabase db) {
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }


    /**
     * Function to make DB as readable and writable
     */
    private void open() {

        try {
            if (myDataBase == null) {
                myDataBase = myDBHelper.getWritableDatabase();
            }
    //        myDataBase.execSQL(CREATE_TABLE_TODO);
            myDataBase.execSQL(CREATE_TABLE_LAT_LONG);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Function to Close the database
     */
    public void close() {

        try {
            Log.d(TAG, "mySQLiteDatabase Closed");

            // ---Closing the database---
            myDBHelper.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Insert the lat and long in the table
     *
     * @param aLatitude
     * @param aLongtitude
     * @param aCurrentDate
     */
    public void insertLatLong(String rideid, double aLatitude, double aLongtitude, String aCurrentDate) {
        try {
            Log.e("Insert", "Store latlong  Info in DB");
            ContentValues values = new ContentValues();
            values.put("ride_id", rideid);
            values.put("geo_lat", aLatitude);
            values.put("geo_long", aLongtitude);
            values.put("geo_time", aCurrentDate);
            myDataBase.insert(LATLONG_INFO_TABLE_NAME
                    , null, values);
        } catch (android.database.SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Insert the lat and long in the table
     */
    public void insertDriverStatus(String status) {
        try {
            manager.createStatus(status);
        } catch (android.database.SQLException e) {
            e.printStackTrace();
        }
    }
    public void insertRide_id(String id) {
        try {
            manager.createid(id);
        } catch (android.database.SQLException e) {
            e.printStackTrace();
        }
    }
    public void insertuser_id(String id) {
        try {
            manager.createuserid(id);
        } catch (android.database.SQLException e) {
            e.printStackTrace();
        }
    }
    public String retriveStatus() {
        String status = manager.getUserDetails().get(SessionManager.KEY_STATUS);
        return status;
    }


    public String retriveid() {
        String status = manager.getUserDetails().get(SessionManager.KEY_id);
        return status;
    }

    public String retriveuserid() {
        String status = manager.getUserDetails().get(SessionManager.KEY_USER_ID);
        return status;
    }


    public ArrayList<String> getData() {
        String selectQuery = "SELECT  * FROM " + LATLONG_INFO_TABLE_NAME;
        SQLiteDatabase db = myDataBase;
        Cursor cursor = db.rawQuery(selectQuery, null);
        String[] data = null;
        ArrayList<String> strings = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {

                String geolong = cursor.getString(0);
                String geolat = cursor.getString(1);
                String geo_time = cursor.getString(2);
                System.out.println("Driver time Lat Long ---------------------" + geo_time + "-------------------" + geolat + "----------------" + geolong);
                strings.add(geolat + ";" + geolong + ";" + geo_time);
            } while (cursor.moveToNext());
        }
        db.close();
        return strings;
    }

    public ArrayList<String> getDataEndTrip(String rideID) {
        String rideIDStatic = "";
        String selectQuery = "SELECT  * FROM " + LATLONG_INFO_TABLE_NAME + " WHERE ride_id = " + rideID;
        SQLiteDatabase db = myDataBase;
        ArrayList<String> endDataTrips = new ArrayList<>();
        if(db.isOpen()){
            Cursor cursor = db.rawQuery(selectQuery, null);
            String[] data = null;
            if (cursor.moveToFirst()) {
                do {
                    String rideid = cursor.getString(0);
                    String geolat = cursor.getString(1);
                    String geolong = cursor.getString(2);
                    String geo_time = cursor.getString(3);
                    System.out.println("Driver time Lat Long ---------------------" + geo_time + "-------------------" + geolat + "----------------" + geolong);
                    endDataTrips.add(geolat + ";" + geolong + ";" + geo_time);
                } while (cursor.moveToNext());
            }
       //     db.close();
            System.out.println(endDataTrips.toString());
        }

        return endDataTrips;
    }
    public ArrayList<LatLng> getDisEndTrip(String rideID) {
        String rideIDStatic = "";
        String selectQuery = "SELECT  * FROM " + LATLONG_INFO_TABLE_NAME + " WHERE ride_id = " + rideID;
        SQLiteDatabase db = myDataBase;
        ArrayList<LatLng> endDataTrips = new ArrayList<LatLng>();
        if(db.isOpen()){
            Cursor cursor = db.rawQuery(selectQuery, null);
            String[] data = null;
            if (cursor.moveToFirst()) {
                do {
                    String rideid = cursor.getString(0);
                    String geolat = cursor.getString(1);
                    String geolong = cursor.getString(2);
                    String geo_time = cursor.getString(3);
                    System.out.println("Driver time Lat Long ---------------------" + geo_time + "-------------------" + geolat + "----------------" + geolong);
                    endDataTrips.add(new LatLng(Double.parseDouble(geolat),Double.parseDouble(geolong)));
                } while (cursor.moveToNext());
            }
            //     db.close();
            System.out.println(endDataTrips.toString());
        }

        return endDataTrips;
    }

    public void cleartable(){
        SQLiteDatabase db = myDataBase;
        db.execSQL("delete from "+ LATLONG_INFO_TABLE_NAME);
    }

}
