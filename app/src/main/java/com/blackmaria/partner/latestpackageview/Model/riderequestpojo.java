package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class riderequestpojo implements Serializable {
    @SerializedName("message")
    private String message;
    @SerializedName("action")
    private String action;
    @SerializedName("key1")
    private String key1;
    @SerializedName("key2")
    private String key2;
    @SerializedName("key3")
    private String key3;
    @SerializedName("key4")
    private String key4;
    @SerializedName("key5")
    private String key5;

    public String getKey6() {
        return key6;
    }

    public void setKey6(String key6) {
        this.key6 = key6;
    }

    public String getKey7() {
        return key7;
    }

    public void setKey7(String key7) {
        this.key7 = key7;
    }

    @SerializedName("key6")
    private String key6;
    @SerializedName("key7")
    private String key7;

    public String getKey8() {
        return key8;
    }

    public void setKey8(String key8) {
        this.key8 = key8;
    }

    @SerializedName("key8")
    private String key8;

    // Getter Methods

    public String getMessage() {
        return message;
    }

    public String getAction() {
        return action;
    }

    public String getKey1() {
        return key1;
    }

    public String getKey2() {
        return key2;
    }

    public String getKey3() {
        return key3;
    }

    public String getKey4() {
        return key4;
    }

    public String getKey5() {
        return key5;
    }

    // Setter Methods

    public void setMessage(String message) {
        this.message = message;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public void setKey1(String key1) {
        this.key1 = key1;
    }

    public void setKey2(String key2) {
        this.key2 = key2;
    }

    public void setKey3(String key3) {
        this.key3 = key3;
    }

    public void setKey4(String key4) {
        this.key4 = key4;
    }

    public void setKey5(String key5) {
        this.key5 = key5;
    }
}
