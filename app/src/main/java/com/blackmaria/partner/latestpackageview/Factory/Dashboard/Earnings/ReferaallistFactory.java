package com.blackmaria.partner.latestpackageview.Factory.Dashboard.Earnings;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.earnings.ReferrallistViewModel;


public class ReferaallistFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public ReferaallistFactory(Activity context) {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new ReferrallistViewModel(context);
    }
}
