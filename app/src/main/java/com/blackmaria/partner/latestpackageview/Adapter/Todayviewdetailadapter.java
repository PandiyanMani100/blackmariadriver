package com.blackmaria.partner.latestpackageview.Adapter;

import android.content.Context;
import android.content.Intent;

import androidx.cardview.widget.CardView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.latestpackageview.Model.Rides;
import com.blackmaria.partner.latestpackageview.widgets.RoundedImageView;
import com.blackmaria.partner.latestpackageview.Model.todaytripviewdetailpojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Mytrips.TripSummaryConstrain;

import java.util.ArrayList;

public class Todayviewdetailadapter extends BaseAdapter {

    private ArrayList<Rides> data;
    private LayoutInflater mInflater;
    private Context context;
    AccessLanguagefromlocaldb db;


    public Todayviewdetailadapter(Context context, ArrayList<Rides> rides) {
        this.context = context;
        this.data = rides;
        mInflater = LayoutInflater.from(context);
        db = new AccessLanguagefromlocaldb(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    public class ViewHolder {
        private TextView Tv_date, Tv_distance, Tv_status, Tv_amount, time,miless,crn;
        private ImageView status_image;
        RelativeLayout viewall_list_layout;
        private RoundedImageView user_image_view;
        private CardView cardview_signup;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.ridelist_single_new, parent, false);
            holder = new ViewHolder();
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        holder.time = view.findViewById(R.id.time);
        holder.time.setText(db.getvalue("timess"));

        holder.miless = view.findViewById(R.id.miless);
        holder.miless.setText(db.getvalue("miless"));


        holder.crn = view.findViewById(R.id.crn);
        holder.crn.setText(db.getvalue("crn_noss"));


        holder.cardview_signup = view.findViewById(R.id.cardview_signup);
        holder.user_image_view = view.findViewById(R.id.user_image_view);
        holder.Tv_date = view.findViewById(R.id.ride_list_single_date_time_textview);
        holder.Tv_distance = view.findViewById(R.id.ride_list_single_distance_textview);
        holder.Tv_amount = view.findViewById(R.id.ride_list_single_amount);
        holder.Tv_status = view.findViewById(R.id.ride_list_single_ride_status_textview);
        holder.status_image = view.findViewById(R.id.status_image);
        holder.viewall_list_layout = view.findViewById(R.id.viewall_list_layout);

        holder.Tv_date.setText(data.get(position).getDatetime());
        holder.Tv_amount.setText(data.get(position).getRide_id());


        if (data.get(position).getDistance() != null) {
            holder.Tv_distance.setText(data.get(position).getDistance());
        } else {
            holder.Tv_distance.setText("-");
        }

        if (data.get(position).getUser_image() != null) {
            if (!data.get(position).getUser_image().equalsIgnoreCase("")) {
                AppUtils.setImageView(context, data.get(position).getUser_image(), holder.user_image_view);
            }
        }
        if (data.get(position).getUser_name() != null) {
            holder.Tv_status.setText(data.get(position).getUser_name());
        }

        if ("Completed".equalsIgnoreCase(data.get(position).getRide_status())) {
            holder.status_image.setBackground(context.getResources().getDrawable(R.drawable.right));
        } else if ("Cancelled".equalsIgnoreCase(data.get(position).getRide_status())) {
            holder.status_image.setBackground(context.getResources().getDrawable(R.drawable.wrong));
        }

        holder.cardview_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(view.getContext(), TripSummaryConstrain.class);
                i.putExtra("rideid", data.get(position).getRide_id());
                view.getContext().startActivity(i);
            }
        });


        return view;
    }

}
