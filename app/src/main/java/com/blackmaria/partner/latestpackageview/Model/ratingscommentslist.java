package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class ratingscommentslist implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }

    public class Response {
        @SerializedName("current_page")
        private String current_page;
        @SerializedName("next_page")
        private String next_page;
        @SerializedName("perPage")
        private String perPage;
        @SerializedName("total_record")
        private String total_record;

        public ArrayList<Review> getReview() {
            return review;
        }

        public void setReview(ArrayList<Review> review) {
            this.review = review;
        }

        @SerializedName("review")
        ArrayList<Review> review = new ArrayList<Review>();

        public class Review {
            @SerializedName("user_name")
            private String user_name;
            @SerializedName("comments")
            private String comments;
            @SerializedName("image")
            private String image;
            @SerializedName("ride_id")
            private String ride_id;
            @SerializedName("review_date")
            private String review_date;


            // Getter Methods

            public String getUser_name() {
                return user_name;
            }

            public String getComments() {
                return comments;
            }

            public String getImage() {
                return image;
            }

            public String getRide_id() {
                return ride_id;
            }

            public String getReview_date() {
                return review_date;
            }

            // Setter Methods

            public void setUser_name(String user_name) {
                this.user_name = user_name;
            }

            public void setComments(String comments) {
                this.comments = comments;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public void setRide_id(String ride_id) {
                this.ride_id = ride_id;
            }

            public void setReview_date(String review_date) {
                this.review_date = review_date;
            }
        }
        @SerializedName("date")
        private String date;


        // Getter Methods

        public String getCurrent_page() {
            return current_page;
        }

        public String getNext_page() {
            return next_page;
        }

        public String getPerPage() {
            return perPage;
        }

        public String getTotal_record() {
            return total_record;
        }

        public String getDate() {
            return date;
        }

        // Setter Methods

        public void setCurrent_page(String current_page) {
            this.current_page = current_page;
        }

        public void setNext_page(String next_page) {
            this.next_page = next_page;
        }

        public void setPerPage(String perPage) {
            this.perPage = perPage;
        }

        public void setTotal_record(String total_record) {
            this.total_record = total_record;
        }

        public void setDate(String date) {
            this.date = date;
        }

    }

}
