package com.blackmaria.partner.latestpackageview.widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Ganesh on 31-10-2017.
 */

@SuppressLint("AppCompatCustomView")
public class TextRobotoItalic extends TextView {

    public TextRobotoItalic(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public TextRobotoItalic(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextRobotoItalic(Context context) {
        super(context);
        init();
    }


    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Roboto-Italic.ttf");
        setTypeface(tf);
    }
}
