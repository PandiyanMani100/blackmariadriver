package com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Transfer;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityTransferamountenterConstrainBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Wallet.Transfer.TransferamountenterFactory;
import com.blackmaria.partner.latestpackageview.Model.amounttransferfriendpinenter;
import com.blackmaria.partner.latestpackageview.Model.moneytransferpojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Pin_activity;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet.transfer.TransferamountenterViewModel;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

public class TransferamountenterConstrain extends AppCompatActivity {

    private ActivityTransferamountenterConstrainBinding binding;
    private TransferamountenterViewModel transferamountenterViewModel;
    private SessionManager sessionManager;
    private AppUtils appUtils;
    private moneytransferpojo pojo;
    private String sDriverID = "";

    @Override
    protected void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);}
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);}
    }
    AccessLanguagefromlocaldb db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        EventBus.getDefault().register(this);
        db = new AccessLanguagefromlocaldb(this);
        binding = DataBindingUtil.setContentView(TransferamountenterConstrain.this, R.layout.activity_transferamountenter_constrain);
        transferamountenterViewModel = ViewModelProviders.of(this, new TransferamountenterFactory(this)).get(TransferamountenterViewModel.class);
        binding.setTransferamountenterViewModel(transferamountenterViewModel);
        transferamountenterViewModel.setIds(binding);

        initView();
        clicklistener();
        apiresponse();

        binding.datemonthyear.setText(db.getvalue("fastwallet"));
        binding.transfer.setText(db.getvalue("transfer"));
        binding.mobileno.setHint(db.getvalue("inseramount"));
        binding.sendnow.setText(db.getvalue("sendnow"));
        binding.cancel.setText(db.getvalue("cancel_lable"));
    }

    private void apiresponse() {
        transferamountenterViewModel.getTransferapiresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                        Intent intent = new Intent(TransferamountenterConstrain.this, TransfersuccessConstrain.class);
                        intent.putExtra("json", jsonObject.toString());
                        startActivity(intent);
                        finish();
//                        EventBus.getDefault().unregister(this);
                    } else {
                        appUtils.AlertError(TransferamountenterConstrain.this,db.getvalue("action_error"),jsonObject.getString("response"));
//                        binding.notfounduser.setText(jsonObject.getString("response"));
//                        binding.notfounduser.setVisibility(View.VISIBLE);
//                        binding.notfoundusercontent.setVisibility(View.VISIBLE);
                        binding.transfer.setText("ERROR!");
                        binding.transfer.setBackground(getResources().getDrawable(R.drawable.white_curve));
                        binding.notfoundusercontent.setText(pojo.getResponse().getDriver_name() + " " +db.getvalue("usernotfound"));
                        binding.notfoundusercontent.setVisibility(View.INVISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        transferamountenterViewModel.getCrossborderratesresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                        Intent intent = new Intent(TransferamountenterConstrain.this, TransfercrossborderConstrain.class);
                        intent.putExtra("json", jsonObject.toString());
                        startActivity(intent);
//                        EventBus.getDefault().unregister(this);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void initView() {
        sessionManager = SessionManager.getInstance(this);
        appUtils = AppUtils.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        if (getIntent().hasExtra("json")) {
            Type type = new TypeToken<moneytransferpojo>() {
            }.getType();
            pojo = new GsonBuilder().create().fromJson(getIntent().getStringExtra("json").toString(), type);
            setTextresponse(pojo);
        }


    }

    private void clicklistener() {
        binding.sendnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.mobileno.getText().length() == 0) {
                    appUtils.AlertError(TransferamountenterConstrain.this, db.getvalue("action_error"),db.getvalue("amountcantbempty"));
                }  else if (Integer.parseInt(binding.mobileno.getText().toString().trim()) < 1) {
                    appUtils.AlertError(TransferamountenterConstrain.this, db.getvalue("action_error"),db.getvalue("amountcantbemptyzero"));
                } else if (pojo.getResponse().getCross_border().equalsIgnoreCase("1")) {
                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    jsonParams.put("driver_id", sDriverID);
                    jsonParams.put("transfer_amount", binding.mobileno.getText().toString().trim());
                    jsonParams.put("friend_id", pojo.getResponse().getDriver_id());
                    transferamountenterViewModel.crossbordergetrates(Iconstant.cloudmoney_crossbordervalue_Url, jsonParams);
                } else {
                    Intent ii = new Intent(TransferamountenterConstrain.this, Pin_activity.class);
                    ii.putExtra("mode", "transferamount");
                    startActivity(ii);
                }
            }
        });
    }

    @Subscribe
    public void pinentered(amounttransferfriendpinenter pojos) {
        if (pojos.isIspinenter()) {
            HashMap<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("driver_id", sDriverID);
            jsonParams.put("transfer_amount", binding.mobileno.getText().toString().trim());
            jsonParams.put("friend_id", pojo.getResponse().getDriver_id());
            jsonParams.put("cross_transfer", pojo.getResponse().getCross_border());
            transferamountenterViewModel.transferamounttofriend(Iconstant.driver_wallet_to_wallet_transfer_url, jsonParams);
        }
    }

    private void setTextresponse(moneytransferpojo pojo) {
        AppUtils.setImageView(TransferamountenterConstrain.this, pojo.getResponse().getImage(), binding.userimage);
        binding.usercontent.setText("TRANSFER TO : \n" + "NAME : " + pojo.getResponse().getDriver_name() + "\n" + "MOBILE NO : " + pojo.getResponse().getDail_code() + "" + pojo.getResponse().getMobile_number() + "\n" + "CITY : " + pojo.getResponse().getCity());
        if (pojo.getResponse().getCross_border().equalsIgnoreCase("1")) {
            binding.sendnow.setText("GET RATES");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        EventBus.getDefault().unregister(this);
    }
}
