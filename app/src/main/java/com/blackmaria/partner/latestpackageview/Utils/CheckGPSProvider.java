package com.blackmaria.partner.latestpackageview.Utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.LocationManager;


import static android.content.Context.LOCATION_SERVICE;

public class CheckGPSProvider {
    private Context context;
    LocationManager locationManager;
    public static CheckGPSProvider instance;

    @SuppressLint("ServiceCast")

    public CheckGPSProvider(Context context) {
        this.context = context;
        locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
    }

    public static CheckGPSProvider getInstance(Context context) {
        if (instance == null) {
            instance = new CheckGPSProvider(context);
        }
        return instance;
    }

    public boolean checkprovider() {
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            return true;
        } else {
            return false;
        }
    }

}
