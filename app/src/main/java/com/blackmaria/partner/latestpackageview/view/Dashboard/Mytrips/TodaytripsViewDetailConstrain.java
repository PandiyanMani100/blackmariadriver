package com.blackmaria.partner.latestpackageview.view.Dashboard.Mytrips;

import androidx.appcompat.app.AppCompatActivity;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.Nullable;

import android.os.Bundle;
import android.view.View;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityTodaytripsViewDetailConstrainBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.Adapter.Todayviewdetailadapter;
import com.blackmaria.partner.latestpackageview.Database.RidesDB;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Mytrips.TodaypaginationFactory;
import com.blackmaria.partner.latestpackageview.Model.Rides;
import com.blackmaria.partner.latestpackageview.Model.todaytripviewdetailpojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.mytrips.TodayviewdetailsViewModel;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

public class TodaytripsViewDetailConstrain extends AppCompatActivity {

    private ActivityTodaytripsViewDetailConstrainBinding binding;
    private TodayviewdetailsViewModel todayviewdetailsViewModel;
    private Todayviewdetailadapter rideListAdapter;
    private RidesDB ridesDBobj;
    private SessionManager sessionManager;
    private String driverid = "";
    private ArrayList<Rides> rideslist;
    private AppUtils appUtils;
    AccessLanguagefromlocaldb db;

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
        binding = DataBindingUtil.setContentView(TodaytripsViewDetailConstrain.this, R.layout.activity_todaytrips_view_detail_constrain);
        todayviewdetailsViewModel = ViewModelProviders.of(this, new TodaypaginationFactory(this)).get(TodayviewdetailsViewModel.class);
        binding.setTodayviewdetailsViewModel(todayviewdetailsViewModel);

        binding.datemonthyear.setText(db.getvalue("mytrips"));

        ridesDBobj = new RidesDB(this);
        appUtils = new AppUtils(this);
        sessionManager = SessionManager.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        driverid = user.get(SessionManager.KEY_DRIVERID);
        todayviewdetailsViewModel.setIds(binding);
        rideslist = new ArrayList<>();
//        rideslist = ridesDBobj.getonedayfullrides();
        String startdate = String.valueOf(appUtils.getdatetotiestampwithtime(getIntent().getStringExtra("date") + " 00:00:00"));
        String enddate = String.valueOf(appUtils.getdatetotiestampwithtime(getIntent().getStringExtra("date") + " 24:00:00"));
        rideslist = ridesDBobj.getonedayfullrides(startdate, enddate);
        if (rideslist != null) {
            if (rideslist.size() > 0) {
                todayviewdetailsViewModel.gettodaytrips(getIntent().getStringExtra("date"), false);
                setresponselocaldb(rideslist);
            } else {
                todayviewdetailsViewModel.gettodaytrips(getIntent().getStringExtra("date"), true);
            }
        } else {
            todayviewdetailsViewModel.gettodaytrips(getIntent().getStringExtra("date"), true);
        }

        response();
    }

    private void response() {
        todayviewdetailsViewModel.getTodaytrips().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    try {
                        Type type = new TypeToken<todaytripviewdetailpojo>() {
                        }.getType();
                        todaytripviewdetailpojo object = new GsonBuilder().create().fromJson(response.toString(), type);
                        setresponse(object);
                        updatelocaldb(object.getResponse().getRides());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void updatelocaldb(final ArrayList<Rides> rides) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i <= rides.size() - 1; i++) {
                    ridesDBobj.insertride(driverid, rides.get(i).getRide_id(), rides.get(i).getDatetime_timestamp(),
                            "", rides.get(i).getDatetime(), rides.get(i).getRide_status(), rides.get(i).getDistance(), rides.get(i).getUser_image(), rides.get(i).getUser_name());
                }
            }
        };
        thread.start();
    }

    private void setresponse(todaytripviewdetailpojo object) {
        if (object.getResponse().getRides().size() > 0) {
            binding.rideListNoRidesTextview1.setVisibility(View.GONE);
            binding.rideListListview.setVisibility(View.VISIBLE);
        } else {
            binding.rideListNoRidesTextview1.setVisibility(View.VISIBLE);
            binding.rideListListview.setVisibility(View.GONE);
        }
        binding.tripcount.setText(object.getResponse().getTotal_rides());
        binding.datetxt.setText(object.getResponse().getFilter_date());
        if (object.getResponse().getRides().size() > 0) {
            rideListAdapter = new Todayviewdetailadapter(this, object.getResponse().getRides());
            binding.rideListListview.setAdapter(rideListAdapter);
        }
        binding.rideListPreviousTextview.setVisibility(View.GONE);
        binding.rideListNextTextview.setVisibility(View.GONE);
    }

    private void setresponselocaldb(ArrayList<Rides> object) {
        if (object.size() > 0) {
            binding.rideListNoRidesTextview1.setVisibility(View.GONE);
            binding.rideListListview.setVisibility(View.VISIBLE);
        } else {
            binding.rideListNoRidesTextview1.setVisibility(View.VISIBLE);
            binding.rideListListview.setVisibility(View.GONE);
        }
        binding.tripcount.setText("" + object.size());
        binding.datetxt.setText(ridesDBobj.getsingleridedate(String.valueOf(appUtils.getdatetotiestamp(getIntent().getStringExtra("date")))));
        if (object.size() > 0) {
            rideListAdapter = new Todayviewdetailadapter(this, object);
            binding.rideListListview.setAdapter(rideListAdapter);
        }
        binding.rideListPreviousTextview.setVisibility(View.GONE);
        binding.rideListNextTextview.setVisibility(View.GONE);

    }
}
