package com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.app.Dialog;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.ServiceConnection;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.app.NewCloudMoneyTransferUrlProgress;
import com.blackmaria.partner.databinding.ActivityRechargechoosepaymentBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Wallet.RechargechoosepaymentFactory;
import com.blackmaria.partner.latestpackageview.Model.bankpaymentlistpojo;
import com.blackmaria.partner.latestpackageview.Model.choosepaymentmodel;
import com.blackmaria.partner.latestpackageview.Model.rechargeamountpojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Pin_activity;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Recharge.Bank.RechargeselectbankConstrain;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Recharge.Card.RechargecardpaymentConstrain;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Recharge.paypal.PaypalenteridConstrain;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet.RechargechoosepaymentViewModel;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

public class Rechargechoosepayment extends AppCompatActivity {

    private ActivityRechargechoosepaymentBinding binding;
    private RechargechoosepaymentViewModel rechargechoosepaymentViewModel;
    private SessionManager sessionManager;
    private String sDriverID = "", insertAmount = "", paymenttype = "",jsonoldcard="";
    private rechargeamountpojo pojomodel;
    private bankpaymentlistpojo bankpojo;
    private JSONObject bankpaymentlistpojo;
    private ServiceRequest mRequest;

    @Override
    protected void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);}
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);}
    }
    AccessLanguagefromlocaldb db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
        binding = DataBindingUtil.setContentView(Rechargechoosepayment.this, R.layout.activity_rechargechoosepayment);
        rechargechoosepaymentViewModel = ViewModelProviders.of(this, new RechargechoosepaymentFactory(this)).get(RechargechoosepaymentViewModel.class);
        binding.setRechargechoosepaymentViewModel(rechargechoosepaymentViewModel);
        rechargechoosepaymentViewModel.setIds(binding);

        initView();
        setresponse();

        binding.datemonthyear.setText(db.getvalue("fastwallet"));
        binding.selectpayment.setText(db.getvalue("selectpaymentchannel"));
    }

    private void setresponse() {
        rechargechoosepaymentViewModel.getGetbankpaymentlistresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    bankpaymentlistpojo = new JSONObject(response);
                    Type type = new TypeToken<bankpaymentlistpojo>() {
                    }.getType();
                    bankpojo = new GsonBuilder().create().fromJson(bankpaymentlistpojo.toString(), type);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void initView() {
        sessionManager = SessionManager.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);

        if (getIntent().hasExtra("jsonpojo")) {
            insertAmount = getIntent().getStringExtra("insertAmount");
            Type type = new TypeToken<rechargeamountpojo>() {
            }.getType();
            jsonoldcard = getIntent().getStringExtra("jsonpojo").toString();
            pojomodel = new GsonBuilder().create().fromJson(getIntent().getStringExtra("jsonpojo").toString(), type);
            setpaymentlist();
        }
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        rechargechoosepaymentViewModel.getbankrechargepaymentlist(Iconstant.getPaymentOPtion_url_new, jsonParams);
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Log.v("TRIMMEMORY", " " + level);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }


    private void setpaymentlist() {
        try {
            binding.paymentlist.removeAllViews();
        } catch (NullPointerException e) {
        }
        for (int i = 0; i <= pojomodel.getResponse().getPayment_list().size() - 1; i++) {
            View view = getLayoutInflater().inflate(R.layout.choosepaymentcustomlayout, null);
            ImageView image = view.findViewById(R.id.image);
            LinearLayout constrainview = view.findViewById(R.id.constrainview);
            AppUtils.setImageviewwithoutcropplaceholder(Rechargechoosepayment.this, pojomodel.getResponse().getPayment_list().get(i).getIcon(), image);
            final int finalI = i;
            constrainview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    jsonParams.put("driver_id", sDriverID);
                    jsonParams.put("amount", insertAmount);
                    jsonParams.put("mode", pojomodel.getResponse().getPayment_list().get(finalI).getCode());
                    jsonParams.put("mode_id", pojomodel.getResponse().getPayment_list().get(finalI).getId());
                    postRequest_walletapicheck(Iconstant.rechargecheck_url, jsonParams,finalI);

                }
            });
            binding.paymentlist.addView(view);
        }
    }

    //-----------------------wallet Check Post Request-----------------
    private void postRequest_walletapicheck(String Url, HashMap<String, String> jsonParams, final int finalI) {
        final Dialog dialog = new Dialog(Rechargechoosepayment.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(db.getvalue("action_loading"));


        System.out.println("-------------walletMoney Transaction Url----------------" + Url);


        mRequest = new ServiceRequest(Rechargechoosepayment.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------walletMoney Transaction Response----------------" + response);

                String Sstatus = "", Str_CurrentPage = "", str_NextPage = "", perPage = "",ScurrencySymbol="",Str_total_amount="",Str_total_transaction="";
                try {
                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {

                        if (pojomodel.getResponse().getPayment_list().get(finalI).getCode().equalsIgnoreCase("xendit-bankpayment")) {
                            paymenttype = pojomodel.getResponse().getPayment_list().get(finalI).getCode();
                            /*Intent inten = new Intent(Rechargechoosepayment.this, Pin_activity.class);
                            inten.putExtra("mode", "choosepayment");
                            startActivity(inten);*/
                        } else if (pojomodel.getResponse().getPayment_list().get(finalI).getCode().equalsIgnoreCase("xendit-card")) {
                            paymenttype = pojomodel.getResponse().getPayment_list().get(finalI).getCode();
                           /* Intent inten = new Intent(Rechargechoosepayment.this, Pin_activity.class);
                            inten.putExtra("mode", "choosepayment");
                            startActivity(inten);*/
                        } else if (pojomodel.getResponse().getPayment_list().get(finalI).getCode().equalsIgnoreCase("Paypal")) {
                            paymenttype = pojomodel.getResponse().getPayment_list().get(finalI).getCode();
                           /* Intent inten = new Intent(Rechargechoosepayment.this, Pin_activity.class);
                            inten.putExtra("mode", "choosepayment");
                            startActivity(inten);*/
                        }


                        if (paymenttype.equalsIgnoreCase("xendit-bankpayment")) {
                            Intent i = new Intent(Rechargechoosepayment.this, RechargeselectbankConstrain.class);
                            i.putExtra("paymentlist", bankpaymentlistpojo.toString());
                            i.putExtra("insertAmount", insertAmount);
                            i.putExtra("currency", pojomodel.getResponse().getCurrency());
                            startActivity(i);
//                EventBus.getDefault().unregister(this);
                        } else if (paymenttype.equalsIgnoreCase("xendit-card")) {
                            if (pojomodel.getXendit_charge_status().equalsIgnoreCase("1")) {
                                Double amount = Double.parseDouble(insertAmount) * Double.parseDouble(pojomodel.getResponse().getExchange_value());
                                amount = (Double) Math.ceil(amount);
                                Intent intent = new Intent(Rechargechoosepayment.this, NewCloudMoneyTransferUrlProgress.class);
                                intent.putExtra("total_amount", insertAmount);
                                intent.putExtra("pay_amount", amount + "");
                                intent.putExtra("authentication_id", "");
                                intent.putExtra("token_id", "");
                                intent.putExtra("flag", "2");
                                startActivity(intent);
//                    EventBus.getDefault().unregister(this);
                            } else {
                                Intent intent1 = new Intent(Rechargechoosepayment.this, RechargecardpaymentConstrain.class);
                                intent1.putExtra("insertAmount", insertAmount);
                                intent1.putExtra("jsonpojo", jsonoldcard);
                                intent1.putExtra("currency", pojomodel.getResponse().getCurrency());
                                startActivity(intent1);
//                    EventBus.getDefault().unregister(this);
                            }
                        } else if (paymenttype.toLowerCase().equalsIgnoreCase("paypal")) {
                            AlertPaypal("", db.getvalue("reloadam") + " " + insertAmount.trim() + db.getvalue("viapaypal"));
                        }

                    } else {
                        String Sresponse = object.getString("response");
                        Toast.makeText(Rechargechoosepayment.this, Sresponse, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                if (dialog != null) {
                    dialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });

    }

    @Subscribe
    public void pincheck(choosepaymentmodel pojo) {
        if (pojo.isIsenabled()) {
            if (paymenttype.equalsIgnoreCase("xendit-bankpayment")) {
                Intent i = new Intent(Rechargechoosepayment.this, RechargeselectbankConstrain.class);
                i.putExtra("paymentlist", bankpaymentlistpojo.toString());
                i.putExtra("insertAmount", insertAmount);
                i.putExtra("currency", pojomodel.getResponse().getCurrency());
                startActivity(i);
//                EventBus.getDefault().unregister(this);
            } else if (paymenttype.equalsIgnoreCase("xendit-card")) {
                if (pojomodel.getXendit_charge_status().equalsIgnoreCase("1")) {
                    Double amount = Double.parseDouble(insertAmount) * Double.parseDouble(pojomodel.getResponse().getExchange_value());
                    amount = (Double) Math.ceil(amount);
                    Intent intent = new Intent(Rechargechoosepayment.this, NewCloudMoneyTransferUrlProgress.class);
                    intent.putExtra("total_amount", insertAmount);
                    intent.putExtra("pay_amount", amount + "");
                    intent.putExtra("authentication_id", "");
                    intent.putExtra("token_id", "");
                    intent.putExtra("flag", "2");
                    startActivity(intent);
//                    EventBus.getDefault().unregister(this);
                } else {
                    Intent intent1 = new Intent(Rechargechoosepayment.this, RechargecardpaymentConstrain.class);
                    intent1.putExtra("insertAmount", insertAmount);
                    intent1.putExtra("jsonpojo", jsonoldcard);
                    intent1.putExtra("currency", pojomodel.getResponse().getCurrency());
                    startActivity(intent1);
//                    EventBus.getDefault().unregister(this);
                }
            } else if (paymenttype.toLowerCase().equalsIgnoreCase("paypal")) {
                AlertPaypal("", "RELOAD" + " " + insertAmount.trim() + " VIA PAYPAL?");
            }
        }
    }

    private void AlertPaypal(String title, String alert) {
        final PkDialog mDialog = new PkDialog(Rechargechoosepayment.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(db.getvalue("action_yes"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("walletMoney_recharge_amount", insertAmount);
                    jsonObject.put("walletMoney_currency_symbol", pojomodel.getResponse().getCurrency());
                    jsonObject.put("walletMoney_currentBalance", pojomodel.getResponse().getCurrent_balance());
                    jsonObject.put("walletMoney_paymentType", paymenttype);
                    Intent intent = new Intent(Rechargechoosepayment.this, PaypalenteridConstrain.class);
                    intent.putExtra("json", jsonObject.toString());
                    startActivity(intent);
                    overridePendingTransition(R.anim.enter, R.anim.exit);
//                    EventBus.getDefault().unregister(this);
                    mDialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        mDialog.setNegativeButton(db.getvalue("action_no"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
//        EventBus.getDefault().unregister(this);
    }
}
