package com.blackmaria.partner.latestpackageview.location.Service;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.content.res.Configuration;
import android.location.Location;
import android.os.Build;
import androidx.annotation.RequiresApi;
import android.util.Log;

import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.latestpackageview.location.LocationUpdatesComponent;

import org.greenrobot.eventbus.EventBus;

/**
 * location update service continues to running and getting location information
 */
@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class LocationUpdatesService extends JobService implements LocationUpdatesComponent.ILocationProvider {
    private static final String TAG = LocationUpdatesService.class.getSimpleName();
    public static final int LOCATION_MESSAGE = 9999;
    private LocationUpdatesComponent locationUpdatesComponent;
    private SessionManager sessionManager;
    public LocationUpdatesService() {
    }
    @Override
    public boolean onStartJob(JobParameters params) {
        return true;
    }
    @Override
    public boolean onStopJob(JobParameters params) {

        locationUpdatesComponent.onStop();
        return false;
    }
    @Override
    public void onCreate() {
        sessionManager = SessionManager.getInstance(this);
        locationUpdatesComponent = new LocationUpdatesComponent(this);
        locationUpdatesComponent.onCreate(this);

    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        locationUpdatesComponent.onStart();

        return START_NOT_STICKY;
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
    }
    @Override
    public boolean onUnbind(Intent intent) {
        return true; // Ensures onRebind() is called when a client re-binds.
    }
    @Override
    public void onDestroy() {
        locationUpdatesComponent.onStop();
    }
    private void sendMessage(int messageID, Location location)
    {
        Log.i(TAG, "New location: " + location);
        sessionManager.setLatitude(String.valueOf(location.getLatitude()));
        sessionManager.setLongitude(String.valueOf(location.getLongitude()));
        EventBus.getDefault().post(location);
    }
    @Override
    public void onLocationUpdate(Location location) {
        sendMessage(LOCATION_MESSAGE, location);
    }
}
