package com.blackmaria.partner.latestpackageview.vieewmodel.ratings;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import com.blackmaria.partner.R;
import com.blackmaria.partner.databinding.ActivityRatingsconstrainBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;

import java.util.HashMap;

public class RatingsViewModel extends ViewModel implements ApIServices.completelisner {

    private Activity context;
    private ActivityRatingsconstrainBinding binding;
    private Dialog dialog;
    private int count = 0;
    private MutableLiveData<String> reviewresponse = new MutableLiveData<>();
    private MutableLiveData<String> ratingssubmitresponse = new MutableLiveData<>();

    public RatingsViewModel(Activity context) {
        this.context = context;
    }

    public void setIds(ActivityRatingsconstrainBinding binding) {
        this.binding = binding;
    }

    public MutableLiveData<String> getReviewresponse() {
        return reviewresponse;
    }

    public MutableLiveData<String> getRatingssubmitresponse() {
        return ratingssubmitresponse;
    }

    public void reviewapihit(HashMap<String, String> jsonParams) {
        count = 0;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ApIServices apIServices = new ApIServices(context, RatingsViewModel.this);
        apIServices.dooperation(Iconstant.Rating_option_Url, jsonParams);
    }

    public void ratingssubmitapihit(HashMap<String, String> jsonParams) {
        count = 1;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ApIServices apIServices = new ApIServices(context, RatingsViewModel.this);
        apIServices.dooperation(Iconstant.Rating_submit_Url, jsonParams);
    }

    @Override
    public void sucessresponse(String val) {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        if (count == 0) {
            reviewresponse.postValue(val);
        } else if (count == 1) {
            ratingssubmitresponse.postValue(val);
        }
    }

    @Override
    public void errorreponse() {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
    }

    @Override
    public void jsonexception() {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        AppUtils.toastprint(context, "Json Exception");
    }
}
