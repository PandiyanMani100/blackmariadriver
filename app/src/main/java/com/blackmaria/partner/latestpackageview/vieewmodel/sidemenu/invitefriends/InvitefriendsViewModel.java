package com.blackmaria.partner.latestpackageview.vieewmodel.sidemenu.invitefriends;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.View;
import android.view.Window;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.databinding.ActivityInvitefriendsConstrainBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Model.invitefriends;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.google.android.gms.plus.PlusShare;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Type;
import java.util.HashMap;

public class InvitefriendsViewModel extends ViewModel implements ApIServices.completelisner {

    private Activity context;
    private ActivityInvitefriendsConstrainBinding binding;
    private Dialog dialog;
    private MutableLiveData<String> referalprogram = new MutableLiveData<>();
    private int count = 0;
    private invitefriends objectglobal;
    private AppUtils appUtils;
    AccessLanguagefromlocaldb db;


    public InvitefriendsViewModel(Activity context) {
        this.context = context;
        appUtils = AppUtils.getInstance(context);
        db = new AccessLanguagefromlocaldb(context);
    }

    public void setIds(ActivityInvitefriendsConstrainBinding binding) {
        this.binding = binding;
    }

    public void updatereferalprogram(String url, HashMap<String, String> jsonParams) {
        count = 0;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ApIServices apIServices = new ApIServices(context, InvitefriendsViewModel.this);
        apIServices.dooperation(url, jsonParams);
    }

    public void getinvitedriver(String driveid) {
        count = 1;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", driveid);
        ApIServices apIServices = new ApIServices(context, InvitefriendsViewModel.this);
        apIServices.dooperation(Iconstant.driver_and_earn_url, jsonParams);
    }

    public MutableLiveData<String> getReferalprogram() {
        return referalprogram;
    }


    @Override
    public void sucessresponse(String val) {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        if (count == 1) {
            try {
                JSONObject object = new JSONObject(val);
                Type type = new TypeToken<invitefriends>() {
                }.getType();
                objectglobal = new GsonBuilder().create().fromJson(val.toString(), type);
                if (objectglobal.getStatus().equalsIgnoreCase("1")) {
                    binding.inviteearnreferralcodetv.setText(objectglobal.getResponse().getDetails().getReferral_code());
                } else {
                    appUtils.AlertError(context, db.getvalue("action_error"), object.getString("response"));
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            referalprogram.postValue(val);
        }
    }

    @Override
    public void errorreponse() {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
    }

    @Override
    public void jsonexception() {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        AppUtils.toastprint(context, "Json Exception");
    }


    public void sharesms() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.putExtra("sms_body", objectglobal.getResponse().getDetails().getMessage());
        intent.setData(Uri.parse("smsto:" + ""));
        context.startActivity(intent);
    }

    public void sharegplus() {
        if (AppUtils.isAppinstalled(context, "com.google.android.apps.plus")) {
            Intent shareIntent = new PlusShare.Builder(context)
                    .setType("text/plain")
                    .setText(objectglobal.getResponse().getDetails().getMessage())
                    .setContentUrl(Uri.parse("https://developers.google.com/+/"))
                    .getIntent();
            context.startActivityForResult(shareIntent, 0);
        } else {
            Alert(db.getvalue("alert_label_title"),db.getvalue("invite_earn_label_gplus_not_installed"), "https://play.google.com/store/apps/details?id=com.google.android.apps.plus&hl=en");
        }
    }

    public void sharewhatsapp() {
        PackageManager pm = context.getPackageManager();
        try {
            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");
            PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);  //Check if package exists or not. If not then codein catch block will be called
            waIntent.setPackage("com.whatsapp");
            waIntent.putExtra(Intent.EXTRA_TEXT, objectglobal.getResponse().getDetails().getMessage());
            context.startActivity(Intent.createChooser(waIntent, "Share with"));
        } catch (PackageManager.NameNotFoundException e) {
            Alert(db.getvalue("alert_label_title"),db.getvalue("invite_earn_label_whatsApp_not_installed"), "https://play.google.com/store/apps/details?id=com.whatsapp&hl=en");
        }
    }

    public void shareinsta() {
        Uri imageUri = Uri.parse(objectglobal.getResponse().getDetails().getUrl());
        try {
            imageUri = Uri.parse(MediaStore.Images.Media.insertImage(context.getContentResolver(),
                    BitmapFactory.decodeResource(context.getResources(), R.drawable.app_icon), null, null));
        } catch (NullPointerException e) {
        }
        if (objectglobal.getResponse().getDetails().getUrl().length() > 0) {
           // shareInstagramelink(objectglobal.getResponse().getDetails().getMessage(), imageUri);
            shareInstagrame(objectglobal.getResponse().getDetails().getMessage());
        } else {
            shareInstagrame(objectglobal.getResponse().getDetails().getMessage());
        }
    }

    protected void shareInstagrame(String text) {
        Bitmap b = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.app_icon);

        Intent share = new Intent(Intent.ACTION_SEND);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        b.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), b, "Title", null);
        Uri imageUri =  Uri.parse(path);
        share.putExtra(Intent.EXTRA_STREAM, imageUri);
        share.setType("image/jpeg");
        share.setPackage("com.instagram.android");
        share.putExtra(Intent.EXTRA_TEXT, text);
        share.setType("text/plain");
        try {
            context.startActivity(Intent.createChooser(share, "Share"));
        }
        catch (android.content.ActivityNotFoundException ex) {
            Alert(db.getvalue("alert_label_title"),db.getvalue("invite_earn_label_messenger_not_installed"), "https://play.google.com/store/apps/details?id=com.instagram.android");
        }

    }

    protected void shareInstagramelink(String text, Uri image) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT, text);
        intent.setType("image/jpeg");
        intent.setPackage("com.instagram.android");
        intent.putExtra(Intent.EXTRA_STREAM, image);
        try {
            context.startActivity(Intent.createChooser(intent, "Share to"));
        } catch (android.content.ActivityNotFoundException ex) {
            Alert(db.getvalue("alert_label_title"), db.getvalue("invite_earn_label_instagram_not_installed"), "https://play.google.com/store/search?q=instagram&c=apps&hl=en");
        }
    }


    public void sharetwitter() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, objectglobal.getResponse().getDetails().getMessage());
        intent.setPackage("com.twitter.android");
        try {
            context.startActivity(intent);
        } catch (android.content.ActivityNotFoundException ex) {
            Alert(db.getvalue("alert_label_title"), db.getvalue("invite_earn_label_twitter_not_installed"), "https://play.google.com/store/apps/details?id=com.twitter.android&hl=en");
        }
    }

    public void sharefacebook() {
        String facebook_text = objectglobal.getResponse().getDetails().getUrl();
        Uri imageUri = null;
        try {
            imageUri = Uri.parse(MediaStore.Images.Media.insertImage(context.getContentResolver(),
                    BitmapFactory.decodeResource(context.getResources(), R.drawable.app_icon), null, null));
        } catch (NullPointerException e) {
        }

        if (objectglobal.getResponse().getDetails().getUrl().length() > 0) {
           // shareFacebookLink(objectglobal.getResponse().getDetails().getUrl());
            shareFacebookmsg(objectglobal.getResponse().getDetails().getMessage(), imageUri);
        } else {
            shareFacebookmsg(facebook_text, imageUri);
        }
    }

    protected void shareFacebookmsg(String text, Uri image) {
        Bitmap b = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.app_icon);
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        b.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), b, "Title", null);
        Uri imageUri =  Uri.parse(path);
        share.setPackage("com.facebook.orca");
        share.putExtra(Intent.EXTRA_TEXT, text);
        try {
            context.startActivity(Intent.createChooser(share, "Share"));
        }
        catch (android.content.ActivityNotFoundException ex) {
            Alert(db.getvalue("alert_label_title"), db.getvalue("invite_earn_label_messenger_not_installed"), "https://play.google.com/store/apps/details?id=com.facebook.orca");
        }

    }


    private void shareFacebookLink(String link) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, link);
        Intent pacakage1 = context.getPackageManager().getLaunchIntentForPackage("com.facebook.katana");
        if (pacakage1 != null) {
            intent.setPackage("com.facebook.katana");
        } else {
            intent.setPackage("");
        }
        try {
            context.startActivity(intent);
        } catch (android.content.ActivityNotFoundException ex) {
            Alert(db.getvalue("alert_label_title"), db.getvalue("invite_earn_label_facebook_not_installed"), "https://play.google.com/store/apps/details?id=com.facebook.katana&hl=en");
        }
    }


    //--------------Alert Method-----------
    private void Alert(String title, String alert, final String url) {
        final PkDialog mDialog = new PkDialog(context);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(db.getvalue("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                context.startActivity(i);
            }
        });
        mDialog.show();

    }


}
