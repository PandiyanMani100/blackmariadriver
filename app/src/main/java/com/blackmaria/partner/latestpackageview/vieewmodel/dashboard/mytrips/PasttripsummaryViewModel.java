package com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.mytrips;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import com.blackmaria.partner.R;
import com.blackmaria.partner.databinding.ActivityPastTripSummaryConstrainBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;

import java.util.HashMap;

public class PasttripsummaryViewModel extends ViewModel implements ApIServices.completelisner {
    private ActivityPastTripSummaryConstrainBinding binding;
    private Activity context;
    private MutableLiveData<String> response = new MutableLiveData<>();
    private Dialog dialog;

    public PasttripsummaryViewModel(Activity context) {
        this.context = context;
    }

    public void setIds(ActivityPastTripSummaryConstrainBinding binding) {
        this.binding = binding;
    }

    public void gettripsummaryapihit(String sDriverID,String rideID){
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        jsonParams.put("ride_id", rideID);

        ApIServices apIServices = new ApIServices(context, PasttripsummaryViewModel.this);
        apIServices.dooperation(Iconstant.complete_trip_detail_url, jsonParams);
    }

    public MutableLiveData<String> getResponse() {
        return response;
    }

    @Override
    public void sucessresponse(String val) {
        binding.apiload.setVisibility(View.GONE);
        dialog.dismiss();
        response.postValue(val);
        binding.sendreciept.setVisibility(View.VISIBLE);
        binding.addresss.setVisibility(View.VISIBLE);
    }

    @Override
    public void errorreponse() {
        binding.apiload.setVisibility(View.GONE);
        dialog.dismiss();
    }

    @Override
    public void jsonexception() {
        binding.apiload.setVisibility(View.GONE);
        dialog.dismiss();
        AppUtils.toastprint(context,"Json Exception");
    }
}
