package com.blackmaria.partner.latestpackageview.Factory.Dashboard.Earnings;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.earnings.EarningsStatmentViewModel;


public class EarningsStatementFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public EarningsStatementFactory(Activity context) {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new EarningsStatmentViewModel(context);
    }
}
