package com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Withdraw.Cash;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Toast;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.DowloadPdfActivity;
import com.blackmaria.partner.databinding.ActivityCashwithdrawsuccessConstrainBinding;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Wallet.Withdraw.Cash.CashwithdrawasuccesslFactory;
import com.blackmaria.partner.latestpackageview.Model.withdrawpaymentsendpojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet.withdraw.cash.CashwithdrawsuccessViewModel;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.lang.reflect.Type;
import java.net.URI;
import java.net.URISyntaxException;

public class CashwithdrawsuccessConstrain extends AppCompatActivity implements DowloadPdfActivity.downloadCompleted {

    private ActivityCashwithdrawsuccessConstrainBinding binding;
    private CashwithdrawsuccessViewModel cashwithdrawsuccessViewModel;
    private withdrawpaymentsendpojo pojo;
    private AppUtils appUtils;

    AccessLanguagefromlocaldb db;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
        binding = DataBindingUtil.setContentView(CashwithdrawsuccessConstrain.this, R.layout.activity_cashwithdrawsuccess_constrain);
        cashwithdrawsuccessViewModel = ViewModelProviders.of(this, new CashwithdrawasuccesslFactory(this)).get(CashwithdrawsuccessViewModel.class);
        binding.setCashwithdrawsuccessViewModel(cashwithdrawsuccessViewModel);
        cashwithdrawsuccessViewModel.setIds(binding);
        binding.successtxt.setText(db.getvalue("success"));
        binding.cashcodenumbertxt.setText(db.getvalue("cashcodenumber"));
        binding.scantoredeem.setText(db.getvalue("scantoredeem"));
        binding.save.setText(db.getvalue("save"));
        binding.refund.setText(db.getvalue("refund"));
        binding.successcontent.setText(db.getvalue("withdrawsuccesscontent"));
        binding.close.setText(db.getvalue("close_lable"));

        initView();
    }

    private void initView() {
        appUtils = AppUtils.getInstance(this);
        if (getIntent().hasExtra("json")) {
            try {
                Type type = new TypeToken<withdrawpaymentsendpojo>() {
                }.getType();
                pojo = new GsonBuilder().create().fromJson(getIntent().getStringExtra("json").toString(), type);
                setText(pojo);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setText(withdrawpaymentsendpojo pojo) {
        binding.name.setText("PAY : CASH" + "\n" + "FROM : " + pojo.getFrom() + "\n" + "CREATED : " + pojo.getDate() + "\n" + "TIME : " + pojo.getTime() + "\n" + pojo.getCountry_code() + "" + pojo.getPhone_number());
        binding.cashcodenumbertxt.setText(getResources().getString(R.string.cashcodenumber) + "\n" + pojo.getCash_code());
        binding.transactionumber.setText("TRANSACTION NUMBER : " + pojo.getRef_no());
        if(!pojo.getQrcode().equalsIgnoreCase("")){
            appUtils.setImageViewqr(CashwithdrawsuccessConstrain.this,pojo.getQrcode(),binding.scanimage);
        }else{
            appUtils.setImageViewqr(CashwithdrawsuccessConstrain.this,pojo.getQrcode(),binding.scanimage);
        }
        binding.save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DowloadPdfActivity(CashwithdrawsuccessConstrain.this, binding.card, "Cashwithdraw", CashwithdrawsuccessConstrain.this);
            }
        });
        binding.refund.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appUtils.AlertError(CashwithdrawsuccessConstrain.this, getResources().getString(R.string.action_error), "Working in progress");
            }
        });
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public void OnDownloadComplete(String fileName) {

        ViewPdfAlert("", " Successfully Downloaded", fileName);
    }

    private void ViewPdfAlert(String title, String alert, final String fileName) {

        final PkDialog mDialog = new PkDialog(CashwithdrawsuccessConstrain.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton("OPEN", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPDF(fileName);
                mDialog.dismiss();
            }
        });
        mDialog.setNegativeButton(getResources().getString(R.string.cancel_lable), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();

    }

    public void viewPDF(String pdfFileName) {
        try {
            File pdfFile = new File(Environment.getExternalStorageDirectory() + "/Signature/" + pdfFileName);  // -> filename = maven.pdf
            Uri uri_path = Uri.fromFile(pdfFile);
            Uri uri = uri_path;

            try {
                File file = null;
                String path = uri.toString();// "/mnt/sdcard/FileName.mp3"
                try {
                    file = new File(new URI(path));
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }

                final Intent intent = new Intent(Intent.ACTION_VIEW)//
                        .setDataAndType(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N ?
                                        androidx.core.content.FileProvider.getUriForFile(CashwithdrawsuccessConstrain.this, getPackageName() + ".provider", file) : Uri.fromFile(file),
                                "application/pdf").addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(CashwithdrawsuccessConstrain.this, "No Application available to view PDF", Toast.LENGTH_SHORT).show();
                }

            } catch (ActivityNotFoundException e) {
                Toast.makeText(CashwithdrawsuccessConstrain.this, "Cannot read file error. ", Toast.LENGTH_SHORT).show();
            }

        } catch (NullPointerException e) {

        } catch (Exception ew) {
        }
    }
}
