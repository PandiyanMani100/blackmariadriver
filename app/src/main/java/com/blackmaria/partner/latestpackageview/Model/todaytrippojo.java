package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class todaytrippojo implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;

    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }

    public class Response {
        @SerializedName("today")
        Today TodayObject;
        @SerializedName("today_date")
        private String today_date;


        // Getter Methods

        public Today getToday() {
            return TodayObject;
        }

        public String getToday_date() {
            return today_date;
        }

        // Setter Methods

        public void setToday(Today todayObject) {
            this.TodayObject = todayObject;
        }

        public void setToday_date(String today_date) {
            this.today_date = today_date;
        }

        public class Today {
            @SerializedName("completed_rides")
            private String completed_rides;
            @SerializedName("bidding_trip")
            private String bidding_trip;

            public String getDriver_cancel_rides() {
                return driver_cancel_rides;
            }

            public void setDriver_cancel_rides(String driver_cancel_rides) {
                this.driver_cancel_rides = driver_cancel_rides;
            }

            @SerializedName("driver_cancel_rides")
            private String driver_cancel_rides;
            @SerializedName("sos_trip")
            private String sos_trip;
            @SerializedName("cancel_rides")
            private String cancel_rides;


            // Getter Methods

            public String getCompleted_rides() {
                return completed_rides;
            }

            public String getBidding_trip() {
                return bidding_trip;
            }

            public String getSos_trip() {
                return sos_trip;
            }

            public String getCancel_rides() {
                return cancel_rides;
            }

            // Setter Methods

            public void setCompleted_rides(String completed_rides) {
                this.completed_rides = completed_rides;
            }

            public void setBidding_trip(String bidding_trip) {
                this.bidding_trip = bidding_trip;
            }

            public void setSos_trip(String sos_trip) {
                this.sos_trip = sos_trip;
            }

            public void setCancel_rides(String cancel_rides) {
                this.cancel_rides = cancel_rides;
            }
        }
    }
}


