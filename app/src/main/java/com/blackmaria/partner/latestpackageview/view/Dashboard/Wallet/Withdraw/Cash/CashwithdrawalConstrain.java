package com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Withdraw.Cash;

import android.app.Dialog;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.annotation.Nullable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityCashwithdrawalConstrainBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Wallet.Withdraw.Cash.CashwithdrawalFactory;
import com.blackmaria.partner.latestpackageview.Model.cashwithdrawpincheck;
import com.blackmaria.partner.latestpackageview.Model.lookupcashpojo;
import com.blackmaria.partner.latestpackageview.Model.withdrawpaymentsendpojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Pin_activity;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet.withdraw.cash.CashwithdrawalViewModel;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

public class CashwithdrawalConstrain extends AppCompatActivity {
    private CashwithdrawalViewModel cashwithdrawalViewModel;
    private ActivityCashwithdrawalConstrainBinding binding;
    private String cashwithdrawid = "",sDriverID = "", transfer_amount = "", service_amount = "", name = "", phone = "", image = "", currency = "", paymentcode = "";
    private SessionManager sessionManager;
    private AppUtils appUtils;
    AccessLanguagefromlocaldb db;

    @Override
    protected void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);}
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);}
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
//        EventBus.getDefault().register(this);
        binding = DataBindingUtil.setContentView(CashwithdrawalConstrain.this, R.layout.activity_cashwithdrawal_constrain);
        cashwithdrawalViewModel = ViewModelProviders.of(this, new CashwithdrawalFactory(this)).get(CashwithdrawalViewModel.class);
        binding.setCashwithdrawalViewModel(cashwithdrawalViewModel);
        cashwithdrawalViewModel.setIds(binding);

        initView();
        clicklistener();
        setResponse();

        binding.fastpaybalancename.setText(db.getvalue("fastcash"));
        binding.lookupcash.setText(db.getvalue("lookupcash"));
        binding.confirm.setText(db.getvalue("confirm"));
        binding.notes.setText(db.getvalue("notescashwithdrawal"));
        binding.help.setText(db.getvalue("helpcashwithdrawal"));
        binding.cancel.setText(db.getvalue("cancel_lable"));
    }

    private void setResponse() {
        cashwithdrawalViewModel.getWithapihitresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    Type type = new TypeToken<withdrawpaymentsendpojo>() {
                    }.getType();
                    withdrawpaymentsendpojo pojo = new GsonBuilder().create().fromJson(jsonObj.toString(), type);
                    if (pojo.getStatus().equalsIgnoreCase("1")) {
                        Intent i = new Intent(CashwithdrawalConstrain.this, CashwithdrawsuccessConstrain.class);
                        i.putExtra("json", jsonObj.toString());
                        startActivity(i);
                        finish();
                    } else {
                        String Sresponse = jsonObj.getString("response");
                        appUtils.AlertError(CashwithdrawalConstrain.this, db.getvalue("action_error"), Sresponse);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void initView() {
        sessionManager = SessionManager.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        appUtils = AppUtils.getInstance(this);
        if (getIntent().hasExtra("transfer_amount")) {
            transfer_amount = getIntent().getStringExtra("transfer_amount");
            service_amount = getIntent().getStringExtra("service_amount");
            name = getIntent().getStringExtra("name");
            phone = getIntent().getStringExtra("phone");
            image = getIntent().getStringExtra("image");
            currency = getIntent().getStringExtra("currency");
            paymentcode = getIntent().getStringExtra("paymentcode");
            binding.name.setText(name + "\n" + phone);
            binding.withdrawname.setText("WITHDRAWN AMOUNT " + "\n" + "SERVICE FEES " + "\n" + "RECEIVED AMOUNT");
            AppUtils.setImageviewwithoutcropplaceholder(CashwithdrawalConstrain.this, image, binding.image);
            try {
                long minusamount = Long.parseLong(transfer_amount) - Long.parseLong(service_amount);
                binding.withdrawnamevalue.setText(currency + " " + transfer_amount + "\n" + currency + " " + service_amount + "\n" + currency + " " + String.valueOf(minusamount));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
    }

    private void clicklistener() {
        binding.help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CashOutHelpDialog();
            }
        });

        binding.confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!cashwithdrawid.equalsIgnoreCase("")) {
                    Intent inten = new Intent(CashwithdrawalConstrain.this, Pin_activity.class);
                    inten.putExtra("mode", "cashwithdrawal");
                    startActivity(inten);
                }else{
                    Alert(db.getvalue("action_error"),db.getvalue("chooselocationforcashwithdraw"));
                }
            }
        });

        binding.lookupcash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent inten = new Intent(CashwithdrawalConstrain.this, CashwithdrawlocationchooseConstrian.class);
                startActivity(inten);
            }
        });
    }

    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(CashwithdrawalConstrain.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(db.getvalue("action_yes"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                Intent inten = new Intent(CashwithdrawalConstrain.this, CashwithdrawlocationchooseConstrian.class);
                startActivity(inten);
            }
        });
        mDialog.setNegativeButton(db.getvalue("action_no"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    @Subscribe
    public void getcashcenterid(lookupcashpojo pojo){
        cashwithdrawid = pojo.getCashcenterid();
        appUtils.AlertSuccess(CashwithdrawalConstrain.this,db.getvalue("action_success"), db.getvalue("locationchoosed"));
    }

    private void CashOutHelpDialog() {
        final Dialog driveNowDialog = new Dialog(CashwithdrawalConstrain.this, R.style.SlideUpDialog);
        driveNowDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        driveNowDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        driveNowDialog.setCancelable(true);
        driveNowDialog.setCanceledOnTouchOutside(true);
        driveNowDialog.setContentView(R.layout.cashout_help_dialog);
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.90);//fill only 85% of the screen
        int screenHeight = (int) (metrics.heightPixels * 0.90);//fill only 85% of the screen
        driveNowDialog.getWindow().setLayout(screenWidth, screenHeight);
        final TextView cancel = driveNowDialog.findViewById(R.id.close);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                driveNowDialog.dismiss();
            }
        });
        cancel.setText(db.getvalue("close_lable"));

        final TextView cashoutmywallet = driveNowDialog.findViewById(R.id.cashoutmywallet);
        cashoutmywallet.setText(db.getvalue("cashout"));
        final TextView cashoutmywalletcontent = driveNowDialog.findViewById(R.id.cashoutmywalletcontent);
        cashoutmywalletcontent.setText(db.getvalue("cashoutcontent"));

        final TextView overseastitle = driveNowDialog.findViewById(R.id.overseastitle);
        overseastitle.setText(db.getvalue("overseastitle"));
        final TextView overseascontent = driveNowDialog.findViewById(R.id.overseascontent);
        overseascontent.setText(db.getvalue("overseascontent"));



        driveNowDialog.show();
    }

    @Subscribe
    public void caswithdrawpincheck(cashwithdrawpincheck pojo) {
        if (pojo.isIspincheck()) {
            HashMap<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("driver_id", sDriverID);
            jsonParams.put("amount", transfer_amount);
            jsonParams.put("mode", paymentcode);
            jsonParams.put("center_id", cashwithdrawid);
            cashwithdrawalViewModel.cashwithdrawapihit(Iconstant.wallet_withdraw_amount_url, jsonParams);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);}

    }
}
