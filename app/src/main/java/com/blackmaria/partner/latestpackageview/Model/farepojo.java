package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class farepojo implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }

    public class Response {
        @SerializedName("currency")
        private String currency;
        @SerializedName("fare")
        Fare FareObject;


        // Getter Methods

        public String getCurrency() {
            return currency;
        }

        public Fare getFare() {
            return FareObject;
        }

        // Setter Methods

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public void setFare(Fare fareObject) {
            this.FareObject = fareObject;
        }

        public class Fare {
            @SerializedName("need_payment")
            private String need_payment;
            @SerializedName("receive_cash")
            private String receive_cash;

            public ArrayList<Fare_arr> getFare_arr() {
                return fare_arr;
            }

            public void setFare_arr(ArrayList<Fare_arr> fare_arr) {
                this.fare_arr = fare_arr;
            }

            @SerializedName("fare_arr")
            ArrayList < Fare_arr > fare_arr = new ArrayList< Fare_arr >();

            public ArrayList<Trip_receipt> getTrip_receipt() {
                return trip_receipt;
            }

            public void setTrip_receipt(ArrayList<Trip_receipt> trip_receipt) {
                this.trip_receipt = trip_receipt;
            }

            @SerializedName("trip_receipt")
            ArrayList < Trip_receipt > trip_receipt = new ArrayList< Trip_receipt >();

            public class Trip_receipt{
                @SerializedName("title")
                private String title;
                @SerializedName("value")
                private String value;
                @SerializedName("currency_status")
                private String currency_status;


                // Getter Methods

                public String getTitle() {
                    return title;
                }

                public String getValue() {
                    return value;
                }

                public String getCurrency_status() {
                    return currency_status;
                }

                // Setter Methods

                public void setTitle(String title) {
                    this.title = title;
                }

                public void setValue(String value) {
                    this.value = value;
                }

                public void setCurrency_status(String currency_status) {
                    this.currency_status = currency_status;
                }
            }

            public class Fare_arr{
                @SerializedName("title")
                private String title;
                @SerializedName("value")
                private String value;
                @SerializedName("currency_status")
                private String currency_status;


                // Getter Methods

                public String getTitle() {
                    return title;
                }

                public String getValue() {
                    return value;
                }

                public String getCurrency_status() {
                    return currency_status;
                }

                // Setter Methods

                public void setTitle(String title) {
                    this.title = title;
                }

                public void setValue(String value) {
                    this.value = value;
                }

                public void setCurrency_status(String currency_status) {
                    this.currency_status = currency_status;
                }
            }
            @SerializedName("payment_type")
            private String payment_type;
            @SerializedName("currency")
            private String currency;
            @SerializedName("payment_request")
            private String payment_request;
            @SerializedName("grand_fare")
            private String grand_fare;
            @SerializedName("ride_completed_time")
            private String ride_completed_time;
            @SerializedName("ride_completed_date")
            private String ride_completed_date;
            @SerializedName("ride_completed_tim")
            private String ride_completed_tim;
            @SerializedName("message")
            private String message;
            @SerializedName("ratting_content")
            private String ratting_content;
            @SerializedName("payment_timeout")
            private String payment_timeout;
            @SerializedName("user_image")
            private String user_image;
            @SerializedName("user_saveplus_id")
            private String user_saveplus_id;
            @SerializedName("user_name")
            private String user_name;
            @SerializedName("user_id")
            private String user_id;
            @SerializedName("payment_icon")
            private String payment_icon;


            // Getter Methods

            public String getNeed_payment() {
                return need_payment;
            }

            public String getReceive_cash() {
                return receive_cash;
            }

            public String getPayment_type() {
                return payment_type;
            }

            public String getCurrency() {
                return currency;
            }

            public String getPayment_request() {
                return payment_request;
            }

            public String getGrand_fare() {
                return grand_fare;
            }

            public String getRide_completed_time() {
                return ride_completed_time;
            }

            public String getRide_completed_date() {
                return ride_completed_date;
            }

            public String getRide_completed_tim() {
                return ride_completed_tim;
            }

            public String getMessage() {
                return message;
            }

            public String getRatting_content() {
                return ratting_content;
            }

            public String getPayment_timeout() {
                return payment_timeout;
            }

            public String getUser_image() {
                return user_image;
            }

            public String getUser_saveplus_id() {
                return user_saveplus_id;
            }

            public String getUser_name() {
                return user_name;
            }

            public String getUser_id() {
                return user_id;
            }

            public String getPayment_icon() {
                return payment_icon;
            }

            // Setter Methods

            public void setNeed_payment(String need_payment) {
                this.need_payment = need_payment;
            }

            public void setReceive_cash(String receive_cash) {
                this.receive_cash = receive_cash;
            }

            public void setPayment_type(String payment_type) {
                this.payment_type = payment_type;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public void setPayment_request(String payment_request) {
                this.payment_request = payment_request;
            }

            public void setGrand_fare(String grand_fare) {
                this.grand_fare = grand_fare;
            }

            public void setRide_completed_time(String ride_completed_time) {
                this.ride_completed_time = ride_completed_time;
            }

            public void setRide_completed_date(String ride_completed_date) {
                this.ride_completed_date = ride_completed_date;
            }

            public void setRide_completed_tim(String ride_completed_tim) {
                this.ride_completed_tim = ride_completed_tim;
            }

            public void setMessage(String message) {
                this.message = message;
            }

            public void setRatting_content(String ratting_content) {
                this.ratting_content = ratting_content;
            }

            public void setPayment_timeout(String payment_timeout) {
                this.payment_timeout = payment_timeout;
            }

            public void setUser_image(String user_image) {
                this.user_image = user_image;
            }

            public void setUser_saveplus_id(String user_saveplus_id) {
                this.user_saveplus_id = user_saveplus_id;
            }

            public void setUser_name(String user_name) {
                this.user_name = user_name;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public void setPayment_icon(String payment_icon) {
                this.payment_icon = payment_icon;
            }
        }
    }
}


