package com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Fragments;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.app.CreditDebitStatement;
import com.blackmaria.partner.latestpackageview.Model.Fastpayhome_pojo;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.Locale;

public class FragmentTab2 extends Fragment {
    private Fastpayhome_pojo homepage;
    private TextView tv_monthlimitamount,tv_debit_amount;
    private CardView cardview_signup;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragmenttab2, container, false);

        if (getArguments().containsKey("fastpayhome")) {
            String jsonpojo = getArguments().getString("fastpayhome");
            try {
                homepage = new Fastpayhome_pojo();
                Type listType = new TypeToken<Fastpayhome_pojo>() {
                }.getType();
                homepage = new GsonBuilder().create().fromJson(jsonpojo, listType);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        initView(rootView);

        return rootView;
    }

    private void initView(View rootView) {
        tv_monthlimitamount = rootView.findViewById(R.id.tv_monthlimitamount);
        tv_debit_amount = rootView.findViewById(R.id.tv_debit_amount);
        cardview_signup = rootView.findViewById(R.id.cardview_signup);

        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);

        Calendar mCalendar = Calendar.getInstance();
        String month = mCalendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
        tv_monthlimitamount.setText(month +" "+String.valueOf(year));
        tv_debit_amount.setText(homepage.getResponse().getCurrency()+" "+homepage.getResponse().getTotal().getWallet_spend());

        cardview_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CreditDebitStatement.class);
                intent.putExtra("type", "debit");
                startActivity(intent);
            }
        });
    }
}