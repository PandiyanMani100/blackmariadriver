package com.blackmaria.partner.latestpackageview.ApiRequest;


import android.app.IntentService;
import android.content.Intent;


import com.blackmaria.partner.latestpackageview.Database.RidesDB;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class RidedbApiIntentService extends IntentService implements ApIServices.completelisner {

    private String url = "";
    private String driverid = "";
    private String rideid = "";
    private boolean isridecancelledbydriver = false;
    private AppUtils appUtils;


    public RidedbApiIntentService() {
        super("RidedbApiIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        appUtils = new AppUtils(this);
        HashMap<String, String> hashMap = (HashMap<String, String>) intent.getSerializableExtra("params");
        url = intent.getStringExtra("url");
        if (intent.getStringExtra("isdrivercancel").equals("true")) {
            isridecancelledbydriver = true;
        }
        driverid = intent.getStringExtra("driverid");
        rideid = intent.getStringExtra("rideid");
        ApIServices apIServices = new ApIServices(getApplicationContext(), RidedbApiIntentService.this);
        apIServices.dooperation(url, hashMap);
    }

    @Override
    public void sucessresponse(String val) {
        String timestamp = "";
        JSONObject obj = null;
        try {
            obj = new JSONObject(val);
            timestamp = obj.getJSONObject("response").getJSONObject("details").getString("pickup_timestamp");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            String driver_cancel = "0";
            String cancel = "0";
            String completed = "0";
            String ridestatus = obj.getJSONObject("response").getJSONObject("details").getString("ride_status");
            String username = obj.getJSONObject("response").getJSONObject("details").getString("user_name");
            String userimage = obj.getJSONObject("response").getJSONObject("details").getString("user_image");

            String day_statrt = obj.getJSONObject("response").getJSONObject("details").getJSONObject("filter_date").getString("day_start");
            String day_end = obj.getJSONObject("response").getJSONObject("details").getJSONObject("filter_date").getString("day_end");
            String month_statrt = obj.getJSONObject("response").getJSONObject("details").getJSONObject("filter_date").getString("month_start");
            String month_end = obj.getJSONObject("response").getJSONObject("details").getJSONObject("filter_date").getString("month_end");
            String week_statrt = obj.getJSONObject("response").getJSONObject("details").getJSONObject("filter_date").getString("week_start");
            String week_end = obj.getJSONObject("response").getJSONObject("details").getJSONObject("filter_date").getString("week_end");
            day_statrt = day_statrt + " 00:00:00";
            day_statrt = String.valueOf(appUtils.getdatetotiestampwithtime(day_statrt));
            day_end = day_end + " 24:00:00";
            day_end = String.valueOf(appUtils.getdatetotiestampwithtime(day_end));
            month_statrt = month_statrt + " 00:00:00";
            month_statrt = String.valueOf(appUtils.getdatetotiestampwithtime(month_statrt));
            month_end = month_end + " 24:00:00";
            month_end = String.valueOf(appUtils.getdatetotiestampwithtime(month_end));
            week_statrt = week_statrt + " 00:00:00";
            week_statrt = String.valueOf(appUtils.getdatetotiestampwithtime(week_statrt));
            week_end = week_end + " 24:00:00";
            week_end = String.valueOf(appUtils.getdatetotiestampwithtime(week_end));


            if (ridestatus.toLowerCase().equals("completed")) {
                completed = "1";
            } else if (ridestatus.toLowerCase().equals("cancelled")) {
                if (isridecancelledbydriver) {
                    driver_cancel = "1";
                    cancel = "1";
                } else {
                    cancel = "1";
                }
            }
            String ridedistance = "";
            if (ridestatus.toLowerCase().equals("cancelled")) {
                ridedistance = "0";
            } else {
                ridedistance = obj.getJSONObject("response").getJSONObject("details").getJSONObject("summary").getString("ride_distance");
            }
            String ridedatewithtimeshow = obj.getJSONObject("response").getJSONObject("details").getString("trip_date") + " " + obj.getJSONObject("response").getJSONObject("details").getString("pickup_time");
            RidesDB ridesDB = new RidesDB(RidedbApiIntentService.this);
            //singelday
            ridesDB.insertride(driverid, rideid, timestamp, val, ridedatewithtimeshow, ridestatus, ridedistance, userimage, username);
            //month
            long timestamps = Long.parseLong(timestamp) * 1000;
            ridesDB.insertmytrips_month_rideend(driverid, month_statrt, month_end, ridesDB.getmonthyearinstring(String.valueOf(timestamps)), completed, "", "", cancel, driver_cancel);
//            //week
            ridesDB.getupdateweekwiseridetablerideend(week_statrt, week_end, ridesDB.getmonthyearinstring(String.valueOf(timestamps)), ridedistance, "1");

            ridesDB.insertmytrips_weekrideend(driverid, week_statrt, week_end, ridesDB.getmonthinstring(String.valueOf(timestamps)), completed, "", "", cancel, driver_cancel);
////            //day
//            String starttimeofdates = ridesDB.getDate(String.valueOf(timestamps));
//            starttimeofdates = starttimeofdates + " 00:00:00";
//            String endtimeofdates = ridesDB.getDate(String.valueOf(timestamps));
//            endtimeofdates = endtimeofdates + " 24:00:00";
            ridesDB.insertmytrips_dayrideend(driverid, day_statrt, day_end, "AS " + obj.getJSONObject("response").getJSONObject("details").getString("trip_date"), completed, "", "", cancel, driver_cancel, ridedistance, "1");
////            //weektoday
//            String starttimeofdatesa = ridesDB.getDate(String.valueOf(timestamps));
//            starttimeofdatesa = starttimeofdatesa + " 00:00:00";
//            String endtimeofdate = ridesDB.getDate(String.valueOf(timestamps));
//            endtimeofdate = endtimeofdate + " 24:00:00";
            ridesDB.insertmytripsweektodayridend(day_statrt, day_end, ridesDB.convertdatetodayinstring(ridesDB.getDate(String.valueOf(timestamps))), "1", ridedistance, "", ridesDB.getDate(String.valueOf(timestamps)));
////            //weektodayagain
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void errorreponse() {
    }

    @Override
    public void jsonexception() {
    }
}
