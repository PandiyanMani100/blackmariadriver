package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class warningspojo implements Serializable {

    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;

    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }

    public class Response {
        @SerializedName("warning_count")
        private String warning_count;

        public ArrayList<Warning_list> getWarning_list() {
            return warning_list;
        }

        public void setWarning_list(ArrayList<Warning_list> warning_list) {
            this.warning_list = warning_list;
        }

        public String getWarning_count() {
            return warning_count;
        }

        public void setWarning_count(String warning_count) {
            this.warning_count = warning_count;
        }

        @SerializedName("warning_list")
        ArrayList < Warning_list > warning_list = new ArrayList< Warning_list >();

        public class Warning_list{
            @SerializedName("typeName")
            private String typeName;
            @SerializedName("fileName")
            private String fileName;
            @SerializedName("expiryDate")
            private String expiryDate;
            @SerializedName("id")
            private String id;
            @SerializedName("type")
            private String type;
            @SerializedName("text")
            private String text;


            // Getter Methods

            public String getTypeName() {
                return typeName;
            }

            public String getFileName() {
                return fileName;
            }

            public String getExpiryDate() {
                return expiryDate;
            }

            public String getId() {
                return id;
            }

            public String getType() {
                return type;
            }

            public String getText() {
                return text;
            }

            // Setter Methods

            public void setTypeName(String typeName) {
                this.typeName = typeName;
            }

            public void setFileName(String fileName) {
                this.fileName = fileName;
            }

            public void setExpiryDate(String expiryDate) {
                this.expiryDate = expiryDate;
            }

            public void setId(String id) {
                this.id = id;
            }

            public void setType(String type) {
                this.type = type;
            }

            public void setText(String text) {
                this.text = text;
            }
        }
        // Getter Methods

    }
}

