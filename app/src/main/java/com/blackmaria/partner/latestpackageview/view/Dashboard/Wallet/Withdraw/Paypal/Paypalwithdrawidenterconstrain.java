package com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Withdraw.Paypal;


import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.databinding.ActivityPaypalwithdrawidenterconstrainBinding;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Wallet.Withdraw.Paypal.WithdrawpaypalidenterFactory;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet.withdraw.paypal.PaypalwithdrawidenterViewModel;

public class Paypalwithdrawidenterconstrain extends AppCompatActivity {

    private ActivityPaypalwithdrawidenterconstrainBinding binding;
    private PaypalwithdrawidenterViewModel paypalwithdrawidenterViewModel;
    private AppUtils appUtils;
    private String transfer_amount = "", paypalcode = "",currency="";
    AccessLanguagefromlocaldb db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
        binding = DataBindingUtil.setContentView(Paypalwithdrawidenterconstrain.this, R.layout.activity_paypalwithdrawidenterconstrain);
        paypalwithdrawidenterViewModel = ViewModelProviders.of(this, new WithdrawpaypalidenterFactory(this)).get(PaypalwithdrawidenterViewModel.class);
        binding.setPaypalwithdrawidenterViewModel(paypalwithdrawidenterViewModel);
        paypalwithdrawidenterViewModel.setIds(binding);

        initView();
        clicklistener();

        binding.datemonthyear.setText(db.getvalue("fastwallet"));
        binding.withdraw.setText(db.getvalue("withdraw_lable"));
        binding.paypalid.setHint(db.getvalue("paypalidenter"));
        binding.transactionumber.setText(db.getvalue("withdrawcontent"));
        binding.confirm.setText(db.getvalue("confirm_lable"));
    }

    private void initView() {
        appUtils = AppUtils.getInstance(this);
        if (getIntent().hasExtra("transfer_amount")) {
            transfer_amount = getIntent().getStringExtra("transfer_amount");
            paypalcode = getIntent().getStringExtra("paypalcode");
            currency = getIntent().getStringExtra("currency");
        }
    }

    private void clicklistener() {
        binding.confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.paypalid.getText().toString().length() == 0) {
                    appUtils.AlertError(Paypalwithdrawidenterconstrain.this, db.getvalue("action_error"),db.getvalue("paypalidnotemepty"));
                } else if (!AppUtils.isValidEmail(binding.paypalid.getText().toString().trim().replace(" ", ""))) {
                    appUtils.AlertError(Paypalwithdrawidenterconstrain.this,db.getvalue("action_error"),db.getvalue("paypalinvalidid"));
                } else {
                    Intent i = new Intent(Paypalwithdrawidenterconstrain.this, Paypalamountconfirm.class);
                    i.putExtra("transfer_amount", transfer_amount);
                    i.putExtra("paypalcode", paypalcode);
                    i.putExtra("paypalid", binding.paypalid.getText().toString());
                    i.putExtra("currency", currency);
                    startActivity(i);
                }
            }
        });
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Log.v("TRIMMEMORY", " " + level);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }

}
