package com.blackmaria.partner.latestpackageview.view.Dashboard.Mytrips;

import android.app.Dialog;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.annotation.Nullable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.latestpackageview.Adapter.TripSummaryAdapter;
import com.blackmaria.partner.databinding.ActivityPastTripSummaryConstrainBinding;
import com.blackmaria.partner.latestpackageview.Database.RidesDB;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Mytrips.PasttripsummaryFactory;
import com.blackmaria.partner.latestpackageview.Model.tripsummarypojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.mytrips.PasttripsummaryViewModel;
import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

public class PastTripSummaryConstrain extends AppCompatActivity {

    private ActivityPastTripSummaryConstrainBinding binding;
    private PasttripsummaryViewModel pasttripsummaryViewModel;
    private SessionManager sessionManager;
    private String driverid = "";
    private tripsummarypojo object;
    private AppUtils appUtils;
    private RidesDB ridesDBobj;

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        binding = DataBindingUtil.setContentView(PastTripSummaryConstrain.this, R.layout.activity_past_trip_summary_constrain);
        pasttripsummaryViewModel = ViewModelProviders.of(this, new PasttripsummaryFactory(this)).get(PasttripsummaryViewModel.class);
        binding.setPasttripsummaryViewModel(pasttripsummaryViewModel);
        pasttripsummaryViewModel.setIds(binding);
        ridesDBobj = new RidesDB(this);

        String ridegetfromdb = ridesDBobj.getride(getIntent().getStringExtra("rideid"));
        if (!ridegetfromdb.equals("0")) {
            try {
                JSONObject jsonObject = new JSONObject(ridegetfromdb);
                Type type = new TypeToken<tripsummarypojo>() {
                }.getType();
                object = new GsonBuilder().create().fromJson(ridegetfromdb.toString(), type);
                setText(object);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            pasttripsummaryViewModel.gettripsummaryapihit(driverid, getIntent().getStringExtra("rideid"));
        }


        setTextresponse();
        clicklistener();
    }

    private void initView() {
        sessionManager = SessionManager.getInstance(this);
        appUtils = AppUtils.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        driverid = user.get(SessionManager.KEY_DRIVERID);
    }

    private void clicklistener() {
        binding.fareinfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFareInformation();
            }
        });

        binding.sendreciept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void showFareInformation() {

        final Dialog fareInfoDialog = new Dialog(PastTripSummaryConstrain.this, R.style.SlideUpDialog);
        fareInfoDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        fareInfoDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        fareInfoDialog.setCancelable(true);
        fareInfoDialog.setContentView(R.layout.alert_fare_information);

        ExpandableHeightListView Lv_fareDetails = (ExpandableHeightListView) fareInfoDialog.findViewById(R.id.lst_fare_details);
        Button btn_close = (Button) fareInfoDialog.findViewById(R.id.btn_close);

        TripSummaryAdapter fareAdapter = new TripSummaryAdapter(PastTripSummaryConstrain.this, object.getResponse().getDetails().getCurrency(), Color.BLACK, object.getResponse().getDetails().getFareArrL());
        Lv_fareDetails.setAdapter(fareAdapter);

        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fareInfoDialog != null && fareInfoDialog.isShowing()) {
                    fareInfoDialog.dismiss();
                }
            }
        });

        fareInfoDialog.show();

    }


    private void setTextresponse() {
        pasttripsummaryViewModel.getResponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Type type = new TypeToken<tripsummarypojo>() {
                    }.getType();
                    object = new GsonBuilder().create().fromJson(response.toString(), type);
                    setText(object);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    private void setText(final tripsummarypojo object) {

        AppUtils.setImageView(this, object.getResponse().getDetails().getCat_image(), binding.carimage);
        binding.tripdate.setText("Trip date " + object.getResponse().getDetails().getTrip_date());
        binding.pickupvalue.setText(object.getResponse().getDetails().getPickup().getLocation());
        binding.dropoffvalue.setText(object.getResponse().getDetails().getDrop().getLocation());
        binding.paymentmodze.setText(object.getResponse().getDetails().getPayment_mode());
        try {
            binding.bookingfare.removeAllViews();
        } catch (NullPointerException e) {
        }
        for (int i = 0; i <= object.getResponse().getDetails().getBooking_summary().size() - 1; i++) {
            View view = getLayoutInflater().inflate(R.layout.bookingfarecustomlayout, null);
            TextView pickup = view.findViewById(R.id.pickup);
            TextView pickupvalue = view.findViewById(R.id.pickupvalue);
            if(!object.getResponse().getDetails().getBooking_summary().get(i).getTitle().equalsIgnoreCase("fare") && !object.getResponse().getDetails().getBooking_summary().get(i).getTitle().equalsIgnoreCase("DISTANCE") && !object.getResponse().getDetails().getBooking_summary().get(i).getTitle().equalsIgnoreCase("duration"))
            {
                pickup.setText(object.getResponse().getDetails().getBooking_summary().get(i).getTitle());
                pickupvalue.setText(object.getResponse().getDetails().getBooking_summary().get(i).getValue());
                binding.bookingfare.addView(view);
            }


        }
    }

}
