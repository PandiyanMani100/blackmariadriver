package com.blackmaria.partner.latestpackageview.Utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.PowerManager;
import android.os.SystemClock;
import android.provider.Settings;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.latestpackageview.Database.GEODBHelper;
import com.blackmaria.partner.latestpackageview.Model.getaddress;
import com.blackmaria.partner.latestpackageview.Xmpp.MyXMPP;
import com.blackmaria.partner.mylibrary.latlnginterpolation.LatLngInterpolator;
import com.blackmaria.partner.mylibrary.latlnginterpolation.MarkerAnimation;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.devspark.appmsg.AppMsg;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.iid.FirebaseInstanceId;

import org.greenrobot.eventbus.EventBus;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;
import static android.content.Context.POWER_SERVICE;

public class AppUtils {


    public static AppUtils instance;
    private Context context;

    private gcmlisntener gcmlisntener;

    public long getstartdateofmonth(String sFilterMonth, String sFilterYear) {
        String DATE_FORMAT_U = "yyyy-MM-dd";
        Calendar calendar = Calendar.getInstance();
        int yearpart = Integer.parseInt(sFilterYear);
        int monthPart = Integer.parseInt(sFilterMonth);
        int dateDay = 1;
        calendar.set(yearpart, monthPart - 1, dateDay);
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_U);
        return getdatetotiestamp(dateFormat.format(calendar.getTime()));
    }

    public long getenddateofmonth(String sFilterMonth, String sFilterYear) {
        String DATE_FORMAT_U = "yyyy-MM-dd";
        Calendar calendar = Calendar.getInstance();
        int yearpart = Integer.parseInt(sFilterYear);
        int monthPart = Integer.parseInt(sFilterMonth);
        int dateDay = 31;
        calendar.set(yearpart, monthPart - 1, dateDay);
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_U);
        return getdatetotiestamp(dateFormat.format(calendar.getTime()));
    }

    public interface gcmlisntener {
        void getgcmvalue(String gcm);
    }

    public AppUtils(Context context) {
        this.context = context;
    }

    public AppUtils(String val, gcmlisntener gcmlisntener) {
        this.gcmlisntener = gcmlisntener;
    }

    public static AppUtils getInstance(Activity context) {
        if (instance == null) {
            instance = new AppUtils(context);
        }

        return instance;
    }

    public void checkxmpp(Context context) {
        SessionManager sessionManager = SessionManager.getInstance(context);
        HashMap<String, String> domain = sessionManager.getXmpp();
        String ServiceName = domain.get(SessionManager.KEY_HOST_NAME);
        String HostAddress = domain.get(SessionManager.KEY_HOST_URL);
        HashMap<String, String> user = sessionManager.getUserDetails();
        String USERNAME = user.get(SessionManager.KEY_DRIVERID);
        String PASSWORD = user.get(SessionManager.KEY_SEC_KEY);
        if (sessionManager.getUserloggedIn()) {
            MyXMPP xmpp = MyXMPP.getInstance(context, ServiceName, HostAddress, USERNAME, PASSWORD);
            try {
                xmpp.connect("onCreate");
            } catch (IOException e) {
                e.printStackTrace();
            } catch (XMPPException e) {
                e.printStackTrace();
            } catch (SmackException e) {
                e.printStackTrace();
            }
        }
    }

    public void sendloctouseronride(Context context, String hostname, String userid, String lat, String lon, float bearingValue, String ride_id) {
        try {
            SessionManager sessionManager = SessionManager.getInstance(context);
            HashMap<String, String> domain = sessionManager.getXmpp();
            String ServiceName = domain.get(SessionManager.KEY_HOST_NAME);
            String HostAddress = domain.get(SessionManager.KEY_HOST_URL);
            HashMap<String, String> user = sessionManager.getUserDetails();
            String USERNAME = user.get(SessionManager.KEY_DRIVERID);
            String PASSWORD = user.get(SessionManager.KEY_SEC_KEY);

            MyXMPP myXMPP = MyXMPP.getInstance(context, ServiceName, HostAddress, USERNAME, PASSWORD);
            String chatID = userid + "@" + hostname;
            JSONObject job = new JSONObject();
            job.put("action", "driver_loc");
            job.put("latitude", lat);
            job.put("longitude", lon);
            job.put("bearing", bearingValue);
            job.put("ride_id", ride_id);
            int status = myXMPP.sendMessage(chatID, job.toString());
            if (status == 0) {
                toastprint(context, "Driver Location not send to user onride");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String addressfetch(Context context, String lat, String lon) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(Double.parseDouble(lat), Double.parseDouble(lon), 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
//                StringBuilder strReturnedAddress = new StringBuilder("");
//                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
//                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
//                }
//                strAdd = strReturnedAddress.toString();
                getaddress add = new getaddress();
                add.setAddress(returnedAddress.getAddressLine(0));
                EventBus.getDefault().post(add);
            } else {
                strAdd = "error";
                Log.e("Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            strAdd = "error";
            getaddress add = new getaddress();
            add.setAddress(strAdd);
            EventBus.getDefault().post(add);
            Log.e("Current loction address", "Cannot get Address!");
        }
        return strAdd;
    }

    public float calculateDistance(ArrayList<LatLng> points) {

        float tempTotalDistance = 0;

        for (int i = 0; i < points.size() - 1; i++) {
            LatLng pointA = points.get(i);
            LatLng pointB = points.get(i + 1);
            float[] results = new float[3];
            Location.distanceBetween(pointA.latitude, pointA.longitude, pointB.latitude, pointB.longitude, results);
            tempTotalDistance += results[0];
        }

        return tempTotalDistance;
    }



    public long getdatetotiestamp(String str_date) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = (Date) formatter.parse(str_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date.getTime();
    }

    public long getdatetotiestampwithtime(String str_date) {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = (Date) formatter.parse(str_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Timestamp fromTS1 = new Timestamp(date.getTime());
        return fromTS1.getTime();
    }

    public float findbearing(double startlat, double startlon, double endlat, double endlon) {
        float bearingValue = 0;
        Location startLocation = new Location(LocationManager.GPS_PROVIDER);
        startLocation.setLatitude(startlat);
        startLocation.setLongitude(startlon);
        Location endLocationBearing = new Location(LocationManager.GPS_PROVIDER);
        endLocationBearing.setLatitude(endlat);
        endLocationBearing.setLongitude(endlon);
        bearingValue = startLocation.bearingTo(endLocationBearing);
        return bearingValue;
    }

    public void updateCameraBearing(GoogleMap googleMap, float bearing) {
        if (googleMap == null) return;
        CameraPosition camPos = CameraPosition
                .builder(googleMap.getCameraPosition())
                .bearing(bearing)
                .build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(camPos));
    }

    public void call(Context context, String no) {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + no));
        context.startActivity(intent);
    }

    public static void setImageView(Context context, String url, ImageView imageView) {
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.new_no_user_img)
                .error(R.drawable.new_no_user_img)
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        Glide.with(context).load(url).apply(options).into(imageView);
    }

    public static void setImageViewd(Context context, String url, ImageView imageView) {
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .override(400, 400)
                .fitCenter()
                .placeholder(R.drawable.new_no_user_img)
                .error(R.drawable.new_no_user_img)
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        Glide.with(context).load(url).apply(options).into(imageView);
    }


    public static void setImageViewqr(Context context, String url, ImageView imageView) {
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.qr_sm)
                .error(R.drawable.qr_sm)
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        if (url.equalsIgnoreCase("")) {
            Glide.with(context).load(R.drawable.qr_sm).apply(options).into(imageView);
        } else {
            Glide.with(context).load(url).apply(options).into(imageView);
        }

    }

    public static void carsetImageView(Context context, int url, ImageView imageView) {
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.car_menu)
                .error(R.drawable.new_no_user_img)
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        Glide.with(context).load(url).apply(options).into(imageView);
    }

    public static void setImageviewwithoutcrop(Context context, String url, ImageView imageView) {
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.car_menu)
                .error(R.drawable.car_menu)
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        Glide.with(context).load(url).apply(options).into(imageView);
    }

    public static void setImageviewwithoutcropplaceholder(Context context, String url, ImageView imageView) {
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.card_icon)
                .error(R.drawable.card_icon)
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        Glide.with(context).load(url).apply(options).into(imageView);
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    // validating Phone Number
    public static final boolean isValidPhoneNumber(CharSequence target) {
        if (target == null || TextUtils.isEmpty(target) || target.length() <= 5) {
            return false;
        } else {
            return android.util.Patterns.PHONE.matcher(target).matches();
        }
    }

    public void intentbrowser(Context context, String link) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(link));
        context.startActivity(i);
    }

    public static boolean isAppinstalled(Context context, String name) {
        try {
            context.getPackageManager().getApplicationInfo(name, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static int getResId(String drawableName) {

        try {
            Class<com.blackmaria.partner.R.drawable> res = R.drawable.class;
            Field field = res.getField(drawableName);
            int drawableId = field.getInt(null);
            return drawableId;
        } catch (Exception e) {
            Log.e("CountryCodePicker", "Failure to get drawable id.", e);
        }
        return -1;
    }

    @SuppressLint("WrongConstant")
    public static void CloseKeyboard(Activity context) {
        context.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public void runbackground(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Intent intent = new Intent();
            String packageName = context.getPackageName();
            PowerManager pm = (PowerManager) context.getSystemService(POWER_SERVICE);
            if (!pm.isIgnoringBatteryOptimizations(packageName)) {
                intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                intent.setData(Uri.parse("package:" + packageName));
                context.startActivity(intent);
            }
        }
    }

    public void AlertError(Activity context, String title, String message) {
        String msg = title + "\n" + message;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        AppMsg.Style style = new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.red_color);
        AppMsg snack = AppMsg.makeText(context, msg.toUpperCase(), AppMsg.STYLE_ALERT);
        snack.setView(view);
        snack.setLayoutGravity(Gravity.TOP);
        snack.setPriority(AppMsg.PRIORITY_HIGH);
        snack.setAnimation(R.anim.slide_down_anim, R.anim.slide_up_anim);
        snack.show();
    }

    public void ridestatuschange(SessionManager sessionManager, String ridestatus) {
        try {
            JSONObject jsonObject1 = new JSONObject(sessionManager.getContinuetripjson());
            jsonObject1.getJSONObject("response").getJSONObject("user_profile").put("ride_status", ridestatus);
            sessionManager.setContinuetripjson(jsonObject1.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void AlertSuccess(Activity context, String title, String message) {
        String msg = title + "\n" + message;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = view.findViewById(R.id.txt_title);
        TextView Tv_message = view.findViewById(R.id.txt_message);
        RelativeLayout relative = view.findViewById(R.id.relative);
        relative.setBackgroundColor(context.getResources().getColor(R.color.btn_green_color));

        Tv_title.setText(title);
        Tv_message.setText(message);

        AppMsg.Style style = new AppMsg.Style(AppMsg.LENGTH_SHORT, R.color.btn_green_color);
        AppMsg snack = AppMsg.makeText(context, msg.toUpperCase(), AppMsg.STYLE_ALERT);
        snack.setView(view);
        snack.setLayoutGravity(Gravity.TOP);
        snack.setPriority(AppMsg.PRIORITY_HIGH);
        snack.setAnimation(R.anim.slide_down_anim, R.anim.slide_up_anim);
        snack.show();
    }


    public void Gcminitializer(Context context) {
        try {
            gcmlisntener.getgcmvalue(FirebaseInstanceId.getInstance().getToken());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void toastprint(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public void loadinganimation(ImageView imageView) {
        final Animation animation = new AlphaAnimation(1, 0);
        animation.setDuration(500);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
        imageView.startAnimation(animation);
    }

    public static void rotateMarker(final Marker marker, final float toRotation, GoogleMap map) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final float startRotation = marker.getRotation();
        final long duration = 1555;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed / duration);

                float rot = t * toRotation + (1 - t) * startRotation;

                marker.setRotation(-rot > 180 ? rot / 2 : rot);
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
            }
        });
    }

    public Marker Addmarker(GoogleMap googleMap, LatLng latLng, Location location, BitmapDescriptor carIcon) {

        if(latLng != null)
        {
            Marker carMarker = googleMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .icon(carIcon)
                    .anchor(0.5f, 0.5f)
                    .rotation(location.getBearing())
                    .flat(true));
            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(17).build();
            CameraUpdate camUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
            googleMap.moveCamera(camUpdate);
            return carMarker;
        }

        return null;

    }

    public Marker Addmarkeronride(GoogleMap googleMap, LatLng latLng, Location location, BitmapDescriptor carIcon) {
        Marker carMarker = googleMap.addMarker(new MarkerOptions()
                .position(latLng)
                .icon(carIcon)
                .anchor(0.5f, 0.5f)
                .rotation(location.getBearing())
                .flat(true));
        float zoom = googleMap.getCameraPosition().zoom;
        CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(zoom).build();
        CameraUpdate camUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        googleMap.moveCamera(camUpdate);
        return carMarker;
    }

    public void movemarker(Location myLocation, float bearingValue, Marker carMarker, GoogleMap googleMap, LatLng latLng, LatLngInterpolator mLatLngInterpolator) {
        if (!String.valueOf(bearingValue).equalsIgnoreCase("NaN")) {
            rotateMarker(carMarker, bearingValue, googleMap);
            MarkerAnimation.animateMarkerToGB(carMarker, latLng, mLatLngInterpolator);
            float zoom = googleMap.getCameraPosition().zoom;
            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(zoom).build();
            googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    public void animateCamera( GoogleMap googleMap, LatLng latLng, float zoom){
        CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(zoom).build();
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    public void movemarkeronride(Location myLocation, float bearingValue, Marker carMarker, GoogleMap googleMap, LatLng latLng, LatLngInterpolator mLatLngInterpolator) {
        if (!String.valueOf(bearingValue).equalsIgnoreCase("NaN")) {
            rotateMarker(carMarker, bearingValue, googleMap);
            MarkerAnimation.animateMarkerToGB(carMarker, latLng, mLatLngInterpolator);
            float zoom = googleMap.getCameraPosition().zoom;
            CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(zoom).build();
            googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    public void createMarker(GoogleMap googleMap, double latitude, double longitude, String title, int iconResID) {
        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .anchor(0.5f, 0.5f)
                .title(title)
                .icon(BitmapDescriptorFactory.fromResource(iconResID)));
    }


    public static boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
        boolean b = false;
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                b = true;
                break;
            } else {
                b = false;
            }
        }
        System.out.println("3 not running");
        return b;
    }

    public void navigationtogooglemap(Context context, Double latitude, Double longitude, String pickup_lat, String pickup_lon) {
//        Uri gmmIntentUri = Uri.parse("google.navigation:q="+address);
//        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
//        mapIntent.setPackage("com.google.android.apps.maps");
//        context.startActivity(mapIntent);
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?saddr=" + latitude + "," + longitude + "&daddr=" + pickup_lat + "," + pickup_lon + ""));
        intent.setPackage("com.google.android.apps.maps");
        context.startActivity(intent);

    }

    public void insertdriverlatlononride(GEODBHelper geodbHelper, String lat, String lon, String rideid) {
        Calendar aCalendar = Calendar.getInstance();
        SimpleDateFormat aDateFormat = new SimpleDateFormat("HH:mm:ss");
        String aCurrentDate = aDateFormat.format(aCalendar.getTime());
        if (!lat.equalsIgnoreCase("")) {
            geodbHelper.insertLatLong(rideid, Double.parseDouble(lat), Double.parseDouble(lon), aCurrentDate);
        }
    }

    public void erroredit(Context context, EditText editname, String msg) {
        Animation shake = AnimationUtils.loadAnimation(context, R.anim.shake);
        editname.startAnimation(shake);
        ForegroundColorSpan fgcspan = new ForegroundColorSpan(Color.parseColor("#CC0000"));
        SpannableStringBuilder ssbuilder = new SpannableStringBuilder(msg);
        ssbuilder.setSpan(fgcspan, 0, msg.length(), 0);
        editname.setError(ssbuilder);
    }

    public void CreateXendittoken() {

    }

}
