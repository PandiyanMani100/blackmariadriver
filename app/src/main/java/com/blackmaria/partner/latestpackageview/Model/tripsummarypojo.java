package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class tripsummarypojo implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }

    public class Response {
        @SerializedName("details")
        Details DetailsObject;


        // Getter Methods

        public Details getDetails() {
            return DetailsObject;
        }

        // Setter Methods

        public void setDetails(Details detailsObject) {
            this.DetailsObject = detailsObject;
        }

        public class Details {
            @SerializedName("currency")
            private String currency;
            @SerializedName("cab_type")
            private String cab_type;
            @SerializedName("ride_id")
            private String ride_id;
            @SerializedName("ride_status")
            private String ride_status;
            @SerializedName("disp_status")
            private String disp_status;
            @SerializedName("do_cancel_action")
            private String do_cancel_action;
            @SerializedName("do_track_action")
            private String do_track_action;
            @SerializedName("is_fav_location")
            private String is_fav_location;
            @SerializedName("pay_status")
            private String pay_status;
            @SerializedName("disp_pay_status")
            private String disp_pay_status;
            @SerializedName("pickup")
            Pickup PickupObject;
            @SerializedName("drop")
            Drop DropObject;
            @SerializedName("pickup_date")
            private String pickup_date;
            @SerializedName("drop_date")
            private String drop_date;
            @SerializedName("summary")
            Summary SummaryObject;
            @SerializedName("fare")
            Fare FareObject;

            public ArrayList<TripArr> getTripArr() {
                return tripArr;
            }

            public void setTripArr(ArrayList<TripArr> tripArr) {
                this.tripArr = tripArr;
            }

            @SerializedName("tripArr")
            ArrayList < TripArr > tripArr = new ArrayList < TripArr > ();

            public ArrayList<FareArrL> getFareArrL() {
                return fareArrL;
            }

            public void setFareArrL(ArrayList<FareArrL> fareArrL) {
                this.fareArrL = fareArrL;
            }
            @SerializedName("fareArrL")
            ArrayList < FareArrL > fareArrL = new ArrayList < FareArrL > ();
            @SerializedName("distance_unit")
            private String distance_unit;
            @SerializedName("invoice_src")
            private String invoice_src;

            public class FareArrL{
                @SerializedName("title")
                private String title;
                @SerializedName("value")
                private String value;
                @SerializedName("currency_status")
                private String currency_status;


                // Getter Methods

                public String getTitle() {
                    return title;
                }

                public String getValue() {
                    return value;
                }

                public String getCurrency_status() {
                    return currency_status;
                }

                // Setter Methods

                public void setTitle(String title) {
                    this.title = title;
                }

                public void setValue(String value) {
                    this.value = value;
                }

                public void setCurrency_status(String currency_status) {
                    this.currency_status = currency_status;
                }
            }
            
            public class TripArr{
                @SerializedName("title")
                private String title;
                @SerializedName("value")
                private String value;
                @SerializedName("currency_status")
                private String currency_status;


                // Getter Methods

                public String getTitle() {
                    return title;
                }

                public String getValue() {
                    return value;
                }

                public String getCurrency_status() {
                    return currency_status;
                }

                // Setter Methods

                public void setTitle(String title) {
                    this.title = title;
                }

                public void setValue(String value) {
                    this.value = value;
                }

                public void setCurrency_status(String currency_status) {
                    this.currency_status = currency_status;
                }
            }

            public ArrayList<Booking_summary> getBooking_summary() {
                return booking_summary;
            }

            public void setBooking_summary(ArrayList<Booking_summary> booking_summary) {
                this.booking_summary = booking_summary;
            }

            @SerializedName("booking_summary")
            ArrayList < Booking_summary > booking_summary = new ArrayList < Booking_summary > ();

            public class Booking_summary{
                @SerializedName("title")
                private String title;
                @SerializedName("value")
                private String value;
                @SerializedName("currency_status")
                private String currency_status;


                // Getter Methods

                public String getTitle() {
                    return title;
                }

                public String getValue() {
                    return value;
                }

                public String getCurrency_status() {
                    return currency_status;
                }

                // Setter Methods

                public void setTitle(String title) {
                    this.title = title;
                }

                public void setValue(String value) {
                    this.value = value;
                }

                public void setCurrency_status(String currency_status) {
                    this.currency_status = currency_status;
                }

            }
            @SerializedName("payment_mode")
            private String payment_mode;
            @SerializedName("comp_status")
            private String comp_status;
            @SerializedName("comp_stage")
            private String comp_stage;
            @SerializedName("driver_request_mode")
            private String driver_request_mode;
            @SerializedName("trip_date")
            private String trip_date;
            @SerializedName("pickup_time")
            private String pickup_time;
            @SerializedName("drop_time")
            private String drop_time;
            @SerializedName("driver_name")
            private String driver_name;
            @SerializedName("vehicle_number")
            private String vehicle_number;
            @SerializedName("trip_fare")
            private String trip_fare;
            @SerializedName("cat_image")
            private String cat_image;
            @SerializedName("total_passenger")
            private String total_passenger;
            @SerializedName("trip_type")
            private String trip_type;
            @SerializedName("multi_stops_status")
            private String multi_stops_status;

            public ArrayList<Multi_stops> getMulti_stops() {
                return multi_stops;
            }

            public void setMulti_stops(ArrayList<Multi_stops> multi_stops) {
                this.multi_stops = multi_stops;
            }
            @SerializedName("multi_stops")
            ArrayList< Multi_stops > multi_stops = new ArrayList < Multi_stops > ();


            public class Multi_stops{
                @SerializedName("location")
                private String location;
                @SerializedName("latlong")
                Latlong LatlongObject;
                @SerializedName("record_id")
                private String record_id;
                @SerializedName("status")
                private String status;


                // Getter Methods

                public String getLocation() {
                    return location;
                }

                public Latlong getLatlong() {
                    return LatlongObject;
                }

                public String getRecord_id() {
                    return record_id;
                }

                public String getStatus() {
                    return status;
                }

                // Setter Methods

                public void setLocation(String location) {
                    this.location = location;
                }

                public void setLatlong(Latlong latlongObject) {
                    this.LatlongObject = latlongObject;
                }

                public void setRecord_id(String record_id) {
                    this.record_id = record_id;
                }

                public void setStatus(String status) {
                    this.status = status;
                }

                public class Latlong {
                    @SerializedName("lat")
                    private String lat;
                    @SerializedName("lon")
                    private String lon;


                    // Getter Methods

                    public String getLat() {
                        return lat;
                    }

                    public String getLon() {
                        return lon;
                    }

                    // Setter Methods

                    public void setLat(String lat) {
                        this.lat = lat;
                    }

                    public void setLon(String lon) {
                        this.lon = lon;
                    }
                }
            }

            // Getter Methods

            public String getCurrency() {
                return currency;
            }

            public String getCab_type() {
                return cab_type;
            }

            public String getRide_id() {
                return ride_id;
            }

            public String getRide_status() {
                return ride_status;
            }

            public String getDisp_status() {
                return disp_status;
            }

            public String getDo_cancel_action() {
                return do_cancel_action;
            }

            public String getDo_track_action() {
                return do_track_action;
            }

            public String getIs_fav_location() {
                return is_fav_location;
            }

            public String getPay_status() {
                return pay_status;
            }

            public String getDisp_pay_status() {
                return disp_pay_status;
            }

            public Pickup getPickup() {
                return PickupObject;
            }

            public Drop getDrop() {
                return DropObject;
            }

            public String getPickup_date() {
                return pickup_date;
            }

            public String getDrop_date() {
                return drop_date;
            }

            public Summary getSummary() {
                return SummaryObject;
            }

            public Fare getFare() {
                return FareObject;
            }

            public String getDistance_unit() {
                return distance_unit;
            }

            public String getInvoice_src() {
                return invoice_src;
            }

            public String getPayment_mode() {
                return payment_mode;
            }

            public String getComp_status() {
                return comp_status;
            }

            public String getComp_stage() {
                return comp_stage;
            }

            public String getDriver_request_mode() {
                return driver_request_mode;
            }

            public String getTrip_date() {
                return trip_date;
            }

            public String getPickup_time() {
                return pickup_time;
            }

            public String getDrop_time() {
                return drop_time;
            }

            public String getDriver_name() {
                return driver_name;
            }

            public String getVehicle_number() {
                return vehicle_number;
            }

            public String getTrip_fare() {
                return trip_fare;
            }

            public String getCat_image() {
                return cat_image;
            }

            public String getTotal_passenger() {
                return total_passenger;
            }

            public String getTrip_type() {
                return trip_type;
            }

            public String getMulti_stops_status() {
                return multi_stops_status;
            }

            // Setter Methods

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public void setCab_type(String cab_type) {
                this.cab_type = cab_type;
            }

            public void setRide_id(String ride_id) {
                this.ride_id = ride_id;
            }

            public void setRide_status(String ride_status) {
                this.ride_status = ride_status;
            }

            public void setDisp_status(String disp_status) {
                this.disp_status = disp_status;
            }

            public void setDo_cancel_action(String do_cancel_action) {
                this.do_cancel_action = do_cancel_action;
            }

            public void setDo_track_action(String do_track_action) {
                this.do_track_action = do_track_action;
            }

            public void setIs_fav_location(String is_fav_location) {
                this.is_fav_location = is_fav_location;
            }

            public void setPay_status(String pay_status) {
                this.pay_status = pay_status;
            }

            public void setDisp_pay_status(String disp_pay_status) {
                this.disp_pay_status = disp_pay_status;
            }

            public void setPickup(Pickup pickupObject) {
                this.PickupObject = pickupObject;
            }

            public void setDrop(Drop dropObject) {
                this.DropObject = dropObject;
            }

            public void setPickup_date(String pickup_date) {
                this.pickup_date = pickup_date;
            }

            public void setDrop_date(String drop_date) {
                this.drop_date = drop_date;
            }

            public void setSummary(Summary summaryObject) {
                this.SummaryObject = summaryObject;
            }

            public void setFare(Fare fareObject) {
                this.FareObject = fareObject;
            }

            public void setDistance_unit(String distance_unit) {
                this.distance_unit = distance_unit;
            }

            public void setInvoice_src(String invoice_src) {
                this.invoice_src = invoice_src;
            }

            public void setPayment_mode(String payment_mode) {
                this.payment_mode = payment_mode;
            }

            public void setComp_status(String comp_status) {
                this.comp_status = comp_status;
            }

            public void setComp_stage(String comp_stage) {
                this.comp_stage = comp_stage;
            }

            public void setDriver_request_mode(String driver_request_mode) {
                this.driver_request_mode = driver_request_mode;
            }

            public void setTrip_date(String trip_date) {
                this.trip_date = trip_date;
            }

            public void setPickup_time(String pickup_time) {
                this.pickup_time = pickup_time;
            }

            public void setDrop_time(String drop_time) {
                this.drop_time = drop_time;
            }

            public void setDriver_name(String driver_name) {
                this.driver_name = driver_name;
            }

            public void setVehicle_number(String vehicle_number) {
                this.vehicle_number = vehicle_number;
            }

            public void setTrip_fare(String trip_fare) {
                this.trip_fare = trip_fare;
            }

            public void setCat_image(String cat_image) {
                this.cat_image = cat_image;
            }

            public void setTotal_passenger(String total_passenger) {
                this.total_passenger = total_passenger;
            }

            public void setTrip_type(String trip_type) {
                this.trip_type = trip_type;
            }

            public void setMulti_stops_status(String multi_stops_status) {
                this.multi_stops_status = multi_stops_status;
            }

            public class Fare {
                @SerializedName("total_bill")
                private String total_bill;
                @SerializedName("coupon_discount")
                private String coupon_discount;
                @SerializedName("grand_bill")
                private String grand_bill;
                @SerializedName("total_paid")
                private String total_paid;
                @SerializedName("wallet_usage")
                private String wallet_usage;
                @SerializedName("tips_amount")
                private String tips_amount;


                // Getter Methods

                public String getTotal_bill() {
                    return total_bill;
                }

                public String getCoupon_discount() {
                    return coupon_discount;
                }

                public String getGrand_bill() {
                    return grand_bill;
                }

                public String getTotal_paid() {
                    return total_paid;
                }

                public String getWallet_usage() {
                    return wallet_usage;
                }

                public String getTips_amount() {
                    return tips_amount;
                }

                // Setter Methods

                public void setTotal_bill(String total_bill) {
                    this.total_bill = total_bill;
                }

                public void setCoupon_discount(String coupon_discount) {
                    this.coupon_discount = coupon_discount;
                }

                public void setGrand_bill(String grand_bill) {
                    this.grand_bill = grand_bill;
                }

                public void setTotal_paid(String total_paid) {
                    this.total_paid = total_paid;
                }

                public void setWallet_usage(String wallet_usage) {
                    this.wallet_usage = wallet_usage;
                }

                public void setTips_amount(String tips_amount) {
                    this.tips_amount = tips_amount;
                }


            }

            public class Summary {
                @SerializedName("ride_distance")
                private String ride_distance;
                @SerializedName("device_distance")
                private String device_distance;
                @SerializedName("math_distance")
                private String math_distance;
                @SerializedName("ride_duration")
                private String ride_duration;
                @SerializedName("waiting_duration")
                private String waiting_duration;
                @SerializedName("comp_wait_time")
                private String comp_wait_time;


                // Getter Methods

                public String getRide_distance() {
                    return ride_distance;
                }

                public String getDevice_distance() {
                    return device_distance;
                }

                public String getMath_distance() {
                    return math_distance;
                }

                public String getRide_duration() {
                    return ride_duration;
                }

                public String getWaiting_duration() {
                    return waiting_duration;
                }

                public String getComp_wait_time() {
                    return comp_wait_time;
                }

                // Setter Methods

                public void setRide_distance(String ride_distance) {
                    this.ride_distance = ride_distance;
                }

                public void setDevice_distance(String device_distance) {
                    this.device_distance = device_distance;
                }

                public void setMath_distance(String math_distance) {
                    this.math_distance = math_distance;
                }

                public void setRide_duration(String ride_duration) {
                    this.ride_duration = ride_duration;
                }

                public void setWaiting_duration(String waiting_duration) {
                    this.waiting_duration = waiting_duration;
                }

                public void setComp_wait_time(String comp_wait_time) {
                    this.comp_wait_time = comp_wait_time;
                }


            }

            public class Drop {
                @SerializedName("location")
                private String location;
                @SerializedName("latlong")
                Latlong LatlongObject;


                // Getter Methods

                public String getLocation() {
                    return location;
                }

                public Latlong getLatlong() {
                    return LatlongObject;
                }

                // Setter Methods

                public void setLocation(String location) {
                    this.location = location;
                }

                public void setLatlong(Latlong latlongObject) {
                    this.LatlongObject = latlongObject;
                }
                public class Latlong {
                    @SerializedName("lon")
                    private String lon;
                    @SerializedName("lat")
                    private String lat;


                    // Getter Methods

                    public String getLon() {
                        return lon;
                    }

                    public String getLat() {
                        return lat;
                    }

                    // Setter Methods

                    public void setLon(String lon) {
                        this.lon = lon;
                    }

                    public void setLat(String lat) {
                        this.lat = lat;
                    }


                }

            }

            public class Pickup {
                @SerializedName("location")
                private String location;
                @SerializedName("latlong")
                Latlong LatlongObject;


                // Getter Methods

                public String getLocation() {
                    return location;
                }

                public Latlong getLatlong() {
                    return LatlongObject;
                }

                // Setter Methods

                public void setLocation(String location) {
                    this.location = location;
                }

                public void setLatlong(Latlong latlongObject) {
                    this.LatlongObject = latlongObject;
                }

                public class Latlong {
                    @SerializedName("lon")
                    private String lon;
                    @SerializedName("lat")
                    private String lat;


                    // Getter Methods

                    public String getLon() {
                        return lon;
                    }

                    public String getLat() {
                        return lat;
                    }

                    // Setter Methods

                    public void setLon(String lon) {
                        this.lon = lon;
                    }

                    public void setLat(String lat) {
                        this.lat = lat;
                    }

                }
            }
        }
    }
}







