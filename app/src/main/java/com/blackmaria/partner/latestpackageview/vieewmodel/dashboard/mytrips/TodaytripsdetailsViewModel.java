package com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.mytrips;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityMytripsPaginationConstrainBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;

import java.util.HashMap;

public class TodaytripsdetailsViewModel extends ViewModel implements ApIServices.completelisner {

    private Activity context;
    public ActivityMytripsPaginationConstrainBinding binding;
    private Dialog dialog;
    private MutableLiveData<String> todaytrips = new MutableLiveData<>();
    private MutableLiveData<String> weektrips = new MutableLiveData<>();
    private MutableLiveData<String> monthtrips = new MutableLiveData<>();
    private int count = 0;
    private SessionManager sessionManager;
    private String driverid = "", sFilterDate = "";

    public TodaytripsdetailsViewModel(Activity context) {
        this.context = context;
        sessionManager = SessionManager.getInstance(context);
        HashMap<String, String> user = sessionManager.getUserDetails();
        driverid = user.get(SessionManager.KEY_DRIVERID);
    }

    public void setIds(ActivityMytripsPaginationConstrainBinding binding) {
        this.binding = binding;
    }

    public MutableLiveData<String> getMonthtrips() {
        return monthtrips;
    }

    public MutableLiveData<String> getTodaytrips() {
        return todaytrips;
    }

    public MutableLiveData<String> getWeektrips() {
        return weektrips;
    }

    public void back() {
        context.onBackPressed();
    }

    public void todaytripsapi(String sFilterDate,boolean value) {
        count = 1;
        if(value) {
            binding.apiload.setVisibility(View.VISIBLE);
            dialog = new Dialog(context, R.style.CustomDialogTheme);
            dialog.getWindow();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.loading_model);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", driverid);
        jsonParams.put("filter_date", sFilterDate);

        ApIServices apIServices = new ApIServices(context, TodaytripsdetailsViewModel.this);
        apIServices.dooperation(Iconstant.today_trips_url, jsonParams);
    }

    public void weektripsapi(String sFilterDateFrom, String sFilterDateTo,boolean value) {
        count = 2;
        if(value) {
            binding.apiload.setVisibility(View.VISIBLE);
            dialog = new Dialog(context, R.style.CustomDialogTheme);
            dialog.getWindow();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.loading_model);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", driverid);
        jsonParams.put("filter_date_from", sFilterDateFrom);
        jsonParams.put("filter_date_to", sFilterDateTo);

        ApIServices apIServices = new ApIServices(context, TodaytripsdetailsViewModel.this);
        apIServices.dooperation(Iconstant.week_trips_url, jsonParams);
    }

    public void monthtrips(String sFilterDateFrom, String sFilterDateTo,boolean value) {
        count = 3;
        if(value) {
            binding.apiload.setVisibility(View.VISIBLE);
            dialog = new Dialog(context, R.style.CustomDialogTheme);
            dialog.getWindow();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.loading_model);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", driverid);
        jsonParams.put("filter_month", sFilterDateFrom);
        jsonParams.put("filter_year", sFilterDateTo);

        ApIServices apIServices = new ApIServices(context, TodaytripsdetailsViewModel.this);
        apIServices.dooperation(Iconstant.month_trips_url, jsonParams);
    }

    @Override
    public void sucessresponse(String val) {
        if (dialog!=null){
            dialog.dismiss();
            binding.apiload.setVisibility(View.INVISIBLE);
        }

        if (count == 1) {
            todaytrips.postValue(val);
        } else if (count == 2) {
            weektrips.postValue(val);
        } else if (count == 3) {
            monthtrips.postValue(val);
        }
    }

    @Override
    public void errorreponse() {
        if(dialog!=null){
            dialog.dismiss();
        }
        if(binding.apiload.getVisibility() == View.VISIBLE){
            binding.apiload.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public void jsonexception() {
        if(dialog!=null){
            dialog.dismiss();
        }
        if(binding.apiload.getVisibility() == View.VISIBLE){
            binding.apiload.setVisibility(View.INVISIBLE);
        }

    }
}
