package com.blackmaria.partner.latestpackageview.vieewmodel.signup;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityRegistrationSelectLocationConstrainBinding;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.iconstant.Iconstant;

import java.util.HashMap;

public class RegistrationSelectLocationViewModel extends ViewModel  implements ApIServices.completelisner{

    private Activity context;
    private SessionManager sessionManager;
    private ConnectionDetector cd;
    private String sDriverID = "";
    private boolean isInternetPresent = false;

    private Dialog dialog;
    private MutableLiveData<String>getlocationlist =new MutableLiveData<>();
    private MutableLiveData<String>locationbasedcategory =new MutableLiveData<>();
    private int count =0;
    private ActivityRegistrationSelectLocationConstrainBinding binding;


    public RegistrationSelectLocationViewModel(Activity context) {
        this.context = context;
        sessionManager = SessionManager.getInstance(context);
        cd = new ConnectionDetector(context);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
    }


    public void backclick(){
        context.onBackPressed();
    }


    public void getlocationslist(){
        count = 0;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        ApIServices apIServices = new ApIServices(context, RegistrationSelectLocationViewModel.this);
        apIServices.dooperation(Iconstant.getlocationlist, jsonParams);
    }

    public void getlocationbasedcateogry(String Url, String Slatitude, String Slongitude){
        count = 1;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("lat", Slatitude);
        jsonParams.put("lon", Slongitude);
        ApIServices apIServices = new ApIServices(context, RegistrationSelectLocationViewModel.this);
        apIServices.dooperation(Url, jsonParams);
    }

    public MutableLiveData<String> getGetlocationlist() {
        return getlocationlist;
    }

    public MutableLiveData<String> getLocationbasedcategory() {
        return locationbasedcategory;
    }

    @Override
    public void sucessresponse(String val) {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        if(count ==0){
            getlocationlist.postValue(val);
        }else if(count ==1){
            locationbasedcategory.postValue(val);
        }
    }

    @Override
    public void errorreponse() {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
    }

    @Override
    public void jsonexception() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        AppUtils.toastprint(context,"Json Exception");
    }

    public void setIds(ActivityRegistrationSelectLocationConstrainBinding binding) {
        this.binding = binding;
    }
}
