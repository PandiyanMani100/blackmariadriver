package com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.tracking;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityTrackPageBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Model.continuetrippojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Mytrips.PastTripSummaryConstrain;

import java.util.HashMap;

public class TrackpageViewModel extends ViewModel implements ApIServices.completelisner {
    private Activity context;
    private ActivityTrackPageBinding binding;
    private continuetrippojo object;
    private Dialog dialog;
    private int count = 0;
    private AppUtils appUtils;
    private SessionManager sessionManager;
    private MutableLiveData<String> sosresponse = new MutableLiveData<>();
    private MutableLiveData<String> tripendresponse = new MutableLiveData<>();
    private MutableLiveData<String> retundropoffresponse = new MutableLiveData<>();
    private MutableLiveData<String> multidropoffresponse = new MutableLiveData<>();

    public TrackpageViewModel(Activity context) {
        this.context = context;
        appUtils = AppUtils.getInstance(context);
        sessionManager = SessionManager.getInstance(context);
    }

    public void setIds(ActivityTrackPageBinding binding) {
        this.binding = binding;
    }

    public void setjson(continuetrippojo object) {
        this.object = object;
    }

    public MutableLiveData<String> getSosresponse() {
        return sosresponse;
    }

    public MutableLiveData<String> getTripendresponse() {
        return tripendresponse;
    }

    public MutableLiveData<String> getRetundropoffresponse() {
        return retundropoffresponse;
    }

    public MutableLiveData<String> getMultidropoffresponse() {
        return multidropoffresponse;
    }

    @Override
    public void sucessresponse(String val) {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        if (count == 0) {
            sosresponse.postValue(val);
        } else if (count == 1) {
            tripendresponse.postValue(val);
        } else if (count == 2) {
            retundropoffresponse.postValue(val);
        } else if (count == 3) {
            multidropoffresponse.postValue(val);
        }
    }

    @Override
    public void errorreponse() {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
    }

    @Override
    public void jsonexception() {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        AppUtils.toastprint(context, "Json Exception");
    }

    public void sosapihit(HashMap<String, String> jsonParams) {
        count = 0;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ApIServices apIServices = new ApIServices(context, TrackpageViewModel.this);
        apIServices.dooperation(Iconstant.sos_url, jsonParams);
    }

    public void tripendapihit(HashMap<String, String> jsonParams) {
        count = 1;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ApIServices apIServices = new ApIServices(context, TrackpageViewModel.this);
        apIServices.dooperation(Iconstant.driverEnd_Url, jsonParams);
    }


    public void tripinfo() {
        Intent rideid = new Intent(context, PastTripSummaryConstrain.class);
        rideid.putExtra("rideid", object.getResponse().getUser_profile().getRide_id());
        context.startActivity(rideid);
        context.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    public void navigate() {
        if (object.getResponse().getUser_profile().getMode().toLowerCase().equalsIgnoreCase("return")) {
            if (object.getResponse().getUser_profile().getReturn_mode().toLowerCase().equalsIgnoreCase("start")) {
                appUtils.navigationtogooglemap(context, sessionManager.getLatitude(),sessionManager.getLongitude(),object.getResponse().getUser_profile().getDrop_lat(),object.getResponse().getUser_profile().getDrop_lon());
            } else {
                appUtils.navigationtogooglemap(context, sessionManager.getLatitude(),sessionManager.getLongitude(),object.getResponse().getUser_profile().getPickup_lat(),object.getResponse().getUser_profile().getPickup_lon());
            }
        } else if (object.getResponse().getUser_profile().getMode().toLowerCase().equalsIgnoreCase("oneway")) {
            appUtils.navigationtogooglemap(context, sessionManager.getLatitude(),sessionManager.getLongitude(),object.getResponse().getUser_profile().getDrop_lat(),object.getResponse().getUser_profile().getDrop_lon());
        }else if(object.getResponse().getUser_profile().getMode().toLowerCase().equalsIgnoreCase("multistop")){
            for (int i = 0; i <= object.getResponse().getUser_profile().getMultistop_array().size() - 1; i++) {
                if(object.getResponse().getUser_profile().getMultistop_array().get(i).getMode().toLowerCase().equalsIgnoreCase("start") && object.getResponse().getUser_profile().getMultistop_array().get(i).getIs_end().toLowerCase().equalsIgnoreCase("0")) {
                    appUtils.navigationtogooglemap(context, sessionManager.getLatitude(),sessionManager.getLongitude(),object.getResponse().getUser_profile().getMultistop_array().get(i).getLatlong().getLat(),object.getResponse().getUser_profile().getMultistop_array().get(i).getLatlong().getLon());
                    break;
                }
            }
            if (object.getResponse().getUser_profile().getIs_multistop_over().toLowerCase().equalsIgnoreCase("1")) {
                appUtils.navigationtogooglemap(context, sessionManager.getLatitude(),sessionManager.getLongitude(),object.getResponse().getUser_profile().getDrop_lat(),object.getResponse().getUser_profile().getDrop_lon());
            }
        }
//        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
//                Uri.parse("http://maps.google.com/maps?f=d&hl=en&saddr=" + object.getResponse().getUser_profile().getPickup_location() + "," + object.getResponse().getUser_profile().getPickup_lon() + "&daddr=" + object.getResponse().getUser_profile().getDrop_lat() + "," + object.getResponse().getUser_profile().getDrop_lon()));
//        context.startActivity(intent);
//        context.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    public void dropoff_returntrip(HashMap<String, String> jsonParams) {
        count = 2;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ApIServices apIServices = new ApIServices(context, TrackpageViewModel.this);
        apIServices.dooperation(Iconstant.returnTrip_Url, jsonParams);
    }

    public void dropoff_multistoptrip(HashMap<String, String> jsonParams) {
        count = 3;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ApIServices apIServices = new ApIServices(context, TrackpageViewModel.this);
        apIServices.dooperation(Iconstant.multipleDrop_Url, jsonParams);
    }

}
