package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class endride implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("ride_view")
    private String ride_view;
    @SerializedName("response")
    Response ResponseObject;


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public String getRide_view() {
        return ride_view;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setRide_view(String ride_view) {
        this.ride_view = ride_view;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }

    public class Response {
        @SerializedName("need_payment")
        private String need_payment;
        @SerializedName("receive_cash")
        private String receive_cash;

        public ArrayList<Fare_arr> getFare_arr() {
            return fare_arr;
        }

        public void setFare_arr(ArrayList<Fare_arr> fare_arr) {
            this.fare_arr = fare_arr;
        }

        @SerializedName("fare_arr")
        ArrayList< Fare_arr > fare_arr = new ArrayList < Fare_arr > ();

        public class  Fare_arr{
            @SerializedName("title")
            private String title;
            @SerializedName("value")
            private String value;
            @SerializedName("currency_status")
            private String currency_status;


            // Getter Methods 

            public String getTitle() {
                return title;
            }

            public String getValue() {
                return value;
            }

            public String getCurrency_status() {
                return currency_status;
            }

            // Setter Methods 

            public void setTitle(String title) {
                this.title = title;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public void setCurrency_status(String currency_status) {
                this.currency_status = currency_status;
            }
        }

        @SerializedName("payment_type")
        private String payment_type;
        @SerializedName("user_image")
        private String user_image;
        @SerializedName("currency")
        private String currency;
        @SerializedName("ride_completed_time")
        private String ride_completed_time;
        @SerializedName("fare_details")
        Fare_details Fare_detailsObject;
        @SerializedName("message")
        private String message;
        @SerializedName("ratting_content")
        private String ratting_content;
        @SerializedName("payment_timeout")
        private String payment_timeout;


        // Getter Methods

        public String getNeed_payment() {
            return need_payment;
        }

        public String getReceive_cash() {
            return receive_cash;
        }

        public String getPayment_type() {
            return payment_type;
        }

        public String getUser_image() {
            return user_image;
        }

        public String getCurrency() {
            return currency;
        }

        public String getRide_completed_time() {
            return ride_completed_time;
        }

        public Fare_details getFare_details() {
            return Fare_detailsObject;
        }

        public String getMessage() {
            return message;
        }

        public String getRatting_content() {
            return ratting_content;
        }

        public String getPayment_timeout() {
            return payment_timeout;
        }

        // Setter Methods

        public void setNeed_payment(String need_payment) {
            this.need_payment = need_payment;
        }

        public void setReceive_cash(String receive_cash) {
            this.receive_cash = receive_cash;
        }

        public void setPayment_type(String payment_type) {
            this.payment_type = payment_type;
        }

        public void setUser_image(String user_image) {
            this.user_image = user_image;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public void setRide_completed_time(String ride_completed_time) {
            this.ride_completed_time = ride_completed_time;
        }

        public void setFare_details(Fare_details fare_detailsObject) {
            this.Fare_detailsObject = fare_detailsObject;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public void setRatting_content(String ratting_content) {
            this.ratting_content = ratting_content;
        }

        public void setPayment_timeout(String payment_timeout) {
            this.payment_timeout = payment_timeout;
        }
        public class Fare_details {
            @SerializedName("currency")
            private String currency;
            @SerializedName("ride_fare")
            private String ride_fare;
            @SerializedName("ride_distance")
            private String ride_distance;
            @SerializedName("ride_duration")
            private String ride_duration;
            @SerializedName("waiting_duration")
            private String waiting_duration;
            @SerializedName("need_payment")
            private String need_payment;


            // Getter Methods

            public String getCurrency() {
                return currency;
            }

            public String getRide_fare() {
                return ride_fare;
            }

            public String getRide_distance() {
                return ride_distance;
            }

            public String getRide_duration() {
                return ride_duration;
            }

            public String getWaiting_duration() {
                return waiting_duration;
            }

            public String getNeed_payment() {
                return need_payment;
            }

            // Setter Methods

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public void setRide_fare(String ride_fare) {
                this.ride_fare = ride_fare;
            }

            public void setRide_distance(String ride_distance) {
                this.ride_distance = ride_distance;
            }

            public void setRide_duration(String ride_duration) {
                this.ride_duration = ride_duration;
            }

            public void setWaiting_duration(String waiting_duration) {
                this.waiting_duration = waiting_duration;
            }

            public void setNeed_payment(String need_payment) {
                this.need_payment = need_payment;
            }
        }

    }

}


