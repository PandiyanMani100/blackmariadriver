package com.blackmaria.partner.latestpackageview.vieewmodel.sidemenu.warnings;

import android.app.Activity;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.View;
import android.view.Window;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferService;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityWarningsconstrainBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

public class WarningsViewModel extends ViewModel implements ApIServices.completelisner {
    private Activity context;
    private ActivityWarningsconstrainBinding binding;

    private MutableLiveData<String> warningsresponse = new MutableLiveData<>();
    private MutableLiveData<String> imageuplaodresponse = new MutableLiveData<>();
    private MutableLiveData<String> updatedriverdoc = new MutableLiveData<>();
    private SessionManager sessionManager;
    public static int count =0;
    String File_names = "";
    String Extension = "";
    String uploaded_file_name = "";
    private String s3_bucket_access_key = "";
    private String s3_bucket_secret_key = "";
    private String s3_bucket_name = "";
    private AmazonS3Client s3Client;
    private BasicAWSCredentials credentials;
    TransferObserver uploadObserver;


    public WarningsViewModel(Activity context) {
        this.context = context;
    }

    public void setIds(ActivityWarningsconstrainBinding binding) {
        this.binding = binding;
    }

    public MutableLiveData<String> getWarningsresponse() {
        return warningsresponse;
    }

    public MutableLiveData<String> getImageuplaodresponse() {
        return imageuplaodresponse;
    }

    public MutableLiveData<String> getUpdatedriverdoc() {
        return updatedriverdoc;
    }

    public void warningsgetlistapihit(HashMap<String, String> jsonParams) {
        count = 0;
        binding.apiload.setVisibility(View.VISIBLE);

        ApIServices apIServices = new ApIServices(context, WarningsViewModel.this);
        apIServices.dooperation(Iconstant.warninsapi, jsonParams);
    }

    public void UploadDriverImage(byte[] array, File curFile) {
        count = 1;
        binding.apiload.setVisibility(View.VISIBLE);

        sessionManager = SessionManager.getInstance(context);

        if(sessionManager.getimagestatus().equals("1"))
        {
            s3_bucket_name = sessionManager.getbucketname();
            s3_bucket_access_key = sessionManager.getaccesskey();
            s3_bucket_secret_key = sessionManager.getsecretkey();

            context.startService(new Intent(context, TransferService.class));
            credentials = new BasicAWSCredentials(s3_bucket_access_key, s3_bucket_secret_key);
            s3Client = new AmazonS3Client(credentials);
            s3Client.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));
            uploadToS3Bucket(curFile,context);
        }
        else
        {
            ApIServices apIServices = new ApIServices(context, WarningsViewModel.this);
            apIServices.imageupload(Iconstant.DriverReg_upload_Image_URl, array, "vehicledocuments.jpg");
        }
     }

    private void uploadToS3Bucket(File fileUri,Activity context)
    {
        String[] path_split;
        if (fileUri != null) {



            uploaded_file_name = "";
            String path = String.valueOf(fileUri);
            String file_name_full = path.substring(path.lastIndexOf("/") + 1);
            path_split = file_name_full.split(".");
            if (path_split.length == 2) {
                File_names = path_split[0];
                Extension = path_split[1];

            } else {
                path_split = file_name_full.split(".jpg");
                if (path_split.length == 1) {
                    File_names = path_split[0];
                }
            }

            String file_name_upload = File_names;
            //current time stamp
            Long tsLong = System.currentTimeMillis() / 1000;
            String str_current_ts = tsLong.toString();
            String concat_image_name = file_name_upload + "" + str_current_ts;
            final String fileName = md5Encryption(concat_image_name) + "." + "jpg";


            TransferUtility transferUtility =
                    TransferUtility.builder()
                            .context(context)
                            .defaultBucket(s3_bucket_name)
                            .s3Client(s3Client)
                            .build();
            uploaded_file_name = fileName;
            uploadObserver = transferUtility.upload("images/drivers_documents/" + fileName, fileUri, CannedAccessControlList.PublicRead);

            // Attach a listener to the observer to get state update and progress notifications
            uploadObserver.setTransferListener(new TransferListener() {

                @Override
                public void onStateChanged(int id, TransferState state) {
                    // Handle a completed upload.
                    if (TransferState.COMPLETED == state) {
                        String path = uploadObserver.getKey();
                        imageuplaodresponse.postValue(fileName);

                        binding.apiload.setVisibility(View.GONE);
                    } else if (TransferState.FAILED == state) {

                        binding.apiload.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                    float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                    int percentDone = (int) percentDonef;
                    Log.d("YourActivity", "ID:" + id + " bytesCurrent: " + bytesCurrent + " bytesTotal: " + bytesTotal + " " + percentDone + "%");
                }

                @Override
                public void onError(int id, Exception ex) {
                    // Handle errors
                    ex.printStackTrace();
                    uploaded_file_name = "";
                    binding.apiload.setVisibility(View.GONE);

                }

            });

            // If you prefer to poll for the data, instead of attaching a
            // listener, check for the state and progress in the observer.
            if (TransferState.COMPLETED == uploadObserver.getState()) {
                // Handle a completed upload.
                Log.d("completed upload", "completed upload");
            }

        }
    }

    public String md5Encryption(String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public void updatedriverdoc(HashMap<String, String> jsonParams) {
        count = 2;
        binding.apiload.setVisibility(View.VISIBLE);


        ApIServices apIServices = new ApIServices(context, WarningsViewModel.this);
        apIServices.dooperation(Iconstant.updatedriverdoc, jsonParams);
    }

    @Override
    public void sucessresponse(String val) {

        binding.apiload.setVisibility(View.INVISIBLE);
        if (count == 0) {
            warningsresponse.postValue(val);
        } else if (count == 1) {
            imageuplaodresponse.postValue(val);
        } else if (count == 2) {
            updatedriverdoc.postValue(val);
        }
    }

    @Override
    public void errorreponse() {

        binding.apiload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void jsonexception() {

        binding.apiload.setVisibility(View.INVISIBLE);
        AppUtils.toastprint(context, "Json Exception");
    }

    public void backclick() {
        context.onBackPressed();
    }

}
