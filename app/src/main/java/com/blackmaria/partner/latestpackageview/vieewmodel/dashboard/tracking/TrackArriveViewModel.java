package com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.tracking;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityTrackArriveBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Model.continuetrippojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.widgets.RoundedImageView;

import java.util.HashMap;

public class TrackArriveViewModel extends ViewModel implements ApIServices.completelisner {
    public Activity context;
    private ActivityTrackArriveBinding binding;
    private Dialog dialog;
    private int count = 0;
    private continuetrippojo object;
    private AppUtils appUtils;
    private MutableLiveData<String> continuetripresponse = new MutableLiveData<>();
    private MutableLiveData<String> cancelresons = new MutableLiveData<>();
    private MutableLiveData<String> arriverespons = new MutableLiveData<>();
    private MutableLiveData<String> phonemasking = new MutableLiveData<>();
    private SessionManager sessionManager;

    public TrackArriveViewModel(Activity context) {
        this.context = context;
        appUtils = AppUtils.getInstance(context);
        sessionManager = new SessionManager(context);
    }

    public void setIds(ActivityTrackArriveBinding binding) {
        this.binding = binding;
    }

    public MutableLiveData<String> getContinuetripresponse() {
        return continuetripresponse;
    }

    public MutableLiveData<String> getArriverespons() {
        return arriverespons;
    }

    public MutableLiveData<String> getCancelresons() {
        return cancelresons;
    }

    public MutableLiveData<String> getPhonemasking() {
        return phonemasking;
    }

    public void continuretripapihit(String sDriveID, String sRideId) {
        count = 0;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriveID);
        jsonParams.put("ride_id", sRideId);
        ApIServices apIServices = new ApIServices(context, TrackArriveViewModel.this);
        apIServices.dooperation(Iconstant.continuePendingTrip_Url, jsonParams);
    }

    public void arriveapihit(HashMap<String, String> jsonParams) {
        count = 2;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ApIServices apIServices = new ApIServices(context, TrackArriveViewModel.this);
        apIServices.dooperation(Iconstant.driverArrived_Url, jsonParams);
    }


    public void canceltripreasons(HashMap<String, String> jsonParams) {
        count = 1;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ApIServices apIServices = new ApIServices(context, TrackArriveViewModel.this);
        apIServices.dooperation(Iconstant.driverCancel_reason_Url, jsonParams);
    }

    public void chatsupport() {
        //.AlertError(context, context.getResources().getString(R.string.info), "Chat coming soon!!");

    }


    @Override
    public void sucessresponse(String val) {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        if (count == 0) {
            continuetripresponse.postValue(val);
        } else if (count == 1) {
            cancelresons.postValue(val);
        } else if (count == 2) {
            arriverespons.postValue(val);
        } else if (count == 3) {
            phonemasking.postValue(val);
        }
    }

    @Override
    public void errorreponse() {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
    }

    @Override
    public void jsonexception() {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        AppUtils.toastprint(context, "Json Exception");
    }

    public void callsuppport(HashMap<String, String> jsonParams) {
        String sPhoneMaskingStatus = sessionManager.getPhoneMaskingStatus();
        if (sPhoneMaskingStatus.equalsIgnoreCase("Yes")) {
            postRequest_PhoneMasking(jsonParams);
        } else {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + object.getResponse().getUser_profile().getPhone_number()));
            context.startActivity(intent);
        }

    }

    public void postRequest_PhoneMasking(HashMap<String, String> jsonParams) {
        count = 3;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ApIServices apIServices = new ApIServices(context, TrackArriveViewModel.this);
        apIServices.dooperation(Iconstant.phoneMasking_url, jsonParams);
    }


    public void viewporfile() {
        final Dialog cancelConfirmDialog = new Dialog(context);
        cancelConfirmDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        cancelConfirmDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        cancelConfirmDialog.setCancelable(false);
        cancelConfirmDialog.setCanceledOnTouchOutside(true);
        cancelConfirmDialog.setContentView(R.layout.userinfolayoutontrack);
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.99);//fill only 85% of the screen
        int screenHeight = (int) (metrics.heightPixels * 0.99);//fill only 85% of the screen
        cancelConfirmDialog.getWindow().setLayout(screenWidth, screenHeight);
        final ImageView close = cancelConfirmDialog.findViewById(R.id.close);
        final RoundedImageView image = cancelConfirmDialog.findViewById(R.id.image);
        final RatingBar ratings = cancelConfirmDialog.findViewById(R.id.ratings);
        final TextView name = cancelConfirmDialog.findViewById(R.id.name);
        final TextView contact = cancelConfirmDialog.findViewById(R.id.contact);
        AppUtils.setImageView(context, object.getResponse().getUser_profile().getUser_image(), image);
        ratings.setRating(Float.parseFloat(object.getResponse().getUser_profile().getUser_review()));
        name.setText("Name : " + object.getResponse().getUser_profile().getUser_name());
        contact.setText("Phone : " + object.getResponse().getUser_profile().getPhone_number());
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancelConfirmDialog.dismiss();
            }
        });
        cancelConfirmDialog.show();
    }

    public void passresponsetomodel(continuetrippojo object) {
        this.object = object;
    }
}
