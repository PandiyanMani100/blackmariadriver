package com.blackmaria.partner.latestpackageview.Factory.Dashboard.Incentives;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.incentives.IncentivesdashboardViewModel;


public class IncentivesdashboardFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public IncentivesdashboardFactory(Activity context) {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new IncentivesdashboardViewModel(context);
    }
}
