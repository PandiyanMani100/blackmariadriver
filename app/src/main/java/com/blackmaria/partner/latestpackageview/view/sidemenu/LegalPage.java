package com.blackmaria.partner.latestpackageview.view.sidemenu;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;

import java.util.HashMap;

import de.cketti.mailto.EmailIntentBuilder;

public class LegalPage extends AppCompatActivity implements View.OnClickListener {

    private TextView Tv_privacyPolicy, Tv_termsOfService, Tv_endUserAgreement, anysuggestions, sendfeedback, RL_facebookPolicy;
    private ImageView RL_home;
    private SessionManager sessionManager;

    private String sDriveID = "", sSupportNumber = "";
    final int PERMISSION_REQUEST_CODE = 111;
    AccessLanguagefromlocaldb db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.legal_page_new);
        db = new AccessLanguagefromlocaldb(this);
        initialize();

    }

    private void initialize() {
        sessionManager = SessionManager.getInstance(LegalPage.this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriveID = user.get(SessionManager.KEY_DRIVERID);
        sSupportNumber = sessionManager.getContactNumber();

        RL_home = findViewById(R.id.RL_home);
        Tv_privacyPolicy = (TextView) findViewById(R.id.txt_label_privacy_policy);
        Tv_privacyPolicy.setText(db.getvalue("firsttime_login_privacy"));
        Tv_termsOfService = (TextView) findViewById(R.id.txt_label_terms_of_service);
        Tv_termsOfService.setText(db.getvalue("legal_page_label_terms_of_service"));
        Tv_endUserAgreement = (TextView) findViewById(R.id.txt_label_end_user_agreement);
        Tv_endUserAgreement.setText(db.getvalue("eula"));
        RL_facebookPolicy = findViewById(R.id.txt_label_facebook_policy);
        RL_facebookPolicy.setText(db.getvalue("legal_page_label_facebook_policy"));
        anysuggestions = findViewById(R.id.anysuggestions);
        anysuggestions.setText(db.getvalue("any_suggestions"));
        sendfeedback = findViewById(R.id.sendfeedback);
        sendfeedback.setText(db.getvalue("send_feedback"));
        TextView terms = findViewById(R.id.terms);
        terms.setText(db.getvalue("terms_lable"));

        RL_home.setOnClickListener(this);
        Tv_privacyPolicy.setOnClickListener(this);
        Tv_termsOfService.setOnClickListener(this);
        Tv_endUserAgreement.setOnClickListener(this);
        RL_facebookPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = Iconstant.facebookurl;
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
        sendfeedback.setOnClickListener(this);
        anysuggestions.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
        if (view == RL_home) {
            finish();
            overridePendingTransition(R.anim.enter, R.anim.exit);

        } else if (view == Tv_privacyPolicy) {
            String url = Iconstant.privacyurl;
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        } else if (view == Tv_termsOfService) {
            String url = Iconstant.termsurl;
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        } else if (view == Tv_endUserAgreement) {
            String url = Iconstant.enduserurl;
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);

        } else if (view == RL_facebookPolicy) {


        } else if (view == sendfeedback) {
            sendEmail();

        } else if (view == anysuggestions) {

        }
    }

    //----------Method to Send Email--------
    protected void sendEmail() {
        HashMap<String, String> appInfo = sessionManager.getUserDetails();
        String toAddress = appInfo.get(sessionManager.KEY_EMAIL);
        try {
            boolean success = EmailIntentBuilder.from(LegalPage.this)
                    .to(toAddress)
                    .subject("")
                    .body("")
                    .start();
        }catch (IllegalArgumentException e){
            e.printStackTrace();
        }
    }

}
