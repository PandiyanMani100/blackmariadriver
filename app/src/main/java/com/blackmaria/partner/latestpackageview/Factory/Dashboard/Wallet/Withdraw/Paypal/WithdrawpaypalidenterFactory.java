package com.blackmaria.partner.latestpackageview.Factory.Dashboard.Wallet.Withdraw.Paypal;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet.withdraw.paypal.PaypalwithdrawidenterViewModel;


public class WithdrawpaypalidenterFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public WithdrawpaypalidenterFactory(Activity context) {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new PaypalwithdrawidenterViewModel(context);
    }
}
