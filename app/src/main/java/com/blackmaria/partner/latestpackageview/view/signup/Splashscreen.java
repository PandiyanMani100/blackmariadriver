package com.blackmaria.partner.latestpackageview.view.signup;

import android.content.Intent;
import android.os.Build;
import android.os.Process;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.android.volley.Request;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.app.LanguageDb;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.mylibrary.volley.ServiceRequest;
import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;
import com.blackmaria.partner.R;
import com.google.firebase.FirebaseApp;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;

public class Splashscreen extends AppCompatActivity {


    private LanguageDb mHelper;
    private SessionManager session;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseApp.initializeApp(this);
        setContentView(R.layout.activity_splashscreen);
        mHelper=new LanguageDb(this);
        session = new SessionManager(Splashscreen.this);

        AppCenter.start(getApplication(), "59ce9e37-e738-460f-8131-680c5cdcb12e",
                Analytics.class, Crashes.class);

        postRequest_Language(Iconstant.getlanguaage);


    }

    //-----------------------App Information Post Request-----------------
    private void postRequest_Language(String Url)
    {
        String languageToLoad  = session.getCurrentlanguage();// your language
        HashMap<String, String> user = session.getUserDetails();
        String userID = user.get(SessionManager.KEY_DRIVERID);
        System.out.println("-------------Splash App Information Url----------------" + Url);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_type", "driver");
        jsonParams.put("id", userID);
        jsonParams.put("app_type", "AU");
        jsonParams.put("lang_code", languageToLoad);

        ServiceRequest mRequest = new ServiceRequest(Splashscreen.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response)
            {
                try
                {
                    JSONObject object = new JSONObject(response);
                    String Sstatus = object.getString("status");
                    if(Sstatus.equals("1"))
                    {
                        deleteDatabase(LanguageDb.DATABASE_NAME);
                        JSONObject jsonObject = object.getJSONObject("response");
                        JSONObject language_key = jsonObject.getJSONObject("language_key");
                        Iterator<String> iter = language_key.keys();
                        while (iter.hasNext())
                        {
                            String key = iter.next();
                            try
                            {
                                Object value = language_key.get(key);
                                System.out.println("Keysss--"+key+"--"+value.toString());
                                mHelper.saveData(key,value.toString());
                            }
                            catch (JSONException e)
                            {
                            }
                        }
                        mHelper.closedatabase();
                        if (getIntent().hasExtra("frommylifecyle")) {
                            finishAndRemoveTask();
                            System.exit(0);
                            Process.killProcess(Process.myPid());
                        } else {
                            Intent intent = new Intent(Splashscreen.this, FetchingDataPage_constrain.class);
                            startActivity(intent);
                            overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
                            finish();
                        }
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener()
            {
                System.out.println("--------Language Information jsonParams--------failed");
                if (getIntent().hasExtra("frommylifecyle")) {
                    finishAndRemoveTask();
                    System.exit(0);
                    Process.killProcess(Process.myPid());
                } else {
                    Intent intent = new Intent(Splashscreen.this, FetchingDataPage_constrain.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
                    finish();
                }
            }
        });


    }
}
