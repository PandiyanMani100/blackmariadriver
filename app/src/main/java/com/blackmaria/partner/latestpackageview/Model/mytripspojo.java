package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class mytripspojo implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }

    public class Response {
        @SerializedName("today")
        Today TodayObject;
        @SerializedName("week")
        Week WeekObject;
        @SerializedName("month")
        Month MonthObject;

        public Hight_trip getHight_trip() {
            return hight_trip;
        }

        public void setHight_trip(Hight_trip hight_trip) {
            this.hight_trip = hight_trip;
        }

        @SerializedName("hight_trip")
        Hight_trip hight_trip;

        @SerializedName("year_highlight")
        Year_highlight Year_highlightObject;
        @SerializedName("top_trip")
        Top_trip Top_tripObject;


        public ArrayList<Chart_array> getChart_array() {
            return chart_array;
        }

        public void setChart_array(ArrayList<Chart_array> chart_array) {
            this.chart_array = chart_array;
        }

        @SerializedName("chart_array")
        ArrayList<Chart_array> chart_array = new ArrayList<>();
        @SerializedName("highest_trip_amount")
        private String highest_trip_amount;
        @SerializedName("currency")
        private String currency;


        // Getter Methods


        public Today getToday() {
            return TodayObject;
        }

        public Week getWeek() {
            return WeekObject;
        }

        public Month getMonth() {
            return MonthObject;
        }

        public Year_highlight getYear_highlight() {
            return Year_highlightObject;
        }

        public Top_trip getTop_trip() {
            return Top_tripObject;
        }


        public String getHighest_trip_amount() {
            return highest_trip_amount;
        }

        public String getCurrency() {
            return currency;
        }

        // Setter Methods

        public void setToday(Today todayObject) {
            this.TodayObject = todayObject;
        }

        public void setWeek(Week weekObject) {
            this.WeekObject = weekObject;
        }

        public void setMonth(Month monthObject) {
            this.MonthObject = monthObject;
        }

        public void setYear_highlight(Year_highlight year_highlightObject) {
            this.Year_highlightObject = year_highlightObject;
        }

        public void setTop_trip(Top_trip top_tripObject) {
            this.Top_tripObject = top_tripObject;
        }


        public void setHighest_trip_amount(String highest_trip_amount) {
            this.highest_trip_amount = highest_trip_amount;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public class Hight_trip{
            @SerializedName("trips")
            private String trips;
            @SerializedName("image")
            private String image;

            public String getTrips() {
                return trips;
            }

            public void setTrips(String trips) {
                this.trips = trips;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getMonth() {
                return month;
            }

            public void setMonth(String month) {
                this.month = month;
            }

            @SerializedName("month")
            private String month;
        }
        public class Top_trip {
            @SerializedName("driver_image")
            private String driver_image;
            @SerializedName("driver_name")
            private String driver_name;
            @SerializedName("driver_review")
            private String driver_review;
            @SerializedName("city_location")
            private String city_location;
            @SerializedName("brand_image")
            private String brand_image;
            @SerializedName("total_trip")
            private String total_trip;
            @SerializedName("vehicle_model")
            private String vehicle_model;


            // Getter Methods

            public String getDriver_image() {
                return driver_image;
            }

            public String getDriver_name() {
                return driver_name;
            }

            public String getDriver_review() {
                return driver_review;
            }

            public String getCity_location() {
                return city_location;
            }

            public String getBrand_image() {
                return brand_image;
            }

            public String getTotal_trip() {
                return total_trip;
            }

            public String getVehicle_model() {
                return vehicle_model;
            }

            // Setter Methods

            public void setDriver_image(String driver_image) {
                this.driver_image = driver_image;
            }

            public void setDriver_name(String driver_name) {
                this.driver_name = driver_name;
            }

            public void setDriver_review(String driver_review) {
                this.driver_review = driver_review;
            }

            public void setCity_location(String city_location) {
                this.city_location = city_location;
            }

            public void setBrand_image(String brand_image) {
                this.brand_image = brand_image;
            }

            public void setTotal_trip(String total_trip) {
                this.total_trip = total_trip;
            }

            public void setVehicle_model(String vehicle_model) {
                this.vehicle_model = vehicle_model;
            }


        }

        public class Year_highlight {
            @SerializedName("start_engine")
            private String start_engine;
            @SerializedName("last_trip")
            private String last_trip;
            @SerializedName("last_trip_date")
            private String last_trip_date;
            @SerializedName("last_trip_m")
            private String last_trip_m;
            @SerializedName("last_trip_d")
            private String last_trip_d;
            @SerializedName("last_trip_d_txt")
            private String last_trip_d_txt;
            @SerializedName("last_trip_y")
            private String last_trip_y;
            @SerializedName("cancel_trip")
            private String cancel_trip;
            @SerializedName("completed")
            private String completed;
            @SerializedName("sos_trip")
            private String sos_trip;
            @SerializedName("complaint")
            private String complaint;
            @SerializedName("Year")
            private String Year;


            // Getter Methods

            public String getStart_engine() {
                return start_engine;
            }

            public String getLast_trip() {
                return last_trip;
            }

            public String getLast_trip_date() {
                return last_trip_date;
            }

            public String getLast_trip_m() {
                return last_trip_m;
            }

            public String getLast_trip_d() {
                return last_trip_d;
            }

            public String getLast_trip_d_txt() {
                return last_trip_d_txt;
            }

            public String getLast_trip_y() {
                return last_trip_y;
            }

            public String getCancel_trip() {
                return cancel_trip;
            }

            public String getCompleted() {
                return completed;
            }

            public String getSos_trip() {
                return sos_trip;
            }

            public String getComplaint() {
                return complaint;
            }

            public String getYear() {
                return Year;
            }

            // Setter Methods

            public void setStart_engine(String start_engine) {
                this.start_engine = start_engine;
            }

            public void setLast_trip(String last_trip) {
                this.last_trip = last_trip;
            }

            public void setLast_trip_date(String last_trip_date) {
                this.last_trip_date = last_trip_date;
            }

            public void setLast_trip_m(String last_trip_m) {
                this.last_trip_m = last_trip_m;
            }

            public void setLast_trip_d(String last_trip_d) {
                this.last_trip_d = last_trip_d;
            }

            public void setLast_trip_d_txt(String last_trip_d_txt) {
                this.last_trip_d_txt = last_trip_d_txt;
            }

            public void setLast_trip_y(String last_trip_y) {
                this.last_trip_y = last_trip_y;
            }

            public void setCancel_trip(String cancel_trip) {
                this.cancel_trip = cancel_trip;
            }

            public void setCompleted(String completed) {
                this.completed = completed;
            }

            public void setSos_trip(String sos_trip) {
                this.sos_trip = sos_trip;
            }

            public void setComplaint(String complaint) {
                this.complaint = complaint;
            }

            public void setYear(String Year) {
                this.Year = Year;
            }


        }

        public class Month {
            @SerializedName("rides_count")
            private String rides_count;
            @SerializedName("rides_distance")
            private String rides_distance;
            @SerializedName("level")
            private String level;


            // Getter Methods

            public String getRides_count() {
                return rides_count;
            }

            public String getRides_distance() {
                return rides_distance;
            }

            public String getLevel() {
                return level;
            }

            // Setter Methods

            public void setRides_count(String rides_count) {
                this.rides_count = rides_count;
            }

            public void setRides_distance(String rides_distance) {
                this.rides_distance = rides_distance;
            }

            public void setLevel(String level) {
                this.level = level;
            }


        }

        public class Week {
            @SerializedName("rides_count")
            private String rides_count;
            @SerializedName("rides_distance")
            private String rides_distance;
            @SerializedName("level")
            private String level;


            // Getter Methods

            public String getRides_count() {
                return rides_count;
            }

            public String getRides_distance() {
                return rides_distance;
            }

            public String getLevel() {
                return level;
            }

            // Setter Methods

            public void setRides_count(String rides_count) {
                this.rides_count = rides_count;
            }

            public void setRides_distance(String rides_distance) {
                this.rides_distance = rides_distance;
            }

            public void setLevel(String level) {
                this.level = level;
            }


        }

        public class Today {
            @SerializedName("rides_count")
            private String rides_count;
            @SerializedName("rides_distance")
            private String rides_distance;
            @SerializedName("level")
            private String level;


            // Getter Methods

            public String getRides_count() {
                return rides_count;
            }

            public String getRides_distance() {
                return rides_distance;
            }

            public String getLevel() {
                return level;
            }

            // Setter Methods

            public void setRides_count(String rides_count) {
                this.rides_count = rides_count;
            }

            public void setRides_distance(String rides_distance) {
                this.rides_distance = rides_distance;
            }

            public void setLevel(String level) {
                this.level = level;
            }
        }

        public class Chart_array {


            @SerializedName("day")
            private String day;
            @SerializedName("ridecount")
            private String ridecount;

            public String getDay() {
                return day;
            }

            public void setDay(String day) {
                this.day = day;
            }

            public String getRidecount() {
                return ridecount;
            }

            public void setRidecount(String ridecount) {
                this.ridecount = ridecount;
            }

            public String getDistance() {
                return distance;
            }

            public void setDistance(String distance) {
                this.distance = distance;
            }

            @SerializedName("distance")
            private String distance;

        }

    }

}






