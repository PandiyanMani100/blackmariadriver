package com.blackmaria.partner.latestpackageview.vieewmodel.dashboard;

import android.app.Activity;
import android.app.Application;
import android.app.Dialog;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

import android.view.View;
import android.view.Window;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.GPSTracker;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.app.ComplaintPage;
import com.blackmaria.partner.databinding.FragmentDashboardBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Dashboard_constrain;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Earnings.Earningsdashboardconstrain;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Incentives.IncentivedashbaordConstrain;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Message.MessageConstrain;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Mytrips.MytripshomepageConstrain;
import com.blackmaria.partner.latestpackageview.view.Dashboard.SOS.Sosdragmap_screen;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Fastpayhome;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

public class DashboardFragmentViewModel extends AndroidViewModel implements ApIServices.completelisner {

    private Context context;
    private FragmentDashboardBinding binding;
    private SessionManager sessionManager;

    private int count = 0;
    private MutableLiveData<String> driversetting = new MutableLiveData<>();
    private MutableLiveData<String> gooinlineresponse = new MutableLiveData<>();
    private MutableLiveData<String> availabilityupdate = new MutableLiveData<>();
    private MutableLiveData<String> logoutresponse = new MutableLiveData<>();

    public DashboardFragmentViewModel(@NonNull Application application) {
        super(application);
        context = application.getApplicationContext();
        sessionManager = SessionManager.getInstance(context);
    }

    public MutableLiveData<String> getDriversetting() {
        return driversetting;
    }

    public MutableLiveData<String> getGooinlineresponse() {
        return gooinlineresponse;
    }

    public MutableLiveData<String> getAvailabilityupdate() {
        return availabilityupdate;
    }

    public MutableLiveData<String> getLogoutresponse() {
        return logoutresponse;
    }



    @Override
    public void sucessresponse(String val) {
        binding.apiload.setVisibility(View.INVISIBLE);
        if (count == 1) {
            gooinlineresponse.postValue(val);
        } else if (count == 2) {
            availabilityupdate.postValue(val);
        } else if(count ==4) {
            logoutresponse.postValue(val);
        }else{
            driversetting.postValue(val);
        }
    }


    @Override
    public void errorreponse() {
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void jsonexception() {
        binding.apiload.setVisibility(View.INVISIBLE);
        AppUtils.toastprint(context, "Json Exception");
    }


    public void driversettings(String driverHomePage_url, HashMap<String, String> jsonParams) {
        count = 0;
        ApIServices apIServices = new ApIServices(context, DashboardFragmentViewModel.this);
        apIServices.dooperation(driverHomePage_url, jsonParams);
    }

    public void updateavailabilty(HashMap<String, String> jsonParams) {
        count = 2;
        ApIServices apIServices = new ApIServices(context, DashboardFragmentViewModel.this);
        apIServices.dooperation(Iconstant.updateAvailability_Url, jsonParams);
    }

    public void logout(String profile_driver_account_logout_url, HashMap<String, String> jsonParams) {
        count = 4;
        ApIServices apIServices = new ApIServices(context, DashboardFragmentViewModel.this);
        apIServices.dooperation(profile_driver_account_logout_url, jsonParams);
    }

    public void setIds(FragmentDashboardBinding binding) {
        this.binding = binding;
    }
}
