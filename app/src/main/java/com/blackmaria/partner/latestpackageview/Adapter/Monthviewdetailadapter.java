package com.blackmaria.partner.latestpackageview.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;

import androidx.viewpager.widget.PagerAdapter;
import androidx.cardview.widget.CardView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.latestpackageview.Model.Monthreport;
import com.blackmaria.partner.latestpackageview.Model.monthtripviewpojo;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Mytrips.WeektripviewConstrain;

import java.util.ArrayList;

public class Monthviewdetailadapter extends PagerAdapter {

    private ArrayList<Monthreport> IMAGES;
    private LayoutInflater inflater;
    private Context context;
    private String description = "";


    public Monthviewdetailadapter(Context context, ArrayList<Monthreport> IMAGES, String description) {
        this.context = context;
        this.IMAGES = IMAGES;
        this.description = description;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return IMAGES.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View imageLayout = inflater.inflate(R.layout.monthviewadapterlayout, view, false);
        assert imageLayout != null;
        view.addView(imageLayout, 0);
        CardView drivenow = imageLayout.findViewById(R.id.drivenow);
        TextView week = imageLayout.findViewById(R.id.week);
        TextView tripcount = imageLayout.findViewById(R.id.tripcount);
        TextView kilometer = imageLayout.findViewById(R.id.kilometer);

        week.setText(IMAGES.get(position).getWeek() + "" + IMAGES.get(position).getWeekCount());
        if (IMAGES.get(position).getRidecount().equals("")) {
            tripcount.setText("0");
        } else {
            tripcount.setText(IMAGES.get(position).getRidecount());
        }

        if (IMAGES.get(position).getDistance().equals("")) {
            kilometer.setText("0 KM");
        } else {
            kilometer.setText(IMAGES.get(position).getDistance());
        }

        drivenow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent week = new Intent(context, WeektripviewConstrain.class);
                week.putExtra("fromdate", IMAGES.get(position).getStartdate_f());
                week.putExtra("todate", IMAGES.get(position).getEndate_f());
                week.putExtra("description", description);
                context.startActivity(week);
            }
        });

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


}