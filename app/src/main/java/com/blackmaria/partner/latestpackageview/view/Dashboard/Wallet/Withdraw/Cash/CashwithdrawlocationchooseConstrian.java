package com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Withdraw.Cash;

import android.app.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.SessionManager;
//import com.blackmaria.partner.databinding.ActivityCashwithdrawlocationchooseConstrianBinding;
import com.blackmaria.partner.latestpackageview.Adapter.Type_of_registrationadapter;
import com.blackmaria.partner.latestpackageview.Model.lookupcashpojo;
import com.blackmaria.partner.latestpackageview.Model.withdrawalspaymentspojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class CashwithdrawlocationchooseConstrian extends AppCompatActivity implements GoogleMap.OnMarkerClickListener, AdapterView.OnItemSelectedListener {

    //    private ActivityCashwithdrawlocationchooseConstrianBinding binding;
//    private CashwithdrawlocationchooseViewModel cashwithdrawlocationchooseViewModel;
    private SessionManager sessionManager;
    private AppUtils appUtils;

    private GoogleMap googleMap;
    private MapFragment mapFragment;
    private Marker myMarker;
    private BitmapDescriptor carIcon;
    private withdrawalspaymentspojo pojoglobal;
    ArrayList<String> markerPlaces = new ArrayList<>();

    public ViewGroup infoWindow;
    private CustomLayout customLayout;
    private ImageView backimg;
    private Spinner selectgender;
    private TextView txtspnrgender;
    private ArrayList<String> regTypeListid = new ArrayList<String>();


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent == selectgender) {
            String sSelectedItem = selectgender.getSelectedItem().toString().trim();
            txtspnrgender.setText(sSelectedItem);
            for (int i = 0; i <= pojoglobal.getResponse().getWithdraw_location().size() - 1; i++) {
                if (pojoglobal.getResponse().getWithdraw_location().get(i).getId().equalsIgnoreCase(regTypeListid.get(position))) {
                    LatLng latLng = new LatLng(Double.parseDouble(pojoglobal.getResponse().getWithdraw_location().get(i).getLocationobj().getLat()), Double.parseDouble(pojoglobal.getResponse().getWithdraw_location().get(i).getLocationobj().getLng()));
                    float zoom = googleMap.getCameraPosition().zoom;
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
                }
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    AccessLanguagefromlocaldb db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cashwithdrawlocationchoose_constrian);
        db = new AccessLanguagefromlocaldb(this);
//        binding = DataBindingUtil.setContentView(CashwithdrawlocationchooseConstrian.this, R.layout.activity_cashwithdrawlocationchoose_constrian);
//        cashwithdrawlocationchooseViewModel = ViewModelProviders.of(this, new CashwithdrawallocationchooseFactory(this)).get(CashwithdrawlocationchooseViewModel.class);
//        binding.setCashwithdrawlocationchooseViewModel(cashwithdrawlocationchooseViewModel);
//        cashwithdrawlocationchooseViewModel.setIds(binding);

        initView();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Log.v("TRIMMEMORY", " " + level);
    }

    private void initView() {
        sessionManager = SessionManager.getInstance(this);
        appUtils = AppUtils.getInstance(this);
        carIcon = (BitmapDescriptorFactory.fromResource(R.drawable.mappinarrow));
        customLayout = findViewById(R.id.mapcontrain);
        backimg = findViewById(R.id.backimg);
        selectgender = findViewById(R.id.selectgender);
        txtspnrgender = findViewById(R.id.txtspnrgender);
        txtspnrgender.setHint(db.getvalue("label_select_location"));
        backimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        TextView lookupcash = findViewById(R.id.lookupcash);
        lookupcash.setText(db.getvalue("lookupcash"));

        initializeMap();
    }

    private void GenderSelection(ArrayList<String> regTypeList, ArrayList<String> regTypeListid) {
        Type_of_registrationadapter customSpinnerbank = new Type_of_registrationadapter(CashwithdrawlocationchooseConstrian.this, regTypeList);
        selectgender.setAdapter(customSpinnerbank);
        selectgender.setOnItemSelectedListener(this);
    }

    private void initializeMap() {
        if (googleMap == null) {
            mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap Map) {
                    googleMap = Map;
                    googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getApplicationContext(), R.raw.style));
                    googleMap.getUiSettings().setRotateGesturesEnabled(true);
                    try {
                        JSONObject json = new JSONObject(sessionManager.getcashlocationlist());
                        Type type = new TypeToken<withdrawalspaymentspojo>() {
                        }.getType();
                        pojoglobal = new GsonBuilder().create().fromJson(json.toString(), type);
                        ArrayList<String> regTypeList = new ArrayList<String>();
                        regTypeListid.clear();
                        regTypeListid.add("");
                        regTypeList.add("Select Location");
                        LatLngBounds.Builder builder = new LatLngBounds.Builder();
                        for (int i = 0; i <= pojoglobal.getResponse().getWithdraw_location().size() - 1; i++) {
                            LatLng latLng = new LatLng(Double.parseDouble(pojoglobal.getResponse().getWithdraw_location().get(i).getLocationobj().getLat()), Double.parseDouble(pojoglobal.getResponse().getWithdraw_location().get(i).getLocationobj().getLng()));
                            builder.include(latLng);
                            Marker marker = googleMap.addMarker(new MarkerOptions().position(latLng)
                                    .title(pojoglobal.getResponse().getWithdraw_location().get(i).getId())
                                    .icon(carIcon));
                            markerPlaces.add(marker.getId());
                            regTypeList.add(pojoglobal.getResponse().getWithdraw_location().get(i).getLocation_address());
                            regTypeListid.add(pojoglobal.getResponse().getWithdraw_location().get(i).getId());
                        }
                        if(pojoglobal.getResponse().getWithdraw_location().size() != 0)
                        {
                            GenderSelection(regTypeList, regTypeListid);
                            int paddingH, paddingW;
                            final View mapview = mapFragment.getView();
                            float maxX = mapview.getMeasuredWidth();
                            float maxY = mapview.getMeasuredHeight();
                            LatLngBounds bounds = builder.build();
                            float percentageH = 50.0f;
                            float percentageW = 80.0f;
                            paddingH = (int) (maxY * (percentageH / 100.0f));
                            paddingW = (int) (maxX * (percentageW / 100.0f));
                            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, paddingW, paddingH, 100);
                            googleMap.animateCamera(cu);

                            InfoAdapter customInfoWindow = new InfoAdapter();
                            googleMap.setInfoWindowAdapter(customInfoWindow);


                            googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                                @Override
                                public void onInfoWindowClick(Marker marker) {
                                    lookupcashpojo pjo = new lookupcashpojo();
                                    pjo.setCashcenterid(marker.getTitle());
                                    EventBus.getDefault().post(pjo);
                                    finish();
                                }
                            });
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(),"No Location Available",Toast.LENGTH_LONG).show();
                            finish();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    setclickmap();
                }
            });

        }
    }

    private void setclickmap() {
        googleMap.setOnMarkerClickListener(this);
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {


        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    public class InfoAdapter implements GoogleMap.InfoWindowAdapter {
        LayoutInflater inflater = null;
        private TextView textViewstopName;
        private TextView arrivalTime;

        public InfoAdapter() {

        }

        @Override
        public View getInfoWindow(Marker marker) {
            if (marker != null) {
                if (markerPlaces.contains(marker.getId())) {
                    int pos = 0;
                    View v = ((Activity) CashwithdrawlocationchooseConstrian.this).getLayoutInflater()
                            .inflate(R.layout.custommarker, null);
                    for (int jj = 0; jj <= pojoglobal.getResponse().getWithdraw_location().size() - 1; jj++) {
                        if (pojoglobal.getResponse().getWithdraw_location().get(jj).getId().equalsIgnoreCase(marker.getTitle())) {
                            pos = jj;
                            break;
                        }
                    }
                    textViewstopName = (TextView) v.findViewById(R.id.title);
                    textViewstopName.setText(pojoglobal.getResponse().getWithdraw_location().get(pos).getName());
                    arrivalTime = (TextView) v.findViewById(R.id.value);
                    arrivalTime.setText(pojoglobal.getResponse().getWithdraw_location().get(pos).getLocation_address());
                    return (v);
                } //checks if the marker is part of the Position marker or POI marker.
                else {
                    View v = ((Activity) CashwithdrawlocationchooseConstrian.this).getLayoutInflater()
                            .inflate(R.layout.custommarker, null);
                    textViewstopName = (TextView) v.findViewById(R.id.title);
                    textViewstopName.setText(marker.getTitle());
                    arrivalTime = (TextView) v.findViewById(R.id.value);
                    arrivalTime.setText(marker.getSnippet());
                    return (v);
                }
            }
            return null;
        }

        @Override
        public View getInfoContents(Marker marker) {
            return (null);
        }
    }

}
