package com.blackmaria.partner.latestpackageview.widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Ganesh on 12-06-2017.
 */

@SuppressLint("AppCompatCustomView")
public class TextRobotoMediumItalic extends TextView {

    public TextRobotoMediumItalic(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public TextRobotoMediumItalic(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextRobotoMediumItalic(Context context) {
        super(context);
        init();
    }


    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Roboto-MediumItalic.ttf");
        setTypeface(tf);
    }
}
