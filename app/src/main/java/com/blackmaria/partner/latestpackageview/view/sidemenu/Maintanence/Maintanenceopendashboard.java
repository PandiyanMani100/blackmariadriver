package com.blackmaria.partner.latestpackageview.view.sidemenu.Maintanence;


import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityMaintanenceopendashboardBinding;
import com.blackmaria.partner.latestpackageview.Factory.Sidemenu.Maintanence.MaintanenceopendashboardFactory;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.vieewmodel.sidemenu.maintanence.MaintanenceopendashboardViewModel;

import java.util.HashMap;

public class Maintanenceopendashboard extends AppCompatActivity {

    private ActivityMaintanenceopendashboardBinding binding;
    private MaintanenceopendashboardViewModel maintanenceopendashboardViewModel;
    AccessLanguagefromlocaldb db;

    private SessionManager sessionManager;
    private AppUtils appUtils;
    private String sDriverID = "", driverimage = "", vehicleimage = "", drivername = "", carno = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
        binding = DataBindingUtil.setContentView(Maintanenceopendashboard.this, R.layout.activity_maintanenceopendashboard);
        maintanenceopendashboardViewModel = ViewModelProviders.of(this, new MaintanenceopendashboardFactory(this)).get(MaintanenceopendashboardViewModel.class);
        binding.setMaintanenceopendashboardViewModel(maintanenceopendashboardViewModel);
        maintanenceopendashboardViewModel.setIds(binding);


        binding.usertype.setText(db.getvalue("owner"));
        binding.secondusername.setText(db.getvalue("add_driver"));
        binding.changevehiclename.setText(db.getvalue("changevehicle"));
        initView();
        clicklistener();
    }


    private void initView() {
        appUtils = AppUtils.getInstance(this);
        sessionManager = SessionManager.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        drivername = user.get(SessionManager.KEY_DRIVER_NAME);
        driverimage = user.get(SessionManager.KEY_DRIVER_IMAGE);
        vehicleimage = user.get(SessionManager.KEY_VEHICLE_IMAGE);
        binding.username.setText(drivername);
        binding.carno.setText(sessionManager.getdrivercarnumber());
        AppUtils.setImageView(Maintanenceopendashboard.this, driverimage, binding.userimage);
        AppUtils.setImageviewwithoutcrop(Maintanenceopendashboard.this, sessionManager.getUserVehicle(), binding.caricon);
    }


    private void clicklistener() {
        binding.secondviewimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Maintanenceopendashboard.this, Addseconddriver.class));
            }
        });
        binding.changevehicleimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Maintanenceopendashboard.this, EditvehicledetailsConstrain.class));
            }
        });
    }

}
