package com.blackmaria.partner.latestpackageview.view.sidemenu.Ratings;



import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;

import com.blackmaria.partner.Pojo.MenuHomeratingPojo;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.adapter.MenuHomeRateAdapter;
import com.blackmaria.partner.databinding.ActivityRatingHomepageBinding;
import com.blackmaria.partner.latestpackageview.Factory.Sidemenu.RatingHomepageFactory;
import com.blackmaria.partner.latestpackageview.Model.ratingspojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.vieewmodel.sidemenu.RatinghomeViewModel;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

public class RatingHomepageConstrain extends AppCompatActivity {

    private ActivityRatingHomepageBinding binding;
    private RatinghomeViewModel ratinghomeViewModel;
    private SessionManager session;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private MenuHomeRateAdapter adapter;
    private String Driverid = "", lastrideid = "",driverimage="";
    private ArrayList<MenuHomeratingPojo> itemlist;
    private AppUtils appUtils;

    AccessLanguagefromlocaldb db;


    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
        binding = DataBindingUtil.setContentView(RatingHomepageConstrain.this, R.layout.activity_rating_homepage);
        ratinghomeViewModel = ViewModelProviders.of(this, new RatingHomepageFactory(this)).get(RatinghomeViewModel.class);
        binding.setRatinghomeViewModel(ratinghomeViewModel);

        ratinghomeViewModel.setIDs(binding);
        initView();
        binding.ratingPageDriverCartypeTextview.setText(db.getvalue("rating_score"));
    }

    private void initView() {
        session = SessionManager.getInstance(this);
        cd = ConnectionDetector.getInstance(this);
        appUtils = AppUtils.getInstance(this);
        HashMap<String, String> user = session.getUserDetails();
        Driverid = user.get(SessionManager.KEY_DRIVERID);
        driverimage = user.get(SessionManager.KEY_DRIVER_IMAGE);
        AppUtils.setImageView(RatingHomepageConstrain.this,driverimage,binding.imgpagelogo);

        ratinghomeViewModel.getreviewlist(Driverid);

        ratinghomeViewModel.getReviewlist().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    Type type = new TypeToken<ratingspojo>() {
                    }.getType();
                    ratingspojo object = new GsonBuilder().create().fromJson(response.toString(), type);

                    if(object.getStatus() != null)
                    {
                        if (object.getStatus().equalsIgnoreCase("1")) {
                            try {
                                binding.myrideratingsingleratingbar.setRating(Float.parseFloat(object.getResponse().getDriver_profile().getAvg_review()));
                            }catch (NumberFormatException e){
                                e.printStackTrace();
                            }
                            if(object.getResponse().getLast_comment_review().size()>0){
                                binding.viewallreviews.setVisibility(View.VISIBLE);
                                binding.lastcomment.setVisibility(View.VISIBLE);
                                binding.cardviewsignup.setVisibility(View.VISIBLE);
                                ratinghomeViewModel.setRideid(object.getResponse().getLast_comment_review().get(0).getRide_id());
                                binding.datename.setText(object.getResponse().getLast_comment_review().get(0).getDriver_name() + " commented on " + object.getResponse().getLast_comment_review().get(0).getReview_date());
                                binding.comments.setText(object.getResponse().getLast_comment_review().get(0).getComments());
                                appUtils.setImageView(RatingHomepageConstrain.this, object.getResponse().getLast_comment_review().get(0).getImage(), binding.ratingpageprofilephoto1);
                            }else{
                                binding.lastcomment.setVisibility(View.GONE);
                                binding.cardviewsignup.setVisibility(View.GONE);
                                if(object.getResponse().getReason_review().size() > 0)
                                {
                                    binding.noreviews.setVisibility(View.GONE);
                                }
                                else
                                {
                                    binding.noreviews.setVisibility(View.VISIBLE);
                                }

                            }
                            if(object.getResponse().getReason_review().size()>0){
                                adapter = new MenuHomeRateAdapter(RatingHomepageConstrain.this, object.getResponse().getReason_review());
                                binding.menuratinghomelist.setAdapter(adapter);
                            }else{
                                binding.menuratinghomelist.setVisibility(View.GONE);
                            }
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        binding.backImag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }
}
