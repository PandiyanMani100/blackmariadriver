package com.blackmaria.partner.latestpackageview.ApiRequest;


import android.content.Context;
import android.content.res.Configuration;

import com.android.volley.Request;
import com.blackmaria.partner.Utils.SessionManager;

import com.blackmaria.partner.latestpackageview.Model.appinfo;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.xendit.AuthenticationCallback;
import com.xendit.Models.Authentication;
import com.xendit.Models.Card;
import com.xendit.Models.Token;
import com.xendit.Models.XenditError;
import com.xendit.TokenCallback;
import com.xendit.Xendit;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Locale;

public class ApIServices {
    public Context context;
    completelisner completelisner;
    Xenditinterface xenditinterface;
    private SessionManager session;
    private String number = "", sContact_mail = "", app_identity_name = "", Language_code = "", sCustomerServiceNumber = "", sSiteUrl = "", sXmppHostUrl = "", sHostName = "", sFacebookId = "", sGooglePlusId = "", sPhoneMasking = "";
    private boolean isAppInfoAvailable = false;


    public ApIServices(Context context, completelisner completelisner) {
        this.context = context;
        this.completelisner = completelisner;
        session = SessionManager.getInstance(context);
    }

    public ApIServices(Context context, Xenditinterface xenditinterface,Context contexts) {
        this.context = context;
        this.xenditinterface = xenditinterface;
        session = SessionManager.getInstance(context);
    }

    public interface completelisner {
        void sucessresponse(String val);

        void errorreponse();

        void jsonexception();

    }

    public interface Xenditinterface{
        void Xenditerrorresponse(String msg);
        void Xenditsucessresponse(String val);
        void sucessresponse(String val);
        void errorreponse();
    }



    public void imageupload(String url, byte[] array,String driverimagename){
        MultipartRequest mRequest = new MultipartRequest(context,driverimagename);
        mRequest.makeServiceRequest(url, Request.Method.POST, array, new MultipartRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {
                String sStatus = "", sResponse = "", SUser_image = "", Smsg = "";
                try {
                    completelisner.sucessresponse(response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                completelisner.errorreponse();
            }
        });
    }

    public void dooperation(String Url, HashMap<String, String> jsonParams) {
        ServiceRequest mRequest = new ServiceRequest(context);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                try {
                    completelisner.sucessresponse(response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {

                completelisner.errorreponse();
            }
        });
    }

    public void Xendtidooperation(String Url, HashMap<String, String> jsonParams) {
        ServiceRequest mRequest = new ServiceRequest(context);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                try {
                    xenditinterface.sucessresponse(response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                xenditinterface.errorreponse();
            }
        });
    }


    public void CreateXenditToken(Card card, boolean isMultipleUse, Xendit xendit, boolean shouldAuthenticate, String insertAmount,String CURRENCYCONVERSIONKEY_KEY){
        TokenCallback callback = new TokenCallback() {
            @Override
            public void onSuccess(Token token) {
                if (token != null) {
                    if (token.getStatus().equalsIgnoreCase("VERIFIED")) {
                        xenditinterface.Xenditsucessresponse(token.getId());
                    }
                }
            }

            @Override
            public void onError(XenditError xenditError) {
                xenditinterface.Xenditerrorresponse(xenditError.getErrorCode() + xenditError.getErrorMessage());
            }
        };


        if (isMultipleUse) {
            xendit.createMultipleUseToken(card, callback);
        } else {
            Double amount = 0.0;
            amount = Double.parseDouble(insertAmount) * Double.parseDouble(CURRENCYCONVERSIONKEY_KEY);
            amount = (Double) Math.ceil(amount);
            xendit.createSingleUseToken(card, amount, shouldAuthenticate, callback);
        }
    }

    public void CreateAuthenticationId(final String tokenId, final Xendit xendit, final Double amount){
        xendit.createAuthentication(tokenId, amount, new AuthenticationCallback() {
            @Override
            public void onSuccess(Authentication authentication) {
                xenditinterface.Xenditsucessresponse(authentication.getId());
            }

            @Override
            public void onError(XenditError xenditError) {
                xenditinterface.Xenditerrorresponse(xenditError.getErrorCode()+xenditError.getErrorMessage());
            }
        });
    }


    public void amazoniagealoinesend(String Url, HashMap<String, String> jsonParams) {
        ServiceRequest mRequest = new ServiceRequest(context);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                try {
                    completelisner.sucessresponse(response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {

                completelisner.errorreponse();
            }
        });
    }


    public void Appinfo(String Url, HashMap<String, String> jsonParams) {
        ServiceRequest mRequest = new ServiceRequest(context);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                JSONObject object = null;
                try {
                    object = new JSONObject(response);
                    Type listType = new TypeToken<appinfo>() {
                    }.getType();
                    appinfo pojo = new GsonBuilder().create().fromJson(object.toString(), listType);
                    session.setDriverReview(pojo.getResponse().getInfo().getDriver_review());
                    session.setReferelStatus(pojo.getResponse().getInfo().getRide_earn());

                    try {
                        JSONArray jsonarray_emergendy_no = object.getJSONObject("info").getJSONArray("emergencyNumbers");
                        for (int i = 0; i <= jsonarray_emergendy_no.length() - 1; i++) {
                            String value = jsonarray_emergendy_no.getJSONObject(i).getString("title");
                            if (value.equalsIgnoreCase("Police")) {
                                session.setPolice(jsonarray_emergendy_no.getJSONObject(i).getString("number"));
                            } else if (value.equalsIgnoreCase("Fire")) {
                                session.setFire(jsonarray_emergendy_no.getJSONObject(i).getString("number"));
                            } else if (value.equalsIgnoreCase("Ambulance")) {
                                session.setAmbulance(jsonarray_emergendy_no.getJSONObject(i).getString("number"));
                            }
                        }

                    } catch (Exception e) {

                    }
                    Locale locale = null;
                    switch (pojo.getResponse().getInfo().getLang_code()) {

                        case "en":
                            locale = new Locale("en");
                            session.setlamguage("en", "en");
                            break;
                        case "es":
                            locale = new Locale("es");
                            session.setlamguage("es", "es");
                            break;

                        default:
                            locale = new Locale("en");
                            session.setlamguage("en", "en");
                            break;
                    }

                    Locale.setDefault(locale);
                    Configuration config = new Configuration();
                    config.locale = locale;
                    context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());


                    session.setXmpp(pojo.getResponse().getInfo().getXmpp_host_url(), pojo.getResponse().getInfo().getXmpp_host_name());
                    session.setAgent(pojo.getResponse().getInfo().getApp_identity_name());
                    session.setPhoneMaskingDetail(pojo.getResponse().getInfo().getPhone_masking_status());
                    session.setContactNumber(pojo.getResponse().getInfo().getCustomer_service_number());
                    session.setContactEmail(pojo.getResponse().getInfo().getSite_contact_mail());
                    session.setVehicleType(pojo.getResponse().getInfo().getVehicle_type());
                    session.setUserGuide(pojo.getResponse().getInfo().getUser_guide());
                    session.setSecurePin(pojo.getResponse().getInfo().getWith_pincode());

                    completelisner.sucessresponse(response);

                } catch (JSONException e) {
                    e.printStackTrace();
                    completelisner.jsonexception();
                }
            }

            @Override
            public void onErrorListener() {
                completelisner.errorreponse();
            }
        });
    }

}
