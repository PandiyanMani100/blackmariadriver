package com.blackmaria.partner.latestpackageview.Countrycodepicker.countrycodepicker;

public interface CountryPickerListener {
	public void onSelectCountry(String name, String code, String dialCode);
}
