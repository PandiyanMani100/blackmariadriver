package com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.incentives;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import com.blackmaria.partner.R;
import com.blackmaria.partner.databinding.ActivityMyincentivesConstrainBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;

import java.util.HashMap;

public class MyincentivesViewModel extends ViewModel implements ApIServices.completelisner {
    private Activity context;
    private ActivityMyincentivesConstrainBinding binding;
    private Dialog dialog;
    private MutableLiveData<String> myincentiveresponse = new MutableLiveData<>();

    public MyincentivesViewModel(Activity context) {
        this.context = context;
    }

    public void myincentivelist(HashMap<String, String> jsonParams) {
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ApIServices apIServices = new ApIServices(context, MyincentivesViewModel.this);
        apIServices.dooperation(Iconstant.incentives_list_url, jsonParams);
    }

    public MutableLiveData<String> getMyincentiveresponse() {
        return myincentiveresponse;
    }

    @Override
    public void sucessresponse(String val) {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        myincentiveresponse.postValue(val);
    }

    @Override
    public void errorreponse() {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
    }

    @Override
    public void jsonexception() {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        AppUtils.toastprint(context,"Json Exception");
    }

    public void setIds(ActivityMyincentivesConstrainBinding binding) {
        this.binding = binding;
    }

    public void back(){
        context.onBackPressed();
    }
}
