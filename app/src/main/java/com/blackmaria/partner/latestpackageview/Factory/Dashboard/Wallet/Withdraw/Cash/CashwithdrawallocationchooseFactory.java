package com.blackmaria.partner.latestpackageview.Factory.Dashboard.Wallet.Withdraw.Cash;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet.withdraw.cash.CashwithdrawlocationchooseViewModel;


public class CashwithdrawallocationchooseFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public CashwithdrawallocationchooseFactory(Activity context) {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new CashwithdrawlocationchooseViewModel(context);
    }
}
