package com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.earnings;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityReferrallistConstrainBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;

import java.util.HashMap;

public class ReferrallistViewModel extends ViewModel implements ApIServices.completelisner {
    private Activity context;
    private ActivityReferrallistConstrainBinding binding;
    private Dialog dialog;
    private String sDriverID = "";
    private SessionManager sessionManager;
    private MutableLiveData<String> referallistresponse = new MutableLiveData<>();

    public ReferrallistViewModel(Activity context) {
        this.context = context;
        sessionManager = SessionManager.getInstance(context);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
    }

    public void setIds(ActivityReferrallistConstrainBinding binding) {
        this.binding = binding;
        referrallistapihit();
    }

    public MutableLiveData<String> getReferallistresponse() {
        return referallistresponse;
    }

    public void referrallistapihit() {
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        ApIServices apIServices = new ApIServices(context, ReferrallistViewModel.this);
        apIServices.dooperation(Iconstant.referrallist, jsonParams);
    }

    @Override
    public void sucessresponse(String val) {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        referallistresponse.postValue(val);
    }

    @Override
    public void errorreponse() {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
    }

    @Override
    public void jsonexception() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        AppUtils.toastprint(context,"Json Exception");
    }

    public void back() {
        context.onBackPressed();
    }

    public void right() {

    }

    public void left() {

    }
}
