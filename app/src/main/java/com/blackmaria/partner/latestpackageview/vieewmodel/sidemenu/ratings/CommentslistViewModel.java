package com.blackmaria.partner.latestpackageview.vieewmodel.sidemenu.ratings;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityCommentslistConstrainBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;

import java.util.HashMap;

public class CommentslistViewModel extends ViewModel implements ApIServices.completelisner {

    private Activity context;
    private ActivityCommentslistConstrainBinding binding;
    private int currentPage = 1;
    private String perPage = "10",driverid="",driverimage;
    private Boolean isInternetPresent = false;
    private SessionManager session;
    private ConnectionDetector cd;
    private Dialog dialog;

    public MutableLiveData<String> commentslist =new MutableLiveData<>();

    public String getDriverid(){
        return driverid;
    }

    public CommentslistViewModel(Activity context) {
        this.context = context;
        session = SessionManager.getInstance(context);
        cd  = ConnectionDetector.getInstance(context);
        HashMap<String, String> user = session.getUserDetails();
        driverid = user.get(SessionManager.KEY_DRIVERID);
        driverimage = user.get(SessionManager.KEY_DRIVER_IMAGE);

    }

    public void setIds(ActivityCommentslistConstrainBinding binding) {
        this.binding = binding;
        AppUtils.setImageView(context,driverimage,binding.imgpagelogo);
        commentslist();
    }

    public void nextpage(){
        binding.prevpage.setVisibility(View.VISIBLE);
        currentPage = currentPage + 1;
        commentslist();
    }

    public void previouspage(){
        currentPage = currentPage - 1;
        commentslist();
    }

    public void commentslist(){
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", driverid);
        jsonParams.put("page", String.valueOf(currentPage));
        jsonParams.put("perPage", perPage);

        ApIServices apIServices = new ApIServices(context, CommentslistViewModel.this);
        apIServices.dooperation(Iconstant.menupage_ratinglist, jsonParams);
    }

    public MutableLiveData<String> getCommentslist() {
        return commentslist;
    }

    @Override
    public void sucessresponse(String val) {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        commentslist.postValue(val);
    }

    @Override
    public void errorreponse() {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
    }

    @Override
    public void jsonexception() {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        AppUtils.toastprint(context,"Json Exception");
    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(context);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(context.getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }
}
