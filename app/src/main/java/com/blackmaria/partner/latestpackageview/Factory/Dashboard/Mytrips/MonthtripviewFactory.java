package com.blackmaria.partner.latestpackageview.Factory.Dashboard.Mytrips;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.mytrips.MonthviewdetailViewModel;


public class MonthtripviewFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public MonthtripviewFactory(Activity context) {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new MonthviewdetailViewModel(context);
    }
}
