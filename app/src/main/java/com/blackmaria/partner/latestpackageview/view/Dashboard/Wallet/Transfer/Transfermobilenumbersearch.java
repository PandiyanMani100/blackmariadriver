package com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Transfer;


import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityTransfermobilenumbersearchBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.Countrycodepicker.countrycodepicker.CountryPicker;
import com.blackmaria.partner.latestpackageview.Countrycodepicker.countrycodepicker.CountryPickerListener;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Wallet.Transfer.TransfermobilesearchFactory;
import com.blackmaria.partner.latestpackageview.Model.moneytransferpojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet.transfer.TransfermobilenumbersearchViewModel;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Locale;

public class Transfermobilenumbersearch extends AppCompatActivity {

    private ActivityTransfermobilenumbersearchBinding binding;
    private TransfermobilenumbersearchViewModel transfermobilenumbersearchViewModel;
    private SessionManager sessionManager;
    private String sDriverID = "", CountryCode = "";
    private AppUtils appUtils;
    private CountryPicker picker;
    private moneytransferpojo pojo;
    private JSONObject jsonpojo;
    AccessLanguagefromlocaldb db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
        binding = DataBindingUtil.setContentView(Transfermobilenumbersearch.this, R.layout.activity_transfermobilenumbersearch);
        transfermobilenumbersearchViewModel = ViewModelProviders.of(this, new TransfermobilesearchFactory(this)).get(TransfermobilenumbersearchViewModel.class);
        binding.setTransfermobilenumbersearchViewModel(transfermobilenumbersearchViewModel);
        transfermobilenumbersearchViewModel.setIds(binding);

        initView();
        clicklistener();
        getresponse();

        binding.codetxt.setText("INDONESIA");
        CountryCode = "+62";
        binding.codeimage.setImageResource(R.drawable.flag_mc);

        binding.datemonthyear.setText(db.getvalue("fastwallet"));
        binding.transfer.setText(db.getvalue("transfer"));
        binding.sendto.setText(db.getvalue("sendto"));
        binding.mobileno.setHint(db.getvalue("mobilenumber"));
        binding.confirm.setText(db.getvalue("confirm_lable"));
    }

    private void getresponse() {
        transfermobilenumbersearchViewModel.getFindfreindresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    jsonpojo = new JSONObject(response);
                    if (jsonpojo.getString("status").equalsIgnoreCase("1")) {
                        Type type = new TypeToken<moneytransferpojo>() {
                        }.getType();
                        pojo = new GsonBuilder().create().fromJson(jsonpojo.toString(), type);
                        Intent i = new Intent(Transfermobilenumbersearch.this, TransferamountenterConstrain.class);
                        i.putExtra("json", jsonpojo.toString());
                        startActivity(i);
                    } else {
                        appUtils.AlertError(Transfermobilenumbersearch.this,db.getvalue("action_error"), jsonpojo.getString("response").replace("User","Driver"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    private void initView() {
        sessionManager = SessionManager.getInstance(this);
        appUtils = AppUtils.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        picker = CountryPicker.newInstance(db.getvalue("select_country_lable"));
//        SmartLocation.with(this).location()
//                .start(new OnLocationUpdatedListener() {
//                    @Override
//                    public void onLocationUpdated(Location location) {
//                        Geocoder geocoder = new Geocoder(Transfermobilenumbersearch.this, Locale.getDefault());
//                        try {
//                            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
//                            if (addresses != null && !addresses.isEmpty()) {
//
//                                String Str_getCountryCode = addresses.get(0).getCountryCode();
//                                if (Str_getCountryCode.length() > 0 && !Str_getCountryCode.equals(null) && !Str_getCountryCode.equals("null")) {
//                                    String drawableName = "flag_"
//                                            + Str_getCountryCode.toLowerCase(Locale.ENGLISH);
//                                    CountryCode = Str_getCountryCode;
//                                    binding.codeimage.setImageResource(getResId(drawableName));
//                                    binding.codetxt.setText(addresses.get(0).getCountryName());
//                                }
//                            }
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                });
    }

    private void clicklistener() {
        binding.code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });

        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {
                picker.dismiss();
                binding.codetxt.setText(name);
                CountryCode = dialCode;
                String drawableName = "flag_"
                        + code.toLowerCase(Locale.ENGLISH);
                binding.codeimage.setImageResource(AppUtils.getResId(drawableName));
                AppUtils.CloseKeyboard(Transfermobilenumbersearch.this);
            }
        });

        binding.confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.mobileno.getText().length() == 0) {
                    appUtils.AlertError(Transfermobilenumbersearch.this, db.getvalue("action_error"), db.getvalue("profile_label_alert_mobilempty"));
                } else {
                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    jsonParams.put("driver_id", sDriverID);
                    jsonParams.put("country_code", CountryCode);
                    jsonParams.put("phone_number", binding.mobileno.getText().toString().trim());
                    transfermobilenumbersearchViewModel.findfriendapihitresponse(Iconstant.find_Friend_Account_url, jsonParams);
                }
            }
        });
    }


    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Log.v("TRIMMEMORY", " " + level);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }


}
