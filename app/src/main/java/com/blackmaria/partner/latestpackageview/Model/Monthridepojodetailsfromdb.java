package com.blackmaria.partner.latestpackageview.Model;

public class Monthridepojodetailsfromdb {
    private String completedtrip;
    private String canceltrip;
    private String drivercanceltrip;

    public String getCompletedtrip() {
        return completedtrip;
    }

    public void setCompletedtrip(String completedtrip) {
        this.completedtrip = completedtrip;
    }

    public String getCanceltrip() {
        return canceltrip;
    }

    public void setCanceltrip(String canceltrip) {
        this.canceltrip = canceltrip;
    }

    public String getDrivercanceltrip() {
        return drivercanceltrip;
    }

    public void setDrivercanceltrip(String drivercanceltrip) {
        this.drivercanceltrip = drivercanceltrip;
    }

    public String getMonthdescription() {
        return monthdescription;
    }

    public void setMonthdescription(String monthdescription) {
        this.monthdescription = monthdescription;
    }

    private String monthdescription;
}
