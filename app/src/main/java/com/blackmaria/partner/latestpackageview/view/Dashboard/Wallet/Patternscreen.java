package com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Pin_activitynew;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.blackmaria.partner.latestpackageview.widgets.PkDialogtryagain;
import com.blackmaria.partner.mylibrary.volley.ServiceRequest;
import com.itsxtt.patternlock.PatternLockView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class Patternscreen extends AppCompatActivity {

    private PatternLockView patternLockView;
    private ImageView iv_back;
    private TextView confirm,forgetpasword;
    private String ids_confirm = "";
    private int countcheck_wrongpattern = 1;
    private SessionManager session;
    AccessLanguagefromlocaldb db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawmycorrectpassword);
        db = new AccessLanguagefromlocaldb(this);
        session = new SessionManager(this);
        HashMap<String, String> getpattern = session.getpattern();
        String pattern = getpattern.get(SessionManager.KEY_PATTERN);
        ids_confirm = pattern;
        forgetpasword= findViewById(R.id.forgetpasword);
        patternLockView = findViewById(R.id.patternLockView);
        iv_back = findViewById(R.id.booking_back_imgeview);
        confirm = findViewById(R.id.confirm);


        forgetpasword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Pin_activitynew.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            }

        });

        patternLockView.setOnPatternListener(new PatternLockView.OnPatternListener() {
            @Override
            public void onStarted() {

            }

            @Override
            public void onProgress(ArrayList<Integer> ids) {

                System.out.println("progress........... " + ids);
            }

            @Override
            public boolean onComplete(ArrayList<Integer> ids) {
                /*
                 * A return value required
                 * if the pattern is not correct and you'd like change the pattern to error state, return false
                 * otherwise return true
                 */
                System.out.println("completed........... " + ids);
                return isPatternCorrect(ids);
            }
        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });


    }

    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(Patternscreen.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(db.getvalue("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                finish();

            }
        });
        mDialog.show();
    }


    private boolean isPatternCorrect(ArrayList<Integer> ids) {
        boolean value = false;
        String patern = ids.toString().replace(",", "");
        String paterns = patern.replace("[", "");
        String paternse = paterns.replace("]", "");

        if (isequal(paternse, ids_confirm)) {
            value = true;
            Intent intent = new Intent(getApplicationContext(), Fastpayhome.class);
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.exit);
            finish();


        } else {

            if (countcheck_wrongpattern == 1) {
                countcheck_wrongpattern++;
                Alertwrongpattern(db.getvalue("action_error"), db.getvalue("wro"));
            } else if (countcheck_wrongpattern == 2) {
                countcheck_wrongpattern++;
                Alertwrongpattern(db.getvalue("action_error"), db.getvalue("wra"));
            } else if (countcheck_wrongpattern == 3) {
                countcheck_wrongpattern = 1;
                closepopupinseconds(db.getvalue("wpp"), db.getvalue("rea"));
            }
            value = false;
            confirm.setVisibility(View.INVISIBLE);
        }
        return value;
    }


    private void closepopupinseconds(String title, final String alert) {

        final PkDialogtryagain mDialog = new PkDialogtryagain(Patternscreen.this);
        mDialog.setDialogTitle(title);
        mDialog.setCancelble(false);
        mDialog.setCancelOnTouchOutside(false);
        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                mDialog.setDialogMessage(" " + millisUntilFinished / 1000 + "\n \n \n " + alert);
            }

            public void onFinish() {
                mDialog.dismiss();
            }

        }.start();


        mDialog.show();
    }

    private boolean isequal(String pattern, String newpattern) {
        boolean val = false;

        if (pattern.equalsIgnoreCase(newpattern)) {
            val = true;
        }

        return val;
    }


    private void Alertwrongpattern(String title, String alert) {

        final PkDialogtryagain mDialog = new PkDialogtryagain(Patternscreen.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setCloseButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        mDialog.setPositiveButton(db.getvalue("spp"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                Intent intent = new Intent(getApplicationContext(), Pin_activitynew.class);
                startActivity(intent);
                overridePendingTransition(R.anim.enter, R.anim.exit);
                finish();
            }
        });
        mDialog.show();
    }


    public boolean equalLists(ArrayList<Integer> one, ArrayList<Integer> two) {
        if (one == null && two == null) {
            return true;
        }

        if ((one == null && two != null)
                || one != null && two == null
                || one.size() != two.size()) {
            return false;
        }

        //to avoid messing the order of the lists we will use a copy
        //as noted in comments by A. R. S.
        one = new ArrayList<Integer>(one);
        two = new ArrayList<Integer>(two);

        Collections.sort(one);
        Collections.sort(two);
        return one.equals(two);
    }


    private void updatepattern(String Url, String pattern) {

        final Dialog dialog = new Dialog(Patternscreen.this);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> info = session.getUserDetails();
        String UserID = info.get(SessionManager.KEY_DRIVERID);

        String  patern= pattern.replace(",","");
        String paterns = patern.replace("[","");
        String paternse = paterns.replace("]","");

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", UserID);
        jsonParams.put("pattern_code", paternse.trim());


        ServiceRequest mRequest = new ServiceRequest(Patternscreen.this);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                String Sstatus = "", Smessage = "";

                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    Smessage = object.getString("message");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        dialog.dismiss();
                        Alert(db.getvalue("action_success"), Smessage);
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    dialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

}
