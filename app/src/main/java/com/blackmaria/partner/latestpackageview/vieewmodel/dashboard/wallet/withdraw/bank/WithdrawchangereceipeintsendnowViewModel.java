package com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet.withdraw.bank;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import com.blackmaria.partner.R;
import com.blackmaria.partner.databinding.ActivityWithdrawchangereceipeintsendnowConstrainBinding;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;

import java.util.HashMap;

public class WithdrawchangereceipeintsendnowViewModel extends ViewModel implements ApIServices.completelisner {
    private Activity context;
    private Dialog dialog;
    private ActivityWithdrawchangereceipeintsendnowConstrainBinding binding;

    private MutableLiveData<String> withapihitresponse = new MutableLiveData<>();

    public WithdrawchangereceipeintsendnowViewModel(Activity context) {
        this.context = context;
    }

    public MutableLiveData<String> getWithapihitresponse() {
        return withapihitresponse;
    }

    public void withdrawapihit(String url, HashMap<String, String> jsonParams) {
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        ApIServices apIServices = new ApIServices(context, WithdrawchangereceipeintsendnowViewModel.this);
        apIServices.dooperation(url, jsonParams);
    }


    @Override
    public void sucessresponse(String val) {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        withapihitresponse.postValue(val);
    }

    @Override
    public void errorreponse() {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
    }

    @Override
    public void jsonexception() {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
    }

    public void setIds(ActivityWithdrawchangereceipeintsendnowConstrainBinding binding) {
        this.binding = binding;
    }

    public void back() {
        context.onBackPressed();
    }
}
