package com.blackmaria.partner.latestpackageview.Factory.Dashboard.Tracking;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.tracking.TrackArriveViewModel;


public class TrackArriveFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public TrackArriveFactory(Activity context) {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new TrackArriveViewModel(context);
    }
}
