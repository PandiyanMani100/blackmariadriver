package com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Recharge.Card;


import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityRechargecardpaymentConstrainBinding;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Wallet.RechargecardFactory;
import com.blackmaria.partner.latestpackageview.Model.rechargeamountpojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet.RechargecardpaymentViewModel;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class RechargecardpaymentConstrain extends AppCompatActivity {

    private ActivityRechargecardpaymentConstrainBinding binding;
    private RechargecardpaymentViewModel rechargecardpaymentViewModel;
    private String cardNumber = "", PUBLISHABLE_KEY = "", CURRENCYCONVERSIONKEY_KEY = "", insertAmount = "", currency = "";
    private AppUtils appUtils;
    private SessionManager sessionManager;
    private rechargeamountpojo pojomodel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(RechargecardpaymentConstrain.this, R.layout.activity_rechargecardpayment_constrain);
        rechargecardpaymentViewModel = ViewModelProviders.of(this, new RechargecardFactory(this)).get(RechargecardpaymentViewModel.class);
        binding.setRechargecardpaymentViewModel(rechargecardpaymentViewModel);
        rechargecardpaymentViewModel.setIds(binding);
        System.gc();
        initView();
        clicklistener();
    }

    private void initView() {
        sessionManager = SessionManager.getInstance(this);
        appUtils = AppUtils.getInstance(this);
        PUBLISHABLE_KEY = sessionManager.getXenditPublicKey();
        CURRENCYCONVERSIONKEY_KEY = sessionManager.getcurrencyconversionKey();
        rechargecardpaymentViewModel.spinner_month_and_year_process();
        if (getIntent().hasExtra("jsonpojo")) {
            insertAmount = getIntent().getStringExtra("insertAmount");
            currency = getIntent().getStringExtra("currency");
            binding.creditamount.setText(currency + " " + insertAmount);
            Type type = new TypeToken<rechargeamountpojo>() {
            }.getType();
            pojomodel = new GsonBuilder().create().fromJson(getIntent().getStringExtra("jsonpojo").toString(), type);
            if (pojomodel.getXendit_card_have().equalsIgnoreCase("1")) {
                binding.oldusedcard.setVisibility(View.VISIBLE);
                binding.oldcard.setText(pojomodel.getXendit_card_details().getNumber());
            } else {
                binding.oldusedcard.setVisibility(View.INVISIBLE);
            }
        }

    }

    private void clicklistener() {
        binding.confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardNumber = binding.cardnumberedt1.getText().toString() + binding.cardnumberedt2.getText().toString() + binding.cardnumberedt3.getText().toString() + binding.cardnumberedt4.getText().toString();
                if (cardNumber.length() == 0) {
                    appUtils.erroredit(RechargecardpaymentConstrain.this, binding.cardnumberedt1, getResources().getString(R.string.valid_card_no));
                } else if (cardNumber.length() < 16) {
                    if (binding.cardnumberedt1.getText().toString().length() < 4) {
                        appUtils.erroredit(RechargecardpaymentConstrain.this, binding.cardnumberedt1, getResources().getString(R.string.valid_card_no_enter));
                    } else if (binding.cardnumberedt2.getText().toString().length() < 4) {
                        appUtils.erroredit(RechargecardpaymentConstrain.this, binding.cardnumberedt2, getResources().getString(R.string.valid_card_no_enter));
                    } else if (binding.cardnumberedt3.getText().toString().length() < 4) {
                        appUtils.erroredit(RechargecardpaymentConstrain.this, binding.cardnumberedt3, getResources().getString(R.string.valid_card_no_enter));
                    } else if (binding.cardnumberedt4.getText().toString().length() < 4) {
                        appUtils.erroredit(RechargecardpaymentConstrain.this, binding.cardnumberedt4, getResources().getString(R.string.valid_card_no_enter));
                    }
                } else if (binding.cardholdername.getText().toString().length() < 0) {
                    appUtils.erroredit(RechargecardpaymentConstrain.this, binding.cardholdername, getResources().getString(R.string.valid_card_holder_name));
                } else if (binding.cvvedt.getText().toString().length() < 3) {
                    appUtils.erroredit(RechargecardpaymentConstrain.this, binding.cvvedt, getResources().getString(R.string.valid_cvv_code));
                } else {
                    rechargecardpaymentViewModel.CreateXenditToken(cardNumber, insertAmount, currency);
                }
            }
        });

        binding.use.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pojomodel.getXendit_card_have().equalsIgnoreCase("1")) {
                    Double amount = Double.parseDouble(insertAmount) * Double.parseDouble(CURRENCYCONVERSIONKEY_KEY);
                    amount = (Double) Math.ceil(amount);
                    rechargecardpaymentViewModel.CreateAuthenticationid(pojomodel.getXendit_card_details().getId(), amount, insertAmount);
                }
            }
        });

        binding.cardnumberedt1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (binding.cardnumberedt1.getText().toString().length() == 4) {
                    binding.cardnumberedt2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        binding.cardnumberedt2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (binding.cardnumberedt2.getText().toString().length() == 4) {
                    binding.cardnumberedt3.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        binding.cardnumberedt3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (binding.cardnumberedt3.getText().toString().length() == 4) {
                    binding.cardnumberedt4.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        binding.cardnumberedt4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (binding.cardnumberedt4.getText().toString().length() == 4) {

                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Log.v("TRIMMEMORY", " " + level);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }

    public String getPublickey() {
        return PUBLISHABLE_KEY;
    }

    public String getCurrencykey() {
        return CURRENCYCONVERSIONKEY_KEY;
    }

}
