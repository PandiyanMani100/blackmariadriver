package com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.mytrips;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityTodaytripsViewDetailConstrainBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

public class TodayviewdetailsViewModel extends ViewModel implements ApIServices.completelisner {
    public Activity context;
    private ActivityTodaytripsViewDetailConstrainBinding binding;
    private Dialog dialog;
    private MutableLiveData<String> todaytrips = new MutableLiveData<>();
    private String driverid = "", contentPerPage = "10", sFilterDate = "";
    private int pageNo = 1;
    private SessionManager sessionManager;
    private Calendar calendar;
    private SimpleDateFormat mdFormat;

    public TodayviewdetailsViewModel(Activity context) {
        this.context = context;
        sessionManager = SessionManager.getInstance(context);
        HashMap<String, String> user = sessionManager.getUserDetails();
        driverid = user.get(SessionManager.KEY_DRIVERID);
        calendar = Calendar.getInstance();
        mdFormat = new SimpleDateFormat("yyyy-MM-dd");
        sFilterDate = mdFormat.format(calendar.getTime());
    }

    public void setIds(ActivityTodaytripsViewDetailConstrainBinding binding) {
        this.binding = binding;
    }

    public void gettodaytrips(String date,boolean value) {
        if(value) {
            binding.apiload.setVisibility(View.VISIBLE);
            dialog = new Dialog(context, R.style.CustomDialogTheme);
            dialog.getWindow();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.loading_model);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", driverid);
        jsonParams.put("filter_date", date);

        ApIServices apIServices = new ApIServices(context, TodayviewdetailsViewModel.this);
        apIServices.dooperation(Iconstant.today_trips_details_url, jsonParams);
    }

    public void right() {
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", driverid);
        jsonParams.put("page", String.valueOf(pageNo + 1));
        jsonParams.put("perPage", contentPerPage);
        jsonParams.put("filter_date", sFilterDate);

        ApIServices apIServices = new ApIServices(context, TodayviewdetailsViewModel.this);
        apIServices.dooperation(Iconstant.today_trips_details_url, jsonParams);
    }

    public void left() {
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", driverid);
        jsonParams.put("page", String.valueOf(pageNo - 1));
        jsonParams.put("perPage", contentPerPage);
        jsonParams.put("filter_date", sFilterDate);

        ApIServices apIServices = new ApIServices(context, TodayviewdetailsViewModel.this);
        apIServices.dooperation(Iconstant.today_trips_details_url, jsonParams);
    }

    public MutableLiveData<String> getTodaytrips() {
        return todaytrips;
    }

    public void back(){
        context.onBackPressed();
    }
    @Override
    public void sucessresponse(String val) {
        if(dialog!=null){
            dialog.dismiss();
            binding.apiload.setVisibility(View.INVISIBLE);
        }
        todaytrips.postValue(val);
    }

    @Override
    public void errorreponse() {
        if(dialog!=null){
            dialog.dismiss();
            binding.apiload.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void jsonexception() {
        if(dialog!=null){
            dialog.dismiss();
            binding.apiload.setVisibility(View.INVISIBLE);
        }
    }
}
