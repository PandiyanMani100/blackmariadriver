package com.blackmaria.partner.latestpackageview.view.Dashboard;

import android.Manifest;
import android.app.Activity;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;

import androidx.databinding.DataBindingUtil;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;

import androidx.annotation.Nullable;

import android.os.Bundle;

import androidx.fragment.app.FragmentTransaction;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;

import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferService;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.HeaderSessionManager;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.app.AboutUs;
import com.blackmaria.partner.databinding.ActivityDashboardConstrainBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApiIntentService;
import com.blackmaria.partner.latestpackageview.Database.RidesDB;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.DashboardFactory;
import com.blackmaria.partner.latestpackageview.Model.clickdisabledashboardpojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.DashboardViewModel;
import com.blackmaria.partner.latestpackageview.location.Service.LocationUpdatesService;
import com.blackmaria.partner.latestpackageview.view.sidemenu.CloseAccount.DeActiviationAccount;
import com.blackmaria.partner.latestpackageview.view.sidemenu.Invitefriends.Invitefriends_constrain;
import com.blackmaria.partner.latestpackageview.view.sidemenu.LegalPage;
import com.blackmaria.partner.latestpackageview.view.sidemenu.Maintanence.Maintanencedashboard;
import com.blackmaria.partner.latestpackageview.view.sidemenu.Maintanence.Maintanenceopendashboard;
import com.blackmaria.partner.latestpackageview.view.sidemenu.Maintanence.Maintanenceopendashboard1;
import com.blackmaria.partner.latestpackageview.view.sidemenu.Ratings.RatingHomepageConstrain;
import com.blackmaria.partner.latestpackageview.view.sidemenu.Warnings.Warningsconstrain;
import com.blackmaria.partner.latestpackageview.view.sidemenu.profile.ProfileConstrain;
import com.blackmaria.partner.latestpackageview.view.signup.FetchingDataPage_constrain;
import com.blackmaria.partner.latestpackageview.view.signup.Splashscreen;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.yalantis.ucrop.UCrop;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import de.cketti.mailto.EmailIntentBuilder;

import static com.yalantis.ucrop.util.BitmapLoadUtils.exifToDegrees;

public class Dashboard_constrain extends AppCompatActivity {

    private DashboardViewModel dashboardViewModel;
    private static ActivityDashboardConstrainBinding binding;

    private static final int PERMISSION_REQUEST_CODE = 111;
    private int permissionCode = -1;
    private static final int SELECT_IMAGE_REQUEST = 011;
    private static final int TAKE_PHOTO_REQUEST = 022;
    private AppUtils appUtils;
    private SessionManager sessionManager;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private String sDrivername = "";
    private Activity context;
    private Dialog dialog;

    private boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private String sDriverID = "", Agent_Name = "", language_code = "", gcmID = "";


    String File_names = "";
    String Extension = "";
    String uploaded_file_name = "";
    private String s3_bucket_access_key = "";
    private String s3_bucket_secret_key = "";
    private String s3_bucket_name = "";
    private AmazonS3Client s3Client;
    private BasicAWSCredentials credentials;
    TransferObserver uploadObserver;


    private File imageRoot, destination;
    private Uri mImageCaptureUri, outputUri;
    private String appDirectoryName = "";
    private byte[] array = null;
    private HeaderSessionManager headerSessionManager;
    private int imageup = 0;
    AccessLanguagefromlocaldb db;

    private LiveData<String> imageupload = null;
    private LiveData<String> logoutresponse = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        context = this;
        binding = DataBindingUtil.setContentView(Dashboard_constrain.this, R.layout.activity_dashboard_constrain);
        dashboardViewModel = ViewModelProviders.of(this).get(DashboardViewModel.class);
        binding.setDashboardViewModel(dashboardViewModel);

        imageupload = new MutableLiveData<>();
        logoutresponse = new MutableLiveData<>();

        imageupload = new LiveData<String>() {
            @Override
            public void observe(@NonNull LifecycleOwner owner, @NonNull Observer<? super String> observer) {
                super.observe(owner, observer);

                System.out.println("sdfaadfads");
            }
        };

        initView(savedInstanceState);

        appUtils.runbackground(Dashboard_constrain.this);

        binding.profilelabel.setText(db.getvalue("sidemenu_tvprofile"));
        binding.acco.setText(db.getvalue("account"));
        binding.main.setText(db.getvalue("maintenance"));
        binding.drie.setText(db.getvalue("driveearn"));
        binding.rate.setText(db.getvalue("sidemenu_tvratings"));
        binding.warnn.setText(db.getvalue("warnings"));
        binding.tvDrivewithus.setText(db.getvalue("becomeagent"));

        binding.abus.setText(db.getvalue("sidemenu_aboutus"));
        binding.termss.setText(db.getvalue("sidemenu_tvterms"));
        binding.closaa.setText(db.getvalue("sidemenu_closeaccount"));
        binding.tvLogout.setText(db.getvalue("logout_lable"));



        dashboardViewModel.getImageupload().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    dialog.dismiss();
                    JSONObject jsonObject = new JSONObject(response);
                    String sStatus = jsonObject.getString("status");
                    if (sStatus.equalsIgnoreCase("1")) {
                        String image_path = jsonObject.getString("image_path");
                        sessionManager.setDriverImageUpdate(image_path);
                        RequestOptions options = new RequestOptions()
                                .centerCrop()
                                .placeholder(R.drawable.new_no_user_img)
                                .error(R.drawable.new_no_user_img)
                                .diskCacheStrategy(DiskCacheStrategy.ALL);
                        Glide.with(Dashboard_constrain.this).load(image_path).apply(options).into(binding.ivuserbg);
                    } else {
                        String Smsg = jsonObject.getString("response");
                        appUtils.AlertError(Dashboard_constrain.this,db.getvalue("action_error"), Smsg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


        dashboardViewModel.getLogoutresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    dialog.dismiss();
                    JSONObject object = new JSONObject(response);
                    String Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        RidesDB ridesDB = new RidesDB(Dashboard_constrain.this);
                        ridesDB.cleartable();
                        sessionManager.logoutUser();
                        Intent intent = new Intent(Dashboard_constrain.this, Splashscreen.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
                        finish();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });

    }

    public static void openDrawer() {
        try {
            binding.navigationdrawer.openDrawer(binding.drawer);
            clickdisabledashboardpojo pojo = new clickdisabledashboardpojo();
            pojo.setIsenabled(true);
            EventBus.getDefault().post(pojo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void enableSwipeDrawer() {
        binding.navigationdrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
//        binding.navigationdrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    private void initView(Bundle savedInstanceState) {
        startService(new Intent(this, LocationUpdatesService.class));
        appUtils = AppUtils.getInstance(this);
        appUtils.checkxmpp(Dashboard_constrain.this);
        sessionManager = SessionManager.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDrivername = user.get(SessionManager.KEY_DRIVER_NAME);
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        gcmID = user.get(SessionManager.KEY_GCM_ID);
        appUtils.loadinganimation(binding.loading);
        binding.name.setText(sDrivername);


        sessionManager = SessionManager.getInstance(context);
        headerSessionManager = new HeaderSessionManager(context);
        appUtils = AppUtils.getInstance(context);
        cd = ConnectionDetector.getInstance(context);


        appDirectoryName = context.getResources().getString(R.string.app_name);
        HashMap<String, String> header = headerSessionManager.getHeaderSession();
        Agent_Name = header.get(HeaderSessionManager.KEY_ID_NAME);
        language_code = header.get(HeaderSessionManager.KEY_LANGUAGE_CODE);


        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.new_no_user_img)
                .error(R.drawable.new_no_user_img)
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        Glide.with(this).load(sessionManager.getDriverImageUpdate()).apply(options).into(binding.ivuserbg);

        actionBarDrawerToggle = new ActionBarDrawerToggle(Dashboard_constrain.this, binding.navigationdrawer, R.string.app_name, R.string.app_name);
        binding.navigationdrawer.setDrawerListener(actionBarDrawerToggle);

        binding.navigationdrawer.post(new Runnable() {
            @Override
            public void run() {
                actionBarDrawerToggle.syncState();
            }
        });
        if (savedInstanceState == null) {
            FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
            tx.replace(R.id.content_frame, new DashboardFragment());
            tx.commit();
        }


        enableSwipeDrawer();

        binding.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backclick();
            }
        });
        binding.lvaccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openaccount();
            }
        });
        binding.lvaccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openmaintance();
            }
        });
        binding.lvprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openprofile();
            }
        });
        binding.lvmaintance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openmaintance1();
            }
        });
        binding.lvearn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openearn();
            }
        });
        binding.lvrating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openrating();
            }
        });
        binding.lvwarnings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openwarning();
            }
        });
        binding.rlDrivewihtus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                becomeagent();
            }
        });
        binding.lvAboutus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openaboutus();
            }
        });
        binding.lvTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openterms();
            }
        });
        binding.lvCloseaccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                opencloseaccount();
            }
        });
        binding.tvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logout();
            }
        });
        binding.ivuserbg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                uploadpohoto();
            }
        });
    }

    public static void setTextmenu(String warningcount, String earningscount) {
        if (!warningcount.equalsIgnoreCase("0")) {
            binding.tvwarningscount.setText(warningcount);
            binding.tvwarningscount.setVisibility(View.VISIBLE);
        }
//        if (!earningscount.equalsIgnoreCase("0")) {
//            binding.tvearncount.setText(earningscount);
//            binding.tvearncount.setVisibility(View.VISIBLE);
//        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (permissionCode == 0) {
                        cameraIntent();
                    } else if (permissionCode == 1) {
                        galleryIntent();
                    }
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_IMAGE_REQUEST) {
                onSelectFromGalleryResult(data);
            } else if (requestCode == TAKE_PHOTO_REQUEST) {
                onCaptureImageResult(data);
            } else if (requestCode == UCrop.REQUEST_CROP) {
                onCroppedImageResult(data);
            } else if (resultCode == UCrop.RESULT_ERROR) {
                final Throwable cropError = UCrop.getError(data);
            }
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }

    public void logout() {
        final PkDialog mDialog = new PkDialog(context);
        mDialog.setDialogTitle(db.getvalue("dialog_app_logout1"));
        mDialog.setDialogMessage(db.getvalue("dialog_app_logout"));
        mDialog.setPositiveButton(db.getvalue("ok_lable"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                try {
                    dialog = new Dialog(context);
                    dialog.getWindow();
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.custom_loading);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.show();
                    TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
                    dialog_title.setText(db.getvalue("action_loading"));

                    Thread thread = new Thread() {
                        @Override
                        public void run() {
                            HashMap<String, String> jsonParams = new HashMap<>();
                            jsonParams.put("driver_id", sDriverID);
                            jsonParams.put("device", "ANDROID");
                            dashboardViewModel.logoutapi(Iconstant.Profile_driver_account_Logout_Url, jsonParams);

                        }
                    };
                    thread.start();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        mDialog.setNegativeButton(db.getvalue("cancel_lable"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    public void backclick() {
        binding.navigationdrawer.closeDrawer(binding.drawer);
        clickdisabledashboardpojo pojo = new clickdisabledashboardpojo();
        pojo.setIsenabled(false);
        EventBus.getDefault().post(pojo);
        EventBus.getDefault().post("reload");

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void reloadimage(String obj) {

    }
    public void uploadpohoto() {
        imageup = 0;
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            if (!checkCameraPermission() || !checkReadExternalStoragePermission() || !checkWriteExternalStoragePermission()) {
                requestPermission();
            } else {
                chooseimage();
            }
        } else {
            chooseimage();
        }
    }

    public void chooseimage() {
        final Dialog photo_dialog = new Dialog(context);
        photo_dialog.getWindow();
        photo_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        photo_dialog.setContentView(R.layout.image_upload_dialog);
        photo_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        photo_dialog.setCanceledOnTouchOutside(true);
        photo_dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_photo_Picker;
        photo_dialog.show();
        photo_dialog.getWindow().setGravity(Gravity.CENTER);

        RelativeLayout camera = (RelativeLayout) photo_dialog
                .findViewById(R.id.profilelayout_takephotofromcamera);
        RelativeLayout gallery = (RelativeLayout) photo_dialog
                .findViewById(R.id.profilelayout_takephotofromgallery);

        TextView howdo = (TextView) photo_dialog
                .findViewById(R.id.howdo);
        TextView tph = (TextView) photo_dialog
                .findViewById(R.id.tph);
        TextView chosex = (TextView) photo_dialog
                .findViewById(R.id.chosex);
        howdo.setText(db.getvalue("takephoto"));
        tph.setText(db.getvalue("camra"));
        chosex.setText(db.getvalue("existingcamera"));

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraIntent();
                photo_dialog.dismiss();
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                galleryIntent();
                photo_dialog.dismiss();
            }
        });
    }

    public void cameraIntent() {
//        try {
//            imageRoot = new File(Environment.getExternalStoragePublicDirectory(
//                    Environment.DIRECTORY_PICTURES), appDirectoryName);
//            if (!imageRoot.exists()) {
//                imageRoot.mkdir();
//            }
//            String name = dateToString(new Date(), "yyyy-MM-dd-hh-mm-ss");
//            destination = new File(imageRoot, name + ".jpg");
//            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(destination));
//            intent.putExtra("android.intent.extras.CAMERA_FACING", 1);
//            context.startActivityForResult(intent, TAKE_PHOTO_REQUEST);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        try {
            Intent pictureIntent = new Intent(
                    MediaStore.ACTION_IMAGE_CAPTURE);
            if (pictureIntent.resolveActivity(getPackageManager()) != null) {
                //Create a file to store the image
                imageRoot = new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES), appDirectoryName);
            }
            if (imageRoot != null) {
                String name = dateToString(new Date(), "yyyy-MM-dd-hh-mm-ss");
                destination = new File(imageRoot, name + ".jpg");
                Uri photoURI = FileProvider.getUriForFile(this, "com.blackmaria.provider", destination);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        photoURI);
                pictureIntent.putExtra("android.intent.extras.CAMERA_FACING", 1);
                startActivityForResult(pictureIntent,
                        TAKE_PHOTO_REQUEST);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public String dateToString(Date date, String format) {
        SimpleDateFormat df = new SimpleDateFormat(format);
        return df.format(date);
    }

    public void galleryIntent() {
        try {
            imageRoot = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES), appDirectoryName);
            if (!imageRoot.exists()) {
                imageRoot.mkdir();
            }
            Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setType("image/*");
            context.startActivityForResult(intent, SELECT_IMAGE_REQUEST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean checkCameraPermission() {
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkReadExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkWriteExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(context, new String[]{Manifest.permission.CAMERA, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
    }

    public void onCaptureImageResult(Intent data) {
        try {
            String imagePath = destination.getAbsolutePath();
            mImageCaptureUri = Uri.fromFile(new File(imagePath));
            outputUri = mImageCaptureUri;

            UCrop.Options options = new UCrop.Options();
            options.setStatusBarColor(context.getResources().getColor(R.color.black_color));
            options.setToolbarColor(context.getResources().getColor(R.color.app_primary_color));
            options.setMaxBitmapSize(800);

            UCrop.of(mImageCaptureUri, outputUri)
                    .withAspectRatio(1, 1)
                    .withMaxResultSize(8000, 8000)
                    .withOptions(options)
                    .start(context);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onSelectFromGalleryResult(Intent data) {

        try {
            mImageCaptureUri = data.getData();

            if (!imageRoot.exists()) {
                imageRoot.mkdir();
            }

            String name = dateToString(new Date(), "yyyy-MM-dd-hh-mm-ss");
            destination = new File(imageRoot, name + ".jpg");

            outputUri = Uri.fromFile(destination);

            UCrop.Options options = new UCrop.Options();
            options.setStatusBarColor(context.getResources().getColor(R.color.black_color));
            options.setToolbarColor(context.getResources().getColor(R.color.app_primary_color));
            options.setMaxBitmapSize(800);

            UCrop.of(mImageCaptureUri, outputUri)
                    .withAspectRatio(1, 1)
                    .withMaxResultSize(8000, 8000)
                    .withOptions(options)
                    .start(context);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void onCroppedImageResult(Intent data) {

        final Uri resultUri = UCrop.getOutput(data);

        Log.d("Crop success", "" + resultUri);
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), resultUri);

            if (bitmap == null) {
                Log.d("Bitmap", "null");
            } else {
                Log.d("Bitmap", "not null");
//                binding.ivuserbg.setImageBitmap(bitmap);
            }

            Bitmap thumbnail = bitmap;
            final String picturePath = resultUri.getPath();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            File curFile = new File(picturePath);
            try {
                ExifInterface exif = new ExifInterface(curFile.getPath());
                int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                int rotationInDegrees = exifToDegrees(rotation);

                Matrix matrix = new Matrix();
                if (rotation != 0f) {
                    matrix.preRotate(rotationInDegrees);
                }
                thumbnail = Bitmap.createBitmap(thumbnail, 0, 0, thumbnail.getWidth(), thumbnail.getHeight(), matrix, true);
            } catch (IOException ex) {
                Log.e("TAG", "Failed to get Exif data", ex);
            }

            thumbnail.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
            array = byteArrayOutputStream.toByteArray();

            if (cd.isConnectingToInternet()) {
                dialog = new Dialog(context);
                dialog.getWindow();
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.custom_loading);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();

                TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
                dialog_title.setText(db.getvalue("action_loading"));



                if(sessionManager.getimagestatus().equals("1"))
                {

                    s3_bucket_name = sessionManager.getbucketname();
                    s3_bucket_access_key = sessionManager.getaccesskey();
                    s3_bucket_secret_key = sessionManager.getsecretkey();

                    context.startService(new Intent(context, TransferService.class));
                    credentials = new BasicAWSCredentials(s3_bucket_access_key, s3_bucket_secret_key);
                    s3Client = new AmazonS3Client(credentials);
                    s3Client.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));
                    uploadToS3Bucket(curFile,context);
                }
                else
                {
                    Thread thread = new Thread() {
                        @Override
                        public void run() {

                            imageupload = dashboardViewModel.getimageupload(Iconstant.driverimageupdate, array, "blackmariadriver.jpg");

                        }
                    };
                    thread.start();
                }




            } else {
                appUtils.AlertError(context,db.getvalue("alert_nointernet"), db.getvalue("alert_nointernet_message"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void uploadToS3Bucket(File fileUri,Activity context)
    {

        if(fileUri.exists())
        {
            System.out.println("file exist");
        }
        else
        {
            System.out.println("file exist not");
        }


        String[] path_split;
        if (fileUri != null) {

            final Dialog dialog = new Dialog(context);
            dialog.getWindow();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.custom_loading);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();

            uploaded_file_name = "";
            String path = String.valueOf(fileUri);
            String file_name_full = path.substring(path.lastIndexOf("/") + 1);
            path_split = file_name_full.split(".");
            if (path_split.length == 2) {
                File_names = path_split[0];
                Extension = path_split[1];

            } else {
                path_split = file_name_full.split(".jpg");
                if (path_split.length == 1) {
                    File_names = path_split[0];
                }
            }

            String file_name_upload = File_names;
            //current time stamp
            Long tsLong = System.currentTimeMillis() / 1000;
            String str_current_ts = tsLong.toString();
            String concat_image_name = file_name_upload + "" + str_current_ts;
            final String fileName = md5Encryption(concat_image_name) + "." + "jpg";


            TransferUtility transferUtility =
                    TransferUtility.builder()
                            .context(context)
                            .defaultBucket(s3_bucket_name)
                            .s3Client(s3Client)
                            .build();
            uploaded_file_name = fileName;

            uploadObserver = transferUtility.upload("images/users/" + fileName, fileUri, CannedAccessControlList.PublicRead);
            // Attach a listener to the observer to get state update and progress notifications
            uploadObserver.setTransferListener(new TransferListener() {

                @Override
                public void onStateChanged(int id, TransferState state) {
                    // Handle a completed upload.
                    if (TransferState.COMPLETED == state) {
                        dialog.dismiss();
                        String path = uploadObserver.getAbsoluteFilePath();
                        Thread thread = new Thread() {
                            @Override
                            public void run() {

                                imageupload = dashboardViewModel.getimageamazonupload(Iconstant.driverimageupdate, fileName);



                            }
                        };
                        thread.start();
                    } else if (TransferState.FAILED == state) {
                        // dialog.dismiss();
                        dialog.dismiss();
                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                    float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                    int percentDone = (int) percentDonef;
                    Log.d("YourActivity", "ID:" + id + " bytesCurrent: " + bytesCurrent + " bytesTotal: " + bytesTotal + " " + percentDone + "%");
                }

                @Override
                public void onError(int id, Exception ex) {
                    // Handle errors
                    ex.printStackTrace();
                    uploaded_file_name = "";

                    dialog.dismiss();
                }

            });

            // If you prefer to poll for the data, instead of attaching a
            // listener, check for the state and progress in the observer.
            if (TransferState.COMPLETED == uploadObserver.getState()) {
                // Handle a completed upload.
                Log.d("completed upload", "completed upload");
            }

        }
    }


    public String md5Encryption(String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public void openterms() {
        Intent intent = new Intent(context, LegalPage.class);
        context.startActivity(intent);
        context.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    public void openaboutus() {
        Intent intent = new Intent(context, AboutUs.class);
        context.startActivity(intent);
        context.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    public void becomeagent() {
        try {
            boolean success = EmailIntentBuilder.from(context)
                    .to(sessionManager.getContactEmail())
                    .subject("Become Agent")
                    .body("")
                    .start();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    public void openwarning() {
        context.startActivity(new Intent(context, Warningsconstrain.class));
        context.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    public void openrating() {
        Intent intent_sh = new Intent(context, RatingHomepageConstrain.class);
        context.startActivity(intent_sh);
        context.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    public void openearn() {
        Intent intent = new Intent(context, Invitefriends_constrain.class);
        context.startActivity(intent);
        context.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    public void openmaintance() {
        if (sessionManager.getaccountownertype().equalsIgnoreCase("owner")) {

            Thread thread = new Thread() {
                @Override
                public void run() {
                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    Intent intents = new Intent(context, ApiIntentService.class);
                    intents.putExtra("params", jsonParams);
                    intents.putExtra("url", Iconstant.getvehicledetails);
                    context.startService(intents);
                }
            };
            thread.start();


            if (sessionManager.getseonddriverid().equalsIgnoreCase("")) {
                Intent intent = new Intent(context, Maintanenceopendashboard.class);
                context.startActivity(intent);
                context.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            } else {
                Intent intent = new Intent(context, Maintanencedashboard.class);
                intent.putExtra("driverid", sessionManager.getseonddriverid());
                intent.putExtra("from", "dashboard");
                context.startActivity(intent);
                context.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        } else {
            appUtils.AlertError(context,db.getvalue("action_error"),db.getvalue("spos"));
        }
    }
    public void openmaintance1() {
        if (sessionManager.getaccountownertype().equalsIgnoreCase("owner")) {

            Thread thread = new Thread() {
                @Override
                public void run() {
                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    Intent intents = new Intent(context, ApiIntentService.class);
                    intents.putExtra("params", jsonParams);
                    intents.putExtra("url", Iconstant.getvehicledetails);
                    context.startService(intents);
                }
            };
            thread.start();


            if (sessionManager.getseonddriverid().equalsIgnoreCase("")) {
                Intent intent = new Intent(context, Maintanenceopendashboard1.class);
                context.startActivity(intent);
                context.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            } else {
                Intent intent = new Intent(context, Maintanenceopendashboard1.class);
                intent.putExtra("driverid", sessionManager.getseonddriverid());
                intent.putExtra("from", "dashboard");
                context.startActivity(intent);
                context.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        } else {
            appUtils.AlertError(context, db.getvalue("action_error"),db.getvalue("sss"));
        }
    }


    public void openaccount() {
        appUtils.AlertError(context, db.getvalue("action_error"), db.getvalue("inpa"));
//        Intent intent = new Intent(context, Myaccount.class);
//        Intent intent = new Intent(context, DriverAccount.class);
//        context.startActivity(intent);
//        context.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    public void opencloseaccount() {
        final Dialog dialog = new Dialog(context, R.style.DialogSlideAnim2);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.accountclosepopup);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        Button closeImage = dialog.findViewById(R.id.custom_dialog_library_ok_button);
        closeImage.setText(db.getvalue("no"));
        Button InRTv = dialog.findViewById(R.id.custom_dialog_library_cancel_button);
        InRTv.setText(db.getvalue("yes"));
        TextView custom_dialog_library_message_textview1 = dialog.findViewById(R.id.custom_dialog_library_message_textview1);
        custom_dialog_library_message_textview1.setText(db.getvalue("info"));
        TextView custom_title = dialog.findViewById(R.id.custom_title);
        custom_title.setText(db.getvalue("titlecloseaccount"));
        TextView custom_dialog_library_message_textview = dialog.findViewById(R.id.custom_dialog_library_message_textview);
        custom_dialog_library_message_textview.setText(db.getvalue("content"));



        InRTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent closeIntent = new Intent(context, DeActiviationAccount.class);
                context.startActivity(closeIntent);
                context.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                dialog.dismiss();
            }
        });

        closeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }

        });

        dialog.show();
    }

    public void openprofile() {
        Intent intent = new Intent(context, ProfileConstrain.class);
        context.startActivity(intent);
        context.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }


}
