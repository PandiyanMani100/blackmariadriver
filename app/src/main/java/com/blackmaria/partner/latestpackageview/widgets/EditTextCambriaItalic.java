package com.blackmaria.partner.latestpackageview.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;


public class EditTextCambriaItalic extends androidx.appcompat.widget.AppCompatEditText {

    public EditTextCambriaItalic(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public EditTextCambriaItalic(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public EditTextCambriaItalic(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Cambria-Italic.ttf");
            setTypeface(tf);
        }
    }

}
