package com.blackmaria.partner.latestpackageview.vieewmodel.signup;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.GPSTracker;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityVerifyPhonenoConstrainBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;

import java.util.HashMap;

public class VerifyPhonenoViewModel extends ViewModel implements ApIServices.completelisner {

    private Activity context;
    private HashMap<String, String> hashMap = new HashMap<>();
    private MutableLiveData<String> stringMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<String> resendotprespone = new MutableLiveData<>();
    private MutableLiveData<String> validationerrors = new MutableLiveData<>();
    private MutableLiveData<String> changenumber = new MutableLiveData<>();
    private Dialog dialog;
    private ActivityVerifyPhonenoConstrainBinding binding;
    private GPSTracker gps;
    AccessLanguagefromlocaldb db;
    private boolean isresend = false;
    private SessionManager sessionManager;
    private String sOtpPin = "", dialcode = "", mobile = "";
    private boolean ischangenumberhit = false;
    private int count = 0;

    public VerifyPhonenoViewModel(Activity context) {
        this.context = context;
        db = new AccessLanguagefromlocaldb(context);
        gps = GPSTracker.getInstance(context);
        sessionManager = SessionManager.getInstance(context);
        HashMap<String, String> user1 = sessionManager.getOtpstatusAndPin();
        sOtpPin = user1.get(SessionManager.KEY_REG_OTP_PIN);
    }

    public MutableLiveData<String> getChangenumber() {
        return changenumber;
    }

    public MutableLiveData<String> getStringMutableLiveData() {
        return stringMutableLiveData;
    }

    public MutableLiveData<String> getResendotprespone() {
        return resendotprespone;
    }

    public MutableLiveData<String> getValidationerrors() {
        return validationerrors;
    }

    public void setHashMap(HashMap<String, String> hashMap) {
        this.hashMap = hashMap;
    }

    public void setIds(ActivityVerifyPhonenoConstrainBinding binding) {
        this.binding = binding;
    }


    public void resendotp() {
        isresend = true;
        count = 0;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("dail_code", hashMap.get("dail_code"));
        jsonParams.put("mobile_number", hashMap.get("mobile_number"));
        jsonParams.put("lat", String.valueOf(gps.getLatitude()));
        jsonParams.put("lon", String.valueOf(gps.getLongitude()));


        ApIServices apIServices = new ApIServices(context, VerifyPhonenoViewModel.this);
        apIServices.dooperation(Iconstant.DriverRegistration_VerifyMobile_URl, jsonParams);
    }

    public void changenumber(String dialcode, String number) {
        count = 2;
        isresend = true;
        this.dialcode = dialcode;
        this.mobile = number;
        ischangenumberhit = true;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("dail_code", dialcode);
        jsonParams.put("mobile_number", number);
        jsonParams.put("lat", String.valueOf(gps.getLatitude()));
        jsonParams.put("lon", String.valueOf(gps.getLongitude()));


        ApIServices apIServices = new ApIServices(context, VerifyPhonenoViewModel.this);
        apIServices.dooperation(Iconstant.DriverRegistration_VerifyMobile_URl, jsonParams);
    }

    public void authenticate() {
        count = 1;
        isresend = false;
        HashMap<String, String> user1 = sessionManager.getOtpstatusAndPin();
        sOtpPin = user1.get(SessionManager.KEY_REG_OTP_PIN);
        if (binding.otpview.getText().toString().length() == 0) {
            validationerrors.postValue(db.getvalue("otpca"));
        } else if (binding.otpview.getText().toString().length() != 6) {
            validationerrors.postValue(db.getvalue("otpcannem"));
        } else if (!binding.otpview.getText().toString().equalsIgnoreCase(sOtpPin)) {
            validationerrors.postValue(db.getvalue("mism"));
        } else {
            binding.apiload.setVisibility(View.VISIBLE);
            dialog = new Dialog(context, R.style.CustomDialogTheme);
            dialog.getWindow();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.loading_model);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();

            if (ischangenumberhit) {
                hashMap.replace("dail_code", dialcode);
                hashMap.replace("mobile_number", mobile);
            }

            ApIServices apIServices = new ApIServices(context, VerifyPhonenoViewModel.this);
            apIServices.dooperation(Iconstant.DriverRegistration_URl, hashMap);
        }
    }

    @Override
    public void sucessresponse(String val) {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        if (count == 0) {
            resendotprespone.postValue(val);
        } else if(count ==1){
            stringMutableLiveData.postValue(val);
        }else if(count ==2){
            changenumber.postValue(val);
        }

    }

    @Override
    public void errorreponse() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void jsonexception() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        Toast.makeText(context, "Json Exception", Toast.LENGTH_SHORT).show();
    }
}
