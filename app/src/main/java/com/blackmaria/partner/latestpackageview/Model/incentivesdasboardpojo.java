package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class incentivesdasboardpojo implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }

    public class Response {
        @SerializedName("total_amount")
        private String total_amount;
        @SerializedName("currency")
        private String currency;
        @SerializedName("month_statement")
        Month_statement Month_statementObject;
        @SerializedName("total_payout_amount")
        private String total_payout_amount;

        public ArrayList<Incetives_count> getIncetives_count() {
            return incetives_count;
        }

        public void setIncetives_count(ArrayList<Incetives_count> incetives_count) {
            this.incetives_count = incetives_count;
        }
        @SerializedName("incetives_count")
        ArrayList < Incetives_count > incetives_count = new ArrayList< Incetives_count >();
        @SerializedName("proceed_status")
        private String proceed_status;
        @SerializedName("error_message")
        private String error_message;


        public class Incetives_count{
            @SerializedName("incetives_type")
            private String incetives_type;
            @SerializedName("target")
            private String target;
            @SerializedName("completed_level")
            private String completed_level;
            @SerializedName("target_km")
            private String target_km;
            @SerializedName("target_distance")
            private String target_distance;
            @SerializedName("incetive_amount")
            private String incetive_amount;
            @SerializedName("currency")
            private String currency;
            @SerializedName("incentive_period")
            private String incentive_period;
            @SerializedName("current_period")
            private String current_period;
            @SerializedName("hours_diff")
            private String hours_diff;
            @SerializedName("completed_hours")
            private String completed_hours;
            @SerializedName("day_time")
            private String day_time;

            @SerializedName("title")
            private String title;

            @SerializedName("completed_title")
            private String completed_title;

            @SerializedName("Incentive_target")
            private String Incentive_target;

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getCompleted_title() {
                return completed_title;
            }

            public void setCompleted_title(String completed_title) {
                this.completed_title = completed_title;
            }

            public String getIncentive_target() {
                return Incentive_target;
            }

            public void setIncentive_target(String incentive_target) {
                Incentive_target = incentive_target;
            }

            public String getTarget_date() {
                return target_date;
            }

            public void setTarget_date(String target_date) {
                this.target_date = target_date;
            }

            @SerializedName("target_date")
            private String target_date;


            // Getter Methods 

            public String getIncetives_type() {
                return incetives_type;
            }

            public String getTarget() {
                return target;
            }

            public String getCompleted_level() {
                return completed_level;
            }

            public String getTarget_km() {
                return target_km;
            }

            public String getTarget_distance() {
                return target_distance;
            }

            public String getIncetive_amount() {
                return incetive_amount;
            }

            public String getCurrency() {
                return currency;
            }

            public String getIncentive_period() {
                return incentive_period;
            }

            public String getCurrent_period() {
                return current_period;
            }

            public String getHours_diff() {
                return hours_diff;
            }

            public String getCompleted_hours() {
                return completed_hours;
            }

            public String getDay_time() {
                return day_time;
            }

            // Setter Methods 

            public void setIncetives_type(String incetives_type) {
                this.incetives_type = incetives_type;
            }

            public void setTarget(String target) {
                this.target = target;
            }

            public void setCompleted_level(String completed_level) {
                this.completed_level = completed_level;
            }

            public void setTarget_km(String target_km) {
                this.target_km = target_km;
            }

            public void setTarget_distance(String target_distance) {
                this.target_distance = target_distance;
            }

            public void setIncetive_amount(String incetive_amount) {
                this.incetive_amount = incetive_amount;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public void setIncentive_period(String incentive_period) {
                this.incentive_period = incentive_period;
            }

            public void setCurrent_period(String current_period) {
                this.current_period = current_period;
            }

            public void setHours_diff(String hours_diff) {
                this.hours_diff = hours_diff;
            }

            public void setCompleted_hours(String completed_hours) {
                this.completed_hours = completed_hours;
            }

            public void setDay_time(String day_time) {
                this.day_time = day_time;
            }
        }
        // Getter Methods

        public String getTotal_amount() {
            return total_amount;
        }

        public String getCurrency() {
            return currency;
        }

        public Month_statement getMonth_statement() {
            return Month_statementObject;
        }

        public String getTotal_payout_amount() {
            return total_payout_amount;
        }

        public String getProceed_status() {
            return proceed_status;
        }

        public String getError_message() {
            return error_message;
        }

        // Setter Methods

        public void setTotal_amount(String total_amount) {
            this.total_amount = total_amount;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public void setMonth_statement(Month_statement month_statementObject) {
            this.Month_statementObject = month_statementObject;
        }

        public void setTotal_payout_amount(String total_payout_amount) {
            this.total_payout_amount = total_payout_amount;
        }

        public void setProceed_status(String proceed_status) {
            this.proceed_status = proceed_status;
        }

        public void setError_message(String error_message) {
            this.error_message = error_message;
        }

        public class Month_statement {
            @SerializedName("total_amount")
            private String total_amount;
            @SerializedName("daily_trip")
            private String daily_trip;
            @SerializedName("schedule_hours")
            private String schedule_hours;
            @SerializedName("week_trip")
            private String week_trip;


            // Getter Methods

            public String getTotal_amount() {
                return total_amount;
            }

            public String getDaily_trip() {
                return daily_trip;
            }

            public String getSchedule_hours() {
                return schedule_hours;
            }

            public String getWeek_trip() {
                return week_trip;
            }

            // Setter Methods

            public void setTotal_amount(String total_amount) {
                this.total_amount = total_amount;
            }

            public void setDaily_trip(String daily_trip) {
                this.daily_trip = daily_trip;
            }

            public void setSchedule_hours(String schedule_hours) {
                this.schedule_hours = schedule_hours;
            }

            public void setWeek_trip(String week_trip) {
                this.week_trip = week_trip;
            }
        }
    }
}


