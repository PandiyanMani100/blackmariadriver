package com.blackmaria.partner.latestpackageview.view.sidemenu.profile;


import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import android.os.Bundle;
import android.os.StrictMode;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityProfileConstrainBinding;
import com.blackmaria.partner.latestpackageview.Factory.Sidemenu.Profile.ProfileFactory;
import com.blackmaria.partner.latestpackageview.Model.profilepojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.vieewmodel.sidemenu.profile.ProfileViewModel;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ProfileConstrain extends AppCompatActivity {

    private ActivityProfileConstrainBinding binding;
    private ProfileViewModel profileViewModel;
    private SessionManager sessionManager;
    private String driverid = "", driverimage = "", drivername = "";
    private profilepojo pojo;
    private JSONObject jsonObj;
    AccessLanguagefromlocaldb db;

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        db = new AccessLanguagefromlocaldb(this);
        binding = DataBindingUtil.setContentView(ProfileConstrain.this, R.layout.activity_profile_constrain);
        profileViewModel = ViewModelProviders.of(this, new ProfileFactory(this)).get(ProfileViewModel.class);
        binding.setProfileViewModel(profileViewModel);

        profileViewModel.setIds(binding);
        profileViewModel.getprofile();
        initView();
        setresponse();

        binding.profiletxt.setText(db.getvalue("profile"));
    }

    private void setresponse() {
        profileViewModel.getGetdriverporfile().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    jsonObj = new JSONObject(response);
                    Type type = new TypeToken<profilepojo>() {
                    }.getType();
                    pojo = new GsonBuilder().create().fromJson(jsonObj.toString(), type);
                    setupViewPager(binding.pager);
                    binding.pager.setEnableSwipe(false);
                    binding.tabLayout.setupWithViewPager(binding.pager);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    private void initView() {
        sessionManager = SessionManager.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        driverid = user.get(SessionManager.KEY_DRIVERID);
        driverimage = user.get(SessionManager.KEY_DRIVER_IMAGE);
        drivername = user.get(SessionManager.KEY_DRIVER_NAME);
        AppUtils.setImageView(ProfileConstrain.this, driverimage, binding.ivUserbg);
        binding.drivername.setText(drivername);

    }

    private void setupViewPager(FCViewPager viewPager) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new Profilefragment(jsonObj), db.getvalue("proff"));
        adapter.addFragment(new Bankcardfragment(jsonObj), db.getvalue("bankcar"));
        adapter.addFragment(new Emergencyfragment(jsonObj),db.getvalue("enetete"));
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
