package com.blackmaria.partner.latestpackageview.view.Dashboard.Tracking;

import android.app.Dialog;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.ComponentCallbacks2;
import android.content.Intent;

import androidx.databinding.DataBindingUtil;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Handler;

import androidx.annotation.Nullable;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.app.DbHelper;
import com.blackmaria.partner.app.WaitingTimeDrop;
import com.blackmaria.partner.databinding.ActivityTrackPageBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.OnlineupdatelocationtoserverService;
import com.blackmaria.partner.latestpackageview.Database.GEODBHelper;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Tracking.TrackpageFactory;
import com.blackmaria.partner.latestpackageview.Model.continuetrippojo;
import com.blackmaria.partner.latestpackageview.Model.endride;
import com.blackmaria.partner.latestpackageview.Model.getaddress;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.fare.Cashpayment;
import com.blackmaria.partner.latestpackageview.view.fare.FareSummaryConstrain;
import com.blackmaria.partner.latestpackageview.view.Dashboard.OnlinepageConstrain;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.tracking.TrackpageViewModel;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.blackmaria.partner.mylibrary.latlnginterpolation.LatLngInterpolator;
import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;

public class TrackPage extends AppCompatActivity implements RoutingListener, ComponentCallbacks2 {

    private ActivityTrackPageBinding binding;
    private TrackpageViewModel trackpageViewModel;
    private SessionManager sessionManager;
    private AppUtils appUtils;
    private continuetrippojo object;
    private String sDriveID = "", driverimage = "", currentlat = "", currentlon = "", droplocaddress = "", hostname = "", mode = "", recordid = "";
    private Handler mapHandler = new Handler();
    private GEODBHelper dbinstance;
    private ArrayList<LatLng> wayPointList;
    private LatLngBounds.Builder wayPointBuilder;
    private GoogleMap googleMap;
    private MapFragment mapFragment;
    private LatLng newLatLng, oldLatLng;
    private LatLngInterpolator mLatLngInterpolator;
    private Location oldLocation, changelatLocations;
    private Marker carMarker;
    private BitmapDescriptor carIcon;
    private LatLng routefromlatlng = null, routetolatlon = null;
    private double myMovingDistance = 0.0;
    private float globalbearing = 0;
    private Dialog sosDialog;
    private Dialog sosNoRecordDialog;


    private Animation layoutanimation;


    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        binding = DataBindingUtil.setContentView(TrackPage.this, R.layout.activity_track_page);
        trackpageViewModel = ViewModelProviders.of(this, new TrackpageFactory(this)).get(TrackpageViewModel.class);
        binding.setTrackpageViewModel(trackpageViewModel);
        trackpageViewModel.setIds(binding);

        initView();
        clicklistener();
    }

    private void clicklistener() {

        SmartLocation.with(this).location()
                .start(new OnLocationUpdatedListener() {
                    @Override
                    public void onLocationUpdated(Location location) {
                        sessionManager.setLatitude(String.valueOf(location.getLatitude()));
                        sessionManager.setLongitude(String.valueOf(location.getLongitude()));
                        onMessageEvent(location);
                    }
                });

        binding.taptotripmode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (object.getResponse().getUser_profile().getMode().toLowerCase().equalsIgnoreCase("oneway")) {
                    appUtils.addressfetch(TrackPage.this, currentlat, currentlon);
                    onewaypkdialog();
                } else if (object.getResponse().getUser_profile().getMode().toLowerCase().equalsIgnoreCase("return")) {
                    if (object.getResponse().getUser_profile().getReturn_mode().toLowerCase().equalsIgnoreCase("drop")) {
                        //returnmode drop
                        retrundroppkdialog(getResources().getString(R.string.doyouwanttodroptrip));
                    } else if (object.getResponse().getUser_profile().getReturn_mode().toLowerCase().equalsIgnoreCase("start")) {
                        //returnmode start
                        retrundroppkdialog(getResources().getString(R.string.doyouwanttostarttrip));
                    } else if (object.getResponse().getUser_profile().getReturn_mode().toLowerCase().equalsIgnoreCase("over")) {
                        appUtils.addressfetch(TrackPage.this, currentlat, currentlon);
                        onewaypkdialog();
                    }
                } else if (object.getResponse().getUser_profile().getMode().toLowerCase().equalsIgnoreCase("multistop")) {
                    for (int i = 0; i <= object.getResponse().getUser_profile().getMultistop_array().size() - 1; i++) {
                        if (object.getResponse().getUser_profile().getMultistop_array().get(i).getMode().toLowerCase().equalsIgnoreCase("start") && object.getResponse().getUser_profile().getMultistop_array().get(i).getIs_end().toLowerCase().equalsIgnoreCase("0")) {
                            multistopdroppkdialog(getResources().getString(R.string.doyouwanttodroptrip), i);
                            break;
                        }
                    }
                    if (object.getResponse().getUser_profile().getIs_multistop_over().toLowerCase().equalsIgnoreCase("1")) {
                        appUtils.addressfetch(TrackPage.this, currentlat, currentlon);
                        onewaypkdialog();
                    }
                }
            }
        });

        binding.minimize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutanimation = AnimationUtils.loadAnimation(TrackPage.this, R.anim.slidegodown);
                binding.trackview.startAnimation(layoutanimation);
                binding.trackview.setVisibility(View.GONE);
                binding.userview.setVisibility(View.GONE);
                layoutanimation = AnimationUtils.loadAnimation(TrackPage.this, R.anim.shake);
                binding.maximizewindow.startAnimation(layoutanimation);
                binding.maximizewindow.setVisibility(View.VISIBLE);
            }
        });

        binding.maximizewindow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutanimation = AnimationUtils.loadAnimation(TrackPage.this, R.anim.slidegoup);
                binding.trackview.startAnimation(layoutanimation);
                binding.maximizewindow.setVisibility(View.GONE);
                binding.trackview.setVisibility(View.VISIBLE);
                binding.userview.setVisibility(View.VISIBLE);
            }
        });

        binding.soscall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sosDialog != null) {
                    sosDialog.dismiss();
                }
                sosDialog = new Dialog(TrackPage.this, R.style.SlideUpDialog);
                sosDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                sosDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                sosDialog.setCancelable(true);
                sosDialog.setContentView(R.layout.sos_popup1);
                final TextView Tv_confirm = sosDialog.findViewById(R.id.txt_confirm);
                final ImageView Iv_close = sosDialog.findViewById(R.id.label_close_imageview);
                Tv_confirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        HashMap<String, String> jsonParams = new HashMap<String, String>();
                        jsonParams.put("driver_id", sDriveID);
                        jsonParams.put("latitude", currentlat);
                        jsonParams.put("longitude", currentlon);
                        trackpageViewModel.sosapihit(jsonParams);
                        trackpageViewModel.getSosresponse().observe(TrackPage.this, new Observer<String>() {
                            @Override
                            public void onChanged(@Nullable String response) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                        appUtils.AlertError(TrackPage.this, getResources().getString(R.string.action_error), jsonObject.getString("response"));
                                    } else {
                                        sosDialog.dismiss();
                                        callassistanceonride(jsonObject.getString("response"));
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                });
                Iv_close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sosDialog.dismiss();
                    }
                });
                sosDialog.show();
            }
        });
    }

    private void retrundroppkdialog(String label) {
        final PkDialog mDialog = new PkDialog(TrackPage.this);
        mDialog.setDialogTitle(getResources().getString(R.string.trip_label_drop));
        mDialog.setDialogMessage(label);
        mDialog.setPositiveButton(getResources().getString(R.string.action_yes), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
                taptoreturndrop();
            }
        });
        mDialog.setNegativeButton(getResources().getString(R.string.action_no), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void multistopdroppkdialog(String label, final int i) {
        final PkDialog mDialog = new PkDialog(TrackPage.this);
        mDialog.setDialogTitle(getResources().getString(R.string.trip_label_drop));
        mDialog.setDialogMessage(label);
        mDialog.setPositiveButton(getResources().getString(R.string.action_yes), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
                taptomultistopdrop(i);
            }
        });
        mDialog.setNegativeButton(getResources().getString(R.string.action_no), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void taptomultistopdrop(final int i) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("ride_id", object.getResponse().getUser_profile().getRide_id());
        jsonParams.put("latitude", currentlat);
        jsonParams.put("longitude", currentlon);
        jsonParams.put("driver_id", sDriveID);
        jsonParams.put("mode", mode);
        jsonParams.put("record_id", recordid);
        trackpageViewModel.dropoff_multistoptrip(jsonParams);
        trackpageViewModel.getMultidropoffresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                        sessionManager.setContinuetripjson(jsonObject.toString());
                        Intent intent = new Intent(TrackPage.this, WaitingTimeDrop.class);
                        intent.putExtra("rideID", object.getResponse().getUser_profile().getRide_id());
                        intent.putExtra("isContinue", false);
                        intent.putExtra("recordId", object.getResponse().getUser_profile().getMultistop_array().get(i).getRecord_id());
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    } else {
                        appUtils.AlertError(TrackPage.this, getResources().getString(R.string.action_error), jsonObject.getString("response"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    private void taptoreturndrop() {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("ride_id", object.getResponse().getUser_profile().getRide_id());
        jsonParams.put("latitude", currentlat);
        jsonParams.put("longitude", currentlon);
        jsonParams.put("driver_id", sDriveID);
        jsonParams.put("mode", object.getResponse().getUser_profile().getReturn_mode());
        trackpageViewModel.dropoff_returntrip(jsonParams);
        trackpageViewModel.getRetundropoffresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                        sessionManager.setContinuetripjson(jsonObject.toString());
                        Intent intent = new Intent(TrackPage.this, WaitingTimeDrop.class);
                        intent.putExtra("rideID", object.getResponse().getUser_profile().getRide_id());
                        intent.putExtra("isContinue", false);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    } else {
                        appUtils.AlertError(TrackPage.this, getResources().getString(R.string.action_error), jsonObject.getString("response"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void onewaypkdialog() {
        final PkDialog mDialog = new PkDialog(TrackPage.this);
        mDialog.setDialogTitle(getResources().getString(R.string.finishontriplabel));
        mDialog.setDialogMessage(getResources().getString(R.string.doyouwanttofinishtrip));
        mDialog.setPositiveButton(getResources().getString(R.string.action_yes), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
                deleteDatabase(DbHelper.DATABASE_NAME);
                taptofinish();
            }
        });
        mDialog.setNegativeButton(getResources().getString(R.string.action_no), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    private void taptofinish() {

        ArrayList<String> travel_history = dbinstance.getDataEndTrip(object.getResponse().getUser_profile().getRide_id());
        ArrayList<LatLng> distance_travelled = dbinstance.getDisEndTrip(object.getResponse().getUser_profile().getRide_id());
        float distancetravelled = appUtils.calculateDistance(distance_travelled);
        StringBuilder builder = new StringBuilder();
        for (String string : travel_history) {
            builder.append(string + ",");
        }

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriveID);
        jsonParams.put("ride_id", object.getResponse().getUser_profile().getRide_id());
        jsonParams.put("drop_lat", currentlat);
        jsonParams.put("drop_lon", currentlon);
        jsonParams.put("drop_loc", droplocaddress);
        jsonParams.put("distance", "");
        jsonParams.put("interrupted", "");
        jsonParams.put("travel_history", builder.toString());
        jsonParams.put("wait_time", "");
        trackpageViewModel.tripendapihit(jsonParams);
        trackpageViewModel.getTripendresponse().observe(TrackPage.this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    dbinstance.cleartable();
                    sessionManager.setisride("");
                    if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                        Type type = new TypeToken<endride>() {
                        }.getType();
                        endride objectendride = new GsonBuilder().create().fromJson(jsonObject.toString(), type);
                        if (objectendride.getResponse().getPayment_type().toLowerCase().equalsIgnoreCase("cash")) {
                            if (objectendride.getResponse().getNeed_payment().toLowerCase().equalsIgnoreCase("yes")) {
                                Intent intent = new Intent(TrackPage.this, Cashpayment.class);
                                intent.putExtra("rideId", object.getResponse().getUser_profile().getRide_id());
                                startActivity(intent);
                                finish();
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                EventBus.getDefault().unregister(this);
                            }
                        } else {
                            Intent intent = new Intent(TrackPage.this, FareSummaryConstrain.class);
                            intent.putExtra("rideId", object.getResponse().getUser_profile().getRide_id());
                            startActivity(intent);
                            finish();
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            EventBus.getDefault().unregister(this);
                        }
                    } else {
                        Intent intent = new Intent(TrackPage.this, FareSummaryConstrain.class);
                        intent.putExtra("rideId", object.getResponse().getUser_profile().getRide_id());
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        EventBus.getDefault().unregister(this);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

//        }
    }

    public void callassistanceonride(String number) {
        if(sosNoRecordDialog!=null){
            sosNoRecordDialog.dismiss();
        }
        sosNoRecordDialog = new Dialog(TrackPage.this, R.style.SlideUpDialog);
        sosNoRecordDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        sosNoRecordDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        sosNoRecordDialog.setCancelable(true);
        sosNoRecordDialog.setContentView(R.layout.sos_popup2);
        final TextView Tv_mobileNumber = (TextView) sosNoRecordDialog.findViewById(R.id.tv_callassist);
        final ImageView Iv_close = (ImageView) sosNoRecordDialog.findViewById(R.id.img_close);
        final ImageView Iv_call = (ImageView) sosNoRecordDialog.findViewById(R.id.call_assistance);
        final TextView emergn_tv = (TextView) sosNoRecordDialog.findViewById(R.id.lemergency_textview);
        emergn_tv.setText(number);
        Tv_mobileNumber.setText(sessionManager.getContactNumber());
        Iv_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sosNoRecordDialog.dismiss();
                appUtils.call(TrackPage.this, sessionManager.getContactNumber());
            }
        });
        Tv_mobileNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sosNoRecordDialog.dismiss();
                appUtils.call(TrackPage.this, sessionManager.getContactNumber());
            }
        });
        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sosNoRecordDialog.dismiss();
            }
        });
        sosNoRecordDialog.show();
    }

    private void initView() {
        sessionManager = SessionManager.getInstance(this);
        appUtils = AppUtils.getInstance(this);
        dbinstance = GEODBHelper.getInstance(TrackPage.this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        HashMap<String, String> domain = sessionManager.getXmpp();
        hostname = domain.get(SessionManager.KEY_HOST_NAME);
        sDriveID = user.get(SessionManager.KEY_DRIVERID);
        driverimage = user.get(SessionManager.KEY_DRIVER_IMAGE);
        carIcon = (BitmapDescriptorFactory.fromResource(R.drawable.carmove));
        if (currentlat.equalsIgnoreCase("")) {
            currentlat = String.valueOf(sessionManager.getLatitude());
            currentlon = String.valueOf(sessionManager.getLongitude());
        }
        initializeMap();
    }

    private void initializeMap() {
        if (googleMap == null) {
            mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.onlinemap);
            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap Map) {
                    googleMap = Map;
                    googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getApplicationContext(), R.raw.style));
                    googleMap.getUiSettings().setRotateGesturesEnabled(true);
                    googleMap.getUiSettings().setCompassEnabled(true);
                    setresponse();
                }
            });
        }
    }


    private void setresponse() {
        try {
            JSONObject jsonObject = new JSONObject(sessionManager.getContinuetripjson());
            Type type = new TypeToken<continuetrippojo>() {
            }.getType();
            object = new GsonBuilder().create().fromJson(jsonObject.toString(), type);
            trackpageViewModel.setjson(object);
            mapRunnable.run();
            setcontinureresponse(object);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setcontinureresponse(continuetrippojo object) {
        if (object.getResponse().getUser_profile().getRide_status().toLowerCase().equalsIgnoreCase("cancelled")) {
            final PkDialog mDialog = new PkDialog(TrackPage.this);
            mDialog.setDialogTitle(getResources().getString(R.string.tripcancellabel));
            mDialog.setDialogMessage("Trip cancelled already");
            mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sessionManager.setisride("");
                    mDialog.dismiss();
                    startActivity(new Intent(TrackPage.this, OnlinepageConstrain.class));
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    EventBus.getDefault().unregister(this);
                }
            });
            mDialog.show();
        } else {
            if (object.getResponse().getUser_profile().getMode().toLowerCase().equalsIgnoreCase("oneway")) {
                globalbearing = appUtils.findbearing(Double.parseDouble(object.getResponse().getUser_profile().getPickup_lat()), Double.parseDouble(object.getResponse().getUser_profile().getPickup_lon()), Double.parseDouble(object.getResponse().getUser_profile().getDrop_lat()), Double.parseDouble(object.getResponse().getUser_profile().getDrop_lon()));
                routefromlatlng = new LatLng(Double.parseDouble(object.getResponse().getUser_profile().getPickup_lat()), Double.parseDouble(object.getResponse().getUser_profile().getPickup_lon()));
                routetolatlon = new LatLng(Double.parseDouble(object.getResponse().getUser_profile().getDrop_lat()), Double.parseDouble(object.getResponse().getUser_profile().getDrop_lon()));
                binding.droplocationvalue.setText(object.getResponse().getUser_profile().getDrop_loc());
                drawroute(routefromlatlng, routetolatlon);
                AppUtils.setImageView(this, object.getResponse().getUser_profile().getUser_image(), binding.trackusericonnormalbooking);
                binding.etaminutes.setText(object.getResponse().getUser_profile().getEta_arrival());
                binding.trackusername.setText(object.getResponse().getUser_profile().getUser_name());
                binding.paidtype.setText("PAID BY " + object.getResponse().getUser_profile().getPayment_type());
            } else if (object.getResponse().getUser_profile().getMode().toLowerCase().equalsIgnoreCase("return")) {
                if (object.getResponse().getUser_profile().getReturn_mode().toLowerCase().equalsIgnoreCase("drop")) {
                    globalbearing = appUtils.findbearing(Double.parseDouble(object.getResponse().getUser_profile().getPickup_lat()), Double.parseDouble(object.getResponse().getUser_profile().getPickup_lon()), Double.parseDouble(object.getResponse().getUser_profile().getDrop_lat()), Double.parseDouble(object.getResponse().getUser_profile().getDrop_lon()));
                    routefromlatlng = new LatLng(Double.parseDouble(object.getResponse().getUser_profile().getPickup_lat()), Double.parseDouble(object.getResponse().getUser_profile().getPickup_lon()));
                    routetolatlon = new LatLng(Double.parseDouble(object.getResponse().getUser_profile().getDrop_lat()), Double.parseDouble(object.getResponse().getUser_profile().getDrop_lon()));
                    binding.droplocationvalue.setText(object.getResponse().getUser_profile().getDrop_loc());
                } else if (object.getResponse().getUser_profile().getReturn_mode().toLowerCase().equalsIgnoreCase("start")) {
                    startActivity(new Intent(TrackPage.this, StartTrip.class));
                    finish();
                    EventBus.getDefault().unregister(this);
                } else if (object.getResponse().getUser_profile().getReturn_mode().toLowerCase().equalsIgnoreCase("over")) {
                    globalbearing = appUtils.findbearing(Double.parseDouble(object.getResponse().getUser_profile().getDrop_lat()), Double.parseDouble(object.getResponse().getUser_profile().getDrop_lon()), Double.parseDouble(object.getResponse().getUser_profile().getPickup_lat()), Double.parseDouble(object.getResponse().getUser_profile().getPickup_lon()));
                    routetolatlon = new LatLng(Double.parseDouble(object.getResponse().getUser_profile().getPickup_lat()), Double.parseDouble(object.getResponse().getUser_profile().getPickup_lon()));
                    routefromlatlng = new LatLng(Double.parseDouble(object.getResponse().getUser_profile().getDrop_lat()), Double.parseDouble(object.getResponse().getUser_profile().getDrop_lon()));
                    binding.droplocationvalue.setText(object.getResponse().getUser_profile().getPickup_location());
                }
                drawroute(routefromlatlng, routetolatlon);
                AppUtils.setImageView(this, object.getResponse().getUser_profile().getUser_image(), binding.trackusericonnormalbooking);
                binding.etaminutes.setText(object.getResponse().getUser_profile().getEta_arrival());
                binding.trackusername.setText(object.getResponse().getUser_profile().getUser_name());
                binding.paidtype.setText("PAID BY " + object.getResponse().getUser_profile().getPayment_type());

            } else if (object.getResponse().getUser_profile().getMode().toLowerCase().equalsIgnoreCase("multistop")) {

                for (int i = 0; i <= object.getResponse().getUser_profile().getMultistop_array().size() - 1; i++) {
                    if (object.getResponse().getUser_profile().getMultistop_array().get(i).getMode().toLowerCase().equalsIgnoreCase("start") && object.getResponse().getUser_profile().getMultistop_array().get(i).getIs_end().toLowerCase().equalsIgnoreCase("0")) {
                        mode = object.getResponse().getUser_profile().getMultistop_array().get(i).getMode();
                        recordid = object.getResponse().getUser_profile().getMultistop_array().get(i).getRecord_id();
                        globalbearing = appUtils.findbearing(Double.parseDouble(currentlat), Double.parseDouble(currentlon), Double.parseDouble(object.getResponse().getUser_profile().getMultistop_array().get(i).getLatlong().getLat()), Double.parseDouble(object.getResponse().getUser_profile().getMultistop_array().get(i).getLatlong().getLon()));
                        routefromlatlng = new LatLng(Double.parseDouble(currentlat), Double.parseDouble(currentlon));
                        routetolatlon = new LatLng(Double.parseDouble(object.getResponse().getUser_profile().getMultistop_array().get(i).getLatlong().getLat()), Double.parseDouble(object.getResponse().getUser_profile().getMultistop_array().get(i).getLatlong().getLon()));
                        binding.droplocationvalue.setText(object.getResponse().getUser_profile().getMultistop_array().get(i).getLocation());
                        drawroute(routefromlatlng, routetolatlon);
                        AppUtils.setImageView(this, object.getResponse().getUser_profile().getUser_image(), binding.trackusericonnormalbooking);
                        binding.etaminutes.setText(object.getResponse().getUser_profile().getEta_arrival());
                        binding.trackusername.setText(object.getResponse().getUser_profile().getUser_name());
                        binding.paidtype.setText("PAID BY " + object.getResponse().getUser_profile().getPayment_type());

                        break;
                    } else if (object.getResponse().getUser_profile().getMultistop_array().get(i).getMode().toLowerCase().equalsIgnoreCase("end") && object.getResponse().getUser_profile().getMultistop_array().get(i).getIs_end().toLowerCase().equalsIgnoreCase("0")) {
                        startActivity(new Intent(TrackPage.this, StartTrip.class));
                        finish();
                        EventBus.getDefault().unregister(this);
                        break;
                    }
                }

                if (object.getResponse().getUser_profile().getIs_multistop_over().toLowerCase().equalsIgnoreCase("1")) {
                    globalbearing = appUtils.findbearing(Double.parseDouble(object.getResponse().getUser_profile().getPickup_lat()), Double.parseDouble(object.getResponse().getUser_profile().getPickup_lon()), Double.parseDouble(object.getResponse().getUser_profile().getDrop_lat()), Double.parseDouble(object.getResponse().getUser_profile().getDrop_lon()));
                    routefromlatlng = new LatLng(Double.parseDouble(currentlat), Double.parseDouble(currentlon));
                    routetolatlon = new LatLng(Double.parseDouble(object.getResponse().getUser_profile().getDrop_lat()), Double.parseDouble(object.getResponse().getUser_profile().getDrop_lon()));
                    binding.droplocationvalue.setText(object.getResponse().getUser_profile().getDrop_loc());
                    drawroute(routefromlatlng, routetolatlon);
                    AppUtils.setImageView(this, object.getResponse().getUser_profile().getUser_image(), binding.trackusericonnormalbooking);
                    binding.etaminutes.setText(object.getResponse().getUser_profile().getEta_arrival());
                    binding.trackusername.setText(object.getResponse().getUser_profile().getUser_name());
                }
            }
            //waiting time
            if (object.getResponse().getUser_profile().getMode().toLowerCase().equalsIgnoreCase("return")) {
                if (object.getResponse().getUser_profile().getReturn_mode().toLowerCase().equalsIgnoreCase("start") && object.getResponse().getUser_profile().getIs_return_over().toLowerCase().equalsIgnoreCase("0") && object.getResponse().getUser_profile().getClosing_status().toLowerCase().equalsIgnoreCase("open")) {
                    Intent intent = new Intent(TrackPage.this, WaitingTimeDrop.class);
                    intent.putExtra("rideID", object.getResponse().getUser_profile().getRide_id());
                    intent.putExtra("isContinue", true);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            }
        }
    }

    public void onMessageEvent(final Location myLocation) {
        try {
            currentlat = String.valueOf(myLocation.getLatitude());
            currentlon = String.valueOf(myLocation.getLongitude());
            if (currentlat.equalsIgnoreCase("")) {
                currentlat = String.valueOf(sessionManager.getLatitude());
                currentlon = String.valueOf(sessionManager.getLongitude());
            }
            appUtils.insertdriverlatlononride(dbinstance, currentlat, currentlon, object.getResponse().getUser_profile().getRide_id());
            final LatLng latLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
            if (oldLatLng == null) {
                oldLatLng = latLng;
            }
            newLatLng = latLng;
            if (mLatLngInterpolator == null) {
                mLatLngInterpolator = new LatLngInterpolator.Linear();
            }
            oldLocation = new Location("");
            oldLocation.setLatitude(oldLatLng.latitude);
            oldLocation.setLongitude(oldLatLng.longitude);
            final LatLng changelat = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
            changelatLocations = new Location("");
            changelatLocations.setLatitude(changelat.latitude);
            changelatLocations.setLongitude(changelat.longitude);
            final float bearingValue = oldLocation.bearingTo(changelatLocations);
            myMovingDistance = oldLocation.distanceTo(changelatLocations);
            if (myMovingDistance > 40) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (googleMap != null) {
                            if (carMarker != null) {
                                appUtils.movemarker(myLocation, bearingValue, carMarker, googleMap, latLng, mLatLngInterpolator);
                                if (object.getResponse().getUser_profile().getRide_status().toLowerCase().equalsIgnoreCase("onride")) {
                                    appUtils.sendloctouseronride(TrackPage.this, hostname, object.getResponse().getUser_profile().getUser_id(), currentlat, currentlon, bearingValue, object.getResponse().getUser_profile().getRide_id());
                                }
                            } else {
                                carMarker = appUtils.Addmarker(googleMap, latLng, myLocation, carIcon);
                            }
                        }
                        oldLatLng = newLatLng;
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Update location
    private Runnable mapRunnable = new Runnable() {
        @Override
        public void run() {
            HashMap<String, String> user = sessionManager.getUserDetails();
            sDriveID = user.get(SessionManager.KEY_DRIVERID);
            HashMap<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("driver_id", sDriveID);
            jsonParams.put("latitude", currentlat);
            jsonParams.put("longitude", currentlon);
            Intent intents = new Intent(TrackPage.this, OnlineupdatelocationtoserverService.class);
            intents.putExtra("params", jsonParams);
            intents.putExtra("url", Iconstant.updateDriverLocation);
            startService(intents);
            mapHandler.postDelayed(this, 45000);
        }
    };

    @Override
    public void onBackPressed() {
    }

    private void drawroute(LatLng routefromlatlng, LatLng routetolatlon) {
        addWayPointPoint(routefromlatlng, routetolatlon, object.getResponse().getUser_profile().getMultistop_array());
    }


    private void addWayPointPoint(LatLng start, LatLng end, ArrayList<continuetrippojo.Response.User_profile.Multistop_array> mMultipleDropLatLng) {
        if (googleMap != null) {
            googleMap.clear();
            wayPointList = new ArrayList<LatLng>();
            wayPointList.add(start);
//            if (object.getResponse().getUser_profile().getMode().equalsIgnoreCase("multistop")) {
//                if (mMultipleDropLatLng != null) {
//                    for (int i = 0; i < mMultipleDropLatLng.size(); i++) {
//                        String sLat = mMultipleDropLatLng.get(i).getLatlong().getLat();
//                        String sLng = mMultipleDropLatLng.get(i).getLatlong().getLon();
//                        double Dlatitude = Double.parseDouble(sLat);
//                        double Dlongitude = Double.parseDouble(sLng);
//                        MarkerOptions marker = new MarkerOptions().position(new LatLng(Dlatitude, Dlongitude));
//                        marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.red));
//                        googleMap.addMarker(marker);
//                    }
//                }
//            }
            wayPointList.add(end);
            Routing routing = new Routing.Builder()
                    .travelMode(AbstractRouting.TravelMode.DRIVING)
                    .withListener(TrackPage.this)
                    .alternativeRoutes(true)
                    .key(Iconstant.place_search_key)
                    .waypoints(wayPointList)
                    .build();
            routing.execute();
        }
    }

    @Override
    public void onRoutingFailure(RouteException e) {
        appUtils.AlertError(this, getResources().getString(R.string.action_error), e.getMessage());
    }

    @Override
    public void onRoutingStart() {
    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int i) {
        try {
            List<Polyline> polyLines = new ArrayList<>();
            PolylineOptions polyOptions = new PolylineOptions();
            polyOptions.color(getResources().getColor(R.color.btn_green_color));
            polyOptions.width(8);
            polyOptions.addAll(route.get(0).getPoints());
            Polyline polyline = googleMap.addPolyline(polyOptions);
            polyLines.add(polyline);
            wayPointBuilder = new LatLngBounds.Builder();
            for (int k = 0; k <= route.get(0).getPoints().size() - 1; k++) {
                wayPointBuilder.include(route.get(0).getPoints().get(k));
            }
//            if (object.getResponse().getUser_profile().getRide_status().toLowerCase().equalsIgnoreCase("end")) {
//                if (object.getResponse().getUser_profile().getMode().toLowerCase().equalsIgnoreCase("oneway")) {
//                    googleMap.addMarker(new MarkerOptions()
//                            .position(pickup_latlong)
//                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.green)));
//                    googleMap.addMarker(new MarkerOptions()
//                            .position(drop_latlong)
//                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.red)));
//                } else if (object.getResponse().getUser_profile().getMode().toLowerCase().equalsIgnoreCase("return")) {
//                    if (isReturnAvailable.equalsIgnoreCase("1")) {
//                        googleMap.addMarker(new MarkerOptions()
//                                .position(return_pickUp_latLong)
//                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.green)));
//                        googleMap.addMarker(new MarkerOptions()
//                                .position(return_latLong)
//                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.red)));
//                    } else {
//                        googleMap.addMarker(new MarkerOptions()
//                                .position(pickup_latlong)
//                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.green)));
//                        googleMap.addMarker(new MarkerOptions()
//                                .position(drop_latlong)
//                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.red)));
//                    }
//                } else if (object.getResponse().getUser_profile().getMode().toLowerCase().equalsIgnoreCase("multistop")) {
//                    googleMap.addMarker(new MarkerOptions()
//                            .position(pickup_latlong)
//                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.green)));
//                    googleMap.addMarker(new MarkerOptions()
//                            .position(drop_latlong)
//                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.red)));
//                }
//            } else if (object.getResponse().getUser_profile().getRide_status().toLowerCase().equals("arrived")) {
//                googleMap.addMarker(new MarkerOptions()
//                        .position(endWayPoint)
//                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.green)));
//            } else {
//                googleMap.addMarker(new MarkerOptions()
//                        .position(pickup_latlong)
//                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.green)));
//                googleMap.addMarker(new MarkerOptions()
//                        .position(endWayPoint)
//                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.red)));
//            }

            if (object.getResponse().getUser_profile().getMode().toLowerCase().equalsIgnoreCase("oneway")) {
                googleMap.addMarker(new MarkerOptions()
                        .position(routefromlatlng)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.green)));
                googleMap.addMarker(new MarkerOptions()
                        .position(routetolatlon)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.red)));
                binding.ontripmode.setText(getResources().getString(R.string.finishontrip));
            } else if (object.getResponse().getUser_profile().getMode().toLowerCase().equalsIgnoreCase("return")) {
                googleMap.addMarker(new MarkerOptions()
                        .position(routefromlatlng)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.green)));
                googleMap.addMarker(new MarkerOptions()
                        .position(routetolatlon)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.red)));
                if (object.getResponse().getUser_profile().getReturn_mode().toLowerCase().equalsIgnoreCase("over")) {
                    binding.ontripmode.setText("finish trip");
                } else {
                    binding.ontripmode.setText(object.getResponse().getUser_profile().getReturn_mode());
                }
            } else if (object.getResponse().getUser_profile().getMode().toLowerCase().equalsIgnoreCase("multistop")) {
                googleMap.addMarker(new MarkerOptions()
                        .position(routefromlatlng)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.green)));
                googleMap.addMarker(new MarkerOptions()
                        .position(routetolatlon)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.red)));

                if (object.getResponse().getUser_profile().getIs_multistop_over().toLowerCase().equalsIgnoreCase("1")) {
                    binding.ontripmode.setText(getResources().getString(R.string.finishtriplabel));
                } else {
                    binding.ontripmode.setText(getResources().getString(R.string.trip_label_drop));
                }
            }


            if (wayPointBuilder != null) {
                LatLngBounds bounds = wayPointBuilder.build();
                int padding, paddingH, paddingW;
                final View mapview = mapFragment.getView();
                float maxX = mapview.getMeasuredWidth();
                float maxY = mapview.getMeasuredHeight();

                DisplayMetrics displayMetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                int height = displayMetrics.heightPixels;
                int width = displayMetrics.widthPixels;
                float percentageH = 50.0f;
                float percentageW = 80.0f;
                paddingH = (int) (maxY * (percentageH / 100.0f));
                paddingW = (int) (maxX * (percentageW / 100.0f));
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, paddingW, paddingH, 100);
                googleMap.animateCamera(cu, new GoogleMap.CancelableCallback() {
                    @Override
                    public void onFinish() {
                        if (globalbearing <= -35 && globalbearing >= -135) {
                            appUtils.updateCameraBearing(googleMap, -90);
                        } else if (globalbearing >= 35 && globalbearing <= 135) {
                            appUtils.updateCameraBearing(googleMap, -90);
                        }
                    }

                    @Override
                    public void onCancel() {
                    }
                });
            }
        } catch (NullPointerException ee) {
        } catch (Exception e) {
        }
    }

    @Override
    public void onRoutingCancelled() {
        appUtils.AlertError(this, getResources().getString(R.string.action_error), "Route draw cancelled");
    }

    @Subscribe
    public void ondroplocationaddress(getaddress obj) {
        droplocaddress = obj.getAddress();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mapRunnable != null) {
            mapHandler.removeCallbacks(mapRunnable);
        }
        SmartLocation.with(this).location().stop();
        EventBus.getDefault().unregister(this);
        stopService(new Intent(TrackPage.this, OnlineupdatelocationtoserverService.class));
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Log.v("TRIMMEMORY", " " + level);
    }

}
