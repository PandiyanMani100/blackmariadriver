package com.blackmaria.partner.latestpackageview.widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by user127 on 30-06-2017.
 */

@SuppressLint("AppCompatCustomView")
public class TextRCRegular extends TextView {

    public TextRCRegular(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public TextRCRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextRCRegular(Context context) {
        super(context);
        init();
    }


    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/RobotoCondensed-Regular.ttf");
        setTypeface(tf);
    }
}

