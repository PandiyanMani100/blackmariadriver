package com.blackmaria.partner.latestpackageview.view.Dashboard.Mytrips;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.blackmaria.partner.R;
import com.blackmaria.partner.databinding.ActivityWeektripviewConstrainBinding;
import com.blackmaria.partner.latestpackageview.Database.RidesDB;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Mytrips.WeektripviewFactory;
import com.blackmaria.partner.latestpackageview.Model.Week;
import com.blackmaria.partner.latestpackageview.Model.weekviewdetailpojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.mytrips.WeektripViewModel;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class WeektripviewConstrain extends AppCompatActivity {

    private ActivityWeektripviewConstrainBinding binding;
    private WeektripViewModel weektripViewModel;
    private RidesDB ridesDBobj;
    private ArrayList<Week> weeklist;
    private AppUtils appUtils;

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(WeektripviewConstrain.this, R.layout.activity_weektripview_constrain);
        weektripViewModel = ViewModelProviders.of(this, new WeektripviewFactory(this)).get(WeektripViewModel.class);
        binding.setWeektripViewModel(weektripViewModel);
        weektripViewModel.setIds(binding);
        appUtils = new AppUtils(WeektripviewConstrain.this);
        ridesDBobj = new RidesDB(WeektripviewConstrain.this);
        weeklist = new ArrayList<>();
        String startdate = String.valueOf(appUtils.getdatetotiestampwithtime(getIntent().getStringExtra("fromdate") + " 00:00:00"));
        String enddate = String.valueOf(appUtils.getdatetotiestampwithtime(getIntent().getStringExtra("todate") + " 24:00:00"));
        weeklist = ridesDBobj.getmytripsdayofweekwiselist(startdate, enddate);
        if (weeklist != null) {
            if (weeklist.size() > 0) {
                weektripViewModel.weekwiseapihit(getIntent().getStringExtra("fromdate"), getIntent().getStringExtra("todate"), false);
                long startdateofmonth = appUtils.getdatetotiestamp(getIntent().getStringExtra("fromdate"));
                setText(ridesDBobj.getmonthinstring(String.valueOf(startdateofmonth)), weeklist);
            } else {
                weektripViewModel.weekwiseapihit(getIntent().getStringExtra("fromdate"), getIntent().getStringExtra("todate"), true);
            }
        } else {
            weektripViewModel.weekwiseapihit(getIntent().getStringExtra("fromdate"), getIntent().getStringExtra("todate"), true);
        }


        responselistener();
    }

    private void responselistener() {
        weektripViewModel.getWeekwiseresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Type type = new TypeToken<weekviewdetailpojo>() {
                    }.getType();
                    weekviewdetailpojo object = new GsonBuilder().create().fromJson(response.toString(), type);
                    updatelocaldb(object.getResponse().getWeek());
                    setText(object.getResponse().getDate_desc(), object.getResponse().getWeek());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void updatelocaldb(final ArrayList<Week> object) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i <= object.size() - 1; i++) {
                    long firstdateofmonth = appUtils.getdatetotiestampwithtime(object.get(i).getDate_f() + " 00:00:00");
                    long lastdateofmonth = appUtils.getdatetotiestampwithtime(object.get(i).getDate_f() + " 24:00:00");

                    ridesDBobj.insertmytripsweektoday(String.valueOf(firstdateofmonth), String.valueOf(lastdateofmonth),
                            object.get(i).getDay(), object.get(i).getRidecount(), object.get(i).getDistance(), object.get(i).getDriver_earning()
                            , object.get(i).getDate_f());
                }
            }
        };
        thread.start();
    }

    private void setText(String data, final ArrayList<Week> object) {
        binding.weektxt.setText(data);
        try {
            binding.linearviewtrips.removeAllViews();
        } catch (NullPointerException e) {
        }
        for (int i = 0; i <= object.size() - 1; i++) {
            View view = getLayoutInflater().inflate(R.layout.weekviewdetailadapter, null);
            LinearLayout childview = view.findViewById(R.id.childview);
            TextView day = view.findViewById(R.id.day);
            TextView trips = view.findViewById(R.id.trips);
            TextView kilometer = view.findViewById(R.id.kilometer);
            day.setText(object.get(i).getDay());
            if (object.get(i).getRidecount().equalsIgnoreCase("-") || object.get(i).getRidecount().equalsIgnoreCase("")) {
                trips.setText("0 TRIPS");
            } else {
                trips.setText(object.get(i).getRidecount() + " TRIPS");
            }
            if (object.get(i).getDistance().equals("-")) {
                kilometer.setText("-");
            } else {
                kilometer.setText(object.get(i).getDistance());
            }

            if ((i & 1) == 0) {
                childview.setBackgroundColor(getResources().getColor(R.color.white));
            } else {
                childview.setBackgroundColor(getResources().getColor(R.color.saveplus_bottombg));
            }

            final int finalI = i;
            childview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent today = new Intent(WeektripviewConstrain.this, TodaytripsViewDetailConstrain.class);
                    today.putExtra("date", object.get(finalI).getDate_f());
                    startActivity(today);
                }
            });

            binding.linearviewtrips.addView(view);
        }
    }
}
