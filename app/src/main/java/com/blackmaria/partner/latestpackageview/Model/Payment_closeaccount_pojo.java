package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Payment_closeaccount_pojo implements Serializable {

    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }

    public class Response {
        public ArrayList<Payment> getPayment() {
            return payment;
        }

        public void setPayment(ArrayList<Payment> payment) {
            this.payment = payment;
        }
        @SerializedName("payment")
        ArrayList < Payment > payment = new ArrayList< Payment >();

        public class Payment{
            @SerializedName("name")
            private String name;
            @SerializedName("code")
            private String code;
            @SerializedName("icon")
            private String icon;
            @SerializedName("inactive_icon")
            private String inactive_icon;


            // Getter Methods

            public String getName() {
                return name;
            }

            public String getCode() {
                return code;
            }

            public String getIcon() {
                return icon;
            }

            public String getInactive_icon() {
                return inactive_icon;
            }

            // Setter Methods

            public void setName(String name) {
                this.name = name;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public void setIcon(String icon) {
                this.icon = icon;
            }

            public void setInactive_icon(String inactive_icon) {
                this.inactive_icon = inactive_icon;
            }
        }

    }
}


