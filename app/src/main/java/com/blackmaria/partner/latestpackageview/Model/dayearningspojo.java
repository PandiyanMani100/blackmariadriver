package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class dayearningspojo implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }

    public class Response {
        @SerializedName("current_page")
        private String current_page;
        @SerializedName("next_page")
        private String next_page;
        @SerializedName("perPage")
        private String perPage;

        public ArrayList<Rides> getRides() {
            return rides;
        }

        public void setRides(ArrayList<Rides> rides) {
            this.rides = rides;
        }

        @SerializedName("rides")
        ArrayList<Rides> rides = new ArrayList<Rides>();
        @SerializedName("total_rides")
        private String total_rides;
        @SerializedName("filter_date")
        private String filter_date;


        public class Rides {
            @SerializedName("ride_id")
            private String ride_id;
            @SerializedName("ride_status")
            private String ride_status;
            @SerializedName("ride_time")
            private String ride_time;
            @SerializedName("driver_earning")
            private String driver_earning;
            @SerializedName("currency")
            private String currency;
            @SerializedName("group")
            private String group;
            @SerializedName("s_no")
            private String s_no;
            @SerializedName("datetime")
            private String datetime;


            // Getter Methods

            public String getRide_id() {
                return ride_id;
            }

            public String getRide_status() {
                return ride_status;
            }

            public String getRide_time() {
                return ride_time;
            }

            public String getDriver_earning() {
                return driver_earning;
            }

            public String getCurrency() {
                return currency;
            }

            public String getGroup() {
                return group;
            }

            public String getS_no() {
                return s_no;
            }

            public String getDatetime() {
                return datetime;
            }

            // Setter Methods

            public void setRide_id(String ride_id) {
                this.ride_id = ride_id;
            }

            public void setRide_status(String ride_status) {
                this.ride_status = ride_status;
            }

            public void setRide_time(String ride_time) {
                this.ride_time = ride_time;
            }

            public void setDriver_earning(String driver_earning) {
                this.driver_earning = driver_earning;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public void setGroup(String group) {
                this.group = group;
            }

            public void setS_no(String s_no) {
                this.s_no = s_no;
            }

            public void setDatetime(String datetime) {
                this.datetime = datetime;
            }
        }
        // Getter Methods

        public String getCurrent_page() {
            return current_page;
        }

        public String getNext_page() {
            return next_page;
        }

        public String getPerPage() {
            return perPage;
        }

        public String getTotal_rides() {
            return total_rides;
        }

        public String getFilter_date() {
            return filter_date;
        }

        // Setter Methods

        public void setCurrent_page(String current_page) {
            this.current_page = current_page;
        }

        public void setNext_page(String next_page) {
            this.next_page = next_page;
        }

        public void setPerPage(String perPage) {
            this.perPage = perPage;
        }

        public void setTotal_rides(String total_rides) {
            this.total_rides = total_rides;
        }

        public void setFilter_date(String filter_date) {
            this.filter_date = filter_date;
        }
    }
}

