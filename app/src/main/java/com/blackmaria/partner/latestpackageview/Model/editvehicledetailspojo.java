package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class editvehicledetailspojo implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }

    public class Response {
        @SerializedName("vehicle_type")
        private String vehicle_type;
        @SerializedName("vehicle_number")
        private String vehicle_number;
        @SerializedName("vehicle_maker")
        private String vehicle_maker;
        @SerializedName("vehicle_model")
        private String vehicle_model;
        @SerializedName("vehicle_model_year")
        private String vehicle_model_year;
        @SerializedName("road_tax_expiry")
        private String road_tax_expiry;
        @SerializedName("licence_expiry")
        private String licence_expiry;
        @SerializedName("insurance_expiry")
        private String insurance_expiry;
        @SerializedName("vehicle_image")
        private String vehicle_image;

        public String getVehicle_image_path() {
            return vehicle_image_path;
        }

        public void setVehicle_image_path(String vehicle_image_path) {
            this.vehicle_image_path = vehicle_image_path;
        }

        @SerializedName("vehicle_image_path")
        private String vehicle_image_path;


        // Getter Methods

        public String getVehicle_type() {
            return vehicle_type;
        }

        public String getVehicle_number() {
            return vehicle_number;
        }

        public String getVehicle_maker() {
            return vehicle_maker;
        }

        public String getVehicle_model() {
            return vehicle_model;
        }

        public String getVehicle_model_year() {
            return vehicle_model_year;
        }

        public String getRoad_tax_expiry() {
            return road_tax_expiry;
        }

        public String getLicence_expiry() {
            return licence_expiry;
        }

        public String getInsurance_expiry() {
            return insurance_expiry;
        }

        public String getVehicle_image() {
            return vehicle_image;
        }

        // Setter Methods

        public void setVehicle_type(String vehicle_type) {
            this.vehicle_type = vehicle_type;
        }

        public void setVehicle_number(String vehicle_number) {
            this.vehicle_number = vehicle_number;
        }

        public void setVehicle_maker(String vehicle_maker) {
            this.vehicle_maker = vehicle_maker;
        }

        public void setVehicle_model(String vehicle_model) {
            this.vehicle_model = vehicle_model;
        }

        public void setVehicle_model_year(String vehicle_model_year) {
            this.vehicle_model_year = vehicle_model_year;
        }

        public void setRoad_tax_expiry(String road_tax_expiry) {
            this.road_tax_expiry = road_tax_expiry;
        }

        public void setLicence_expiry(String licence_expiry) {
            this.licence_expiry = licence_expiry;
        }

        public void setInsurance_expiry(String insurance_expiry) {
            this.insurance_expiry = insurance_expiry;
        }

        public void setVehicle_image(String vehicle_image) {
            this.vehicle_image = vehicle_image;
        }
    }
}

