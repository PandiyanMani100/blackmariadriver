package com.blackmaria.partner.latestpackageview.Factory.Dashboard.Mytrips;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;


import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.mytrips.MytripsViewModel;


public class MytripsFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public MytripsFactory(Activity context) {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new MytripsViewModel(context);
    }
}
