package com.blackmaria.partner.latestpackageview.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.latestpackageview.Model.tripsummarypojo;

import java.util.ArrayList;


public class HomeRateCardFareSummaryAdapter extends BaseAdapter {

    private ArrayList<tripsummarypojo.Response.Details.FareArrL> data;
    private LayoutInflater mInflater;
    private Context context;
    private String currency = "";

    public HomeRateCardFareSummaryAdapter(Context c, ArrayList<tripsummarypojo.Response.Details.FareArrL> d, String currency) {
        context = c;
        mInflater = LayoutInflater.from(context);
        data = d;
        this.currency = currency;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private TextView title, value;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.home_ratecard_display_single, parent, false);
            holder = new ViewHolder();
            holder.title = (TextView) view.findViewById(R.id.pickup);
            holder.value = (TextView) view.findViewById(R.id.pickupvalue);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }
        holder.title.setText(data.get(position).getTitle().toUpperCase());
        holder.value.setText(currency + " " + data.get(position).getValue().toUpperCase());

        return view;
    }
}



