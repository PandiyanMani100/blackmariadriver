package com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.latestpackageview.Adapter.Type_of_registrationadapter;
import com.blackmaria.partner.databinding.ActivityChangepaymentConstrainBinding;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Wallet.ChangepyamentFactory;
import com.blackmaria.partner.latestpackageview.Model.rechargeamountpojo;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Recharge.Bank.RechargeselectbankConstrain;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet.ChangepaymentViewModel;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class ChangepaymentConstrain extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private ActivityChangepaymentConstrainBinding binding;
    private ChangepaymentViewModel changepaymentViewModel;
    private SessionManager sessionManager;
    private rechargeamountpojo pojo;
    private int selectedposition = 0;
    private String insertAmount = "";

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Log.v("TRIMMEMORY", " " + level);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(ChangepaymentConstrain.this, R.layout.activity_changepayment_constrain);
        changepaymentViewModel = ViewModelProviders.of(this, new ChangepyamentFactory(this)).get(ChangepaymentViewModel.class);
        binding.setChangepaymentViewModel(changepaymentViewModel);
        changepaymentViewModel.setIds(binding);

        initView();
    }

    @Override
    public void onClick(View view) {
        if (view == binding.backimg) {
            onBackPressed();
        }
    }

    public void initView() {
        sessionManager = SessionManager.getInstance(this);
        if (getIntent().hasExtra("amount")) {
            insertAmount = getIntent().getStringExtra("amount");
        }
        Type type = new TypeToken<rechargeamountpojo>() {
        }.getType();
        pojo = new GsonBuilder().create().fromJson(sessionManager.getpaymentlist(), type);
        paymentslist(pojo);

        binding.retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pojo.getResponse().getPayment_list().get(selectedposition).getCode().equalsIgnoreCase("xendit-bankpayment")) {
                    Intent i = new Intent(ChangepaymentConstrain.this, RechargeselectbankConstrain.class);
                    i.putExtra("paymentlist", sessionManager.getpaymentlist());
                    i.putExtra("insertAmount", insertAmount);
                    i.putExtra("currency", pojo.getResponse().getCurrency());
                    startActivity(i);
                }
            }
        });
    }

    private void paymentslist(rechargeamountpojo pojo) {
        ArrayList<String> paymentslist = new ArrayList<String>();
        for (int i = 0; i <= pojo.getResponse().getPayment_list().size() - 1; i++) {
            paymentslist.add(pojo.getResponse().getPayment_list().get(i).getName());
        }
        Type_of_registrationadapter customSpinnerbank = new Type_of_registrationadapter(ChangepaymentConstrain.this, paymentslist);
        binding.selectgender.setAdapter(customSpinnerbank);
        binding.selectgender.setOnItemSelectedListener(this);
        binding.creditamount.setText(getIntent().getStringExtra("amounts"));
        binding.errormsg.setText(getIntent().getStringExtra("msg"));
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent == binding.selectgender) {
            selectedposition = position;
            String sSelectedItem = binding.selectgender.getSelectedItem().toString();
            binding.txtspnrgender.setText(sSelectedItem);
        }
    }


    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

}
