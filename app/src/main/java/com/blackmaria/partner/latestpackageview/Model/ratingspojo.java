package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class ratingspojo implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }

    public class Response {
        @SerializedName("driver_profile")
        Driver_profile Driver_profileObject;

        public ArrayList<Reason_review> getReason_review() {
            return reason_review;
        }

        public void setReason_review(ArrayList<Reason_review> reason_review) {
            this.reason_review = reason_review;
        }

        @SerializedName("reason_review")
        ArrayList< Reason_review > reason_review = new ArrayList < Reason_review > ();

        public class Reason_review{
            @SerializedName("title")
            private String title;
            @SerializedName("avg_ratting")
            private String avg_ratting;
            @SerializedName("total_count")
            private String total_count;


            // Getter Methods 

            public String getTitle() {
                return title;
            }

            public String getAvg_ratting() {
                return avg_ratting;
            }

            public String getTotal_count() {
                return total_count;
            }

            // Setter Methods 

            public void setTitle(String title) {
                this.title = title;
            }

            public void setAvg_ratting(String avg_ratting) {
                this.avg_ratting = avg_ratting;
            }

            public void setTotal_count(String total_count) {
                this.total_count = total_count;
            }
        }

        public ArrayList<Last_comment_review> getLast_comment_review() {
            return last_comment_review;
        }

        public void setLast_comment_review(ArrayList<Last_comment_review> last_comment_review) {
            this.last_comment_review = last_comment_review;
        }

        @SerializedName("last_comment_review")
        ArrayList< Last_comment_review> last_comment_review =new ArrayList<Last_comment_review>();


        // Getter Methods

        public Driver_profile getDriver_profile() {
            return Driver_profileObject;
        }


        // Setter Methods

        public void setDriver_profile(Driver_profile driver_profileObject) {
            this.Driver_profileObject = driver_profileObject;
        }


        public class Last_comment_review {
            @SerializedName("driver_name")
            private String driver_name;
            @SerializedName("comments")
            private String comments;
            @SerializedName("image")
            private String image;
            @SerializedName("ride_id")
            private String ride_id;
            @SerializedName("review_date")
            private String review_date;


            // Getter Methods

            public String getDriver_name() {
                return driver_name;
            }

            public String getComments() {
                return comments;
            }

            public String getImage() {
                return image;
            }

            public String getRide_id() {
                return ride_id;
            }

            public String getReview_date() {
                return review_date;
            }

            // Setter Methods

            public void setDriver_name(String driver_name) {
                this.driver_name = driver_name;
            }

            public void setComments(String comments) {
                this.comments = comments;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public void setRide_id(String ride_id) {
                this.ride_id = ride_id;
            }

            public void setReview_date(String review_date) {
                this.review_date = review_date;
            }

        }

        public class Driver_profile {
            @SerializedName("driver_name")
            private String driver_name;
            @SerializedName("email")
            private String email;
            @SerializedName("gender")
            private String gender;
            @SerializedName("driver_image")
            private String driver_image;
            @SerializedName("avg_review")
            private String avg_review;


            // Getter Methods

            public String getDriver_name() {
                return driver_name;
            }

            public String getEmail() {
                return email;
            }

            public String getGender() {
                return gender;
            }

            public String getDriver_image() {
                return driver_image;
            }

            public String getAvg_review() {
                return avg_review;
            }

            // Setter Methods

            public void setDriver_name(String driver_name) {
                this.driver_name = driver_name;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public void setGender(String gender) {
                this.gender = gender;
            }

            public void setDriver_image(String driver_image) {
                this.driver_image = driver_image;
            }

            public void setAvg_review(String avg_review) {
                this.avg_review = avg_review;
            }
        }

    }
}


