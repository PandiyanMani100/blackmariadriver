package com.blackmaria.partner.latestpackageview.view.sidemenu.profile;

import android.annotation.SuppressLint;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.EmergencyviewpagerlayoutBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.Factory.Sidemenu.Profile.EmergencyFactory;
import com.blackmaria.partner.latestpackageview.Model.profilepojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.vieewmodel.sidemenu.profile.EmergencyviewpagerViewModel;
import com.countrycodepicker.CountryPicker;
import com.countrycodepicker.CountryPickerListener;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

@SuppressLint("ValidFragment")
public class Emergencyfragment extends Fragment {

    private EmergencyviewpagerlayoutBinding binding;
    private EmergencyviewpagerViewModel emergencyviewpagerViewModel;
    private JSONObject jsonObj;
    private AppUtils appUtils;
    private profilepojo pojo;
    private String driverid = "", driverimage = "", globalchangedemail = "";
    private SessionManager sessionManager;
    private CountryPicker phonepicker, countrypicker;
    private int hitcount = 0;
    AccessLanguagefromlocaldb db;

    public Emergencyfragment(JSONObject jsonObj) {
        this.jsonObj = jsonObj;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.emergencyviewpagerlayout, container, false);
        View view = binding.getRoot();
        db = new   AccessLanguagefromlocaldb(getActivity());
        emergencyviewpagerViewModel = ViewModelProviders.of(this, new EmergencyFactory(getActivity())).get(EmergencyviewpagerViewModel.class);
        binding.setEmergencyviewpagerViewModel(emergencyviewpagerViewModel);
        emergencyviewpagerViewModel.setIds(binding);
        Type type = new TypeToken<profilepojo>() {
        }.getType();
        pojo = new GsonBuilder().create().fromJson(jsonObj.toString(), type);
        initView();
        clicklistener();
        setResponse();

        binding.myprofiletile.setText(db.getvalue("myemergencydetails"));
        binding.emer.setText(db.getvalue("emergency_contact"));
        binding.editemergencydetails.setText(db.getvalue("addss"));
        binding.sosDialogFormNameEdittext.setHint(db.getvalue("name"));
        binding.sosmyloginDialogFormCountryCodeTextview.setHint(db.getvalue("code"));
        binding.sosDialogFormPhoneEdittext.setHint(db.getvalue("phone"));
        binding.sosDialogFormEmailEdittext.setHint(db.getvalue("email"));
        binding.sosDialogFormRelationEdittext.setHint(db.getvalue("relationship"));
        binding.contactaddress.setText(db.getvalue("emergency_contact_address"));
        binding.sosDialogFormAddressEdittext.setHint(db.getvalue("address"));

        binding.sosDialogFormCityEdittext.setHint(db.getvalue("city_town"));
        binding.sosDialogFormCountryEdittext.setHint(db.getvalue("country"));
        binding.updatesos.setText(db.getvalue("update"));

        return view;
    }

    private void initView() {
        appUtils = AppUtils.getInstance(getActivity());
        sessionManager = SessionManager.getInstance(getActivity());
        HashMap<String, String> user = sessionManager.getUserDetails();
        driverid = user.get(SessionManager.KEY_DRIVERID);
        driverimage = user.get(SessionManager.KEY_DRIVER_IMAGE);
        phonepicker = CountryPicker.newInstance(db.getvalue("select_country_lable"));
        countrypicker = CountryPicker.newInstance(db.getvalue("select_country_lable"));


        try {
            if (jsonObj.getJSONObject("response").getJSONObject("emergency_contact").length() > 0) {
                binding.updatesos.setVisibility(View.INVISIBLE);
                binding.editemergencydetails.setText("EDIT");
                binding.editemergencydetails.setVisibility(View.VISIBLE);
                binding.sosDialogFormNameEdittext.setText(pojo.getResponse().getEmergency_contact().getName());
                binding.sosmyloginDialogFormCountryCodeTextview.setText(pojo.getResponse().getEmergency_contact().getMobile_code());
                binding.sosDialogFormPhoneEdittext.setText(pojo.getResponse().getEmergency_contact().getMobile());
                binding.sosDialogFormEmailEdittext.setText(pojo.getResponse().getEmergency_contact().getEmail());
                binding.sosDialogFormRelationEdittext.setText(pojo.getResponse().getEmergency_contact().getRelationship());
                binding.sosDialogFormAddressEdittext.setText(pojo.getResponse().getEmergency_contact().getHome_address());
                binding.sosDialogFormCityEdittext.setText(pojo.getResponse().getEmergency_contact().getHome_city());
                binding.sosDialogFormCountryEdittext.setText(pojo.getResponse().getEmergency_contact().getHome_country());
                //            setenabled(false);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setenabled(boolean value) {
        binding.sosDialogFormNameEdittext.setEnabled(value);
        binding.sosmyloginDialogFormCountryCodeTextview.setEnabled(value);
        binding.sosDialogFormPhoneEdittext.setEnabled(value);
        binding.sosDialogFormEmailEdittext.setEnabled(value);
        binding.sosDialogFormRelationEdittext.setEnabled(value);
        binding.sosDialogFormAddressEdittext.setEnabled(value);
        binding.sosDialogFormCityEdittext.setEnabled(value);
        binding.sosDialogFormCountryEdittext.setEnabled(value);
        if (!value) {
            binding.editemergencydetails.setText("EDIT");
            binding.editemergencydetails.setVisibility(View.VISIBLE);
            binding.updatesos.setVisibility(View.INVISIBLE);
        }
    }

    private void clicklistener() {

        binding.editemergencydetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.editemergencydetails.setText("UPDATE");
                binding.editemergencydetails.setVisibility(View.INVISIBLE);
                binding.updatesos.setVisibility(View.VISIBLE);
                setenabled(true);
                binding.sosDialogFormNameEdittext.setFocusable(true);
                binding.sosDialogFormNameEdittext.setCursorVisible(true);
            }
        });

        binding.sosmyloginDialogFormCountryCodeTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                phonepicker.show(getActivity().getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });

        binding.sosDialogFormCountryEdittext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countrypicker.show(getActivity().getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });

        phonepicker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {
                phonepicker.dismiss();
                binding.sosmyloginDialogFormCountryCodeTextview.setText(dialCode);
                // close keyboard
                AppUtils.CloseKeyboard(getActivity());
            }
        });

        countrypicker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {
                countrypicker.dismiss();
                binding.sosDialogFormCountryEdittext.setText(name);
                // close keyboard
                AppUtils.CloseKeyboard(getActivity());
            }
        });

        binding.updatesos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validate();
            }
        });
    }

    private void validate() {
        if (binding.sosDialogFormNameEdittext.getText().toString().length() == 0) {
            appUtils.AlertError(getActivity(),db.getvalue("action_error"),db.getvalue("profile_label_alert_username"));
        } else if (binding.sosmyloginDialogFormCountryCodeTextview.getText().toString().length() == 0) {
            appUtils.AlertError(getActivity(), db.getvalue("action_error"), db.getvalue("driver_account_label_alert_countrycode"));
        } else if (!AppUtils.isValidPhoneNumber(binding.sosDialogFormPhoneEdittext.getText().toString())) {
            appUtils.AlertError(getActivity(),  db.getvalue("action_error"), db.getvalue("profile_lable_error_mobile"));
        } else if (binding.sosDialogFormEmailEdittext.getText().toString().trim().length() == 0) {
            appUtils.AlertError(getActivity(),  db.getvalue("action_error"),db.getvalue("profile_label_alert_emailempty"));
        } else if (!AppUtils.isValidEmail(binding.sosDialogFormEmailEdittext.getText().toString().trim().replace(" ", ""))) {
            appUtils.AlertError(getActivity(),  db.getvalue("action_error"),db.getvalue("profile_label_alert_email"));
        } else if (binding.sosDialogFormRelationEdittext.getText().toString().length() == 0) {
            appUtils.AlertError(getActivity(),  db.getvalue("action_error"), db.getvalue("profile_label_alert_relationship"));
        } else if (binding.sosDialogFormAddressEdittext.getText().toString().length() == 0) {
            appUtils.AlertError(getActivity(),  db.getvalue("action_error"), db.getvalue("profile_label_alert_address"));
        } else if (binding.sosDialogFormCityEdittext.getText().toString().length() == 0) {
            appUtils.AlertError(getActivity(),  db.getvalue("action_error"), db.getvalue("profile_label_alert_cityname"));
        } else if (binding.sosDialogFormCountryEdittext.getText().toString().length() == 0) {
            appUtils.AlertError(getActivity(),  db.getvalue("action_error"),db.getvalue("profile_label_alert_selectcountryname"));
        } else {
            HashMap<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("driver_id", driverid);
            jsonParams.put("em_name", binding.sosDialogFormNameEdittext.getText().toString());
            jsonParams.put("em_email", binding.sosDialogFormEmailEdittext.getText().toString());
            jsonParams.put("em_mobile", binding.sosDialogFormPhoneEdittext.getText().toString());
            jsonParams.put("em_mobile_code", binding.sosmyloginDialogFormCountryCodeTextview.getText().toString());
            jsonParams.put("em_relationship", binding.sosDialogFormRelationEdittext.getText().toString());
            jsonParams.put("em_address[address]", binding.sosDialogFormAddressEdittext.getText().toString());
            jsonParams.put("em_address[city]", binding.sosDialogFormCityEdittext.getText().toString());
            jsonParams.put("em_address[country]", binding.sosDialogFormCountryEdittext.getText().toString());
            hitcount = 1;
            emergencyviewpagerViewModel.updateemergencydetails(Iconstant.Profile_emergency_add_Url, jsonParams);
        }
    }

    private void setResponse() {
        emergencyviewpagerViewModel.getUpdateemergencydetailsresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    String Sstatus = object.getString("status");
                    if (Sstatus.equalsIgnoreCase("1")) {
                        String Smessage = object.getString("response");
                        setenabled(false);
                        if (hitcount == 1) {
                            appUtils.AlertSuccess(getActivity(), db.getvalue("action_success"), Smessage);
                            hitcount = 0;
                        }
                    } else {
                        String Smessage = object.getString("response");
                        appUtils.AlertError(getActivity(), db.getvalue("action_error"), Smessage);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
