package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class myincentivespojo  implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }

    public class Response {
        @SerializedName("total_amount")
        private String total_amount;
        @SerializedName("highlight_string")
        private String highlight_string;

        public String getHighlight_string() {
            return highlight_string;
        }

        public void setHighlight_string(String highlight_string) {
            this.highlight_string = highlight_string;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        @SerializedName("date")
        private String date;

        public ArrayList<Incetives> getIncetives() {
            return incetives;
        }

        public void setIncetives(ArrayList<Incetives> incetives) {
            this.incetives = incetives;
        }
        @SerializedName("incetives")
        ArrayList < Incetives > incetives = new ArrayList< Incetives >();
        @SerializedName("currency")
        private String currency;
        @SerializedName("total_count")
        private String total_count;
        @SerializedName("total_payout_amount")
        private String total_payout_amount;

        public class Incetives{
            @SerializedName("name")
            private String name;
            @SerializedName("amount")
            private String amount;

            public String getName() {
                return name;
            }

            public String getAmount() {
                return amount;
            }

            // Setter Methods

            public void setName(String name) {
                this.name = name;
            }

            public void setAmount(String amount) {
                this.amount = amount;
            }
        }

        public class Types_array{

        }

        // Getter Methods

        public String getTotal_amount() {
            return total_amount;
        }

        public String getCurrency() {
            return currency;
        }

        public String getTotal_count() {
            return total_count;
        }

        public String getTotal_payout_amount() {
            return total_payout_amount;
        }

        // Setter Methods

        public void setTotal_amount(String total_amount) {
            this.total_amount = total_amount;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public void setTotal_count(String total_count) {
            this.total_count = total_count;
        }

        public void setTotal_payout_amount(String total_payout_amount) {
            this.total_payout_amount = total_payout_amount;
        }
    }
}

