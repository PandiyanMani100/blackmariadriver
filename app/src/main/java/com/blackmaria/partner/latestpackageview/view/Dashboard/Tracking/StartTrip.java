package com.blackmaria.partner.latestpackageview.view.Dashboard.Tracking;


import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.location.Location;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.app.MessagesActivity;
import com.blackmaria.partner.app.TrackRideCancelTrip;
import com.blackmaria.partner.app.WaitingTimeDrop;
import com.blackmaria.partner.app.WaitingTimePage;
import com.blackmaria.partner.databinding.ActivityStartTripBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.OnlineupdatelocationtoserverService;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Tracking.StartTripFactory;
import com.blackmaria.partner.latestpackageview.Model.continuetrippojo;
import com.blackmaria.partner.latestpackageview.Model.rideidpojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Dashboard_constrain;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.tracking.StartTripViewModel;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.blackmaria.partner.mylibrary.latlnginterpolation.LatLngInterpolator;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;

public class StartTrip extends AppCompatActivity {

    private ActivityStartTripBinding binding;
    private StartTripViewModel startTripViewModel;
    private SessionManager sessionManager;
    private AppUtils appUtils;
    private continuetrippojo object;
    private String sDriveID = "", driverimage = "", currentlat = "", currentlon = "", hostname = "", mode = "", recordid = "", countstring = "";
    private Handler mapHandler = new Handler();
    private LatLng newLatLng, oldLatLng;
    private LatLngInterpolator mLatLngInterpolator;
    private Location oldLocation, changelatLocations;


    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(StartTrip.this, R.layout.activity_start_trip);
        startTripViewModel = ViewModelProviders.of(this, new StartTripFactory(this)).get(StartTripViewModel.class);
        binding.setStartTripViewModel(startTripViewModel);
        startTripViewModel.setIds(binding);

        initView();
        setresponse();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    private void initView() {
//        EventBus.getDefault().register(this);
        sessionManager = SessionManager.getInstance(this);
        appUtils = AppUtils.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        HashMap<String, String> domain = sessionManager.getXmpp();
        hostname = domain.get(SessionManager.KEY_HOST_NAME);
        sDriveID = user.get(SessionManager.KEY_DRIVERID);
        driverimage = user.get(SessionManager.KEY_DRIVER_IMAGE);
        if (currentlat.equalsIgnoreCase("")) {
            currentlat = String.valueOf(sessionManager.getLatitude());
            currentlon = String.valueOf(sessionManager.getLongitude());
        }

    }

    public void clicklistener() {
        SmartLocation.with(this).location()
                .start(new OnLocationUpdatedListener() {
                    @Override
                    public void onLocationUpdated(Location location) {
                        sessionManager.setLatitude(String.valueOf(location.getLatitude()));
                        sessionManager.setLongitude(String.valueOf(location.getLongitude()));
                        onMessageEvent(location);
                    }
                });

        binding.starttrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (object.getResponse().getUser_profile().getMode().toLowerCase().equalsIgnoreCase("return")) {
                    if (object.getResponse().getUser_profile().getReturn_mode().toLowerCase().equalsIgnoreCase("start")) {
                        //returnmode start
                        retrundroppkdialog(getResources().getString(R.string.doyouwanttostarttrip));
                    } else {
                        starttriponly();
                    }
                } else if (object.getResponse().getUser_profile().getMode().toLowerCase().equalsIgnoreCase("multistop")) {
                    if (object.getResponse().getUser_profile().getRide_status().toLowerCase().equalsIgnoreCase("arrived")) {
                        starttriponly();
                    } else if (object.getResponse().getUser_profile().getRide_status().toLowerCase().equalsIgnoreCase("onride")) {
                        for (int i = 0; i <= object.getResponse().getUser_profile().getMultistop_array().size() - 1; i++) {
                            if (object.getResponse().getUser_profile().getMultistop_array().get(i).getMode().toLowerCase().equalsIgnoreCase("end")) {
                                multistoppkdialog(getResources().getString(R.string.doyouwanttostarttrip), i);
                                break;
                            }
                        }
                    }
                } else {
                    starttriponly();
                }
            }
        });

        binding.viewallmultipstop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent passIntent = new Intent(StartTrip.this, Multistopviewall.class);
                passIntent.putExtra("count", countstring);
                startActivity(passIntent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        binding.messageicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), MessagesActivity.class);
                intent.putExtra("name",object.getResponse().getUser_profile().getUser_name());
                intent.putExtra("userimage",object.getResponse().getUser_profile().getUser_image());
                intent.putExtra("rideid",object.getResponse().getUser_profile().getRide_id());
                intent.putExtra("useridtosend",object.getResponse().getUser_profile().getUser_id());
                intent.putExtra("userphonenumber",object.getResponse().getUser_profile().getPhone_number());
                intent.putExtra("ios_fcm_key",object.getResponse().getUser_profile().getNotification_details().getIos_fcm_key());
                intent.putExtra("user_fcm_token",object.getResponse().getUser_profile().getNotification_details().getUser_fcm_token());
                startActivity(intent);
            }
        });

        binding.canceltrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final PkDialog mDialog = new PkDialog(StartTrip.this);
                mDialog.setDialogTitle(getResources().getString(R.string.tripcancellabel));
                mDialog.setDialogMessage(getResources().getString(R.string.doyouwanttocanceltrip));
                mDialog.setPositiveButton(getResources().getString(R.string.action_yes), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.showprogress(true);
                        mDialog.clickenabeled(false);
                        HashMap<String, String> jsonParams = new HashMap<String, String>();
                        jsonParams.put("driver_id", sDriveID);
                        jsonParams.put("ride_id", object.getResponse().getUser_profile().getRide_id());
                        startTripViewModel.canceltripreasons(jsonParams);
                        startTripViewModel.getCancelresons().observe(StartTrip.this, new Observer<String>() {
                            @Override
                            public void onChanged(@Nullable String response) {
                                mDialog.showprogress(false);
                                mDialog.clickenabeled(true);
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    Intent passIntent = new Intent(StartTrip.this, TrackRideCancelTrip.class);
                                    passIntent.putExtra("Reason", jsonObject.toString());
                                    passIntent.putExtra("driverImage", driverimage);
                                    passIntent.putExtra("RideID", object.getResponse().getUser_profile().getRide_id());
                                    startActivity(passIntent);
                                    mDialog.dismiss();
                                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                    }
                });
                mDialog.setNegativeButton(getResources().getString(R.string.action_no), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mDialog.dismiss();
                    }
                });
                mDialog.show();
            }
        });
    }

    private void starttriponly() {
        final PkDialog mDialog = new PkDialog(StartTrip.this);
        mDialog.setDialogTitle(getResources().getString(R.string.starttriplabel));
        mDialog.setDialogMessage(getResources().getString(R.string.doyouwanttostarttrip));
        mDialog.setPositiveButton(getResources().getString(R.string.action_yes), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
                taptostart();
            }
        });
        mDialog.setNegativeButton(getResources().getString(R.string.action_no), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void taptomultistopdrop(int i) {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("ride_id", object.getResponse().getUser_profile().getRide_id());
        jsonParams.put("latitude", currentlat);
        jsonParams.put("longitude", currentlon);
        jsonParams.put("driver_id", sDriveID);
        jsonParams.put("mode", mode);
        jsonParams.put("record_id", recordid);
        startTripViewModel.dropoff_multistoptrip(jsonParams);
        startTripViewModel.getMultidropoffresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                        sessionManager.setContinuetripjson(jsonObject.toString());
                        startActivity(new Intent(StartTrip.this, TrackPage.class));
                        finish();
                        if (EventBus.getDefault().isRegistered(this)) {
                            EventBus.getDefault().unregister(this);
                        }
                    } else {
                        appUtils.AlertError(StartTrip.this, getResources().getString(R.string.action_error), jsonObject.getString("response"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void taptoreturndrop() {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("ride_id", object.getResponse().getUser_profile().getRide_id());
        jsonParams.put("latitude", currentlat);
        jsonParams.put("longitude", currentlon);
        jsonParams.put("driver_id", sDriveID);
        jsonParams.put("mode", object.getResponse().getUser_profile().getReturn_mode());
        startTripViewModel.dropoff_returntrip(jsonParams);
        startTripViewModel.getRetundropoffresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                        sessionManager.setContinuetripjson(jsonObject.toString());
                        startActivity(new Intent(StartTrip.this, TrackPage.class));
                        finish();
                        if (EventBus.getDefault().isRegistered(this)) {
                            EventBus.getDefault().unregister(this);
                        }
                    } else {
                        appUtils.AlertError(StartTrip.this, getResources().getString(R.string.action_error), jsonObject.getString("response"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void multistoppkdialog(String label, final int i) {
        final PkDialog mDialog = new PkDialog(StartTrip.this);
        mDialog.setDialogTitle(getResources().getString(R.string.starttriplabel));
        mDialog.setDialogMessage(label);
        mDialog.setPositiveButton(getResources().getString(R.string.action_yes), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
                taptomultistopdrop(i);
            }
        });
        mDialog.setNegativeButton(getResources().getString(R.string.action_no), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void retrundroppkdialog(String label) {
        final PkDialog mDialog = new PkDialog(StartTrip.this);
        mDialog.setDialogTitle(getResources().getString(R.string.starttriplabel));
        mDialog.setDialogMessage(label);
        mDialog.setPositiveButton(getResources().getString(R.string.action_yes), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
                taptoreturndrop();
            }
        });
        mDialog.setNegativeButton(getResources().getString(R.string.action_no), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    private void taptostart() {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriveID);
        jsonParams.put("ride_id", object.getResponse().getUser_profile().getRide_id());
        jsonParams.put("pickup_lat", currentlat);
        jsonParams.put("pickup_lon", currentlon);
        startTripViewModel.begintripapihit(jsonParams);
        startTripViewModel.getBeginapiresponse().
                observe(StartTrip.this, new Observer<String>() {
                    @Override
                    public void onChanged(@Nullable String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                sessionManager.setContinuetripjson(jsonObject.toString());
                                appUtils.AlertSuccess(StartTrip.this, getResources().getString(R.string.action_success), "Ride Started");
                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        //Do something after 100ms
                                        startActivity(new Intent(StartTrip.this, TrackPage.class));
                                        finish();
                                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                        if (EventBus.getDefault().isRegistered(this)) {
                                            EventBus.getDefault().unregister(this);
                                        }
                                    }
                                }, 1500);
                            } else {
                                final PkDialog mDialog = new PkDialog(StartTrip.this);
                                mDialog.setDialogTitle(getResources().getString(R.string.action_error));
                                mDialog.setDialogMessage(jsonObject.getString("response"));
                                mDialog.setPositiveButton(getResources().getString(R.string.action_yes), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        startActivity(new Intent(StartTrip.this, Dashboard_constrain.class));
                                        finish();
                                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                                        if (EventBus.getDefault().isRegistered(this)) {
                                            EventBus.getDefault().unregister(this);
                                        }
                                    }
                                });
                                mDialog.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    private void setresponse() {
        try {
            JSONObject jsonObject = new JSONObject(sessionManager.getContinuetripjson());
            Type type = new TypeToken<continuetrippojo>() {
            }.getType();
            object = new GsonBuilder().create().fromJson(jsonObject.toString(), type);
            startTripViewModel.setjson(object);
            mapRunnable.run();
            AppUtils.setImageView(StartTrip.this, object.getResponse().getUser_profile().getUser_image(), binding.userimage);
            binding.username.setText(object.getResponse().getUser_profile().getUser_name());
            if (object.getResponse().getUser_profile().getMode().toLowerCase().equals("multistop")) {
                binding.triptype.setText("MULTI STOP TRIP");
            } else {
                binding.triptype.setText(object.getResponse().getUser_profile().getMode() + " TRIP");
            }

            if (object.getResponse().getUser_profile().getTotal_passenger().equalsIgnoreCase("1")) {
                binding.passengercount.setText(object.getResponse().getUser_profile().getTotal_passenger() + " passenger");
            } else {
                binding.passengercount.setText(object.getResponse().getUser_profile().getTotal_passenger() + " passengers");
            }
            binding.estimatedfarevalue.setText(object.getResponse().getUser_profile().getCurrency() + " " + object.getResponse().getUser_profile().getEst_cost());
            binding.paymenttype.setText("BY " + object.getResponse().getUser_profile().getPayment_type());
            clicklistener();
            if (object.getResponse().getUser_profile().getRide_status().toLowerCase().equalsIgnoreCase("onride")) {
                binding.canceltrip.setVisibility(View.INVISIBLE);
            }
            if (object.getResponse().getUser_profile().getMode().toLowerCase().equalsIgnoreCase("return")) {
                binding.pickuplocationvalue.setText(object.getResponse().getUser_profile().getDrop_loc());
                if (object.getResponse().getUser_profile().getReturn_mode().equalsIgnoreCase("start")) {
                    binding.uparrowreturntrip.setVisibility(View.VISIBLE);
                    binding.pickuplabelreturntrip.setVisibility(View.VISIBLE);
                }
                if (object.getResponse().getUser_profile().getReturn_mode().toLowerCase().equalsIgnoreCase("start") && object.getResponse().getUser_profile().getIs_return_over().toLowerCase().equalsIgnoreCase("0") && object.getResponse().getUser_profile().getClosing_status().toLowerCase().equalsIgnoreCase("open")) {
                    Intent intent = new Intent(StartTrip.this, WaitingTimeDrop.class);
                    intent.putExtra("rideID", object.getResponse().getUser_profile().getRide_id());
                    intent.putExtra("isContinue", true);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            } else if (object.getResponse().getUser_profile().getMode().toLowerCase().equalsIgnoreCase("oneway")) {
                binding.pickuplocationvalue.setText(object.getResponse().getUser_profile().getDrop_loc());
            } else if (object.getResponse().getUser_profile().getMode().toLowerCase().equalsIgnoreCase("multistop")) {
                binding.viewallmultipstop.setVisibility(View.VISIBLE);
                for (int i = 0; i <= object.getResponse().getUser_profile().getMultistop_array().size() - 1; i++) {
                    if (object.getResponse().getUser_profile().getMultistop_array().get(i).getMode().toLowerCase().equalsIgnoreCase("start") && object.getResponse().getUser_profile().getMultistop_array().get(i).getIs_end().toLowerCase().equalsIgnoreCase("0")) {
                        binding.pickuplocationvalue.setText(object.getResponse().getUser_profile().getMultistop_array().get(i).getLocation());
                        binding.uparrowreturntrip.setVisibility(View.GONE);
                        binding.pickuplabelreturntrip.setVisibility(View.GONE);
                        binding.multipstopcount.setVisibility(View.VISIBLE);
                        binding.multipstopcount.setText(String.valueOf(i + 1));
                        binding.viewallmultipstop.setVisibility(View.VISIBLE);
                        countstring = String.valueOf(i + 1);
                        break;
                    } else if (object.getResponse().getUser_profile().getMultistop_array().get(i).getMode().toLowerCase().equalsIgnoreCase("end") && object.getResponse().getUser_profile().getMultistop_array().get(i).getIs_end().toLowerCase().equalsIgnoreCase("0") && object.getResponse().getUser_profile().getClosing_status().toLowerCase().equalsIgnoreCase("open")) {
                        Intent intent = new Intent(StartTrip.this, WaitingTimeDrop.class);
                        intent.putExtra("rideID", object.getResponse().getUser_profile().getRide_id());
                        intent.putExtra("isContinue", false);
                        intent.putExtra("recordId", object.getResponse().getUser_profile().getMultistop_array().get(i).getRecord_id());
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        break;
                    } else if (object.getResponse().getUser_profile().getMultistop_array().get(i).getMode().toLowerCase().equalsIgnoreCase("end") && object.getResponse().getUser_profile().getMultistop_array().get(i).getIs_end().toLowerCase().equalsIgnoreCase("0")) {
                        binding.pickuplocationvalue.setText(object.getResponse().getUser_profile().getMultistop_array().get(i).getLocation());
                        binding.uparrowreturntrip.setVisibility(View.GONE);
                        binding.pickuplabelreturntrip.setVisibility(View.GONE);
                        binding.multipstopcount.setVisibility(View.VISIBLE);
                        binding.multipstopcount.setText(String.valueOf(i + 1));
                        binding.viewallmultipstop.setVisibility(View.VISIBLE);
                        countstring = String.valueOf(i + 1);
                        mode = object.getResponse().getUser_profile().getMultistop_array().get(i).getMode();
                        recordid = object.getResponse().getUser_profile().getMultistop_array().get(i).getRecord_id();
                        break;
                    }
                }
                for (int i = 0; i <= object.getResponse().getUser_profile().getMultistop_array().size() - 1; i++) {
                    if (object.getResponse().getUser_profile().getMultistop_array().get(i).getMode().toLowerCase().equalsIgnoreCase("start") && object.getResponse().getUser_profile().getMultistop_array().get(i).getIs_end().toLowerCase().equalsIgnoreCase("0")) {
                        binding.pickuplocationvalue.setText(object.getResponse().getUser_profile().getMultistop_array().get(i).getLocation());
                        binding.uparrowreturntrip.setVisibility(View.GONE);
                        binding.pickuplabelreturntrip.setVisibility(View.GONE);
                        binding.multipstopcount.setVisibility(View.VISIBLE);
                        binding.multipstopcount.setText(String.valueOf(i + 1));
                        binding.viewallmultipstop.setVisibility(View.VISIBLE);
                        countstring = String.valueOf(i + 1);
                        break;
                    } else {
                        binding.multipstopcount.setVisibility(View.GONE);
                        binding.viewallmultipstop.setVisibility(View.GONE);
                        binding.pickuplocationvalue.setText(object.getResponse().getUser_profile().getDrop_loc());
                    }
                }
//                }
            }

            if (object.getResponse().getUser_profile().getComp_status().toLowerCase().equalsIgnoreCase("yes")) {
                if (object.getResponse().getUser_profile().getComp_stage().toLowerCase().equalsIgnoreCase("onprocess")) {
                    Intent intent = new Intent(StartTrip.this, WaitingTimePage.class);
                    intent.putExtra("rideID", object.getResponse().getUser_profile().getRide_id());
                    intent.putExtra("isContinue", true);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void onMessageEvent(Location myLocation) {
        try {
            currentlat = String.valueOf(myLocation.getLatitude());
            currentlon = String.valueOf(myLocation.getLongitude());
            if (currentlat.equalsIgnoreCase("")) {
                currentlat = String.valueOf(sessionManager.getLatitude());
                currentlon = String.valueOf(sessionManager.getLongitude());
            }
            final LatLng latLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
            if (oldLatLng == null) {
                oldLatLng = latLng;
            }
            newLatLng = latLng;
            if (mLatLngInterpolator == null) {
                mLatLngInterpolator = new LatLngInterpolator.Linear();
            }
            oldLocation = new Location("");
            oldLocation.setLatitude(oldLatLng.latitude);
            oldLocation.setLongitude(oldLatLng.longitude);
            final LatLng changelat = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
            changelatLocations = new Location("");
            changelatLocations.setLatitude(changelat.latitude);
            changelatLocations.setLongitude(changelat.longitude);
            final float bearingValue = oldLocation.bearingTo(changelatLocations);

            if (object.getResponse().getUser_profile().getRide_status().toLowerCase().equalsIgnoreCase("onride")) {
                appUtils.sendloctouseronride(StartTrip.this, hostname, object.getResponse().getUser_profile().getUser_id(), currentlat, currentlon, bearingValue, object.getResponse().getUser_profile().getRide_id());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Update location
    private Runnable mapRunnable = new Runnable() {
        @Override
        public void run() {
            HashMap<String, String> user = sessionManager.getUserDetails();
            sDriveID = user.get(SessionManager.KEY_DRIVERID);
            HashMap<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("driver_id", sDriveID);
            jsonParams.put("latitude", currentlat);
            jsonParams.put("longitude", currentlon);
            Intent intents = new Intent(StartTrip.this, OnlineupdatelocationtoserverService.class);
            intents.putExtra("params", jsonParams);
            intents.putExtra("url", Iconstant.updateDriverLocation);
            startService(intents);
            mapHandler.postDelayed(this, 45000);
        }
    };

    @Override
    public void onBackPressed() {
    }

    @Subscribe
    public void waitingtimepageonconfirmedstate(rideidpojo rideid) {
        Intent intents = new Intent(StartTrip.this, WaitingTimePage.class);
        intents.putExtra("rideID", rideid.getKey1());
        intents.putExtra("isContinue", false);
        intents.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intents);
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mapRunnable != null) {
            mapHandler.removeCallbacks(mapRunnable);
        }
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        SmartLocation.with(this).location().stop();
        stopService(new Intent(StartTrip.this, OnlineupdatelocationtoserverService.class));

    }
}
