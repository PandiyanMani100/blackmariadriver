package com.blackmaria.partner.latestpackageview.view.sidemenu;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;

import java.util.HashMap;


public class AboutUsConstrain extends AppCompatActivity implements View.OnClickListener {

    private ImageView Iv_back, Iv_facebook, Iv_twitter, Iv_instagram, callicon;
    private TextView helps;
    private boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private SessionManager sessionManager;
    private ServiceRequest mRequest;
    private String sDriveID = "", basicUserProfileUpdateStatus = "", UserProfileUpdateStatus = "", UserID = "";
    private boolean isDataPresent = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aboutus_constrain);

        initialize();
    }

    private void initialize() {
        sessionManager = new SessionManager(AboutUsConstrain.this);
        cd = new ConnectionDetector(AboutUsConstrain.this);
        isInternetPresent = cd.isConnectingToInternet();
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriveID = user.get(SessionManager.KEY_DRIVERID);

        Iv_back = (ImageView) findViewById(R.id.img_back);
        helps = (TextView) findViewById(R.id.helps);
        Iv_facebook = (ImageView) findViewById(R.id.img_facebook);
        Iv_twitter = (ImageView) findViewById(R.id.img_twitter);
        Iv_instagram = (ImageView) findViewById(R.id.img_instagram);
        callicon = (ImageView) findViewById(R.id.callicon);

        Iv_instagram.setOnClickListener(this);
        callicon.setOnClickListener(this);
        Iv_back.setOnClickListener(this);
        Iv_facebook.setOnClickListener(this);
        Iv_twitter.setOnClickListener(this);
//        helps.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startActivity(new Intent(AboutUsConstrain.this, HelpFaq_subactivity.class));
//            }
//        });
    }

    @Override
    public void onClick(View view) {

        if (cd.isConnectingToInternet()) {

            if (view == Iv_back) {
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            } else if (view == callicon) {
                if (Build.VERSION.SDK_INT >= 23) {
                    // Marshmallow+
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + sessionManager.getCustomerServiceNo()));
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + sessionManager.getCustomerServiceNo()));
                    startActivity(intent);
                }
            } else if (view == Iv_facebook) {
                String url = "http://facebook.com/blackmaria.co";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                //shareFacebookLink("");
            } else if (view == Iv_twitter) {
                String url = "http://twitter.com/blackmaria_inc";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                // shareTwitter("");
            } else if (view == Iv_instagram) {
                String url = "http://instagram.com/blackmaria.inc";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        } else {
            Alert(getResources().getString(R.string.alert_nointernet), getResources().getString(R.string.alert_nointernet_message));
        }

    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(AboutUsConstrain.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
}
