package com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.tracking;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.view.View;
import android.view.Window;

import com.blackmaria.partner.R;
import com.blackmaria.partner.databinding.ActivityStartTripBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Model.continuetrippojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;

import java.util.HashMap;

public class StartTripViewModel extends ViewModel implements ApIServices.completelisner {
    private Activity context;
    private ActivityStartTripBinding binding;
    private Dialog dialog;
    private int count = 0;
    private continuetrippojo object;
    private MutableLiveData<String> cancelresons = new MutableLiveData<>();
    private MutableLiveData<String> beginapiresponse = new MutableLiveData<>();
    private MutableLiveData<String> retundropoffresponse = new MutableLiveData<>();
    private MutableLiveData<String> multidropoffresponse = new MutableLiveData<>();

    public StartTripViewModel(Activity context) {
        this.context = context;
    }

    public void setIds(ActivityStartTripBinding binding) {
        this.binding = binding;
    }

    public MutableLiveData<String> getCancelresons() {
        return cancelresons;
    }

    public MutableLiveData<String> getBeginapiresponse() {
        return beginapiresponse;
    }

    public MutableLiveData<String> getRetundropoffresponse() {
        return retundropoffresponse;
    }

    public MutableLiveData<String> getMultidropoffresponse() {
        return multidropoffresponse;
    }

    public void canceltripreasons(HashMap<String, String> jsonParams) {
        count = 0;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ApIServices apIServices = new ApIServices(context, StartTripViewModel.this);
        apIServices.dooperation(Iconstant.driverCancel_reason_Url, jsonParams);
    }

    public void begintripapihit(HashMap<String, String> jsonParams) {
        count = 1;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ApIServices apIServices = new ApIServices(context, StartTripViewModel.this);
        apIServices.dooperation(Iconstant.driverBegin_Url, jsonParams);
    }

    public void dropoff_returntrip(HashMap<String, String> jsonParams) {
        count = 2;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ApIServices apIServices = new ApIServices(context, StartTripViewModel.this);
        apIServices.dooperation(Iconstant.returnTrip_Url, jsonParams);
    }

    public void dropoff_multistoptrip(HashMap<String, String> jsonParams) {
        count = 3;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ApIServices apIServices = new ApIServices(context, StartTripViewModel.this);
        apIServices.dooperation(Iconstant.multipleDrop_Url, jsonParams);
    }


    public void setjson(continuetrippojo object) {
        this.object = object;
    }

    @Override
    public void sucessresponse(String val) {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        if (count == 0) {
            cancelresons.postValue(val);
        } else if (count == 1) {
            beginapiresponse.postValue(val);
        } else if (count == 2) {
            retundropoffresponse.postValue(val);
        } else if (count == 3) {
            multidropoffresponse.postValue(val);
        }

    }

    @Override
    public void errorreponse() {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
    }

    @Override
    public void jsonexception() {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        AppUtils.toastprint(context, "Json Exception");
    }

    public void callsuppport() {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + object.getResponse().getUser_profile().getPhone_number()));
        context.startActivity(intent);
    }

    public void chatsupport() {
        AppUtils.toastprint(context, "coming soon!!");
    }
}
