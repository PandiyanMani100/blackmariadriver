package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class referrallist implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }

    public class Response {
        @SerializedName("driver_id")
        private String driver_id;
        @SerializedName("ref_count")
        private String ref_count;

        public String getReferal_code() {
            return referal_code;
        }

        public void setReferal_code(String referal_code) {
            this.referal_code = referal_code;
        }

        @SerializedName("referal_code")
        private String referal_code;

        public ArrayList<Ref_info> getRef_info() {
            return ref_info;
        }

        public void setRef_info(ArrayList<Ref_info> ref_info) {
            this.ref_info = ref_info;
        }
        @SerializedName("ref_info")
        ArrayList < Ref_info > ref_info = new ArrayList< Ref_info >();


        public class Ref_info{
            @SerializedName("driver_name")
            private String driver_name;
            @SerializedName("driver_image")
            private String driver_image;
            @SerializedName("location")
            private String location;
            @SerializedName("created")
            private String created;


            // Getter Methods 

            public String getDriver_name() {
                return driver_name;
            }

            public String getDriver_image() {
                return driver_image;
            }

            public String getLocation() {
                return location;
            }

            public String getCreated() {
                return created;
            }

            // Setter Methods 

            public void setDriver_name(String driver_name) {
                this.driver_name = driver_name;
            }

            public void setDriver_image(String driver_image) {
                this.driver_image = driver_image;
            }

            public void setLocation(String location) {
                this.location = location;
            }

            public void setCreated(String created) {
                this.created = created;
            }
        }
        // Getter Methods

        public String getDriver_id() {
            return driver_id;
        }

        public String getRef_count() {
            return ref_count;
        }

        // Setter Methods

        public void setDriver_id(String driver_id) {
            this.driver_id = driver_id;
        }

        public void setRef_count(String ref_count) {
            this.ref_count = ref_count;
        }
    }
}

