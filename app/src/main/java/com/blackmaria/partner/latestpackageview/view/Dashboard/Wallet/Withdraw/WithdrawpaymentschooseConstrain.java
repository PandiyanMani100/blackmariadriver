package com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Withdraw;

import androidx.appcompat.app.AppCompatActivity;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityWithdrawpaymentschooseConstrainBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Wallet.Withdraw.WithdrawpaymentschooseFactory;
import com.blackmaria.partner.latestpackageview.Model.withdrawalspaymentspojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Withdraw.Bank.NobankaccountConstrain;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Withdraw.Bank.WithdrawchangereceipeintsendnowConstrain;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Withdraw.Cash.CashwithdrawalConstrain;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Withdraw.Paypal.Paypalwithdrawidenterconstrain;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet.withdraw.WithdrawpaymentschooseViewModel;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

public class WithdrawpaymentschooseConstrain extends AppCompatActivity {

    private ActivityWithdrawpaymentschooseConstrainBinding binding;
    private WithdrawpaymentschooseViewModel withdrawpaymentschooseViewModel;
    private SessionManager sessionManager;
    private AppUtils appUtils;
    private String sDriverID = "", sDrivername = "", sDrivernumber = "";
    private withdrawalspaymentspojo pojoglobal;
    private JSONObject json;
    AccessLanguagefromlocaldb db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
        binding = DataBindingUtil.setContentView(WithdrawpaymentschooseConstrain.this, R.layout.activity_withdrawpaymentschoose_constrain);
        withdrawpaymentschooseViewModel = ViewModelProviders.of(this, new WithdrawpaymentschooseFactory(this)).get(WithdrawpaymentschooseViewModel.class);
        binding.setWithdrawpaymentschooseViewModel(withdrawpaymentschooseViewModel);
        withdrawpaymentschooseViewModel.setIds(binding);

        initView();
        clicklistener();
        setresponse();

        binding.fastpaybalancename.setText(db.getvalue("fastpaybalance"));
        binding.mobileno.setHint(db.getvalue("inserwithdrawamount"));
        binding.howdoyoupaidxt.setText(db.getvalue("howdoyoupaidxt"));
    }

    private void clicklistener() {

    }

    private void setresponse() {
        withdrawpaymentschooseViewModel.getWalletwithdrawresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    json = new JSONObject(response);
                    Type type = new TypeToken<withdrawalspaymentspojo>() {
                    }.getType();
                    pojoglobal = new GsonBuilder().create().fromJson(json.toString(), type);
                    sessionManager.setcashlocationlist(json.toString());
                    setpaymentlist();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setpaymentlist() {
        binding.fastpaybalance.setText(pojoglobal.getResponse().getCurrency() + " " + pojoglobal.getResponse().getAvailable_amount());
        binding.currency.setText(pojoglobal.getResponse().getCurrency());
        try {
            binding.payments.removeAllViews();
        } catch (NullPointerException e) {
        }
        for (int i = 0; i <= pojoglobal.getResponse().getPayment().size() - 1; i++) {
            View view = getLayoutInflater().inflate(R.layout.choosepaymentcustomlayout, null);
            ImageView image = view.findViewById(R.id.image);
            LinearLayout constrainview = view.findViewById(R.id.constrainview);
            AppUtils.setImageviewwithoutcropplaceholder(WithdrawpaymentschooseConstrain.this, pojoglobal.getResponse().getPayment().get(i).getIcon(), image);
            binding.payments.addView(view);
            final int finalI = i;
            constrainview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (pojoglobal.getResponse().getProceed_status().equalsIgnoreCase("1")) {
                        double withdrawAmount = Double.parseDouble(binding.mobileno.getText().toString().trim());
                        if (withdrawAmount > 0) {
                            checkallpayments(withdrawAmount, finalI, pojoglobal);
                        } else {
                            appUtils.AlertError(WithdrawpaymentschooseConstrain.this, db.getvalue("action_error"), db.getvalue("withdrawamountcannotbeempty"));
                        }
                        } else {
                            appUtils.AlertError(WithdrawpaymentschooseConstrain.this, db.getvalue("action_error"), pojoglobal.getResponse().getError_message());
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                        appUtils.AlertError(WithdrawpaymentschooseConstrain.this, db.getvalue("action_error"),db.getvalue("withdrawamountcannotbeempty"));
                    }
                }
            });
        }

    }

    private void checkallpayments(double withdrawAmount, int finalI, withdrawalspaymentspojo pojo) {

        if (pojo.getResponse().getPayment().get(finalI).getCode().equalsIgnoreCase("xendit-banktransfer") || pojo.getResponse().getPayment().get(finalI).getCode().equalsIgnoreCase("xendit-bankpayment")) {
            if (pojo.getResponse().getBanking().equalsIgnoreCase("0")) {
                sessionManager.setpaymentcode(pojo.getResponse().getPayment().get(finalI).getCode());
                Intent bankIntent = new Intent(WithdrawpaymentschooseConstrain.this, NobankaccountConstrain.class);
                bankIntent.putExtra("transfer_amount", binding.mobileno.getText().toString().trim());
                bankIntent.putExtra("json", json.toString());
                startActivity(bankIntent);
            } else {
                sessionManager.setpaymentcode(pojo.getResponse().getPayment().get(finalI).getCode());
                Intent bankIntent = new Intent(WithdrawpaymentschooseConstrain.this, WithdrawchangereceipeintsendnowConstrain.class);
                bankIntent.putExtra("transfer_amount", binding.mobileno.getText().toString().trim());
                bankIntent.putExtra("json", json.toString());
                startActivity(bankIntent);
            }
        } else if (pojo.getResponse().getPayment().get(finalI).getCode().equalsIgnoreCase("paypal")) {
            Intent paypal = new Intent(WithdrawpaymentschooseConstrain.this, Paypalwithdrawidenterconstrain.class);
            paypal.putExtra("transfer_amount", binding.mobileno.getText().toString().trim());
            paypal.putExtra("paypalcode", pojo.getResponse().getPayment().get(finalI).getCode());
            paypal.putExtra("currency", pojo.getResponse().getCurrency());
            startActivity(paypal);
        } else if (pojo.getResponse().getPayment().get(finalI).getCode().equalsIgnoreCase("cash")) {
            Intent paypal = new Intent(WithdrawpaymentschooseConstrain.this, CashwithdrawalConstrain.class);
            paypal.putExtra("transfer_amount", binding.mobileno.getText().toString().trim());
            paypal.putExtra("service_amount", pojoglobal.getResponse().getWithdraw_fee());
            paypal.putExtra("name", pojoglobal.getResponse().getDriver_name());
            paypal.putExtra("phone", pojoglobal.getResponse().getDail_code() + "" + pojoglobal.getResponse().getMobile_number());
            paypal.putExtra("image", pojo.getResponse().getPayment().get(finalI).getIcon());
            paypal.putExtra("currency", pojo.getResponse().getCurrency());
            paypal.putExtra("paymentcode", pojo.getResponse().getPayment().get(finalI).getCode());
            startActivity(paypal);
        }
    }

    private void initView() {
        sessionManager = SessionManager.getInstance(this);
        appUtils = AppUtils.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        sDrivername = user.get(SessionManager.KEY_DRIVER_NAME);
        sDrivernumber = user.get(SessionManager.KEY_CONTACT_NUMBER);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        withdrawpaymentschooseViewModel.walletwithdrawhome(Iconstant.wallet_withdrawal_dashboard_url, jsonParams);

    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Log.v("TRIMMEMORY", " " + level);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }


}
