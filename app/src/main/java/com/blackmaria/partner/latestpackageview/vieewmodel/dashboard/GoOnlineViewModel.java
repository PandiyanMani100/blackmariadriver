package com.blackmaria.partner.latestpackageview.vieewmodel.dashboard;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityOnlinepageConstrainBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Dashboard_constrain;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

public class GoOnlineViewModel extends ViewModel implements ApIServices.completelisner {
    private Activity context;
    private ActivityOnlinepageConstrainBinding binding;
    private Dialog dialog;
    private MutableLiveData<String> onlineresponse = new MutableLiveData<>();
    private MutableLiveData<String> updateavailability = new MutableLiveData<>();
    private MutableLiveData<String> acceptride = new MutableLiveData<>();
    private SessionManager sessionManager;
    private String sDriveID = "";
    private int count = 0;

    public GoOnlineViewModel(Activity context) {
        this.context = context;
        sessionManager = SessionManager.getInstance(context);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriveID = user.get(SessionManager.KEY_DRIVERID);
    }

    public void setIds(ActivityOnlinepageConstrainBinding binding) {
        this.binding = binding;
    }

    public void goofline() {

        context.startActivity(new Intent(context, Dashboard_constrain.class));
        EventBus.getDefault().unregister(this);
    }

    public MutableLiveData<String> getOnlineresponse() {
        return onlineresponse;
    }

    public MutableLiveData<String> getAcceptride() {
        return acceptride;
    }

    public MutableLiveData<String> getUpdateavailability() {
        return updateavailability;
    }

    public void onlineapihit() {
        count = 1;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        Thread thread = new Thread() {
            @Override
            public void run() {
                HashMap<String, String> jsonParams = new HashMap<String, String>();
                jsonParams.put("driver_id", sDriveID);
                ApIServices apIServices = new ApIServices(context, GoOnlineViewModel.this);
                apIServices.dooperation(Iconstant.driverMapView_Url, jsonParams);
            }
        };
        thread.start();

    }

    public void updateavailablity() {
        count = 2;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        Thread thread = new Thread() {
            @Override
            public void run() {
                HashMap<String, String> jsonParams = new HashMap<String, String>();
                jsonParams.put("driver_id", sDriveID);
                jsonParams.put("availability", "No");

                ApIServices apIServices = new ApIServices(context, GoOnlineViewModel.this);
                apIServices.dooperation(Iconstant.updateAvailability_Url, jsonParams);
            }
        };
        thread.start();

    }

    public void rideaceptapihit(final String rider_id, final String lat, final String lon) {
        count = 3;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        Thread thread = new Thread() {
            @Override
            public void run() {
                HashMap<String, String> jsonParams = new HashMap<String, String>();
                jsonParams.put("ride_id", rider_id);
                jsonParams.put("driver_id", sDriveID);
                jsonParams.put("driver_lat", lat);
                jsonParams.put("driver_lon", lon);

                ApIServices apIServices = new ApIServices(context, GoOnlineViewModel.this);
                apIServices.dooperation(Iconstant.driverAcceptRequest_Url, jsonParams);
            }
        };
        thread.start();

    }


    @Override
    public void sucessresponse(String val) {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        if (count == 1) {
            onlineresponse.postValue(val);
        } else if (count == 2) {
            updateavailability.postValue(val);
        }else if (count == 3) {
            acceptride.postValue(val);
        }
    }

    @Override
    public void errorreponse() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void jsonexception() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }
}
