package com.blackmaria.partner.latestpackageview.view.Dashboard.Tracking;


import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.view.View;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityMultistopviewallBinding;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Tracking.MultstopviewallFactory;
import com.blackmaria.partner.latestpackageview.Model.continuetrippojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.tracking.MultistopviewViewModel;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;

public class Multistopviewall extends AppCompatActivity {

    private ActivityMultistopviewallBinding binding;
    private MultistopviewViewModel multistopviewViewModel;
    private SessionManager sessionManager;
    private continuetrippojo object;
    private int count = 0;

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(Multistopviewall.this, R.layout.activity_multistopviewall);
        multistopviewViewModel = ViewModelProviders.of(this, new MultstopviewallFactory(this)).get(MultistopviewViewModel.class);
        binding.setMultistopviewViewModel(multistopviewViewModel);
        multistopviewViewModel.setIds(binding);

        initView();
        binding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void initView() {
        sessionManager = SessionManager.getInstance(this);
        count = Integer.parseInt(getIntent().getStringExtra("count"));
        setText();
    }

    private void setText() {
        try {
            binding.droplocations.removeAllViews();
            JSONObject jsonObject = new JSONObject(sessionManager.getContinuetripjson());
            Type type = new TypeToken<continuetrippojo>() {
            }.getType();
            object = new GsonBuilder().create().fromJson(jsonObject.toString(), type);
        } catch (JSONException e) {
        }
        AppUtils.setImageView(Multistopviewall.this, object.getResponse().getUser_profile().getUser_image(), binding.userimage);
        binding.username.setText(object.getResponse().getUser_profile().getUser_name());
        for (int i = 0; i <= object.getResponse().getUser_profile().getMultistop_array().size() - 1; i++) {
            View view = getLayoutInflater().inflate(R.layout.multipstopcustomlayout, null);
            CardView drivenow = view.findViewById(R.id.drivenow);
            TextView multistopcount = view.findViewById(R.id.multistopcount);
            TextView pickuplocationvalue = view.findViewById(R.id.pickuplocationvalue);
            multistopcount.setText(String.valueOf(i + 1));
            pickuplocationvalue.setText(object.getResponse().getUser_profile().getMultistop_array().get(i).getLocation());
            if (count == i + 1) {
                multistopcount.setTextColor(getResources().getColor(R.color.btn_green_color));
            } else {
                multistopcount.setTextColor(getResources().getColor(R.color.grey));
            }
            binding.droplocations.addView(view);
        }
    }
}
