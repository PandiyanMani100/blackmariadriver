package com.blackmaria.partner.latestpackageview.ApiRequest;


import android.app.IntentService;
import android.content.Intent;

import com.blackmaria.partner.latestpackageview.Database.RidesDB;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class MytripsdbApiIntentService extends IntentService implements ApIServices.completelisner {

    private String url = "";
    private String driverid = "";

    public MytripsdbApiIntentService() {
        super("MytripsdbApiIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        HashMap<String, String> hashMap = (HashMap<String, String>) intent.getSerializableExtra("params");
        url = intent.getStringExtra("url");
        driverid = intent.getStringExtra("driverid");
        ApIServices apIServices = new ApIServices(getApplicationContext(), MytripsdbApiIntentService.this);
        apIServices.dooperation(url, hashMap);
    }

    @Override
    public void sucessresponse(String val) {
        RidesDB ridesDB = new RidesDB(MytripsdbApiIntentService.this);
        ridesDB.insertmytrips(driverid, val);
    }

    @Override
    public void errorreponse() {
    }

    @Override
    public void jsonexception() {
    }
}
