package com.blackmaria.partner.latestpackageview.vieewmodel.dashboard;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.app.Dialog;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModel;

import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.HeaderSessionManager;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.app.AboutUs;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApiIntentService;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Dashboardconstrainfactory;
import com.blackmaria.partner.latestpackageview.Model.clickdisabledashboardpojo;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Dashboard_constrain;
import com.blackmaria.partner.latestpackageview.view.sidemenu.CloseAccount.DeActiviationAccount;
import com.blackmaria.partner.latestpackageview.view.sidemenu.Invitefriends.Invitefriends_constrain;
import com.blackmaria.partner.latestpackageview.view.sidemenu.LegalPage;
import com.blackmaria.partner.databinding.ActivityDashboardConstrainBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.sidemenu.Maintanence.Maintanencedashboard;
import com.blackmaria.partner.latestpackageview.view.sidemenu.Maintanence.Maintanenceopendashboard;
import com.blackmaria.partner.latestpackageview.view.sidemenu.profile.ProfileConstrain;
import com.blackmaria.partner.latestpackageview.view.sidemenu.Ratings.RatingHomepageConstrain;
import com.blackmaria.partner.latestpackageview.view.sidemenu.Warnings.Warningsconstrain;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.yalantis.ucrop.UCrop;

import org.greenrobot.eventbus.EventBus;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import de.cketti.mailto.EmailIntentBuilder;

import static com.yalantis.ucrop.util.BitmapLoadUtils.exifToDegrees;

public class DashboardViewModel extends AndroidViewModel implements ApIServices.completelisner {

    private Dashboardconstrainfactory factoryobj;
    Context context;
    private MutableLiveData<String> imageupload = new MutableLiveData<>();
    private MutableLiveData<String> logoutresponse = new MutableLiveData<>();

    private int imageup = 0;

    public MutableLiveData<String> getImageupload() {
        return imageupload;
    }

    public MutableLiveData<String> getLogoutresponse() {
        return logoutresponse;
    }

    public DashboardViewModel(@NonNull Application application) {
        super(application);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        context = application.getApplicationContext();
        factoryobj = new Dashboardconstrainfactory();
    }


    public LiveData<String> getimageupload(String driverimageupdate, byte[] array, String s) {
        ApIServices apIServices = new ApIServices(context, DashboardViewModel.this);
        apIServices.imageupload(driverimageupdate, array, s);
        return imageupload;
    }

    public LiveData<String> getimageamazonupload(String driverimageupdate, String filename) {
        HashMap<String, String> jsonParams = new HashMap<>();
        jsonParams.put("image", filename);

        ApIServices apIServices = new ApIServices(context, DashboardViewModel.this);
        apIServices.amazoniagealoinesend(driverimageupdate,jsonParams);
        return imageupload;
    }

    public LiveData<String> logoutapi(String profile_driver_account_logout_url, HashMap<String, String> jsonParams) {
        imageup = 1;
        ApIServices apIServices = new ApIServices(context, DashboardViewModel.this);
        apIServices.dooperation(profile_driver_account_logout_url, jsonParams);

        return logoutresponse;
    }

    @Override
    public void sucessresponse(String val) {
        if (imageup == 1) {
            logoutresponse.postValue(val);
        } else {
            imageupload.postValue(val);
        }
    }

    @Override
    public void errorreponse() {

    }

    @Override
    public void jsonexception() {

    }
}
