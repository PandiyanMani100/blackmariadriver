package com.blackmaria.partner.latestpackageview.Factory.Dashboard.Wallet;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet.RechargecardpaymentViewModel;


public class RechargecardFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public RechargecardFactory(Activity context) {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new RechargecardpaymentViewModel(context);
    }
}
