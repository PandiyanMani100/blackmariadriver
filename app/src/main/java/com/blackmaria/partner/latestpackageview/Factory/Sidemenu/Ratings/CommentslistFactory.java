package com.blackmaria.partner.latestpackageview.Factory.Sidemenu.Ratings;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackmaria.partner.latestpackageview.vieewmodel.sidemenu.ratings.CommentslistViewModel;


public class CommentslistFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public CommentslistFactory(Activity context)
    {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass)
    {
        return (T) new CommentslistViewModel(context);
    }
}
