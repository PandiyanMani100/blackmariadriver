package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class driverdashboad implements Serializable {

    @SerializedName("status")
    private String status;



    @SerializedName("response")
    Response ResponseObject;

    // Getter Methods 

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods 

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }
    
    public class Response {
        @SerializedName("normal_fare")
        private String normal_fare;
        @SerializedName("pending_ride_status")
        private String pending_ride_status;

        public String getCategory_id() {
            return category_id;
        }

        public void setCategory_id(String category_id) {
            this.category_id = category_id;
        }

        @SerializedName("category_id")
        private String category_id;

        public String getDriver_type() {
            return driver_type;
        }

        public void setDriver_type(String driver_type) {
            this.driver_type = driver_type;
        }

        @SerializedName("driver_type")
        private String driver_type;

        public String getAdditional_driver_id() {
            return additional_driver_id;
        }

        public void setAdditional_driver_id(String additional_driver_id) {
            this.additional_driver_id = additional_driver_id;
        }

        @SerializedName("additional_driver_id")
        private String additional_driver_id;

        public String getPending_ride_status() {
            return pending_ride_status;
        }

        public void setPending_ride_status(String pending_ride_status) {
            this.pending_ride_status = pending_ride_status;
        }

        public String getPending_ride_id() {
            return pending_ride_id;
        }

        public void setPending_ride_id(String pending_ride_id) {
            this.pending_ride_id = pending_ride_id;
        }

        @SerializedName("pending_ride_id")
        private String pending_ride_id;

        public String getDriver_name() {
            return driver_name;
        }

        public void setDriver_name(String driver_name) {
            this.driver_name = driver_name;
        }

        @SerializedName("driver_name")
        private String driver_name;

        public String getWallet_constraint() {
            return wallet_constraint;
        }

        public void setWallet_constraint(String wallet_constraint) {
            this.wallet_constraint = wallet_constraint;
        }

        @SerializedName("wallet_constraint")
        private String wallet_constraint;

        public String getWallet_message() {
            return wallet_message;
        }

        public void setWallet_message(String wallet_message) {
            this.wallet_message = wallet_message;
        }

        @SerializedName("wallet_message")
        private String wallet_message;

        @SerializedName("referral_code")
        private String referral_code;

        @SerializedName("referral_count")
        private String referral_count;

        public String getReferral_code() {
            return referral_code;
        }

        public void setReferral_code(String referral_code) {
            this.referral_code = referral_code;
        }

        public String getReferral_count() {
            return referral_count;
        }

        public void setReferral_count(String referral_count) {
            this.referral_count = referral_count;
        }

        public String getWarning_count() {
            return warning_count;
        }

        public void setWarning_count(String warning_count) {
            this.warning_count = warning_count;
        }

        @SerializedName("warning_count")
        private String warning_count;

        public String getMin_wallet_amount() {
            return min_wallet_amount;
        }

        public void setMin_wallet_amount(String min_wallet_amount) {
            this.min_wallet_amount = min_wallet_amount;
        }

        public String getCurrentbalance() {
            return current_balance;
        }

        public void setCurrentbalance(String current_balance) {
            this.current_balance = current_balance;
        }

        @SerializedName("min_wallet_amount")
        private String min_wallet_amount ;
        @SerializedName("current_balance")
        private String current_balance ;
        @SerializedName("bid_fare")
        private String bid_fare;
        @SerializedName("sos")
        private String sos;
        @SerializedName("referal_code")
        private String referal_code;
        @SerializedName("complaint_badge")
        private String complaint_badge;
        @SerializedName("info_badge")
        private String info_badge;
        @SerializedName("total_completed_trip")
        private String total_completed_trip;
        @SerializedName("near_by_driver")
        private String near_by_driver;
        @SerializedName("searching_radius")
        private String searching_radius;
        @SerializedName("verify_status")
        private String verify_status;
        @SerializedName("driver_earning")
        private String driver_earning;
        @SerializedName("wallet_amount")
        private String wallet_amount;
        @SerializedName("incetives_total")
        private String incetives_total;
        @SerializedName("currencyCode")
        private String currencyCode;
        @SerializedName("vehicle_number")
        private String vehicle_number;
        @SerializedName("image")
        private String image;
        @SerializedName("go_online_status")
        private String go_online_status;
        @SerializedName("go_online_string")
        private String go_online_string;
        @SerializedName("city_name")
        private String city_name;
        @SerializedName("driver_availability")
        private String driver_availability;
        @SerializedName("category_icon")
        private String category_icon;
        @SerializedName("driver_image")
        private String driver_image;

        @SerializedName("driver_active")
        private String driver_active;

        public String getDriver_active() {
            return driver_active;
        }

        public void setDriver_active(String driver_active) {
            this.driver_active = driver_active;
        }

        public String getDriver_active_message() {
            return driver_active_message;
        }

        public void setDriver_active_message(String driver_active_message) {
            this.driver_active_message = driver_active_message;
        }

        @SerializedName("driver_active_message")
        private String driver_active_message;


        // Getter Methods 

        public String getNormal_fare() {
            return normal_fare;
        }

        public String getBid_fare() {
            return bid_fare;
        }

        public String getSos() {
            return sos;
        }

        public String getReferal_code() {
            return referal_code;
        }

        public String getComplaint_badge() {
            return complaint_badge;
        }

        public String getInfo_badge() {
            return info_badge;
        }

        public String getTotal_completed_trip() {
            return total_completed_trip;
        }

        public String getNear_by_driver() {
            return near_by_driver;
        }

        public String getSearching_radius() {
            return searching_radius;
        }

        public String getVerify_status() {
            return verify_status;
        }

        public String getDriver_earning() {
            return driver_earning;
        }

        public String getWallet_amount() {
            return wallet_amount;
        }

        public String getIncetives_total() {
            return incetives_total;
        }

        public String getCurrencyCode() {
            return currencyCode;
        }

        public String getVehicle_number() {
            return vehicle_number;
        }

        public String getImage() {
            return image;
        }

        public String getGo_online_status() {
            return go_online_status;
        }

        public String getGo_online_string() {
            return go_online_string;
        }

        public String getCity_name() {
            return city_name;
        }

        public String getDriver_availability() {
            return driver_availability;
        }

        public String getCategory_icon() {
            return category_icon;
        }

        public String getDriver_image() {
            return driver_image;
        }

        // Setter Methods 

        public void setNormal_fare(String normal_fare) {
            this.normal_fare = normal_fare;
        }

        public void setBid_fare(String bid_fare) {
            this.bid_fare = bid_fare;
        }

        public void setSos(String sos) {
            this.sos = sos;
        }

        public void setReferal_code(String referal_code) {
            this.referal_code = referal_code;
        }

        public void setComplaint_badge(String complaint_badge) {
            this.complaint_badge = complaint_badge;
        }

        public void setInfo_badge(String info_badge) {
            this.info_badge = info_badge;
        }

        public void setTotal_completed_trip(String total_completed_trip) {
            this.total_completed_trip = total_completed_trip;
        }

        public void setNear_by_driver(String near_by_driver) {
            this.near_by_driver = near_by_driver;
        }

        public void setSearching_radius(String searching_radius) {
            this.searching_radius = searching_radius;
        }

        public void setVerify_status(String verify_status) {
            this.verify_status = verify_status;
        }

        public void setDriver_earning(String driver_earning) {
            this.driver_earning = driver_earning;
        }

        public void setWallet_amount(String wallet_amount) {
            this.wallet_amount = wallet_amount;
        }

        public void setIncetives_total(String incetives_total) {
            this.incetives_total = incetives_total;
        }

        public void setCurrencyCode(String currencyCode) {
            this.currencyCode = currencyCode;
        }

        public void setVehicle_number(String vehicle_number) {
            this.vehicle_number = vehicle_number;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public void setGo_online_status(String go_online_status) {
            this.go_online_status = go_online_status;
        }

        public void setGo_online_string(String go_online_string) {
            this.go_online_string = go_online_string;
        }

        public void setCity_name(String city_name) {
            this.city_name = city_name;
        }

        public void setDriver_availability(String driver_availability) {
            this.driver_availability = driver_availability;
        }

        public void setCategory_icon(String category_icon) {
            this.category_icon = category_icon;
        }

        public void setDriver_image(String driver_image) {
            this.driver_image = driver_image;
        }
    }
    
}

