package com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Transfer;


import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityTransfersuccessConstrainBinding;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Wallet.Transfer.TransfersuccessFactory;
import com.blackmaria.partner.latestpackageview.Model.moneytransferasuccess;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet.transfer.TransfersuccessViewModel;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class TransfersuccessConstrain extends AppCompatActivity {

    private ActivityTransfersuccessConstrainBinding binding;
    private TransfersuccessViewModel transfersuccessViewModel;
    private SessionManager sessionManager;
    private AppUtils appUtils;
    private moneytransferasuccess pojo;
    AccessLanguagefromlocaldb db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfersuccess_constrain);
        db = new AccessLanguagefromlocaldb(this);
        binding = DataBindingUtil.setContentView(TransfersuccessConstrain.this, R.layout.activity_transfersuccess_constrain);
        transfersuccessViewModel = ViewModelProviders.of(this, new TransfersuccessFactory(this)).get(TransfersuccessViewModel.class);
        binding.setTransfersuccessViewModel(transfersuccessViewModel);
        transfersuccessViewModel.setIds(binding);

        initView();

        binding.datemonthyear.setText(db.getvalue("fastwallet"));
        binding.transfer.setText(db.getvalue("thankyou"));
        binding.success.setText(db.getvalue("moneytransfersuccess"));
        binding.notificationtxt.setText(db.getvalue("successcontent"));
        binding.cancel.setText(db.getvalue("close_lable"));
    }

    private void initView() {
        sessionManager = SessionManager.getInstance(this);
        appUtils = AppUtils.getInstance(this);

        if (getIntent().hasExtra("json")) {
            Type type = new TypeToken<moneytransferasuccess>() {
            }.getType();
            pojo = new GsonBuilder().create().fromJson(getIntent().getStringExtra("json").toString(), type);
            AppUtils.setImageView(TransfersuccessConstrain.this, pojo.getUser_image(), binding.userimage);
            binding.amount.setText(pojo.getCurrency() + " " + pojo.getTransfer_amount());
            binding.usercontent.setText("SENT TO :\n" + "NAME : " + pojo.getDriver_name() + "\n" + "MOBILE NO : " + pojo.getDail_code() + "" + pojo.getMobile_number() + "\n" + "CITY : " + pojo.getCity());
            binding.transactionumber.setText("TRANSACTION NUMBER : " + pojo.getTransaction_number());
        }
    }

    @Override
    public void onBackPressed() {
    }
}
