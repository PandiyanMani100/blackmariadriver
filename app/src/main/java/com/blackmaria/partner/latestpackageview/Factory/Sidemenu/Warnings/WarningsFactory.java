package com.blackmaria.partner.latestpackageview.Factory.Sidemenu.Warnings;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackmaria.partner.latestpackageview.vieewmodel.sidemenu.warnings.WarningsViewModel;


public class WarningsFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public WarningsFactory(Activity context)
    {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass)
    {
        return (T) new WarningsViewModel(context);
    }
}
