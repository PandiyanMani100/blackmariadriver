package com.blackmaria.partner.latestpackageview.Factory.Dashboard;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.GoOnlineViewModel;


public class GoonlineFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public GoonlineFactory(Activity context) {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new GoOnlineViewModel(context);
    }
}
