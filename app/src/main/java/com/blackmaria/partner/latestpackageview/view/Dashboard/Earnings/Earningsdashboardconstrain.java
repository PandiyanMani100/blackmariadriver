package com.blackmaria.partner.latestpackageview.view.Dashboard.Earnings;

import android.app.Dialog;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.annotation.Nullable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.app.WaitingTimePage;
import com.blackmaria.partner.latestpackageview.widgets.RoundedImageView;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityEarningsdashboardBinding;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Earnings.EarningsdashboardFactory;
import com.blackmaria.partner.latestpackageview.Model.eariningsdashboardpojo;
import com.blackmaria.partner.latestpackageview.Model.withdrawalstatementpojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Dashboard_constrain;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Pin_activity;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.earnings.EarningsdashboardViewModel;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.HashMap;

public class Earningsdashboardconstrain extends AppCompatActivity {

    private ActivityEarningsdashboardBinding binding;
    private EarningsdashboardViewModel earningsdashboardViewModel;
    private eariningsdashboardpojo eariningsdashboardpojo;
    private AppUtils appUtils;
    private SessionManager sessionManager;
    private String sDriverID = "", sSecurePin = "";
    private boolean earningwithdrawndone = false;
    AccessLanguagefromlocaldb db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
//        EventBus.getDefault().register(this);
        binding = DataBindingUtil.setContentView(Earningsdashboardconstrain.this, R.layout.activity_earningsdashboard);
        earningsdashboardViewModel = ViewModelProviders.of(this, new EarningsdashboardFactory(this)).get(EarningsdashboardViewModel.class);
//        binding.setEarningsdashboardViewModel(earningsdashboardViewModel);

        earningsdashboardViewModel.setIds(binding);

        binding.datemonthyear.setText(db.getvalue("earnings"));
        binding.incomebalance.setText(db.getvalue("incomebalance"));
        binding.withdraw.setText(db.getvalue("withdraw_lable"));
        binding.tripearnings.setText(db.getvalue("tripearnings"));
        binding.today.setText(db.getvalue("today"));
        binding.week.setText(db.getvalue("this_week"));
        binding.month.setText(db.getvalue("this_month"));

        binding.cashtoday.setText(db.getvalue("cash"));
        binding.cardtoday.setText(db.getvalue("card"));
        binding.wallettoday.setText(db.getvalue("wallet"));

        binding.cashweek.setText(db.getvalue("cash"));
        binding.cardweek.setText(db.getvalue("card"));
        binding.walletweek.setText(db.getvalue("wallet"));

        binding.cashmonth.setText(db.getvalue("cash"));
        binding.cardmonth.setText(db.getvalue("card"));
        binding.walletmonth.setText(db.getvalue("wallet"));

        binding.totaleariningstodate.setText(db.getvalue("total_earinings_to_date"));
        binding.driveearn.setText(db.getvalue("driveearn"));
        binding.driveearnenter.setText(db.getvalue("enter"));
        binding.invited.setText(db.getvalue("invited"));
        binding.commissions.setText(db.getvalue("commission"));
        binding.amountdriveearn.setText(db.getvalue("amount"));
        binding.referral.setText(db.getvalue("referral"));
        binding.referralenter.setText(db.getvalue("viewall"));
        binding.noreferral.setText(db.getvalue("earnings_page_label_no_referral_data"));

        dasboardresponse();
        clicklistener();
    }

    private void clicklistener() {
        binding.referralenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Earningsdashboardconstrain.this, ReferrallistConstrain.class));
            }
        });
        binding.driveearnenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                earningsdashboardViewModel.driveandearn();
            }
        });
        binding.backimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                earningsdashboardViewModel.back();
            }
        });
        binding.todayearningcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                earningsdashboardViewModel.today();
            }
        });
        binding.thisweekcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                earningsdashboardViewModel.week();
            }
        });
        binding.thismonthcons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                earningsdashboardViewModel.month();
            }
        });
    }

    private void dasboardresponse() {
        appUtils = AppUtils.getInstance(this);
        sessionManager = SessionManager.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        sSecurePin = sessionManager.getSecurePin();
        earningsdashboardViewModel.getEarningsresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Type type = new TypeToken<eariningsdashboardpojo>() {
                    }.getType();
                    eariningsdashboardpojo = new GsonBuilder().create().fromJson(jsonObject.toString(), type);
                    System.out.println("" + eariningsdashboardpojo.getStatus());
                    setResponse(eariningsdashboardpojo);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        binding.withdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                withdrawAmount();
            }
        });
    }

    private void setResponse(eariningsdashboardpojo eariningsdashboardpojo) {
        if(eariningsdashboardpojo.getStatus() == null)
            return;
        binding.balance.setText(eariningsdashboardpojo.getResponse().getCurrency() + " " + eariningsdashboardpojo.getResponse().getAvailable_amount());
        //today
        binding.todayamount.setText(eariningsdashboardpojo.getResponse().getCurrency() + " " + eariningsdashboardpojo.getResponse().getToday_earning());
        binding.todaycashvalue.setText( eariningsdashboardpojo.getResponse().getToday_payment_ride().getCash());
        binding.todaywalletvalue.setText(eariningsdashboardpojo.getResponse().getToday_payment_ride().getWallet());
        binding.todaycardvalue.setText(eariningsdashboardpojo.getResponse().getToday_payment_ride().getCard());
        //week
        binding.weekamount.setText(eariningsdashboardpojo.getResponse().getCurrency() + " " + eariningsdashboardpojo.getResponse().getWeek_earning());
        binding.weekcashvalue.setText( eariningsdashboardpojo.getResponse().getWeek_payment_ride().getCash());
        binding.weekwalletvalue.setText( eariningsdashboardpojo.getResponse().getWeek_payment_ride().getWallet());
        binding.weekcardvalue.setText( eariningsdashboardpojo.getResponse().getWeek_payment_ride().getCard());
        //month
        binding.monthamount.setText(eariningsdashboardpojo.getResponse().getCurrency() + " " + eariningsdashboardpojo.getResponse().getMonth_earning());
        binding.monthcashvalue.setText( eariningsdashboardpojo.getResponse().getMonth_payment_ride().getCash());
        binding.monthwalletvalue.setText( eariningsdashboardpojo.getResponse().getMonth_payment_ride().getWallet());
        binding.monthcardvalue.setText( eariningsdashboardpojo.getResponse().getMonth_payment_ride().getCard());

        binding.highlights.setText("highlights" + " " + Calendar.getInstance().get(Calendar.YEAR));
        binding.invitedvalue.setText(eariningsdashboardpojo.getResponse().getReferral_count());
        binding.commissionsvalue.setText(eariningsdashboardpojo.getResponse().getRates());
        binding.amountdriveearnvalue.setText(eariningsdashboardpojo.getResponse().getReferral_earning());
        binding.totaleariningstodateamount.setText(eariningsdashboardpojo.getResponse().getCurrency()+" "+eariningsdashboardpojo.getResponse().getTotal_earning_amount());
        setreferrallist(eariningsdashboardpojo);
    }


    public void setreferrallist(eariningsdashboardpojo json) {
        try {
            binding.referralsview.removeAllViews();
        } catch (NullPointerException e) {
        }
        if (json.getResponse().getRef_info().size() > 0) {
            binding.noreferral.setVisibility(View.GONE);
            binding.referralsview.setVisibility(View.VISIBLE);
        } else {
            binding.noreferral.setVisibility(View.VISIBLE);
            binding.referralsview.setVisibility(View.GONE);
        }

        for (int i = 0; i <= json.getResponse().getRef_info().size() - 1; i++) {
            View view = getLayoutInflater().inflate(R.layout.earningsdashboard_referraladapter, null);
            RoundedImageView userimageview = view.findViewById(R.id.userimageview);
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.profileicon_dashboard)
                    .error(R.drawable.profileicon_dashboard)
                    .diskCacheStrategy(DiskCacheStrategy.ALL);
            Glide.with(this).load(json.getResponse().getRef_info().get(i).getDriver_image()).apply(options).into(userimageview);
            binding.referralsview.addView(view);
        }
    }

    private void withdrawAmount() {
        String amount = eariningsdashboardpojo.getResponse().getAvailable_amount();
        if (amount.length() > 0) {
            double totalAmount = Double.parseDouble(amount);
            if (totalAmount > 0) {
                if (eariningsdashboardpojo.getResponse().getProceed_status().equalsIgnoreCase("1")) {
                    if (eariningsdashboardpojo.getResponse().getWithdraw_status().equalsIgnoreCase("1")) {
                        earningsdashboardViewModel.withdrawnapihit();
                        earningsdashboardViewModel.getWithdrawalresponse().observe(Earningsdashboardconstrain.this, new Observer<String>() {
                            @Override
                            public void onChanged(@Nullable String response) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    Type type = new TypeToken<withdrawalstatementpojo>() {
                                    }.getType();
                                    withdrawalstatementpojo withdrawalstatementpojo = new GsonBuilder().create().fromJson(jsonObject.toString(), type);
                                    confirmWithdrawDialog(withdrawalstatementpojo);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    } else {
                        appUtils.AlertError(this, db.getvalue("action_error"), eariningsdashboardpojo.getResponse().getWithdraw_error());
                    }
                } else {
                    appUtils.AlertError(this, db.getvalue("action_error"), eariningsdashboardpojo.getResponse().getError_message());
                }
            } else {
                String message = db.getvalue("earnings_page_label_no_amount_to_withdraw");
                appUtils.AlertError(this, db.getvalue("action_error"), message);
            }

        }

    }

    //--------------Alert Function------------------
    private void confirmWithdrawDialog(final withdrawalstatementpojo withdrawalstatementpojo) {
        final PkDialog mDialog = new PkDialog(Earningsdashboardconstrain.this);
        mDialog.setDialogTitle(db.getvalue("ssdd"));
        mDialog.setDialogMessage(db.getvalue("witowa"));
        mDialog.setPositiveButton(db.getvalue("action_yes"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                earningsdashboardViewModel.withdrawnamouttowallet(withdrawalstatementpojo.getResponse().getAvailable_amount().replace(",",""));
                earningsdashboardViewModel.getWithdrawalamounttowallet().observe(Earningsdashboardconstrain.this, new Observer<String>() {
                    @Override
                    public void onChanged(@Nullable String response) {
                        try {
                            String status = "", sResponse = "";
                            JSONObject object = new JSONObject(response);
                            if (object.length() > 0) {
                                status = object.getString("status");
                                sResponse = object.getString("response");
                                if (status.equalsIgnoreCase("1")) {
                                    earningwithdrawndone = true;
                                    WithdrawSuccessAlert(sResponse, withdrawalstatementpojo.getResponse().getCurrency(), withdrawalstatementpojo.getResponse().getAvailable_amount(), "wallet");
                                } else {
                                    appUtils.AlertError(Earningsdashboardconstrain.this, db.getvalue("action_error"), sResponse);
                                }
                            }
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
        mDialog.setNegativeButton(db.getvalue("action_no"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }




    private void WithdrawSuccessAlert(String title, String currency, String withdrawAmount, String paymentType) {
        final PkDialog mDialog = new PkDialog(Earningsdashboardconstrain.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage("\n" + currency + " " + withdrawAmount + "\n\n" + "MODE : " + paymentType);
        mDialog.setPositiveButton(db.getvalue("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                earningsdashboardViewModel.earningsdasboardapihit();
            }
        });
        mDialog.show();
    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);}
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (earningwithdrawndone) {
            startActivity(new Intent(this, Dashboard_constrain.class));
        }
    }
}




