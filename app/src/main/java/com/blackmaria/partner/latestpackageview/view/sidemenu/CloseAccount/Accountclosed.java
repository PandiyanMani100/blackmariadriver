package com.blackmaria.partner.latestpackageview.view.sidemenu.CloseAccount;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class Accountclosed extends AppCompatActivity implements ApIServices.completelisner {

    private TextView accountno, name, mobile, withdrawnamountvalue, feeschargesvalue, receivedmountvalue, tracsactionno, RL_close_account;
    private ImageView bankimage;
    private String UserID = "";
    private SessionManager sessionManager;
    private Dialog dialog;
    private AppUtils appUtils;
    private ProgressBar apiload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accountclosed);

        sessionManager = SessionManager.getInstance(this);
        appUtils = AppUtils.getInstance(Accountclosed.this);
        apiload = findViewById(R.id.apiload);


        HashMap<String, String> user = sessionManager.getUserDetails();
        UserID = user.get(SessionManager.KEY_DRIVERID);

        initView();

        if (getIntent().hasExtra("mode")) {
            HashMap<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("driver_id", UserID);
            jsonParams.put("amount", sessionManager.getaccountcloseamount());
            jsonParams.put("payment_mode", getIntent().getStringExtra("mode"));

            closeaccount(Iconstant.closeaccount_request, jsonParams);
        }
    }

    private void initView() {
        accountno = findViewById(R.id.accountno);
        name = findViewById(R.id.name);
        mobile = findViewById(R.id.mobile);
        withdrawnamountvalue = findViewById(R.id.withdrawnamountvalue);
        feeschargesvalue = findViewById(R.id.feeschargesvalue);
        receivedmountvalue = findViewById(R.id.receivedmountvalue);
        tracsactionno = findViewById(R.id.tracsactionno);
        RL_close_account = findViewById(R.id.RL_close_account);
        bankimage = findViewById(R.id.bankimage);

        RL_close_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sessionManager.logoutUser();
                finish();
            }
        });
    }

    private void closeaccount(String Url, final HashMap<String, String> jsonParams) {
        apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(this, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ApIServices apIServices = new ApIServices(this, Accountclosed.this);
        apIServices.dooperation(Url, jsonParams);
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void sucessresponse(String response) {
        dialog.dismiss();
        apiload.setVisibility(View.INVISIBLE);
        try {
            JSONObject object = new JSONObject(response);
            String Sstatus = object.getString("status");
            if (Sstatus.equalsIgnoreCase("1")) {
                withdrawnamountvalue.setText(sessionManager.getCurrency() + " " + object.getString("withdrawal_amount"));
                feeschargesvalue.setText(sessionManager.getCurrency() + " " + object.getString("transaction_fee"));
                receivedmountvalue.setText(sessionManager.getCurrency() + " " + object.getString("receive_amount"));
                name.setText(object.getString("driver_name"));
                mobile.setText(object.getString("phone_number"));
                if (object.has("id_number")) {
                    if (!object.getString("id_number").equalsIgnoreCase("")) {
                        accountno.setText("Identification :" + object.getString("id_number"));
                        accountno.setVisibility(View.VISIBLE);
                    } else {
                        accountno.setVisibility(View.GONE);
                    }
                }
                if (object.has("image")) {
                    if (!object.getString("image").equalsIgnoreCase("")) {
                        bankimage.setVisibility(View.VISIBLE);
                        RequestOptions options = new RequestOptions()
                                .centerCrop()
                                .placeholder(R.drawable.may_bank)
                                .error(R.drawable.may_bank)
                                .diskCacheStrategy(DiskCacheStrategy.ALL);
                        Glide.with(Accountclosed.this).load(object.getString("image")).apply(options).into(bankimage);
                    } else {
                        bankimage.setVisibility(View.INVISIBLE);
                    }
                }

            } else {
                appUtils.AlertError(this, getResources().getString(R.string.action_error), object.getString("response"));
            }

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            System.out.println("eee------------------" + e);
            e.printStackTrace();
        }
    }

    @Override
    public void errorreponse() {
        dialog.dismiss();
        apiload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void jsonexception() {
        dialog.dismiss();
        apiload.setVisibility(View.INVISIBLE);
        AppUtils.toastprint(this, "Json Exception");
    }
}
