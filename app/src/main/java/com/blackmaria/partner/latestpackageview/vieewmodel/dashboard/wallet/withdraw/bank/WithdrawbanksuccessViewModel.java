package com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet.withdraw.bank;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import android.content.Intent;

import com.blackmaria.partner.databinding.ActivityWithdrawbanksuccessConstrainBinding;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Dashboard_constrain;

public class WithdrawbanksuccessViewModel extends ViewModel implements ApIServices.completelisner {

    private Activity context;
    private ActivityWithdrawbanksuccessConstrainBinding binding;

    public WithdrawbanksuccessViewModel(Activity context) {
        this.context = context;
    }

    @Override
    public void sucessresponse(String val) {

    }

    @Override
    public void errorreponse() {

    }

    @Override
    public void jsonexception() {

    }

    public void setIds(ActivityWithdrawbanksuccessConstrainBinding binding) {
        this.binding = binding;
    }

    public void cancel() {
        context.startActivity(new Intent(context, Dashboard_constrain.class));
        context.finish();
    }
}
