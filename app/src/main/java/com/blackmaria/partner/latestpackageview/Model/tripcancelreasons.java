package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class tripcancelreasons implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;
    @SerializedName("affected_performance")
    private int affected_performance;


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }


    public int getAffected_performance() {
        return affected_performance;
    }

    public void setAffected_performance(int affected_performance) {
        this.affected_performance = affected_performance;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }
    public class Response {

        public ArrayList<Reason> getReason() {
            return reason;
        }

        public void setReason(ArrayList<Reason> reason) {
            this.reason = reason;
        }
        @SerializedName("reason")
        ArrayList< Reason > reason = new ArrayList < Reason > ();

        public class Reason{
            @SerializedName("id")
            private String id;
            @SerializedName("reason")
            private String reason;

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            @SerializedName("status")
            private String status;

            // Getter Methods

            public String getId() {
                return id;
            }

            public String getReason() {
                return reason;
            }

            // Setter Methods

            public void setId(String id) {
                this.id = id;
            }

            public void setReason(String reason) {
                this.reason = reason;
            }
        }
    }

}

