package com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.mytrips;

import android.app.Activity;
import android.app.Dialog;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityWeektripviewConstrainBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;

import java.util.HashMap;

public class WeektripViewModel extends ViewModel implements ApIServices.completelisner {

    private Activity context;
    private ActivityWeektripviewConstrainBinding binding;
    private Dialog dialog;
    private MutableLiveData<String> weekwiseresponse = new MutableLiveData<>();
    private SessionManager sessionManager;
    private String driverid = "";

    public WeektripViewModel(Activity context) {
        this.context = context;
        sessionManager = SessionManager.getInstance(context);
        HashMap<String, String> user = sessionManager.getUserDetails();
        driverid = user.get(SessionManager.KEY_DRIVERID);
    }

    public void setIds(ActivityWeektripviewConstrainBinding binding) {
        this.binding = binding;
    }

    public MutableLiveData<String> getWeekwiseresponse() {
        return weekwiseresponse;
    }

    public void weekwiseapihit(String fromdate, String todate, boolean value) {
        if (value) {
            binding.apiload.setVisibility(View.VISIBLE);
            dialog = new Dialog(context, R.style.CustomDialogTheme);
            dialog.getWindow();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.loading_model);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", driverid);
        jsonParams.put("from_date", fromdate);
        jsonParams.put("to_date", todate);
        ApIServices apIServices = new ApIServices(context, WeektripViewModel.this);
        apIServices.dooperation(Iconstant.week_trips_details_url, jsonParams);
    }

    public void back() {
        context.onBackPressed();
    }

    @Override
    public void sucessresponse(String val) {
        if (dialog != null) {
            dialog.dismiss();
            binding.apiload.setVisibility(View.INVISIBLE);
        }
        weekwiseresponse.postValue(val);
    }

    @Override
    public void errorreponse() {
        if (dialog != null) {
            dialog.dismiss();
            binding.apiload.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void jsonexception() {
        if (dialog != null) {
            dialog.dismiss();
            binding.apiload.setVisibility(View.INVISIBLE);
        }
    }
}
