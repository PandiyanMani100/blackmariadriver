package com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.mytrips;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityMytripshomepageConstrainBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Mytrips.MytripsPagination_constrain;

import java.util.HashMap;

public class MytripsViewModel extends ViewModel implements ApIServices.completelisner {

    private Activity context;
    public ActivityMytripshomepageConstrainBinding binding;
    public MutableLiveData<String> getdashboardresponse = new MutableLiveData<>();
    private Dialog dialog;
    private SessionManager sessionManager;
    private String driver_id = "";

    public MytripsViewModel(Activity context) {
        this.context = context;
        sessionManager = SessionManager.getInstance(context);
        HashMap<String, String> user = sessionManager.getUserDetails();
        driver_id = user.get(SessionManager.KEY_DRIVERID);
    }

    public void setIds(ActivityMytripshomepageConstrainBinding binding) {
        this.binding = binding;
    }

    public void hittriphomepage(){
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", driver_id);

        ApIServices apIServices = new ApIServices(context, MytripsViewModel.this);
        apIServices.dooperation(Iconstant.myTrip_Url, jsonParams);
    }

    public MutableLiveData<String> getGetdashboardresponse() {
        return getdashboardresponse;
    }

    @Override
    public void sucessresponse(String val) {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        getdashboardresponse.postValue(val);
    }

    @Override
    public void errorreponse() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void jsonexception() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        AppUtils.toastprint(context, "Json Exception");
    }

    public void month() {
        Intent i = new Intent(context, MytripsPagination_constrain.class);
        i.putExtra("type", "3");
        context.startActivity(i);
    }

    public void week() {
        Intent i = new Intent(context, MytripsPagination_constrain.class);
        i.putExtra("type", "2");
        context.startActivity(i);
    }

    public void today() {
        Intent i = new Intent(context, MytripsPagination_constrain.class);
        i.putExtra("type", "1");
        context.startActivity(i);
    }
}

