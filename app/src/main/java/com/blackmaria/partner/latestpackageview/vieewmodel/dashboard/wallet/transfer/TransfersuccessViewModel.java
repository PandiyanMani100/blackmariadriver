package com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet.transfer;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import android.content.Intent;

import com.blackmaria.partner.databinding.ActivityTransfersuccessConstrainBinding;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Dashboard_constrain;

public class TransfersuccessViewModel extends ViewModel {
    private Activity context;
    private ActivityTransfersuccessConstrainBinding binding;

    public TransfersuccessViewModel(Activity context) {
        this.context = context;
    }

    public void setIds(ActivityTransfersuccessConstrainBinding binding) {
        this.binding = binding;
    }

    public void close() {
        context.startActivity(new Intent(context, Dashboard_constrain.class));
        context.finish();
    }
}
