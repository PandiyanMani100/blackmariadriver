package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class monthtripviewpojo implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }
    public class Response {
        @SerializedName("month")
        private String month;
        @SerializedName("year")
        private String year;

        public ArrayList<Monthreport> getMonthreport() {
            return monthreport;
        }

        public void setMonthreport(ArrayList<Monthreport> monthreport) {
            this.monthreport = monthreport;
        }

        @SerializedName("monthreport")
        ArrayList < Monthreport > monthreport = new ArrayList< Monthreport >();
        @SerializedName("total_rides")
        private String total_rides;



        // Getter Methods

        public String getMonth() {
            return month;
        }

        public String getYear() {
            return year;
        }

        public String getTotal_rides() {
            return total_rides;
        }

        // Setter Methods

        public void setMonth(String month) {
            this.month = month;
        }

        public void setYear(String year) {
            this.year = year;
        }

        public void setTotal_rides(String total_rides) {
            this.total_rides = total_rides;
        }
    }

}

