package com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.earnings;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.app.DriveAndEarnStatementDownload;
import com.blackmaria.partner.databinding.ActivityRideearnConstrainBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Earnings.EarningsStatement;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

public class RideearnViewModel extends ViewModel implements ApIServices.completelisner {
    private Activity context;
    private Dialog dialog;
    private MutableLiveData<String> rideearnresponse = new MutableLiveData<>();
    private MutableLiveData<String> forgetpinresponse = new MutableLiveData<>();
    private MutableLiveData<String> withdrawalresponse = new MutableLiveData<>();
    private ActivityRideearnConstrainBinding binding;
    private String sDriverID = "";
    private SessionManager sessionManager;
    private int count = 0;

    public RideearnViewModel(Activity context) {
        this.context = context;
        sessionManager = SessionManager.getInstance(context);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
    }

    public MutableLiveData<String> getRideearnresponse() {
        return rideearnresponse;
    }

    public MutableLiveData<String> getForgetpinresponse() {
        return forgetpinresponse;
    }

    public MutableLiveData<String> getWithdrawalresponse() {
        return withdrawalresponse;
    }

    @Override
    public void sucessresponse(String val) {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        if (count == 0) {
            rideearnresponse.postValue(val);
        } else if (count == 1) {
            forgetpinresponse.postValue(val);
        } else if (count == 2) {
            withdrawalresponse.postValue(val);
        }
    }

    @Override
    public void errorreponse() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void jsonexception() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        AppUtils.toastprint(context, "Json Exception");
    }

    public void back() {
        context.onBackPressed();
    }

    public void downloadstatement(String driverImageUpdate, String referal_code, String download_url){
        Intent intent = new Intent(context, DriveAndEarnStatementDownload.class);
        intent.putExtra("driverImage", driverImageUpdate);
        intent.putExtra("referralCode", referal_code);
        intent.putExtra("downloadLink", download_url);
        context.startActivity(intent);
        context.overridePendingTransition(R.anim.enter, R.anim.exit);
        if (EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);}
    }

    public void rideearnapihit() {
        count = 0;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        ApIServices apIServices = new ApIServices(context, RideearnViewModel.this);
        apIServices.dooperation(Iconstant.drive_and_earn_statement_url, jsonParams);
    }

    public void forgetpinapihit() {
        count = 1;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        ApIServices apIServices = new ApIServices(context, RideearnViewModel.this);
        apIServices.dooperation(Iconstant.forgot_pin_url, jsonParams);
    }

    public void withdrawnapihit(String available_amount) {
        count = 2;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        jsonParams.put("amount", available_amount);
        jsonParams.put("mode", "wallet");
        ApIServices apIServices = new ApIServices(context, RideearnViewModel.this);
        apIServices.dooperation(Iconstant.referral_withdraw_amount_url, jsonParams);
    }


    public void setIds(ActivityRideearnConstrainBinding binding) {
        this.binding = binding;
        rideearnapihit();
    }

    public void today() {
        Intent intenttoday = new Intent(context, EarningsStatement.class);
        intenttoday.putExtra("statement", "1");
        context.startActivity(intenttoday);
        if (EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);}
    }

    public void week() {
        Intent intenttoday = new Intent(context, EarningsStatement.class);
        intenttoday.putExtra("statement", "2");
        context.startActivity(intenttoday);
        if (EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);}
    }

    public void month() {
        Intent intenttoday = new Intent(context, EarningsStatement.class);
        intenttoday.putExtra("statement", "3");
        context.startActivity(intenttoday);
        if (EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);}
    }


}
