package com.blackmaria.partner.latestpackageview.view.signup;

import android.app.Activity;
import androidx.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import androidx.databinding.DataBindingUtil;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.databinding.RegisterVehicleDetailsConstrainBinding;
import com.blackmaria.partner.latestpackageview.Factory.SignUp.RegistervehicledetailsFactory;
import com.blackmaria.partner.latestpackageview.vieewmodel.signup.RegistervehicledetailsViewModel;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.yalantis.ucrop.UCrop;

import java.util.Calendar;
import java.util.HashMap;

public class RegistervehicledetailsConstrain extends AppCompatActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener, RegistervehicledetailsViewModel.registerclickreturn {

    private RegisterVehicleDetailsConstrainBinding binding;
    private RegistervehicledetailsViewModel registervehicledetailsViewModel;
    private static final int PERMISSION_REQUEST_CODE = 111;
    private static final int SELECT_IMAGE_REQUEST = 011;
    private static final int TAKE_PHOTO_REQUEST = 022;
    private int year, month, day, pickerNumber = 0;
    private Calendar calendar;
    private DatePickerDialog pickerDialog;
    private String strRoadTax = "", strLicenseTax = "", strInsuranceExp = "";
    int typeofimage = 0;
    AccessLanguagefromlocaldb db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
        binding = DataBindingUtil.setContentView(this, R.layout.register_vehicle_details_constrain);
        registervehicledetailsViewModel = ViewModelProviders.of(this, new RegistervehicledetailsFactory(this)).get(RegistervehicledetailsViewModel.class);
        binding.setRegistervehicledetailsViewModel(registervehicledetailsViewModel);

        registervehicledetailsViewModel.setIds(binding);
        registervehicledetailsViewModel.setregisterclickreturn(this);

        binding.roadtax.setOnClickListener(this);
        binding.drivinglicense.setOnClickListener(this);
        binding.insurance.setOnClickListener(this);
        binding.imgBack.setOnClickListener(this);

        binding.personalinfo.setText(db.getvalue("vehicleinfo"));
        binding.exp.setText(db.getvalue("expiry_date"));
        binding.vehiclename.setText(db.getvalue("vehicle_photo_n_upload"));
        binding.roadtax.setHint(db.getvalue("roadtax"));
        binding.insurance.setHint(db.getvalue("insurance"));
        binding.drivinglicense.setHint(db.getvalue("driving_license"));
        binding.platenumber.setHint(db.getvalue("plate_number"));
        binding.vehicletypee.setText(db.getvalue("vehicle_type"));
        binding.vehimaker.setText(db.getvalue("vehicle_maker"));
        binding.txtvehicletype.setText(db.getvalue("select"));
        binding.txtvehiclemaker.setText(db.getvalue("select"));

        binding.txtvehiclmodel.setText(db.getvalue("vehicle_model"));
        binding.modelnotlisted.setHint(db.getvalue("type_model_if_not_listed"));
        binding.yma.setText(db.getvalue("manufacturer"));
        binding.txtyears.setText(db.getvalue("select"));
        binding.btnConfirm.setText(db.getvalue("register"));

        binding.attachroadtax.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                typeofimage  =1;
                registervehicledetailsViewModel.uploadpohoto(typeofimage);
            }
        });

        binding.attachinsurance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                typeofimage  =2;
                registervehicledetailsViewModel.uploadpohoto(typeofimage);
            }
        });

        binding.drivinglicsenseimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                typeofimage  =3;
                registervehicledetailsViewModel.uploadpohoto(typeofimage);
            }
        });

        binding.vehiclename.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                typeofimage  =0;
                registervehicledetailsViewModel.uploadpohoto(typeofimage);
            }
        });


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // If permissionCode is 0 -> Its For Camera
                    // If permissionCode is 1 -> Its For Pick From Gallery
                    registervehicledetailsViewModel.uploadpohoto(typeofimage);
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_IMAGE_REQUEST) {
                registervehicledetailsViewModel.onSelectFromGalleryResult(data);
            } else if (requestCode == TAKE_PHOTO_REQUEST) {
                registervehicledetailsViewModel.onCaptureImageResult(data);
            } else if (requestCode == UCrop.REQUEST_CROP) {
                registervehicledetailsViewModel.onCroppedImageResult(data);
            }
        } else if (resultCode == UCrop.RESULT_ERROR) {
            final Throwable cropError = UCrop.getError(data);
        }
    }

    private void showDatePicker() {

        pickerDialog = DatePickerDialog.newInstance(this, year, month, day);
        pickerDialog.setThemeDark(false);
        pickerDialog.showYearPickerFirst(false);
        pickerDialog.setAccentColor(getResources().getColor(R.color.signin_and_signup_slider_ball_blue));
        pickerDialog.setMinDate(Calendar.getInstance());
        pickerDialog.show(getFragmentManager(), "DatePickerDialog");
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int day) {
        datePickerDialog.dismiss();
        int months = month + 1;

        this.year = year;
        this.month = month;
        this.day = day;

        if (pickerNumber == 1) {
            binding.roadtax.setText(day + "-" + months + "-" + year);
            strRoadTax = day + "-" + months + "-" + year;
        } else if (pickerNumber == 2) {
            binding.drivinglicense.setText(day + "-" + months + "-" + year);
            strLicenseTax = day + "-" + months + "-" + year;
        } else if (pickerNumber == 3) {
            binding.insurance.setText(day + "-" + months + "-" + year);
            strInsuranceExp = day + "-" + months + "-" + year;
        }
    }

    @Override
    public void onClick(View view) {
        if (view == binding.roadtax) {
            CloseKeyboardNew();
            pickerNumber = 1;
            showDatePicker();
        } else if (view == binding.drivinglicense) {
            CloseKeyboardNew();
            pickerNumber = 2;
            showDatePicker();
        } else if (view == binding.insurance) {
            CloseKeyboardNew();
            pickerNumber = 3;
            showDatePicker();
        } else if (view == binding.imgBack) {
            onBackPressed();

        }
    }

    private void CloseKeyboardNew() {
        try {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow((null == getCurrentFocus()) ? null : getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void movetonextscreen(HashMap<String, String> jsonParams) {
        Intent i = new Intent(this, VerifyPhoneno_Constrain.class);
        i.putExtra("hashMap", jsonParams);
        startActivity(i);
    }
}
