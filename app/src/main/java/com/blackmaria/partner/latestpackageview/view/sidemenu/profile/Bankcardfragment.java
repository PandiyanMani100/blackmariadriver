package com.blackmaria.partner.latestpackageview.view.sidemenu.profile;

import android.annotation.SuppressLint;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.BankcardviewpagerlayoutBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.Factory.Sidemenu.Profile.BankFragmentFactory;
import com.blackmaria.partner.latestpackageview.Model.profilepojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.vieewmodel.sidemenu.profile.BankcardviewpagerViewModel;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

@SuppressLint("ValidFragment")
public class Bankcardfragment extends Fragment {

    private BankcardviewpagerlayoutBinding binding;
    private BankcardviewpagerViewModel bankcardviewpagerViewModel;
    private JSONObject jsonObj;
    private AppUtils appUtils;
    private profilepojo pojo;
    private String driverid = "", driverimage = "", globalchangedemail = "";
    private SessionManager sessionManager;
    private int countapi = 0;
    AccessLanguagefromlocaldb db;
    public Bankcardfragment(JSONObject jsonObj) {
        this.jsonObj = jsonObj;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.bankcardviewpagerlayout, container, false);

        db = new AccessLanguagefromlocaldb(getActivity());
        View view = binding.getRoot();
        bankcardviewpagerViewModel = ViewModelProviders.of(this, new BankFragmentFactory(getActivity())).get(BankcardviewpagerViewModel.class);
        binding.setBankcardviewpagerViewModel(bankcardviewpagerViewModel);
        bankcardviewpagerViewModel.setIds(binding);
        Type type = new TypeToken<profilepojo>() {
        }.getType();
        pojo = new GsonBuilder().create().fromJson(jsonObj.toString(), type);
        initView();
        clicklistener();
        setresponse();
        binding.myprofiletile.setText(db.getvalue("myaccountdetails"));
        binding.accde.setText(db.getvalue("account_details"));
        binding.editaccount.setText(db.getvalue("add_bank"));
        binding.bankname.setHint(db.getvalue("bank_name"));
        binding.etaccountnumber.setHint(db.getvalue("account_number"));
        binding.accounttype.setHint(db.getvalue("account_holder_name"));
        binding.updateaccount.setText(db.getvalue("update_bank"));

        return view;
    }


    private void initView() {
        appUtils = AppUtils.getInstance(getActivity());
        sessionManager = SessionManager.getInstance(getActivity());
        HashMap<String, String> user = sessionManager.getUserDetails();
        driverid = user.get(SessionManager.KEY_DRIVERID);
        driverimage = user.get(SessionManager.KEY_DRIVER_IMAGE);
        try {
            if (jsonObj.getJSONObject("response").getJSONObject("bank_details").length()>0) {
                binding.editaccount.setText("+Edit Bank");
                binding.etaccountnumber.setText(pojo.getResponse().getBank_detailsobj().getAcc_number());
                binding.accounttype.setText(pojo.getResponse().getBank_detailsobj().getAcc_holder_name());
                binding.bankname.setText(pojo.getResponse().getBank_detailsobj().getBank_name());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        binding.etaccountnumber.setEnabled(false);
        binding.accounttype.setEnabled(false);
        binding.bankname.setEnabled(false);
        binding.updateaccount.setVisibility(View.INVISIBLE);
    }

    private void setresponse() {
        bankcardviewpagerViewModel.getUpdatebankdetailsresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                if (countapi == 1) {
                    countapi = 0;
                    try {
                        JSONObject object = new JSONObject(response);
                        if (object.getString("status").equalsIgnoreCase("1")) {
                            String message = object.getString("message");
                            binding.etaccountnumber.setEnabled(false);
                            binding.accounttype.setEnabled(false);
                            binding.bankname.setEnabled(false);
                            binding.updateaccount.setVisibility(View.INVISIBLE);
                            appUtils.AlertSuccess(getActivity(),db.getvalue("action_success"), message);
                        } else {
                            String message = object.getString("message");
                            appUtils.AlertError(getActivity(),db.getvalue("action_error"), message);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }


    private void clicklistener() {

        binding.updateaccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.bankname.getText().toString().trim().length() == 0) {
                    appUtils.AlertError(getActivity(), db.getvalue("action_error"),db.getvalue("banknamecantempty"));
                } else if (binding.accounttype.getText().toString().trim().length() == 0) {
                    appUtils.AlertError(getActivity(), db.getvalue("action_error"), db.getvalue("valid_card_holder_name"));
                } else if (binding.etaccountnumber.getText().toString().trim().length() == 0) {
                    appUtils.AlertError(getActivity(), db.getvalue("action_error"),db.getvalue("valid_account_no"));
                } else {
                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    jsonParams.put("driver_id", driverid);
                    jsonParams.put("acc_holder_name", binding.accounttype.getText().toString().trim());
                    jsonParams.put("acc_holder_address", "");
                    jsonParams.put("acc_number", binding.etaccountnumber.getText().toString().trim());
                    jsonParams.put("city", "");
                    jsonParams.put("country", "");
                    jsonParams.put("swift_code", "");
                    jsonParams.put("bank_name", binding.bankname.getText().toString().trim());
                    jsonParams.put("routing_number", "");
                    jsonParams.put("iban_number", "");
                    countapi = 1;
                    bankcardviewpagerViewModel.updatebankdetails(Iconstant.Bank_account_Save_Url, jsonParams);
                }
            }
        });
        binding.editaccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.editaccount.setText("+Add Bank");
                binding.etaccountnumber.setEnabled(true);
                binding.accounttype.setEnabled(true);
                binding.bankname.setEnabled(true);
                binding.updateaccount.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
