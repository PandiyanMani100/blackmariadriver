package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

public class  Monthreport{
    @SerializedName("week")
    public String week;
    @SerializedName("WeekCount")
    public String WeekCount;
    @SerializedName("ridecount")
    public String ridecount;
    @SerializedName("distance")
    public String distance;
    @SerializedName("income")
    public String income;
    @SerializedName("currency")
    public String currency;
    @SerializedName("startdate")
    public String startdate;
    @SerializedName("endate")
    public String endate;
    @SerializedName("startdate_f")
    public String startdate_f;
    @SerializedName("endate_f")
    public String endate_f;

    public String getWeekdesc() {
        return weekdesc;
    }

    public void setWeekdesc(String weekdesc) {
        this.weekdesc = weekdesc;
    }

    @SerializedName("weekdesc")
    public String weekdesc;


    // Getter Methods

    public String getWeek() {
        return week;
    }

    public String getWeekCount() {
        return WeekCount;
    }

    public String getRidecount() {
        return ridecount;
    }

    public String getDistance() {
        return distance;
    }

    public String getIncome() {
        return income;
    }

    public String getCurrency() {
        return currency;
    }

    public String getStartdate() {
        return startdate;
    }

    public String getEndate() {
        return endate;
    }

    public String getStartdate_f() {
        return startdate_f;
    }

    public String getEndate_f() {
        return endate_f;
    }

    // Setter Methods

    public void setWeek(String week) {
        this.week = week;
    }

    public void setWeekCount(String WeekCount) {
        this.WeekCount = WeekCount;
    }

    public void setRidecount(String ridecount) {
        this.ridecount = ridecount;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public void setEndate(String endate) {
        this.endate = endate;
    }

    public void setStartdate_f(String startdate_f) {
        this.startdate_f = startdate_f;
    }

    public void setEndate_f(String endate_f) {
        this.endate_f = endate_f;
    }
}
