package com.blackmaria.partner.latestpackageview.Utils;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.latestpackageview.Model.InfoWindowData;
import com.blackmaria.partner.latestpackageview.Model.withdrawalspaymentspojo;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

public class CustomInfoWindowGoogleMap implements GoogleMap.InfoWindowAdapter {

    private Context context;
    private withdrawalspaymentspojo.Response.Withdraw_location withdraw_location;

    public CustomInfoWindowGoogleMap(Context ctx, withdrawalspaymentspojo.Response.Withdraw_location withdraw_location){
        context = ctx;
        this.withdraw_location = withdraw_location;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View view = ((Activity)context).getLayoutInflater()
                .inflate(R.layout.custommarker, null);

        TextView name_tv = view.findViewById(R.id.title);
        TextView value = view.findViewById(R.id.value);
//        InfoWindowData infoWindowData = (InfoWindowData) marker.getTag();

        name_tv.setText(withdraw_location.getName());
        value.setText(withdraw_location.getLocation_address());

        return view;
    }
}
