package com.blackmaria.partner.latestpackageview.Factory.Dashboard.Wallet.Withdraw;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet.withdraw.WithdrawpaymentschooseViewModel;


public class WithdrawpaymentschooseFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public WithdrawpaymentschooseFactory(Activity context) {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new WithdrawpaymentschooseViewModel(context);
    }
}
