package com.blackmaria.partner.latestpackageview.vieewmodel.sidemenu.maintanence;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.blackmaria.partner.R;
import com.blackmaria.partner.databinding.ActivityMaintanenceopendashboard1Binding;
import com.blackmaria.partner.databinding.ActivityMaintanenceopendashboardBinding;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;

import java.util.HashMap;

public class MaintanenceopendashboardViewModel1 extends ViewModel implements ApIServices.completelisner {

    private Activity context;
    private Dialog dialog;
    private ActivityMaintanenceopendashboard1Binding binding;
    private MutableLiveData<String> getdriverdetailsresponse = new MutableLiveData<>();

    public MaintanenceopendashboardViewModel1(Activity context) {
        this.context = context;
    }

    public MutableLiveData<String> getGetdriverdetailsresponse() {
        return getdriverdetailsresponse;
    }

    public void getdriverdtails(String url, HashMap<String, String> jsonParams) {
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ApIServices apIServices = new ApIServices(context, MaintanenceopendashboardViewModel1.this);
        apIServices.dooperation(url, jsonParams);
    }

    @Override
    public void sucessresponse(String val) {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        getdriverdetailsresponse.postValue(val);
    }

    @Override
    public void errorreponse() {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
    }

    @Override
    public void jsonexception() {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        AppUtils.toastprint(context, "Json Exception");
    }

    public void back() {
        context.onBackPressed();
    }

    public void changevehicle() {

    }


    public void setIds(ActivityMaintanenceopendashboard1Binding binding) {
        this.binding = binding;
    }
}
