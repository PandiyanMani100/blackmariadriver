package com.blackmaria.partner.latestpackageview.view.Dashboard.Incentives;


import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityMyincentivesConstrainBinding;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Incentives.MyincentivesFactory;
import com.blackmaria.partner.latestpackageview.Model.myincentivespojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.incentives.MyincentivesViewModel;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

public class MyincentivesConstrain extends AppCompatActivity {

    private ActivityMyincentivesConstrainBinding binding;
    private MyincentivesViewModel myincentivesViewModel;
    private SessionManager sessionManager;
    private String sDriverID = "";
    private AppUtils appUtils;
    AccessLanguagefromlocaldb db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
        binding = DataBindingUtil.setContentView(MyincentivesConstrain.this, R.layout.activity_myincentives_constrain);
        myincentivesViewModel = ViewModelProviders.of(this, new MyincentivesFactory(this)).get(MyincentivesViewModel.class);
        binding.setMyincentivesViewModel(myincentivesViewModel);
        myincentivesViewModel.setIds(binding);

        initView();
        binding.hightlights.setText(db.getvalue("hightlights"));
        binding.myincentives.setText(db.getvalue("myincentives"));
        binding.noincentives.setText(db.getvalue("noincentives"));
        binding.estimatedincetivestodatetxt.setText(db.getvalue("estimatedincetivestodate"));
        binding.contentincentives.setText(db.getvalue("contentincentives"));
    }

    private void initView() {
        sessionManager = SessionManager.getInstance(this);
        appUtils = AppUtils.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        myincentivesViewModel.myincentivelist(jsonParams);
        setRespons();
    }

    private void setRespons() {
        myincentivesViewModel.getMyincentiveresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Type type = new TypeToken<myincentivespojo>() {
                    }.getType();
                    myincentivespojo pojo = new GsonBuilder().create().fromJson(jsonObject.toString(), type);
                    setText(pojo);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setText(myincentivespojo pojo) {
        binding.myincentivesdate.setText(pojo.getResponse().getDate());
        binding.hightlights.setText(pojo.getResponse().getHighlight_string());
        binding.estimatedincetivestodateamount.setText(pojo.getResponse().getCurrency() + "" + pojo.getResponse().getTotal_amount());
        seticentiveslist(pojo);
    }

    public void seticentiveslist(final myincentivespojo responsetoday) {
        try {
            binding.targetlist.removeAllViews();
            if (responsetoday.getResponse().getIncetives().size() > 0) {
                binding.vertical.setVisibility(View.VISIBLE);
                binding.noincentives.setVisibility(View.GONE);
            } else {
                binding.vertical.setVisibility(View.INVISIBLE);
                binding.noincentives.setVisibility(View.VISIBLE);
            }
        } catch (NullPointerException e) {
        }
        for (int i = 0; i <= responsetoday.getResponse().getIncetives().size() - 1; i++) {
            View view = getLayoutInflater().inflate(R.layout.myincentiveadapterlayout, null);
            LinearLayout layout = view.findViewById(R.id.layout);
            TextView type = view.findViewById(R.id.type);
            TextView value = view.findViewById(R.id.value);
            type.setText(responsetoday.getResponse().getIncetives().get(i).getName());
            value.setText(responsetoday.getResponse().getCurrency() + "" + responsetoday.getResponse().getIncetives().get(i).getAmount());
            if ((i % 2) == 0) {
                layout.setBackgroundColor(getResources().getColor(R.color.registeration_bottom_layout_bg));
            } else {
                layout.setBackgroundColor(getResources().getColor(R.color.white));
            }
            binding.targetlist.addView(view);
        }
    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }


}
