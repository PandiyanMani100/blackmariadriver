package com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.earnings;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityEarningsdashboardBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Earnings.EarningsStatement;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Earnings.RideearnConstrain;

import java.util.HashMap;

public class EarningsdashboardViewModel extends ViewModel implements ApIServices.completelisner {

    private Activity context;
    private ActivityEarningsdashboardBinding binding;
    private Dialog dialog;
    private String sDriverID = "";
    private SessionManager sessionManager;
    private MutableLiveData<String> earningsresponse = new MutableLiveData<>();
    private MutableLiveData<String> forgetpinresponse = new MutableLiveData<>();
    private MutableLiveData<String> withdrawalresponse = new MutableLiveData<>();
    private MutableLiveData<String> withdrawalamounttowallet = new MutableLiveData<>();
    private MutableLiveData<String> paymenttypes = new MutableLiveData<>();
    private int count = 0;

    public EarningsdashboardViewModel(Activity context) {
        this.context = context;
        sessionManager = SessionManager.getInstance(context);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
    }

    public void setIds(ActivityEarningsdashboardBinding binding) {
        this.binding = binding;
        earningsdasboardapihit();
    }

    public MutableLiveData<String> getEarningsresponse() {
        return earningsresponse;
    }

    public MutableLiveData<String> getForgetpinresponse() {
        return forgetpinresponse;
    }

    public MutableLiveData<String> getWithdrawalresponse() {
        return withdrawalresponse;
    }

    public MutableLiveData<String> getWithdrawalamounttowallet() {
        return withdrawalamounttowallet;
    }

    public MutableLiveData<String> getPaymenttypes() {
        return paymenttypes;
    }

    public void withdrawnamouttowallet(String available_amount) {
        count = 3;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        jsonParams.put("amount", available_amount);
        jsonParams.put("mode", "wallet");
        ApIServices apIServices = new ApIServices(context, EarningsdashboardViewModel.this);
        apIServices.dooperation(Iconstant.earnings_withdraw_amount_url, jsonParams);
    }

    public void forgetpinapihit() {
        count = 1;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        ApIServices apIServices = new ApIServices(context, EarningsdashboardViewModel.this);
        apIServices.dooperation(Iconstant.forgot_pin_url, jsonParams);
    }

    public void withdrawnapihit() {
        count = 2;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        ApIServices apIServices = new ApIServices(context, EarningsdashboardViewModel.this);
        apIServices.dooperation(Iconstant.withdrawal_statement_url, jsonParams);
    }

    public void earningsdasboardapihit() {
        count = 0;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        ApIServices apIServices = new ApIServices(context, EarningsdashboardViewModel.this);
        apIServices.dooperation(Iconstant.earnings_dashboard_url, jsonParams);
    }

    public void choosepaymenttype() {
        count = 4;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        ApIServices apIServices = new ApIServices(context, EarningsdashboardViewModel.this);
        apIServices.dooperation(Iconstant.earnings_withdraw_url, jsonParams);
    }

    @Override
    public void sucessresponse(String val) {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        if (count == 0) {
            earningsresponse.postValue(val);
        } else if (count == 1) {
            forgetpinresponse.postValue(val);
        } else if (count == 2) {
            withdrawalresponse.postValue(val);
        } else if (count == 3) {
            withdrawalamounttowallet.postValue(val);
        } else if (count == 4) {
            paymenttypes.postValue(val);
        }

    }

    @Override
    public void errorreponse() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void jsonexception() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        AppUtils.toastprint(context, "Json Exception");
    }

    public void back() {
        context.onBackPressed();
    }

    public void driveandearn() {
        context.startActivity(new Intent(context, RideearnConstrain.class));
    }


    public void today() {
        Intent intenttoday = new Intent(context, EarningsStatement.class);
        intenttoday.putExtra("statement", "1");
        context.startActivity(intenttoday);
    }

    public void week() {
        Intent intenttoday = new Intent(context, EarningsStatement.class);
        intenttoday.putExtra("statement", "2");
        context.startActivity(intenttoday);
    }

    public void month() {
        Intent intenttoday = new Intent(context, EarningsStatement.class);
        intenttoday.putExtra("statement", "3");
        context.startActivity(intenttoday);
    }
}

