package com.blackmaria.partner.latestpackageview.Xmpp;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.text.format.DateFormat;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.blackmaria.partner.Pojo.relaodpage;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.app.AdsPage;
import com.blackmaria.partner.app.GoOnlinePage;
import com.blackmaria.partner.app.NewTripPage;
import com.blackmaria.partner.app.PushNotificationAlert;
import com.blackmaria.partner.app.WaitingTimePage;
import com.blackmaria.partner.app.chathandlingintentservice;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.RidedbApiIntentService;
import com.blackmaria.partner.latestpackageview.Database.GEODBHelper;
import com.blackmaria.partner.latestpackageview.Model.rideidpojo;
import com.blackmaria.partner.latestpackageview.Model.riderequestpojo;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Tracking.StartTrip;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;
import org.jivesoftware.smack.packet.Message;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.net.URLDecoder;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by user88 on 11/4/2015.
 */
public class ChatHandler {

    private Context context;
    private GEODBHelper myDBHelper;
    private SessionManager sessionManager;
    private Bitmap bitmap;
    private NotificationManagerCompat notificationManagerCompat;
    private static final String CHANNEL_ID = "channel_id";

    public ChatHandler(Context mcontext) {
        context = mcontext;
        myDBHelper = new GEODBHelper(context);
        sessionManager = new SessionManager(context);
        notificationManagerCompat = NotificationManagerCompat.from(context);
        bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.app_icon);
    }

    public void onHandleChatMessage(Message message) {
        try {
            String data = URLDecoder.decode(message.getBody(), "UTF-8");
            System.out.println("-------------Driver Xmpp response---------------------" + data);
            JSONObject messageObject = new JSONObject(data);
            String action = (String) messageObject.get(Iconstant.ACTION_LABEL);

              if (Iconstant.ACTION_TAG_CHATMESSAGE.equalsIgnoreCase(action)) {
                if (messageObject.has("message"))
                {
                    if(messageObject.getString("message").startsWith("http"))
                    {
                        dummytest("You have received new image");
                    }
                    else
                    {
                        dummytest(messageObject.getString("message"));
                    }

                }
            }


            if(sessionManager.getModeupdate().equalsIgnoreCase("unavailable"))
            {
                if (messageObject.has("action"))
                {
                    if (Iconstant.ACTION_TAG_RIDE_CANCELLED.equalsIgnoreCase(action)) {
                        dummytest("Ride has been cancelled for rideid "+messageObject.getString("key1"));
                    }



                    else if (Iconstant.ACTION_TAG_RIDE_CANCELLED.equalsIgnoreCase(action)) {
                        dummytest("Ride has been cancelled for rideid "+messageObject.getString("key1"));
                    }
                    else if (Iconstant.ACTION_TAG_RIDE_REQUEST.equalsIgnoreCase(action)) {
                        Type type = new TypeToken<riderequestpojo>() {
                        }.getType();
                        riderequestpojo object = new GsonBuilder().create().fromJson(messageObject.toString(), type);
                        EventBus.getDefault().post(object);
                    } else if (Iconstant.ACTION_TAG_RECEIVE_CASH.equalsIgnoreCase(action)) {
                        if (messageObject.has("message"))
                        {
                            dummytest(messageObject.getString("message"));
                        }
                    } else if (Iconstant.ACTION_TAG_PAYMENT_PAID.equalsIgnoreCase(action)) {
                        if (messageObject.has("message"))
                        {
                            dummytest(messageObject.getString("message"));
                        }
                    } else if (Iconstant.ACTION_TAG_NEW_TRIP.equalsIgnoreCase(action)) {
                        newTipAlert(messageObject);
                    } else if (Iconstant.pushNotification_Ads.equalsIgnoreCase(action)) {
                        if (messageObject.has("message"))
                        {
                            dummytest(messageObject.getString("message"));
                        }
                    } else if (Iconstant.ACTION_TAG_COMPLEMENTARY.equalsIgnoreCase(action)) {
                        if (messageObject.has("message"))
                        {
                            dummytest(messageObject.getString("message"));
                        }
                        complementary(messageObject);
                    } else if (Iconstant.ACTION_TAG_REFERRAL_CREDIT.equalsIgnoreCase(action)) {
                        if (messageObject.has("message"))
                        {
                            dummytest(messageObject.getString("message"));
                        }
                    } else if (Iconstant.ACTION_WALLET_SUCCESS.equalsIgnoreCase(action)) {
                        if (messageObject.has("message"))
                        {
                            dummytest(messageObject.getString("message"));
                        }
                    } else if (Iconstant.ACTION_BANK_SUCCESS.equalsIgnoreCase(action)) {
                        if (messageObject.has("message"))
                        {
                            dummytest(messageObject.getString("message"));
                        }
                    } else if (action.equalsIgnoreCase(Iconstant.ACTION_TAG_WEEK_TRIP) || action.equalsIgnoreCase(Iconstant.ACTION_TAG_DAILY_TRIP)
                            || action.equalsIgnoreCase(Iconstant.ACTION_TAG_DAILY_MILE) || action.equalsIgnoreCase(Iconstant.ACTION_TAG_SCHEDULE_INCENTIVE)) {
                        if (messageObject.has("message"))
                        {
                            dummytest(messageObject.getString("message"));
                        }
                    } else if (Iconstant.ACTION_TAG_WALLET_CREDIT.equalsIgnoreCase(action)) {
                        if (messageObject.has("message"))
                        {
                            dummytest(messageObject.getString("message"));
                        }
                    } else if (Iconstant.userEndTrip.equalsIgnoreCase(action)) {
                        if (messageObject.has("message"))
                        {
                            dummytest(messageObject.getString("message"));
                        }
                    } else if (Iconstant.userRidePaymentTrip.equalsIgnoreCase(action)) {
                        if (messageObject.has("message"))
                        {
                            dummytest(messageObject.getString("message"));
                        }
                    }
                    else
                    {
                        if (!Iconstant.ACTION_TAG_CHATMESSAGE.equalsIgnoreCase(action))
                        {
                            if (messageObject.has("message"))
                            {
                                dummytest(messageObject.getString("message"));
                            }
                        }

                    }

                }
            }




            if (Iconstant.ACTION_TAG_RIDE_CANCELLED.equalsIgnoreCase(action)) {
                rideCancelled(messageObject);
            }

            else if (Iconstant.ACTION_TAG_CHATMESSAGE.equalsIgnoreCase(action)) {
                chatmessage(messageObject);
            }
           else if (Iconstant.ACTION_TAG_RIDE_CANCELLED.equalsIgnoreCase(action)) {
                rideCancelled(messageObject);
            }
            else if (Iconstant.ACTION_TAG_RIDE_REQUEST.equalsIgnoreCase(action)) {
                Type type = new TypeToken<riderequestpojo>() {
                }.getType();
                riderequestpojo object = new GsonBuilder().create().fromJson(messageObject.toString(), type);
                EventBus.getDefault().post(object);
            } else if (Iconstant.ACTION_TAG_RECEIVE_CASH.equalsIgnoreCase(action)) {
                receiveCash(messageObject);
            } else if (Iconstant.ACTION_TAG_PAYMENT_PAID.equalsIgnoreCase(action)) {
                paymentPaid(messageObject);
            } else if (Iconstant.ACTION_TAG_NEW_TRIP.equalsIgnoreCase(action)) {
                newTipAlert(messageObject);
            } else if (Iconstant.pushNotification_Ads.equalsIgnoreCase(action)) {
                display_Ads(messageObject);
            } else if (Iconstant.ACTION_TAG_COMPLEMENTARY.equalsIgnoreCase(action)) {
                complementary(messageObject);
            } else if (Iconstant.ACTION_TAG_REFERRAL_CREDIT.equalsIgnoreCase(action)) {
                display_AdsReferralCredit(messageObject);
            } else if (Iconstant.ACTION_WALLET_SUCCESS.equalsIgnoreCase(action)) {
                walletSuccess(messageObject);
            } else if (Iconstant.ACTION_BANK_SUCCESS.equalsIgnoreCase(action)) {
                bankSuccess(messageObject);
            } else if (action.equalsIgnoreCase(Iconstant.ACTION_TAG_WEEK_TRIP) || action.equalsIgnoreCase(Iconstant.ACTION_TAG_DAILY_TRIP)
                    || action.equalsIgnoreCase(Iconstant.ACTION_TAG_DAILY_MILE) || action.equalsIgnoreCase(Iconstant.ACTION_TAG_SCHEDULE_INCENTIVE)) {
                incentivesAchieved(messageObject);
            } else if (Iconstant.ACTION_TAG_WALLET_CREDIT.equalsIgnoreCase(action)) {
                walletCredit(messageObject);
            } else if (Iconstant.userEndTrip.equalsIgnoreCase(action)) {
                RideCompletedByUser(messageObject);
            } else if (Iconstant.userRidePaymentTrip.equalsIgnoreCase(action)) {
                RideCompletedByUserPaymentCompleted(messageObject);
            }
            else
            {
                if(messageObject.has("action"))
                {
                    if(messageObject.getString("action").equals("dectar_chat"))
                    {

                    }
                    else
                    {
                        if (messageObject.has("message"))
                        {
                            showmessagealonepge(messageObject);
                        }
                    }
                }
            }

        } catch (Exception e) {
            System.out.print(".................." + e.getMessage());
        }
    }


    @SuppressWarnings("deprecation")
    private void dummytest(String msg)
    {

        NotificationManager nm = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);

        Resources res = context.getResources();
        Notification.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder = new Notification.Builder(context, "5");
            builder
                    .setSmallIcon(R.drawable.app_icon)
                    .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.app_icon))
                    .setTicker(msg)
                    .setWhen(System.currentTimeMillis())
                    .setAutoCancel(true)
                    .setContentTitle(context.getResources().getString(R.string.app_name))
                    .setLights(0xffff0000, 100, 2000)
                    .setPriority(Notification.DEFAULT_SOUND)
                    .setContentText(msg);
        } else {
            builder = new Notification.Builder(context);
            builder
                    .setSmallIcon(R.drawable.app_icon)
                    .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.app_icon))
                    .setTicker(msg)
                    .setWhen(System.currentTimeMillis())
                    .setAutoCancel(true)
                    .setContentTitle(context.getResources().getString(R.string.app_name))
                    .setLights(0xffff0000, 100, 2000)
                    .setPriority(Notification.DEFAULT_SOUND)
                    .setContentText(msg);
        }


        NotificationManager notificationmanager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("5",
                    context.getResources().getString(R.string.app_name),
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationmanager.createNotificationChannel(channel);
        }
        notificationmanager.notify(5, builder.build());
    }

    private void RideCompletedByUser(JSONObject messageObject) throws Exception {
        Intent intent = new Intent(context, PushNotificationAlert.class);
        intent.putExtra("RideId", messageObject.getString("key1"));
        intent.putExtra("driverId", messageObject.getString("key2"));
        intent.putExtra("Action", messageObject.getString("action"));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    private void RideCompletedByUserPaymentCompleted(JSONObject messageObject) throws Exception {
        Intent intent = new Intent(context, PushNotificationAlert.class);
        intent.putExtra("RideId", messageObject.getString("key1"));
        intent.putExtra("driverId", messageObject.getString("key2"));
        intent.putExtra("Action", messageObject.getString("action"));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    private void complementary(JSONObject messageObject) throws Exception {
        sessionManager.setWaitingtime_rideid(messageObject.getString("key1"));
        Type type = new TypeToken<rideidpojo>() {
        }.getType();
        rideidpojo pojo = new GsonBuilder().create().fromJson(messageObject.toString(), type);
        EventBus.getDefault().post(pojo);

        Intent intents = new Intent(context, WaitingTimePage.class);
        intents.putExtra("rideID", messageObject.getString("key1"));
        intents.putExtra("isContinue", false);
        intents.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intents);
    }

    private void rideCancelled(JSONObject messageObject) throws Exception {
        HashMap<String, String> user = sessionManager.getUserDetails();
        String sDriverID = user.get(SessionManager.KEY_DRIVERID);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        jsonParams.put("ride_id", messageObject.getString("key1"));

        Intent intents = new Intent(context, RidedbApiIntentService.class);
        intents.putExtra("driverid", sDriverID);
        intents.putExtra("isdrivercancel", "false");
        intents.putExtra("rideid", messageObject.getString("key1"));
        intents.putExtra("params", jsonParams);
        intents.putExtra("url", Iconstant.complete_trip_detail_url);
        context.startService(intents);

        myDBHelper.insertDriverStatus("service_stop");
        sessionManager.setUpdateLocationServiceState("");
        Intent intent = new Intent(context, PushNotificationAlert.class);
        intent.putExtra("MessagePage", messageObject.getString("message"));
        intent.putExtra("Action", messageObject.getString("action"));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    private void receiveCash(JSONObject messageObject) throws Exception {

        Intent intent = new Intent(context, PushNotificationAlert.class);
        intent.putExtra("MessagePage", messageObject.getString("message"));
        intent.putExtra("Action", messageObject.getString("action"));
        intent.putExtra("amount", messageObject.getString("key3"));
        intent.putExtra("RideId", messageObject.getString("key1"));
        intent.putExtra("Currencycode", messageObject.getString("key4"));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    private void incentivesAchieved(JSONObject messageObject) throws Exception {
        Intent intent = new Intent(context, PushNotificationAlert.class);
        intent.putExtra("MessagePage", messageObject.getString("message"));
        intent.putExtra("Action", messageObject.getString("action"));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    private void paymentPaid(JSONObject messageObject) throws Exception {

        Intent intentRefresh = new Intent();
        intentRefresh.setAction("com.app.RefreshFarePage");
        context.sendBroadcast(intentRefresh);

        Intent intent = new Intent(context, PushNotificationAlert.class);
        intent.putExtra("MessagePage", messageObject.getString("message"));
        intent.putExtra("Action", messageObject.getString("action"));
        intent.putExtra("RideId", messageObject.getString("key1"));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }


    private void showmessagealonepge(JSONObject messageObject) throws Exception {

        Intent intentRefresh = new Intent();
        intentRefresh.setAction("com.app.RefreshFarePage");
        context.sendBroadcast(intentRefresh);

        Intent intent = new Intent(context, PushNotificationAlert.class);
        intent.putExtra("MessagePage", messageObject.getString("message"));
        intent.putExtra("Action", messageObject.getString("action"));
        intent.putExtra("RideId","");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    private void newTipAlert(JSONObject messageObject) throws Exception {
        Intent intent = new Intent(context, NewTripPage.class);
        intent.putExtra("NewTrip", messageObject.getString("message"));
        intent.putExtra("MessagePage", messageObject.getString("message"));
        intent.putExtra("Action", messageObject.getString("action"));
        intent.putExtra("Username", messageObject.getString("key1"));
        intent.putExtra("Mobilenumber", messageObject.getString("key3"));
        intent.putExtra("UserImage", messageObject.getString("key4"));
        intent.putExtra("RideId", messageObject.getString("key6"));
        intent.putExtra("UserPickuplocation", messageObject.getString("key7"));
        intent.putExtra("UserPickupTime", messageObject.getString("key10"));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);

    }

    private void display_Ads(JSONObject messageObject) throws Exception {

        Intent i1 = new Intent(context, AdsPage.class);
        i1.putExtra("AdsTitle", messageObject.getString(Iconstant.Ads_title));
        i1.putExtra("AdsMessage", messageObject.getString(Iconstant.Ads_Message));
        if (messageObject.has(Iconstant.Ads_image)) {
            i1.putExtra("AdsBanner", messageObject.getString(Iconstant.Ads_image));
        }
        i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i1);

        relaodpage obj = new relaodpage();
        obj.setIstrue(true);
        EventBus.getDefault().post(obj);
    }

    private void display_AdsReferralCredit(JSONObject messageObject) throws Exception {
        Intent i1 = new Intent(context, AdsPage.class);
        i1.putExtra("AdsTitle", context.getResources().getString(R.string.label_referral_credit));
        i1.putExtra("AdsMessage", messageObject.getString(Iconstant.Referral_Message));
        if (messageObject.has(Iconstant.Ads_image)) {
            i1.putExtra("AdsBanner", messageObject.getString(Iconstant.Ads_image));
        }
        i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i1);
    }


    private void walletSuccess(JSONObject messageObject) throws Exception {
        Intent intent = new Intent(context, PushNotificationAlert.class);
        intent.putExtra("Action", messageObject.getString("action"));
        intent.putExtra("MessagePage", messageObject.getString("message"));

        intent.putExtra("user_id", messageObject.getString("key1"));
        intent.putExtra("Amount", messageObject.getString("key4"));
        intent.putExtra("Currencycode", messageObject.getString("key5"));
        intent.putExtra("payment_via", messageObject.getString("key2"));
        intent.putExtra("from_number", messageObject.getString("key3"));

        intent.putExtra("from", messageObject.getString("key6"));
        intent.putExtra("date", messageObject.getString("key7"));
        intent.putExtra("time", messageObject.getString("key8"));
        intent.putExtra("trans_id", messageObject.getString("key9"));


        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }


    private void walletCredit(JSONObject messageObject) throws JSONException {
        Intent i1 = new Intent(context, PushNotificationAlert.class);
        i1.putExtra("Action", messageObject.getString("action"));
        i1.putExtra("MessagePage", messageObject.getString("message"));


        i1.putExtra("user_id", messageObject.getString("key1"));
        i1.putExtra("Amount", messageObject.getString("key2"));
        i1.putExtra("Currencycode", messageObject.getString("key3"));
        i1.putExtra("paymenttype", messageObject.getString("key4"));
        i1.putExtra("payment_via", messageObject.getString("key5"));
        i1.putExtra("from_number", messageObject.getString("key6"));
        i1.putExtra("from", messageObject.getString("key7"));
        i1.putExtra("date", messageObject.getString("key8"));
        i1.putExtra("time", messageObject.getString("key9"));
        i1.putExtra("trans_id", messageObject.getString("key10"));

        i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i1);
    }

    private void bankSuccess(JSONObject messageObject) throws Exception {
        Intent intent = new Intent(context, PushNotificationAlert.class);
        intent.putExtra("Action", messageObject.getString("action"));
        intent.putExtra("MessagePage", messageObject.getString("message"));


        intent.putExtra("user_id", messageObject.getString("key1"));
        intent.putExtra("Amount", messageObject.getString("key5"));
        intent.putExtra("Currencycode", messageObject.getString("key6"));
        intent.putExtra("paymenttype", messageObject.getString("key2"));
        intent.putExtra("payment_via", messageObject.getString("key3"));
        intent.putExtra("from_number", messageObject.getString("key4"));

        intent.putExtra("from", messageObject.getString("key7"));
        intent.putExtra("date", messageObject.getString("key8"));
        intent.putExtra("time", messageObject.getString("key9"));
        intent.putExtra("trans_id", messageObject.getString("key10"));


        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    private void chatmessage(JSONObject response)
    {
        Intent intent1 = new Intent(context, chathandlingintentservice.class);
        try {
            intent1.putExtra("desc", response.getString("message"));
            intent1.putExtra("sender_ID", response.getString("senderID"));
            intent1.putExtra("timestamp", response.getString("timeStamp"));
            intent1.putExtra("ride_id", response.getString("rideID"));
            if(response.has("datetime"))
            {
                intent1.putExtra("datetime", response.getString("datetime"));
            }
            else
            {
                intent1.putExtra("datetime", getDate(Long.parseLong(response.getString("timeStamp"))));
            }
            context.startService(intent1);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    private String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time * 1000);
        String date = DateFormat.format("dd MMM yy hh:mm a", cal).toString();
        return date;
    }

}

