package com.blackmaria.partner.latestpackageview.view.Dashboard.Earnings;


import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.databinding.ActivityEarningsStatementBinding;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Earnings.EarningsStatementFactory;
import com.blackmaria.partner.latestpackageview.Model.dayearningspojo;
import com.blackmaria.partner.latestpackageview.Model.monthearningspojo;
import com.blackmaria.partner.latestpackageview.Model.weekearningstrippojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Mytrips.TripSummaryConstrain;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.earnings.EarningsStatmentViewModel;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;

public class EarningsStatement extends AppCompatActivity {

    private ActivityEarningsStatementBinding binding;
    private EarningsStatmentViewModel earningsStatmentViewModel;
    private int count = 0;
    private AppUtils appUtils;
    AccessLanguagefromlocaldb db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
        binding = DataBindingUtil.setContentView(EarningsStatement.this, R.layout.activity_earnings_statement);
        earningsStatmentViewModel = ViewModelProviders.of(this, new EarningsStatementFactory(this)).get(EarningsStatmentViewModel.class);
//        binding.setEarningsStatmentViewModel(earningsStatmentViewModel);
        appUtils = AppUtils.getInstance(this);
        earningsStatmentViewModel.setIds(binding);

        binding.earningstitle.setText(db.getvalue("earningsstatement"));
        binding.norides.setText(db.getvalue("no_rides_available"));

        binding.leftarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                earningsStatmentViewModel.EarningsStatement_today();
            }
        });
        binding.backimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                earningsStatmentViewModel.back();
            }
        });
        binding.leftarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                earningsStatmentViewModel.left();
            }
        });
        binding.rightarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                earningsStatmentViewModel.right();
            }
        });

        getresponse();

        if (getIntent().getStringExtra("statement").equalsIgnoreCase("1")) {
            if(getIntent().hasExtra("day")){
                earningsStatmentViewModel.EarningsStatement_today(getIntent().getStringExtra("day"));
            }else{
                earningsStatmentViewModel.EarningsStatement_today();
            }
        } else if (getIntent().getStringExtra("statement").equalsIgnoreCase("2")) {
            if(getIntent().hasExtra("start")){
                earningsStatmentViewModel.Earningstatement_week(getIntent().getStringExtra("start"),getIntent().getStringExtra("end"));
            }else{
                earningsStatmentViewModel.Earningstatement_week();
            }

        } else if (getIntent().getStringExtra("statement").equalsIgnoreCase("3")) {
            earningsStatmentViewModel.Earningsstatement_month();
        }
    }

    private void getresponse() {
        earningsStatmentViewModel.getStatementrespones_today().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    count = 0;
                    JSONObject jsonObject = new JSONObject(response);
                    Type type = new TypeToken<dayearningspojo>() {
                    }.getType();
                    dayearningspojo dayearningspojo = new GsonBuilder().create().fromJson(jsonObject.toString(), type);
                    setResponsetoday(dayearningspojo);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        earningsStatmentViewModel.getStatementrespones_week().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    count = 1;
                    binding.earningstitletoday.setText("THIS WEEK");
                    JSONObject jsonObject = new JSONObject(response);
                    Type type = new TypeToken<weekearningstrippojo>() {
                    }.getType();
                    weekearningstrippojo weekearningstrippojo = new GsonBuilder().create().fromJson(jsonObject.toString(), type);
                    setResponseweek(weekearningstrippojo);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        earningsStatmentViewModel.getStatementrespones_month().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    count = 2;
                    binding.earningstitletoday.setText("THIS MONTH");
                    JSONObject jsonObject = new JSONObject(response);
                    Type type = new TypeToken<monthearningspojo>() {
                    }.getType();
                    monthearningspojo monthearningspojo = new GsonBuilder().create().fromJson(jsonObject.toString(), type);
                    setResponsemonth(monthearningspojo);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public void setResponsetoday(final dayearningspojo responsetoday) {
        try {
            binding.linearearningsstatement.removeAllViews();
        } catch (NullPointerException e) {
        }
        if (responsetoday.getResponse().getRides().size() > 0) {
            binding.scroll.setVisibility(View.VISIBLE);
            binding.norides.setVisibility(View.GONE);
        } else {
            binding.scroll.setVisibility(View.GONE);
            binding.norides.setVisibility(View.VISIBLE);
            binding.norides.setText("No Rides Available");

        }
        binding.earningstitletoday.setText(responsetoday.getResponse().getFilter_date());
        for (int i = 0; i <= responsetoday.getResponse().getRides().size() - 1; i++) {
            View view = getLayoutInflater().inflate(R.layout.statementadapterconstrain, null);
            TextView date = view.findViewById(R.id.date);
            final TextView rideid = view.findViewById(R.id.rideid);
            TextView amount = view.findViewById(R.id.amount);
            date.setText(responsetoday.getResponse().getRides().get(i).getRide_time());
            rideid.setText("CRN NO :" + responsetoday.getResponse().getRides().get(i).getRide_id());
            amount.setText(responsetoday.getResponse().getRides().get(i).getCurrency() + responsetoday.getResponse().getRides().get(i).getDriver_earning());
            ConstraintLayout views = view.findViewById(R.id.view);
            final int finalI = i;
            views.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!responsetoday.getResponse().getRides().get(finalI).getRide_id().equalsIgnoreCase("")){
                        Intent rideid = new Intent(view.getContext(), TripSummaryConstrain.class);
                        rideid.putExtra("rideid",responsetoday.getResponse().getRides().get(finalI).getRide_id());
                        startActivity(rideid);
                    }else{
                        appUtils.AlertError(EarningsStatement.this,getResources().getString(R.string.action_error),"No Rides Available");
                    }
                }
            });
            binding.linearearningsstatement.addView(view);
        }
    }

    public void setResponseweek(final weekearningstrippojo responsetoday) {
        try {
            binding.linearearningsstatement.removeAllViews();
        } catch (NullPointerException e) {
        }
        if (responsetoday.getResponse().getWeek().size() > 0) {
            binding.scroll.setVisibility(View.VISIBLE);
            binding.norides.setVisibility(View.GONE);
        } else {
            binding.scroll.setVisibility(View.GONE);
            binding.norides.setVisibility(View.VISIBLE);
            binding.norides.setText("No Rides Available");

        }
        binding.earningstitletoday.setText(responsetoday.getResponse().getDate_desc());
        for (int i = 0; i <= responsetoday.getResponse().getWeek().size() - 1; i++) {
            View view = getLayoutInflater().inflate(R.layout.statementadapterconstrain, null);
            TextView date = view.findViewById(R.id.date);
            TextView rideid = view.findViewById(R.id.rideid);
            TextView amount = view.findViewById(R.id.amount);
            date.setText(responsetoday.getResponse().getWeek().get(i).getDay());
            if(responsetoday.getResponse().getWeek().get(i).getRidecount().equalsIgnoreCase("-")){
                rideid.setText(responsetoday.getResponse().getWeek().get(i).getRidecount() + "No Trips");
            }else{
                rideid.setText(responsetoday.getResponse().getWeek().get(i).getRidecount() + "Trips");
            }
            amount.setText(responsetoday.getResponse().getWeek().get(i).getCurrency() + responsetoday.getResponse().getWeek().get(i).getDriver_earning());
            ConstraintLayout views = view.findViewById(R.id.view);
            final int finalI = i;
            views.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!responsetoday.getResponse().getWeek().get(finalI).getRidecount().equalsIgnoreCase("-")) {
                        Intent intenttoday = new Intent(EarningsStatement.this, EarningsStatement.class);
                        intenttoday.putExtra("statement", "1");
                        intenttoday.putExtra("day", responsetoday.getResponse().getWeek().get(finalI).getDate_f());
                        startActivity(intenttoday);
                    }else{
                        appUtils.AlertError(EarningsStatement.this,getResources().getString(R.string.action_error),"No Rides Available");
                    }
                }
            });
            binding.linearearningsstatement.addView(view);
        }
    }

    public void setResponsemonth(final monthearningspojo responsetoday) {
        try {
            binding.linearearningsstatement.removeAllViews();
        } catch (NullPointerException e) {
        }
        if (responsetoday.getResponse().getMonthreport().size() > 0) {
            binding.scroll.setVisibility(View.VISIBLE);
            binding.norides.setVisibility(View.GONE);
        } else {
            binding.scroll.setVisibility(View.GONE);
            binding.norides.setVisibility(View.VISIBLE);
            binding.norides.setText("No Rides Available");
        }
        binding.earningstitletoday.setText(responsetoday.getResponse().getMonth()+"/"+responsetoday.getResponse().getYear());
        for (int i = 0; i <= responsetoday.getResponse().getMonthreport().size() - 1; i++) {
            View view = getLayoutInflater().inflate(R.layout.statementadapterconstrain, null);
            TextView date = view.findViewById(R.id.date);
            TextView rideid = view.findViewById(R.id.rideid);
            TextView amount = view.findViewById(R.id.amount);
            date.setText(responsetoday.getResponse().getMonthreport().get(i).getWeek() + " " + responsetoday.getResponse().getMonthreport().get(i).getWeekCount());
            if(responsetoday.getResponse().getMonthreport().get(i).getRidecount().equalsIgnoreCase("-")){
                rideid.setText(responsetoday.getResponse().getMonthreport().get(i).getRidecount() + "No Trips");
            }else{
                rideid.setText(responsetoday.getResponse().getMonthreport().get(i).getRidecount() + "Trips");
            }
            amount.setText(responsetoday.getResponse().getMonthreport().get(i).getCurrency() + responsetoday.getResponse().getMonthreport().get(i).getIncome());
            ConstraintLayout views = view.findViewById(R.id.view);
            final int finalI = i;
            views.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!responsetoday.getResponse().getMonthreport().get(finalI).getRidecount().equalsIgnoreCase("-")) {
                        Intent intenttoday = new Intent(EarningsStatement.this, EarningsStatement.class);
                        intenttoday.putExtra("statement", "2");
                        intenttoday.putExtra("start", responsetoday.getResponse().getMonthreport().get(finalI).getStartdate_f());
                        intenttoday.putExtra("end", responsetoday.getResponse().getMonthreport().get(finalI).getEndate_f());
                        startActivity(intenttoday);
                    }else{
                        appUtils.AlertError(EarningsStatement.this,getResources().getString(R.string.action_error),"No Rides Available");
                    }

                }
            });
            binding.linearearningsstatement.addView(view);
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }

}
