package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Fastpayhome_pojo implements Serializable {
    @SerializedName("status")
    private String status;

    @SerializedName("response")
    Response ResponseObject;


    // Getter Methods 

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods 

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }

    public class Response {
        @SerializedName("currency")
        private String currency;
        @SerializedName("text")
        private String text;
        @SerializedName("currency_balance")
        private String currency_balance;
        @SerializedName("total_transaction")
        private String total_transaction;
        @SerializedName("total")
        Total TotalObject;


        // Getter Methods 

        public String getCurrency() {
            return currency;
        }

        public String getText() {
            return text;
        }

        public String getCurrency_balance() {
            return currency_balance;
        }

        public String getTotal_transaction() {
            return total_transaction;
        }

        public Total getTotal() {
            return TotalObject;
        }

        // Setter Methods 

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public void setText(String text) {
            this.text = text;
        }

        public void setCurrency_balance(String currency_balance) {
            this.currency_balance = currency_balance;
        }

        public void setTotal_transaction(String total_transaction) {
            this.total_transaction = total_transaction;
        }

        public void setTotal(Total totalObject) {
            this.TotalObject = totalObject;
        }

        public class Total {
            @SerializedName("wallet_credit")
            private String wallet_credit;
            @SerializedName("wallet_spend")
            private String wallet_spend;
            @SerializedName("payment")
            private String payment;
            @SerializedName("transfer")
            private String transfer;
            @SerializedName("withdrawal")
            private String withdrawal;


            // Getter Methods 

            public String getWallet_credit() {
                return wallet_credit;
            }

            public String getWallet_spend() {
                return wallet_spend;
            }

            public String getPayment() {
                return payment;
            }

            public String getTransfer() {
                return transfer;
            }

            public String getWithdrawal() {
                return withdrawal;
            }

            // Setter Methods 

            public void setWallet_credit(String wallet_credit) {
                this.wallet_credit = wallet_credit;
            }

            public void setWallet_spend(String wallet_spend) {
                this.wallet_spend = wallet_spend;
            }

            public void setPayment(String payment) {
                this.payment = payment;
            }

            public void setTransfer(String transfer) {
                this.transfer = transfer;
            }

            public void setWithdrawal(String withdrawal) {
                this.withdrawal = withdrawal;
            }
        }
    }
}


