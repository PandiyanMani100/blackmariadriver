package com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.tracking;

import android.app.Activity;
import androidx.lifecycle.ViewModel;

import com.blackmaria.partner.databinding.ActivityMultistopviewallBinding;

public class MultistopviewViewModel extends ViewModel {

    private Activity cotext;
    private ActivityMultistopviewallBinding binding;

    public MultistopviewViewModel(Activity cotext) {
        this.cotext = cotext;
    }

    public void setIds(ActivityMultistopviewallBinding binding) {
        this.binding = binding;
    }
}
