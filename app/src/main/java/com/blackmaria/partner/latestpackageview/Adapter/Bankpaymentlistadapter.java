package com.blackmaria.partner.latestpackageview.Adapter;

import android.content.Context;
import android.os.Parcelable;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.blackmaria.partner.Interface.Slider;
import com.blackmaria.partner.R;
import com.blackmaria.partner.latestpackageview.Model.bankpaymentlistpojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;

import java.util.ArrayList;

public class Bankpaymentlistadapter extends PagerAdapter {


    ArrayList<bankpaymentlistpojo.Bank_list> bank_list;
    private LayoutInflater inflater;
    private Context context;
    private Slider slider;


    public Bankpaymentlistadapter(Slider slider, Context context, ArrayList<bankpaymentlistpojo.Bank_list> bank_list) {
        this.slider = slider;
        this.context = context;
        this.bank_list = bank_list;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return bank_list.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View imageLayout = inflater.inflate(R.layout.bankpaymenticon, view, false);

        assert imageLayout != null;
        final ImageView imageView = imageLayout.findViewById(R.id.image);
        AppUtils.setImageviewwithoutcropplaceholder(context, bank_list.get(position).getImage(), imageView);
        view.addView(imageLayout, 0);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                slider.onComplete(position);
            }
        });
        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


}