package com.blackmaria.partner.latestpackageview.vieewmodel.signup;

import android.app.Activity;
import androidx.lifecycle.ViewModel;

import android.widget.ImageView;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;

import java.util.HashMap;

public class RegistrationTermsViewModel extends ViewModel {

    private Activity context;
    private SessionManager sessionManager;
    private ConnectionDetector cd;
    private String sDriverID = "";

    private ImageView RL_exit;
    private TextView Btn_registerNow;

    public RegistrationTermsViewModel(Activity context) {
        this.context = context;
        sessionManager = SessionManager.getInstance(context);
        cd = new ConnectionDetector(context);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
    }

    public void backclick() {
        context.finish();
        context.overridePendingTransition(R.anim.enter, R.anim.exit);
    }


}
