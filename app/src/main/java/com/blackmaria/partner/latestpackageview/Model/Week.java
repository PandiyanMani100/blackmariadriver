package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

public class Week{
    @SerializedName("day")
    private String day;
    @SerializedName("ridecount")
    private String ridecount;
    @SerializedName("distance")
    private String distance;
    @SerializedName("driver_earning")
    private String driver_earning;
    @SerializedName("currency")
    private String currency;
    @SerializedName("order")
    private String order;
    @SerializedName("date_f")
    private String date_f;


    // Getter Methods

    public String getDay() {
        return day;
    }

    public String getRidecount() {
        return ridecount;
    }

    public String getDistance() {
        return distance;
    }

    public String getDriver_earning() {
        return driver_earning;
    }

    public String getCurrency() {
        return currency;
    }

    public String getOrder() {
        return order;
    }

    public String getDate_f() {
        return date_f;
    }

    // Setter Methods

    public void setDay(String day) {
        this.day = day;
    }

    public void setRidecount(String ridecount) {
        this.ridecount = ridecount;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public void setDriver_earning(String driver_earning) {
        this.driver_earning = driver_earning;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public void setDate_f(String date_f) {
        this.date_f = date_f;
    }
}