package com.blackmaria.partner.latestpackageview.view.Dashboard;


import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.android.volley.Request;
import com.blackmaria.partner.Pojo.relaodpage;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.GPSTracker;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.app.ComplaintPage;
import com.blackmaria.partner.databinding.FragmentDashboardBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.MytripsdbApiIntentService;
import com.blackmaria.partner.latestpackageview.ApiRequest.OnlineupdatelocationtoserverService;
import com.blackmaria.partner.latestpackageview.Database.GEODBHelper;
import com.blackmaria.partner.latestpackageview.Model.clickdisabledashboardpojo;
import com.blackmaria.partner.latestpackageview.Model.driverdashboad;
import com.blackmaria.partner.latestpackageview.Model.onlineupdatelocation;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.DashboardFragmentViewModel;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Earnings.Earningsdashboardconstrain;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Incentives.IncentivedashbaordConstrain;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Message.MessageConstrain;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Mytrips.MytripshomepageConstrain;
import com.blackmaria.partner.latestpackageview.view.Dashboard.SOS.Sosdragmap_screen;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Tracking.TrackArrive;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Confirmpatern_activity;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Fastpayhome;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Patternscreen;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.blackmaria.partner.mylibrary.volley.ServiceRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.HashMap;


public class DashboardFragment extends Fragment {

    private DashboardFragmentViewModel dashboardFragmentViewModel;
    public static FragmentDashboardBinding binding;
    private AppUtils appUtils;
    private driverdashboad object;
    private SessionManager sessionManager;
    private String sDriverID = "";
    private GEODBHelper myDBHelper;
    private Activity context;

    private Dialog dialog;
    private GPSTracker gps;
    AccessLanguagefromlocaldb db;
    private ImageView imghome;
    private boolean ismovedtoonline = false;

    public DashboardFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        EventBus.getDefault().register(this);
        // Inflate the layout for this fragment

        EventBus.getDefault().register(this);
        db = new AccessLanguagefromlocaldb(getActivity());

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_dashboard, container, false);
        View view = binding.getRoot();
        dashboardFragmentViewModel = ViewModelProviders.of(this).get(DashboardFragmentViewModel.class);
        binding.setDashboardFragmentViewModel(dashboardFragmentViewModel);
        dashboardFragmentViewModel.setIds(binding);



        context = getActivity();
        initView();
        Apicalldriversettings();
        imghome = view.findViewById(R.id.imghome);
        settingsresponse();

        binding.drivenowtxt.setText(db.getvalue("drivenow"));
        binding.mytripstxt.setText(db.getvalue("mytrips"));
        binding.earningstxt.setText(db.getvalue("earnings"));
        binding.incentivestxt.setText(db.getvalue("incentives"));
        binding.emer.setText(db.getvalue("home_emergency"));
        binding.ass.setText(db.getvalue("home_complaint"));
        binding.messa.setText(db.getvalue("home_message"));

        imghome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                opendrawer();
            }
        });

        binding.rlSaveplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openwallet();
            }
        });
        binding.rlWallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openmytrips();
            }
        });
        binding.rlTrips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openearnings();
            }
        });
        binding.rlKm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openincentives();
            }
        });
        binding.rlSos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                opensos();
            }
        });
        binding.rlComplaint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openassistance();
            }
        });
        binding.rlMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openmessage();
            }
        });

        return view;
    }

    public void opendrawer() {
        Dashboard_constrain.openDrawer();
    }

    public void openwallet() {
        updatepattern(Iconstant.getpattern);
    }

    private void updatepattern(String Url) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> info = sessionManager.getUserDetails();
        String UserID = info.get(SessionManager.KEY_DRIVERID);


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", UserID);


        ServiceRequest mRequest = new ServiceRequest(getActivity());
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                String Sstatus = "", Smessage = "";

                try {

                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        dialog.dismiss();
                        JSONObject jsonObject = object.getJSONObject("response");
                        if (jsonObject.has("pattern_code")) {
                            sessionManager.setpattern(jsonObject.getString("pattern_code"));
                            if(!jsonObject.getString("pattern_code").equals(""))
                            {
                                Intent intent = new Intent(context, Patternscreen.class);
                                context.startActivity(intent);
                                context.overridePendingTransition(R.anim.enter, R.anim.exit);
                            }
                            else
                            {
                                Intent intent = new Intent(context, Confirmpatern_activity.class);
                                context.startActivity(intent);
                                context.overridePendingTransition(R.anim.enter, R.anim.exit);
                            }
                        }
                        else
                        {
                            Intent intent = new Intent(context, Confirmpatern_activity.class);
                            context.startActivity(intent);
                            context.overridePendingTransition(R.anim.enter, R.anim.exit);
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();
                    dialog.dismiss();
                }
            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    public void openmytrips() {
        Intent intent = new Intent(context, MytripshomepageConstrain.class);
        context.startActivity(intent);
        context.overridePendingTransition(R.anim.enter, R.anim.exit);
        EventBus.getDefault().unregister(this);
    }

    public void openearnings() {
        Intent intent = new Intent(context, Earningsdashboardconstrain.class);
        context.startActivity(intent);
        context.overridePendingTransition(R.anim.enter, R.anim.exit);
        EventBus.getDefault().unregister(this);
    }

    public void openincentives() {
        Intent intent = new Intent(context, IncentivedashbaordConstrain.class);
//        Intent intent = new Intent(context, needtest.class);
//        Intent intent = new Intent(context, IncentivesDashboard.class);
        context.startActivity(intent);
        context.overridePendingTransition(R.anim.enter, R.anim.exit);
        EventBus.getDefault().unregister(this);

    }


    public void opensos() {
        Intent intent = new Intent(context, Sosdragmap_screen.class);
        context.startActivity(intent);
        context.overridePendingTransition(R.anim.enter, R.anim.exit);
        EventBus.getDefault().unregister(this);
    }

    public void openassistance() {
        Intent intent = new Intent(context, ComplaintPage.class);
        context.startActivity(intent);
        context.overridePendingTransition(R.anim.enter, R.anim.exit);
        EventBus.getDefault().unregister(this);
    }

    public void openmessage() {
        Intent intent = new Intent(context, MessageConstrain.class);
        context.startActivity(intent);
        context.overridePendingTransition(R.anim.enter, R.anim.exit);
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void reloadimage(String obj) {
        if(obj.equals("reload"))
        {
            RequestOptions options1 = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.user_icon)
                    .error(R.drawable.user_icon)
                    .diskCacheStrategy(DiskCacheStrategy.ALL);
            Glide.with(getActivity()).load(sessionManager.getDriverImageUpdate()).apply(options1).into(binding.userimage);

        }

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getreloadpage(relaodpage obj) {
        if (obj.isIstrue()) {
            Apicalldriversettings();
            initView();
            settingsresponse();
        }
    }

    public void Apicalldriversettings() {
        gps = GPSTracker.getInstance(context);

        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        Thread thread = new Thread() {
            @Override
            public void run() {
                HashMap<String, String> jsonParams = new HashMap<String, String>();
                jsonParams.put("driver_id", sDriverID);
                jsonParams.put("mode", "get");
                jsonParams.put("lat", String.valueOf(gps.getLatitude()));
                jsonParams.put("lon", String.valueOf(gps.getLongitude()));
                dashboardFragmentViewModel.driversettings(Iconstant.driverHomePage_Url, jsonParams);

                HashMap<String, String> jsonParamsmytrip = new HashMap<String, String>();
                jsonParamsmytrip.put("driver_id", sDriverID);
                Intent intents = new Intent(getActivity(), MytripsdbApiIntentService.class);
                intents.putExtra("driverid", sDriverID);
                intents.putExtra("params", jsonParamsmytrip);
                intents.putExtra("url", Iconstant.myTrip_Url);
                getActivity().startService(intents);
            }
        };
        thread.start();



    }

    @Subscribe
    public void updatelocationtoserver(onlineupdatelocation onlineupdatelocation) {
        if (onlineupdatelocation.getStatus().equalsIgnoreCase("1")) {
            if (onlineupdatelocation.getResponse().getWallet_constraint().equalsIgnoreCase("1")) {
                if (onlineupdatelocation.getResponse().getAvailability().equalsIgnoreCase("Unavailable")) {
                    myDBHelper.insertRide_id(onlineupdatelocation.getResponse().getRide_id());
                    sessionManager.setisride(onlineupdatelocation.getResponse().getRide_id());
//                    Intent intent = new Intent(getActivity(), TrackArrive.class);
//                    intent.putExtra("rideid", onlineupdatelocation.getResponse().getRide_id());
//                    getActivity().startActivity(intent);
//                    getActivity().finish();
//                    getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                } else {
                    sessionManager.setisride("");
                }
            }
        }
    }

    private void initView() {
        sessionManager = SessionManager.getInstance(getActivity());
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        appUtils = AppUtils.getInstance(getActivity());
        myDBHelper = GEODBHelper.getInstance(getActivity());
        appUtils.loadinganimation(binding.loading);


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        jsonParams.put("latitude", String.valueOf(sessionManager.getLatitude()));
        jsonParams.put("longitude", String.valueOf(sessionManager.getLongitude()));
        Intent intents = new Intent(getActivity(), OnlineupdatelocationtoserverService.class);
        intents.putExtra("params", jsonParams);
        intents.putExtra("url", Iconstant.updateDriverLocation);
        getActivity().startService(intents);

        binding.drivenow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (object != null) {
                    if (object.getResponse().getVerify_status().equalsIgnoreCase("Yes")) {
                        if (object.getResponse().getGo_online_status().equalsIgnoreCase("1")) {
                            driveNowDialog();
                        } else {
                            showVerifyAccountDialog(db.getvalue("label_welcome_to") + object.getResponse().getCity_name(), object.getResponse().getGo_online_string());
                        }
                    } else {
                        if (object.getResponse().getGo_online_status().equalsIgnoreCase("0")) {
                            showVerifyAccountDialog(db.getvalue("action_error"), object.getResponse().getGo_online_string());
                        } else {
                            showVerifyAccountDialog(db.getvalue("action_error"), db.getvalue("driver_homepage_alert_label_verify_account"));
                        }
                    }
                }
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        RequestOptions options1 = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.car_menu)
                .error(R.drawable.user_icon)
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        Glide.with(getActivity()).load(sessionManager.getDriverImageUpdate()).apply(options1).into(binding.userimage);

    }

    private void settingsresponse() {

        dashboardFragmentViewModel.getLogoutresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                if (dialog != null) {
                    dialog.dismiss();
                }
                sessionManager.logoutUser();
            }
        });

        dashboardFragmentViewModel.getDriversetting().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    if (dialog != null) {
                        dialog.dismiss();
                    }

                    JSONObject json = new JSONObject(response);
                    if (json.has("status")) {
                        if (json.getString("status").equals("0")) {
                            appUtils.AlertError(getActivity(), db.getvalue("action_error"), json.getString("response"));
                        } else {
                            Type type = new TypeToken<driverdashboad>() {
                            }.getType();
                            object = new GsonBuilder().create().fromJson(response.toString(), type);
                            if (object.getStatus().equalsIgnoreCase("1")) {
                                sessionManager.setcategory_id(object.getResponse().getCategory_id());
                                sessionManager.setseonddriverid(object.getResponse().getAdditional_driver_id());
                                sessionManager.setUserVehicle(object.getResponse().getCategory_icon());
                                sessionManager.setaccountownertype(object.getResponse().getDriver_type());
                                sessionManager.setdrivercarnumber(object.getResponse().getVehicle_number());
                                sessionManager.setCurrency(object.getResponse().getCurrencyCode());
                                if (object.getResponse().getPending_ride_status().equalsIgnoreCase("1")) {
                                    Intent i = new Intent(getActivity(), TrackArrive.class);
                                    i.putExtra("rideid", object.getResponse().getPending_ride_id());
                                    startActivity(i);
                                    getActivity().finish();
                                    if (EventBus.getDefault().isRegistered(this)) {
                                        EventBus.getDefault().unregister(this);
                                    }
                                } else {
                                    RequestOptions options = new RequestOptions()
                                            .centerCrop()
                                            .placeholder(R.drawable.new_no_user_img)
                                            .error(R.drawable.new_no_user_img)
                                            .diskCacheStrategy(DiskCacheStrategy.ALL);
                                    Glide.with(getActivity()).load(object.getResponse().getDriver_image()).apply(options).into(binding.userimage);

                                    RequestOptions options1 = new RequestOptions()
                                            .centerCrop()
                                            .override(400, 400)
                                            .fitCenter()
                                            .placeholder(R.drawable.car_menu)
                                            .error(R.drawable.car_menu)
                                           ;
                                    sessionManager.setcategoryicon(object.getResponse().getCategory_icon());
                                    Glide.with(getActivity()).load(object.getResponse().getCategory_icon()).apply(options1).into(binding.categoryimage);

                                    binding.username.setText(object.getResponse().getDriver_name());
                                    Calendar c = Calendar.getInstance();
                                    int timeOfDay = c.get(Calendar.HOUR_OF_DAY);

                                    String greetingMsg = "";
                                    if (timeOfDay >= 0 && timeOfDay < 12) {
                                        greetingMsg = db.getvalue("driver_homepage_greeting_morning");
                                    } else if (timeOfDay >= 12 && timeOfDay < 16) {
                                        greetingMsg = db.getvalue("driver_homepage_greeting_afternoon");
                                    } else if (timeOfDay >= 16 && timeOfDay < 21) {
                                        greetingMsg = db.getvalue("driver_homepage_greeting_evening");
                                    } else if (timeOfDay >= 21 && timeOfDay < 24) {
                                        greetingMsg = db.getvalue("driver_homepage_greeting_night");
                                    }

                                    binding.usernamecars.setText(greetingMsg + ", " + object.getResponse().getDriver_name());
                                    String noOfVehicles = "";
                                    try {
                                        int vehicleCount = Integer.parseInt(object.getResponse().getNear_by_driver());
                                        if (vehicleCount == 0) {
                                            noOfVehicles =db.getvalue("home_label_no_vehicle");
                                        } else if (vehicleCount == 1) {
                                            noOfVehicles = db.getvalue("home_label_there_is") + vehicleCount + db.getvalue("home_label_vehicle") + db.getvalue("home_label_near_you");
                                        } else if (vehicleCount > 1) {
                                            noOfVehicles = db.getvalue("home_label_there_are") + vehicleCount +db.getvalue("home_label_vehicles") + db.getvalue("home_label_near_you");
                                        }

                                        binding.nearvehicles.setText(noOfVehicles);
                                    } catch (NumberFormatException e) {
                                        appUtils.toastprint(getActivity(), "Number format exception");
                                    }

                                    binding.vehicleno.setText(object.getResponse().getVehicle_number());
                                    binding.walletamountcurrency.setText(object.getResponse().getCurrencyCode());
                                    binding.walletamount.setText(object.getResponse().getCurrentbalance());
                                    binding.mytripscount.setText(object.getResponse().getTotal_completed_trip());
                                    binding.earningsamount.setText(object.getResponse().getCurrencyCode() + " " + object.getResponse().getDriver_earning());
                                    binding.incentivesamount.setText(object.getResponse().getCurrencyCode() + " " + object.getResponse().getIncetives_total());
                                    Dashboard_constrain.setTextmenu(object.getResponse().getWarning_count(), object.getResponse().getReferral_count());
                                    if (object.getResponse().getComplaint_badge().equalsIgnoreCase("0")) {
                                        binding.tvcompnoti.setVisibility(View.INVISIBLE);
                                    } else {
                                        binding.tvcompnoti.setVisibility(View.VISIBLE);
                                        binding.tvcompnoti.setText(object.getResponse().getComplaint_badge());
                                    }

                                    if (object.getResponse().getInfo_badge().equalsIgnoreCase("0")) {
                                        binding.tvmessagenoti.setVisibility(View.INVISIBLE);
                                    } else {
                                        binding.tvmessagenoti.setVisibility(View.VISIBLE);
                                        binding.tvmessagenoti.setText(object.getResponse().getInfo_badge());
                                    }

                                    if (object.getResponse().getDriver_active().equalsIgnoreCase("0")) {
                                        final PkDialog mDialog = new PkDialog(getActivity());
                                        mDialog.setDialogTitle(db.getvalue("closeaccountdashboard"));
                                        mDialog.setDialogMessage(object.getResponse().getDriver_active_message());
                                        mDialog.setPositiveButton(db.getvalue("action_ok"), new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                mDialog.dismiss();
                                                logout();
                                            }
                                        });
                                        mDialog.show();
                                    } else {
                                        if (object.getResponse().getGo_online_status().equalsIgnoreCase("1")) {
                                            if (object.getResponse().getDriver_availability().equalsIgnoreCase("Yes")) {
                                                getActivity().startActivity(new Intent(getActivity(), OnlinepageConstrain.class));
                                                EventBus.getDefault().unregister(this);
                                                getActivity().finish();

                                            }
                                        }
                                    }
                                }
                            } else {
                                appUtils.AlertError(getActivity(), db.getvalue("action_error"), json.getString("response"));
                            }
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IllegalStateException ee) {
                }
            }
        });

    }




    private void alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(getActivity());
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(db.getvalue("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    @Subscribe
    public void dashboarddraweropen(clickdisabledashboardpojo pojo) {




        if (pojo.isIsenabled()) {
            binding.drivenow.setEnabled(false);
            binding.rlSaveplus.setEnabled(false);
            binding.rlWallet.setEnabled(false);
            binding.rlTrips.setEnabled(false);
            binding.rlKm.setEnabled(false);
            binding.rlSos.setEnabled(false);
            binding.rlComplaint.setEnabled(false);
            binding.rlMessage.setEnabled(false);




        } else {
            binding.drivenow.setEnabled(true);
            binding.rlSaveplus.setEnabled(true);
            binding.rlWallet.setEnabled(true);
            binding.rlTrips.setEnabled(true);
            binding.rlKm.setEnabled(true);
            binding.rlSos.setEnabled(true);
            binding.rlComplaint.setEnabled(true);
            binding.rlMessage.setEnabled(true);

            RequestOptions options1 = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.car_menu)
                    .error(R.drawable.user_icon)
                    .diskCacheStrategy(DiskCacheStrategy.ALL);
            Glide.with(getActivity()).load(sessionManager.getDriverImageUpdate()).apply(options1).into(binding.userimage);

        }

    }

    //--------------Alert Method-----------
    private void alertcontraint(String title, String alert) {
        final PkDialog mDialog = new PkDialog(getActivity());
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(db.getvalue("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                Intent intent = new Intent(getActivity(), Fastpayhome.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.enter, R.anim.exit);
                if (EventBus.getDefault().isRegistered(this)) {
                    EventBus.getDefault().unregister(this);
                }
            }
        });
        mDialog.show();
    }

    private void showVerifyAccountDialog(String title, String message) {
        final Dialog dialog = new Dialog(getActivity());
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 85% of the screen
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_could_not_drive);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setLayout(screenWidth, LinearLayout.LayoutParams.WRAP_CONTENT);
        TextView Tv_title = (TextView) dialog.findViewById(R.id.custom_dialog_library_title_textview);
        TextView Tv_message = (TextView) dialog.findViewById(R.id.custom_dialog_library_message_textview);
        Tv_title.setText(title);
        Tv_message.setText(message);

        TextView okButton = dialog.findViewById(R.id.custom_dialog_library_ok_button);
        okButton.setText(db.getvalue("action_ok"));
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });

        dialog.show();
    }

    private void driveNowDialog() {
        final Dialog driveNowDialog = new Dialog(getActivity(), R.style.SlideUpDialog);
        driveNowDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        driveNowDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        driveNowDialog.setCancelable(true);
        driveNowDialog.setCanceledOnTouchOutside(true);
        driveNowDialog.setContentView(R.layout.alert_rules_of_road);
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 85% of the screen
        int screenHeight = (int) (metrics.heightPixels * 0.85);//fill only 85% of the screen
        driveNowDialog.getWindow().setLayout(screenWidth, screenHeight);
        final RelativeLayout RL_driveNow = (RelativeLayout) driveNowDialog.findViewById(R.id.RL_drive_now);
        final RelativeLayout RL_close = (RelativeLayout) driveNowDialog.findViewById(R.id.RL_close);
        final ProgressBar apiload = driveNowDialog.findViewById(R.id.apiload);

        final TextView txt_label_rules_of_road = driveNowDialog.findViewById(R.id.txt_label_rules_of_road);
        txt_label_rules_of_road.setText(db.getvalue("rules_of_the_road"));

        final TextView txt_content1 = driveNowDialog.findViewById(R.id.txt_content1);
        txt_content1.setText(db.getvalue("drive_now_alert_content1"));

        final TextView txt_content2 = driveNowDialog.findViewById(R.id.txt_content2);
        txt_content2.setText(db.getvalue("drive_now_alert_content2"));

        final TextView txt_label_close = driveNowDialog.findViewById(R.id.txt_label_close);
        txt_label_close.setText(db.getvalue("close_lable"));

        final TextView txt_label_drive_now = driveNowDialog.findViewById(R.id.txt_label_drive_now);
        txt_label_drive_now.setText(db.getvalue("driver_homepage_label_drive_now"));


        RL_driveNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String walletcontraint = object.getResponse().getWallet_constraint();
                String minwallet = object.getResponse().getMin_wallet_amount();
                if (walletcontraint.equalsIgnoreCase("1")) {
                    apiload.setVisibility(View.VISIBLE);
                    RL_driveNow.setClickable(false);
                    RL_close.setClickable(false);
                    updateavailablity();
                    dashboardFragmentViewModel.getAvailabilityupdate().observe(DashboardFragment.this, new Observer<String>() {
                        @Override
                        public void onChanged(@Nullable String response) {
                            apiload.setVisibility(View.GONE);
                            RL_driveNow.setClickable(true);
                            RL_close.setClickable(true);
                            driveNowDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                    getActivity().startActivity(new Intent(getActivity(), OnlinepageConstrain.class));
                                    EventBus.getDefault().unregister(this);
                                    getActivity().finish();
                                } else {
                                    appUtils.AlertError(getActivity(), db.getvalue("action_error"), jsonObject.getString("response"));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } else {
                    alertcontraint(db.getvalue("err"), db.getvalue("less") + minwallet + db.getvalue("rere"));
                }
            }
        });
        RL_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (driveNowDialog.isShowing()) {
                    driveNowDialog.dismiss();
                }
            }
        });
        driveNowDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (driveNowDialog.isShowing()) {
                    driveNowDialog.dismiss();
                }
            }
        });
        driveNowDialog.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        EventBus.getDefault().unregister(this);

        System.gc();
    }


    public void updateavailablity() {

        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        Thread thread = new Thread() {
            @Override
            public void run() {
                HashMap<String, String> jsonParams = new HashMap<String, String>();
                jsonParams.put("driver_id", sDriverID);
                jsonParams.put("availability", "Yes");
                dashboardFragmentViewModel.updateavailabilty(jsonParams);

            }
        };
        thread.start();

    }


    //    public void goonline(final String updateAvailability_url) {
//        count = 1;
//        binding.apiload.setVisibility(View.VISIBLE);
//        dialog = new Dialog(context, R.style.CustomDialogTheme);
//        dialog.getWindow();
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.loading_model);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        dialog.setCanceledOnTouchOutside(false);
//        dialog.show();
//
//        Thread thread = new Thread() {
//            @Override
//            public void run() {
//                HashMap<String, String> jsonParams = new HashMap<String, String>();
//                jsonParams.put("driver_id", sDriverID);
//                jsonParams.put("availability", "Yes");
//
//                ApIServices apIServices = new ApIServices(context, DashboardFragmentViewModel.this);
//                apIServices.dooperation(updateAvailability_url, jsonParams);
//            }
//        };
//        thread.start();
//
//    }
//
    public void logout() {

        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        Thread thread = new Thread() {
            @Override
            public void run() {
                HashMap<String, String> jsonParams = new HashMap<>();
                jsonParams.put("driver_id", sDriverID);
                jsonParams.put("device", "ANDROID");
                dashboardFragmentViewModel.logout(Iconstant.Profile_driver_account_Logout_Url, jsonParams);

            }
        };
        thread.start();

    }

}
