package com.blackmaria.partner.latestpackageview.vieewmodel.login;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.provider.Settings;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.GPSTracker;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivitySigninContrainBinding;
import com.blackmaria.partner.databinding.NewloginpageBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.login.Forgetpin;
import com.blackmaria.partner.latestpackageview.view.signup.RegistrationTerms_Constrain;


import java.util.HashMap;

public class SigninViewModel extends ViewModel implements ApIServices.completelisner, AppUtils.gcmlisntener {

    public Activity context;
    private NewloginpageBinding binding;
    private MutableLiveData<String> loginvalidation = new MutableLiveData<>();
    private MutableLiveData<String> appinfo = new MutableLiveData<>();
    private MutableLiveData<String> logincheck = new MutableLiveData<>();
    private MutableLiveData<String> setuserlocation = new MutableLiveData<>();
    AccessLanguagefromlocaldb db;
    private AppUtils appUtils;
    private Dialog dialog;
    private GPSTracker gps;
    private String SdeviceToken = "", GCM_Id = "", driver_id = "", sis_alive_other = "";
    private SessionManager sessionManager;
    private int apihitting = 0;

    public SigninViewModel(Activity context) {
        this.context = context;
        db = new AccessLanguagefromlocaldb(context);
        appUtils = AppUtils.getInstance(context);
        appUtils = new AppUtils("", SigninViewModel.this);
        appUtils.Gcminitializer(context);
        sessionManager = SessionManager.getInstance(context);
        gps = GPSTracker.getInstance(context);
        SdeviceToken = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public void setIds(NewloginpageBinding binding) {
        this.binding = binding;
    }


    public MutableLiveData<String> getLoginvalidation() {
        return loginvalidation;
    }

    public MutableLiveData<String> getAppinfo() {
        return appinfo;
    }

    public MutableLiveData<String> getLogincheck() {
        return logincheck;
    }

    public MutableLiveData<String> getSetuserlocation() {
        return setuserlocation;
    }

    public void signup() {
        context.startActivity(new Intent(context, RegistrationTerms_Constrain.class));
    }

    public void login() {
//        apihitting = 0;
        if (binding.txtcountrycode.getText().length() == 0) {
            appUtils.AlertError(context, db.getvalue("action_error"), db.getvalue("cdnotempty"));
        } else if (binding.edtmobileno.getText().length() == 0) {
            appUtils.AlertError(context, db.getvalue("action_error"), db.getvalue("mbnotempty"));
        } else if (binding.editpinnumnber.getText().length() == 0) {
            appUtils.AlertError(context, db.getvalue("action_error"),db.getvalue("pcnotempty"));
        } else if (binding.editpinnumnber.getText().length() != 6) {
            appUtils.AlertError(context, db.getvalue("action_error"), db.getvalue("minim6c"));
        } else {
//            dialog = new Dialog(context);
//            dialog.getWindow();
//            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//            dialog.setContentView(R.layout.loading_model);
//            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//            dialog.setCanceledOnTouchOutside(false);
//            dialog.show();
//
//            TextView dialog_title = (TextView) dialog.findViewById(R.id.loading_model_textview);
//
//
//            HashMap<String, String> jsonParams = new HashMap<String, String>();
//            jsonParams.put("phone_number", binding.edtmobileno.getText().toString());
//            jsonParams.put("country_code", binding.txtcountrycode.getText().toString());
//            jsonParams.put("pin_number", binding.editpinnumnber.getText().toString().trim());
//            jsonParams.put("deviceToken", SdeviceToken);
//            jsonParams.put("gcm_id", GCM_Id);
//            jsonParams.put("lat", String.valueOf(gps.getLatitude()));
//            jsonParams.put("lon", String.valueOf(gps.getLongitude()));
//
//            ApIServices apIServices = new ApIServices(context, SigninViewModel.this);
//            apIServices.dooperation(Iconstant.loginurl, jsonParams);
            logincheck(Iconstant.register_otp_url);
        }
    }

    public void appinfo() {
        apihitting = 1;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> user = sessionManager.getUserDetails();
        driver_id = user.get(SessionManager.KEY_DRIVERID);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_type", "driver");
        jsonParams.put("id", driver_id);
        jsonParams.put("lat", String.valueOf(gps.getLatitude()));
        jsonParams.put("lon", String.valueOf(gps.getLongitude()));

        ApIServices apIServices = new ApIServices(context, SigninViewModel.this);
        apIServices.Appinfo(Iconstant.getAppInfo, jsonParams);
    }

    public void forgotpin() {
        context.startActivity(new Intent(context, Forgetpin.class));
    }

    @Override
    public void sucessresponse(String val) {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        if (apihitting == 0) {
            loginvalidation.postValue(val);
        } else if (apihitting == 1) {
            appinfo.postValue(val);
        } else if (apihitting == 2) {
            logincheck.postValue(val);
        } else if (apihitting == 3) {
            setUserlocation();
        } else if (apihitting == 4) {
            setuserlocation.postValue(val);
        }
    }

    @Override
    public void errorreponse() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void jsonexception() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void getgcmvalue(String gcm) {
        GCM_Id = gcm;
    }

    public void checklogin(String status) {
        try {
            if (status.equalsIgnoreCase("no")) {
                // show privacy box
                showTermsAndConditions();
            } else {
                logincheck(Iconstant.register_otp_url);
            }
        } catch (Exception e) {

        }
    }

    private void showTermsAndConditions() {

        final Dialog termsDialog = new Dialog(context, R.style.SlideUpDialog);
        termsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        termsDialog.setContentView(R.layout.alert_terms_and_conditions);
        termsDialog.setCanceledOnTouchOutside(false);
        termsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final CheckBox Cb_termsAndConditions = (CheckBox) termsDialog.findViewById(R.id.cb_terms_and_conditions);
        final CheckBox Cb_privacyPolicy = (CheckBox) termsDialog.findViewById(R.id.cb_privacy_policy);
        final CheckBox Cb_eula = (CheckBox) termsDialog.findViewById(R.id.cb_eula);
        final TextView Tv_termsAndConditions = (TextView) termsDialog.findViewById(R.id.txt_label_terms_and_conditions);
        final TextView Tv_privacyPolicy = (TextView) termsDialog.findViewById(R.id.txt_label_privacy_policy);
        final TextView Tv_eula = (TextView) termsDialog.findViewById(R.id.txt_label_eula);
        final RelativeLayout RL_decline = (RelativeLayout) termsDialog.findViewById(R.id.Rl_decline);
        final Button Btn_agree = (Button) termsDialog.findViewById(R.id.Btn_agree);

        Tv_termsAndConditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Cb_termsAndConditions.isChecked()) {
                    Cb_termsAndConditions.setChecked(false);
                } else {
                    Cb_termsAndConditions.setChecked(true);
                }
            }
        });

        Tv_privacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Cb_privacyPolicy.isChecked()) {
                    Cb_privacyPolicy.setChecked(false);
                } else {
                    Cb_privacyPolicy.setChecked(true);
                }
            }
        });

        Tv_eula.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Cb_eula.isChecked()) {
                    Cb_eula.setChecked(false);
                } else {
                    Cb_eula.setChecked(true);
                }
            }
        });

        Btn_agree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Cb_termsAndConditions.isChecked() && Cb_privacyPolicy.isChecked() && Cb_eula.isChecked()) {
                    logincheck(Iconstant.register_otp_url);
                } else {
                    appUtils.AlertError(context,db.getvalue("alert_label_title"), db.getvalue("label_terms_and_conditions_error"));
                }
            }
        });

        RL_decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                termsDialog.dismiss();
            }
        });
        termsDialog.show();
    }


    public void logincheck(String Url) {
        apihitting = 2;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> user = sessionManager.getUserDetails();
        driver_id = user.get(SessionManager.KEY_DRIVERID);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("phone_number", binding.edtmobileno.getText().toString().trim());
        jsonParams.put("country_code", binding.txtcountrycode.getText().toString());
        jsonParams.put("pin_number", binding.editpinnumnber.getText().toString().trim());
        jsonParams.put("deviceToken", SdeviceToken);
        jsonParams.put("gcm_id", GCM_Id);
        jsonParams.put("privacy_status", "yes");
        jsonParams.put("referal_code", "");

        ApIServices apIServices = new ApIServices(context, SigninViewModel.this);
        apIServices.dooperation(Url, jsonParams);
    }

    public void Appinfosecond() {
        apihitting = 3;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        HashMap<String, String> user = sessionManager.getUserDetails();
        driver_id = user.get(SessionManager.KEY_DRIVERID);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_type", "driver");
        jsonParams.put("id", driver_id);
        jsonParams.put("lat", String.valueOf(gps.getLatitude()));
        jsonParams.put("lon", String.valueOf(gps.getLongitude()));

        ApIServices apIServices = new ApIServices(context, SigninViewModel.this);
        apIServices.Appinfo(Iconstant.getAppInfo, jsonParams);

    }

    public void setUserlocation() {
        apihitting = 4;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        HashMap<String, String> user = sessionManager.getUserDetails();
        driver_id = user.get(SessionManager.KEY_DRIVERID);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", driver_id);
        jsonParams.put("latitude", String.valueOf(gps.getLatitude()));
        jsonParams.put("longitude", String.valueOf(gps.getLongitude()));

        ApIServices apIServices = new ApIServices(context, SigninViewModel.this);
        apIServices.dooperation(Iconstant.updateDriverLocation, jsonParams);
    }
}
