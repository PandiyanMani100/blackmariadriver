package com.blackmaria.partner.latestpackageview.Factory.Sidemenu.Account;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackmaria.partner.latestpackageview.vieewmodel.sidemenu.account.MyaccountViewModel;


public class MyaccountFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public MyaccountFactory(Activity context)
    {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass)
    {
        return (T) new MyaccountViewModel(context);
    }
}
