package com.blackmaria.partner.latestpackageview.view.Dashboard.Message;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.Adapter.MessageListviewAdapter1;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.Model.MessagesPojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Dashboard_constrain;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class MessageConstrain extends AppCompatActivity implements ApIServices.completelisner, View.OnClickListener, MessageListviewAdapter1.MessageView {

    private ImageView Iv_back;
    private ConnectionDetector cd;
    private SessionManager sessionManager;
    private ServiceRequest mRequest;
    private Boolean isInternetPresent = false;
    private Dialog dialog;
    String perPage = "5";
    String mode = "";
    int selectorunselect = 0 ;
    private int currentPage = 1;
    private String UserId = "";
    private boolean isDataPresent = false;
    ArrayList<MessagesPojo> itemlist_all;
    ArrayList<MessagesPojo> itemlist_all_copy;
    String[] mStrings;
    private boolean isTransactionAvailable = false;
    MessageListviewAdapter1 adapter;
    ImageView Iv_previous, Iv_next;
    private ListView mMessageList;
    TextView empty_text, thisweek, thismonth, thisyear, selectall, unreadcount, delete;
    private String notificationId = "";
    private ProgressBar apiload;
    private int count = 0;
    private AppUtils appUtils;
    AccessLanguagefromlocaldb db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.information_page_constrain);
        db = new AccessLanguagefromlocaldb(this);
        initialize();
        Iv_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Iv_previous.setVisibility(View.VISIBLE);
                currentPage = currentPage + 1;
                postData();
            }
        });

        Iv_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                currentPage = currentPage - 1;
                postData();

            }
        });

        thisweek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                mode = "week";
                postData();
            }
        });

        thismonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                mode = "month";
                postData();
            }
        });

        thisyear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                mode = "year";
                postData();
            }
        });

        selectall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(selectorunselect == 0)
                {
                    selectall.setText("Unselect All");
                    selectorunselect =1;
                    itemlist_all_copy = new ArrayList<>();
                    itemlist_all_copy.addAll(itemlist_all);
                    itemlist_all.clear();
                    for (int i = 0; i <= itemlist_all_copy.size() - 1; i++) {
                        MessagesPojo pojo = new MessagesPojo();
                        pojo.setDate(itemlist_all_copy.get(i).getDate());
                        pojo.setIMage(itemlist_all_copy.get(i).getIMage());
                        pojo.setTitle(itemlist_all_copy.get(i).getTitle());
                        pojo.setDescription(itemlist_all_copy.get(i).getDescription());
                        pojo.setNotification_id(itemlist_all_copy.get(i).getNotification_id());
                        pojo.setIschecked(true);
                        itemlist_all.add(pojo);
                    }
                    adapter = new MessageListviewAdapter1(MessageConstrain.this, 1, itemlist_all, MessageConstrain.this);
                    mMessageList.setAdapter(adapter);
                }
                else
                {
                    selectall.setText("Select All");
                    selectorunselect = 0;
                    itemlist_all_copy = new ArrayList<>();
                    itemlist_all_copy.addAll(itemlist_all);
                    itemlist_all.clear();
                    for (int i = 0; i <= itemlist_all_copy.size() - 1; i++) {
                        MessagesPojo pojo = new MessagesPojo();
                        pojo.setDate(itemlist_all_copy.get(i).getDate());
                        pojo.setIMage(itemlist_all_copy.get(i).getIMage());
                        pojo.setTitle(itemlist_all_copy.get(i).getTitle());
                        pojo.setDescription(itemlist_all_copy.get(i).getDescription());
                        pojo.setNotification_id(itemlist_all_copy.get(i).getNotification_id());
                        pojo.setIschecked(false);
                        itemlist_all.add(pojo);
                    }
                    adapter = new MessageListviewAdapter1(MessageConstrain.this, 1, itemlist_all, MessageConstrain.this);
                    mMessageList.setAdapter(adapter);
                }

            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final PkDialog mDialog = new PkDialog(MessageConstrain.this);
                mDialog.setDialogTitle("");
                mDialog.setDialogMessage(db.getvalue("wanttodelete"));
                mDialog.setPositiveButton(db.getvalue("action_yes"), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();
                        if (itemlist_all.size() > 0) {
                            boolean isselected = false;
                            HashMap<String, String> jsonParams = new HashMap<String, String>();
                            jsonParams.put("id", UserId);
                            ArrayList<String> strings = new ArrayList<>();
                            for (int i = 0; i <= itemlist_all.size() - 1; i++) {
                                if (itemlist_all.get(i).isIschecked()) {
                                    strings.add(itemlist_all.get(i).getNotification_id());
                                    isselected = true;
                                }
                            }
                            for (int j = 0; j <= strings.size() - 1; j++) {
                                jsonParams.put("notification_id[" + j + "]", strings.get(j));
                            }
                            if (isselected) {
                                if (isInternetPresent) {
                                    postRequest_MessageUpdate(Iconstant.message_update_list, jsonParams);
                                } else {
                                    Alert(db.getvalue("action_error"), db.getvalue("alert_nointernet"));
                                }
                            } else {
                                Alert(db.getvalue("action_error"), db.getvalue("chode"));
                            }
                        }
                    }
                });
                mDialog.show();

                mDialog.setNegativeButton(db.getvalue("action_no"), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();
                    }
                });
                mDialog.show();
            }
        });

    }

    private void initialize() {
        sessionManager = SessionManager.getInstance(MessageConstrain.this);
        cd = ConnectionDetector.getInstance(MessageConstrain.this);
        isInternetPresent = cd.isConnectingToInternet();
        HashMap<String, String> user = sessionManager.getUserDetails();
        UserId = user.get(SessionManager.KEY_DRIVERID);
        itemlist_all = new ArrayList<MessagesPojo>();
        appUtils = AppUtils.getInstance(this);
        apiload = findViewById(R.id.apiload);
        empty_text = (TextView) findViewById(R.id.emptytext);
        empty_text.setText(db.getvalue("no_messages_available"));
        mMessageList = (ListView) findViewById(R.id.message_listview);
        Iv_previous = (ImageView) findViewById(R.id.img_prev);
        Iv_next = (ImageView) findViewById(R.id.img_next);
        Iv_back = (ImageView) findViewById(R.id.img_back);

        TextView txt_page_title = findViewById(R.id.txt_page_title);
        txt_page_title.setText(db.getvalue("message"));

        TextView viewmessage = findViewById(R.id.viewmessage);
        viewmessage.setText(db.getvalue("view_message"));


        thisweek = findViewById(R.id.thisweek);
        thisweek.setText(db.getvalue("this_week"));
        thismonth = findViewById(R.id.thismonth);
        thismonth.setText(db.getvalue("this_month"));
        thisyear = findViewById(R.id.thisyear);
        thisyear.setText(db.getvalue("this_year"));
        selectall = findViewById(R.id.selectall);
        selectall.setText(db.getvalue("select_all"));
        unreadcount = findViewById(R.id.unreadcount);
        delete = findViewById(R.id.delete);
        delete.setText(db.getvalue("delete"));
        Iv_back.setOnClickListener(this);

        mode = "";
    }

    @Override
    protected void onResume() {
        super.onResume();
        postData();
    }

    private void postData() {
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("id", UserId);
        jsonParams.put("type", "notification");
        jsonParams.put("page", String.valueOf(currentPage));
        jsonParams.put("perPage", perPage);
        jsonParams.put("mode", mode);
        if (isInternetPresent) {
            postRequest_walletMoney(Iconstant.mesggaeg_url, jsonParams);
        } else {
            appUtils.AlertError(this,db.getvalue("action_error"), db.getvalue("alert_nointernet"));
        }
    }


    @Override
    public void onClick(View view) {

        if (cd.isConnectingToInternet()) {

            if (view == Iv_back) {
                Intent in = new Intent(MessageConstrain.this, Dashboard_constrain.class);
                startActivity(in);
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }

        } else {
            appUtils.AlertError(this,db.getvalue("alert_nointernet"), db.getvalue("alert_nointernet_message"));
        }

    }

    private void postRequest_MessageUpdate(String Url, HashMap<String, String> jsonParams) {
        selectall.setText("Select All");
        selectorunselect = 0;
        count = 1;
        apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(this, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ApIServices apIServices = new ApIServices(this, MessageConstrain.this);
        apIServices.dooperation(Url, jsonParams);
    }

    //-----------------------wallet Money Post Request-----------------
    private void postRequest_walletMoney(String Url, HashMap<String, String> jsonParams) {
        count = 0;
        apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(this, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ApIServices apIServices = new ApIServices(this, MessageConstrain.this);
        apIServices.dooperation(Url, jsonParams);
    }


    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(MessageConstrain.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(db.getvalue("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    @Override
    public void onClickview(int position, MessagesPojo pojo) {
        Intent i = new Intent(MessageConstrain.this, Message_subpage.class);
        i.putExtra("image", itemlist_all.get(position).getIMage());
        i.putExtra("message", itemlist_all.get(position).getDescription());
        i.putExtra("id", itemlist_all.get(position).getNotification_id());
        startActivity(i);
    }

    @Override
    public void sucessresponse(String response) {
        apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        if (count == 0) {
            String Sstatus = "", Str_CurrentPage = "", str_NextPage = "", perPage = "", ScurrencySymbol = "", Str_total_amount = "", Str_total_transaction = "";
            try {
                JSONObject object = new JSONObject(response);
                Sstatus = object.getString("status");

                if (Sstatus.equalsIgnoreCase("1")) {
                    JSONObject response_object = object.getJSONObject("response");
                    itemlist_all.clear();
                    if (response_object.length() > 0) {
                        String totlanotification = response_object.getString("total_record");
                        sessionManager.setInfoBadge(totlanotification);
                        Object check_trans_object = response_object.get("notification");
                        if (check_trans_object instanceof JSONArray) {
                            JSONArray trans_array = response_object.getJSONArray("notification");
                            mStrings = new String[trans_array.length()];
                            if (trans_array.length() > 0) {
                                for (int i = 0; i < trans_array.length(); i++) {
                                    JSONObject trans_object = trans_array.getJSONObject(i);
                                    MessagesPojo pojo = new MessagesPojo();
                                    pojo.setDate(trans_object.getString("date"));
                                    pojo.setIMage(trans_object.getString("image"));
                                    pojo.setTitle(trans_object.getString("title"));
                                    pojo.setDescription(trans_object.getString("description"));
                                    pojo.setNotification_id(trans_object.getString("notification_id"));
                                    pojo.setIschecked(false);
                                    mStrings[i] = trans_object.getString("title");
                                    itemlist_all.add(pojo);
                                }
                                unreadcount.setText(String.valueOf(itemlist_all.size()));
                                isTransactionAvailable = true;
                            } else {
                                unreadcount.setText(String.valueOf(itemlist_all.size()));
                                isTransactionAvailable = false;
                                Iv_next.setVisibility(View.GONE);
                            }
                        } else {
                            isTransactionAvailable = false;
                            Iv_next.setVisibility(View.GONE);
                        }
                        currentPage = Integer.parseInt(response_object.getString("current_page"));
                        str_NextPage = response_object.getString("next_page");
                        if (str_NextPage.equalsIgnoreCase("")) {
                            Iv_next.setVisibility(View.GONE);
                        } else {
                            Iv_next.setVisibility(View.VISIBLE);
                        }
                        perPage = response_object.getString("perPage");
                    }
                }

                if (currentPage == 1) {
                    Iv_previous.setVisibility(View.GONE);
                } else {
                    Iv_previous.setVisibility(View.VISIBLE);
                }
                if (Sstatus.equalsIgnoreCase("1")) {
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                    if (isTransactionAvailable) {
                        adapter = new MessageListviewAdapter1(MessageConstrain.this, 1, itemlist_all, MessageConstrain.this);
                        mMessageList.setAdapter(adapter);
                        empty_text.setVisibility(View.GONE);
                        mMessageList.setVisibility(View.VISIBLE);
                        //  Iv_next.setVisibility(View.VISIBLE);
                    } else {
                        empty_text.setVisibility(View.VISIBLE);
                        mMessageList.setVisibility(View.GONE);
                        //  Iv_next.setVisibility(View.GONE);
                    }
                } else {
                    String Sresponse = object.getString("response");
                    appUtils.AlertSuccess(this,db.getvalue("alert_label_title"), Sresponse);
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else if (count == 1) {
            try {
                JSONObject object = new JSONObject(response);
                String Sstatus = object.getString("status");

                if (Sstatus.equalsIgnoreCase("1")) {
                    String Sresponse = object.getString("response");
                    appUtils.AlertSuccess(this, db.getvalue("action_success"), db.getvalue("messagedeleted"));
                    postData();
                } else {
                    String Sresponse = object.getString("response");
                    appUtils.AlertError(this, db.getvalue("action_success"), Sresponse);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void errorreponse() {
        apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
    }

    @Override
    public void jsonexception() {
        apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        AppUtils.toastprint(this, "Json Exception");
    }
}


