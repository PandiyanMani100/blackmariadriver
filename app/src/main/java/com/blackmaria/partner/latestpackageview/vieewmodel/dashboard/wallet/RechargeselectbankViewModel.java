package com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityRechargeselectbankConstrainBinding;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Dashboard_constrain;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.HowtopayConstrain;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;

import java.util.HashMap;

public class RechargeselectbankViewModel extends ViewModel implements ApIServices.completelisner {

    private Activity context;
    private Dialog dialog;
    private AppUtils appUtils;
    private SessionManager sessionManager;
    private String sDriverID = "";
    private int count = 0;
    private ActivityRechargeselectbankConstrainBinding binding;
    private MutableLiveData<String> bankpaymentdriverresponse = new MutableLiveData<>();

    public RechargeselectbankViewModel(Activity context) {
        this.context = context;
        sessionManager = SessionManager.getInstance(context);
        appUtils = AppUtils.getInstance(context);
    }

    public void back() {
        context.onBackPressed();
    }

    public void howtopay() {
        context.startActivity(new Intent(context, HowtopayConstrain.class));
    }

    public MutableLiveData<String> getBankpaymentdriverresponse() {
        return bankpaymentdriverresponse;
    }

    public void bankpaymentdriver(String url, HashMap<String, String> jsonParams) {
        count = 0;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        ApIServices apIServices = new ApIServices(context, RechargeselectbankViewModel.this);
        apIServices.dooperation(url, jsonParams);
    }


    @Override
    public void sucessresponse(String val) {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        bankpaymentdriverresponse.postValue(val);
    }

    @Override
    public void errorreponse() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void jsonexception() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    public void setIds(ActivityRechargeselectbankConstrainBinding binding) {
        this.binding = binding;
    }

    public void cancel() {
        final PkDialog mDialog = new PkDialog(context);
        mDialog.setDialogTitle(context.getResources().getString(R.string.action_error));
        mDialog.setDialogMessage(context.getResources().getString(R.string.canceltransation));
        mDialog.setPositiveButton(context.getResources().getString(R.string.action_yes), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                context.startActivity(new Intent(context, Dashboard_constrain.class));
                context.finish();
            }
        });
        mDialog.setNegativeButton(context.getResources().getString(R.string.action_no), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();

    }
}
