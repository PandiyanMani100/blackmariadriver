package com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.ViewModel;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityRechargecardpaymentConstrainBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.RechargepaymentsuccessConstrain;
import com.xendit.Models.Card;
import com.xendit.Xendit;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class RechargecardpaymentViewModel extends ViewModel implements ApIServices.Xenditinterface{
    private Activity context;
    private ActivityRechargecardpaymentConstrainBinding binding;
    private Xendit xendit = null;
    private SessionManager sessionManager;
    private String driverid = "", tokenid = "", authenticationid = "", PUBLISHABLE_KEY = "", CURRENCYCONVERSIONKEY_KEY = "", cardNumber = "", expMonth = "", expYear = "", insertAmount = "", currency = "";
    private AppUtils appUtils;
    private Dialog dialog;
    private int count = 0;
    private Double amount = 0.0;

    public RechargecardpaymentViewModel(Activity context) {
        this.context = context;
        appUtils = AppUtils.getInstance(context);
        sessionManager = SessionManager.getInstance(context);
        HashMap<String, String> user = sessionManager.getUserDetails();
        driverid = user.get(SessionManager.KEY_DRIVERID);
        PUBLISHABLE_KEY = sessionManager.getXenditPublicKey();
        CURRENCYCONVERSIONKEY_KEY = sessionManager.getcurrencyconversionKey();
    }

    @Override
    public void sucessresponse(String val) {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        try {
            JSONObject jsonObject = new JSONObject(val);
            if(jsonObject.getString("status").equalsIgnoreCase("0")){
                appUtils.AlertError(context,context.getResources().getString(R.string.action_error),jsonObject.getString("msg"));
            }else if(jsonObject.getString("status").equalsIgnoreCase("2")){
                appUtils.AlertError(context,context.getResources().getString(R.string.action_error),jsonObject.getString("msg"));
            }else{
                Intent intent1 = new Intent(context, RechargepaymentsuccessConstrain.class);
                intent1.putExtra("json", val);
                intent1.putExtra("paymenttype", "xtendit");
                context.startActivity(intent1);
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    @Override
    public void errorreponse() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }


    @Override
    public void Xenditerrorresponse(String val) {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        if (count == 0) {
            appUtils.AlertError(context, context.getResources().getString(R.string.alert_label_title), val);
        }

    }

    @Override
    public void Xenditsucessresponse(String val) {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        if (count == 0) {
            tokenid = val;
            amount = Double.parseDouble(insertAmount) * Double.parseDouble(CURRENCYCONVERSIONKEY_KEY);
            amount = (Double) Math.ceil(amount);
            CreateAuthenticationid(tokenid, amount,insertAmount);
        } else if (count == 1) {
            authenticationid = val;
            System.out.println("asdfas"+tokenid+" "+authenticationid);
            xendtipaymentrelaodapihit(authenticationid);
        }

    }


    public void setIds(ActivityRechargecardpaymentConstrainBinding binding) {
        this.binding = binding;
    }

    public void back() {
        context.onBackPressed();
    }

    public void spinner_month_and_year_process() {
        String[] month = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"};
        ArrayAdapter<CharSequence> monthAdapter = new ArrayAdapter<CharSequence>(context, R.layout.spinner_text, month);
        monthAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
        binding.spinner2.setAdapter(monthAdapter);
        String thisMonth = String.valueOf(Calendar.getInstance().get(Calendar.MONTH) + 1);
        if (thisMonth.length() == 1) {
            thisMonth = "0" + thisMonth;
        }
        for (int j = 0; j <= month.length; j++) {
            if (month[j].equalsIgnoreCase(thisMonth)) {
                binding.spinner2.setSelection(j);
                break;
            }
        }
        binding.spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                expMonth = parent.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        ArrayList<String> years = new ArrayList<String>();
        int thisYear = Calendar.getInstance().get(Calendar.YEAR);
        for (int i = thisYear; i <= thisYear + 40; i++) {
            years.add(Integer.toString(i));
        }
        ArrayAdapter<String> yearAdapter = new ArrayAdapter<String>(context, R.layout.spinner_text, years);
        yearAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown);
        binding.spinner3.setAdapter(yearAdapter);
        binding.spinner3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                expYear = parent.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    public void xendtipaymentrelaodapihit(String authenticationid) {
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", driverid);
        jsonParams.put("pay_amount", amount.toString());
        jsonParams.put("total_amount", insertAmount);
        jsonParams.put("token_id", tokenid);
        jsonParams.put("redeem_code", "");
        if (authenticationid.length() > 0) {
            jsonParams.put("authentication_id", authenticationid);
        }
        ApIServices apIServices = new ApIServices(context, RechargecardpaymentViewModel.this, context);
        apIServices.Xendtidooperation(Iconstant.xendit_card_pay_url, jsonParams);
    }

    public void CreateXenditToken(String cardNumber, String insertAmount, String currency) {
        count = 0;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        this.cardNumber = cardNumber;
        this.insertAmount = insertAmount;
        this.currency = currency;
        xendit = new Xendit(context, PUBLISHABLE_KEY);
        final boolean isMultipleUse = true;
        boolean shouldAuthenticate = true;

        Card card = new Card(cardNumber,
                expMonth,
                expYear,
                binding.cvvedt.getText().toString());

        ApIServices apIServices = new ApIServices(context, RechargecardpaymentViewModel.this, context);
        apIServices.CreateXenditToken(card, isMultipleUse, xendit, shouldAuthenticate, insertAmount, CURRENCYCONVERSIONKEY_KEY);

    }

    public void CreateAuthenticationid(String tokenids, Double amount,String insertAmount) {
        tokenid = tokenids;
        this.insertAmount = insertAmount;
        this.amount = amount;
        xendit = new Xendit(context, PUBLISHABLE_KEY);
        count = 1;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        ApIServices apIServices = new ApIServices(context, RechargecardpaymentViewModel.this, context);
        apIServices.CreateAuthenticationId(tokenid, xendit, amount);
    }


}
