package com.blackmaria.partner.latestpackageview.Database

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import android.text.format.DateFormat
import com.blackmaria.partner.Utils.SessionManager
import com.blackmaria.partner.latestpackageview.Model.*
import com.blackmaria.partner.latestpackageview.Utils.AppUtils
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class RidesDB(context: Context) {

    private val DATABASE_VERSION = 1
    private val DATABASE_NAME = "ridesblackmaria.db"
    private var context: Context? = null
    lateinit var dataBaseHelper: DataBaseHelper
    lateinit var myDataBase: SQLiteDatabase
    lateinit var sessionManager: SessionManager
    lateinit var appUtils: AppUtils

    private var TABLE_RIDE: String = "blackmaria_rides"
    private var ride_id: String = "ride_id"
    private var driver_id: String = "driver_id"
    private var ride_date: String = "ride_date"
    private var ride_json: String = "ride_json"
    private var singleride_ride_date: String = "singleride_ride_date"
    private var singleride_ride_status: String = "singleride_ride_status"
    private var singleride_distance: String = "singleride_distance"
    private var singleride_userimage: String = "singleride_userimage"
    private var singleride_user_name: String = "singleride_user_name"

    private var TABLE_MYTRIP: String = "blackmaria_myrides"
    private var myride_json: String = "myride_json"

    private var TABLE_MYTRIP_MONTH: String = "blackmaria_myrides_month"
    private var TABLE_MYTRIP_WEEK: String = "blackmaria_myrides_week"
    private var TABLE_MYTRIP_MONTHTOWEEK: String = "blackmaria_myridesmonth_to_week"
    private var TABLE_MYTRIP_DAY: String = "blackmaria_myrides_day"
    private var TABLE_MYTRIP_WEEK_TO_DAY: String = "blackmaria_myrides_week_to_day"

    private var completed_rides: String = "completed_rides"
    private var rides_km: String = "rides_km"
    private var weektotalrides: String = "weektotalrides"
    private var daytotalrides: String = "daytotalrides"
    private var bidding_trip: String = "bidding_trip"
    private var sos_trip: String = "sos_trip"
    private var cancel_rides: String = "cancel_rides"
    private var driver_cancel_rides: String = "driver_cancel_rides"
    private var from_date: String = "from_date"
    private var to_date: String = "to_date"
    private var month_desc: String = "month_desc"
    private var weektoday_day: String = "weektoday_day"
    private var weektoday_ridecount: String = "weektoday_ridecount"
    private var weektoday_distance: String = "weektoday_distance"
    private var weektoday_driver_earning: String = "weektoday_driver_earning"
    private var weektoday_driver_date_f: String = "weektoday_driver_date_f"


    private val CREATE_TABLE_RIDE = "CREATE TABLE IF NOT EXISTS $TABLE_RIDE($driver_id TEXT,$ride_id TEXT NOT NULL UNIQUE,$ride_date TEXT,$ride_json TEXT,$singleride_ride_date TEXT,$singleride_ride_status TEXT,$singleride_distance TEXT,$singleride_userimage TEXT,$singleride_user_name TEXT)"
    private val CREATE_TABLE_MYTRIP = "CREATE TABLE IF NOT EXISTS $TABLE_MYTRIP($driver_id TEXT,$myride_json TEXT)"
    private val CREATE_TABLE_MYTRIP_MONTH = "CREATE TABLE IF NOT EXISTS $TABLE_MYTRIP_MONTH($driver_id TEXT,$from_date TEXT NOT NULL UNIQUE,$to_date TEXT NOT NULL UNIQUE,$month_desc TEXT,$completed_rides TEXT,$bidding_trip TEXT,$sos_trip TEXT,$cancel_rides TEXT,$driver_cancel_rides TEXT)"

    private val CREATE_TABLE_MYTRIP_MONTH_TO_WEEK = "CREATE TABLE IF NOT EXISTS $TABLE_MYTRIP_MONTHTOWEEK($driver_id TEXT,$from_date TEXT NOT NULL UNIQUE,$to_date TEXT NOT NULL UNIQUE,$month_desc TEXT,$rides_km TEXT,$weektotalrides TEXT)"
    private val CREATE_TABLE_MYTRIP_WEEK = "CREATE TABLE IF NOT EXISTS $TABLE_MYTRIP_WEEK($driver_id TEXT,$from_date TEXT NOT NULL UNIQUE,$to_date TEXT NOT NULL UNIQUE,$month_desc TEXT,$completed_rides TEXT,$bidding_trip TEXT,$sos_trip TEXT,$cancel_rides TEXT,$driver_cancel_rides TEXT,$rides_km TEXT,$weektotalrides TEXT)"
    private val CREATE_TABLE_MYTRIP_DAY = "CREATE TABLE IF NOT EXISTS $TABLE_MYTRIP_DAY($driver_id TEXT,$from_date TEXT NOT NULL UNIQUE,$to_date TEXT NOT NULL UNIQUE,$month_desc TEXT,$completed_rides TEXT,$bidding_trip TEXT,$sos_trip TEXT,$cancel_rides TEXT,$driver_cancel_rides TEXT,$rides_km TEXT,$daytotalrides TEXT)"
    private val CREATE_TABLE_MYTRIP_WEEK_TO_DAY = "CREATE TABLE IF NOT EXISTS $TABLE_MYTRIP_WEEK_TO_DAY($driver_id TEXT,$from_date TEXT NOT NULL UNIQUE,$to_date TEXT NOT NULL UNIQUE,$weektoday_day TEXT,$weektoday_ridecount TEXT,$weektoday_distance TEXT,$weektoday_driver_earning TEXT,$weektoday_driver_date_f TEXT)"

    init {
        this.context = context;
        dataBaseHelper = DataBaseHelper(context)
        myDataBase = dataBaseHelper.writableDatabase
        sessionManager = SessionManager(context)
        appUtils = AppUtils(context)
        createtable()
    }


    inner class DataBaseHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

        override fun onCreate(db: SQLiteDatabase) {}

        override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        }
    }

    private fun createtable() {
        try {
            if (myDataBase == null) {
                myDataBase = dataBaseHelper.getWritableDatabase()
            }
            myDataBase.execSQL(CREATE_TABLE_RIDE)
            myDataBase.execSQL(CREATE_TABLE_MYTRIP)
            myDataBase.execSQL(CREATE_TABLE_MYTRIP_MONTH)
            myDataBase.execSQL(CREATE_TABLE_MYTRIP_MONTH_TO_WEEK)
            myDataBase.execSQL(CREATE_TABLE_MYTRIP_WEEK)
            myDataBase.execSQL(CREATE_TABLE_MYTRIP_DAY)
            myDataBase.execSQL(CREATE_TABLE_MYTRIP_WEEK_TO_DAY)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun insertride(driverid: String, rideid: String, ridedate: String, ridejson: String, singleridedate: String, singleridestatus: String, singleridedistance: String, singlerideuserimage: String, singlerideusername: String) {
        try {
            val selectQuery = "SELECT  * FROM $TABLE_RIDE WHERE ride_id = $rideid"
            val db = myDataBase
            if (db.isOpen) {
                val cursor = db.rawQuery(selectQuery, null)
                if (cursor.moveToFirst()) {
                    val strFilter = "ride_id = $rideid"
                    val values = ContentValues()
                    if (!driverid.isNullOrEmpty()) {
                        values.put(driver_id, driverid)
                    }
                    if (!ridedate.isNullOrEmpty()) {
                        values.put(ride_date, ridedate)
                    }
                    if (!ridejson.isNullOrEmpty()) {
                        values.put(ride_json, ridejson)
                    }
                    if (!singleridedate.isNullOrEmpty()) {
                        values.put(singleride_ride_date, singleridedate)
                    }
                    if (!singleridestatus.isNullOrEmpty()) {
                        values.put(singleride_ride_status, singleridestatus)
                    }

                    var km = singleridedistance.toLowerCase().replace("km", "").trim()
                    if (!km.isNullOrEmpty()) {
                        var rides_kmwhole = cursor.getString(cursor.getColumnIndex(singleride_distance))
                        if (!rides_kmwhole.isNullOrEmpty()) {
                            var kmwhole = rides_kmwhole.toLowerCase().replace("km", "").trim()
                            values.put(singleride_distance, km.toDouble() + kmwhole.toDouble())
                        } else {
                            values.put(singleride_distance, km.toDouble())
                        }
                    }

                    if (!singlerideuserimage.isNullOrEmpty()) {
                        values.put(singleride_userimage, singlerideuserimage)
                    }
                    if (!singlerideusername.isNullOrEmpty()) {
                        values.put(singleride_user_name, singlerideusername)
                    }

                    myDataBase.update(TABLE_RIDE, values, strFilter, null)
                } else {
                    val values = ContentValues()
                    if (!driverid.isNullOrEmpty()) {
                        values.put(driver_id, driverid)
                    }
                    if (!rideid.isNullOrEmpty()) {
                        values.put(ride_id, rideid)
                    }
                    if (!ridedate.isNullOrEmpty()) {
                        values.put(ride_date, ridedate)
                    }
                    if (!ridejson.isNullOrEmpty()) {
                        values.put(ride_json, ridejson)
                    }
                    if (!singleridedate.isNullOrEmpty()) {
                        values.put(singleride_ride_date, singleridedate)
                    }
                    if (!singleridestatus.isNullOrEmpty()) {
                        values.put(singleride_ride_status, singleridestatus)
                    }
                    if (!singleridedistance.isNullOrEmpty()) {
                        values.put(singleride_distance, singleridedistance)
                    }

                    if (!singlerideuserimage.isNullOrEmpty()) {
                        values.put(singleride_userimage, singlerideuserimage)
                    }
                    if (!singlerideusername.isNullOrEmpty()) {
                        values.put(singleride_user_name, singlerideusername)
                    }
                    myDataBase.insert(TABLE_RIDE, null, values)
                }
            }
        } catch (e: android.database.SQLException) {
            e.printStackTrace()
        }
    }

    fun insertmytrips(driverid: String, mytripjson: String) {
        try {
            val values = ContentValues()
            values.put(driver_id, driverid)
            values.put(myride_json, mytripjson)
            myDataBase.insert(TABLE_MYTRIP, null, values)
        } catch (e: android.database.SQLException) {
            e.printStackTrace()
        }
    }

    fun insertmytrips_month(driverid: String, from_dates: String, to_dates: String, month_descs: String, completed_ridess: String, bidding_trips: String,
                            sos_trips: String, cancel_ridess: String, driver_cancel_ridess: String) {
        try {

            val selectQuery = "SELECT  * FROM $TABLE_MYTRIP_MONTH WHERE $from_dates <= from_date AND $to_dates >= to_date"
            val db = myDataBase
            if (db.isOpen) {
                val cursor = db.rawQuery(selectQuery, null)
                if (cursor.moveToFirst()) {
                    val strFilter = "$from_dates <= from_date AND $to_dates >= to_date"
                    val values = ContentValues()
                    if (!driverid.isNullOrEmpty()) {
                        values.put(driver_id, driverid)
                    }
                    if (!month_descs.isNullOrEmpty()) {
                        values.put(month_desc, month_descs)
                    }
                    if (!completed_ridess.isNullOrEmpty()) {
                        values.put(completed_rides, completed_ridess.toInt())
                    }

                    if (!bidding_trips.isNullOrEmpty()) {
                        values.put(bidding_trip, bidding_trips)
                    }
                    if (!sos_trips.isNullOrEmpty()) {
                        values.put(sos_trip, sos_trips)
                    }
                    if (!cancel_ridess.isNullOrEmpty()) {
                        values.put(cancel_rides, cancel_ridess.toInt())
                    }
                    if (!driver_cancel_ridess.isNullOrEmpty()) {
                        values.put(driver_cancel_rides, driver_cancel_ridess.toInt())
                    }

                    myDataBase.update(TABLE_MYTRIP_MONTH, values, strFilter, null)
                } else {
                    val values = ContentValues()
                    values.put(driver_id, driverid)
                    values.put(from_date, from_dates)
                    values.put(to_date, to_dates)
                    values.put(month_desc, month_descs)
                    if (completed_ridess.equals("-")) {
                        values.put(completed_rides, "")
                    } else {
                        values.put(completed_rides, completed_ridess)
                    }
                    if (bidding_trips.equals("-")) {
                        values.put(bidding_trip, "")
                    } else {
                        values.put(bidding_trip, bidding_trips)
                    }
                    if (sos_trips.equals("-")) {
                        values.put(sos_trip, "")
                    } else {
                        values.put(sos_trip, sos_trips)
                    }
                    if (cancel_ridess.equals("-")) {
                        values.put(cancel_rides, "")
                    } else {
                        values.put(cancel_rides, cancel_ridess)
                    }
                    if (driver_cancel_ridess.equals("-")) {
                        values.put(driver_cancel_rides, "")
                    } else {
                        values.put(driver_cancel_rides, driver_cancel_ridess)
                    }
                    myDataBase.insert(TABLE_MYTRIP_MONTH, null, values)
                }
            }


        } catch (e: android.database.SQLException) {
            e.printStackTrace()
        }
    }


    fun insertmytrips_month_rideend(driverid: String, from_dates: String, to_dates: String, month_descs: String, completed_ridess: String, bidding_trips: String,
                                    sos_trips: String, cancel_ridess: String, driver_cancel_ridess: String) {
        try {

            val selectQuery = "SELECT  * FROM $TABLE_MYTRIP_MONTH WHERE $from_dates <= from_date AND $to_dates >= to_date"
            val db = myDataBase
            if (db.isOpen) {
                val cursor = db.rawQuery(selectQuery, null)
                if (cursor.moveToFirst()) {
                    val strFilter = "$from_dates <= from_date AND $to_dates >= to_date"
                    val values = ContentValues()
                    if (!driverid.isNullOrEmpty()) {
                        values.put(driver_id, driverid)
                    }
                    if (!month_descs.isNullOrEmpty()) {
                        values.put(month_desc, month_descs)
                    }
                    if (!completed_ridess.isNullOrEmpty()) {
                        var completed_ridesswhole = cursor.getString(cursor.getColumnIndex(completed_rides))
                        completed_ridesswhole = completed_ridesswhole.replace("-", "")
                        if (!completed_ridesswhole.isNullOrEmpty()) {
                            values.put(completed_rides, completed_ridess.toInt() + completed_ridesswhole.toInt())
                        } else {
                            values.put(completed_rides, completed_ridess.toInt())
                        }
                    }
                    if (!bidding_trips.isNullOrEmpty()) {
                        values.put(bidding_trip, bidding_trips)
                    }
                    if (!sos_trips.isNullOrEmpty()) {
                        values.put(sos_trip, sos_trips)
                    }
                    if (!cancel_ridess.isNullOrEmpty()) {
                        var cancel_ridesswhole = cursor.getString(cursor.getColumnIndex(cancel_rides))
                        cancel_ridesswhole = cancel_ridesswhole.replace("-", "")
                        if (!cancel_ridesswhole.isNullOrEmpty()) {
                            values.put(cancel_rides, cancel_ridess.toInt() + cancel_ridesswhole.toInt())
                        } else {
                            values.put(cancel_rides, cancel_ridess.toInt())
                        }
                    }
                    if (!driver_cancel_ridess.isNullOrEmpty()) {
                        var driver_cancel_ridesswhole = cursor.getString(cursor.getColumnIndex(driver_cancel_rides))
                        driver_cancel_ridesswhole = driver_cancel_ridesswhole.replace("-", "")
                        if (!driver_cancel_ridesswhole.isNullOrEmpty()) {
                            values.put(driver_cancel_rides, driver_cancel_ridess.toInt() + driver_cancel_ridesswhole.toInt())
                        } else {
                            values.put(driver_cancel_rides, driver_cancel_ridess.toInt())
                        }
                    }

                    myDataBase.update(TABLE_MYTRIP_MONTH, values, strFilter, null)
                } else {
                    val values = ContentValues()
                    values.put(driver_id, driverid)
                    values.put(from_date, from_dates)
                    values.put(to_date, to_dates)
                    values.put(month_desc, month_descs)
                    if (completed_ridess.equals("-")) {
                        values.put(completed_rides, "")
                    } else {
                        values.put(completed_rides, completed_ridess)
                    }
                    if (bidding_trips.equals("-")) {
                        values.put(bidding_trip, "")
                    } else {
                        values.put(bidding_trip, bidding_trips)
                    }
                    if (sos_trips.equals("-")) {
                        values.put(sos_trip, "")
                    } else {
                        values.put(sos_trip, sos_trips)
                    }
                    if (cancel_ridess.equals("-")) {
                        values.put(cancel_rides, "")
                    } else {
                        values.put(cancel_rides, cancel_ridess)
                    }
                    if (driver_cancel_ridess.equals("-")) {
                        values.put(driver_cancel_rides, "")
                    } else {
                        values.put(driver_cancel_rides, driver_cancel_ridess)
                    }
                    myDataBase.insert(TABLE_MYTRIP_MONTH, null, values)
                }
            }


        } catch (e: android.database.SQLException) {
            e.printStackTrace()
        }
    }

    fun insertmytrips_week(driverid: String, from_dates: String, to_dates: String, month_descs: String, completed_ridess: String, bidding_trips: String,
                           sos_trips: String, cancel_ridess: String, driver_cancel_ridess: String) {
        try {
            val selectQuery = "SELECT  * FROM $TABLE_MYTRIP_WEEK WHERE $from_dates <= from_date AND $to_dates >= to_date"
            val db = myDataBase

            if (db.isOpen) {
                val cursor = db.rawQuery(selectQuery, null)
                if (cursor.moveToFirst()) {
                    val strFilter = "$from_dates <= from_date AND $to_dates >= to_date"
                    val values = ContentValues()
                    if (!driverid.isNullOrEmpty()) {
                        values.put(driver_id, driverid)
                    }
                    if (!month_descs.isNullOrEmpty()) {
                        values.put(month_desc, month_descs)
                    }
                    if (!completed_ridess.isNullOrEmpty()) {
                        values.put(completed_rides, completed_ridess)
                    }
                    if (!bidding_trips.isNullOrEmpty()) {
                        values.put(bidding_trip, bidding_trips)
                    }
                    if (!sos_trips.isNullOrEmpty()) {
                        values.put(sos_trip, sos_trips)
                    }
                    if (!cancel_ridess.isNullOrEmpty()) {
                        values.put(cancel_rides, cancel_ridess)
                    }
                    if (!driver_cancel_ridess.isNullOrEmpty()) {
                        values.put(driver_cancel_rides, driver_cancel_ridess)
                    }
                    myDataBase.update(TABLE_MYTRIP_WEEK, values, strFilter, null)
                } else {
                    val values = ContentValues()
                    values.put(driver_id, driverid)
                    values.put(from_date, from_dates)
                    values.put(to_date, to_dates)
                    values.put(month_desc, month_descs)
                    if (completed_ridess.equals("-")) {
                        values.put(completed_rides, "")
                    } else {
                        values.put(completed_rides, completed_ridess)
                    }
                    if (bidding_trips.equals("-")) {
                        values.put(bidding_trip, "")
                    } else {
                        values.put(bidding_trip, bidding_trips)
                    }
                    if (sos_trips.equals("-")) {
                        values.put(sos_trip, "")
                    } else {
                        values.put(sos_trip, sos_trips)
                    }
                    if (cancel_ridess.equals("-")) {
                        values.put(cancel_rides, "")
                    } else {
                        values.put(cancel_rides, cancel_ridess)
                    }
                    if (driver_cancel_ridess.equals("-")) {
                        values.put(driver_cancel_rides, "")
                    } else {
                        values.put(driver_cancel_rides, driver_cancel_ridess)
                    }
                    myDataBase.insert(TABLE_MYTRIP_WEEK, null, values)
                }
            }


        } catch (e: android.database.SQLException) {
            e.printStackTrace()
        }
    }

    fun insertmytrips_weekrideend(driverid: String, from_dates: String, to_dates: String, month_descs: String, completed_ridess: String, bidding_trips: String,
                                  sos_trips: String, cancel_ridess: String, driver_cancel_ridess: String) {
        try {
            val selectQuery = "SELECT  * FROM $TABLE_MYTRIP_WEEK WHERE $from_dates <= from_date AND $to_dates >= to_date"
            val db = myDataBase

            if (db.isOpen) {
                val cursor = db.rawQuery(selectQuery, null)
                if (cursor.moveToFirst()) {
                    val strFilter = "$from_dates <= from_date AND $to_dates >= to_date"
                    val values = ContentValues()
                    if (!driverid.isNullOrEmpty()) {
                        values.put(driver_id, driverid)
                    }
                    if (!month_descs.isNullOrEmpty()) {
                        values.put(month_desc, month_descs)
                    }
                    if (!completed_ridess.isNullOrEmpty()) {
                        var completed_ridesswhole = cursor.getString(cursor.getColumnIndex(completed_rides))
                        completed_ridesswhole = completed_ridesswhole.replace("-", "")
                        if (!completed_ridesswhole.isNullOrEmpty()) {
                            values.put(completed_rides, completed_ridess.toInt() + completed_ridesswhole.toInt())
                        } else {
                            values.put(completed_rides, completed_ridess)
                        }
                    }
                    if (!bidding_trips.isNullOrEmpty()) {
                        values.put(bidding_trip, bidding_trips)
                    }
                    if (!sos_trips.isNullOrEmpty()) {
                        values.put(sos_trip, sos_trips)
                    }
                    if (!cancel_ridess.isNullOrEmpty()) {
                        var cancel_ridesswhole = cursor.getString(cursor.getColumnIndex(cancel_rides))
                        cancel_ridesswhole = cancel_ridesswhole.replace("-", "")
                        if (!cancel_ridesswhole.isNullOrEmpty()) {
                            values.put(cancel_rides, cancel_ridess.toInt() + cancel_ridesswhole.toInt())
                        } else {
                            values.put(cancel_rides, cancel_ridess.toInt())
                        }
                    }
                    if (!driver_cancel_ridess.isNullOrEmpty()) {
                        var driver_cancel_ridesswhole = cursor.getString(cursor.getColumnIndex(driver_cancel_rides))
                        driver_cancel_ridesswhole = driver_cancel_ridesswhole.replace("-", "")
                        if (!driver_cancel_ridesswhole.isNullOrEmpty()) {
                            values.put(driver_cancel_rides, driver_cancel_ridess.toInt() + driver_cancel_ridesswhole.toInt())
                        } else {
                            values.put(driver_cancel_rides, driver_cancel_ridess.toInt())
                        }
                    }
                    myDataBase.update(TABLE_MYTRIP_WEEK, values, strFilter, null)
                } else {
                    val values = ContentValues()
                    values.put(driver_id, driverid)
                    values.put(from_date, from_dates)
                    values.put(to_date, to_dates)
                    values.put(month_desc, month_descs)
                    if (completed_ridess.equals("-")) {
                        values.put(completed_rides, "")
                    } else {
                        values.put(completed_rides, completed_ridess)
                    }
                    if (bidding_trips.equals("-")) {
                        values.put(bidding_trip, "")
                    } else {
                        values.put(bidding_trip, bidding_trips)
                    }
                    if (sos_trips.equals("-")) {
                        values.put(sos_trip, "")
                    } else {
                        values.put(sos_trip, sos_trips)
                    }
                    if (cancel_ridess.equals("-")) {
                        values.put(cancel_rides, "")
                    } else {
                        values.put(cancel_rides, cancel_ridess)
                    }
                    if (driver_cancel_ridess.equals("-")) {
                        values.put(driver_cancel_rides, "")
                    } else {
                        values.put(driver_cancel_rides, driver_cancel_ridess)
                    }
                    myDataBase.insert(TABLE_MYTRIP_WEEK, null, values)
                }
            }


        } catch (e: android.database.SQLException) {
            e.printStackTrace()
        }
    }

    fun insertmytrips_day(driverid: String, from_dates: String, to_dates: String, month_descs: String, completed_ridess: String, bidding_trips: String,
                          sos_trips: String, cancel_ridess: String, driver_cancel_ridess: String
                          , rideskm: String, distance: String) {
        try {


            val selectQuery = "SELECT  * FROM $TABLE_MYTRIP_DAY WHERE $from_dates <= from_date  AND $to_dates >= to_date"
            val db = myDataBase
            if (db.isOpen) {
                val cursor = db.rawQuery(selectQuery, null)
                if (cursor.moveToFirst()) {
                    val strFilter = "$from_dates <= from_date AND $to_dates >= to_date"
                    val values = ContentValues()
                    if (!driverid.isNullOrEmpty()) {
                        values.put(driver_id, driverid)
                    }
                    if (!month_descs.isNullOrEmpty()) {
                        values.put(month_desc, month_descs)
                    }
                    if (!completed_ridess.isNullOrEmpty()) {
                        values.put(completed_rides, completed_ridess)
                    }
                    if (!bidding_trips.isNullOrEmpty()) {
                        values.put(bidding_trip, bidding_trips)
                    }
                    if (!sos_trips.isNullOrEmpty()) {
                        values.put(sos_trip, sos_trips)
                    }
                    if (!cancel_ridess.isNullOrEmpty()) {
                        values.put(cancel_rides, cancel_ridess)
                    }
                    if (!driver_cancel_ridess.isNullOrEmpty()) {
                        values.put(driver_cancel_rides, driver_cancel_ridess)
                    }

                    if (!rideskm.isNullOrEmpty()) {
                        values.put(rides_km, rideskm)
                    }

                    if (!distance.isNullOrEmpty()) {
                        values.put(daytotalrides, distance)
                    }

                    myDataBase.update(TABLE_MYTRIP_DAY, values, strFilter, null)
                } else {
                    val values = ContentValues()
                    values.put(driver_id, driverid)
                    values.put(from_date, from_dates)
                    values.put(to_date, to_dates)
                    values.put(month_desc, month_descs)
                    if (completed_ridess.equals("-")) {
                        values.put(completed_rides, "")
                    } else {
                        values.put(completed_rides, completed_ridess)
                    }
                    if (bidding_trips.equals("-")) {
                        values.put(bidding_trip, "")
                    } else {
                        values.put(bidding_trip, bidding_trips)
                    }
                    if (sos_trips.equals("-")) {
                        values.put(sos_trip, "")
                    } else {
                        values.put(sos_trip, sos_trips)
                    }
                    if (cancel_ridess.equals("-")) {
                        values.put(cancel_rides, "")
                    } else {
                        values.put(cancel_rides, cancel_ridess)
                    }
                    if (driver_cancel_ridess.equals("-")) {
                        values.put(driver_cancel_rides, "")
                    } else {
                        values.put(driver_cancel_rides, driver_cancel_ridess)
                    }
                    myDataBase.insert(TABLE_MYTRIP_DAY, null, values)
                }
            }
        } catch (e: android.database.SQLException) {
            e.printStackTrace()
        }
    }

    fun insertmytrips_dayrideend(driverid: String, from_dates: String, to_dates: String, month_descs: String, completed_ridess: String, bidding_trips: String,
                                 sos_trips: String, cancel_ridess: String, driver_cancel_ridess: String
                                 , rideskm: String, distance: String) {
        try {

            val selectQuery = "SELECT  * FROM $TABLE_MYTRIP_DAY WHERE $from_dates <= from_date AND $to_dates >= to_date"
            val db = myDataBase
            if (db.isOpen) {
                val cursor = db.rawQuery(selectQuery, null)
                if (cursor.moveToFirst()) {
                    val strFilter = "$from_dates <= from_date AND $to_dates >= to_date"
                    val values = ContentValues()
                    if (!driverid.isNullOrEmpty()) {
                        values.put(driver_id, driverid)
                    }
                    if (!month_descs.isNullOrEmpty()) {
                        values.put(month_desc, month_descs)
                    }
                    if (!completed_ridess.isNullOrEmpty()) {
                        var completed_ridesswhole = cursor.getString(cursor.getColumnIndex(completed_rides))
                        if (!completed_ridesswhole.isNullOrEmpty()) {
                            completed_ridesswhole = completed_ridesswhole.replace("-", "")
                            values.put(completed_rides, completed_ridess.toInt() + completed_ridesswhole.toInt())
                        } else {
                            values.put(completed_rides, "")
                        }
                    }
                    if (!bidding_trips.isNullOrEmpty()) {
                        values.put(bidding_trip, bidding_trips)
                    }
                    if (!sos_trips.isNullOrEmpty()) {
                        values.put(sos_trip, sos_trips)
                    }
                    if (!cancel_ridess.isNullOrEmpty()) {
                        var cancel_ridesswhole = cursor.getString(cursor.getColumnIndex(cancel_rides))
                        if (!cancel_ridesswhole.isNullOrEmpty()) {
                            cancel_ridesswhole = cancel_ridesswhole.replace("-", "")
                            values.put(cancel_rides, cancel_ridess.toInt() + cancel_ridesswhole.toInt())
                        } else {
                            values.put(cancel_rides, "")
                        }
                    }
                    if (!driver_cancel_ridess.isNullOrEmpty()) {
                        var driver_cancel_ridesswhole = cursor.getString(cursor.getColumnIndex(driver_cancel_rides))
                        if (!driver_cancel_ridesswhole.isNullOrEmpty()) {
                            driver_cancel_ridesswhole = driver_cancel_ridesswhole.replace("-", "")
                            values.put(driver_cancel_rides, driver_cancel_ridess.toInt() + driver_cancel_ridesswhole.toInt())
                        } else {
                            values.put(driver_cancel_rides, "")
                        }
                    }

                    if (!rideskm.isNullOrEmpty()) {
                        var rideskmswhole = cursor.getString(cursor.getColumnIndex(rides_km))
                        if (!rideskmswhole.isNullOrEmpty()) {
                            rideskmswhole = rideskmswhole.replace("-", "")
                            values.put(rides_km, rideskm.toInt() + rideskmswhole.toInt())
                        } else {
                            values.put(rides_km, "")
                        }
                    }

                    if (!distance.isNullOrEmpty()) {
                        var distancewhole = cursor.getString(cursor.getColumnIndex(daytotalrides))
                        if (!distancewhole.isNullOrEmpty()) {
                            distancewhole = distancewhole.replace("-", "")
                            values.put(daytotalrides, distance.toInt() + distancewhole.toInt())
                        } else {
                            values.put(daytotalrides, "")
                        }
                    }

                    myDataBase.update(TABLE_MYTRIP_DAY, values, strFilter, null)
                } else {
                    val values = ContentValues()
                    values.put(driver_id, driverid)
                    values.put(from_date, from_dates)
                    values.put(to_date, to_dates)
                    values.put(month_desc, month_descs)
                    if (completed_ridess.equals("-")) {
                        values.put(completed_rides, "")
                    } else {
                        values.put(completed_rides, completed_ridess)
                    }
                    if (bidding_trips.equals("-")) {
                        values.put(bidding_trip, "")
                    } else {
                        values.put(bidding_trip, bidding_trips)
                    }
                    if (sos_trips.equals("-")) {
                        values.put(sos_trip, "")
                    } else {
                        values.put(sos_trip, sos_trips)
                    }
                    if (cancel_ridess.equals("-")) {
                        values.put(cancel_rides, "")
                    } else {
                        values.put(cancel_rides, cancel_ridess)
                    }
                    if (driver_cancel_ridess.equals("-")) {
                        values.put(driver_cancel_rides, "")
                    } else {
                        values.put(driver_cancel_rides, driver_cancel_ridess)
                    }
                    myDataBase.insert(TABLE_MYTRIP_DAY, null, values)
                }
            }
        } catch (e: android.database.SQLException) {
            e.printStackTrace()
        }
    }


    fun insertmytripsweektoday(from_dates: String, to_dates: String, daystring: String, ridecount: String, distance: String, driver_earning: String,
                               date_f: String) {
        try {

            val selectQuery = "SELECT  * FROM $TABLE_MYTRIP_WEEK_TO_DAY WHERE $from_dates <= from_date AND $to_dates >= to_date"
            val db = myDataBase
            if (db.isOpen) {
                val cursor = db.rawQuery(selectQuery, null)
                if (cursor.moveToFirst()) {
                    val strFilter = "$from_dates <= from_date AND $to_dates >= to_date"
                    val values = ContentValues()
                    if (!ridecount.isNullOrEmpty()) {
                        values.put(weektoday_ridecount, ridecount.replace("-", ""))
                    }
                    if (!distance.isNullOrEmpty()) {
                        var distances = distance.replace("km", "").trim()
                        values.put(weektoday_distance, distances.replace("-", ""))
                    }
                    values.put(weektoday_driver_earning, driver_earning)
                    myDataBase.update(TABLE_MYTRIP_WEEK_TO_DAY, values, strFilter, null)
                } else {
                    val values = ContentValues()
                    values.put(from_date, from_dates)
                    values.put(to_date, to_dates)
                    values.put(weektoday_day, daystring)
                    if (!ridecount.equals("-")) {
                        values.put(weektoday_ridecount, ridecount)
                    } else {
                        values.put(weektoday_ridecount, "")
                    }
                    if (!distance.equals("-")) {
                        var distances = distance.replace("km", "").trim()
                        values.put(weektoday_distance, distances.replace("-", ""))
                    } else {
                        values.put(weektoday_distance, "")
                    }
                    values.put(weektoday_driver_earning, driver_earning)
                    values.put(weektoday_driver_date_f, appUtils.getdatetotiestamp(date_f))
                    myDataBase.insert(TABLE_MYTRIP_WEEK_TO_DAY, null, values)
                }
            }
        } catch (e: android.database.SQLException) {
            e.printStackTrace()
        }
    }

    fun insertmytripsweektodayridend(from_dates: String, to_dates: String, daystring: String, ridecount: String, distance: String, driver_earning: String,
                                     date_f: String) {
        try {

            val selectQuery = "SELECT  * FROM $TABLE_MYTRIP_WEEK_TO_DAY WHERE $from_dates <= from_date  AND $to_dates >= to_date"
            val db = myDataBase
            if (db.isOpen) {
                val cursor = db.rawQuery(selectQuery, null)
                if (cursor.moveToFirst()) {
                    val strFilter = "$from_dates <= from_date AND $to_dates >= to_date"
                    val values = ContentValues()
                    if (!ridecount.isNullOrEmpty()) {
                        var weektoday_ridecountwhole = cursor.getString(cursor.getColumnIndex(weektoday_ridecount))
                        if (!weektoday_ridecountwhole.isNullOrEmpty()) {
                            weektoday_ridecountwhole = weektoday_ridecountwhole.replace("-", "")
                            if (!ridecount.equals("-")) {
                                values.put(weektoday_ridecount, ridecount.toInt() + weektoday_ridecountwhole.toInt())
                            }
                        } else {
                            values.put(weektoday_ridecount, "")
                        }
                    }
                    if (!distance.isNullOrEmpty()) {
                        var weektoday_distancewhole = cursor.getString(cursor.getColumnIndex(weektoday_distance))
                        if (!weektoday_distancewhole.isNullOrEmpty()) {
                            weektoday_distancewhole = weektoday_distancewhole.replace("-", "")
                            weektoday_distancewhole = weektoday_distancewhole.replace("km", "").trim()
                            values.put(weektoday_distance, distance.toInt() + weektoday_distancewhole.toInt())
                        } else {
                            values.put(weektoday_distance, "")
                        }
                    }
                    values.put(weektoday_driver_earning, driver_earning)
                    myDataBase.update(TABLE_MYTRIP_WEEK_TO_DAY, values, strFilter, null)
                } else {
                    val values = ContentValues()
                    values.put(from_date, from_dates)
                    values.put(to_date, to_dates)
                    values.put(weektoday_day, daystring)
                    if (!ridecount.equals("-")) {
                        values.put(weektoday_ridecount, ridecount)
                    } else {
                        values.put(weektoday_ridecount, "")
                    }
                    if (!distance.equals("-")) {
                        var distance = distance.replace("km", "").trim()
                        values.put(weektoday_distance, distance.replace("-", ""))
                    } else {
                        values.put(weektoday_distance, "")
                    }
                    values.put(weektoday_driver_earning, driver_earning)
                    values.put(weektoday_driver_date_f, appUtils.getdatetotiestamp(date_f))
                    myDataBase.insert(TABLE_MYTRIP_WEEK_TO_DAY, null, values)
                }
            }
        } catch (e: android.database.SQLException) {
            e.printStackTrace()
        }
    }


    fun getride(rideID: String): String {
        var ridejson_global: String = "0"
        val selectQuery = "SELECT  * FROM $TABLE_RIDE WHERE ride_id = $rideID"
        val db = myDataBase
        if (db.isOpen) {
            val cursor = db.rawQuery(selectQuery, null)
            if (cursor.moveToFirst()) {
                do {
                    var ridejson = cursor.getString(cursor.getColumnIndex(ride_json))
                    if (!ridejson.isNullOrEmpty()) {
                        ridejson_global = ridejson
                    }
                } while (cursor.moveToNext())
            }
            //     db.close();
            println(ridejson_global.toString())
        }

        return ridejson_global!!
    }

    fun getonedayfullrides(from_dates: String, to_dates: String): ArrayList<Rides> {
        var ridejson_global = ArrayList<Rides>()
        try {

            val selectQuery = "SELECT  * FROM $TABLE_RIDE WHERE  $from_dates <= ride_date  AND $to_dates >= ride_date ORDER BY ride_id DESC"
            val db = myDataBase
            if (db.isOpen) {
                val cursor = db.rawQuery(selectQuery, null)
                if (cursor.moveToFirst()) {
                    do {
                        var ride_id = cursor.getString(cursor.getColumnIndex(ride_id))
                        var singleride_ride_date = cursor.getString(cursor.getColumnIndex(singleride_ride_date))
                        var singleride_ride_status = cursor.getString(cursor.getColumnIndex(singleride_ride_status))
                        var singleride_distance = cursor.getString(cursor.getColumnIndex(singleride_distance))
                        var singleride_userimage = cursor.getString(cursor.getColumnIndex(singleride_userimage))
                        var singleride_user_name = cursor.getString(cursor.getColumnIndex(singleride_user_name))

                        var ridesobj = Rides();
                        ridesobj.ride_id = ride_id
                        ridesobj.datetime = singleride_ride_date
                        ridesobj.ride_status = singleride_ride_status
                        ridesobj.distance = singleride_distance
                        ridesobj.user_image = singleride_userimage
                        ridesobj.user_name = singleride_user_name
                        ridejson_global.add(ridesobj)
                    } while (cursor.moveToNext())
                }
            }
        } catch (e: android.database.SQLException) {
            e.printStackTrace()
        }

        return ridejson_global
    }

    fun getmytripjson(driverid: String): String {
        var ridejson_global = "0"
        try {
            val selectQuery = "SELECT  * FROM $TABLE_MYTRIP"
            val db = myDataBase
            if (db.isOpen) {
                val cursor = db.rawQuery(selectQuery, null)
                if (cursor.moveToFirst()) {
                    do {
                        var ridejson = cursor.getString(cursor.getColumnIndex(myride_json))
                        ridejson_global = ridejson
                    } while (cursor.moveToNext())
                }
                //     db.close();
            }
        } catch (er: SQLiteException) {
            er.printStackTrace()
        }

        return ridejson_global
    }

    fun getmytripsmonthwise(firstdateofmonth: String, lastdateofmonth: String): Monthridepojodetailsfromdb {
        var ridejson_global = Monthridepojodetailsfromdb()
        try {
            val selectQuery = "SELECT  * FROM $TABLE_MYTRIP_MONTH WHERE $firstdateofmonth <= from_date  AND $lastdateofmonth >= to_date"
            val db = myDataBase
            if (db.isOpen) {
                val cursor = db.rawQuery(selectQuery, null)
                if (cursor.moveToFirst()) {
                    do {
                        var month_desc = cursor.getString(cursor.getColumnIndex(month_desc))
                        var completed_rides = cursor.getString(cursor.getColumnIndex(completed_rides))
                        var cancel_rides = cursor.getString(cursor.getColumnIndex(cancel_rides))
                        var driver_cancel_rides = cursor.getString(cursor.getColumnIndex(driver_cancel_rides))
                        ridejson_global.monthdescription = month_desc
                        ridejson_global.completedtrip = completed_rides
                        ridejson_global.canceltrip = cancel_rides
                        ridejson_global.drivercanceltrip = driver_cancel_rides
                    } while (cursor.moveToNext());
                }

                //     db.close();
            }
        } catch (er: SQLiteException) {
            er.printStackTrace()
        }

        return ridejson_global
    }

    fun getmytripsweekwise(firstdateofmonth: String, lastdateofmonth: String): Monthridepojodetailsfromdb {
        var ridejson_global = Monthridepojodetailsfromdb()
        try {
            val selectQuery = "SELECT  * FROM $TABLE_MYTRIP_WEEK WHERE $firstdateofmonth <= from_date  AND $lastdateofmonth >= to_date"
            val db = myDataBase
            if (db.isOpen) {
                val cursor = db.rawQuery(selectQuery, null)
                if (cursor.moveToFirst()) {
                    do {
                        var month_desc = cursor.getString(cursor.getColumnIndex(month_desc))
                        var completed_rides = cursor.getString(cursor.getColumnIndex(completed_rides))
                        var cancel_rides = cursor.getString(cursor.getColumnIndex(cancel_rides))
                        var driver_cancel_rides = cursor.getString(cursor.getColumnIndex(driver_cancel_rides))
                        ridejson_global.monthdescription = getmonthinstring(firstdateofmonth)
                        ridejson_global.completedtrip = completed_rides
                        ridejson_global.canceltrip = cancel_rides
                        ridejson_global.drivercanceltrip = driver_cancel_rides
                    } while (cursor.moveToNext());
                }

                //     db.close();
            }
        } catch (er: SQLiteException) {
            er.printStackTrace()
        }

        return ridejson_global
    }

    fun getmonthtoweekridetable(firstdateofmonth: String, lastdateofmonth: String): ArrayList<Monthreport> {
        var weekwisetrippojo = ArrayList<Monthreport>()
//        var firstdateofmonth = appUtils.getdatetotiestamp(firstdateofmonth)
//        var lastdateofmonth = appUtils.getdatetotiestamp(lastdateofmonth)
        try {
            val selectQuery = "SELECT  * FROM $TABLE_MYTRIP_MONTHTOWEEK WHERE $firstdateofmonth <= from_date  AND $lastdateofmonth >= to_date"
            val db = myDataBase
            if (db.isOpen) {
                val cursor = db.rawQuery(selectQuery, null)
                if (cursor.moveToFirst()) {
                    do {
                        var rides_km = cursor.getString(cursor.getColumnIndex(rides_km))
                        var weektotalrides = cursor.getString(cursor.getColumnIndex(weektotalrides))
                        var fromadate = cursor.getString(cursor.getColumnIndex(from_date))
                        var to_date = cursor.getString(cursor.getColumnIndex(to_date))
                        rides_km = rides_km.replace("km", "")
                        var positioncoutn = cursor.position + 1
                        var weekobj = Monthreport();
                        weekobj.week = "WEEK"
                        weekobj.WeekCount = positioncoutn.toString()
                        weekobj.ridecount = weektotalrides
                        weekobj.distance = rides_km + " KM"
                        weekobj.income = ""
                        weekobj.currency = ""
                        weekobj.startdate = ""
                        weekobj.endate = ""
                        weekobj.weekdesc = getmonthinstring(fromadate)
                        weekobj.startdate_f = getDate(fromadate)
                        weekobj.endate_f = getDate(to_date)

                        weekwisetrippojo.add(weekobj)
                    } while (cursor.moveToNext());
                }
            }
        } catch (er: SQLiteException) {
            er.printStackTrace()
        }
        return weekwisetrippojo;
    }

    fun getupdateweekwiseridetable(firstdateofmonth: String, lastdateofmonth: String, month_descs: String, rides_kms: String, weektotalridess: String) {

        try {
            val selectQuery = "SELECT  * FROM $TABLE_MYTRIP_MONTHTOWEEK WHERE $firstdateofmonth <= from_date AND $lastdateofmonth >= to_date"
            val db = myDataBase
            if (db.isOpen) {
                val cursor = db.rawQuery(selectQuery, null)
                if (cursor.moveToFirst()) {
                    val strFilter = "$firstdateofmonth <= from_date AND $lastdateofmonth >= to_date"
                    val values = ContentValues()
                    if (!month_descs.isNullOrEmpty()) {
                        values.put(month_desc, month_descs)
                    }
                    var km = rides_kms.toLowerCase().replace("km", "").trim()
                    km = km.toLowerCase().replace("-", "").trim()
                    if (!km.isNullOrEmpty()) {
                        values.put(rides_km, km)
                    }
                    if (!weektotalridess.isNullOrEmpty()) {
                        if (!weektotalridess.equals("-")) {
                            try {
                                values.put(weektotalrides, weektotalridess.replace("-", ""))
                            } catch (e: java.lang.Exception) {
                                e.printStackTrace()
                            }
                        }
                    }
                    myDataBase.update(TABLE_MYTRIP_MONTHTOWEEK, values, strFilter, null)
                } else {
                    try {
                        val values = ContentValues()
                        values.put(from_date, firstdateofmonth)
                        values.put(to_date, lastdateofmonth)
                        values.put(month_desc, month_descs)
                        if (rides_kms.equals("-")) {
                            values.put(rides_km, "")
                        } else {
                            values.put(rides_km, rides_kms)
                        }
                        if (weektotalridess.equals("-")) {
                            values.put(weektotalrides, "")
                        } else {
                            values.put(weektotalrides, weektotalridess.replace("-", ""))
                        }
                        myDataBase.insert(TABLE_MYTRIP_MONTHTOWEEK, null, values)
                    } catch (e: android.database.SQLException) {
                        e.printStackTrace()
                    }
                }
            }
        } catch (er: SQLiteException) {
            er.printStackTrace()
        }
    }

    fun getupdateweekwiseridetablerideend(firstdateofmonth: String, lastdateofmonth: String, month_descs: String, rides_kms: String, weektotalridess: String) {

        try {
            val selectQuery = "SELECT  * FROM $TABLE_MYTRIP_MONTHTOWEEK WHERE $firstdateofmonth <= from_date AND $lastdateofmonth >= to_date"
            val db = myDataBase
            if (db.isOpen) {
                val cursor = db.rawQuery(selectQuery, null)
                if (cursor.moveToFirst()) {
                    val strFilter = "$firstdateofmonth <= from_date AND $lastdateofmonth >= to_date"
                    val values = ContentValues()
                    if (!month_descs.isNullOrEmpty()) {
                        values.put(month_desc, month_descs)
                    }
                    var km = rides_kms.toLowerCase().replace("km", "").trim()
                    km = km.toLowerCase().replace("-", "").trim()
                    if (!km.isNullOrEmpty()) {
                        var rides_kmwhole = cursor.getString(cursor.getColumnIndex(rides_km))
                        if (!rides_kmwhole.isNullOrEmpty()) {
                            var kmwhole = rides_kmwhole.toLowerCase().replace("km", "").trim()
                            values.put(rides_km, km.toInt() + kmwhole.toInt())
                        } else {
                            values.put(rides_km, km)
                        }
                    }
                    if (!weektotalridess.isNullOrEmpty()) {
                        var weektotalrideswhole = cursor.getString(cursor.getColumnIndex(weektotalrides))
                        weektotalrideswhole = weektotalrideswhole.replace("-", "")
                        if (!weektotalrideswhole.isNullOrEmpty()) {
                            values.put(weektotalrides, weektotalridess.toInt() + weektotalrideswhole.toInt())
                        } else {
                            values.put(weektotalrides, weektotalridess.replace("-", ""))
                        }
                    }
                    myDataBase.update(TABLE_MYTRIP_MONTHTOWEEK, values, strFilter, null)
                } else {
                    try {
                        val values = ContentValues()
                        values.put(from_date, firstdateofmonth)
                        values.put(to_date, lastdateofmonth)
                        values.put(month_desc, month_descs)
                        if (rides_kms.equals("-")) {
                            values.put(rides_km, "")
                        } else {
                            values.put(rides_km, rides_kms)
                        }
                        if (weektotalridess.equals("-")) {
                            values.put(weektotalrides, "")
                        } else {
                            values.put(weektotalrides, weektotalridess.replace("-", ""))
                        }
                        myDataBase.insert(TABLE_MYTRIP_MONTHTOWEEK, null, values)
                    } catch (e: android.database.SQLException) {
                        e.printStackTrace()
                    }
                }
            }
        } catch (er: SQLiteException) {
            er.printStackTrace()
        }
    }

    fun getmytripsdaywise(firstdateofmonth: String, lastdateofmonth: String): Monthridepojodetailsfromdb {
        var ridejson_global = Monthridepojodetailsfromdb()
        try {
            val selectQuery = "SELECT  * FROM $TABLE_MYTRIP_DAY WHERE $firstdateofmonth <= from_date AND $lastdateofmonth >= to_date"
            val db = myDataBase
            if (db.isOpen) {
                val cursor = db.rawQuery(selectQuery, null)
                if (cursor.moveToFirst()) {
                    do {
                        var month_desc = cursor.getString(cursor.getColumnIndex(month_desc))
                        var completed_rides = cursor.getString(cursor.getColumnIndex(completed_rides))
                        var cancel_rides = cursor.getString(cursor.getColumnIndex(cancel_rides))
                        var driver_cancel_rides = cursor.getString(cursor.getColumnIndex(driver_cancel_rides))
                        ridejson_global.monthdescription = month_desc
                        ridejson_global.completedtrip = completed_rides
                        ridejson_global.canceltrip = cancel_rides
                        ridejson_global.drivercanceltrip = driver_cancel_rides
                    } while (cursor.moveToNext());
                }

                //     db.close();
            }
        } catch (er: SQLiteException) {
            er.printStackTrace()
        }

        return ridejson_global
    }

    fun getmytripsdayofweekwiselist(firstdateofmonth: String, lastdateofmonth: String): ArrayList<Week> {
        var ridejson_global = ArrayList<Week>()

        try {
            val selectQuery = "SELECT  * FROM $TABLE_MYTRIP_WEEK_TO_DAY WHERE $firstdateofmonth <= from_date AND $lastdateofmonth >= to_date"
            val db = myDataBase
            if (db.isOpen) {
                val cursor = db.rawQuery(selectQuery, null)
                if (cursor.moveToFirst()) {
                    do {
                        var weektoday_day = cursor.getString(cursor.getColumnIndex(weektoday_day))
                        var weektoday_ridecount = cursor.getString(cursor.getColumnIndex(weektoday_ridecount))
                        var weektoday_distance = cursor.getString(cursor.getColumnIndex(weektoday_distance))
                        var weektoday_driver_earning = cursor.getString(cursor.getColumnIndex(weektoday_driver_earning))
                        var weektoday_driver_date_f = cursor.getString(cursor.getColumnIndex(weektoday_driver_date_f))
                        var weekpojo = Week();
                        weekpojo.ridecount = weektoday_ridecount
                        weekpojo.distance = weektoday_distance
                        weekpojo.day = weektoday_day
                        weekpojo.currency = ""
                        weekpojo.date_f = getDate(weektoday_driver_date_f)
                        weekpojo.driver_earning = weektoday_driver_earning
                        weekpojo.order = ""
                        ridejson_global.add(weekpojo)
                    } while (cursor.moveToNext());
                }
            }
        } catch (er: SQLiteException) {
            er.printStackTrace()
        }
        return ridejson_global
    }

    private fun dbclose() {
        dataBaseHelper.close()
    }

    public fun getDate(fromadate: String): String {
        val cal = Calendar.getInstance(Locale.ENGLISH)
        cal.timeInMillis = (fromadate).toLong()
        cal.timeZone = cal.timeZone
        return DateFormat.format("yyyy-MM-dd", cal).toString()
    }


    fun convertdatetomonthinstring(fromadate: String): Int {
        val cal = Calendar.getInstance(Locale.ENGLISH)
        cal.timeInMillis = (fromadate).toLong()
        cal.timeZone = cal.timeZone
        cal.minimalDaysInFirstWeek = 1
        var wk = cal.get(Calendar.WEEK_OF_MONTH)
        return wk;
    }

    fun convertdatetodayinstring(fromadate: String): String {
        val inFormat = SimpleDateFormat("yyyy-MM-dd")
        val date = inFormat.parse(fromadate)
        val outFormat = SimpleDateFormat("EEEE")
        val goal = outFormat.format(date)
        return goal;
    }

    fun converttimestamptodate(fromadate: String): String {
        var cal = Calendar.getInstance(Locale.ENGLISH)
        cal.timeInMillis = (fromadate).toLong()
        cal.timeZone = cal.timeZone
        var month = DateFormat.format("MM", cal).toString()
        var years = DateFormat.format("yyyy", cal).toString()
        var date = DateFormat.format("dd", cal).toString()
        return years + "-" + month + "-" + date;
    }

    public fun getmonthyearinstring(date: String): String {
        var cal = Calendar.getInstance(Locale.ENGLISH)
        cal.timeInMillis = (date).toLong()
        cal.timeZone = cal.timeZone
        var monthString = DateFormat.format("MMM", cal).toString()
        var yearstring = DateFormat.format("yyyy", cal).toString()
        var returnmonth = "AS " + monthString + " " + yearstring
        return returnmonth
    }

    public fun getmonthinstring(date: String): String {
        var cal = Calendar.getInstance(Locale.ENGLISH)
        cal.timeInMillis = (date).toLong()
        cal.timeZone = cal.timeZone
        var day = DateFormat.format("dd", cal).toString()
        var monthString = DateFormat.format("MMM", cal).toString()
        var yearstring = DateFormat.format("yyyy", cal).toString()
        var returnmonth = ""
        var month = convertdatetomonthinstring(date)
        if (month == 1) {
            returnmonth = "First Week of " + monthString + " " + yearstring
        } else if (month == 2) {
            returnmonth = "Second Week of " + monthString + " " + yearstring
        } else if (month == 3) {
            returnmonth = "Third Week of " + monthString + " " + yearstring
        } else if (month == 4) {
            returnmonth = "Fourth Week of " + monthString + " " + yearstring
        } else if (month == 5) {
            returnmonth = "Fifth Week of " + monthString + " " + yearstring
        }
        return returnmonth
    }

    fun getsingleridedate(date: String): String {
        var cal = Calendar.getInstance(Locale.ENGLISH)
        cal.timeInMillis = (date).toLong()
        cal.timeZone = cal.timeZone
        var day = DateFormat.format("dd", cal).toString()
        var monthString = DateFormat.format("MMM", cal).toString()
        var yearstring = DateFormat.format("yyyy", cal).toString()
        var returnmonth = ""
        returnmonth = day + " " + monthString + " " + yearstring
        return returnmonth
    }

    fun mains(args: Array<String>) {
        data class GFG(var name1: String, var name2: String, var name3: String)
        // instantiating object of class
        var gfg = GFG("Geeks", "for", "hi")
        // apply function invoked to change the name3 value
        gfg.apply { this.name3 = "Geeks" }

        with(gfg)
        {
            name1 = "Geeks"
            name3 = "Geeks"
        }
        println(gfg)
    }

    fun cleartable() {
        myDataBase.execSQL("delete from  $TABLE_RIDE")
        myDataBase.execSQL("delete from  $TABLE_MYTRIP")
        myDataBase.execSQL("delete from  $TABLE_MYTRIP_MONTH")
        myDataBase.execSQL("delete from  $TABLE_MYTRIP_MONTHTOWEEK")
        myDataBase.execSQL("delete from  $TABLE_MYTRIP_WEEK")
        myDataBase.execSQL("delete from  $TABLE_MYTRIP_DAY")
        myDataBase.execSQL("delete from  $TABLE_MYTRIP_WEEK_TO_DAY")
    }
}
