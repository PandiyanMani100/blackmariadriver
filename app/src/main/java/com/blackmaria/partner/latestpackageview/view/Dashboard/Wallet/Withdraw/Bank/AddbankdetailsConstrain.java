package com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Withdraw.Bank;

import androidx.appcompat.app.AppCompatActivity;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.Nullable;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.latestpackageview.Adapter.Type_of_registrationadapter;
import com.blackmaria.partner.databinding.ActivityAddbankdetailsConstrainBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Wallet.Withdraw.Bank.AddbankdetailsFactory;
import com.blackmaria.partner.latestpackageview.Model.banklistpojo;
import com.blackmaria.partner.latestpackageview.Model.withdrawalspaymentspojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet.withdraw.bank.AddbankdetailsViewModel;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

public class AddbankdetailsConstrain extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private ActivityAddbankdetailsConstrainBinding binding;
    private AddbankdetailsViewModel addbankdetailsViewModel;
    private SessionManager sessionManager;
    private AppUtils appUtils;
    private String sDriverID = "", withdrawamount = "", accountcode = "";
    private banklistpojo pojo;
    private withdrawalspaymentspojo withdrawalspaymentspojo;
    private int bankselected = 0;
    private boolean iseditbank = false;
    private JSONObject json;
    AccessLanguagefromlocaldb db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        db = new AccessLanguagefromlocaldb(this);
        binding = DataBindingUtil.setContentView(AddbankdetailsConstrain.this, R.layout.activity_addbankdetails_constrain);
        addbankdetailsViewModel = ViewModelProviders.of(this, new AddbankdetailsFactory(this)).get(AddbankdetailsViewModel.class);
        binding.setAddbankdetailsViewModel(addbankdetailsViewModel);
        addbankdetailsViewModel.setIds(binding);

        initView();
        clicklistener();
        setResponse();

        binding.datemonthyear.setText(db.getvalue("fastwallet"));
        binding.withdraw.setText(db.getvalue("bankdetails"));
        binding.accountname.setHint(db.getvalue("accountname"));
        binding.accountnumber.setHint(db.getvalue("accountnumber"));
        binding.selectbank.setText(db.getvalue("Selectbank"));
        binding.confirm.setText(db.getvalue("confirm_lable"));
    }

    private void setResponse() {
        addbankdetailsViewModel.getAddbankdetailsresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Type type = new TypeToken<banklistpojo>() {
                    }.getType();
                    pojo = new GsonBuilder().create().fromJson(jsonObject.toString(), type);
                    bankspinnersetup();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        addbankdetailsViewModel.getGettransactionfeechargesresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                        Intent i = new Intent(AddbankdetailsConstrain.this, WithdrawchangereceipeintsendnowConstrain.class);
                        i.putExtra("json", jsonObject.toString());
                        i.putExtra("transfer_amount",withdrawamount);
                        startActivity(i);
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void initView() {
        sessionManager = SessionManager.getInstance(this);
        appUtils = AppUtils.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        try {
            if (getIntent().hasExtra("transfer_amount")) {
                withdrawamount = getIntent().getStringExtra("transfer_amount");
                json = new JSONObject(getIntent().getStringExtra("json"));
                Type type = new TypeToken<withdrawalspaymentspojo>() {
                }.getType();
                withdrawalspaymentspojo = new GsonBuilder().create().fromJson(json.toString(), type);
            }
            if (getIntent().hasExtra("changereceipient")) {
                iseditbank = true;
                withdrawamount = getIntent().getStringExtra("transfer_amount");
                HashMap<String, String> domain = sessionManager.getbankdetails();
                String name = domain.get(SessionManager.KEY_ACCOUNTNAME);
                String number = domain.get(SessionManager.KEY_ACCOUNTNUMBER);
                accountcode = domain.get(SessionManager.KEY_ACCOUNTCODE);
                String accountbank = domain.get(SessionManager.KEY_ACCOUNTBANK);
//                binding.accountname.setText(name);
//                binding.accountnumber.setText(number);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        addbankdetailsViewModel.getbanklist(Iconstant.getPaymentOPtion_url_new, jsonParams);
    }

    private void clicklistener() {
        binding.confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.accountname.getText().toString().length() == 0) {
                    appUtils.AlertError(AddbankdetailsConstrain.this, db.getvalue("action_error"), db.getvalue("accountnamecouldnotbeempty"));
                } else if (binding.accountnumber.getText().toString().length() == 0) {
                    appUtils.AlertError(AddbankdetailsConstrain.this, db.getvalue("action_error"), db.getvalue("accountnumbercouldnotbeempty"));
                } else if (binding.selectbank.getText().toString().length() == 0) {
                    appUtils.AlertError(AddbankdetailsConstrain.this, db.getvalue("action_error"), db.getvalue("seletanyonebank"));
                } else {
                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    jsonParams.put("driver_id", sDriverID);
                    jsonParams.put("acc_holder_name", binding.accountname.getText().toString().trim());
                    jsonParams.put("acc_number", binding.accountnumber.getText().toString().trim());
                    jsonParams.put("bank_name", pojo.getBank_list().get(bankselected).getName());
                    jsonParams.put("bank_code", pojo.getBank_list().get(bankselected).getCode());
                    addbankdetailsViewModel.updatebankdetails(Iconstant.getXendit_bank_save_url, jsonParams);
                    addbankdetailsViewModel.getUpdatebankdetailsresponse().observe(AddbankdetailsConstrain.this, new Observer<String>() {
                        @Override
                        public void onChanged(@Nullable String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                    sessionManager.setbankdetails(binding.accountname.getText().toString().trim(), binding.accountnumber.getText().toString().trim(), pojo.getBank_list().get(bankselected).getCode(), pojo.getBank_list().get(bankselected).getName());
                                    appUtils.AlertSuccess(AddbankdetailsConstrain.this,db.getvalue("action_success"), "Bank details updated successfully");
                                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                                    jsonParams.put("driver_id", sDriverID);
                                    addbankdetailsViewModel.gettransactionfeesandcharges(Iconstant.wallet_withdrawal_dashboard_url, jsonParams);
                                } else {
                                    appUtils.AlertError(AddbankdetailsConstrain.this, db.getvalue("action_error"), jsonObject.getString("response"));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
        });
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent == binding.selectbankspinner) {
            if(position >0){
                bankselected = position-1;
                String sSelectedItem = binding.selectbankspinner.getSelectedItem().toString().trim();
                binding.selectbank.setText(sSelectedItem);
            }

        }
    }


    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void bankspinnersetup() {
        ArrayList<String> regTypeList = new ArrayList<String>();
        regTypeList.add("Select Bank");
        for (int i = 0; i <= pojo.getBank_list().size() - 1; i++) {
            regTypeList.add(pojo.getBank_list().get(i).getName());
        }
        Type_of_registrationadapter customSpinnerbank = new Type_of_registrationadapter(AddbankdetailsConstrain.this, regTypeList);
        binding.selectbankspinner.setAdapter(customSpinnerbank);
        binding.selectbankspinner.setOnItemSelectedListener(this);

        if (iseditbank) {
//            for (int i = 0; i <= pojo.getBank_list().size() - 1; i++) {
//                if (pojo.getBank_list().get(i).getCode().equalsIgnoreCase(accountcode)) {
//                    binding.selectbankspinner.setSelection(i);
//                    break;
//                }
//            }
        }
    }


}
