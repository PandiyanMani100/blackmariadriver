package com.blackmaria.partner.latestpackageview.view.Dashboard.Mytrips;


import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.blackmaria.partner.latestpackageview.Database.RidesDB;
import com.blackmaria.partner.latestpackageview.Model.Monthreport;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.newdesign.slider.CirclePageIndicator;
import com.blackmaria.partner.R;
import com.blackmaria.partner.databinding.ActivityMonthviewdetailConstrainBinding;
import com.blackmaria.partner.latestpackageview.Adapter.Monthviewdetailadapter;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Mytrips.MonthtripviewFactory;
import com.blackmaria.partner.latestpackageview.Model.monthtripviewpojo;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.mytrips.MonthviewdetailViewModel;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class MonthviewdetailConstrain extends AppCompatActivity {

    private ActivityMonthviewdetailConstrainBinding binding;
    private MonthviewdetailViewModel monthviewdetailViewModel;
    private RidesDB ridesDBobj;
    private ArrayList<Monthreport> weekwisetrippojos;
    private AppUtils appUtils;


    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(MonthviewdetailConstrain.this, R.layout.activity_monthviewdetail_constrain);
        monthviewdetailViewModel = ViewModelProviders.of(this, new MonthtripviewFactory(this)).get(MonthviewdetailViewModel.class);
        binding.setMonthviewdetailViewModel(monthviewdetailViewModel);
        monthviewdetailViewModel.setIds(binding);

        appUtils = new AppUtils(this);
        ridesDBobj = new RidesDB(MonthviewdetailConstrain.this);
        weekwisetrippojos = new ArrayList<>();

        weekwisetrippojos = ridesDBobj.getmonthtoweekridetable(getIntent().getStringExtra("startdate"), getIntent().getStringExtra("enddate"));
        if (weekwisetrippojos != null) {
            if (weekwisetrippojos.size() >= 4) {
                monthviewdetailViewModel.getmonthwiseapihit(getIntent().getStringExtra("month"), getIntent().getStringExtra("year"), false);
                if (weekwisetrippojos.get(0).getRidecount() != null) {
                    binding.monthtxt.setText(getIntent().getStringExtra("description").toString());
                    init_slidebanner(weekwisetrippojos);
                } else {
                    monthviewdetailViewModel.getmonthwiseapihit(getIntent().getStringExtra("month"), getIntent().getStringExtra("year"), true);
                }
            } else {
                monthviewdetailViewModel.getmonthwiseapihit(getIntent().getStringExtra("month"), getIntent().getStringExtra("year"), true);
            }
        } else {
            monthviewdetailViewModel.getmonthwiseapihit(getIntent().getStringExtra("month"), getIntent().getStringExtra("year"), true);
        }


        reponselistener();
    }

    private void reponselistener() {
        monthviewdetailViewModel.getMonthresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Type type = new TypeToken<monthtripviewpojo>() {
                    }.getType();
                    monthtripviewpojo object = new GsonBuilder().create().fromJson(response.toString(), type);
                    updateridesdb(object);
                    binding.monthtxt.setText(object.getResponse().getMonth() + " " + object.getResponse().getYear());
                    init_slidebanner(object.getResponse().getMonthreport());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void updateridesdb(final monthtripviewpojo object) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                for (int i = 0; i <= object.getResponse().getMonthreport().size() - 1; i++) {
                    long firstdateofmonth = appUtils.getdatetotiestampwithtime(object.getResponse().getMonthreport().get(i).getStartdate_f()+ " 00:00:00");
                    long lastdateofmonth = appUtils.getdatetotiestampwithtime(object.getResponse().getMonthreport().get(i).getEndate_f()+ " 24:00:00");
                    ridesDBobj.getupdateweekwiseridetable(String.valueOf(firstdateofmonth), String.valueOf(lastdateofmonth),
                            getIntent().getStringExtra("description").toString(), object.getResponse().getMonthreport().get(i).getDistance(), object.getResponse().getMonthreport().get(i).getRidecount());
                }
            }
        };
        thread.start();
    }

    private void init_slidebanner(ArrayList<Monthreport> object) {
        try {
            binding.pager.setAdapter(new Monthviewdetailadapter(MonthviewdetailConstrain.this, object, getIntent().getStringExtra("description")));
            CirclePageIndicator indicator = findViewById(R.id.indicator);
            indicator.setViewPager(binding.pager);
            final float density = getResources().getDisplayMetrics().density;
            indicator.setRadius(5 * density);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
