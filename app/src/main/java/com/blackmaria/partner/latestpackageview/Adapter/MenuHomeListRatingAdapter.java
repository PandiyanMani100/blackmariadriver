package com.blackmaria.partner.latestpackageview.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.latestpackageview.widgets.RoundedImageView;
import com.blackmaria.partner.latestpackageview.Model.ratingscommentslist;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Mytrips.TripSummaryConstrain;

import java.util.ArrayList;

public class MenuHomeListRatingAdapter extends BaseAdapter {
    private Context cxt;
    private LayoutInflater mInflater;
    private ArrayList<ratingscommentslist.Response.Review> itemlist;
    private String UserId = "";

    public MenuHomeListRatingAdapter(Context cxt, ArrayList<ratingscommentslist.Response.Review> itemlist, String userId) {
        this.cxt = cxt;
        this.itemlist = itemlist;
        mInflater = LayoutInflater.from(cxt);
        this.UserId = userId;
    }

    @Override
    public int getCount() {
        return itemlist.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    public class ViewHolder {
        private TextView title, comments,tapInfo;
        private RoundedImageView rating_page_profilephoto1;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.menu_ride_rating_list_single, parent, false);
            holder = new ViewHolder();

            holder.title =  view.findViewById(R.id.date_name);
            holder.comments =  view.findViewById(R.id.comments);
            holder.rating_page_profilephoto1 =  view.findViewById(R.id.rating_page_profilephoto1);
            holder.tapInfo =  view.findViewById(R.id.info_btn);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }
        AppUtils.setImageView(cxt, itemlist.get(position).getImage(), holder.rating_page_profilephoto1);

        holder.title.setText(itemlist.get(position).getUser_name() + " commented on " + itemlist.get(position).getReview_date());
        holder.comments.setText(itemlist.get(position).getComments());

        holder.tapInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String credittype = itemlist.get(position).getRide_id();
                if (!credittype.equalsIgnoreCase("")) {
                    Intent intent = new Intent(cxt, TripSummaryConstrain.class);
                    intent.putExtra("rideid", itemlist.get(position).getRide_id());
                    cxt.startActivity(intent);
                }
            }
        });

        return view;
    }
}