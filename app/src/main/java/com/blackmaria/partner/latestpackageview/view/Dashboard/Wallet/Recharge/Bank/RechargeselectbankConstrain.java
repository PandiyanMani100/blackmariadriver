package com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Recharge.Bank;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.view.View;

import com.blackmaria.partner.Interface.Slider;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.Adapter.Bankpaymentlistadapter;
import com.blackmaria.partner.databinding.ActivityRechargeselectbankConstrainBinding;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Wallet.RechargeselectbankpaymentFactory;
import com.blackmaria.partner.latestpackageview.Model.bankpaymentlistpojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.ChangepaymentConstrain;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.RechargepaymentsuccessConstrain;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet.RechargeselectbankViewModel;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

public class RechargeselectbankConstrain extends AppCompatActivity {

    private ActivityRechargeselectbankConstrainBinding binding;
    private RechargeselectbankViewModel rechargeselectbankViewModel;
    private AppUtils appUtils;
    private SessionManager sessionManager;
    private bankpaymentlistpojo pojo;
    private String insertAmount = "", insertAmounts = "", sDriverID;

    AccessLanguagefromlocaldb db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
        binding = DataBindingUtil.setContentView(RechargeselectbankConstrain.this, R.layout.activity_rechargeselectbank_constrain);
        rechargeselectbankViewModel = ViewModelProviders.of(this, new RechargeselectbankpaymentFactory(this)).get(RechargeselectbankViewModel.class);
        binding.setRechargeselectbankViewModel(rechargeselectbankViewModel);
        rechargeselectbankViewModel.setIds(binding);

        initView();
        setresponse();
        binding.datemonthyear.setText(db.getvalue("fastwallet"));
        binding.addcredit.setText(db.getvalue("addcredit"));
        binding.selectbank.setText(db.getvalue("selectbank"));
        binding.howtopay.setText(db.getvalue("howtopay"));
        binding.cancel.setText(db.getvalue("cancel_lable"));
    }

    private void initView() {
        appUtils = AppUtils.getInstance(this);
        sessionManager = SessionManager.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);

        if (getIntent().hasExtra("paymentlist")) {
            Type type = new TypeToken<bankpaymentlistpojo>() {
            }.getType();
            pojo = new GsonBuilder().create().fromJson(getIntent().getStringExtra("paymentlist").toString(), type);
            binding.creditamount.setText(getIntent().getStringExtra("currency") + " " + getIntent().getStringExtra("insertAmount"));
            insertAmount = getIntent().getStringExtra("insertAmount");
            insertAmounts = getIntent().getStringExtra("currency") + " " + getIntent().getStringExtra("insertAmount");
            init_paymentlist(pojo);
        }
    }

    private void init_paymentlist(bankpaymentlistpojo pojo) {
        binding.pager.setAdapter(new Bankpaymentlistadapter(slider, getApplicationContext(), pojo.getBank_list()));
        binding.indicator.setViewPager(binding.pager);
        binding.pager.setPageTransformer(false, new ViewPager.PageTransformer() {
                    @Override
                    public void transformPage(View page, float position) {
                        if (position < -1) {
                            page.setScaleY(0.7f);
                            page.setAlpha(1);
                        } else if (position <= 1) {
                            float scaleFactor = Math.max(0.7f, 1 - Math.abs(position - 0.14285715f));
                            page.setScaleX(scaleFactor);
                            page.setScaleY(scaleFactor);
                            page.setAlpha(scaleFactor);
                        } else {
                            page.setScaleY(0.7f);
                            page.setAlpha(1);
                        }
                    }
                }
        );
        binding.pager.setClipToPadding(false);
        binding.pager.setPadding(10, 0, 10, 0);
        binding.pager.setPageMargin(2);
        final float density = getResources().getDisplayMetrics().density;
        binding.indicator.setRadius(5 * density);
    }

    Slider slider = new Slider() {
        @Override
        public void onComplete(int position) {
            final String bankCode = pojo.getBank_list().get(position).getCode();

            final PkDialog mDialog = new PkDialog(RechargeselectbankConstrain.this);
            mDialog.setDialogTitle(db.getvalue("confirm_labels"));
            mDialog.setDialogMessage(db.getvalue("wantorelaodbank")+" "+pojo.getBank_list().get(position).getName());
            mDialog.setPositiveButton(db.getvalue("action_yes"), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                      mDialog.dismiss();
                        HashMap<String, String> jsonParams = new HashMap<String, String>();
                        jsonParams.put("driver_id", sDriverID);
                        jsonParams.put("total_amount", insertAmount);
                        jsonParams.put("bank_code", bankCode);
                        rechargeselectbankViewModel.bankpaymentdriver(Iconstant.xendit_bank_pay_url, jsonParams);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            mDialog.setNegativeButton(db.getvalue("action_no"), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                }
            });
            mDialog.show();

        }
    };


    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Log.v("TRIMMEMORY", " " + level);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }


    public void setresponse() {
        rechargeselectbankViewModel.getBankpaymentdriverresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    try {
                        JSONObject object = new JSONObject(response);
                        if (object.getString("status").equalsIgnoreCase("1")) {
                            Intent intent1 = new Intent(RechargeselectbankConstrain.this, RechargepaymentsuccessConstrain.class);
                            intent1.putExtra("json", jsonObject.toString());
                            intent1.putExtra("paymenttype", "bank");
                            startActivity(intent1);
                        } else {
                            Intent intent1 = new Intent(RechargeselectbankConstrain.this, ChangepaymentConstrain.class);
                            intent1.putExtra("amount", insertAmount);
                            intent1.putExtra("amounts", insertAmounts);
                            intent1.putExtra("msg", object.getString("msg"));
                            startActivity(intent1);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
