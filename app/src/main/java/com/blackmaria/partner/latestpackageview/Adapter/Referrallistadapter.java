package com.blackmaria.partner.latestpackageview.Adapter;

import android.content.Context;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.latestpackageview.Model.Rides;
import com.blackmaria.partner.latestpackageview.widgets.RoundedImageView;
import com.blackmaria.partner.latestpackageview.Model.todaytripviewdetailpojo;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

public class Referrallistadapter extends BaseAdapter {

    private ArrayList<Rides> data;
    private LayoutInflater mInflater;
    private Context context;

    public Referrallistadapter(Context context, ArrayList<Rides> rides) {
        this.context = context;
        this.data = rides;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    public class ViewHolder {
        private TextView username, signupdate, location;
        private CardView viewall_list_layout;
        private RoundedImageView userimageview;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.referrallistadapter, parent, false);
            holder = new ViewHolder();
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        holder.userimageview = view.findViewById(R.id.userimageview);
        holder.username = view.findViewById(R.id.username);
        holder.signupdate = view.findViewById(R.id.signupdate);
        holder.location = view.findViewById(R.id.location);
        holder.viewall_list_layout = view.findViewById(R.id.viewall_list_layout);
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.profileicon_dashboard)
                .error(R.drawable.profileicon_dashboard)
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        Glide.with(context).load("").apply(options).into(holder.userimageview);

        return view;
    }

}
