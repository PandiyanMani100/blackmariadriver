package com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityChangepaymentConstrainBinding;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Dashboard_constrain;

import java.util.HashMap;

public class ChangepaymentViewModel extends ViewModel implements ApIServices.completelisner {
    private Activity context;
    private ActivityChangepaymentConstrainBinding binding;
    private Dialog dialog;
    private AppUtils appUtils;
    private SessionManager sessionManager;
    private String sDriverID = "";
    private MutableLiveData<String> changepaymentretry = new MutableLiveData<>();


    public ChangepaymentViewModel(Activity context) {
        this.context = context;
        sessionManager = SessionManager.getInstance(context);
        appUtils = AppUtils.getInstance(context);
    }

    public MutableLiveData<String> getChangepaymentretry() {
        return changepaymentretry;
    }

    public void changepaymenthitretry(String url, HashMap<String, String> jsonParams) {
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        ApIServices apIServices = new ApIServices(context, ChangepaymentViewModel.this);
        apIServices.dooperation(url, jsonParams);
    }


    @Override
    public void sucessresponse(String val) {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        changepaymentretry.postValue(val);
    }

    @Override
    public void errorreponse() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void jsonexception() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    public void setIds(ActivityChangepaymentConstrainBinding binding) {
        this.binding = binding;
    }

    public void back() {
        context.onBackPressed();
    }

    public void cancel() {
        Intent i = new Intent(context, Dashboard_constrain.class);
        context.startActivity(i);
        context.finish();
    }
}
