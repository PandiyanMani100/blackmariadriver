package com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import android.content.Intent;

import com.blackmaria.partner.databinding.ActivityRechargepaymentsuccessConstrainBinding;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Dashboard_constrain;

public class RechargepaymentsuccessViewModel extends ViewModel {
    private Activity context;
    private ActivityRechargepaymentsuccessConstrainBinding binding;

    public RechargepaymentsuccessViewModel(Activity context) {
        this.context = context;
    }

    public void setIds(ActivityRechargepaymentsuccessConstrainBinding binding) {
        this.binding = binding;
    }

    public void close() {
        Intent i = new Intent(context, Dashboard_constrain.class);
        context.startActivity(i);
    }

}
