package com.blackmaria.partner.latestpackageview.Adapter;

import android.content.Context;
import android.os.Parcelable;

import androidx.viewpager.widget.PagerAdapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackmaria.partner.Interface.Slider;
import com.blackmaria.partner.Pojo.RegisterDriverLocationCatCarPojo;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

public class SlidingImage_Adapter_homepage extends PagerAdapter {


    private ArrayList<RegisterDriverLocationCatCarPojo> IMAGES;
    private LayoutInflater inflater;
    private Context context;
    private Slider slider;
    AccessLanguagefromlocaldb db;


    public SlidingImage_Adapter_homepage(Slider slider, Context context, ArrayList<RegisterDriverLocationCatCarPojo> IMAGES) {
        this.slider = slider;
        this.context = context;
        this.IMAGES = IMAGES;
        db = new AccessLanguagefromlocaldb(context);
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return IMAGES.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View imageLayout = inflater.inflate(R.layout.homepage_custombanner_layout, view, false);

        assert imageLayout != null;
        final ImageView imageView = imageLayout.findViewById(R.id.iv_car);
        final TextView carname = imageLayout.findViewById(R.id.carname);
        String passender = "";

        if (Integer.parseInt(IMAGES.get(position).getSeatsCount()) > 1) {
            passender = IMAGES.get(position).getSeatsCount() +" " +db.getvalue("passen");
        } else {
            passender = IMAGES.get(position).getSeatsCount() + " "+db.getvalue("passen");
        }

        carname.setText(IMAGES.get(position).getCatName() +" "+ db.getvalue("whh")+" \n"+ IMAGES.get(position).getCc() + " - " +passender);

        RequestOptions options = new RequestOptions()
                .centerCrop()
                .fitCenter()
                .placeholder(R.drawable.new_no_user_img)
                .error(R.drawable.new_no_user_img)
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        Glide.with(context).load(IMAGES.get(position).getCatIcon()).apply(options).into(imageView);
        view.addView(imageLayout, 0);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                slider.onComplete(position);
            }
        });
        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


}