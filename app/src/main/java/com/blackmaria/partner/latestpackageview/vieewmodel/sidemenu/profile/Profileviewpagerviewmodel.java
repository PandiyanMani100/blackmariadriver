package com.blackmaria.partner.latestpackageview.vieewmodel.sidemenu.profile;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import com.blackmaria.partner.R;
import com.blackmaria.partner.databinding.ProfileviewpagerlayoutBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;

import java.util.HashMap;

public class Profileviewpagerviewmodel extends ViewModel implements ApIServices.completelisner {
    private Activity context;
    private ProfileviewpagerlayoutBinding binding;
    private Dialog dialog;
    private int count = 0;

    private MutableLiveData<String> changeemailresponse = new MutableLiveData<>();
    private MutableLiveData<String> changemobileresponse = new MutableLiveData<>();
    private MutableLiveData<String> changeemailupdateresponse = new MutableLiveData<>();
    private MutableLiveData<String> changemobileupdateresponse = new MutableLiveData<>();
    private MutableLiveData<String> getprofileview = new MutableLiveData<>();

    public Profileviewpagerviewmodel(Activity context) {
        this.context = context;
    }

    public void edit() {
        binding.myloginDialogFormEmailidEdittext.setEnabled(true);
        binding.myloginDialogFormMobileNoEdittext.setEnabled(true);
        binding.myloginDialogFormCountryCodeTextview.setEnabled(true);
        binding.updatelogin.setClickable(true);
        binding.updatelogin.setEnabled(true);
        binding.updatelogin.setVisibility(View.VISIBLE);
    }

    public MutableLiveData<String> getChangeemailresponse() {
        return changeemailresponse;
    }

    public MutableLiveData<String> getChangemobileresponse() {
        return changemobileresponse;
    }

    public MutableLiveData<String> getChangeemailupdateresponse() {
        return changeemailupdateresponse;
    }

    public MutableLiveData<String> getChangemobileupdateresponse() {
        return changemobileupdateresponse;
    }

    public MutableLiveData<String> getGetprofileview() {
        return getprofileview;
    }

    public void setIds(ProfileviewpagerlayoutBinding binding) {
        this.binding = binding;
    }

    public void changeemail(String url, HashMap<String, String> jsonParams) {
        count = 0;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        ApIServices apIServices = new ApIServices(context, Profileviewpagerviewmodel.this);
        apIServices.dooperation(url, jsonParams);
    }

    public void changemobile(String url, HashMap<String, String> jsonParams) {
        count = 1;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        ApIServices apIServices = new ApIServices(context, Profileviewpagerviewmodel.this);
        apIServices.dooperation(url, jsonParams);
    }

    @Override
    public void sucessresponse(String val) {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        if (count == 0) {
            changeemailresponse.postValue(val);
        } else if (count == 1) {
            changemobileresponse.postValue(val);
        } else if (count == 2) {
            changeemailupdateresponse.postValue(val);
        } else if (count == 3) {
            changemobileupdateresponse.postValue(val);
        } else if (count == 4) {
            getprofileview.postValue(val);
        }
    }

    @Override
    public void errorreponse() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void jsonexception() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        AppUtils.toastprint(context, "Json Exception");
    }

    public void getprofile(String driverid) {
        count = 4;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", driverid);
        ApIServices apIServices = new ApIServices(context, Profileviewpagerviewmodel.this);
        apIServices.dooperation(Iconstant.Profile_driver_profile_Url, jsonParams);
    }

    public void getemailupdatewithcode(String url, HashMap<String, String> jsonParams) {
        count = 2;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        ApIServices apIServices = new ApIServices(context, Profileviewpagerviewmodel.this);
        apIServices.dooperation(url, jsonParams);
    }

    public void updatemobilewithcode(String url, HashMap<String, String> jsonParams) {
        count = 3;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        ApIServices apIServices = new ApIServices(context, Profileviewpagerviewmodel.this);
        apIServices.dooperation(url, jsonParams);
    }
}
