package com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet.withdraw.cash;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.ViewModel;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;

import com.blackmaria.partner.R;
//import com.blackmaria.partner.databinding.ActivityCashwithdrawlocationchooseConstrianBinding;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;

import java.util.HashMap;

public class CashwithdrawlocationchooseViewModel extends ViewModel implements ApIServices.completelisner {
    private Activity context;
    private Dialog dialog;
//    private ActivityCashwithdrawlocationchooseConstrianBinding binding;

    public CashwithdrawlocationchooseViewModel(Activity context) {
        this.context = context;
    }

    public void getavailablelocations(String url, HashMap<String, String> jsonParams) {
//        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        ApIServices apIServices = new ApIServices(context, CashwithdrawlocationchooseViewModel.this);
        apIServices.dooperation(url, jsonParams);
    }

    @Override
    public void sucessresponse(String val) {
//        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
    }

    @Override
    public void errorreponse() {
//        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
    }

    @Override
    public void jsonexception() {
//        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
    }

//    public void setIds(ActivityCashwithdrawlocationchooseConstrianBinding binding) {
//        this.binding = binding;
//    }

    public void back() {
        context.onBackPressed();
    }
}
