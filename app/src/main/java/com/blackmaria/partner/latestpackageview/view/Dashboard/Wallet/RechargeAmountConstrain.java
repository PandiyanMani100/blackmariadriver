package com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet;


import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;

import androidx.databinding.DataBindingUtil;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityRechargeAmountConstrainBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Wallet.RechargeamountFactory;
import com.blackmaria.partner.latestpackageview.Model.rechargeamountpojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet.RechargeamountViewModel;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

public class RechargeAmountConstrain extends AppCompatActivity {

    private ActivityRechargeAmountConstrainBinding binding;
    private RechargeamountViewModel rechargeamountViewModel;
    private SessionManager sessionManager;
    private String sDriverID = "";
    private rechargeamountpojo pojo;
    private JSONObject json;
    private AppUtils appUtils;
    AccessLanguagefromlocaldb db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
        binding = DataBindingUtil.setContentView(RechargeAmountConstrain.this, R.layout.activity_recharge_amount_constrain);
        rechargeamountViewModel = ViewModelProviders.of(this, new RechargeamountFactory(this)).get(RechargeamountViewModel.class);
        binding.setRechargeamountViewModel(rechargeamountViewModel);
        rechargeamountViewModel.setIds(binding);

        initView();
        setResponse();
        clicklistener();

        binding.datemonthyear.setText(db.getvalue("fastwallet"));
        binding.insertamount.setHint(db.getvalue("insertamountrecharge"));
        binding.confirminsertamount.setText(db.getvalue("confirm_label"));

        binding.preselectamount.setText(db.getvalue("preselectamount"));
        binding.havevoucher.setText(db.getvalue("havevoucher"));
        binding.insertvoucher.setHint(db.getvalue("insertvoucher"));

        binding.okconfirm.setText(db.getvalue("ok_lable"));
        binding.weaccept.setText(db.getvalue("weaccept"));
    }

    private void initView() {
        sessionManager = SessionManager.getInstance(this);
        appUtils = AppUtils.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        rechargeamountViewModel.getmoneypagedriver(Iconstant.wallet_money_url, jsonParams);
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Log.v("TRIMMEMORY", " " + level);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }


    private void setResponse() {
        rechargeamountViewModel.getGetmoneypagedriverresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {

                    json = new JSONObject(response);
                    if(json.getString("status").equals("0"))
                    {
                        Toast.makeText(getApplicationContext(),json.getString("response"),Toast.LENGTH_LONG).show();
                        finish();
                    }
                    else
                    {
                        sessionManager.setpaymentlist(json.toString());
                        Type type = new TypeToken<rechargeamountpojo>() {
                        }.getType();
                        pojo = new GsonBuilder().create().fromJson(json.toString(), type);
                        rechargeamountViewModel.setpojo(json);
                        settext(pojo);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void settext(rechargeamountpojo pojo) {
        sessionManager.setXenditPublicKey(pojo.getResponse().getXendit_public_key());
        sessionManager.setcurrencyconversionKey(pojo.getResponse().getExchange_value());
        binding.amount.setText(pojo.getResponse().getCurrency() + " " + pojo.getResponse().getRecharge_boundary().getWallet_amount_one());
        binding.amount1.setText(pojo.getResponse().getCurrency() + " " + pojo.getResponse().getRecharge_boundary().getWallet_amount_two());
        binding.amount2.setText(pojo.getResponse().getCurrency() + " " + pojo.getResponse().getRecharge_boundary().getWallet_amount_three());
        binding.amount3.setText(pojo.getResponse().getCurrency() + " " + pojo.getResponse().getRecharge_boundary().getWallet_amount_four());
    }

    private void clicklistener() {
        binding.confirminsertamount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.insertamount.getText().toString().length() == 0) {
                    appUtils.AlertError(RechargeAmountConstrain.this, db.getvalue("action_error"), db.getvalue("rechargeamountempty"));
                } else if (Integer.parseInt(binding.insertamount.getText().toString()) == 0) {
                    appUtils.AlertError(RechargeAmountConstrain.this, db.getvalue("action_error"), db.getvalue("rechargeamountemptyzero"));
                } else {
                    Intent intent = new Intent(RechargeAmountConstrain.this, Rechargechoosepayment.class);
                    intent.putExtra("insertAmount", binding.insertamount.getText().toString().trim());
                    intent.putExtra("jsonpojo", json.toString());
                    startActivity(intent);
                }
            }
        });
    }
}
