package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class moneytransferpojo implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;

    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }

    public class Response {
        @SerializedName("driver_name")
        private String driver_name;
        @SerializedName("image")
        private String image;
        @SerializedName("city")
        private String city;
        @SerializedName("dail_code")
        private String dail_code;
        @SerializedName("mobile_number")
        private String mobile_number;
        @SerializedName("status")
        private String status;
        @SerializedName("driver_id")
        private String driver_id;
        @SerializedName("cross_border")
        private String cross_border;


        // Getter Methods

        public String getDriver_name() {
            return driver_name;
        }

        public String getImage() {
            return image;
        }

        public String getCity() {
            return city;
        }

        public String getDail_code() {
            return dail_code;
        }

        public String getMobile_number() {
            return mobile_number;
        }

        public String getStatus() {
            return status;
        }

        public String getDriver_id() {
            return driver_id;
        }

        public String getCross_border() {
            return cross_border;
        }

        // Setter Methods

        public void setDriver_name(String driver_name) {
            this.driver_name = driver_name;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public void setDail_code(String dail_code) {
            this.dail_code = dail_code;
        }

        public void setMobile_number(String mobile_number) {
            this.mobile_number = mobile_number;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public void setDriver_id(String driver_id) {
            this.driver_id = driver_id;
        }

        public void setCross_border(String cross_border) {
            this.cross_border = cross_border;
        }
    }
}


