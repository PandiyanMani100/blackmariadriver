package com.blackmaria.partner.latestpackageview.view.sidemenu.Ratings;


import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.Nullable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;

import com.blackmaria.partner.R;
import com.blackmaria.partner.latestpackageview.Adapter.MenuHomeListRatingAdapter;
import com.blackmaria.partner.databinding.ActivityCommentslistConstrainBinding;
import com.blackmaria.partner.latestpackageview.Factory.Sidemenu.Ratings.CommentslistFactory;
import com.blackmaria.partner.latestpackageview.Model.ratingscommentslist;
import com.blackmaria.partner.latestpackageview.vieewmodel.sidemenu.ratings.CommentslistViewModel;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;

public class Commentslist_constrain extends AppCompatActivity {

    private ActivityCommentslistConstrainBinding binding;
    private CommentslistViewModel commentslistViewModel;
    private MenuHomeListRatingAdapter adapter;


    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(Commentslist_constrain.this, R.layout.activity_commentslist_constrain);
        commentslistViewModel = ViewModelProviders.of(this, new CommentslistFactory(this)).get(CommentslistViewModel.class);
        binding.setCommentslistViewModel(commentslistViewModel);

        commentslistViewModel.setIds(binding);

        listener();
    }

    private void listener() {
        binding.imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        commentslistViewModel.getCommentslist().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    Type type = new TypeToken<ratingscommentslist>() {
                    }.getType();
                    ratingscommentslist ratingsobj = new GsonBuilder().create().fromJson(response.toString(), type);
                    if (ratingsobj.getStatus().equalsIgnoreCase("1")) {
                        binding.totalreviewscount.setText("Total "+ratingsobj.getResponse().getTotal_record()+" Reviews");
                        int currentPage = Integer.parseInt(ratingsobj.getResponse().getCurrent_page());
                        String str_NextPage = ratingsobj.getResponse().getNext_page();
                        if (str_NextPage.equalsIgnoreCase("")) {
                            binding.nextpage.setVisibility(View.GONE);
                        } else {
                            binding.nextpage.setVisibility(View.VISIBLE);
                        }
                        if (currentPage == 1) {
                            binding.prevpage.setVisibility(View.GONE);
                        } else {
                            binding.prevpage.setVisibility(View.VISIBLE);
                        }
                        binding.yearmonth.setText(ratingsobj.getResponse().getDate());
                        adapter = new MenuHomeListRatingAdapter(Commentslist_constrain.this, ratingsobj.getResponse().getReview(), commentslistViewModel.getDriverid());
                        binding.menulistview.setAdapter(adapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
