package com.blackmaria.partner.latestpackageview.vieewmodel.signup;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import android.content.Intent;
import android.os.Handler;
import androidx.viewpager.widget.ViewPager;
import android.widget.RelativeLayout;

import com.blackmaria.partner.Interface.Slider;
import com.blackmaria.partner.newdesign.slider.CirclePageIndicator;
import com.blackmaria.partner.R;

import com.blackmaria.partner.latestpackageview.Adapter.Signinsignup_adapter;
import com.blackmaria.partner.latestpackageview.view.login.Signin_contrain;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class SignupAndSignInPageViewModel extends ViewModel  {

    private Activity context;
    private RelativeLayout Rl_Login;
    private ViewPager mPager;
    private CirclePageIndicator circlePageIndicator;
    private int currentPage = 0;
    private int NUM_PAGES = 0;
    private Timer swipeTimer;

    public SignupAndSignInPageViewModel(Activity context) {
        this.context = context;
    }

    public void sliderMethod(ViewPager mSlider, CirclePageIndicator pagerIndicator, RelativeLayout Rl_Login, Activity signupAndSignInPage_constrain) {
        this.mPager = mSlider;
        this.circlePageIndicator = pagerIndicator;
        this.Rl_Login = Rl_Login;
        ArrayList<Integer> productImage = new ArrayList<Integer>();
        productImage.add(R.drawable.slide1);
        productImage.add(R.drawable.slide2);
        productImage.add(R.drawable.slide3);
        productImage.add(R.drawable.slide4);
        productImage.add(R.drawable.slide5);

        mPager.setAdapter(new Signinsignup_adapter(slider, context.getApplicationContext(), productImage));
        circlePageIndicator.setViewPager(mPager);
        final float density = context.getResources().getDisplayMetrics().density;
        circlePageIndicator.setRadius(6 * density);
        circlePageIndicator.setStrokeWidth(2);

        NUM_PAGES = productImage.size();

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 2500, 2500);

        // Pager listener over indicator
        circlePageIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });
    }

    Slider slider = new Slider() {
        @Override
        public void onComplete(int position) {

        }
    };


    public void stopscrolling() {

    }

    public void movetologin() {
        Intent intent = new Intent(context, Signin_contrain.class);
        context.startActivity(intent);
        context.finish();
        swipeTimer.cancel();
    }
}
