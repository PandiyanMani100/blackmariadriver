package com.blackmaria.partner.latestpackageview.view.Dashboard.Mytrips;


import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;

import androidx.databinding.DataBindingUtil;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityMytripsPaginationConstrainBinding;
import com.blackmaria.partner.latestpackageview.Database.RidesDB;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Mytrips.TodayviewdetailsFactory;
import com.blackmaria.partner.latestpackageview.Model.Monthridepojodetailsfromdb;
import com.blackmaria.partner.latestpackageview.Model.monthtrippojo;
import com.blackmaria.partner.latestpackageview.Model.todaytrippojo;
import com.blackmaria.partner.latestpackageview.Model.weektrippojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.mytrips.TodaytripsdetailsViewModel;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class MytripsPagination_constrain extends AppCompatActivity implements View.OnClickListener {

    private ActivityMytripsPaginationConstrainBinding binding;
    private TodaytripsdetailsViewModel todaytripsdetailsViewModel;
    private Calendar calendartoday, calendar_week, calendar_month;
    public SimpleDateFormat mdFormat, monthFormat, yearFormat;
    private weektrippojo object_week;
    private monthtrippojo object_month;
    private todaytrippojo object_today;
    private String sFilterDate = "", sFilterMonth = "", sFilterYear = "", sFilterDateFrom = "", sFilterDateTo = "", type = "", driverid = "";
    private RidesDB ridesDBobj;
    AccessLanguagefromlocaldb db;
    private AppUtils appUtils;
    private SessionManager sessionManager;
    private long startdateofmonth, enddateofmonth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
        binding = DataBindingUtil.setContentView(MytripsPagination_constrain.this, R.layout.activity_mytrips_pagination_constrain);
        todaytripsdetailsViewModel = ViewModelProviders.of(this, new TodayviewdetailsFactory(this)).get(TodaytripsdetailsViewModel.class);
        binding.setTodaytripsdetailsViewModel(todaytripsdetailsViewModel);

        todaytripsdetailsViewModel.setIds(binding);
        initView();
        binding.mytripstxt.setText(db.getvalue("mytrips"));
        binding.completedtxt.setText(db.getvalue("completed"));
        binding.canceltxt.setText(db.getvalue("cancel_lable"));
        binding.drivercanceltxt.setText(db.getvalue("drivercancel"));
        binding.viewdetails.setText(db.getvalue("viewdetails"));
    }

    private void initView() {
        monthFormat = new SimpleDateFormat("MM");
        yearFormat = new SimpleDateFormat("yyyy");
        type = getIntent().getStringExtra("type");
        sessionManager = SessionManager.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        driverid = user.get(SessionManager.KEY_DRIVERID);
        appUtils = new AppUtils(MytripsPagination_constrain.this);
        ridesDBobj = new RidesDB(MytripsPagination_constrain.this);


        if (getIntent().getStringExtra("type").equalsIgnoreCase("1")) {
            calendartoday = Calendar.getInstance();
            mdFormat = new SimpleDateFormat("yyyy-MM-dd");
            sFilterDate = mdFormat.format(calendartoday.getTime());

            long startdateofmonth = appUtils.getdatetotiestampwithtime(sFilterDate+ " 00:00:00");
            long enddateofmonth = appUtils.getdatetotiestampwithtime(sFilterDate+ " 24:00:00");

            try {
                Monthridepojodetailsfromdb object_month = ridesDBobj.getmytripsdaywise(String.valueOf(startdateofmonth), String.valueOf(enddateofmonth));
                if (object_month != null) {
                    if (object_month.getCompletedtrip() != null) {
                        todaytripsdetailsViewModel.todaytripsapi(sFilterDate, false);
                        binding.daytxt.setText("TODAY");
                        binding.datetxt.setText(object_month.getMonthdescription());
                        binding.completedcount.setText(object_month.getCompletedtrip());
                        binding.cancelcount.setText(object_month.getCanceltrip());
                        binding.drivercancelcount.setText(object_month.getDrivercanceltrip());
                    } else {
                        todaytripsdetailsViewModel.todaytripsapi(sFilterDate, true);
                    }
                } else {
                    todaytripsdetailsViewModel.todaytripsapi(sFilterDate, true);
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

        } else if (getIntent().getStringExtra("type").equalsIgnoreCase("2")) {
            calendar_week = Calendar.getInstance();
            calendar_week.set(Calendar.DAY_OF_WEEK, 1);
            mdFormat = new SimpleDateFormat("yyyy-MM-dd");
            sFilterDateFrom = mdFormat.format(calendar_week.getTime());
            calendar_week.set(Calendar.DAY_OF_WEEK, 7);
            sFilterDateTo = mdFormat.format(calendar_week.getTime());

            long startdateofmonth = appUtils.getdatetotiestampwithtime(sFilterDateFrom+ " 00:00:00");
            long enddateofmonth = appUtils.getdatetotiestampwithtime(sFilterDateTo+ " 24:00:00");

            try {
                Monthridepojodetailsfromdb object_month = ridesDBobj.getmytripsweekwise(String.valueOf(startdateofmonth), String.valueOf(enddateofmonth));
                if (object_month != null) {
                    if (object_month.getCompletedtrip() != null) {
                        todaytripsdetailsViewModel.weektripsapi(sFilterDateFrom, sFilterDateTo, false);
                        binding.daytxt.setText("WEEK");
                        binding.datetxt.setText(object_month.getMonthdescription());
                        binding.completedcount.setText(object_month.getCompletedtrip());
                        binding.cancelcount.setText(object_month.getCanceltrip());
                        binding.drivercancelcount.setText(object_month.getDrivercanceltrip());
                    } else {
                        todaytripsdetailsViewModel.weektripsapi(sFilterDateFrom, sFilterDateTo, true);
                    }
                } else {
                    todaytripsdetailsViewModel.weektripsapi(sFilterDateFrom, sFilterDateTo, true);
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }


        } else if (getIntent().getStringExtra("type").equalsIgnoreCase("3")) {
            calendar_month = Calendar.getInstance();
            mdFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat monthFormat = new SimpleDateFormat("MM");
            SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
            sFilterMonth = monthFormat.format(calendar_month.getTime());
            sFilterYear = yearFormat.format(calendar_month.getTime());

            startdateofmonth = appUtils.getstartdateofmonth(sFilterMonth, sFilterYear);
            startdateofmonth =   appUtils.getdatetotiestampwithtime(ridesDBobj.getDate(String.valueOf(startdateofmonth))+ " 00:00:00");

            enddateofmonth = appUtils.getenddateofmonth(sFilterMonth, sFilterYear);
            enddateofmonth =   appUtils.getdatetotiestampwithtime(ridesDBobj.getDate(String.valueOf(enddateofmonth))+ " 24:00:00");

            try {
                Monthridepojodetailsfromdb object_month = ridesDBobj.getmytripsmonthwise(String.valueOf(startdateofmonth), String.valueOf(enddateofmonth));
                if (object_month != null) {
                    if (object_month.getCompletedtrip() != null) {
                        todaytripsdetailsViewModel.monthtrips(sFilterMonth, sFilterYear, false);
                        binding.daytxt.setText("MONTH");
                        binding.datetxt.setText(object_month.getMonthdescription());
                        binding.completedcount.setText(object_month.getCompletedtrip());
                        binding.cancelcount.setText(object_month.getCanceltrip());
                        binding.drivercancelcount.setText(object_month.getDrivercanceltrip());
                    } else {
                        todaytripsdetailsViewModel.monthtrips(sFilterMonth, sFilterYear, true);
                    }
                } else {
                    todaytripsdetailsViewModel.monthtrips(sFilterMonth, sFilterYear, true);
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

        }

        binding.ivleft.setOnClickListener(this);
        binding.ivright.setOnClickListener(this);
        binding.viewdetails.setOnClickListener(this);

        responselistener();
    }

    private void responselistener() {

        todaytripsdetailsViewModel.getTodaytrips().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Type type = new TypeToken<todaytrippojo>() {
                    }.getType();
                    object_today = new GsonBuilder().create().fromJson(response.toString(), type);
                    long firstdateofmonth = appUtils.getdatetotiestampwithtime(sFilterDate+ " 00:00:00");
                    long enddateofmonth = appUtils.getdatetotiestampwithtime(sFilterDate+ " 24:00:00");

                    ridesDBobj.insertmytrips_day(driverid, String.valueOf(firstdateofmonth), String.valueOf(enddateofmonth),
                            object_today.getResponse().getToday_date(), object_today.getResponse().getToday().getCompleted_rides()
                            , object_today.getResponse().getToday().getBidding_trip(), object_today.getResponse().getToday().getSos_trip()
                            , object_today.getResponse().getToday().getCancel_rides(), object_today.getResponse().getToday().getDriver_cancel_rides(), "", "");

                    Date c = Calendar.getInstance().getTime();
                    SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
                    String formattedDate = "As "+df.format(c);

                    if(formattedDate.equalsIgnoreCase(object_today.getResponse().getToday_date()))
                    {
                        binding.daytxt.setText("TODAY");
                    }
                    else
                    {
                        binding.daytxt.setText("DAY");
                    }

                    binding.datetxt.setText(object_today.getResponse().getToday_date());
                    binding.completedcount.setText(object_today.getResponse().getToday().getCompleted_rides());
                    binding.cancelcount.setText(object_today.getResponse().getToday().getCancel_rides());
                    binding.drivercancelcount.setText(object_today.getResponse().getToday().getDriver_cancel_rides());
                } catch (JSONException e) {

                }
            }
        });

        todaytripsdetailsViewModel.getWeektrips().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Type type = new TypeToken<weektrippojo>() {
                    }.getType();
                    object_week = new GsonBuilder().create().fromJson(response.toString(), type);
                    long firstdateofmonth = appUtils.getdatetotiestampwithtime(object_week.getResponse().getFrom_date()+ " 00:00:00");
                    long lastdateofmonth = appUtils.getdatetotiestampwithtime(object_week.getResponse().getTo_date()+ " 24:00:00");

                    ridesDBobj.insertmytrips_week(driverid, String.valueOf(firstdateofmonth), String.valueOf(lastdateofmonth),
                            object_week.getResponse().getWeek_desc(), object_week.getResponse().getWeek().getCompleted_rides()
                            , object_week.getResponse().getWeek().getBidding_trip(), object_week.getResponse().getWeek().getSos_trip()
                            , object_week.getResponse().getWeek().getCancel_rides(), object_week.getResponse().getWeek().getDriver_cancel_rides());

                    binding.daytxt.setText("WEEK");
                    binding.datetxt.setText(object_week.getResponse().getWeek_desc());
                    binding.completedcount.setText(object_week.getResponse().getWeek().getCompleted_rides());
                    binding.cancelcount.setText(object_week.getResponse().getWeek().getCancel_rides());
                    binding.drivercancelcount.setText(object_week.getResponse().getWeek().getDriver_cancel_rides());
                } catch (JSONException e) {

                }
            }
        });

        todaytripsdetailsViewModel.getMonthtrips().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Type type = new TypeToken<monthtrippojo>() {
                    }.getType();
                    object_month = new GsonBuilder().create().fromJson(response.toString(), type);
                    //local db store
                    long from_dates = appUtils.getdatetotiestampwithtime(object_month.getResponse().getFrom_date()+ " 00:00:00");
                    long to_dates = appUtils.getdatetotiestampwithtime(object_month.getResponse().getTo_date()+ " 24:00:00");

                    ridesDBobj.insertmytrips_month(driverid, String.valueOf(from_dates), String.valueOf(to_dates),
                            object_month.getResponse().getMonth_desc(), object_month.getResponse().getMonth().getCompleted_rides()
                            , object_month.getResponse().getMonth().getBidding_trip(), object_month.getResponse().getMonth().getSos_trip()
                            , object_month.getResponse().getMonth().getCancel_rides(), object_month.getResponse().getMonth().getDriver_cancel_rides());

                    binding.daytxt.setText("MONTH");
                    binding.datetxt.setText(object_month.getResponse().getMonth_desc());
                    binding.completedcount.setText(object_month.getResponse().getMonth().getCompleted_rides());
                    binding.cancelcount.setText(object_month.getResponse().getMonth().getCancel_rides());
                    binding.drivercancelcount.setText(object_month.getResponse().getMonth().getDriver_cancel_rides());
                } catch (JSONException e) {

                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivleft:
                if (type.equalsIgnoreCase("1")) {
                    Date date = null;
                    try {
                        date = mdFormat.parse(sFilterDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if (date != null) {
                        calendartoday.setTime(date);
                        calendartoday.add(Calendar.DATE, -1);
                        sFilterDate = mdFormat.format(calendartoday.getTime());
                    }

                    long startdateofmonth = appUtils.getdatetotiestampwithtime(sFilterDate+ " 00:00:00");
                    long enddateofmonth = appUtils.getdatetotiestampwithtime(sFilterDate+ " 24:00:00");

                    try {
                        Monthridepojodetailsfromdb object_month = ridesDBobj.getmytripsdaywise(String.valueOf(startdateofmonth), String.valueOf(enddateofmonth));
                        if (object_month != null) {
                            if (object_month.getCompletedtrip() != null) {
                                todaytripsdetailsViewModel.todaytripsapi(sFilterDate, false);
                                binding.daytxt.setText("DAY");
                                binding.datetxt.setText(object_month.getMonthdescription());
                                binding.completedcount.setText(object_month.getCompletedtrip());
                                binding.cancelcount.setText(object_month.getCanceltrip());
                                binding.drivercancelcount.setText(object_month.getDrivercanceltrip());
                            } else {
                                todaytripsdetailsViewModel.todaytripsapi(sFilterDate, true);
                            }
                        } else {
                            todaytripsdetailsViewModel.todaytripsapi(sFilterDate, true);
                        }
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }

                } else if (type.equalsIgnoreCase("2")) {
                    calendar_week.add(Calendar.DATE, -7);
                    calendar_week.set(Calendar.DAY_OF_WEEK, 1);
                    sFilterDateFrom = mdFormat.format(calendar_week.getTime());
                    calendar_week.set(Calendar.DAY_OF_WEEK, 7);
                    sFilterDateTo = mdFormat.format(calendar_week.getTime());
//                    todaytripsdetailsViewModel.weektripsapi(sFilterDateFrom, sFilterDateTo);
                    long startdateofmonth = appUtils.getdatetotiestampwithtime(sFilterDateFrom+ " 00:00:00");
                    long enddateofmonth = appUtils.getdatetotiestampwithtime(sFilterDateTo+ " 24:00:00");

                    try {
                        Monthridepojodetailsfromdb object_month = ridesDBobj.getmytripsweekwise(String.valueOf(startdateofmonth), String.valueOf(enddateofmonth));
                        if (object_month != null) {
                            if (object_month.getCompletedtrip() != null) {
                                todaytripsdetailsViewModel.weektripsapi(sFilterDateFrom, sFilterDateTo, false);
                                binding.daytxt.setText("WEEK");
                                binding.datetxt.setText(object_month.getMonthdescription());
                                binding.completedcount.setText(object_month.getCompletedtrip());
                                binding.cancelcount.setText(object_month.getCanceltrip());
                                binding.drivercancelcount.setText(object_month.getDrivercanceltrip());
                            } else {
                                todaytripsdetailsViewModel.weektripsapi(sFilterDateFrom, sFilterDateTo, true);
                            }
                        } else {
                            todaytripsdetailsViewModel.weektripsapi(sFilterDateFrom, sFilterDateTo, true);
                        }
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }


                } else if (type.equalsIgnoreCase("3")) {
                    calendar_month.add(Calendar.MONTH, -1);
                    sFilterMonth = monthFormat.format(calendar_month.getTime());
                    sFilterYear = yearFormat.format(calendar_month.getTime());
//                    todaytripsdetailsViewModel.monthtrips(sFilterMonth, sFilterYear);

                    startdateofmonth = appUtils.getstartdateofmonth(sFilterMonth, sFilterYear);
                    startdateofmonth =   appUtils.getdatetotiestampwithtime(ridesDBobj.getDate(String.valueOf(startdateofmonth))+ " 00:00:00");

                    enddateofmonth = appUtils.getenddateofmonth(sFilterMonth, sFilterYear);
                    enddateofmonth =   appUtils.getdatetotiestampwithtime(ridesDBobj.getDate(String.valueOf(enddateofmonth))+ " 24:00:00");


                    try {
                        Monthridepojodetailsfromdb object_month = ridesDBobj.getmytripsmonthwise(String.valueOf(startdateofmonth), String.valueOf(enddateofmonth));
                        if (object_month != null) {
                            if (object_month.getCompletedtrip() != null) {
                                todaytripsdetailsViewModel.monthtrips(sFilterMonth, sFilterYear, false);
                                binding.daytxt.setText("MONTH");
                                binding.datetxt.setText(object_month.getMonthdescription());
                                binding.completedcount.setText(object_month.getCompletedtrip());
                                binding.cancelcount.setText(object_month.getCanceltrip());
                                binding.drivercancelcount.setText(object_month.getDrivercanceltrip());
                            } else {
                                todaytripsdetailsViewModel.monthtrips(sFilterMonth, sFilterYear, true);
                            }
                        } else {
                            todaytripsdetailsViewModel.monthtrips(sFilterMonth, sFilterYear, true);
                        }
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case R.id.ivright:
                if (type.equalsIgnoreCase("1")) {
                    Date dates = null;
                    try {
                        dates = mdFormat.parse(sFilterDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if (dates != null) {
                        calendartoday.setTime(dates);
                        calendartoday.add(Calendar.DATE, 1);
                        sFilterDate = mdFormat.format(calendartoday.getTime());
                    }

                    long startdateofmonth = appUtils.getdatetotiestampwithtime(sFilterDate+ " 00:00:00");
                    long enddateofmonth = appUtils.getdatetotiestampwithtime(sFilterDate+ " 24:00:00");

                    try {
                        Monthridepojodetailsfromdb object_month = ridesDBobj.getmytripsdaywise(String.valueOf(startdateofmonth), String.valueOf(enddateofmonth));
                        if (object_month != null) {
                            if (object_month.getCompletedtrip() != null) {
                                todaytripsdetailsViewModel.todaytripsapi(sFilterDate, false);
                                binding.daytxt.setText("DAY");
                                binding.datetxt.setText(object_month.getMonthdescription());
                                binding.completedcount.setText(object_month.getCompletedtrip());
                                binding.cancelcount.setText(object_month.getCanceltrip());
                                binding.drivercancelcount.setText(object_month.getDrivercanceltrip());
                            } else {
                                todaytripsdetailsViewModel.todaytripsapi(sFilterDate, true);
                            }
                        } else {
                            todaytripsdetailsViewModel.todaytripsapi(sFilterDate, true);
                        }
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                } else if (type.equalsIgnoreCase("2")) {
                    calendar_week.add(Calendar.DATE, 7);
                    calendar_week.set(Calendar.DAY_OF_WEEK, 1);
                    sFilterDateFrom = mdFormat.format(calendar_week.getTime());
                    calendar_week.set(Calendar.DAY_OF_WEEK, 7);
                    sFilterDateTo = mdFormat.format(calendar_week.getTime());
//                    todaytripsdetailsViewModel.weektripsapi(sFilterDateFrom, sFilterDateTo);
                    long startdateofmonth = appUtils.getdatetotiestampwithtime(sFilterDateFrom+ " 00:00:00");
                    long enddateofmonth = appUtils.getdatetotiestampwithtime(sFilterDateTo+ " 24:00:00");

                    try {
                        Monthridepojodetailsfromdb object_month = ridesDBobj.getmytripsweekwise(String.valueOf(startdateofmonth), String.valueOf(enddateofmonth));
                        if (object_month != null) {
                            if (object_month.getCompletedtrip() != null) {
                                todaytripsdetailsViewModel.weektripsapi(sFilterDateFrom, sFilterDateTo, false);
                                binding.daytxt.setText("WEEK");
                                binding.datetxt.setText(object_month.getMonthdescription());
                                binding.completedcount.setText(object_month.getCompletedtrip());
                                binding.cancelcount.setText(object_month.getCanceltrip());
                                binding.drivercancelcount.setText(object_month.getDrivercanceltrip());
                            } else {
                                todaytripsdetailsViewModel.weektripsapi(sFilterDateFrom, sFilterDateTo, true);
                            }
                        } else {
                            todaytripsdetailsViewModel.weektripsapi(sFilterDateFrom, sFilterDateTo, true);
                        }
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }


                } else if (type.equalsIgnoreCase("3")) {
                    calendar_month.add(Calendar.MONTH, 1);
                    sFilterMonth = monthFormat.format(calendar_month.getTime());
                    sFilterYear = yearFormat.format(calendar_month.getTime());
//                    todaytripsdetailsViewModel.monthtrips(sFilterMonth, sFilterYear);

                    startdateofmonth = appUtils.getstartdateofmonth(sFilterMonth, sFilterYear);
                    startdateofmonth =   appUtils.getdatetotiestampwithtime(ridesDBobj.getDate(String.valueOf(startdateofmonth))+ " 00:00:00");

                    enddateofmonth = appUtils.getenddateofmonth(sFilterMonth, sFilterYear);
                    enddateofmonth =   appUtils.getdatetotiestampwithtime(ridesDBobj.getDate(String.valueOf(enddateofmonth))+ " 24:00:00");


                    try {
                        Monthridepojodetailsfromdb object_month = ridesDBobj.getmytripsmonthwise(String.valueOf(startdateofmonth), String.valueOf(enddateofmonth));
                        if (object_month != null) {
                            if (object_month.getCompletedtrip() != null) {
                                todaytripsdetailsViewModel.monthtrips(sFilterMonth, sFilterYear, false);
                                binding.daytxt.setText("MONTH");
                                binding.datetxt.setText(object_month.getMonthdescription());
                                binding.completedcount.setText(object_month.getCompletedtrip());
                                binding.cancelcount.setText(object_month.getCanceltrip());
                                binding.drivercancelcount.setText(object_month.getDrivercanceltrip());
                            } else {
                                todaytripsdetailsViewModel.monthtrips(sFilterMonth, sFilterYear, true);
                            }
                        } else {
                            todaytripsdetailsViewModel.monthtrips(sFilterMonth, sFilterYear, true);
                        }
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                }

                break;
            case R.id.viewdetails:
                if (type.equalsIgnoreCase("1")) {
                    Intent today = new Intent(this, TodaytripsViewDetailConstrain.class);
                    today.putExtra("date", sFilterDate);
                    startActivity(today);
                } else if (type.equalsIgnoreCase("2")) {
                    Intent week = new Intent(this, WeektripviewConstrain.class);
                    week.putExtra("fromdate", sFilterDateFrom);
                    week.putExtra("todate", sFilterDateTo);
                    week.putExtra("description", binding.datetxt.getText().toString());
                    startActivity(week);
                } else if (type.equalsIgnoreCase("3")) {
                    Intent week = new Intent(this, MonthviewdetailConstrain.class);
                    week.putExtra("month", sFilterMonth);
                    week.putExtra("year", sFilterYear);
                    week.putExtra("startdate", String.valueOf(startdateofmonth));
                    week.putExtra("enddate", String.valueOf(enddateofmonth));
                    week.putExtra("description", binding.datetxt.getText().toString());
                    startActivity(week);
                }
                break;
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }
}
