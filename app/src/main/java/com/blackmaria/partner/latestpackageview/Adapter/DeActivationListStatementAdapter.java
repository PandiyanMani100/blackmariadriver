package com.blackmaria.partner.latestpackageview.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.blackmaria.partner.R;

import java.util.ArrayList;


public class DeActivationListStatementAdapter extends BaseAdapter {
    private Context cxt;
    ArrayList<String> arrTitle;
    ArrayList<String> arrValue;
    private LayoutInflater mInflater;

    public DeActivationListStatementAdapter(Context conxt, ArrayList<String> arrTitle, ArrayList<String> arrValue) {
        this.cxt = conxt;
        mInflater = LayoutInflater.from(cxt);
        this.arrTitle = arrTitle;
        this.arrValue = arrValue;

    }

    @Override
    public int getCount() {
        return arrValue.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public int getViewTypeCount() {
        return 1;
    }

    public class ViewHolder {
        private TextView title, value;


    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.row_single_last_statement, parent, false);
            holder = new ViewHolder();

            holder.title = view.findViewById(R.id.txt_title);
            holder.value = view.findViewById(R.id.txt_value);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }
        holder.title.setText(arrTitle.get(position));
        holder.value.setText(arrValue.get(position));
        return view;
    }
}
