package com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet.withdraw.cash;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import android.content.Intent;

import com.blackmaria.partner.databinding.ActivityCashwithdrawsuccessConstrainBinding;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Dashboard_constrain;

public class CashwithdrawsuccessViewModel extends ViewModel {
    private Activity context;
    private ActivityCashwithdrawsuccessConstrainBinding binding;

    public CashwithdrawsuccessViewModel(Activity context) {
        this.context = context;
    }

    public void setIds(ActivityCashwithdrawsuccessConstrainBinding binding) {
        this.binding = binding;
    }

    public void close() {
        context.startActivity(new Intent(context, Dashboard_constrain.class));
        context.finish();
    }
}
