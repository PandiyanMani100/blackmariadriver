package com.blackmaria.partner.latestpackageview.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

/**
 * Created by Prem Kumar and Anitha on 10/8/2015.
 */
public class CustomEdittext extends androidx.appcompat.widget.AppCompatEditText {

    public CustomEdittext(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomEdittext(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomEdittext(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
           // Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Roboto-Regular.ttf");
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/robos.ttf");
            setTypeface(tf);
        }
    }

}
