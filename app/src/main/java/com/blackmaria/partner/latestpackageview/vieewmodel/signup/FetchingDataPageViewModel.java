package com.blackmaria.partner.latestpackageview.vieewmodel.signup;

import android.Manifest;
import android.app.Activity;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.HeaderSessionManager;
import com.blackmaria.partner.Utils.PkDialogWithoutButton;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.Utils.CheckGPSProvider;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Dashboard_constrain;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Tracking.TrackArrive;
import com.blackmaria.partner.latestpackageview.view.signup.SignupAndSignInPage_Constrain;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.location.Service.LocationUpdatesService;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;

import java.util.HashMap;
import java.util.Locale;

public class FetchingDataPageViewModel extends ViewModel implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private Activity context;
    private Location mLocation;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private PendingResult<LocationSettingsResult> result;
    final static int REQUEST_LOCATION = 199;
    final int PERMISSION_REQUEST_CODE = 111;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private long UPDATE_INTERVAL = 5000;  /* 15 secs */
    private long FASTEST_INTERVAL = 2000; /* 5 secs */
    private FetchingInterface fetchingInterface;
    private Locationonalready locationonalready;

    private int SPLASH_TIME_OUT = 2000;
    private SessionManager session;
    private String driverID = "", sLatitude = "", sLongitude = "";
    private String server_mode, site_mode, site_string, site_url, app_identity_name = "", Language_code = "";
    private boolean isAppInfoAvailable = false;
    private Boolean isInternetPresent = false;
    private ConnectionDetector cd;
    private HeaderSessionManager headerSessionManager;
    private CheckGPSProvider checkGPSProvider;

    public MutableLiveData<String> status = new MutableLiveData<>();

    public FetchingDataPageViewModel(Activity context) {
        this.context = context;
        session = SessionManager.getInstance(context);
        HashMap<String, String> user = session.getUserDetails();
        driverID = user.get(SessionManager.KEY_DRIVERID);
        cd = ConnectionDetector.getInstance(context);
        isInternetPresent = cd.isConnectingToInternet();
        headerSessionManager = new HeaderSessionManager(context);
        checkGPSProvider = CheckGPSProvider.getInstance(context);
        // Session class instance
    }

    public void startlocation(final FetchingInterface fetchingInterface, final Locationonalready locationonalready) {
        this.fetchingInterface = fetchingInterface;
        this.locationonalready = locationonalready;
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Build.VERSION.SDK_INT >= 23) {
                    // Marshmallow+
                    if (!checkAccessFineLocationPermission() || !checkAccessCoarseLocationPermission() || !checkWriteExternalStoragePermission() || !checkreadExternalStoragePermission()) {
                        requestPermission();
                    } else {
                        if (checkGPSProvider.checkprovider()) {
                            locationonalready.locationonoff(true);
                        } else {
                            enableGpsService();
                        }
                    }
                } else {
                    if (checkGPSProvider.checkprovider()) {
                        locationonalready.locationonoff(true);
                    } else {
                        enableGpsService();
                    }
                }
            }
        }, SPLASH_TIME_OUT);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        startLocationUpdates();
    }

    protected void startLocationUpdates() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(context, "Enable Permissions", Toast.LENGTH_LONG).show();
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }


    //Enabling Gps Service
    private void enableGpsService() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        //...

                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(context, REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        //...
                        break;
                }
            }
        });
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    public interface FetchingInterface {
        void usercancelled(boolean val);
    }

    public interface Locationonalready {
        void locationonoff(boolean val);
    }


    public void triggeragainlocation() {
        enableGpsService();
    }


    private boolean checkAccessFineLocationPermission() {
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkAccessCoarseLocationPermission() {
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkWriteExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkreadExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(context, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
    }

    public void getversioncode(String sLatitude, String sLongitude) {
        this.sLatitude = sLatitude;
        this.sLongitude = sLongitude;
//        GetVersionCode getVersionCode = new GetVersionCode();
//        getVersionCode.execute();
        postRequest_AppInformation(Iconstant.getAppInfo);
    }

    private class GetVersionCode extends AsyncTask<Void, String, String> {
        @Override
        protected String doInBackground(Void... voids) {

            String newVersion = null;
            try {
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + context.getPackageName() + "&hl=it")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div[itemprop=softwareVersion]")
                        .first()
                        .ownText();
                return newVersion;
            } catch (Exception e) {
                return newVersion;
            }
        }

        @Override
        protected void onPostExecute(String onlineVersion) {
            super.onPostExecute(onlineVersion);
            if (onlineVersion != null && !onlineVersion.isEmpty()) {
                String currentVersion = "";
                try {
                    currentVersion = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }

                if (Float.valueOf(currentVersion) >= Float.valueOf(onlineVersion)) {
                    postRequest_AppInformation(Iconstant.getAppInfo);
                } else {
                    Alert(context.getResources().getString(R.string.app_name), "There is newer version of this application available, click OK to upgrade now?");
                }

            } else {
                postRequest_AppInformation(Iconstant.getAppInfo);
            }

        }
    }

    private void postRequest_AppInformation(String Url) {

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("user_type", "driver");
        jsonParams.put("id", driverID);
        jsonParams.put("lat", sLatitude);
        jsonParams.put("lon", sLongitude);
        System.out.println("--------App Information jsonParams--------" + jsonParams);
        ServiceRequest mRequest = new ServiceRequest(context);
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("-------------Splash App Information Response----------------" + response);


                String Str_status = "", sContact_mail = "", sCustomerServiceNumber = "", sSiteUrl = "", sXmppHostUrl = "", sHostName = "", sFacebookId = "", sGooglePlusId = "", sPhoneMasking = "";
                try {
                    JSONObject object = new JSONObject(response);
                    Str_status = object.getString("status");
                    if (Str_status.equalsIgnoreCase("1")) {
                        JSONObject response_object = object.getJSONObject("response");
                        if (response_object.length() > 0) {
                            JSONObject info_object = response_object.getJSONObject("info");
                            if (info_object.length() > 0) {
                                sContact_mail = info_object.getString("site_contact_mail");
                                sCustomerServiceNumber = info_object.getString("customer_service_number");
                                if (info_object.has("with_pincode")) {
                                    String with_pincode = info_object.getString("with_pincode");
                                    session.setSecurePin(with_pincode);
                                }
                                session.setCustomerServiceNo(sCustomerServiceNumber);
                                sSiteUrl = info_object.getString("site_url");
                                sXmppHostUrl = info_object.getString("xmpp_host_url");
                                sHostName = info_object.getString("xmpp_host_name");
                              /*  sFacebookId = info_object.getString("facebook_app_id");
                                sGooglePlusId = info_object.getString("google_plus_app_id");*/
                                sPhoneMasking = info_object.getString("phone_masking_status");
                                app_identity_name = info_object.getString("app_identity_name");
                                server_mode = info_object.getString("server_mode");
                                site_mode = info_object.getString("site_mode");
                                site_string = info_object.getString("site_mode_string");
                                site_url = info_object.getString("site_url");

                                if(info_object.has("s3_status"))
                                {
                                    if(info_object.getString("s3_status").equals("1"))
                                    {
                                        JSONObject s3_info = info_object.getJSONObject("s3_info");

                                        String access_key = s3_info.getString("access_key");
                                        String secret_key = s3_info.getString("secret_key");
                                        String bucket_name = s3_info.getString("bucket_name");
                                        String bucket_url = s3_info.getString("bucket_url");
                                        session.setAmazondetails(info_object.getString("s3_status"),access_key,secret_key,bucket_name,bucket_url);
                                    }
                                    else
                                    {
                                        session.setImagestatus("0");
                                    }
                                }


                                try {
                                    JSONArray jsonarray_emergendy_no = info_object.getJSONArray("emergencyNumbers");
                                    for (int i = 0; i <= jsonarray_emergendy_no.length() - 1; i++) {
                                        String value = jsonarray_emergendy_no.getJSONObject(i).getString("title");
                                        if (value.equalsIgnoreCase("Police")) {
                                            session.setPolice(jsonarray_emergendy_no.getJSONObject(i).getString("number"));
                                        } else if (value.equalsIgnoreCase("Fire")) {
                                            session.setFire(jsonarray_emergendy_no.getJSONObject(i).getString("number"));
                                        } else if (value.equalsIgnoreCase("Ambulance")) {
                                            session.setAmbulance(jsonarray_emergendy_no.getJSONObject(i).getString("number"));
                                        }
                                    }

                                } catch (JSONException e) {

                                }


                                if (info_object.has("driver_review")) {
                                    session.setDriverReview(info_object.getString("driver_review"));
                                }
                                if (info_object.has("ride_earn")) {

                                    session.setReferelStatus(info_object.getString("ride_earn"));
                                }
                                /* Language_code="ta";*/
                                Language_code = info_object.getString("lang_code");
                                isAppInfoAvailable = true;
                            } else {
                                isAppInfoAvailable = false;
                            }


                            headerSessionManager.createHeaderSession(app_identity_name, Language_code);
                            session.setPhoneMaskingDetail(sPhoneMasking);
                            session.setContactNumber(sCustomerServiceNumber);

                           /* sPendingRideId= response_object.getString("pending_rideid");
                            sRatingStatus= response_object.getString("ride_status");*/

                        } else {
                            isAppInfoAvailable = false;
                        }
                    } else {
                        isAppInfoAvailable = false;
                    }
                    if (Str_status.equalsIgnoreCase("1") && isAppInfoAvailable) {

                        HashMap<String, String> language = session.getLanaguage();
                        Locale locale = null;

                        switch (Language_code) {

                            case "en":
                                locale = new Locale("en");
                                session.setlamguage("en", "en");

                                break;
                            case "es":
                                locale = new Locale("es");
                                session.setlamguage("es", "es");
                                break;

                            default:
                                locale = new Locale("en");
                                session.setlamguage("en", "en");
                                break;
                        }

                        Locale.setDefault(locale);
                        Configuration config = new Configuration();
                        config.locale = locale;
                        context.getApplicationContext().getResources().updateConfiguration(config, context.getApplicationContext().getResources().getDisplayMetrics());

                        session.setXmpp(sXmppHostUrl, sHostName);
                        session.setAgent(app_identity_name);

                        if (site_mode.equalsIgnoreCase("development")) {
                            PkDialogWithoutButton mInfoDialog = new PkDialogWithoutButton(context);
                            mInfoDialog.setDialogTitle("ALERT");
                            mInfoDialog.setDialogMessage(site_string);
                            mInfoDialog.show();
                        } else {

                            System.out.println("---------prem session.isLoggedIn()--------------" + session.isLoggedIn());
                            if (session.isLoggedIn()) {
                                context.startService(new Intent(context, LocationUpdatesService.class));
                                if (!session.getisride().equalsIgnoreCase("")) {
                                    Intent intent = new Intent(context, TrackArrive.class);
                                    intent.putExtra("rideid", session.getisride());
                                    context.startActivity(intent);
                                    context.finish();
                                    context.overridePendingTransition(R.anim.enter, R.anim.exit);
                                } else {
                                    Intent intent = new Intent(context, Dashboard_constrain.class);
                                    context.startActivity(intent);
                                    context.finish();
                                    context.overridePendingTransition(R.anim.enter, R.anim.exit);
                                }
                            } else {
                                Intent i = new Intent(context, SignupAndSignInPage_Constrain.class);
                                context.startActivity(i);
                                context.finish();
                                context.overridePendingTransition(R.anim.enter, R.anim.exit);
                            }
                        }
                    } else {
                        Toast.makeText(context, "BAD URL", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorListener() {
                Toast.makeText(context, Iconstant.BaseUrl, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(context);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(context.getResources().getString(R.string.alert_ok), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                context.finish();
                String appPackageName = context.getPackageName(); // getPackageName() from Context or Activity object
                try {
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });
        mDialog.setNegativeButton(context.getResources().getString(R.string.alert_cancel), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                context.finish();

            }
        });
        mDialog.show();
    }

}
