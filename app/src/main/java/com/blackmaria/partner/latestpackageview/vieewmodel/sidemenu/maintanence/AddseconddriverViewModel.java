package com.blackmaria.partner.latestpackageview.vieewmodel.sidemenu.maintanence;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import com.blackmaria.partner.R;
import com.blackmaria.partner.databinding.ActivityAddseconddriverBinding;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;

import java.util.HashMap;

public class AddseconddriverViewModel extends ViewModel implements ApIServices.completelisner {

    private Dialog dialog;
    private Activity context;
    private AppUtils appUtils;
    private int count = 0;
    private ActivityAddseconddriverBinding binding;
    private MutableLiveData<String> imageuploadresponse = new MutableLiveData<>();
    private MutableLiveData<String> adddriverresponse = new MutableLiveData<>();

    public MutableLiveData<String> getImageuploadresponse() {
        return imageuploadresponse;
    }

    public MutableLiveData<String> getAdddriverresponse() {
        return adddriverresponse;
    }

    public AddseconddriverViewModel(Activity context) {
        this.context = context;
        appUtils = AppUtils.getInstance(context);
    }

    public void updatereferalprogram(String url, HashMap<String, String> jsonParams) {
        count = 0;
//        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ApIServices apIServices = new ApIServices(context, AddseconddriverViewModel.this);
        apIServices.dooperation(url, jsonParams);
    }


    @Override
    public void sucessresponse(String val) {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        if (count == 0) {
            imageuploadresponse.postValue(val);
        } else if (count == 1) {
            adddriverresponse.postValue(val);
        }
    }

    @Override
    public void errorreponse() {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
    }

    @Override
    public void jsonexception() {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        AppUtils.toastprint(context, "Json Exception");
    }

    public void setIds(ActivityAddseconddriverBinding binding) {
        this.binding = binding;
    }

    public void uploadimage(String url, byte[] array, String name) {
        count = 0;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ApIServices apIServices = new ApIServices(context, AddseconddriverViewModel.this);
        apIServices.imageupload(url, array, name);
    }

    public void updateanotherdriver(String url, HashMap<String, String> jsonParams) {
        count = 1;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ApIServices apIServices = new ApIServices(context, AddseconddriverViewModel.this);
        apIServices.dooperation(url, jsonParams);
    }

    public void back() {
        context.onBackPressed();
    }
}
