package com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.earnings;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityEarningsStatementBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class EarningsStatmentViewModel extends ViewModel implements ApIServices.completelisner {

    private Activity context;
    private Dialog dialog;
    private String sDriverID = "";
    private SessionManager sessionManager;
    private ActivityEarningsStatementBinding binding;
    public MutableLiveData<String> statementrespones_today = new MutableLiveData<>();
    public MutableLiveData<String> statementrespones_week = new MutableLiveData<>();
    public MutableLiveData<String> statementrespones_month = new MutableLiveData<>();
    private int count = 0;
    private String todayfilterdate = "", filterFromDateweek = "", filterToDateweek = "",filterFromDatemonth = "", filterToyearmonth = "";
    private Calendar todaycalenderobj;
    private Calendar weekcalenderobj;
    private Calendar monthcalenderobj;

    public EarningsStatmentViewModel(Activity context) {
        this.context = context;
        sessionManager = SessionManager.getInstance(context);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        todaycalenderobj = Calendar.getInstance();
        SimpleDateFormat mdFormat = new SimpleDateFormat("yyyy-MM-dd");
        todayfilterdate = mdFormat.format(todaycalenderobj.getTime());

        weekcalenderobj = Calendar.getInstance();
        weekcalenderobj.set(Calendar.DAY_OF_WEEK, 1);
        filterFromDateweek = mdFormat.format(weekcalenderobj.getTime());
        weekcalenderobj.set(Calendar.DAY_OF_WEEK, 7);
        filterToDateweek = mdFormat.format(weekcalenderobj.getTime());

        monthcalenderobj = Calendar.getInstance();
        SimpleDateFormat mmFormat = new SimpleDateFormat("MM");
        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy");
        filterFromDatemonth = mmFormat.format(monthcalenderobj.getTime());
        filterToyearmonth = myFormat.format(monthcalenderobj.getTime());
    }

    public MutableLiveData<String> getStatementrespones_today() {
        return statementrespones_today;
    }

    public MutableLiveData<String> getStatementrespones_month() {
        return statementrespones_month;
    }

    public MutableLiveData<String> getStatementrespones_week() {
        return statementrespones_week;
    }

    public void EarningsStatement_today() {
        count = 0;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        jsonParams.put("filter_date", todayfilterdate);
        ApIServices apIServices = new ApIServices(context, EarningsStatmentViewModel.this);
        apIServices.dooperation(Iconstant.earnings_daywise_url, jsonParams);
    }


    public void EarningsStatement_today(String todayfilterdate) {
        count = 0;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        jsonParams.put("filter_date", todayfilterdate);
        ApIServices apIServices = new ApIServices(context, EarningsStatmentViewModel.this);
        apIServices.dooperation(Iconstant.earnings_daywise_url, jsonParams);
    }

    public void Earningstatement_week() {
        count = 1;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        jsonParams.put("from_date", filterFromDateweek);
        jsonParams.put("to_date", filterToDateweek);
        ApIServices apIServices = new ApIServices(context, EarningsStatmentViewModel.this);
        apIServices.dooperation(Iconstant.earnings_weekwise_url, jsonParams);
    }

    public void Earningstatement_week(String filterFromDateweek,String filterToDateweek) {
        count = 1;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        jsonParams.put("from_date", filterFromDateweek);
        jsonParams.put("to_date", filterToDateweek);
        ApIServices apIServices = new ApIServices(context, EarningsStatmentViewModel.this);
        apIServices.dooperation(Iconstant.earnings_weekwise_url, jsonParams);
    }

    public void Earningsstatement_month() {
        count = 2;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        jsonParams.put("month", filterFromDatemonth);
        jsonParams.put("year", filterToyearmonth);
        ApIServices apIServices = new ApIServices(context, EarningsStatmentViewModel.this);
        apIServices.dooperation(Iconstant.earnings_monthwise_url, jsonParams);
    }

    public void left()
    {
        if (count == 0) {
            SimpleDateFormat mdFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            try {
                date = mdFormat.parse(todayfilterdate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (date != null) {
                todaycalenderobj.setTime(date);
                todaycalenderobj.add(Calendar.DATE, -1);
                todayfilterdate = mdFormat.format(todaycalenderobj.getTime());
            }
            EarningsStatement_today();
        } else if (count == 1) {
            SimpleDateFormat mdFormat = new SimpleDateFormat("yyyy-MM-dd");
            weekcalenderobj.add(Calendar.DATE, -7);
            weekcalenderobj.set(Calendar.DAY_OF_WEEK, 1);
            filterFromDateweek = mdFormat.format(weekcalenderobj.getTime());
            weekcalenderobj.set(Calendar.DAY_OF_WEEK, 7);
            filterToDateweek = mdFormat.format(weekcalenderobj.getTime());
            Earningstatement_week();
        } else if (count == 2) {
            SimpleDateFormat mmFormat = new SimpleDateFormat("MM");
            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy");
            monthcalenderobj.add(Calendar.MONTH, -1);
            filterFromDatemonth = mmFormat.format(monthcalenderobj.getTime());
            filterToyearmonth = myFormat.format(monthcalenderobj.getTime());
            Earningsstatement_month();
        }
    }

    public void right() {
        if (count == 0) {
            SimpleDateFormat mdFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            try {
                date = mdFormat.parse(todayfilterdate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (date != null) {
                todaycalenderobj.setTime(date);
                todaycalenderobj.add(Calendar.DATE, 1);
                todayfilterdate = mdFormat.format(todaycalenderobj.getTime());
            }
            EarningsStatement_today();
        } else if (count == 1) {
            SimpleDateFormat mdFormat = new SimpleDateFormat("yyyy-MM-dd");
            weekcalenderobj.add(Calendar.DATE, 7);
            weekcalenderobj.set(Calendar.DAY_OF_WEEK, 1);
            filterFromDateweek = mdFormat.format(weekcalenderobj.getTime());
            weekcalenderobj.set(Calendar.DAY_OF_WEEK, 7);
            filterToDateweek = mdFormat.format(weekcalenderobj.getTime());
            Earningstatement_week();
        } else if (count == 2) {
            SimpleDateFormat mmFormat = new SimpleDateFormat("MM");
            SimpleDateFormat myFormat = new SimpleDateFormat("yyyy");
            monthcalenderobj.add(Calendar.MONTH, 1);
            filterFromDatemonth = mmFormat.format(monthcalenderobj.getTime());
            filterToyearmonth = myFormat.format(monthcalenderobj.getTime());
            Earningsstatement_month();
        }
    }


    @Override
    public void sucessresponse(String val) {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        if (count == 0) {
            statementrespones_today.postValue(val);
        } else if (count == 1) {
            statementrespones_week.postValue(val);
        } else if (count == 2) {
            statementrespones_month.postValue(val);
        }
    }

    @Override
    public void errorreponse() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void jsonexception() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        AppUtils.toastprint(context, "Json Exception");
    }

    public void setIds(ActivityEarningsStatementBinding binding) {
        this.binding = binding;
    }

    public void back() {
        context.onBackPressed();
    }
}
