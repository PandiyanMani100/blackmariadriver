package com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.util.Log;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityRechargepaymentsuccessConstrainBinding;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Wallet.RechargepaymentsuccessFactory;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet.RechargepaymentsuccessViewModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class RechargepaymentsuccessConstrain extends AppCompatActivity {

    private ActivityRechargepaymentsuccessConstrainBinding binding;
    private RechargepaymentsuccessViewModel rechargepaymentsuccessViewModel;
    private SessionManager sessionManager;

    AccessLanguagefromlocaldb db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
        binding = DataBindingUtil.setContentView(RechargepaymentsuccessConstrain.this, R.layout.activity_rechargepaymentsuccess_constrain);
        rechargepaymentsuccessViewModel = ViewModelProviders.of(this, new RechargepaymentsuccessFactory(this)).get(RechargepaymentsuccessViewModel.class);
        binding.setRechargepaymentsuccessViewModel(rechargepaymentsuccessViewModel);
        rechargepaymentsuccessViewModel.setIds(binding);

        sessionManager = SessionManager.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        String mobile = user.get(SessionManager.KEY_CONTACT_NUMBER);

        if (getIntent().hasExtra("json")) {
            try {
                JSONObject jsonObject = new JSONObject(getIntent().getStringExtra("json"));
                setText(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        binding.selectpayment.setText(db.getvalue("reload_success"));
        binding.thankyou.setText(db.getvalue("thankyou"));
        binding.from.setText(db.getvalue("trip_label_from"));
        binding.cancel.setText(db.getvalue("close_lable"));
    }

    private void setText(JSONObject obj) {
        try {
            binding.name.setText(obj.getString("driver_name"));
            binding.date.setText(obj.getString("date"));
            binding.time.setText(obj.getString("time"));
            binding.amount.setText(obj.getString("currency_code") + " " + obj.getString("recharge_amount"));
            binding.transactionumber.setText("TRANSACTION NO : " + obj.getString("transaction_no"));
            if (getIntent().getStringExtra("paymenttype").equalsIgnoreCase("bank")) {
                binding.mobileno.setText(obj.getString("dail_code") + "" + obj.getString("mobile_number"));
                binding.paymenttype.setTextColor(getResources().getColor(R.color.blue_color));
                binding.paymentcode.setTextColor(getResources().getColor(R.color.blue_color));
                binding.paymenttype.setText("BANK PAYMENT CODE");
                binding.paymentcode.setAllCaps(true);
                binding.paymentcode.setText(obj.getString("payment_code"));
                AppUtils.setImageviewwithoutcropplaceholder(RechargepaymentsuccessConstrain.this, obj.getString("bank_image"), binding.paymentimage);
            } else if (getIntent().getStringExtra("paymenttype").equalsIgnoreCase("paypal")) {
                binding.mobileno.setText(obj.getString("dail_code") + "" + obj.getString("mobile_number"));
                binding.paymenttype.setText("PAYPAL ID");
                binding.paymentcode.setText(obj.getString("paypal_id"));
                binding.paymentcode.setAllCaps(false);
                AppUtils.setImageviewwithoutcropplaceholder(RechargepaymentsuccessConstrain.this, obj.getString("icon_active"), binding.paymentimage);
            } else if(getIntent().getStringExtra("paymenttype").equalsIgnoreCase("xtendit")){
                binding.mobileno.setText(obj.getString("mobile_number"));
                binding.paymenttype.setText("CARD NUMBER");
                binding.paymentcode.setText(obj.getString("card_number"));
                AppUtils.setImageviewwithoutcropplaceholder(RechargepaymentsuccessConstrain.this, obj.getString("card_image"), binding.paymentimage);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Log.v("TRIMMEMORY", " " + level);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }



    @Override
    public void onBackPressed() {

    }
}
