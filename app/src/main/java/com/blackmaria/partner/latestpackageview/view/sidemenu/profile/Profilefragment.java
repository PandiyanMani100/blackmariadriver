package com.blackmaria.partner.latestpackageview.view.sidemenu.profile;

import android.annotation.SuppressLint;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.app.Dialog;
import android.content.Context;
import androidx.databinding.DataBindingUtil;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.app.DriverProfile_New;
import com.blackmaria.partner.databinding.ProfileviewpagerlayoutBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ServiceRequest;
import com.blackmaria.partner.latestpackageview.Factory.Sidemenu.Profile.ProfileViewpagerFactory;
import com.blackmaria.partner.latestpackageview.Model.profilepojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.vieewmodel.sidemenu.profile.Profileviewpagerviewmodel;
import com.blackmaria.partner.latestpackageview.widgets.Emailchange;
import com.blackmaria.partner.latestpackageview.widgets.Mobilenochange;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.countrycodepicker.CountryPicker;
import com.countrycodepicker.CountryPickerListener;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;


@SuppressLint("ValidFragment")
public class Profilefragment extends Fragment {

    private ProfileviewpagerlayoutBinding binding;
    private Profileviewpagerviewmodel profileviewpagerviewmodel;
    private JSONObject jsonObj;
    private AppUtils appUtils;
    private profilepojo pojo;
    private String driverid = "", driverimage = "", globalchangedemail = "";
    private SessionManager sessionManager;
    private boolean isemailchange = false;
    private CountryPicker picker;
    private int countapihit = 0;
    private String globalmobile="",globalemails="";
    TextView chnagepin;
    AccessLanguagefromlocaldb db;
    public Profilefragment(JSONObject jsonObj) {
        this.jsonObj = jsonObj;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.profileviewpagerlayout, container, false);

        db = new   AccessLanguagefromlocaldb(getActivity());
        View view = binding.getRoot();
        profileviewpagerviewmodel = ViewModelProviders.of(this, new ProfileViewpagerFactory(getActivity())).get(Profileviewpagerviewmodel.class);
        binding.setProfileviewpagerviewmodel(profileviewpagerviewmodel);
        profileviewpagerviewmodel.setIds(binding);
        Type type = new TypeToken<profilepojo>() {
        }.getType();
        pojo = new GsonBuilder().create().fromJson(jsonObj.toString(), type);
        initView();
        setprofiledetails();
        clicklistener();
        chnagepin = view.findViewById(R.id.chnagepin);
        globalmobile = binding.myloginDialogFormMobileNoEdittext.getText().toString();
        globalemails = binding.myloginDialogFormEmailidEdittext.getText().toString();


        binding.myprofiletile.setText(db.getvalue("myprofile"));
        binding.logibdd.setText(db.getvalue("login_details"));
        binding.editprofile.setText(db.getvalue("edit"));
        binding.myloginDialogFormEmailidEdittext.setHint(db.getvalue("email_id"));
        binding.myloginDialogFormCountryCodeTextview.setHint(db.getvalue("code"));
        binding.myloginDialogFormMobileNoEdittext.setHint(db.getvalue("phone"));
        binding.chnagepin.setText(db.getvalue("change_pin_code"));

        binding.updatelogin.setText(db.getvalue("update_login"));
        binding.profiledetailstxt.setText(db.getvalue("profiledetailstxt"));
        binding.chnagepin.setText(db.getvalue("change_pin_code"));
        binding.chnagepin.setText(db.getvalue("change_pin_code"));

        chnagepin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(getActivity());
                View view = getLayoutInflater().inflate(R.layout.changepindialog, null);
                bottomSheetDialog.setContentView(view);
                bottomSheetDialog.setCancelable(true);

                final TextView changepincodec = (TextView)view.findViewById(R.id.changepincodec);
                changepincodec.setText(db.getvalue("change_pin_code"));
                final TextView enternew = (TextView)view.findViewById(R.id.enternew);
                enternew.setText(db.getvalue("enter_new_pin_code"));
                final TextView con = (TextView)view.findViewById(R.id.con);
                con.setText(db.getvalue("confirm"));

                final EditText oldpin = (EditText)view.findViewById(R.id.oldpin);
                oldpin.setHint(db.getvalue("enter_old_pin"));
                final EditText newpin = (EditText)view.findViewById(R.id.newpin);
                LinearLayout layout_booking_confirm=(LinearLayout)view.findViewById(R.id.layout_booking_confirm);
                layout_booking_confirm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(oldpin.getText().toString().equals(""))
                        {
                            Toast.makeText(getActivity(),db.getvalue("enter_old_pin"),Toast.LENGTH_LONG).show();
                        }
                        else if(newpin.getText().toString().equals(""))
                        {
                            Toast.makeText(getActivity(),db.getvalue("enter_new_pin_code"),Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            bottomSheetDialog.dismiss();
                            PostRequestnewpincode(Iconstant.updatepincode,oldpin.getText().toString(),newpin.getText().toString());

                        }
                    }
                });


                ImageView closedialog = (ImageView)view.findViewById(R.id.closedialog);
                closedialog.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        bottomSheetDialog.dismiss();
                    }
                });

                bottomSheetDialog.show();

            }
        });

        return view;
    }


    private void PostRequestnewpincode(String Url,String oldpin,String newpin) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_loading);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> user = sessionManager.getUserDetails();
        String sDriverID = user.get(SessionManager.KEY_DRIVERID);

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        jsonParams.put("pin", oldpin);
        jsonParams.put("new_pin", newpin);

        ServiceRequest mRequest = new ServiceRequest(getActivity());
        mRequest.makeServiceRequest(Url, Request.Method.POST, jsonParams, new ServiceRequest.ServiceListener() {
            @Override
            public void onCompleteListener(String response) {

                System.out.println("--------------Profilepage getdata reponse-------------------" + response);

                dialog.dismiss();
                String Sstatus = "", key1_sos_name = "", key1_sos_email = "", key1_sos_mobileno = "", key1_sos_relationship = "", key1_sos_address = "",
                        key2_sos_name = "", key2_sos_email = "", key2_sos_mobileno = "", key2_sos_relationship = "", key2_sos_address = "";

                try {


                    JSONObject object = new JSONObject(response);
                    Sstatus = object.getString("status");
                    response = object.getString("response");

                    if (Sstatus.equalsIgnoreCase("1")) {
                        Alert(db.getvalue("action_success"), response);
                    } else {
                        Alert(db.getvalue("action_error"), response);
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    System.out.println("eee------------------" + e);
                    e.printStackTrace();

                }



            }

            @Override
            public void onErrorListener() {
                dialog.dismiss();
            }
        });
    }

    private void Alert(String title, String alert) {
        final PkDialog mDialog = new PkDialog(getActivity());
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(db.getvalue("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    private void initView() {
        appUtils = AppUtils.getInstance(getActivity());
        sessionManager = SessionManager.getInstance(getActivity());
        HashMap<String, String> user = sessionManager.getUserDetails();
        driverid = user.get(SessionManager.KEY_DRIVERID);
        driverimage = user.get(SessionManager.KEY_DRIVER_IMAGE);
        picker = CountryPicker.newInstance(db.getvalue("select_country_lable"));
    }

    public void getprofileview() {
        profileviewpagerviewmodel.getprofile(driverid);
        profileviewpagerviewmodel.getGetprofileview().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObj = new JSONObject(response);
                    Type type = new TypeToken<profilepojo>() {
                    }.getType();
                    pojo = new GsonBuilder().create().fromJson(jsonObj.toString(), type);
                    setprofiledetails();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private void setunedtabletext() {
        binding.myloginDialogFormEmailidEdittext.setEnabled(false);
        binding.myloginDialogFormMobileNoEdittext.setEnabled(false);
        binding.myloginDialogFormCountryCodeTextview.setEnabled(false);
        binding.updatelogin.setClickable(false);
        binding.updatelogin.setEnabled(false);
        binding.updatelogin.setVisibility(View.INVISIBLE);
    }

    private void setprofiledetails() {
        if(pojo.getStatus() != null)
        {
            binding.myloginDialogFormEmailidEdittext.setText(pojo.getResponse().getDriver_profile().getEmail());
            binding.myloginDialogFormMobileNoEdittext.setText(pojo.getResponse().getDriver_profile().getMobile_number());
            binding.myloginDialogFormCountryCodeTextview.setText(pojo.getResponse().getDriver_profile().getCountry_code());
            binding.myloginDialogFormEmailidEdittext.setEnabled(false);
            binding.myloginDialogFormMobileNoEdittext.setEnabled(false);
            binding.myloginDialogFormCountryCodeTextview.setEnabled(false);
            binding.updatelogin.setClickable(false);
            binding.updatelogin.setEnabled(false);
            try {
                binding.profiledetailsview.removeAllViews();
            } catch (NullPointerException e) {
            }
            for (int i = 0; i <= pojo.getResponse().getProfile().size() - 1; i++) {
                View view = getLayoutInflater().inflate(R.layout.profilecustomlayout, null);
                TextView name = view.findViewById(R.id.name);
                TextView value = view.findViewById(R.id.value);
                name.setText(pojo.getResponse().getProfile().get(i).getTitle());
                value.setText(pojo.getResponse().getProfile().get(i).getValue());
                binding.profiledetailsview.addView(view);
            }
            profileviewpagerviewmodel.getChangeemailresponse().observe(this, new Observer<String>() {
                @Override
                public void onChanged(@Nullable String response) {
                    if (countapihit == 1) {
                        countapihit = 0;
                        try {
                            String Sstatus = "", Smessage = "", otp_status = "", otp = "", globalemail = "";
                            JSONObject object = new JSONObject(response);
                            if (object.getString("status").equalsIgnoreCase("1")) {
                                Smessage = object.getString("response");
                                globalemail = object.getJSONObject("user_profile").getString("email");
                                globalemails = globalemail;
                                otp_status = object.getJSONObject("user_profile").getString("passcode_show");
                                otp = object.getJSONObject("user_profile").getString("email_passcode");
                                appUtils.AlertSuccess(getActivity(), db.getvalue("action_success"), Smessage);
                                if (otp_status.equalsIgnoreCase("1")) {
                                    Emailchange("", globalemail, otp);
                                } else {
                                    Emailchange(otp, globalemail, otp);
                                }
                            } else {
                                Smessage = object.getString("response");
                                appUtils.AlertError(getActivity(), db.getvalue("action_error"), Smessage);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            profileviewpagerviewmodel.getChangeemailupdateresponse().observe(this, new Observer<String>() {
                @Override
                public void onChanged(@Nullable String response) {
                    if (countapihit == 1) {
                        countapihit = 0;
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                                setunedtabletext();
                                appUtils.AlertSuccess(getActivity(), db.getvalue("action_success"), jsonObject.getString("response"));
                            } else {
                                appUtils.AlertError(getActivity(), db.getvalue("action_error"), jsonObject.getString("response"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            profileviewpagerviewmodel.getChangemobileresponse().observe(this, new Observer<String>() {
                @Override
                public void onChanged(@Nullable String response) {
                    if (countapihit == 1) {
                        countapihit = 0;
                        try {
                            String Sstatus = "", Smessage = "", otp_status = "", otp = "", globalmobilenumber = "", country_code = "";
                            JSONObject object = new JSONObject(response);
                            if (object.getString("status").equalsIgnoreCase("1")) {
                                Smessage = object.getString("response");
                                globalmobilenumber = object.getString("mobile_number");
                                globalmobile = globalmobilenumber;
                                country_code = object.getString("dail_code");
                                otp_status = object.getString("otp_status");
                                otp = object.getString("otp");
                                sessionManager.setOtpstatusAndPin(otp_status, otp);
                                appUtils.AlertSuccess(getActivity(), db.getvalue("action_success"), Smessage);
                                if (otp_status.equalsIgnoreCase("development")) {
                                    Mobilechange(otp, country_code, globalmobilenumber, otp);
                                } else {
                                    Mobilechange("", country_code, globalmobilenumber, otp);
                                }
                            } else {
                                Smessage = object.getString("response");
                                appUtils.AlertError(getActivity(), db.getvalue("action_error"), Smessage);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            profileviewpagerviewmodel.getChangemobileupdateresponse().observe(this, new Observer<String>() {
                @Override
                public void onChanged(@Nullable String response) {
                    if (countapihit == 1) {
                        countapihit = 0;
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("status").equalsIgnoreCase("1")) {
//                            binding.myloginDialogFormCountryCodeTextview.setText(jsonObject.getString("dail_code"));
//                            binding.myloginDialogFormMobileNoEdittext.setText(jsonObject.getString("mobile_number"));
                                setunedtabletext();
                                appUtils.AlertSuccess(getActivity(), db.getvalue("action_success"), jsonObject.getString("response"));
                            } else {
                                appUtils.AlertError(getActivity(),db.getvalue("action_error"), jsonObject.getString("response"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            picker.setListener(new CountryPickerListener() {
                @Override
                public void onSelectCountry(String name, String code, String dialCode) {
                    picker.dismiss();
                    binding.myloginDialogFormCountryCodeTextview.setText(dialCode);
                    // close keyboard
                    InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    mgr.hideSoftInputFromWindow(binding.myloginDialogFormMobileNoEdittext.getWindowToken(), 0);
                }
            });
        }

    }

    private void Mobilechange(final String otp, final String countrycode, final String mobile, final String checkotp) {
        final Mobilenochange mDialog = new Mobilenochange(getActivity());
        mDialog.setCloseButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        mDialog.setPositiveButton(otp, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog.getenteredotp().equalsIgnoreCase(checkotp)) {
                    AppUtils.CloseKeyboard(getActivity());
                    mDialog.dismiss();
                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    jsonParams.put("driver_id", driverid);
                    jsonParams.put("dail_code", countrycode);
                    jsonParams.put("mobile_number", mobile);
                    jsonParams.put("otp", checkotp);
                    jsonParams.put("mode", "update");
                    countapihit = 1;
                    profileviewpagerviewmodel.updatemobilewithcode(Iconstant.profilemobilechange, jsonParams);
                } else {
                    appUtils.AlertError(getActivity(), db.getvalue("alert_label_title"), db.getvalue("enetrvalidot"));
                }
            }
        });
        mDialog.show();
    }


    private void Emailchange(final String otp, final String email, final String checkotp) {
        final Emailchange mDialog = new Emailchange(getActivity());
        mDialog.setCancelble(false);
        mDialog.setCancelOnTouchOutside(false);
        mDialog.setCloseButton(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });
        mDialog.setPositiveButton(otp, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDialog.getenteredotp().equalsIgnoreCase(checkotp)) {
                    AppUtils.CloseKeyboard(getActivity());
                    mDialog.dismiss();
                    globalchangedemail = email;
                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    jsonParams.put("driver_id", driverid);
                    jsonParams.put("email", email);
                    jsonParams.put("email_passcode", checkotp);
                    countapihit = 1;
                    profileviewpagerviewmodel.getemailupdatewithcode(Iconstant.profileemailupdate, jsonParams);
                    //updatewholeprofile object
                } else {
                    appUtils.AlertError(getActivity(), db.getvalue("alert_label_title"), db.getvalue("invao"));
                }

            }
        });
        mDialog.show();
    }


    private void clicklistener() {
        binding.updatelogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validate();
            }
        });
        binding.myloginDialogFormCountryCodeTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picker.show(getActivity().getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });
    }

    private void validate() {
        if (binding.myloginDialogFormEmailidEdittext.getText().toString().length() == 0) {
            appUtils.AlertError(getActivity(), db.getvalue("action_error"), db.getvalue("profile_label_alert_email"));
        } else if (binding.myloginDialogFormCountryCodeTextview.getText().toString().equalsIgnoreCase("code")) {
            appUtils.AlertError(getActivity(), db.getvalue("action_error"), db.getvalue("login_page_alert_country_code"));
        } else if (binding.myloginDialogFormMobileNoEdittext.getText().toString().length() == 0) {
            appUtils.AlertError(getActivity(), db.getvalue("action_error"), db.getvalue("profile_label_alert_mobilempty"));
        } else if (!AppUtils.isValidPhoneNumber(binding.myloginDialogFormMobileNoEdittext.getText().toString())) {
            appUtils.AlertError(getActivity(), db.getvalue("action_error"), db.getvalue("profile_lable_error_mobile"));
        } else {
            if (!binding.myloginDialogFormMobileNoEdittext.getText().toString().equalsIgnoreCase(globalmobile)) {
                HashMap<String, String> jsonParams = new HashMap<String, String>();
                jsonParams.put("driver_id", driverid);
                jsonParams.put("dail_code", binding.myloginDialogFormCountryCodeTextview.getText().toString().trim());
                jsonParams.put("mobile_number", binding.myloginDialogFormMobileNoEdittext.getText().toString().trim());
                jsonParams.put("mode", "request");
                isemailchange = false;
                countapihit = 1;
                profileviewpagerviewmodel.changemobile(Iconstant.profilemobilechange, jsonParams);

            } else if (!binding.myloginDialogFormEmailidEdittext.getText().toString().equalsIgnoreCase(globalemails)) {
                HashMap<String, String> jsonParams = new HashMap<String, String>();
                jsonParams.put("driver_id", driverid);
                jsonParams.put("email", binding.myloginDialogFormEmailidEdittext.getText().toString());
                isemailchange = true;
                countapihit = 1;
                profileviewpagerviewmodel.changeemail(Iconstant.profileemailchange, jsonParams);
            } else if (!binding.myloginDialogFormCountryCodeTextview.getText().toString().equalsIgnoreCase(pojo.getResponse().getDriver_profile().getCountry_code())) {
                HashMap<String, String> jsonParams = new HashMap<String, String>();
                jsonParams.put("driver_id", driverid);
                jsonParams.put("dail_code", binding.myloginDialogFormCountryCodeTextview.getText().toString().trim());
                jsonParams.put("mobile_number", binding.myloginDialogFormMobileNoEdittext.getText().toString().trim());
                jsonParams.put("mode", "request");
                isemailchange = false;
                countapihit = 1;
                profileviewpagerviewmodel.changemobile(Iconstant.profilemobilechange, jsonParams);
            } else {
                appUtils.AlertError(getActivity(), db.getvalue("action_error"), db.getvalue("noch"));
            }
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
