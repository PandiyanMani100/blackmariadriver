package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class withdrawpaymentpojo implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }
    public class Response {
        @SerializedName("available_amount")
        private String available_amount;
        @SerializedName("currency")
        private String currency;
        @SerializedName("last_withdrawal_txt")
        private String last_withdrawal_txt;

        public ArrayList<Payment> getPayment() {
            return payment;
        }

        public void setPayment(ArrayList<Payment> payment) {
            this.payment = payment;
        }

        @SerializedName("payment")
        ArrayList < Payment > payment = new ArrayList< Payment >();
        @SerializedName("proceed_date")
        private String proceed_date;
        @SerializedName("proceed_status")
        private String proceed_status;
        @SerializedName("proceed_error")
        private String proceed_error;
        @SerializedName("min_amount")
        private String min_amount;
        @SerializedName("max_amount")
        private String max_amount;


        public class Payment{
            @SerializedName("name")
            private String name;
            @SerializedName("code")
            private String code;
            @SerializedName("icon")
            private String icon;
            @SerializedName("inactive_icon")
            private String inactive_icon;


            // Getter Methods 

            public String getName() {
                return name;
            }

            public String getCode() {
                return code;
            }

            public String getIcon() {
                return icon;
            }

            public String getInactive_icon() {
                return inactive_icon;
            }

            // Setter Methods 

            public void setName(String name) {
                this.name = name;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public void setIcon(String icon) {
                this.icon = icon;
            }

            public void setInactive_icon(String inactive_icon) {
                this.inactive_icon = inactive_icon;
            }
        }
        // Getter Methods

        public String getAvailable_amount() {
            return available_amount;
        }

        public String getCurrency() {
            return currency;
        }

        public String getLast_withdrawal_txt() {
            return last_withdrawal_txt;
        }

        public String getProceed_date() {
            return proceed_date;
        }

        public String getProceed_status() {
            return proceed_status;
        }

        public String getProceed_error() {
            return proceed_error;
        }

        public String getMin_amount() {
            return min_amount;
        }

        public String getMax_amount() {
            return max_amount;
        }

        // Setter Methods

        public void setAvailable_amount(String available_amount) {
            this.available_amount = available_amount;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public void setLast_withdrawal_txt(String last_withdrawal_txt) {
            this.last_withdrawal_txt = last_withdrawal_txt;
        }

        public void setProceed_date(String proceed_date) {
            this.proceed_date = proceed_date;
        }

        public void setProceed_status(String proceed_status) {
            this.proceed_status = proceed_status;
        }

        public void setProceed_error(String proceed_error) {
            this.proceed_error = proceed_error;
        }

        public void setMin_amount(String min_amount) {
            this.min_amount = min_amount;
        }

        public void setMax_amount(String max_amount) {
            this.max_amount = max_amount;
        }
    }

}

