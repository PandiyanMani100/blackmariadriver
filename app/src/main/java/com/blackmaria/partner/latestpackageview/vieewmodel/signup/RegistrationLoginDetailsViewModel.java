package com.blackmaria.partner.latestpackageview.vieewmodel.signup;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;

import androidx.core.content.FileProvider;
import androidx.lifecycle.ViewModel;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferService;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.util.IOUtils;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.CountryDialCode;
import com.blackmaria.partner.Utils.GPSTracker;
import com.blackmaria.partner.Utils.HeaderSessionManager;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.adapter.Company_of_registrationadapter;
import com.blackmaria.partner.latestpackageview.Adapter.Type_of_registrationadapter;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.signup.RegistrationPersonalDetails;
import com.blackmaria.partner.databinding.RegisterLoginDetailsConstrainBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;
import com.blackmaria.partner.latestpackageview.Countrycodepicker.countrycodepicker.CountryPicker;
import com.blackmaria.partner.latestpackageview.Countrycodepicker.countrycodepicker.CountryPickerListener;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.enums.SnackbarType;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;
import static com.yalantis.ucrop.util.BitmapLoadUtils.exifToDegrees;

public class RegistrationLoginDetailsViewModel extends ViewModel implements ApIServices.completelisner {

    private Activity context;
    private SessionManager sessionManager;
    private ConnectionDetector cd;
    String companyidsss="";
    public CountryPicker picker;
    private HeaderSessionManager headerSessionManager;
    private String Agent_Name = "", language_code = "";
    private String sDriverID = "", gcmID = "";
    private static final int PERMISSION_REQUEST_CODE = 111;
    int permissionCode = -1;
    private boolean isInternetPresent = false;
    private static final int SELECT_IMAGE_REQUEST = 011;
    private static final int TAKE_PHOTO_REQUEST = 022;
    private File imageRoot, destination;
    private Uri mImageCaptureUri, outputUri;
    String appDirectoryName = "";
    private String CarCatId = "", SDriverReferalCode = "", driverLocationPlaceId = "", SimageName = "";
    private byte[] array = null;
    private String sReferralDriverName = "", sReferralDriverPercentage = "", sReferralDriverImage = "", sReferralCode = "", referalStatus = "";
    ArrayList<String> idss = new ArrayList<String>();
    private GPSTracker gpsTracker;
    private RegisterLoginDetailsConstrainBinding binding;
    public Getcountrycodepicker getcountrycodepicker;
    private Dialog dialog;
    private int count = 0;

    String File_names = "";
    String Extension = "";
    String uploaded_file_name = "";
    private String s3_bucket_access_key = "";
    private String s3_bucket_secret_key = "";
    private String s3_bucket_name = "";
    private AmazonS3Client s3Client;
    private BasicAWSCredentials credentials;
    TransferObserver uploadObserver;
    AccessLanguagefromlocaldb db;


    public interface Getcountrycodepicker {
        void triggerpicker();
    }

    public RegistrationLoginDetailsViewModel(Activity context) {
        this.context = context;
        db = new AccessLanguagefromlocaldb(context);
        headerSessionManager = new HeaderSessionManager(context);
        sessionManager = SessionManager.getInstance(context);
        cd = ConnectionDetector.getInstance(context);
        isInternetPresent = cd.isConnectingToInternet();
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        HashMap<String, String> header = headerSessionManager.getHeaderSession();
        Agent_Name = header.get(HeaderSessionManager.KEY_ID_NAME);
        language_code = header.get(HeaderSessionManager.KEY_LANGUAGE_CODE);
        picker = CountryPicker.newInstance(db.getvalue("select_country_lable"));
        gcmID = user.get(SessionManager.KEY_GCM_ID);
        gpsTracker = new GPSTracker(context);
    }

    public void setpickercontext(Getcountrycodepicker getcountrycodepicker, String CarCatId, String driverLocationPlaceId) {
        this.getcountrycodepicker = getcountrycodepicker;
        this.CarCatId = CarCatId;
        this.driverLocationPlaceId = driverLocationPlaceId;
    }

    private void settypeofregistration() {
        String spinnerData[] = context.getResources().getStringArray(R.array.type_of_reg);
        ArrayList<String> regTypeList = new ArrayList<String>();
        regTypeList.addAll(Arrays.asList(spinnerData));
        Type_of_registrationadapter customSpinnerbank = new Type_of_registrationadapter(context, regTypeList);
        binding.selectregistration.setAdapter(customSpinnerbank);

        if (gpsTracker.canGetLocation() && gpsTracker.isgpsenabled()) {

            double MyCurrent_lat = gpsTracker.getLatitude();
            double MyCurrent_long = gpsTracker.getLongitude();
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            try {
                List<Address> addresses = geocoder.getFromLocation(MyCurrent_lat, MyCurrent_long, 1);
                if (addresses != null && !addresses.isEmpty()) {

                    String Str_getCountryCode = addresses.get(0).getCountryCode();
                    if (Str_getCountryCode.length() > 0 && !Str_getCountryCode.equals(null) && !Str_getCountryCode.equals("null")) {
                        String Str_countyCode = CountryDialCode.getCountryCode(Str_getCountryCode);
                        binding.phonecode.setText(Str_countyCode);
                        String drawableName = "flag_"
                                + Str_getCountryCode.toLowerCase(Locale.ENGLISH);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode) {
                picker.dismiss();
                binding.phonecode.setText(dialCode);

                String drawableName = "flag_"
                        + code.toLowerCase(Locale.ENGLISH);
//                Iv_flag.setImageResource(getResId(drawableName));

                InputMethodManager mgr = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                mgr.hideSoftInputFromWindow(binding.phonecode.getWindowToken(), 0);
            }
        });

        binding.selectregistration.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                binding.locationtext.setText(item);
                if(item.equalsIgnoreCase("Company"))
                {
                    binding.typeofsregxis.setVisibility(View.VISIBLE);
                    binding.choosecompanyspinnearre.setVisibility(View.VISIBLE);
                }
                else
                {
                    binding.typeofsregxis.setVisibility(View.GONE);
                    binding.choosecompanyspinnearre.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void setiDs(RegisterLoginDetailsConstrainBinding binding) {
        this.binding = binding;
        settypeofregistration();
    }
    public void getcompanynames(String Url,String driverLocationPlaceId) {

        count = 3;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("location_id", driverLocationPlaceId);


        ApIServices apIServices = new ApIServices(context, RegistrationLoginDetailsViewModel.this);
        apIServices.dooperation(Url, jsonParams);

    }
    public void backclick() {
        context.onBackPressed();
    }

    public void selectimage() {
        checkAndGrantPermission(0);
    }

    private void chooseimage() {
        final Dialog photo_dialog = new Dialog(context);
        photo_dialog.getWindow();
        photo_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        photo_dialog.setContentView(R.layout.image_upload_dialog);
        photo_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        photo_dialog.setCanceledOnTouchOutside(true);
        photo_dialog.getWindow().getAttributes().windowAnimations = R.style.Animations_photo_Picker;
        photo_dialog.show();
        photo_dialog.getWindow().setGravity(Gravity.CENTER);

        RelativeLayout camera = (RelativeLayout) photo_dialog
                .findViewById(R.id.profilelayout_takephotofromcamera);
        RelativeLayout gallery = (RelativeLayout) photo_dialog
                .findViewById(R.id.profilelayout_takephotofromgallery);

        TextView howdo = (TextView) photo_dialog
                .findViewById(R.id.howdo);
        TextView tph = (TextView) photo_dialog
                .findViewById(R.id.tph);
        TextView chosex = (TextView) photo_dialog
                .findViewById(R.id.chosex);
        howdo.setText(db.getvalue("takephoto"));
        tph.setText(db.getvalue("camra"));
        chosex.setText(db.getvalue("existingcamera"));

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraIntent();
                photo_dialog.dismiss();
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                galleryIntent();
                photo_dialog.dismiss();
            }
        });
    }

    private void checkAndGrantPermission(int code) {

        // Permission Code -> For Identifying Whether the click is from take photo (or) Pick Image From Galley
        permissionCode = code;

        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            if (!checkCameraPermission() || !checkReadExternalStoragePermission() || !checkWriteExternalStoragePermission()) {
                requestPermission();
            } else {
                chooseimage();
            }
        } else {
            chooseimage();
        }
    }

    private boolean checkCameraPermission() {
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkReadExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkWriteExternalStoragePermission() {
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(context, new String[]{Manifest.permission.CAMERA, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
    }

    public void cameraIntent() {
        try {
            Intent pictureIntent = new Intent(
                    MediaStore.ACTION_IMAGE_CAPTURE);
            if (pictureIntent.resolveActivity(context.getPackageManager()) != null) {
                //Create a file to store the image
                imageRoot = new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_PICTURES), appDirectoryName);
            }
            if (imageRoot != null) {
                String name = dateToString(new Date(), "yyyy-MM-dd-hh-mm-ss");
                destination = new File(imageRoot, name + ".jpg");
                Uri photoURI = FileProvider.getUriForFile(context, "com.blackmaria.provider", destination);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        photoURI);
                pictureIntent.putExtra("android.intent.extras.CAMERA_FACING", 1);
                context.startActivityForResult(pictureIntent,
                        TAKE_PHOTO_REQUEST);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String dateToString(Date date, String format) {
        SimpleDateFormat df = new SimpleDateFormat(format);
        return df.format(date);
    }

    public void galleryIntent() {
        try {

            imageRoot = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES), appDirectoryName);
            if (!imageRoot.exists()) {
                imageRoot.mkdir();
            }

            Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setType("image/*");
            context.startActivityForResult(intent, SELECT_IMAGE_REQUEST);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onCaptureImageResult(Intent data) {
        try {
            String imagePath = destination.getAbsolutePath();
            mImageCaptureUri = Uri.fromFile(new File(imagePath));
            outputUri = mImageCaptureUri;

//            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), mImageCaptureUri);

            UCrop.Options options = new UCrop.Options();
            options.setStatusBarColor(context.getResources().getColor(R.color.black_color));
            options.setToolbarColor(context.getResources().getColor(R.color.app_primary_color));
            options.setMaxBitmapSize(800);

            UCrop.of(mImageCaptureUri, outputUri)
                    .withAspectRatio(1, 1)
                    .withMaxResultSize(8000, 8000)
                    .withOptions(options)
                    .start(context);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onSelectFromGalleryResult(Intent data) {
        try {
            mImageCaptureUri = data.getData();
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), mImageCaptureUri);
            if (!imageRoot.exists()) {
                imageRoot.mkdir();
            }
            String name = dateToString(new Date(), "yyyy-MM-dd-hh-mm-ss");
            destination = new File(imageRoot, name + ".jpg");
            outputUri = Uri.fromFile(destination);
            UCrop.Options options = new UCrop.Options();
            options.setStatusBarColor(context.getResources().getColor(R.color.black_color));
            options.setToolbarColor(context.getResources().getColor(R.color.app_primary_color));
            options.setMaxBitmapSize(800);

            UCrop.of(mImageCaptureUri, outputUri)
                    .withAspectRatio(1, 1)
                    .withMaxResultSize(8000, 8000)
                    .withOptions(options)
                    .start(context);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void onCroppedImageResult(Intent data) {

        final Uri resultUri = UCrop.getOutput(data);

        Log.d("Crop success", "" + resultUri);
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), resultUri);

            if (bitmap == null) {
                Log.d("Bitmap", "null");
            } else {
                Log.d("Bitmap", "not null");
                binding.ivUser.setImageBitmap(bitmap);

            }

            Bitmap thumbnail = bitmap;
            final String picturePath = resultUri.getPath();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            File curFile = new File(picturePath);
            try {
                ExifInterface exif = new ExifInterface(curFile.getPath());
                int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
                int rotationInDegrees = exifToDegrees(rotation);

                Matrix matrix = new Matrix();
                if (rotation != 0f) {
                    matrix.preRotate(rotationInDegrees);
                }
                thumbnail = Bitmap.createBitmap(thumbnail, 0, 0, thumbnail.getWidth(), thumbnail.getHeight(), matrix, true);
            } catch (IOException ex) {
                Log.e("TAG", "Failed to get Exif data", ex);
            }

            thumbnail.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
            array = byteArrayOutputStream.toByteArray();

            if (isInternetPresent)
            {
                UploadDriverImage(Iconstant.DriverReg_upload_Image_URl,curFile);

            } else {
                Alert(db.getvalue("alert_nointernet"), db.getvalue("alert_nointernet_message"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //--------------Alert Method-----------
    private void Alert(String title, String alert) {

        final PkDialog mDialog = new PkDialog(context);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(db.getvalue("close_lable"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    private void UploadDriverImage(String url,File resultUri) {
        count = 2;


        if(sessionManager.getimagestatus().equals("1"))
        {
            s3_bucket_name = sessionManager.getbucketname();
            s3_bucket_access_key = sessionManager.getaccesskey();
            s3_bucket_secret_key = sessionManager.getsecretkey();

            context.startService(new Intent(context, TransferService.class));
            credentials = new BasicAWSCredentials(s3_bucket_access_key, s3_bucket_secret_key);
            s3Client = new AmazonS3Client(credentials);
            s3Client.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));
            uploadToS3Bucket(resultUri,context);

        }
        else
        {
            binding.apiload.setVisibility(View.VISIBLE);
            dialog = new Dialog(context, R.style.CustomDialogTheme);
            dialog.getWindow();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.loading_model);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
            ApIServices apIServices = new ApIServices(context, RegistrationLoginDetailsViewModel.this);
            apIServices.imageupload(url, array, "blackmariadriver.jpg");
        }
    }

    public void verifyreferral() {
        if (isInternetPresent) {
            if (binding.tvRefferalcode.getText().toString().trim().length() > 0) {
                applyReferalCode(Iconstant.DriverReg_applyReferal_URl);
            } else {
                AlertError(db.getvalue("timer_label_alert_sorry"), db.getvalue("otp_page_referral_error"));
            }

        } else {
            Alert(db.getvalue("alert_nointernet"), db.getvalue("alert_nointernet_message"));
        }
    }

    public void countrycodepicker() {
        getcountrycodepicker.triggerpicker();

    }

    private void applyReferalCode(String Url) {
        count = 1;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("code", binding.tvRefferalcode.getText().toString().trim());

        ApIServices apIServices = new ApIServices(context, RegistrationLoginDetailsViewModel.this);
        apIServices.dooperation(Url, jsonParams);
    }

    private void CheckMobileNum_verify(String Url) {
        count = 0;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("dail_code", binding.phonecode.getText().toString().trim());
        jsonParams.put("mobile_number", binding.editMobilenumber.getText().toString().trim());
        jsonParams.put("location_id", driverLocationPlaceId);

        ApIServices apIServices = new ApIServices(context, RegistrationLoginDetailsViewModel.this);
        apIServices.dooperation(Url, jsonParams);

    }

    @SuppressLint("SetTextI18n")
    private void showVerifiedAlert() {

        //--------Adjusting Dialog width and height-----
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 85% of the screen
        int screenHeight = (int) (metrics.heightPixels * 0.80);//fill only 80% of the screen

        final Dialog verifiedDialog = new Dialog(context, R.style.SlideUpDialog);
        verifiedDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        verifiedDialog.setContentView(R.layout.alert_verified_status);
        verifiedDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        verifiedDialog.getWindow().setLayout(screenWidth, screenHeight);

        final ImageView Iv_profile = (ImageView) verifiedDialog.findViewById(R.id.img_profile);
        final TextView Tv_driverName = (TextView) verifiedDialog.findViewById(R.id.txt_user_name);
        final TextView Tv_content1 = (TextView) verifiedDialog.findViewById(R.id.txt_content1);
        final TextView Tv_earnAtLabel = (TextView) verifiedDialog.findViewById(R.id.txt_label_earn_percentage);
        final TextView Tv_earnPercentage = (TextView) verifiedDialog.findViewById(R.id.txt_earn_percentage);
        final Button Btn_proceed = (Button) verifiedDialog.findViewById(R.id.btn_proceed);
        final ImageView img_close = (ImageView) verifiedDialog.findViewById(R.id.img_close);

        if (sReferralDriverImage.length() > 0) {
            Picasso.with(context).load(sReferralDriverImage).placeholder(R.drawable.no_user_profile).error(R.drawable.no_user_profile).resize(150, 150).memoryPolicy(MemoryPolicy.NO_CACHE).into(Iv_profile);
        }
        Tv_driverName.setText(sReferralDriverName);
        Tv_content1.setText(sReferralDriverName + "\t" + db.getvalue("referral_verified_content1"));
        Tv_earnAtLabel.setText(sReferralDriverName + "\t" + db.getvalue("referral_verified_earn_at"));
        Tv_earnPercentage.setText(sReferralDriverPercentage + "%");

        Btn_proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifiedDialog.dismiss();
            }
        });
        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifiedDialog.dismiss();
            }
        });

        verifiedDialog.show();

    }

    private void AlertError(String title, String message) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.snack_view, null, false);
        TextView Tv_title = (TextView) view.findViewById(R.id.txt_title);
        TextView Tv_message = (TextView) view.findViewById(R.id.txt_message);

        Tv_title.setText(title);
        Tv_message.setText(message);

        Snackbar bar = Snackbar.with(context);
        bar.addView(view);
        bar.colorResource(R.color.dark_red);
        bar.position(Snackbar.SnackbarPosition.TOP);
        bar.type(SnackbarType.MULTI_LINE);
        bar.duration(Snackbar.SnackbarDuration.LENGTH_SHORT);
        bar.animation(true);
        SnackbarManager.show(bar, context);
    }

    public void confirmvalidation() {
        String sSelectedItem = binding.selectregistration.getSelectedItem().toString();
        if (SimageName.equalsIgnoreCase("")) {
            AlertError(db.getvalue("timer_label_alert_sorry"), db.getvalue("uplo"));
        }
        else if (sSelectedItem.equalsIgnoreCase( db.getvalue("type_of_registration"))) {
            AlertError(db.getvalue("timer_label_alert_sorry"), db.getvalue("chre"));
        }
        else if (sSelectedItem.equalsIgnoreCase("Company") && companyidsss.equalsIgnoreCase("")) {
            AlertError(db.getvalue("timer_label_alert_sorry"), db.getvalue("select_your_campany"));
        }
        else if (binding.phonecode.getText().toString().length() == 0) {
            AlertError(db.getvalue("timer_label_alert_sorry"), db.getvalue("cdcanntem"));
        } else if (binding.editMobilenumber.getText().toString().trim().length() == 0) {
            AlertError(db.getvalue("timer_label_alert_sorry"), db.getvalue("mobcannem"));
        } else if (!isValidPhoneNumber(binding.editMobilenumber.getText().toString())) {
            AlertError(db.getvalue("timer_label_alert_sorry"), db.getvalue("invm"));
        } else if (binding.editPincode.getText().toString().trim().length() == 0) {
            AlertError(db.getvalue("timer_label_alert_sorry"), db.getvalue("profile_label_alert_pincode"));
        } else if (binding.editPincode.getText().toString().trim().length() != 6) {
            AlertError(db.getvalue("timer_label_alert_sorry"), db.getvalue("pincodemustbedigit"));
        }
        else if (binding.tvRefferalcode.getText().toString().trim().length() > 0 && referalStatus.equalsIgnoreCase("")) {
            AlertError(db.getvalue("timer_label_alert_sorry"), db.getvalue("profile_label_alert_referala"));
        }

        else if (binding.tvRefferalcode.getText().toString().trim().length() > 0 && referalStatus.equalsIgnoreCase("0")) {
            AlertError(db.getvalue("timer_label_alert_sorry"), db.getvalue("profile_label_alert_referal"));
        }
        else {
            if (isInternetPresent) {
                CheckMobileNum_verify(Iconstant.Verifymobile);
            } else {
                Alert(db.getvalue("alert_nointernet"), db.getvalue("alert_nointernet_message"));
            }
        }
    }

    // validating Phone Number
    public static final boolean isValidPhoneNumber(CharSequence target) {
        if (target == null || TextUtils.isEmpty(target) || target.length() <= 5) {
            return false;
        } else {
            return Patterns.PHONE.matcher(target).matches();
        }
    }

    @Override
    public void sucessresponse(String response) {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        if (count == 0) {
            String status = "", Sresponse = "", otpStatus = "", otpPin = "";
            try {
                JSONObject object = new JSONObject(response);
                status = object.getString("status");
                Sresponse = object.getString("response");
                if (status.equalsIgnoreCase("1")) {
                    otpStatus = object.getString("otp_status");
                    otpPin = object.getString("otp");
                }
            } catch (JSONException e) {
                e.printStackTrace();
                dialog.dismiss();
            }

            if (status.equalsIgnoreCase("1")) {

                sessionManager.setOtpstatusAndPin(otpStatus, otpPin);

                Intent intent = new Intent(context, RegistrationPersonalDetails.class);
                intent.putExtra("driverVehicleId", CarCatId);
                intent.putExtra("driverTypeofregistration", binding.locationtext.getText().toString());
                intent.putExtra("driverLocationPlaceId", driverLocationPlaceId);
                intent.putExtra("driverLocationPlaceId", driverLocationPlaceId);
                intent.putExtra("driverProImage", SimageName);
                intent.putExtra("driverCountryCode", binding.phonecode.getText().toString().trim());
                intent.putExtra("driverPhoneNumber", binding.editMobilenumber.getText().toString().trim());
                intent.putExtra("driverReferelCode", sReferralCode);
                intent.putExtra("companyid", companyidsss);
                intent.putExtra("driverPinCode", binding.editPincode.getText().toString().trim());
                context.startActivity(intent);
                context.overridePendingTransition(R.anim.enter, R.anim.exit);
            } else {
                Alert("Sorry", Sresponse);
            }
        } else if (count == 1) {
            try {
                JSONObject object = new JSONObject(response);
                String status = object.getString("status");
                String Sresponse = object.getString("response");
                referalStatus = status;
                if (status.equalsIgnoreCase("1")) {

                    sReferralDriverName = object.getString("driver_name");
                    sReferralDriverPercentage = object.getString("ref_per");
                    sReferralDriverImage = object.getString("driver_image");
                    if(sReferralDriverImage.contains("imagePath"))
                    {
                        sReferralDriverImage =sReferralDriverImage.replace("imagePath","https://blackmaria-01.s3.amazonaws.com/images/users/");
                    }

                    sReferralCode = binding.tvRefferalcode.getText().toString().trim();
                    showVerifiedAlert();
                } else {
                    sReferralCode = "";
                    Alert(db.getvalue("action_error"), Sresponse);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (count == 2) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                String sStatus = jsonObject.getString("status");
                JSONObject imagObj = jsonObject.getJSONObject("response");
                if (sStatus.equalsIgnoreCase("1")) {
                    SimageName = imagObj.getString("image_name");
                } else {
                    String Smsg = jsonObject.getString("response");
                    Alert(db.getvalue("timer_label_alert_sorry"), Smsg);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                dialog.dismiss();
            }
        }
        else if (count == 3) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                String sStatus = jsonObject.getString("status");

                if (sStatus.equalsIgnoreCase("1")) {

                    ArrayList<String> companynames = new ArrayList<String>();
                    idss.clear();
                    companynames.clear();

                    companynames.add(db.getvalue("ccse"));
                    idss.add("");

                    JSONObject response_object = jsonObject.getJSONObject("response");
                    Object check_payment_list_object = response_object.get("company");
                    if (check_payment_list_object instanceof JSONArray) {
                        JSONArray payment_list_jsonArray = response_object.getJSONArray("company");
                        if (payment_list_jsonArray.length() > 0) {
                            for (int i = 0; i < payment_list_jsonArray.length(); i++) {
                                JSONObject payment_obj = payment_list_jsonArray.getJSONObject(i);
                                idss.add(payment_obj.getString("id"));
                                companynames.add(payment_obj.getString("company_name"));
                            }
                        }


                        Company_of_registrationadapter customSpinnerbank = new Company_of_registrationadapter(context, companynames);
                        binding.choosecompany.setAdapter(customSpinnerbank);

                        binding.choosecompany.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                String item = parent.getItemAtPosition(position).toString();
                                companyidsss=idss.get(position);
                                binding.companytextt.setText(item);

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    }




                } else {
                    String Smsg = jsonObject.getString("response");
                    Alert(db.getvalue("timer_label_alert_sorry"), db.getvalue("company_not_available"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
                dialog.dismiss();
            }
        }
    }

    @Override
    public void errorreponse() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void jsonexception() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        AppUtils.toastprint(context, "Json Exception");
    }

    private void uploadToS3Bucket(File fileUri,Activity context)
    {

        if(fileUri.exists())
        {
            System.out.println("file exist");
        }
       else
        {
            System.out.println("file exist not");
        }


        String[] path_split;
        if (fileUri != null) {

            final Dialog dialog = new Dialog(context);
            dialog.getWindow();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.custom_loading);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();

            uploaded_file_name = "";
            String path = String.valueOf(fileUri);
            String file_name_full = path.substring(path.lastIndexOf("/") + 1);
            path_split = file_name_full.split(".");
            if (path_split.length == 2) {
                File_names = path_split[0];
                Extension = path_split[1];

            } else {
                path_split = file_name_full.split(".jpg");
                if (path_split.length == 1) {
                    File_names = path_split[0];
                }
            }

            String file_name_upload = File_names;
            //current time stamp
            Long tsLong = System.currentTimeMillis() / 1000;
            String str_current_ts = tsLong.toString();
            String concat_image_name = file_name_upload + "" + str_current_ts;
            final String fileName = md5Encryption(concat_image_name) + "." + "jpg";


            TransferUtility transferUtility =
                    TransferUtility.builder()
                            .context(context)
                            .defaultBucket(s3_bucket_name)
                            .s3Client(s3Client)
                            .build();
            uploaded_file_name = fileName;

            uploadObserver = transferUtility.upload("images/users/" + fileName, fileUri, CannedAccessControlList.PublicRead);
            // Attach a listener to the observer to get state update and progress notifications
            uploadObserver.setTransferListener(new TransferListener() {

                @Override
                public void onStateChanged(int id, TransferState state) {
                    // Handle a completed upload.
                    if (TransferState.COMPLETED == state) {
                        String path = uploadObserver.getAbsoluteFilePath();
                        SimageName = fileName;
                        dialog.dismiss();
                    } else if (TransferState.FAILED == state) {
                        // dialog.dismiss();
                        dialog.dismiss();
                    }
                }

                @Override
                public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                    float percentDonef = ((float) bytesCurrent / (float) bytesTotal) * 100;
                    int percentDone = (int) percentDonef;
                    Log.d("YourActivity", "ID:" + id + " bytesCurrent: " + bytesCurrent + " bytesTotal: " + bytesTotal + " " + percentDone + "%");
                }

                @Override
                public void onError(int id, Exception ex) {
                    // Handle errors
                    ex.printStackTrace();
                    uploaded_file_name = "";

                    dialog.dismiss();
                }

            });

            // If you prefer to poll for the data, instead of attaching a
            // listener, check for the state and progress in the observer.
            if (TransferState.COMPLETED == uploadObserver.getState()) {
                // Handle a completed upload.
                Log.d("completed upload", "completed upload");
            }

        }
    }


    public String md5Encryption(String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    private void createFile(Context context, Uri srcUri, File dstFile) {
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(srcUri);
            if (inputStream == null) return;
            OutputStream outputStream = new FileOutputStream(dstFile);
            IOUtils.copy(inputStream, outputStream);
            inputStream.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
