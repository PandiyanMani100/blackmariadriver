package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class weekviewdetailpojo implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }

    public class Response {
        public ArrayList<Week> getWeek() {
            return week;
        }

        public void setWeek(ArrayList<Week> week) {
            this.week = week;
        }

        @SerializedName("week")
        ArrayList< Week > week = new ArrayList < Week > ();
        @SerializedName("total_rides")
        private String total_rides;
        @SerializedName("date_desc")
        private String date_desc;



        // Getter Methods

        public String getTotal_rides() {
            return total_rides;
        }

        public String getDate_desc() {
            return date_desc;
        }

        // Setter Methods

        public void setTotal_rides(String total_rides) {
            this.total_rides = total_rides;
        }

        public void setDate_desc(String date_desc) {
            this.date_desc = date_desc;
        }
    }
}

