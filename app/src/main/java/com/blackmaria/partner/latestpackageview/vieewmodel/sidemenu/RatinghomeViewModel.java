package com.blackmaria.partner.latestpackageview.vieewmodel.sidemenu;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import com.blackmaria.partner.R;

import com.blackmaria.partner.databinding.ActivityRatingHomepageBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Mytrips.TripSummaryConstrain;
import com.blackmaria.partner.latestpackageview.view.sidemenu.Ratings.Commentslist_constrain;


import java.util.HashMap;

public class RatinghomeViewModel extends ViewModel implements ApIServices.completelisner {
    private Dialog dialog;
    public Activity context;
    private ActivityRatingHomepageBinding binding;
    private MutableLiveData<String> reviewlist = new MutableLiveData<>();
    private String rideid="";

    public RatinghomeViewModel(Activity context) {
        this.context = context;
    }

    public void setIDs(ActivityRatingHomepageBinding binding) {
        this.binding = binding;
    }

    public void setRideid(String rideid){
        this.rideid = rideid;
    }

    public void getreviewlist(String driverid) {
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", driverid);
        ApIServices apIServices = new ApIServices(context, RatinghomeViewModel.this);
        apIServices.dooperation(Iconstant.menupage_ratinglist_home, jsonParams);
    }

    public void viewallreviews() {
        Intent intent = new Intent(context, Commentslist_constrain.class);
        context.startActivity(intent);
        context.overridePendingTransition(R.anim.enter, R.anim.exit);
    }

    public void tripinfo(){

        Intent rideids = new Intent(context, TripSummaryConstrain.class);
        rideids.putExtra("rideid",rideid);
        context.startActivity(rideids);
        context.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    public MutableLiveData<String> getReviewlist() {
        return reviewlist;
    }

    @Override
    public void sucessresponse(String val) {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        reviewlist.postValue(val);
    }

    @Override
    public void errorreponse() {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
    }

    @Override
    public void jsonexception() {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        AppUtils.toastprint(context, "Json Exception");
    }
}
