package com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Withdraw.Bank;


import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityNobankaccountConstrainBinding;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Wallet.Withdraw.Bank.NobankaccountFactory;
import com.blackmaria.partner.latestpackageview.Model.withdrawalspaymentspojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet.withdraw.bank.NobankaccountViewModel;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;

public class NobankaccountConstrain extends AppCompatActivity {

    private ActivityNobankaccountConstrainBinding binding;
    private NobankaccountViewModel nobankaccountViewModel;
    private SessionManager session;
    private AppUtils appUtils;
    private String withdrawamount = "";
    private JSONObject json;
    private withdrawalspaymentspojo pojo;
    AccessLanguagefromlocaldb db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
        binding = DataBindingUtil.setContentView(NobankaccountConstrain.this, R.layout.activity_nobankaccount_constrain);
        nobankaccountViewModel = ViewModelProviders.of(this, new NobankaccountFactory(this)).get(NobankaccountViewModel.class);
        binding.setNobankaccountViewModel(nobankaccountViewModel);
        nobankaccountViewModel.setIds(binding);

        initView();
        clicklistener();

        binding.datemonthyear.setText(db.getvalue("fastwallet"));
        binding.withdraw.setText(db.getvalue("withdraw_lable"));
        binding.sorry.setText(db.getvalue("sorry"));
        binding.sorrynextcontent.setText(db.getvalue("sorrynoaccount"));
        binding.updateprofilecontent.setText(db.getvalue("updateprofilecontent"));
        binding.continuewithdraw.setText(db.getvalue("updateprofile"));
        binding.cancel.setText(db.getvalue("cancel_lable"));
    }

    private void initView() {
        session = SessionManager.getInstance(this);
        appUtils = AppUtils.getInstance(this);
        try {
            if (getIntent().hasExtra("transfer_amount")) {
                withdrawamount = getIntent().getStringExtra("transfer_amount");
                json = new JSONObject(getIntent().getStringExtra("json"));
                Type type = new TypeToken<withdrawalspaymentspojo>() {
                }.getType();
                pojo = new GsonBuilder().create().fromJson(json.toString(), type);
            }
        }catch (JSONException e){e.printStackTrace();}
    }

    private void clicklistener() {
        binding.continuewithdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(NobankaccountConstrain.this, AddbankdetailsConstrain.class);
                i.putExtra("transfer_amount", withdrawamount);
                i.putExtra("json", json.toString());
                startActivity(i);
            }
        });
    }

}
