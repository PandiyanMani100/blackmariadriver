package com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.newdesign.slider.CirclePageIndicator;
import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.ConnectionDetector;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Model.Fastpayhome_pojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Fragments.FragmentTab1;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Fragments.FragmentTab2;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Fragments.FragmentTab3;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Transfer.Transfermobilenumbersearch;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Withdraw.WithdrawpaymentschooseConstrain;
import com.blackmaria.partner.latestpackageview.widgets.PkDialogtryagain;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

public class Fastpayhome extends AppCompatActivity implements ApIServices.completelisner {

    private ImageView booking_back_imgeview;
    private TextView tv_menu, tv_balamount, tv_paylateramount;
    private LinearLayout sendli, withdrawli, topupli;
    private ViewPager mPager;
    private int currentPage = 0;
    private int NUM_PAGES = 0;
    private ConstraintLayout downlayouts, menuview;
    private boolean isdown = false;
    private String sDriverID = "", jsonpojo = "";
    private SessionManager session;
    private HashMap<String, String> info;
    private Fastpayhome_pojo homepage;
    private ConnectionDetector cd;
    private ProgressBar apiload;
    private Dialog dialog;
    private AppUtils appUtils;
    AccessLanguagefromlocaldb db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.walletnewpage);
        db = new AccessLanguagefromlocaldb(this);
        System.gc();
        initView();
        clicklistener();
    }

    @Override
    public void sucessresponse(String response) {
        dialog.dismiss();
        apiload.setVisibility(View.INVISIBLE);
        String Sstatus = "", Smessage = "";
        dialog.dismiss();
        JSONObject object = null;
        try {
            object = new JSONObject(response);
            Sstatus = object.getString("status");
            if (Sstatus.equalsIgnoreCase("1")) {
                Type listType = new TypeToken<Fastpayhome_pojo>() {
                }.getType();
                homepage = new GsonBuilder().create().fromJson(object.toString(), listType);
                Gson gson = new Gson();
                Type type = new TypeToken<Fastpayhome_pojo>() {
                }.getType();
                jsonpojo = gson.toJson(homepage, type);
                tv_balamount.setText(homepage.getResponse().getCurrency() + " " + homepage.getResponse().getCurrency_balance());
                init_slidebanner();
            } else {
                Smessage = object.getString("response");
                Alert(db.getvalue("action_error"), Smessage);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void errorreponse() {
        dialog.dismiss();
        apiload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void jsonexception() {
        dialog.dismiss();
        apiload.setVisibility(View.INVISIBLE);
    }


    private void initView() {
        cd =  ConnectionDetector.getInstance(Fastpayhome.this);
        session = SessionManager.getInstance(this);
        appUtils = AppUtils.getInstance(this);
        info = session.getUserDetails();
        sDriverID = info.get(SessionManager.KEY_DRIVERID);
        tv_paylateramount = findViewById(R.id.tv_paylateramount);
        mPager = findViewById(R.id.pager);
        apiload = findViewById(R.id.apiload);

        TextView topupp = findViewById(R.id.topupp);
        topupp.setText(db.getvalue("top_up"));

        TextView withdra = findViewById(R.id.withdra);
        withdra.setText(db.getvalue("withdrawl"));

        TextView sendamiut = findViewById(R.id.sendamiut);
        sendamiut.setText(db.getvalue("send"));

        topupli = findViewById(R.id.topupli);
        sendli = findViewById(R.id.sendli);
        withdrawli = findViewById(R.id.withdrawli);
        tv_menu = findViewById(R.id.tv_menu);
        tv_balamount = findViewById(R.id.tv_balamount);
        downlayouts = findViewById(R.id.downlayouts);
        menuview = findViewById(R.id.menuview);
        booking_back_imgeview = findViewById(R.id.booking_back_imgeview);

        fastpayhome(Iconstant.fastpayhome);
    }


    private void clicklistener() {

        topupli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Fastpayhome.this, RechargeAmountConstrain.class);
                startActivity(intent);
            }
        });

        sendli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Fastpayhome.this, Transfermobilenumbersearch.class);
                startActivity(intent);
            }
        });

        withdrawli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(homepage.getResponse().getCurrency_balance().contains("-"))
                {

                    Toast.makeText(getApplicationContext(),db.getvalue("neagtiveamount"),Toast.LENGTH_LONG).show();
                }
                else
                { Intent intent = new Intent(Fastpayhome.this, WithdrawpaymentschooseConstrain.class);
                    startActivity(intent);

                }


//                    Intent intent = new Intent(Fastpayhome.this, NewCloudMoneyWithDrawHome.class);

//                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });


        booking_back_imgeview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(R.anim.enter, R.anim.exit);
            }
        });

        tv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isdown) {
                    isdown = false;
                    slideUp(downlayouts);
                } else {
                    isdown = true;
                    slideDown(downlayouts);
                }

            }
        });

    }

    private void init_slidebanner() {
        mPager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));

    }

    public void slideUp(View view) {
        view.setVisibility(View.GONE);
    }

    // slide the view from its current position to below itself
    public void slideDown(View view) {
        view.setVisibility(View.VISIBLE);
    }

    class MyPagerAdapter extends FragmentPagerAdapter {
        public MyPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:

                    Bundle bundle = new Bundle();
                    bundle.putString("fastpayhome", jsonpojo);
                    FragmentTab1 fm = new FragmentTab1();
                    fm.setArguments(bundle);
                    return fm;


            }
            return null;
        }

        @Override
        public int getCount() {
            return 1;
        }
    }

    private void fastpayhome(String Url) {
        apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(this, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        jsonParams.put("type", "CREDIT");
        ApIServices apIServices = new ApIServices(this, Fastpayhome.this);
        apIServices.dooperation(Url, jsonParams);
    }


    private void Alert(String title, String alert) {
        final PkDialogtryagain mDialog = new PkDialogtryagain(Fastpayhome.this);
        mDialog.setDialogTitle(title);
        mDialog.setDialogMessage(alert);
        mDialog.setPositiveButton(db.getvalue("action_ok"), new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }
}
