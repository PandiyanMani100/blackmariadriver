package com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet.transfer;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import com.blackmaria.partner.R;
import com.blackmaria.partner.databinding.ActivityTransfermobilenumbersearchBinding;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;

import java.util.HashMap;

public class TransfermobilenumbersearchViewModel extends ViewModel implements ApIServices.completelisner {
    private Activity contetxt;
    private ActivityTransfermobilenumbersearchBinding binding;
    private MutableLiveData<String> findfreindresponse = new MutableLiveData<>();
    private Dialog dialog;

    public TransfermobilenumbersearchViewModel(Activity contetxt) {
        this.contetxt = contetxt;
    }


    public MutableLiveData<String> getFindfreindresponse() {
        return findfreindresponse;
    }

    @Override
    public void sucessresponse(String val) {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        findfreindresponse.postValue(val);
    }

    @Override
    public void errorreponse() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void jsonexception() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    public void setIds(ActivityTransfermobilenumbersearchBinding binding) {
        this.binding = binding;
    }

    public void back() {
        contetxt.onBackPressed();
    }

    public void findfriendapihitresponse(String url, HashMap<String, String> jsonParams) {
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(contetxt, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        ApIServices apIServices = new ApIServices(contetxt, TransfermobilenumbersearchViewModel.this);
        apIServices.dooperation(url, jsonParams);
    }
}
