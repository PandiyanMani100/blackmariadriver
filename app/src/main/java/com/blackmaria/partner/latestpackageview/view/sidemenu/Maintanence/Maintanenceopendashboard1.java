package com.blackmaria.partner.latestpackageview.view.sidemenu.Maintanence;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityMaintanenceopendashboard1Binding;
import com.blackmaria.partner.databinding.ActivityMaintanenceopendashboardBinding;
import com.blackmaria.partner.latestpackageview.Factory.Sidemenu.Maintanence.MaintanenceopendashboardFactory;
import com.blackmaria.partner.latestpackageview.Factory.Sidemenu.Maintanence.MaintanenceopendashboardFactory1;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.vieewmodel.sidemenu.maintanence.MaintanenceopendashboardViewModel;
import com.blackmaria.partner.latestpackageview.vieewmodel.sidemenu.maintanence.MaintanenceopendashboardViewModel1;

import java.util.HashMap;

public class Maintanenceopendashboard1 extends AppCompatActivity {

    private ActivityMaintanenceopendashboard1Binding binding;
    private MaintanenceopendashboardViewModel1 maintanenceopendashboardViewModel;
    AccessLanguagefromlocaldb db;

    private SessionManager sessionManager;
    private AppUtils appUtils;
    private String sDriverID = "", driverimage = "", vehicleimage = "", drivername = "", carno = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
        binding = DataBindingUtil.setContentView(Maintanenceopendashboard1.this, R.layout.activity_maintanenceopendashboard1);
        maintanenceopendashboardViewModel = ViewModelProviders.of(this, new MaintanenceopendashboardFactory1(this)).get(MaintanenceopendashboardViewModel1.class);
        binding.setMaintanenceopendashboardViewModel(maintanenceopendashboardViewModel);
        maintanenceopendashboardViewModel.setIds(binding);

        initView();
        clicklistener();
        binding.servicelist.setText(db.getvalue("servicelist"));
        binding.noservierecord.setText(db.getvalue("noservierecord"));
    }


    private void initView() {
        appUtils = AppUtils.getInstance(this);
        sessionManager = SessionManager.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        drivername = user.get(SessionManager.KEY_DRIVER_NAME);
        driverimage = user.get(SessionManager.KEY_DRIVER_IMAGE);
        vehicleimage = user.get(SessionManager.KEY_VEHICLE_IMAGE);
        binding.username.setText(drivername);
        binding.carno.setText(sessionManager.getdrivercarnumber());
        AppUtils.setImageView(Maintanenceopendashboard1.this, driverimage, binding.userimage);
        AppUtils.setImageviewwithoutcrop(Maintanenceopendashboard1.this, sessionManager.getUserVehicle(), binding.caricon);
    }


    private void clicklistener() {
        binding.secondviewimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Maintanenceopendashboard1.this, Addseconddriver.class));
            }
        });
        binding.changevehicleimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Maintanenceopendashboard1.this, EditvehicledetailsConstrain.class));
            }
        });
    }

}
