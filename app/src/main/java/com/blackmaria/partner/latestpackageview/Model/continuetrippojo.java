package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class continuetrippojo implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }

    public class Response {
        @SerializedName("user_profile")
        User_profile User_profileObject;
        @SerializedName("message")
        private String message;
        // Getter Methods

        public User_profile getUser_profile() {
            return User_profileObject;
        }

        public String getMessage() {
            return message;
        }

        // Setter Methods

        public void setUser_profile(User_profile user_profileObject) {
            this.User_profileObject = user_profileObject;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public class User_profile {
            public String getRide_status() {
                return ride_status;
            }

            public void setRide_status(String ride_status) {
                this.ride_status = ride_status;
            }

            @SerializedName("ride_status")
            private String ride_status;

            public String getTotal_passenger() {
                return total_passenger;
            }

            public void setTotal_passenger(String total_passenger) {
                this.total_passenger = total_passenger;
            }

            @SerializedName("total_passenger")
            private String total_passenger;

            public String getDriver_rating() {
                return driver_rating;
            }

            public void setDriver_rating(String driver_rating) {
                this.driver_rating = driver_rating;
            }

            @SerializedName("driver_rating")
            private String driver_rating;
            @SerializedName("user_name")
            private String user_name;
            @SerializedName("user_email")
            private String user_email;
            @SerializedName("user_id")
            private String user_id;
            @SerializedName("user_gender")
            private String user_gender;
            @SerializedName("phone_number")
            private String phone_number;
            @SerializedName("user_image")
            private String user_image;
            @SerializedName("user_review")
            private String user_review;
            @SerializedName("ride_id")
            private String ride_id;
            @SerializedName("member_since")
            private String member_since;
            @SerializedName("location")
            private String location;
            @SerializedName("eta_arrival")
            private String eta_arrival;

            @SerializedName("ios_fcm_key")
            private String ios_fcm_key;
            @SerializedName("user_fcm_token")
            private String user_fcm_token;

            public String getApprox_dist() {
                return approx_dist;
            }

            public void setApprox_dist(String approx_dist) {
                this.approx_dist = approx_dist;
            }

            @SerializedName("approx_dist")
            private String approx_dist;
            @SerializedName("pickup_location")
            private String pickup_location;
            @SerializedName("pickup_lat")
            private String pickup_lat;
            @SerializedName("pickup_lon")
            private String pickup_lon;
            @SerializedName("pickup_time")
            private String pickup_time;
            @SerializedName("continue_trip")
            private String continue_trip;
            @SerializedName("currency")
            private String currency;
            @SerializedName("drop_location")
            private String drop_location;
            @SerializedName("drop_loc")
            private String drop_loc;
            @SerializedName("drop_lat")
            private String drop_lat;
            @SerializedName("drop_lon")
            private String drop_lon;
            @SerializedName("title_text")
            private String title_text;
            @SerializedName("subtitle_text")
            private String subtitle_text;
            @SerializedName("est_cost")
            private String est_cost;
            @SerializedName("is_stop")
            private String is_stop;
            @SerializedName("mode")
            private String mode;

            public ArrayList<Multistop_array> getMultistop_array() {
                return multistop_array;
            }

            public void setMultistop_array(ArrayList<Multistop_array> multistop_array) {
                this.multistop_array = multistop_array;
            }
            @SerializedName("multistop_array")
            ArrayList<Multistop_array> multistop_array = new ArrayList<Multistop_array>();
            @SerializedName("is_multistop_over")
            private String is_multistop_over;
            @SerializedName("is_return_over")
            private String is_return_over;
            @SerializedName("return_mode")
            private String return_mode;

            public Next_drop_location getNext_drop_location() {
                return next_drop_location;
            }

            public void setNext_drop_location(Next_drop_location next_drop_location) {
                this.next_drop_location = next_drop_location;
            }

            @SerializedName("next_drop_location")
            private Next_drop_location next_drop_location;



            public notification_details getNotification_details() {
                return notification_details;
            }

            public void setNotification_details(notification_details notification_details) {
                this.notification_details = notification_details;
            }

            @SerializedName("notification_details")
            private notification_details notification_details;

            public Drop_location_array getDrop_location_array() {
                return drop_location_array;
            }

            public void setDrop_location_array(Drop_location_array drop_location_array) {
                this.drop_location_array = drop_location_array;
            }

            @SerializedName("drop_location_array")
            private Drop_location_array drop_location_array;

            @SerializedName("rider_type")
            private String rider_type;
            @SerializedName("cancelled_rides")
            private String cancelled_rides;
            @SerializedName("completed_rides")
            private String completed_rides;
            @SerializedName("payment_type")
            private String payment_type;
            @SerializedName("comp_status")
            private String comp_status;
            @SerializedName("comp_stage")
            private String comp_stage;
            @SerializedName("payment_icon")
            private String payment_icon;
            @SerializedName("wait_time")
            private String wait_time;
            @SerializedName("closing_status")
            private String closing_status;

            public class Multistop_array {
                @SerializedName("location")
                private String location;
                @SerializedName("latlong")
                Latlong LatlongObject;
                @SerializedName("is_end")
                private String is_end;
                @SerializedName("mode")
                private String mode;
                @SerializedName("record_id")
                private String record_id;


                // Getter Methods

                public String getLocation() {
                    return location;
                }

                public Latlong getLatlong() {
                    return LatlongObject;
                }

                public String getIs_end() {
                    return is_end;
                }

                public String getMode() {
                    return mode;
                }

                public String getRecord_id() {
                    return record_id;
                }

                // Setter Methods

                public void setLocation(String location) {
                    this.location = location;
                }

                public void setLatlong(Latlong latlongObject) {
                    this.LatlongObject = latlongObject;
                }

                public void setIs_end(String is_end) {
                    this.is_end = is_end;
                }

                public void setMode(String mode) {
                    this.mode = mode;
                }

                public void setRecord_id(String record_id) {
                    this.record_id = record_id;
                }

                public class Latlong {
                    @SerializedName("lon")
                    private String lon;
                    @SerializedName("lat")
                    private String lat;


                    // Getter Methods

                    public String getLon() {
                        return lon;
                    }

                    public String getLat() {
                        return lat;
                    }

                    // Setter Methods

                    public void setLon(String lon) {
                        this.lon = lon;
                    }

                    public void setLat(String lat) {
                        this.lat = lat;
                    }
                }
            }

            public class Next_drop_location {
                @SerializedName("location")
                private String location;
                @SerializedName("latlong")
                Latlong LatlongObject;


                // Getter Methods

                public String getLocation() {
                    return location;
                }

                public Latlong getLatlong() {
                    return LatlongObject;
                }

                // Setter Methods

                public void setLocation(String location) {
                    this.location = location;
                }

                public void setLatlong(Latlong latlongObject) {
                    this.LatlongObject = latlongObject;
                }

                public class Latlong {
                    @SerializedName("lat")
                    private String lat;
                    @SerializedName("lon")
                    private String lon;


                    // Getter Methods

                    public String getLat() {
                        return lat;
                    }

                    public String getLon() {
                        return lon;
                    }

                    // Setter Methods

                    public void setLat(String lat) {
                        this.lat = lat;
                    }

                    public void setLon(String lon) {
                        this.lon = lon;
                    }
                }
            }



            public class notification_details {
                @SerializedName("ios_fcm_key")
                private String ios_fcm_key;
                @SerializedName("user_fcm_token")
                String user_fcm_token;


                // Getter Methods

                public String getIos_fcm_key() {
                    return ios_fcm_key;
                }

                public String getUser_fcm_token() {
                    return user_fcm_token;
                }

                // Setter Methods

                public void setIos_fcm_key(String ios_fcm_key) {
                    this.ios_fcm_key = ios_fcm_key;
                }

                public void setUser_fcm_token(String user_fcm_token) {
                    this.user_fcm_token = user_fcm_token;
                }

                public class Latlong {
                    @SerializedName("lat")
                    private String lat;
                    @SerializedName("lon")
                    private String lon;


                    // Getter Methods

                    public String getLat() {
                        return lat;
                    }

                    public String getLon() {
                        return lon;
                    }

                    // Setter Methods

                    public void setLat(String lat) {
                        this.lat = lat;
                    }

                    public void setLon(String lon) {
                        this.lon = lon;
                    }
                }
            }


            public class Drop_location_array {
                @SerializedName("location")
                private String location;
                @SerializedName("latlong")
                Latlong LatlongObject;


                // Getter Methods 

                public String getLocation() {
                    return location;
                }

                public Latlong getLatlong() {
                    return LatlongObject;
                }

                // Setter Methods 

                public void setLocation(String location) {
                    this.location = location;
                }

                public void setLatlong(Latlong latlongObject) {
                    this.LatlongObject = latlongObject;
                }

                public class Latlong {
                    @SerializedName("lat")
                    private String lat;
                    @SerializedName("lon")
                    private String lon;


                    // Getter Methods 

                    public String getLat() {
                        return lat;
                    }

                    public String getLon() {
                        return lon;
                    }

                    // Setter Methods 

                    public void setLat(String lat) {
                        this.lat = lat;
                    }

                    public void setLon(String lon) {
                        this.lon = lon;
                    }
                }
            }
            
            // Getter Methods

            public String getUser_name() {
                return user_name;
            }

            public String getUser_email() {
                return user_email;
            }

            public String getUser_id() {
                return user_id;
            }

            public String getUser_gender() {
                return user_gender;
            }

            public String getPhone_number() {
                return phone_number;
            }

            public String getUser_image() {
                return user_image;
            }

            public String getUser_review() {
                return user_review;
            }

            public String getRide_id() {
                return ride_id;
            }

            public String getMember_since() {
                return member_since;
            }

            public String getLocation() {
                return location;
            }

            public String getEta_arrival() {
                return eta_arrival;
            }

            public String getPickup_location() {
                return pickup_location;
            }

            public String getPickup_lat() {
                return pickup_lat;
            }

            public String getPickup_lon() {
                return pickup_lon;
            }

            public String getPickup_time() {
                return pickup_time;
            }

            public String getContinue_trip() {
                return continue_trip;
            }

            public String getCurrency() {
                return currency;
            }

            public String getDrop_location() {
                return drop_location;
            }

            public String getDrop_loc() {
                return drop_loc;
            }

            public String getDrop_lat() {
                return drop_lat;
            }

            public String getDrop_lon() {
                return drop_lon;
            }

            public String getTitle_text() {
                return title_text;
            }

            public String getSubtitle_text() {
                return subtitle_text;
            }

            public String getEst_cost() {
                return est_cost;
            }

            public String getIs_stop() {
                return is_stop;
            }

            public String getMode() {
                return mode;
            }

            public String getIs_multistop_over() {
                return is_multistop_over;
            }

            public String getIs_return_over() {
                return is_return_over;
            }

            public String getReturn_mode() {
                return return_mode;
            }

            public String getRider_type() {
                return rider_type;
            }

            public String getCancelled_rides() {
                return cancelled_rides;
            }

            public String getCompleted_rides() {
                return completed_rides;
            }

            public String getPayment_type() {
                return payment_type;
            }

            public String getComp_status() {
                return comp_status;
            }

            public String getComp_stage() {
                return comp_stage;
            }

            public String getPayment_icon() {
                return payment_icon;
            }

            public String getWait_time() {
                return wait_time;
            }

            public String getClosing_status() {
                return closing_status;
            }

            // Setter Methods

            public void setUser_name(String user_name) {
                this.user_name = user_name;
            }

            public void setUser_email(String user_email) {
                this.user_email = user_email;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public void setUser_gender(String user_gender) {
                this.user_gender = user_gender;
            }

            public void setPhone_number(String phone_number) {
                this.phone_number = phone_number;
            }

            public void setUser_image(String user_image) {
                this.user_image = user_image;
            }

            public void setUser_review(String user_review) {
                this.user_review = user_review;
            }

            public void setRide_id(String ride_id) {
                this.ride_id = ride_id;
            }

            public void setMember_since(String member_since) {
                this.member_since = member_since;
            }

            public void setLocation(String location) {
                this.location = location;
            }

            public void setEta_arrival(String eta_arrival) {
                this.eta_arrival = eta_arrival;
            }

            public void setPickup_location(String pickup_location) {
                this.pickup_location = pickup_location;
            }

            public void setPickup_lat(String pickup_lat) {
                this.pickup_lat = pickup_lat;
            }

            public void setPickup_lon(String pickup_lon) {
                this.pickup_lon = pickup_lon;
            }

            public void setPickup_time(String pickup_time) {
                this.pickup_time = pickup_time;
            }

            public void setContinue_trip(String continue_trip) {
                this.continue_trip = continue_trip;
            }

            public void setCurrency(String currency) {
                this.currency = currency;
            }

            public void setDrop_location(String drop_location) {
                this.drop_location = drop_location;
            }

            public void setDrop_loc(String drop_loc) {
                this.drop_loc = drop_loc;
            }

            public void setDrop_lat(String drop_lat) {
                this.drop_lat = drop_lat;
            }

            public void setDrop_lon(String drop_lon) {
                this.drop_lon = drop_lon;
            }

            public void setTitle_text(String title_text) {
                this.title_text = title_text;
            }

            public void setSubtitle_text(String subtitle_text) {
                this.subtitle_text = subtitle_text;
            }

            public void setEst_cost(String est_cost) {
                this.est_cost = est_cost;
            }

            public void setIs_stop(String is_stop) {
                this.is_stop = is_stop;
            }

            public void setMode(String mode) {
                this.mode = mode;
            }

            public void setIs_multistop_over(String is_multistop_over) {
                this.is_multistop_over = is_multistop_over;
            }

            public void setIs_return_over(String is_return_over) {
                this.is_return_over = is_return_over;
            }

            public void setReturn_mode(String return_mode) {
                this.return_mode = return_mode;
            }

            public void setRider_type(String rider_type) {
                this.rider_type = rider_type;
            }

            public void setCancelled_rides(String cancelled_rides) {
                this.cancelled_rides = cancelled_rides;
            }

            public void setCompleted_rides(String completed_rides) {
                this.completed_rides = completed_rides;
            }

            public void setPayment_type(String payment_type) {
                this.payment_type = payment_type;
            }

            public void setComp_status(String comp_status) {
                this.comp_status = comp_status;
            }

            public void setComp_stage(String comp_stage) {
                this.comp_stage = comp_stage;
            }

            public void setPayment_icon(String payment_icon) {
                this.payment_icon = payment_icon;
            }

            public void setWait_time(String wait_time) {
                this.wait_time = wait_time;
            }

            public void setClosing_status(String closing_status) {
                this.closing_status = closing_status;
            }
        }
    }
}


