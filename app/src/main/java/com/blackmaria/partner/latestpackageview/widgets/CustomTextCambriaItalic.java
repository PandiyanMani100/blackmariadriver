package com.blackmaria.partner.latestpackageview.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by user144 on 8/23/2017.
 */

public class CustomTextCambriaItalic extends TextView {

    public CustomTextCambriaItalic(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomTextCambriaItalic(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomTextCambriaItalic(Context context) {
        super(context);
        init();
    }


    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Cambria-Italic.ttf");
        setTypeface(tf);
    }

}
