package com.blackmaria.partner.latestpackageview.widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by user127 on 24-10-2017.
 */


@SuppressLint("AppCompatCustomView")
public class TextREBRegular extends TextView {

    public TextREBRegular(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public TextREBRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextREBRegular(Context context) {
        super(context);
        init();
    }


    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/rockeb.ttf");
        setTypeface(tf);
    }
}

