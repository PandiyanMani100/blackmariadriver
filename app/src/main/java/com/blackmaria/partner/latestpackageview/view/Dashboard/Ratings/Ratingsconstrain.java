package com.blackmaria.partner.latestpackageview.view.Dashboard.Ratings;


import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import android.os.Handler;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityRatingsconstrainBinding;
import com.blackmaria.partner.latestpackageview.Factory.Ratings.RatingsFactory;
import com.blackmaria.partner.latestpackageview.Model.continuetrippojo;
import com.blackmaria.partner.latestpackageview.Model.reviewspojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Dashboard_constrain;
import com.blackmaria.partner.latestpackageview.vieewmodel.ratings.RatingsViewModel;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

public class Ratingsconstrain extends AppCompatActivity {

    private ActivityRatingsconstrainBinding binding;
    private RatingsViewModel ratingsViewModel;
    private SessionManager sessionManager;
    private continuetrippojo object;
    private reviewspojo pojo;
    private AppUtils appUtils;

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(Ratingsconstrain.this, R.layout.activity_ratingsconstrain);
        ratingsViewModel = ViewModelProviders.of(this, new RatingsFactory(this)).get(RatingsViewModel.class);
        binding.setRatingsViewModel(ratingsViewModel);
        ratingsViewModel.setIds(binding);

        initView();
        continutripresponse();
        clicklistener();
    }

    private void clicklistener() {
        binding.ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent passIntent = new Intent(Ratingsconstrain.this, Dashboard_constrain.class);
                startActivity(passIntent);
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        binding.myRidesRatingSubmitLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isRatingEmpty = false;
                for (int i = 0; i < pojo.getReview_options().size(); i++) {
                    if (pojo.getReview_options().get(i).getRatingcount() != null) {
                        if (!pojo.getReview_options().get(i).getRatingcount().equalsIgnoreCase("")) {
                            float number = Float.parseFloat(pojo.getReview_options().get(i).getRatingcount());
                            if (number > 0.0) {
                                isRatingEmpty = true;
                            }
                        }
                    }else {
                        isRatingEmpty = false;
                    }
                }
                if (isRatingEmpty) {
                    HashMap<String, String> jsonParams = new HashMap<String, String>();
                    jsonParams.put("comments", binding.myRidesRatingCommentEdittext.getText().toString().trim());
                    jsonParams.put("ratingsFor", "rider");
                    jsonParams.put("ride_id", object.getResponse().getUser_profile().getRide_id());
                    for (int i = 0; i < pojo.getReview_options().size(); i++) {
                        jsonParams.put("ratings[" + i + "][option_id]", pojo.getReview_options().get(i).getOption_id());
                        jsonParams.put("ratings[" + i + "][option_title]", pojo.getReview_options().get(i).getOption_title());
                        jsonParams.put("ratings[" + i + "][rating]", pojo.getReview_options().get(i).getRatingcount());
                    }
                    ratingsViewModel.ratingssubmitapihit(jsonParams);
                } else {
                    appUtils.AlertError(Ratingsconstrain.this, getResources().getString(R.string.action_error), getResources().getString(R.string.rating_label_rate_everything));
                }
            }
        });

        ratingsViewModel.getRatingssubmitresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                        appUtils.AlertSuccess(Ratingsconstrain.this, getResources().getString(R.string.action_success), jsonObject.getString("response"));
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                startActivity(new Intent(Ratingsconstrain.this, Dashboard_constrain.class));
                                finish();
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            }
                        }, 1500);
                    } else {
                        appUtils.AlertError(Ratingsconstrain.this, getResources().getString(R.string.action_error), jsonObject.getString("response"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void initView() {
        sessionManager = SessionManager.getInstance(this);
        appUtils = AppUtils.getInstance(this);
    }

    private void continutripresponse() {
        try {
            JSONObject jsonObject = new JSONObject(sessionManager.getContinuetripjson());
            Type type = new TypeToken<continuetrippojo>() {
            }.getType();
            object = new GsonBuilder().create().fromJson(jsonObject.toString(), type);
            HashMap<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("optionsFor", "rider");
            jsonParams.put("ride_id", object.getResponse().getUser_profile().getRide_id());
            ratingsViewModel.reviewapihit(jsonParams);
            ratingsViewModel.getReviewresponse().observe(this, new Observer<String>() {
                @Override
                public void onChanged(@Nullable String response) {
                    try {
                        JSONObject jsonObject1 = new JSONObject(response);
                        Type type = new TypeToken<reviewspojo>() {
                        }.getType();
                        pojo = new GsonBuilder().create().fromJson(jsonObject1.toString(), type);
                        setText(pojo);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setText(final reviewspojo object) {
        AppUtils.setImageView(Ratingsconstrain.this, object.getUser_profile().getUser_image(), binding.ivUsericon);
        binding.ratingPageDriverProfilenameTextview.setText(object.getUser_profile().getUser_name());
        try {
            binding.linearviewrating.removeAllViews();
        } catch (NullPointerException e) {
        }
        for (int i = 0; i <= object.getReview_options().size() - 1; i++) {
            View view = getLayoutInflater().inflate(R.layout.myride_rating_single, null);
            TextView myride_rating_single_title = view.findViewById(R.id.myride_rating_single_title);
            RatingBar myride_rating_single_ratingbar = view.findViewById(R.id.myride_rating_single_ratingbar);
            final int finalI = i;
            myride_rating_single_ratingbar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                    object.getReview_options().get(finalI).setRatingcount(String.valueOf(rating));
                }
            });
            myride_rating_single_title.setText(object.getReview_options().get(i).getOption_title());
            binding.linearviewrating.addView(view);
        }
    }


}
