package com.blackmaria.partner.latestpackageview.view.sidemenu.Maintanence;


import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityMaintanencedashboardBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.Factory.Sidemenu.Maintanence.MaintanencedashboardFactory;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.vieewmodel.sidemenu.maintanence.MaintanencedashboardViewModel;
import com.blackmaria.partner.latestpackageview.widgets.PkDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class Maintanencedashboard extends AppCompatActivity {

    private ActivityMaintanencedashboardBinding binding;
    private MaintanencedashboardViewModel maintanencedashboardViewModel;
    private SessionManager sessionManager;
    private AppUtils appUtils;
    private String sDriverID = "", additional_driver_id = "";
    AccessLanguagefromlocaldb db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
        binding = DataBindingUtil.setContentView(Maintanencedashboard.this, R.layout.activity_maintanencedashboard);
        maintanencedashboardViewModel = ViewModelProviders.of(this, new MaintanencedashboardFactory(this)).get(MaintanencedashboardViewModel.class);
        binding.setMaintanencedashboardViewModel(maintanencedashboardViewModel);
        maintanencedashboardViewModel.setIds(binding);

        binding.txt.setText(db.getvalue("seconduseradded"));
        binding.usertype.setText(db.getvalue("owner"));
        binding.secondusertype.setText(db.getvalue("seconduser"));
        binding.removeseconduser.setText(db.getvalue("remove"));
        binding.secondusername.setText(db.getvalue("uploadphoto"));
        binding.changevehiclename.setText(db.getvalue("changevehicle"));
        binding.operatingprocedure.setText(db.getvalue("operatingprocedure"));
        binding.operatingsteps.setText(db.getvalue("operatingsteps"));
        binding.close.setText(db.getvalue("close_lable"));

        initView();
        clicklistener();
        setResponse();
    }

    private void setResponse() {
        maintanencedashboardViewModel.getRemoveseconduserresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String sStatus = jsonObject.getString("status");
                    if (sStatus.equalsIgnoreCase("1")) {
                        appUtils.AlertSuccess(Maintanencedashboard.this, getResources().getString(R.string.action_success), jsonObject.getString("response"));
                        startActivity(new Intent(Maintanencedashboard.this, Maintanenceopendashboard.class));
                        finish();
                    } else {
                        String Smsg = jsonObject.getString("response");
                        appUtils.AlertError(Maintanencedashboard.this, getResources().getString(R.string.action_error), Smsg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        maintanencedashboardViewModel.getAdditionaldriverresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String sStatus = jsonObject.getString("status");
                    if (sStatus.equalsIgnoreCase("1")) {
                        AppUtils.setImageView(Maintanencedashboard.this, jsonObject.getJSONObject("response").getString("image"), binding.seconduserimage);
                        binding.secondusername.setText(jsonObject.getJSONObject("response").getString("driver_name"));
                    } else {
                        String Smsg = jsonObject.getString("response");
                        appUtils.AlertError(Maintanencedashboard.this, getResources().getString(R.string.action_error), Smsg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void clicklistener() {
        binding.removeseconduser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final PkDialog mDialog = new PkDialog(Maintanencedashboard.this);
                mDialog.setDialogTitle(getResources().getString(R.string.info_lable));
                mDialog.setDialogMessage(getResources().getString(R.string.doyouwanttoremovedriver));
                mDialog.setPositiveButton(getResources().getString(R.string.action_ok), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.dismiss();
                        HashMap<String, String> jsonParams = new HashMap<String, String>();
                        jsonParams.put("driver_id", sDriverID);
                        maintanencedashboardViewModel.removeuser(Iconstant.removedriver, jsonParams);
                    }
                });
                mDialog.show();
            }
        });
        binding.changevehicleimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Maintanencedashboard.this, EditvehicledetailsConstrain.class));
            }
        });
    }

    private void initView() {
        appUtils = AppUtils.getInstance(this);
        sessionManager = SessionManager.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        String name = user.get(SessionManager.KEY_DRIVER_NAME);
        String image = user.get(SessionManager.KEY_DRIVER_IMAGE);
        binding.username.setText(name);
        AppUtils.setImageView(Maintanencedashboard.this, image, binding.userimage);

        if (getIntent().hasExtra("driverid")) {
            additional_driver_id = getIntent().getStringExtra("driverid");
        }

        if (getIntent().hasExtra("from")) {
            binding.imgback.setVisibility(View.VISIBLE);
            binding.close.setVisibility(View.INVISIBLE);
            binding.tick.setVisibility(View.GONE);
            binding.txt.setVisibility(View.GONE);
        }

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        jsonParams.put("additional_driver_id", additional_driver_id);
        jsonParams.put("mode", "get");
        maintanencedashboardViewModel.getadditionaldriverdetails(Iconstant.updateanotherdriver, jsonParams);
    }
}
