package com.blackmaria.partner.latestpackageview.Factory.Dashboard.Wallet.Withdraw.Paypal;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet.withdraw.paypal.PaypalamountconfirmViewModel;


public class WithdrawamountenterFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public WithdrawamountenterFactory(Activity context) {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new PaypalamountconfirmViewModel(context);
    }
}
