package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class profilepojo implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }

    public class Response {
        @SerializedName("driver_profile")
        Driver_profile Driver_profileObject;

        public Bank_details getBank_detailsobj() {
            return bank_detailsobj;
        }

        public void setBank_detailsobj(Bank_details bank_detailsobj) {
            this.bank_detailsobj = bank_detailsobj;
        }

        @SerializedName("bank_details")
        Bank_details bank_detailsobj;

        public ArrayList<Profile> getProfile() {
            return profile;
        }

        public void setProfile(ArrayList<Profile> profile) {
            this.profile = profile;
        }

        public ArrayList<Driver_info> getDriver_info() {
            return driver_info;
        }

        public void setDriver_info(ArrayList<Driver_info> driver_info) {
            this.driver_info = driver_info;
        }

        @SerializedName("profile")
        ArrayList<Profile> profile = new ArrayList<Profile>();
        @SerializedName("driver_info")
        ArrayList<Driver_info> driver_info = new ArrayList<Driver_info>();

        public ArrayList<Emergency_contact_list> getEmergency_contact_list() {
            return emergency_contact_list;
        }

        public void setEmergency_contact_list(ArrayList<Emergency_contact_list> emergency_contact_list) {
            this.emergency_contact_list = emergency_contact_list;
        }

        ArrayList<Emergency_contact_list> emergency_contact_list = new ArrayList<Emergency_contact_list>();
        @SerializedName("emergency_contact")
        Emergency_contact Emergency_contactObject;
        @SerializedName("country_code")
        private String country_code;


        public class Profile {
            @SerializedName("title")
            private String title;
            @SerializedName("value")
            private String value;


            // Getter Methods

            public String getTitle() {
                return title;
            }

            public String getValue() {
                return value;
            }

            // Setter Methods

            public void setTitle(String title) {
                this.title = title;
            }

            public void setValue(String value) {
                this.value = value;
            }
        }

        public class Driver_info {
            @SerializedName("title")
            private String title;
            @SerializedName("value")
            private String value;


            // Getter Methods

            public String getTitle() {
                return title;
            }

            public String getValue() {
                return value;
            }

            // Setter Methods

            public void setTitle(String title) {
                this.title = title;
            }

            public void setValue(String value) {
                this.value = value;
            }
        }

        // Getter Methods
        public Driver_profile getDriver_profile() {
            return Driver_profileObject;
        }

        public Emergency_contact getEmergency_contact() {
            return Emergency_contactObject;
        }

        public String getCountry_code() {
            return country_code;
        }


        // Setter Methods

        public void setDriver_profile(Driver_profile driver_profileObject) {
            this.Driver_profileObject = driver_profileObject;
        }

        public void setEmergency_contact(Emergency_contact emergency_contactObject) {
            this.Emergency_contactObject = emergency_contactObject;
        }

        public void setCountry_code(String country_code) {
            this.country_code = country_code;
        }

        public class Emergency_contact_list{
            @SerializedName("title")
            private String title;
            @SerializedName("value")
            private String value;


            // Getter Methods

            public String getTitle() {
                return title;
            }

            public String getValue() {
                return value;
            }

            // Setter Methods

            public void setTitle(String title) {
                this.title = title;
            }

            public void setValue(String value) {
                this.value = value;
            }
        }

        public class Bank_details{
            @SerializedName("acc_holder_name")
            private String acc_holder_name;
            @SerializedName("acc_holder_address")
            private String acc_holder_address;
            @SerializedName("acc_number")
            private String acc_number;
            @SerializedName("bank_name")
            private String bank_name;
            @SerializedName("branch_name")
            private String branch_name;
            @SerializedName("branch_address")
            private String branch_address;
            @SerializedName("swift_code")
            private String swift_code;
            @SerializedName("routing_number")
            private String routing_number;


            // Getter Methods

            public String getAcc_holder_name() {
                return acc_holder_name;
            }

            public String getAcc_holder_address() {
                return acc_holder_address;
            }

            public String getAcc_number() {
                return acc_number;
            }

            public String getBank_name() {
                return bank_name;
            }

            public String getBranch_name() {
                return branch_name;
            }

            public String getBranch_address() {
                return branch_address;
            }

            public String getSwift_code() {
                return swift_code;
            }

            public String getRouting_number() {
                return routing_number;
            }

            // Setter Methods

            public void setAcc_holder_name(String acc_holder_name) {
                this.acc_holder_name = acc_holder_name;
            }

            public void setAcc_holder_address(String acc_holder_address) {
                this.acc_holder_address = acc_holder_address;
            }

            public void setAcc_number(String acc_number) {
                this.acc_number = acc_number;
            }

            public void setBank_name(String bank_name) {
                this.bank_name = bank_name;
            }

            public void setBranch_name(String branch_name) {
                this.branch_name = branch_name;
            }

            public void setBranch_address(String branch_address) {
                this.branch_address = branch_address;
            }

            public void setSwift_code(String swift_code) {
                this.swift_code = swift_code;
            }

            public void setRouting_number(String routing_number) {
                this.routing_number = routing_number;
            }
        }

        public class Driver_profile {
            @SerializedName("driver_id")
            private String driver_id;
            @SerializedName("driver_name")
            private String driver_name;
            @SerializedName("driver_image")
            private String driver_image;
            @SerializedName("vehicle_image")
            private String vehicle_image;
            @SerializedName("driver_review")
            private String driver_review;
            @SerializedName("country_code")
            private String country_code;
            @SerializedName("mobile_number")
            private String mobile_number;
            @SerializedName("email")
            private String email;
            @SerializedName("pass_code")
            private String pass_code;


            // Getter Methods

            public String getDriver_id() {
                return driver_id;
            }

            public String getDriver_name() {
                return driver_name;
            }

            public String getDriver_image() {
                return driver_image;
            }

            public String getVehicle_image() {
                return vehicle_image;
            }

            public String getDriver_review() {
                return driver_review;
            }

            public String getCountry_code() {
                return country_code;
            }

            public String getMobile_number() {
                return mobile_number;
            }

            public String getEmail() {
                return email;
            }

            public String getPass_code() {
                return pass_code;
            }

            // Setter Methods

            public void setDriver_id(String driver_id) {
                this.driver_id = driver_id;
            }

            public void setDriver_name(String driver_name) {
                this.driver_name = driver_name;
            }

            public void setDriver_image(String driver_image) {
                this.driver_image = driver_image;
            }

            public void setVehicle_image(String vehicle_image) {
                this.vehicle_image = vehicle_image;
            }

            public void setDriver_review(String driver_review) {
                this.driver_review = driver_review;
            }

            public void setCountry_code(String country_code) {
                this.country_code = country_code;
            }

            public void setMobile_number(String mobile_number) {
                this.mobile_number = mobile_number;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public void setPass_code(String pass_code) {
                this.pass_code = pass_code;
            }
        }


        public class Emergency_contact {
            @SerializedName("name")
            private String name;
            @SerializedName("email")
            private String email;
            @SerializedName("mobile_code")
            private String mobile_code;
            @SerializedName("mobile")
            private String mobile;
            @SerializedName("relationship")
            private String relationship;
            @SerializedName("home_address")
            private String home_address;
            @SerializedName("home_city")
            private String home_city;
            @SerializedName("home_country")
            private String home_country;


            // Getter Methods

            public String getName() {
                return name;
            }

            public String getEmail() {
                return email;
            }

            public String getMobile_code() {
                return mobile_code;
            }

            public String getMobile() {
                return mobile;
            }

            public String getRelationship() {
                return relationship;
            }

            public String getHome_address() {
                return home_address;
            }

            public String getHome_city() {
                return home_city;
            }

            public String getHome_country() {
                return home_country;
            }

            // Setter Methods

            public void setName(String name) {
                this.name = name;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public void setMobile_code(String mobile_code) {
                this.mobile_code = mobile_code;
            }

            public void setMobile(String mobile) {
                this.mobile = mobile;
            }

            public void setRelationship(String relationship) {
                this.relationship = relationship;
            }

            public void setHome_address(String home_address) {
                this.home_address = home_address;
            }

            public void setHome_city(String home_city) {
                this.home_city = home_city;
            }

            public void setHome_country(String home_country) {
                this.home_country = home_country;
            }

        }
    }

}






