package com.blackmaria.partner.latestpackageview.view.Dashboard.Mytrips;


import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.graphics.Color;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityMytripshomepageConstrainBinding;
import com.blackmaria.partner.latestpackageview.ApiRequest.MytripsdbApiIntentService;
import com.blackmaria.partner.latestpackageview.Database.RidesDB;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Mytrips.MytripsFactory;
import com.blackmaria.partner.latestpackageview.Model.mytripspojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.mytrips.MytripsViewModel;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Column;
import lecho.lib.hellocharts.model.ColumnChartData;
import lecho.lib.hellocharts.model.SubcolumnValue;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.util.ChartUtils;

public class MytripshomepageConstrain extends AppCompatActivity implements View.OnClickListener {

    public ActivityMytripshomepageConstrainBinding binding;
    public MytripsViewModel mytripsViewModel;
    private SessionManager sessionManager;
    private ColumnChartData data;
    private RidesDB ridesDBobj;
    private String driver_id="";
    AccessLanguagefromlocaldb db;

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
        binding = DataBindingUtil.setContentView(MytripshomepageConstrain.this, R.layout.activity_mytripshomepage_constrain);
        mytripsViewModel = ViewModelProviders.of(this, new MytripsFactory(this)).get(MytripsViewModel.class);
        binding.setMytripsViewModel(mytripsViewModel);
        mytripsViewModel.setIds(binding);

        initView();
        clicklistener();

        binding.mytrips.setText(db.getvalue("my_trips"));
        binding.tday.setText(db.getvalue("today"));
        binding.thisweek.setText(db.getvalue("this_week"));
        binding.thismon.setText(db.getvalue("this_month"));
        binding.lasttriplabel.setText(db.getvalue("highest_trip"));

        binding.tvTotalTrip.setText(db.getvalue("total_trips"));
        binding.tvCancelTrip.setText(db.getvalue("cancel_trip"));
        binding.tvBiddingTrip.setText(db.getvalue("bidding_trip"));
        binding.tvSosTrip.setText(db.getvalue("sos_trip"));
        binding.tvCancelTrip.setText(db.getvalue("complaint"));
        binding.weeklyperformance.setText(db.getvalue("weeekly_performance"));
        binding.toptripweek.setText(db.getvalue("top_trip_this_week"));
        binding.nochart.setText(db.getvalue("no_chart_data"));

        ridesDBobj = new RidesDB(MytripshomepageConstrain.this);
        String mytripfromdb = ridesDBobj.getmytripjson(driver_id);
        if(mytripfromdb.equals("0")){
            mytripsViewModel.hittriphomepage();
        }else{
            Type type = new TypeToken<mytripspojo>() {
            }.getType();
            mytripspojo object = new GsonBuilder().create().fromJson(mytripfromdb.toString(), type);
            setdashboard(object);
        }

    }

    private void initView() {
        sessionManager = SessionManager.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        driver_id = user.get(SessionManager.KEY_DRIVERID);
        AppUtils.setImageView(this, sessionManager.getDriverImageUpdate(), binding.myTriptriptopTripprofileimageView);
        int year = Calendar.getInstance().get(Calendar.YEAR);

        binding.hightlights.setText(year + " Highlights");
        mytripsViewModel.getdashboardresponse.observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {

                    ridesDBobj.insertmytrips(driver_id, response);
                    //save local db
                    Type type = new TypeToken<mytripspojo>() {
                    }.getType();
                    mytripspojo object = new GsonBuilder().create().fromJson(response.toString(), type);
                    setdashboard(object);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setdashboard(mytripspojo object) {
        if(object.getStatus() == null)
        {
            return;
        }
        binding.myTriptotalTriptodaytextView.setText(object.getResponse().getToday().getRides_count());
        binding.myTriptotalTriptodaykmtextView.setText(object.getResponse().getToday().getRides_distance());
        binding.myTriptotalTripthisWeektextView.setText(object.getResponse().getWeek().getRides_count());
        binding.myTriptotalTripthisWeekkmtextView.setText(object.getResponse().getWeek().getRides_distance());
        binding.myTriptotalTripthisMonthtextView.setText(object.getResponse().getMonth().getRides_count());
        binding.myTriptotalTripthisMonthkmtextView.setText(object.getResponse().getMonth().getRides_distance());

        binding.tvTotalTripValue.setText(object.getResponse().getYear_highlight().getCompleted());
        binding.myTripTodayHighlightCancelTripTextView.setText(object.getResponse().getYear_highlight().getCancel_trip());
        binding.myTripTodayHighlightSosTripTextView.setText(object.getResponse().getYear_highlight().getSos_trip());
        binding.myTripTodayHighlightComplaintTextView.setText(object.getResponse().getYear_highlight().getComplaint());
//        binding.tvTotalBiddingValue.setText(object.getResponse().getYear_highlight().get());
        AppUtils.setImageView(this, object.getResponse().getTop_trip().getDriver_image(), binding.toptripimage);
        binding.toptripusername.setText(object.getResponse().getTop_trip().getDriver_name());
        binding.toptripplace.setText(object.getResponse().getTop_trip().getCity_location());

        AppUtils.setImageviewwithoutcrop(this, sessionManager.getcategoryicon(), binding.lastdrivercarimage);
        if(object.getResponse().getHight_trip().getTrips() == null){
            binding.tripscount.setText("0" + " Trips");
        }else{
            binding.tripscount.setText(object.getResponse().getHight_trip().getTrips() + " Trips");
        }

        binding.tripdate.setText(object.getResponse().getHight_trip().getMonth());

        if (object.getResponse().getChart_array().size() > 0) {
            binding.nochart.setVisibility(View.GONE);
            binding.tripstxt.setVisibility(View.VISIBLE);
            binding.myTripweeklyPerformancechart.setVisibility(View.VISIBLE);
            setBarchart(object);
        } else {
            binding.nochart.setVisibility(View.VISIBLE);
            binding.tripstxt.setVisibility(View.GONE);
            binding.myTripweeklyPerformancechart.setVisibility(View.GONE);
        }
    }

    private void setBarchart(mytripspojo object) {
        List<Column> columns = new ArrayList<Column>();
        List<SubcolumnValue> values;
        int numSubcolumns = 1;
        for (int i = 0; i < object.getResponse().getChart_array().size(); i++) {
            values = new ArrayList<SubcolumnValue>();
            for (int j = 0; j < numSubcolumns; j++) {
                values.add(new SubcolumnValue(Float.parseFloat(String.valueOf(object.getResponse().getChart_array().get(i).getRidecount())), ChartUtils.pickColor()));
            }
            Column column = new Column(values);
            column.setHasLabels(false);
            columns.add(column);
        }

        data = new ColumnChartData(columns);

        Axis axisX;
        Axis axisy = new Axis();
        axisy.setTextColor(Color.WHITE);
        axisy.setTextSize(8);
        List<AxisValue> axisValues = new ArrayList<AxisValue>();
        for (int j = 0; j < object.getResponse().getChart_array().size(); ++j) {
            axisValues.add(new AxisValue(j).setLabel(object.getResponse().getChart_array().get(j).getDay()));
        }
        axisX = new Axis(axisValues);
        axisX.setTextColor(Color.WHITE);
        axisX.setTextSize(8);
        data.setAxisXBottom(axisX);
        data.setAxisYLeft(axisy);
        binding.myTripweeklyPerformancechart.setColumnChartData(data);

        final Viewport v = new Viewport(binding.myTripweeklyPerformancechart.getMaximumViewport());
        final Viewport maxViewport = new Viewport(v.left, v.top, v.right, v.bottom);
        binding.myTripweeklyPerformancechart.setMaximumViewport(maxViewport);
        final Viewport currentViewport = new Viewport(v.left, v.top, v.right, v.bottom);//smaller one
        binding.myTripweeklyPerformancechart.setCurrentViewport(currentViewport);
        binding.myTripweeklyPerformancechart.setViewportCalculationEnabled(false);
        //chart.setBackground(getContext().getDrawable(R.drawable.colorpick));
        binding.myTripweeklyPerformancechart.setScrollEnabled(false);
        binding.myTripweeklyPerformancechart.setZoomEnabled(false);


    }

    private void clicklistener() {
        binding.backbtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.backbtn:
                onBackPressed();
                break;
        }
    }
}
