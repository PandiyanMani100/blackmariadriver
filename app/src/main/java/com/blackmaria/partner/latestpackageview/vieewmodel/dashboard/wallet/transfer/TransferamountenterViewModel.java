package com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet.transfer;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import com.blackmaria.partner.R;
import com.blackmaria.partner.databinding.ActivityTransferamountenterConstrainBinding;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Dashboard_constrain;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

public class TransferamountenterViewModel extends ViewModel implements ApIServices.completelisner {
    private Activity context;
    private Dialog dialog;
    private int count = 0;
    private ActivityTransferamountenterConstrainBinding binding;
    private MutableLiveData<String> transferapiresponse = new MutableLiveData<>();
    private MutableLiveData<String> crossborderratesresponse = new MutableLiveData<>();

    public TransferamountenterViewModel(Activity context) {
        this.context = context;
    }

    public MutableLiveData<String> getTransferapiresponse() {
        return transferapiresponse;
    }

    public MutableLiveData<String> getCrossborderratesresponse() {
        return crossborderratesresponse;
    }

    @Override
    public void sucessresponse(String val) {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        if (count == 0) {
            transferapiresponse.postValue(val);
        }else if(count == 1){
            crossborderratesresponse.postValue(val);
        }

    }

    @Override
    public void errorreponse() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void jsonexception() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    public void setIds(ActivityTransferamountenterConstrainBinding binding) {
        this.binding = binding;
    }

    public void back() {
        context.onBackPressed();
    }

    public void transferamounttofriend(String url, HashMap<String, String> jsonParams) {
        count = 0;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        ApIServices apIServices = new ApIServices(context, TransferamountenterViewModel.this);
        apIServices.dooperation(url, jsonParams);
    }

    public void crossbordergetrates(String url, HashMap<String, String> jsonParams) {
        count = 1;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        ApIServices apIServices = new ApIServices(context, TransferamountenterViewModel.this);
        apIServices.dooperation(url, jsonParams);
    }

    public void cancel() {
        context.startActivity(new Intent(context, Dashboard_constrain.class));
        context.finish();
//        EventBus.getDefault().unregister(this);
        if (EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);}
    }
}
