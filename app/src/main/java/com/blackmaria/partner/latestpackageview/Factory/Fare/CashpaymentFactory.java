package com.blackmaria.partner.latestpackageview.Factory.Fare;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackmaria.partner.latestpackageview.vieewmodel.fare.CashpaymentViewModel;


public class CashpaymentFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public CashpaymentFactory(Activity context)
    {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass)
    {
        return (T) new CashpaymentViewModel(context);
    }
}
