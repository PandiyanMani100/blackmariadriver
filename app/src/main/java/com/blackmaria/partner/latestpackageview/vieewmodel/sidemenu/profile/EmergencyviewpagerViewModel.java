package com.blackmaria.partner.latestpackageview.vieewmodel.sidemenu.profile;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import com.blackmaria.partner.R;
import com.blackmaria.partner.databinding.EmergencyviewpagerlayoutBinding;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;

import java.util.HashMap;

public class EmergencyviewpagerViewModel extends ViewModel  implements ApIServices.completelisner {
    private Activity context;
    private EmergencyviewpagerlayoutBinding binding;
    private Dialog dialog;
    private int count = 0;
    private MutableLiveData<String> updateemergencydetailsresponse = new MutableLiveData<>();


    public EmergencyviewpagerViewModel(Activity context) {
        this.context = context;
    }

    public void setIds(EmergencyviewpagerlayoutBinding binding) {
        this.binding = binding;
    }

    public MutableLiveData<String> getUpdateemergencydetailsresponse() {
        return updateemergencydetailsresponse;
    }

    public void updateemergencydetails(String url, HashMap<String, String> jsonParams) {
        count = 0;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ApIServices apIServices = new ApIServices(context, EmergencyviewpagerViewModel.this);
        apIServices.dooperation(url, jsonParams);
    }

    @Override
    public void sucessresponse(String val) {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        if (count == 0) {
            updateemergencydetailsresponse.postValue(val);
        }
    }

    @Override
    public void errorreponse() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void jsonexception() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        AppUtils.toastprint(context, "Json Exception");
    }
}
