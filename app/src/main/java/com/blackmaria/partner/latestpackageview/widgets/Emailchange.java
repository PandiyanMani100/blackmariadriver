package com.blackmaria.partner.latestpackageview.widgets;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blackmaria.partner.R;


public class Emailchange {
    private Context mContext;
    private Dialog dialog;
    private View view;
    private TextView tv_saveplsu,tv_text,confirm,tv_tryagain;
    private EditText entercode;
    private ImageView close;
    private boolean isPositiveAvailable = false;
    private boolean isNegativeAvailable = false;
    private boolean isCloseAvailable = false;


    public Emailchange(Context context) {

        this.mContext = context;

        //--------Adjusting Dialog width-----
        DisplayMetrics metrics = mContext.getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 85% of the screen

        view = View.inflate(mContext, R.layout.emailchangedialog, null);

        dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(view);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setLayout(screenWidth, LinearLayout.LayoutParams.WRAP_CONTENT);
        tv_saveplsu = view.findViewById(R.id.tv_saveplsu);
        entercode = view.findViewById(R.id.entercode);
//        tv_text = view.findViewById(R.id.tv_text);
        confirm = view.findViewById(R.id.confirm);
//        tv_tryagain = view.findViewById(R.id.tv_tryagain);
        close = view.findViewById(R.id.close);
    }


    public void show() {

//        /*Enable or Disable positive Button*/
        if (isPositiveAvailable) {
            confirm.setVisibility(View.VISIBLE);
        } else {
            confirm.setVisibility(View.GONE);
        }
//        if (isNegativeAvailable) {
//            tv_tryagain.setVisibility(View.VISIBLE);
//        } else {
//            tv_tryagain.setVisibility(View.GONE);
//        }
//
        if (isCloseAvailable) {
            close.setVisibility(View.VISIBLE);
        } else {
            close.setVisibility(View.GONE);
        }

        dialog.show();
    }


    public void dismiss() {
        dialog.dismiss();
    }


    public void setDialogTitle(String title) {
        tv_saveplsu.setText(title);
    }

    public void setDialogUpper(boolean yesNo) {
        tv_text.setAllCaps(yesNo);
    }

    public void setDialogMessage(String message) {
        tv_text.setText(message);
    }

    public void setDialogMessageSize(int size) {
        tv_saveplsu.setTextSize(size);
    }


    public void setOnDismissListener(final DialogInterface.OnDismissListener listener) {
        dialog.setOnDismissListener(listener);
    }

    public void setCancelOnTouchOutside(boolean value) {
        dialog.setCanceledOnTouchOutside(value);
    }

    public void setCancelble(boolean value) {
        dialog.setCancelable(value);
    }

    /*Action Button for Dialog*/
    public void setPositiveButton(String text, final View.OnClickListener listener) {
        isPositiveAvailable = true;
        entercode.setText(text);
        confirm.setOnClickListener(listener);
    }

    public void setNegativeButton(String text, final View.OnClickListener listener) {
        isNegativeAvailable = true;
//        tv_tryagain.setOnClickListener(listener);
    }

    public void setCloseButton(final View.OnClickListener listener) {
        isCloseAvailable = true;
        close.setOnClickListener(listener);
    }
    public String getenteredotp(){
        return entercode.getText().toString();
    }
}
