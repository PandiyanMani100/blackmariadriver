package com.blackmaria.partner.latestpackageview.view.fare;


import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;

import androidx.databinding.DataBindingUtil;

import android.graphics.Typeface;
import android.os.Handler;

import androidx.annotation.Nullable;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityFareSummaryConstrainBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApiIntentService;
import com.blackmaria.partner.latestpackageview.ApiRequest.RidedbApiIntentService;
import com.blackmaria.partner.latestpackageview.Factory.Fare.FaresummaryFactory;
import com.blackmaria.partner.latestpackageview.Model.continuetrippojo;
import com.blackmaria.partner.latestpackageview.Model.farepojo;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Ratings.Ratingsconstrain;
import com.blackmaria.partner.latestpackageview.vieewmodel.fare.FaresummaryViewModel;
import com.bumptech.glide.Glide;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;

public class FareSummaryConstrain extends AppCompatActivity {

    private ActivityFareSummaryConstrainBinding binding;
    private FaresummaryViewModel faresummaryViewModel;
    private SessionManager sessionManager;
    private String sDriverID = "", rideid = "";
    private AppUtils appUtils;
    private continuetrippojo object;
    private farepojo fareobjec;
    private Typeface face;

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        System.gc();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(FareSummaryConstrain.this, R.layout.activity_fare_summary_constrain);
        faresummaryViewModel = ViewModelProviders.of(this, new FaresummaryFactory(this)).get(FaresummaryViewModel.class);
        binding.setFaresummaryViewModel(faresummaryViewModel);
        faresummaryViewModel.setIds(binding);

        initView();
        clicklistener();

        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        jsonParams.put("ride_id", rideid);

        Intent intents = new Intent(this, RidedbApiIntentService.class);
        intents.putExtra("driverid", sDriverID);
        intents.putExtra("isdrivercancel", "false");
        intents.putExtra("rideid", rideid);
        intents.putExtra("params", jsonParams);
        intents.putExtra("url", Iconstant.complete_trip_detail_url);
        startService(intents);
    }

    private void clicklistener() {
        binding.close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(FareSummaryConstrain.this, Ratingsconstrain.class);
                if (rideid.equalsIgnoreCase("")) {
                    intent1.putExtra("RideID", object.getResponse().getUser_profile().getRide_id());
                } else {
                    intent1.putExtra("RideID", rideid);
                }
                startActivity(intent1);
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        binding.saveplusenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.receivepayment.getText().toString().length() == 0) {
                    appUtils.AlertError(FareSummaryConstrain.this, getResources().getString(R.string.action_error), "Received amount should not be empty");
                } else {
                    int entermoney = Math.round(Float.parseFloat(binding.receivepayment.getText().toString().trim()));
                    int yourmoney = Math.round(Float.parseFloat(fareobjec.getResponse().getFare().getGrand_fare().trim()));
                    try {
                        if (yourmoney < entermoney) {
                            HashMap<String, String> jsonParams = new HashMap<String, String>();
                            jsonParams.put("receiver_id", fareobjec.getResponse().getFare().getUser_id());
                            jsonParams.put("received_amount", String.valueOf(entermoney));
                            jsonParams.put("total_amount", String.valueOf(yourmoney));
                            jsonParams.put("driver_id", sDriverID);
                            jsonParams.put("ride_id", rideid);
                            faresummaryViewModel.saveplusapihit(jsonParams);
                            binding.saveplusdone.setVisibility(View.VISIBLE);
                            Glide.with(FareSummaryConstrain.this).asGif().load(R.raw.giloadingsaveplus).into(binding.saveplusdone);
                            saveplustransferresponse();
                        } else {
                            appUtils.AlertError(FareSummaryConstrain.this, getResources().getString(R.string.action_error), "Received amount shoud be greater than the price amount");
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void saveplustransferresponse() {
        faresummaryViewModel.getSaveplusresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                        appUtils.AlertSuccess(FareSummaryConstrain.this, getResources().getString(R.string.action_success), jsonObject.getJSONObject("response").getString("message"));
                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Do something after 100ms
                                Intent intent1 = new Intent(FareSummaryConstrain.this, Ratingsconstrain.class);
                                if (rideid.equalsIgnoreCase("")) {
                                    intent1.putExtra("RideID", object.getResponse().getUser_profile().getRide_id());
                                } else {
                                    intent1.putExtra("RideID", rideid);
                                }
                                startActivity(intent1);
                                finish();
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            }
                        }, 1500);
                    } else {
                        appUtils.AlertError(FareSummaryConstrain.this, getResources().getString(R.string.action_success), jsonObject.getString("response"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void initView() {
        sessionManager = SessionManager.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        appUtils = AppUtils.getInstance(this);
        face = Typeface.createFromAsset(getAssets(), "fonts/Vladimir.ttf");
        binding.tripfinishedlabel.setTypeface(face, Typeface.BOLD);

        try {
            if (getIntent().hasExtra("rideid")) {
                rideid = getIntent().getStringExtra("rideid");
                HashMap<String, String> jsonParams = new HashMap<String, String>();
                jsonParams.put("ride_id", getIntent().getStringExtra("rideid"));
                jsonParams.put("driver_id", sDriverID);
                faresummaryViewModel.farebreakupapihit(jsonParams);
            } else {
                JSONObject jsonObject = new JSONObject(sessionManager.getContinuetripjson());
                Type type = new TypeToken<continuetrippojo>() {
                }.getType();
                object = new GsonBuilder().create().fromJson(jsonObject.toString(), type);
                HashMap<String, String> jsonParams = new HashMap<String, String>();
                jsonParams.put("ride_id", object.getResponse().getUser_profile().getRide_id());
                rideid = object.getResponse().getUser_profile().getRide_id();
                jsonParams.put("driver_id", sDriverID);
                faresummaryViewModel.farebreakupapihit(jsonParams);
            }

            setResponse();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void setResponse() {
        faresummaryViewModel.getFareresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Type type = new TypeToken<farepojo>() {
                    }.getType();
                    fareobjec = new GsonBuilder().create().fromJson(jsonObject.toString(), type);
                    setTextview(fareobjec);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setTextview(farepojo pojo) {
        binding.date.setText("Date " + pojo.getResponse().getFare().getRide_completed_date() + "\n" + pojo.getResponse().getFare().getRide_completed_tim());
        try {
            binding.faredetails.removeAllViews();
        } catch (NullPointerException e) {
        }
        for (int i = 0; i <= pojo.getResponse().getFare().getTrip_receipt().size() - 1; i++) {
            View view = getLayoutInflater().inflate(R.layout.farecustomlayout, null);
            TextView day = view.findViewById(R.id.date);
            TextView trips = view.findViewById(R.id.value);
            day.setText(pojo.getResponse().getFare().getTrip_receipt().get(i).getTitle());
            if (pojo.getResponse().getFare().getTrip_receipt().get(i).getCurrency_status().equalsIgnoreCase("1")) {
                trips.setText(pojo.getResponse().getCurrency() + " " + pojo.getResponse().getFare().getTrip_receipt().get(i).getValue());
            } else {
                trips.setText(pojo.getResponse().getFare().getTrip_receipt().get(i).getValue());
            }
            binding.faredetails.addView(view);
        }
        binding.bookingno.setText("Booking No :" + object.getResponse().getUser_profile().getRide_id());
        if (rideid.equalsIgnoreCase("")) {
            binding.bookingno.setText("Booking No :" + object.getResponse().getUser_profile().getRide_id());
        } else {
            binding.bookingno.setText("Booking No :" + rideid);
        }
        binding.fareamount.setText(pojo.getResponse().getCurrency() + " " + pojo.getResponse().getFare().getGrand_fare());
        if (pojo.getResponse().getFare().getPayment_type().toLowerCase().equalsIgnoreCase("cash")) {
            binding.receivepayment.setVisibility(View.VISIBLE);
            binding.saveplusenter.setVisibility(View.VISIBLE);
            binding.paidbytype.setVisibility(View.GONE);
        } else {
            binding.receivepayment.setVisibility(View.GONE);
            binding.saveplusenter.setVisibility(View.GONE);
            binding.paidbytype.setVisibility(View.VISIBLE);
            binding.paidbytype.setText("paid by " + pojo.getResponse().getFare().getPayment_type());
        }
    }
}
