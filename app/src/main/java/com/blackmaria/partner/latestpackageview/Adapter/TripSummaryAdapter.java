package com.blackmaria.partner.latestpackageview.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.blackmaria.partner.Pojo.TripSummaryPojo;
import com.blackmaria.partner.R;
import com.blackmaria.partner.latestpackageview.Model.tripsummarypojo;

import java.util.ArrayList;


public class TripSummaryAdapter extends BaseAdapter {

    private String currencyCode = "";
    private int textColor = Color.WHITE;
    private ArrayList<tripsummarypojo.Response.Details.FareArrL> data;
    private LayoutInflater mInflater;
    private Context context;

    public TripSummaryAdapter(Context c, String currencyCode, int textColor, ArrayList<tripsummarypojo.Response.Details.FareArrL> d) {
        context = c;
        mInflater = LayoutInflater.from(context);
        this.currencyCode = currencyCode;
        this.textColor = textColor;
        data = d;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public tripsummarypojo.Response.Details.FareArrL getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }


    public class ViewHolder {
        private TextView Tv_title, Tv_value;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.row_trip_summary_single, parent, false);
            holder = new ViewHolder();
            holder.Tv_title = (TextView) view.findViewById(R.id.txt_title);
            holder.Tv_value = (TextView) view.findViewById(R.id.txt_value);

            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder) view.getTag();
        }

        holder.Tv_title.setTextColor(textColor);
        holder.Tv_value.setTextColor(textColor);

        holder.Tv_title.setText(data.get(position).getTitle());

        if(data.get(position).getTitle().toLowerCase().contains("pick") || data.get(position).getTitle().toLowerCase().contains("drop")){
            holder.Tv_value.setAllCaps(false);
        }

        String currencyStatus = data.get(position).getCurrency_status();
        if (currencyStatus.equalsIgnoreCase("1")) {
            holder.Tv_value.setText(currencyCode +" "+ data.get(position).getValue());
        } else {
            holder.Tv_value.setText(data.get(position).getValue());
        }


        return view;
    }
}

