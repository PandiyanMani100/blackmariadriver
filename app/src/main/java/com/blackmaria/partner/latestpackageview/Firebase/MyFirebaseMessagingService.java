package com.blackmaria.partner.latestpackageview.Firebase;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.os.Build;

import android.util.Log;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.app.DriveAndEarnStatement;
import com.blackmaria.partner.app.FetchingDataPage;

import com.blackmaria.partner.app.IncentivesDashboard;
import com.blackmaria.partner.app.NewTripPage;
import com.blackmaria.partner.app.Reload_Transfer_PushNotification;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.view.Dashboard.OnlinepageConstrain;
import com.blackmaria.partner.latestpackageview.view.fare.FareSummaryConstrain;
import com.blackmaria.partner.latestpackageview.view.signup.FetchingDataPage_constrain;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    public Context mcontext;
    JSONObject extras;
    private String driverID = "", driverName = "", driverEmail = "", driverImage = "", driverRating = "",
            driverLat = "", driverLong = "", driverTime = "", rideID = "", driverMobile = "",
            driverCar_no = "", driverCar_model = "";

    private String key1 = "", key2 = "", key3 = "", message = "", action = "", key4 = "", key5 = "", key6 = "", key7 = "", key8 = "";
    private SessionManager session;
    public static boolean isTimerpage = false;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        mcontext = this;
        // Check if message contains a notification payload.
        if (remoteMessage.getData() != null) {
            try {
                Map<String, String> params = remoteMessage.getData();

                extras = new JSONObject(params);

                System.out.println("ssssss---->"+extras.toString());
                if (extras.has("key1")) {
                    key1 = extras.getString("key1").toString();
                }
                if (extras.has("key2")) {
                    key2 = extras.getString("key2").toString();
                }
                if (extras.has("key3")) {
                    key3 = extras.getString("key3").toString();
                }
                if (extras.has("key4")) {
                    key4 = extras.getString("key4").toString();
                }
                if (extras.has("key5")) {
                    key5 = extras.getString("key5").toString();
                }
                if (extras.has("key6")) {
                    key6 = extras.getString("key6").toString();
                }
                if (extras.has("key7")) {
                    key7 = extras.getString("key7").toString();
                }
                if (extras.has("key8")) {
                    key8 = extras.getString("key8").toString();
                }
                if (extras.has("action")) {
                    action = extras.getString("action").toString();
                }
                if (extras.has("message")) {
                    message = extras.getString("message").toString();
                }

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("message", message);
                jsonObject.put("action", action);
                jsonObject.put("key1", key1);
                jsonObject.put("key2", key2);
                jsonObject.put("key3", key3);
                jsonObject.put("key4", key4);
                jsonObject.put("key5", key5);
                jsonObject.put("key6", key6);
                jsonObject.put("key7", key7);
                jsonObject.put("key8", key8);

                if (Iconstant.ACTION_TAG_RIDE_REQUEST.equalsIgnoreCase(action) || Iconstant.ACTION_TAG_REFERRAL_CREDIT.equalsIgnoreCase(action) ||
                        action.equalsIgnoreCase(Iconstant.ACTION_TAG_WEEK_TRIP) || action.equalsIgnoreCase(Iconstant.ACTION_TAG_DAILY_TRIP) ||
                        action.equalsIgnoreCase(Iconstant.ACTION_TAG_DAILY_MILE) || action.equalsIgnoreCase(Iconstant.ACTION_TAG_SCHEDULE_INCENTIVE) ||
                        action.equalsIgnoreCase(Iconstant.ACTION_TAG_WALLET_CREDIT) || action.equalsIgnoreCase(Iconstant.userEndTrip)) {
                    sendNotification(message.toString(), jsonObject.toString());
                } else {
                    sendNotification(message.toString());
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    @SuppressWarnings("deprecation")
    private void sendNotification(String msg) {
        try {
            if (Iconstant.ACTION_TAG_NEW_TRIP.equalsIgnoreCase(action)) {
                Intent intent = new Intent(mcontext, NewTripPage.class);
                intent.putExtra("NewTrip", extras.getString("message").toString());
                intent.putExtra("MessagePage", extras.getString("message").toString());
                intent.putExtra("Action", extras.getString("action").toString());
                intent.putExtra("Username", extras.getString("key1").toString());
                intent.putExtra("Mobilenumber", extras.getString("key3").toString());
                intent.putExtra("UserImage", extras.getString("key4").toString());
                intent.putExtra("RideId", extras.getString("key6").toString());
                intent.putExtra("UserPickuplocation", extras.getString("key7").toString());
                intent.putExtra("UserPickupTime", extras.getString("key10").toString());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mcontext.startActivity(intent);
            } else {
                Intent notificationIntent = null;
                notificationIntent = new Intent(mcontext, FetchingDataPage_constrain.class);
                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                PendingIntent contentIntent = PendingIntent.getActivity(mcontext, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
                Resources res = mcontext.getResources();
                Notification.Builder builder;


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    builder = new Notification.Builder(mcontext, "1");
                    builder.setContentIntent(contentIntent)
                            .setSmallIcon(R.drawable.app_icon)
                            .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.app_icon))
                            .setTicker(msg)
                            .setWhen(System.currentTimeMillis())
                            .setAutoCancel(true)
                            .setContentTitle(mcontext.getString(R.string.app_name))
                            .setLights(0xffff0000, 100, 2000)
                            .setPriority(Notification.DEFAULT_SOUND)
                            .setContentText(msg);
                } else {
                    builder = new Notification.Builder(mcontext);
                    builder.setContentIntent(contentIntent)
                            .setSmallIcon(R.drawable.app_icon)
                            .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.app_icon))
                            .setTicker(msg)
                            .setWhen(System.currentTimeMillis())
                            .setAutoCancel(true)
                            .setContentTitle(mcontext.getString(R.string.app_name))
                            .setLights(0xffff0000, 100, 2000)
                            .setPriority(Notification.DEFAULT_SOUND)
                            .setContentText(msg);
                }


                NotificationManager notificationmanager = (NotificationManager) mcontext.getSystemService(NOTIFICATION_SERVICE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    NotificationChannel channel = new NotificationChannel("1",
                            mcontext.getResources().getString(R.string.app_name),
                            NotificationManager.IMPORTANCE_DEFAULT);
                    notificationmanager.createNotificationChannel(channel);
                }
                notificationmanager.notify(5, builder.build());

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @SuppressWarnings("deprecation")
    private void sendNotification(String msg, String data) {
        try {
            Intent notificationIntent = null;

            session = new SessionManager(mcontext);
            if (session.isLoggedIn()) {

                if (action.equalsIgnoreCase(Iconstant.ACTION_TAG_REFERRAL_CREDIT)) {

                    notificationIntent = new Intent(mcontext, DriveAndEarnStatement.class);
                    notificationIntent.putExtra("fromPage", "GCMPushNotification");
                    notificationIntent.putExtra("data", data);
                    notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                } else if (action.equalsIgnoreCase(Iconstant.ACTION_WALLET_SUCCESS) || action.equalsIgnoreCase(Iconstant.ACTION_BANK_SUCCESS)) {

                    notificationIntent = new Intent(mcontext, Reload_Transfer_PushNotification.class);
                    if (action.equalsIgnoreCase(Iconstant.ACTION_BANK_SUCCESS)) {

                        notificationIntent.putExtra("Str_action", extras.getString("action").toString());
                        notificationIntent.putExtra("user_id", extras.getString("key1"));
                        notificationIntent.putExtra("Amount", extras.getString("key5"));
                        notificationIntent.putExtra("Currencycode", extras.getString("key6"));
                        notificationIntent.putExtra("paymenttype", extras.getString("key2"));
                        notificationIntent.putExtra("payment_via", extras.getString("key3"));
                        notificationIntent.putExtra("from_number", extras.getString("key4"));

                        notificationIntent.putExtra("from", extras.getString("key7"));
                        notificationIntent.putExtra("date", extras.getString("key8"));
                        notificationIntent.putExtra("time", extras.getString("key9"));
                        notificationIntent.putExtra("trans_id", extras.getString("key10"));
                    } else {
                        notificationIntent.putExtra("Str_action", extras.get("action").toString());
                        notificationIntent.putExtra("user_id", extras.getString("key1"));
                        notificationIntent.putExtra("Amount", extras.getString("key4"));
                        notificationIntent.putExtra("Currencycode", extras.getString("key5"));
                        notificationIntent.putExtra("payment_via", extras.getString("key2"));
                        notificationIntent.putExtra("from_number", extras.getString("key3"));
                        notificationIntent.putExtra("paymenttype", "");
                        notificationIntent.putExtra("from", extras.getString("key6"));
                        notificationIntent.putExtra("date", extras.getString("key7"));
                        notificationIntent.putExtra("time", extras.getString("key8"));
                        notificationIntent.putExtra("trans_id", extras.getString("key9"));
                    }
                    notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                } else if (action.equalsIgnoreCase(Iconstant.ACTION_TAG_WALLET_CREDIT)) {

                    notificationIntent = new Intent(mcontext, Reload_Transfer_PushNotification.class);
                    notificationIntent.putExtra("Str_action", extras.getString("action").toString());
                    notificationIntent.putExtra("user_id", extras.getString("key1"));
                    notificationIntent.putExtra("Amount", extras.getString("key2"));
                    notificationIntent.putExtra("Currencycode", extras.getString("key3"));
                    notificationIntent.putExtra("paymenttype", extras.getString("key4"));
                    notificationIntent.putExtra("payment_via", extras.getString("key5"));
                    notificationIntent.putExtra("from_number", extras.getString("key6"));
                    notificationIntent.putExtra("from", extras.getString("key7"));
                    notificationIntent.putExtra("date", extras.getString("key8"));
                    notificationIntent.putExtra("time", extras.getString("key9"));
                    notificationIntent.putExtra("trans_id", extras.getString("key10"));


                    notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                } else if (action.equalsIgnoreCase(Iconstant.ACTION_TAG_PAYMENT_PAID) || action.equalsIgnoreCase(Iconstant.userEndTrip)) {

                    notificationIntent = new Intent(mcontext, FareSummaryConstrain.class);
                    notificationIntent.putExtra("fromPage", "GCMPushNotification");
                    notificationIntent.putExtra("data", data);
                    notificationIntent.putExtra("rideid", key1);
                    notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                } else if (action.equalsIgnoreCase(Iconstant.ACTION_TAG_WEEK_TRIP) || action.equalsIgnoreCase(Iconstant.ACTION_TAG_DAILY_TRIP)
                        || action.equalsIgnoreCase(Iconstant.ACTION_TAG_DAILY_MILE) || action.equalsIgnoreCase(Iconstant.ACTION_TAG_SCHEDULE_INCENTIVE)) {

                    notificationIntent = new Intent(mcontext, IncentivesDashboard.class);
                    notificationIntent.putExtra("fromPage", "GCMPushNotification");
                    notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                } else {
                    HashMap<String, Integer> user = session.getRequestCount();
                    int req_count = user.get(SessionManager.KEY_COUNT);
                    if (req_count < 1) {
                        notificationIntent = new Intent(mcontext, OnlinepageConstrain.class);
                        notificationIntent.putExtra("EXTRA", data);
                        notificationIntent.putExtra("notification", "notification");
                        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        isTimerpage = true;
                    }
                }

            } else {
                notificationIntent = new Intent(mcontext, FetchingDataPage.class);
                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            }

            if (isTimerpage) {
                try {
                    mcontext.startActivity(notificationIntent);
                    isTimerpage = false;
                } catch (Exception e) {
                    Log.d("Message", e.getMessage());
                }

            } else {
                PendingIntent contentIntent = PendingIntent.getActivity(mcontext, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
                NotificationManager nm = (NotificationManager) mcontext.getSystemService(NOTIFICATION_SERVICE);

                Resources res = mcontext.getResources();
                Notification.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    builder = new Notification.Builder(mcontext, "5");
                    builder.setContentIntent(contentIntent)
                            .setSmallIcon(R.drawable.app_icon)
                            .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.app_icon))
                            .setTicker(msg)
                            .setWhen(System.currentTimeMillis())
                            .setAutoCancel(true)
                            .setContentTitle(mcontext.getResources().getString(R.string.app_name))
                            .setLights(0xffff0000, 100, 2000)
                            .setPriority(Notification.DEFAULT_SOUND)
                            .setContentText(msg);
                } else {
                    builder = new Notification.Builder(mcontext);
                    builder.setContentIntent(contentIntent)
                            .setSmallIcon(R.drawable.app_icon)
                            .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.app_icon))
                            .setTicker(msg)
                            .setWhen(System.currentTimeMillis())
                            .setAutoCancel(true)
                            .setContentTitle(mcontext.getResources().getString(R.string.app_name))
                            .setLights(0xffff0000, 100, 2000)
                            .setPriority(Notification.DEFAULT_SOUND)
                            .setContentText(msg);
                }


                NotificationManager notificationmanager = (NotificationManager) mcontext.getSystemService(NOTIFICATION_SERVICE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    NotificationChannel channel = new NotificationChannel("5",
                            mcontext.getResources().getString(R.string.app_name),
                            NotificationManager.IMPORTANCE_DEFAULT);
                    notificationmanager.createNotificationChannel(channel);
                }
                notificationmanager.notify(5, builder.build());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
