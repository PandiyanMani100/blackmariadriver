package com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Withdraw.Bank;


import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.databinding.DataBindingUtil;
import android.os.Bundle;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.databinding.ActivityWithdrawbanksuccessConstrainBinding;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Wallet.Withdraw.Bank.WithdrawbanksuccessFactory;
import com.blackmaria.partner.latestpackageview.Model.withdrawpaymentsendpojo;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet.withdraw.bank.WithdrawbanksuccessViewModel;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class WithdrawbanksuccessConstrain extends AppCompatActivity {

    private ActivityWithdrawbanksuccessConstrainBinding binding;
    private WithdrawbanksuccessViewModel withdrawbanksuccessViewModel;
    private withdrawpaymentsendpojo pojo;
    AccessLanguagefromlocaldb db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
        binding = DataBindingUtil.setContentView(WithdrawbanksuccessConstrain.this, R.layout.activity_withdrawbanksuccess_constrain);
        withdrawbanksuccessViewModel = ViewModelProviders.of(this, new WithdrawbanksuccessFactory(this)).get(WithdrawbanksuccessViewModel.class);
        binding.setWithdrawbanksuccessViewModel(withdrawbanksuccessViewModel);
        withdrawbanksuccessViewModel.setIds(binding);

        initView();

        binding.withdraw.setText(db.getvalue("withdraw_lable"));
        binding.success.setText(db.getvalue("success"));
        binding.cancel.setText(db.getvalue("close_lable"));
        binding.datemonthyear.setText(db.getvalue("fastwallet"));

    }

    private void initView() {
        if (getIntent().hasExtra("json")) {
            Type type = new TypeToken<withdrawpaymentsendpojo>() {
            }.getType();
            pojo = new GsonBuilder().create().fromJson(getIntent().getStringExtra("json").toString(), type);

            if (getIntent().hasExtra("paypal")) {
                setText(pojo, "paypal");
            } else if (getIntent().hasExtra("cash")) {
                setText(pojo, "cash");
            } else if (getIntent().hasExtra("bank")) {
                setText(pojo, "bank");
            }

        }
    }

    private void setText(withdrawpaymentsendpojo pojo, String paymenttype) {
        if (paymenttype.equalsIgnoreCase("bank")) {
            binding.amount.setText(pojo.getCurrency() + " " + pojo.getAmount());
            binding.usercontent.setText("WITDRAWN TO : " + pojo.getBank_name() + "\n" + "ACCOUNT NO : " + pojo.getAccount() + "\n\n" + "FROM : " + pojo.getFrom() + "\n" + "DATE : " + pojo.getDate() + "\n" + "TIME : " + pojo.getTime());
            binding.transactionumber.setText("TRANSACTION NUMBER : " + pojo.getRef_no());
            binding.contentcredit.setText("Your bank account will be credited" + "\n" + "according to bank schedule cut of time");
        } else if (paymenttype.equalsIgnoreCase("paypal")) {
            try {
                binding.amount.setText(pojo.getCurrency() + " " + pojo.getAmount());
                binding.usercontent.setText("FROM : " + pojo.getFrom() + "\n" + "DATE : " + pojo.getDate() + "\n" + "TIME : " + pojo.getTime() + "\n" + pojo.getCountry_code() + "" + pojo.getPhone_number() + "\n\n" + "PAYPAL ID" + "\n" + pojo.getPaypal_id());
                binding.transactionumber.setText("TRANSACTION NUMBER : " + pojo.getRef_no());
                binding.contentcredit.setText("Your paypal account will be credited" + "\n" + "according to Cloudmoney cut of time");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onBackPressed() {

    }
}
