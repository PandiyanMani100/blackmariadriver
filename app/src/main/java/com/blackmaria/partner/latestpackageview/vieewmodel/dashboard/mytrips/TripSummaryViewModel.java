package com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.mytrips;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import com.blackmaria.partner.R;
import com.blackmaria.partner.databinding.ActivityTripSummaryConstrainBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;

import java.util.HashMap;

public class TripSummaryViewModel extends ViewModel implements ApIServices.completelisner {
    private Activity context;
    private ActivityTripSummaryConstrainBinding binding;
    private Dialog dialog;
    private MutableLiveData<String> response = new MutableLiveData<>();

    public TripSummaryViewModel(Activity context) {
        this.context = context;
    }

    public void setIds(ActivityTripSummaryConstrainBinding binding) {
        this.binding = binding;
    }

    public MutableLiveData<String> getResponse() {
        return response;
    }

    public void tripinfoapihit(String rideID, String sDriverID) {
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();


        HashMap<String, String> jsonParams = new HashMap<String, String>();
        jsonParams.put("driver_id", sDriverID);
        jsonParams.put("ride_id", rideID);

        ApIServices apIServices = new ApIServices(context, TripSummaryViewModel.this);
        apIServices.dooperation(Iconstant.complete_trip_detail_url, jsonParams);
    }

    public void back(){
        context.onBackPressed();
    }

    @Override
    public void sucessresponse(String val) {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        response.postValue(val);
    }

    @Override
    public void errorreponse() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
    }

    @Override
    public void jsonexception() {
        dialog.dismiss();
        binding.apiload.setVisibility(View.INVISIBLE);
        AppUtils.toastprint(context, "Json Exception");
    }
}
