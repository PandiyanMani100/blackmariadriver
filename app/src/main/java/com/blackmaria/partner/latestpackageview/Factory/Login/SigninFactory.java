package com.blackmaria.partner.latestpackageview.Factory.Login;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackmaria.partner.latestpackageview.vieewmodel.login.SigninViewModel;


public class SigninFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public SigninFactory(Activity context)
    {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass)
    {
        return (T) new SigninViewModel(context);
    }
}
