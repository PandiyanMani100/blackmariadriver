package com.blackmaria.partner.latestpackageview.vieewmodel.sidemenu.maintanence;

import android.app.Activity;
import android.app.Dialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import com.blackmaria.partner.R;
import com.blackmaria.partner.databinding.ActivityMaintanencedashboardBinding;
import com.blackmaria.partner.latestpackageview.ApiRequest.ApIServices;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Dashboard_constrain;

import java.util.HashMap;

public class MaintanencedashboardViewModel extends ViewModel implements ApIServices.completelisner {

    private Activity context;
    private Dialog dialog;
    private ActivityMaintanencedashboardBinding binding;
    private MutableLiveData<String> removeseconduserresponse = new MutableLiveData<>();
    private MutableLiveData<String> additionaldriverresponse = new MutableLiveData<>();
    private int count = 0;

    public MaintanencedashboardViewModel(Activity context) {
        this.context = context;
    }

    public void setIds(ActivityMaintanencedashboardBinding binding) {
        this.binding = binding;
    }

    public MutableLiveData<String> getRemoveseconduserresponse() {
        return removeseconduserresponse;
    }

    public MutableLiveData<String> getAdditionaldriverresponse() {
        return additionaldriverresponse;
    }

    public void changevehicle() {

    }

    public void removeuser(String url, HashMap<String, String> jsonParams) {
        count = 0;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ApIServices apIServices = new ApIServices(context, MaintanencedashboardViewModel.this);
        apIServices.dooperation(url, jsonParams);
    }


    public void getadditionaldriverdetails(String url, HashMap<String, String> jsonParams) {
        count = 1;
        binding.apiload.setVisibility(View.VISIBLE);
        dialog = new Dialog(context, R.style.CustomDialogTheme);
        dialog.getWindow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_model);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        ApIServices apIServices = new ApIServices(context, MaintanencedashboardViewModel.this);
        apIServices.dooperation(url, jsonParams);
    }


    @Override
    public void sucessresponse(String val) {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        if (count == 0) {
            removeseconduserresponse.postValue(val);
        } else if (count == 1) {
            additionaldriverresponse.postValue(val);
        }

    }

    @Override
    public void errorreponse() {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
    }

    @Override
    public void jsonexception() {
        binding.apiload.setVisibility(View.INVISIBLE);
        dialog.dismiss();
        AppUtils.toastprint(context, "Json Exception");
    }

    public void back() {
        context.onBackPressed();
    }

    public void close() {
        context.startActivity(new Intent(context, Dashboard_constrain.class));
        context.finish();
    }
}
