package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class reviewspojo implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("driver_profile")
    Driver_profile Driver_profileObject;
    @SerializedName("user_profile")
    User_profile User_profileObject;
    @SerializedName("ride_ratting_status")
    private String ride_ratting_status;
    @SerializedName("optionsFor")
    private String optionsFor;
    @SerializedName("total")
    private String total;

    public ArrayList<Review_options> getReview_options() {
        return review_options;
    }

    public void setReview_options(ArrayList<Review_options> review_options) {
        this.review_options = review_options;
    }

    @SerializedName("review_options")
    ArrayList< Review_options > review_options = new ArrayList < Review_options > ();

    public class Review_options{
        @SerializedName("option_title")
        private String option_title;
        @SerializedName("option_id")
        private String option_id;

        public String getRatingcount() {
            return ratingcount;
        }

        public void setRatingcount(String ratingcount) {
            this.ratingcount = ratingcount;
        }

        @SerializedName("ratingcount")
        private String ratingcount;


        // Getter Methods

        public String getOption_title() {
            return option_title;
        }

        public String getOption_id() {
            return option_id;
        }

        // Setter Methods

        public void setOption_title(String option_title) {
            this.option_title = option_title;
        }

        public void setOption_id(String option_id) {
            this.option_id = option_id;
        }
    }
    // Getter Methods 

    public String getStatus() {
        return status;
    }

    public Driver_profile getDriver_profile() {
        return Driver_profileObject;
    }

    public User_profile getUser_profile() {
        return User_profileObject;
    }

    public String getRide_ratting_status() {
        return ride_ratting_status;
    }

    public String getOptionsFor() {
        return optionsFor;
    }

    public String getTotal() {
        return total;
    }

    // Setter Methods 

    public void setStatus(String status) {
        this.status = status;
    }

    public void setDriver_profile(Driver_profile driver_profileObject) {
        this.Driver_profileObject = driver_profileObject;
    }

    public void setUser_profile(User_profile user_profileObject) {
        this.User_profileObject = user_profileObject;
    }

    public void setRide_ratting_status(String ride_ratting_status) {
        this.ride_ratting_status = ride_ratting_status;
    }

    public void setOptionsFor(String optionsFor) {
        this.optionsFor = optionsFor;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public class User_profile {
        @SerializedName("user_id")
        private String user_id;
        @SerializedName("user_name")
        private String user_name;
        @SerializedName("user_gender")
        private String user_gender;
        @SerializedName("user_image")
        private String user_image;
        @SerializedName("user_review")
        private String user_review;
        @SerializedName("member_since")
        private String member_since;
        @SerializedName("location")
        private String location;


        // Getter Methods 

        public String getUser_id() {
            return user_id;
        }

        public String getUser_name() {
            return user_name;
        }

        public String getUser_gender() {
            return user_gender;
        }

        public String getUser_image() {
            return user_image;
        }

        public String getUser_review() {
            return user_review;
        }

        public String getMember_since() {
            return member_since;
        }

        public String getLocation() {
            return location;
        }

        // Setter Methods 

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public void setUser_name(String user_name) {
            this.user_name = user_name;
        }

        public void setUser_gender(String user_gender) {
            this.user_gender = user_gender;
        }

        public void setUser_image(String user_image) {
            this.user_image = user_image;
        }

        public void setUser_review(String user_review) {
            this.user_review = user_review;
        }

        public void setMember_since(String member_since) {
            this.member_since = member_since;
        }

        public void setLocation(String location) {
            this.location = location;
        }
        
    }
    public class Driver_profile {
        @SerializedName("driver_id")
        private String driver_id;
        @SerializedName("driver_name")
        private String driver_name;
        @SerializedName("driver_image")
        private String driver_image;
        @SerializedName("driver_review")
        private String driver_review;
        @SerializedName("vehicle_number")
        private String vehicle_number;
        @SerializedName("vehicle_model")
        private String vehicle_model;
        @SerializedName("last_ride")
        private String last_ride;


        // Getter Methods 

        public String getDriver_id() {
            return driver_id;
        }

        public String getDriver_name() {
            return driver_name;
        }

        public String getDriver_image() {
            return driver_image;
        }

        public String getDriver_review() {
            return driver_review;
        }

        public String getVehicle_number() {
            return vehicle_number;
        }

        public String getVehicle_model() {
            return vehicle_model;
        }

        public String getLast_ride() {
            return last_ride;
        }

        // Setter Methods 

        public void setDriver_id(String driver_id) {
            this.driver_id = driver_id;
        }

        public void setDriver_name(String driver_name) {
            this.driver_name = driver_name;
        }

        public void setDriver_image(String driver_image) {
            this.driver_image = driver_image;
        }

        public void setDriver_review(String driver_review) {
            this.driver_review = driver_review;
        }

        public void setVehicle_number(String vehicle_number) {
            this.vehicle_number = vehicle_number;
        }

        public void setVehicle_model(String vehicle_model) {
            this.vehicle_model = vehicle_model;
        }

        public void setLast_ride(String last_ride) {
            this.last_ride = last_ride;
        }
    }
    
}


