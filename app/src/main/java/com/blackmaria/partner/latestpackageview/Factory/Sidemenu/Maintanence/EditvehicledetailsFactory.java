package com.blackmaria.partner.latestpackageview.Factory.Sidemenu.Maintanence;

import android.app.Activity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.blackmaria.partner.latestpackageview.vieewmodel.sidemenu.maintanence.EditvehicledetailsViewModel;

public class EditvehicledetailsFactory extends ViewModelProvider.NewInstanceFactory {

    private Activity context;

    public EditvehicledetailsFactory(Activity context) {
        this.context = context;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new EditvehicledetailsViewModel(context);
    }
}
