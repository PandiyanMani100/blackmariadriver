package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class withdrawalstatementpojo implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;


    // Getter Methods 

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods 

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }

    public class Response {
        @SerializedName("period_statment")
        private String period_statment;
        @SerializedName("available_amount")
        private String available_amount;
        @SerializedName("income_statement_txt")
        private String income_statement_txt;
        @SerializedName("completed_trip")
        private String completed_trip;
        @SerializedName("paid_by_cash")
        private String paid_by_cash;
        @SerializedName("by_card")
        private String by_card;
        @SerializedName("by_wallet")
        private String by_wallet;
        @SerializedName("deduct_amount")
        private String deduct_amount;
        @SerializedName("gross_earning")
        private String gross_earning;
        @SerializedName("nett_earn")
        private String nett_earn;
        @SerializedName("currency")
        private String currency;
        @SerializedName("fees_charge")
        private String fees_charge;
        


        // Getter Methods 

        public String getPeriod_statment() {
            return period_statment;
        }

        public String getAvailable_amount() {
            return available_amount;
        }

        public String getIncome_statement_txt() {
            return income_statement_txt;
        }

        public String getCompleted_trip() {
            return completed_trip;
        }

        public String getPaid_by_cash() {
            return paid_by_cash;
        }

        public String getBy_card() {
            return by_card;
        }

        public String getBy_wallet() {
            return by_wallet;
        }

        public String getDeduct_amount() {
            return deduct_amount;
        }

        public String getGross_earning() {
            return gross_earning;
        }

        public String getNett_earn() {
            return nett_earn;
        }

        public String getCurrency() {
            return currency;
        }

        public String getFees_charge() {
            return fees_charge;
        }

        // Setter Methods 

        public void setPeriod_statment(String period_statment) {
            this.period_statment = period_statment;
        }

        public void setAvailable_amount(String available_amount) {
            this.available_amount = available_amount;
        }

        public void setIncome_statement_txt(String income_statement_txt) {
            this.income_statement_txt = income_statement_txt;
        }

        public void setCompleted_trip(String completed_trip) {
            this.completed_trip = completed_trip;
        }

        public void setPaid_by_cash(String paid_by_cash) {
            this.paid_by_cash = paid_by_cash;
        }

        public void setBy_card(String by_card) {
            this.by_card = by_card;
        }

        public void setBy_wallet(String by_wallet) {
            this.by_wallet = by_wallet;
        }

        public void setDeduct_amount(String deduct_amount) {
            this.deduct_amount = deduct_amount;
        }

        public void setGross_earning(String gross_earning) {
            this.gross_earning = gross_earning;
        }

        public void setNett_earn(String nett_earn) {
            this.nett_earn = nett_earn;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public void setFees_charge(String fees_charge) {
            this.fees_charge = fees_charge;
        }
    }
}

