package com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Withdraw.Paypal;


import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.databinding.ActivityPaypalamountconfirmBinding;
import com.blackmaria.partner.iconstant.Iconstant;
import com.blackmaria.partner.latestpackageview.Factory.Dashboard.Wallet.Withdraw.Paypal.WithdrawamountenterFactory;
import com.blackmaria.partner.latestpackageview.Model.Paypalwithdrawpincheck;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Pin_activity;
import com.blackmaria.partner.latestpackageview.view.Dashboard.Wallet.Withdraw.Bank.WithdrawbanksuccessConstrain;
import com.blackmaria.partner.latestpackageview.vieewmodel.dashboard.wallet.withdraw.paypal.PaypalamountconfirmViewModel;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class Paypalamountconfirm extends AppCompatActivity {

    private ActivityPaypalamountconfirmBinding binding;
    private PaypalamountconfirmViewModel paypalamountconfirmViewModel;
    private String sDriverID = "", paypal_amount = "", paypal_id = "", paypal_paymentcode = "", currency = "";
    private SessionManager sessionManager;

    @Override
    protected void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);}
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);}
    }
    AccessLanguagefromlocaldb db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new AccessLanguagefromlocaldb(this);
//        EventBus.getDefault().register(this);
        binding = DataBindingUtil.setContentView(Paypalamountconfirm.this, R.layout.activity_paypalamountconfirm);
        paypalamountconfirmViewModel = ViewModelProviders.of(this, new WithdrawamountenterFactory(this)).get(PaypalamountconfirmViewModel.class);
        binding.setPaypalamountconfirmViewModel(paypalamountconfirmViewModel);
        paypalamountconfirmViewModel.setIds(binding);

        initView();
        clicklistener();
        setResponse();

        binding.datemonthyear.setText(db.getvalue("fastwallet"));
        binding.paytopaypal.setText(db.getvalue("paytopaypal"));
        binding.confirm.setText(db.getvalue("confirm_lable"));
        binding.cancel.setText(db.getvalue("cancel_lable"));
    }

    private void initView() {
        sessionManager = SessionManager.getInstance(this);
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        if (getIntent().hasExtra("transfer_amount")) {
            paypal_amount = getIntent().getStringExtra("transfer_amount");
            paypal_paymentcode = getIntent().getStringExtra("paypalcode");
            paypal_id = getIntent().getStringExtra("paypalid");
            currency = getIntent().getStringExtra("currency");
            binding.amount.setText(currency + " " + paypal_amount);
            binding.email.setText(paypal_id);
        }
    }

    private void clicklistener() {
        binding.confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent inten = new Intent(Paypalamountconfirm.this, Pin_activity.class);
                inten.putExtra("mode", "paypalwithdrawpayment");
                startActivity(inten);
            }
        });
    }

    private void setResponse() {
        paypalamountconfirmViewModel.getPaypalapihitresponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equalsIgnoreCase("1")) {
                        Intent i = new Intent(Paypalamountconfirm.this, WithdrawbanksuccessConstrain.class);
                        i.putExtra("json", jsonObject.toString());
                        i.putExtra("paypal", "paypal");
                        startActivity(i);
                    }else{

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Subscribe
    public void pincheck(Paypalwithdrawpincheck pojo) {
        if (pojo.isIspincheck()) {
            HashMap<String, String> jsonParams = new HashMap<String, String>();
            jsonParams.put("driver_id", sDriverID);
            jsonParams.put("amount", paypal_amount);
            jsonParams.put("mode", paypal_paymentcode);
            jsonParams.put("paypal_id", paypal_id);
            paypalamountconfirmViewModel.walletwithdrawtopaypal(Iconstant.wallet_withdraw_amount_url, jsonParams);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);}
    }
}
