package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

public class Rides {
    @SerializedName("ride_id")
    private String ride_id;
    @SerializedName("ride_time")
    private String ride_time;
    @SerializedName("ride_date")
    private String ride_date;
    @SerializedName("pickup")
    private String pickup;
    @SerializedName("ride_status")
    private String ride_status;
    @SerializedName("distance")
    private String distance;
    @SerializedName("ride_type")
    private String ride_type;
    @SerializedName("ride_category")
    private String ride_category;
    @SerializedName("display_status")
    private String display_status;
    @SerializedName("user_image")
    private String user_image;
    @SerializedName("user_review")
    private String user_review;
    @SerializedName("user_name")
    private String user_name;
    @SerializedName("group")
    private String group;
    @SerializedName("s_no")
    private String s_no;
    @SerializedName("time")
    private String time;
    @SerializedName("datetime")
    private String datetime;

    public String getDatetime_timestamp() {
        return datetime_timestamp;
    }

    public void setDatetime_timestamp(String datetime_timestamp) {
        this.datetime_timestamp = datetime_timestamp;
    }

    @SerializedName("datetime_timestamp")
    private String datetime_timestamp;


    // Getter Methods

    public String getRide_id() {
        return ride_id;
    }

    public String getRide_time() {
        return ride_time;
    }

    public String getRide_date() {
        return ride_date;
    }

    public String getPickup() {
        return pickup;
    }

    public String getRide_status() {
        return ride_status;
    }

    public String getDistance() {
        return distance;
    }

    public String getRide_type() {
        return ride_type;
    }

    public String getRide_category() {
        return ride_category;
    }

    public String getDisplay_status() {
        return display_status;
    }

    public String getUser_image() {
        return user_image;
    }

    public String getUser_review() {
        return user_review;
    }

    public String getUser_name() {
        return user_name;
    }

    public String getGroup() {
        return group;
    }

    public String getS_no() {
        return s_no;
    }

    public String getTime() {
        return time;
    }

    public String getDatetime() {
        return datetime;
    }

    // Setter Methods

    public void setRide_id(String ride_id) {
        this.ride_id = ride_id;
    }

    public void setRide_time(String ride_time) {
        this.ride_time = ride_time;
    }

    public void setRide_date(String ride_date) {
        this.ride_date = ride_date;
    }

    public void setPickup(String pickup) {
        this.pickup = pickup;
    }

    public void setRide_status(String ride_status) {
        this.ride_status = ride_status;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public void setRide_type(String ride_type) {
        this.ride_type = ride_type;
    }

    public void setRide_category(String ride_category) {
        this.ride_category = ride_category;
    }

    public void setDisplay_status(String display_status) {
        this.display_status = display_status;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public void setUser_review(String user_review) {
        this.user_review = user_review;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public void setS_no(String s_no) {
        this.s_no = s_no;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }
}