package com.blackmaria.partner.latestpackageview.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class onlineupdatelocation implements Serializable {
    @SerializedName("status")
    private String status;
    @SerializedName("response")
    Response ResponseObject;


    // Getter Methods

    public String getStatus() {
        return status;
    }

    public Response getResponse() {
        return ResponseObject;
    }

    // Setter Methods

    public void setStatus(String status) {
        this.status = status;
    }

    public void setResponse(Response responseObject) {
        this.ResponseObject = responseObject;
    }

    public class Response {
        @SerializedName("message")
        private String message;
        @SerializedName("availability")
        private String availability;
        @SerializedName("ride_id")
        private String ride_id;
        @SerializedName("is_continue")
        private String is_continue;
        @SerializedName("errorMsg")
        private String errorMsg;
        @SerializedName("wallet_constraint")
        private String wallet_constraint;
        @SerializedName("wallet_message")
        private String wallet_message;
        @SerializedName("currency_code")
        private String currency_code;
        @SerializedName("min_wallet_amount")
        private String min_wallet_amount;
        @SerializedName("current_balance")
        private float current_balance;
        @SerializedName("verify_status")
        private String verify_status;


        // Getter Methods

        public String getMessage() {
            return message;
        }

        public String getAvailability() {
            return availability;
        }

        public String getRide_id() {
            return ride_id;
        }

        public String getIs_continue() {
            return is_continue;
        }

        public String getErrorMsg() {
            return errorMsg;
        }

        public String getWallet_constraint() {
            return wallet_constraint;
        }

        public String getWallet_message() {
            return wallet_message;
        }

        public String getCurrency_code() {
            return currency_code;
        }

        public String getMin_wallet_amount() {
            return min_wallet_amount;
        }

        public float getCurrent_balance() {
            return current_balance;
        }

        public String getVerify_status() {
            return verify_status;
        }

        // Setter Methods

        public void setMessage(String message) {
            this.message = message;
        }

        public void setAvailability(String availability) {
            this.availability = availability;
        }

        public void setRide_id(String ride_id) {
            this.ride_id = ride_id;
        }

        public void setIs_continue(String is_continue) {
            this.is_continue = is_continue;
        }

        public void setErrorMsg(String errorMsg) {
            this.errorMsg = errorMsg;
        }

        public void setWallet_constraint(String wallet_constraint) {
            this.wallet_constraint = wallet_constraint;
        }

        public void setWallet_message(String wallet_message) {
            this.wallet_message = wallet_message;
        }

        public void setCurrency_code(String currency_code) {
            this.currency_code = currency_code;
        }

        public void setMin_wallet_amount(String min_wallet_amount) {
            this.min_wallet_amount = min_wallet_amount;
        }

        public void setCurrent_balance(float current_balance) {
            this.current_balance = current_balance;
        }

        public void setVerify_status(String verify_status) {
            this.verify_status = verify_status;
        }
    }
}

