package com.blackmaria.partner.latestpackageview.view.signup;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackmaria.partner.R;
import com.blackmaria.partner.Utils.AccessLanguagefromlocaldb;
import com.blackmaria.partner.Utils.SessionManager;
import com.blackmaria.partner.latestpackageview.Utils.AppUtils;
import com.blackmaria.partner.latestpackageview.view.login.Signin_contrain;

import java.util.HashMap;

public class RegistrationSuccess extends AppCompatActivity implements View.OnClickListener {

    private SessionManager sessionManager;


    private String sDriverID = "", name = "", image = "";
    private TextView Loginnow, RL_readMe, username,contentaware;
    private ImageView userimage;
    String walletammount = "";
TextView credites;
    public String sContactNumber = "";
    final int PERMISSION_REQUEST_CODE = 111;
    AccessLanguagefromlocaldb db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_success_constrain);
        db = new AccessLanguagefromlocaldb(this);
         walletammount = (String) getIntent().getExtras().get("welcome_amount");
        initialize();
        credites.setText("youv'e got free credit "+walletammount);
    }

    private void initialize() {
        sessionManager = SessionManager.getInstance(RegistrationSuccess.this);
        sContactNumber = sessionManager.getContactNumber();
        HashMap<String, String> user = sessionManager.getUserDetails();
        sDriverID = user.get(SessionManager.KEY_DRIVERID);
        image = user.get(SessionManager.KEY_DRIVER_IMAGE);
        name = user.get(SessionManager.KEY_DRIVER_NAME);
        credites= findViewById(R.id.credites);
        RL_readMe = findViewById(R.id.readme);
        Loginnow = findViewById(R.id.loginnow);
        userimage = findViewById(R.id.userimage);
        username = findViewById(R.id.username);
        contentaware = findViewById(R.id.contentaware);
        contentaware.setMovementMethod(new ScrollingMovementMethod());
        contentaware.setText(db.getvalue("content_registration_success"));
        Loginnow.setOnClickListener(this);
        RL_readMe.setOnClickListener(this);
        RL_readMe.setText(db.getvalue("readme"));

        TextView welcome = findViewById(R.id.welcome);
        welcome.setText(db.getvalue("welcomeblackaria"));

        TextView submitted = findViewById(R.id.submitted);
        submitted.setText(db.getvalue("registrationsubmitted"));


        TextView loginnow = findViewById(R.id.loginnow);
        loginnow.setText(db.getvalue("label_login_now"));


        TextView thankyou = findViewById(R.id.thankyou);
        thankyou.setText(db.getvalue("thank_you_for_joining"));



        if (getIntent().hasExtra("hashMap")) {
            HashMap<String, String> hashMap = (HashMap<String, String>) getIntent().getSerializableExtra("hashMap");
            AppUtils.setImageView(RegistrationSuccess.this, "https://blackmaria.casperon.co/images/users/"+hashMap.get("profile_image"), userimage);
            username.setText(hashMap.get("driver_name"));
        }

    }


    @Override
    public void onClick(View view) {
        if (view == RL_readMe) {
            showBasicProcedures();
        } else if (view == Loginnow) {
            Intent intent = new Intent(RegistrationSuccess.this, Signin_contrain.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.enter, R.anim.exit);
            finish();
        }
    }

    private void showBasicProcedures() {

        final Dialog basicProceduresDialog = new Dialog(RegistrationSuccess.this, R.style.SlideUpDialog);
        basicProceduresDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        basicProceduresDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        basicProceduresDialog.setCancelable(true);
        basicProceduresDialog.setContentView(R.layout.alert_registration_read_me);


        //--------Adjusting Dialog width and height-----
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.85);//fill only 85% of the screen
        int screenHeight = (int) (metrics.heightPixels * 0.85);//fill only 85% of the screen
        basicProceduresDialog.getWindow().setLayout(screenWidth, screenHeight);


        final ImageView Iv_close = (ImageView) basicProceduresDialog.findViewById(R.id.img_close);

        TextView txt_basic_procedures = (TextView) basicProceduresDialog.findViewById(R.id.txt_basic_procedures);
        txt_basic_procedures.setText(db.getvalue("basic_procedures"));
        TextView txt_title_unemployed_person = (TextView) basicProceduresDialog.findViewById(R.id.txt_title_unemployed_person);
        txt_title_unemployed_person.setText(db.getvalue("unemployed_person"));
        TextView txt_unemployed_person = (TextView) basicProceduresDialog.findViewById(R.id.txt_unemployed_person);
        txt_unemployed_person.setText(db.getvalue("you_may_drive_with_us_at_anytime_without_restriction_24_hours_a_day"));

        TextView txt_title_working_person = (TextView) basicProceduresDialog.findViewById(R.id.txt_title_working_person);
        txt_title_working_person.setText(db.getvalue("working_person"));
        TextView txt_working_person = (TextView) basicProceduresDialog.findViewById(R.id.txt_working_person);
        txt_working_person.setText(db.getvalue("for_working_person_please_drive_outside_your_working_hours"));





        TextView txt_title_woman_driver = (TextView) basicProceduresDialog.findViewById(R.id.txt_title_woman_driver);
        txt_title_woman_driver.setText(db.getvalue("woman_driver"));
        TextView txt_woman_driver = (TextView) basicProceduresDialog.findViewById(R.id.txt_woman_driver);
        txt_woman_driver.setText(db.getvalue("your_are_not_allowed_to_drive_from_7pm_to_7am_consent_letter_from_husband_required_except_single_mother"));
        TextView txt_title_student_driver = (TextView) basicProceduresDialog.findViewById(R.id.txt_title_student_driver);
        txt_title_student_driver.setText(db.getvalue("student_driver"));
        TextView txt_student_driver = (TextView) basicProceduresDialog.findViewById(R.id.txt_student_driver);
        txt_student_driver.setText(db.getvalue("driving_are_allowed_not_more_than_6_hours_per_day_consent_letter_from_parents_guardian_is_required"));
        TextView sdd = (TextView) basicProceduresDialog.findViewById(R.id.sdd);
        sdd.setText(db.getvalue("please_consider_to_follow_the_above_procedures_for_mutual_benefit_any_claim_legal_action_againts_our_operating_procedures_will_not_be_accepted"));






        Iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                basicProceduresDialog.dismiss();
            }
        });

        basicProceduresDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                RL_readMe.setClickable(true);
            }
        });

        basicProceduresDialog.show();
        RL_readMe.setClickable(false);

    }

    @Override
    public void onBackPressed() {

    }
}