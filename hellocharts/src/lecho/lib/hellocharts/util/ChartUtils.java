package lecho.lib.hellocharts.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.util.TypedValue;

import lecho.lib.hellocharts.R;
import lecho.lib.hellocharts.model.Line;

public abstract class ChartUtils {

    public static final int DEFAULT_COLOR = Color.parseColor("#D9AB00");
    public static final int DEFAULT_COLOR1 = Color.parseColor("#3399CC");
    public static final int DEFAULT_DARKEN_COLOR = Color.parseColor("#D9AB00");
    public static final int[] COLORS = new int[]{DEFAULT_COLOR};
    public static final int[] COLORS1 = new int[]{DEFAULT_COLOR1};
    private static final float DARKEN_SATURATION = 5.10f;
    private static final float DARKEN_INTENSITY = 0.0f;


    private static int COLOR_INDEX = 0;

    public static final int pickColor() {

//        float hoffset = 240;
//
//        float h = 200 + hoffset; // Those value should be not more than 360, so yourvalue should be reduced to 0 (Blue) till 120 (Red)
//        float s = 1.0f;
//        float v = 1.0f;
//
//        float[] hsv = {h, s, v};
//
//        Color.HSVToColor(hsv);

        return COLORS[(int) Math.round(Math.random() * (COLORS.length - 1))];
    }


    public static final int pickColor1() {

//        float hoffset = 240;
//
//        float h = 200 + hoffset; // Those value should be not more than 360, so yourvalue should be reduced to 0 (Blue) till 120 (Red)
//        float s = 1.0f;
//        float v = 1.0f;
//
//        float[] hsv = {h, s, v};
//
//        Color.HSVToColor(hsv);

        return COLORS1[(int) Math.round(Math.random() * (COLORS1.length - 1))];
    }

    public static final int nextColor() {
        if (COLOR_INDEX >= COLORS.length) {
            COLOR_INDEX = 0;
        }
        return COLORS[COLOR_INDEX++];
    }

    public static int dp2px(float density, int dp) {
        if (dp == 0) {
            return 0;
        }
        return (int) (dp * density + 0.5f);

    }

    public static int px2dp(float density, int px) {
        return (int) Math.ceil(px / density);
    }

    public static int sp2px(float scaledDensity, int sp) {
        if (sp == 0) {
            return 0;
        }
        return (int) (sp * scaledDensity + 0.5f);
    }

    public static int px2sp(float scaledDensity, int px) {
        return (int) Math.ceil(px / scaledDensity);
    }

    public static int mm2px(Context context, int mm) {
        return (int) (TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_MM, mm, context.getResources()
                .getDisplayMetrics()) + 0.5f);
    }

    public static int darkenColor(int color) {
        float[] hsv = new float[3];
        int alpha = Color.alpha(color);
        Color.colorToHSV(color, hsv);
        hsv[1] = Math.min(hsv[1] * DARKEN_SATURATION, 2.0f);
        hsv[2] = hsv[2] * DARKEN_INTENSITY;
        int tempColor = Color.HSVToColor(hsv);
        return Color.argb(alpha, Color.red(tempColor), Color.green(tempColor), Color.blue(tempColor));
    }


    protected void drawCubicBezier() {
        float hoffset = 240;

        float h = 230 + hoffset; // Those value should be not more than 360, so yourvalue should be reduced to 0 (Blue) till 120 (Red)
        float s = 1.0f;
        float v = 1.0f;

        float[] hsv = {h, s, v};

        Color.HSVToColor(hsv);

    }

    public static final Canvas pickColor1(Canvas canvas ) {
        Paint p = new Paint();
        // start at 0,0 and go to 0,max to use a vertical
        // gradient the full height of the screen.
        p.setShader(new LinearGradient(0, 0, 0, 1, Color.BLACK, Color.WHITE, Shader.TileMode.MIRROR));
        canvas.drawPaint(p);


        return canvas;
    }


}
